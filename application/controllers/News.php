<?php
class News extends CI_Controller {
	function __construct() {
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
	    $this->load->model('dsw_model');
	    $this->load->model('News_model');
	}

	public function index() {
    	if (! $this->session->userdata('isLoggedIn') ) redirect("login/show_login");
		$this->grid();
	}

	public function grid() {
		$this->fc->log('Admin - News Ticker');
		$row = $this->News_model->get_data();
		$config = $this->fc->pagination( site_url("news/grid"), count($row), 10, '3');
		$this->pagination->initialize($config);

		$data['table'] = array_slice($row, $this->uri->segment(3), 10);
		$data['menu']  = $this->dsw_model->get_menu( $this->uri->segment(1) );
		$data['bread'] = array('header'=>'Admin News Ticker', 'subheader'=>'Admin');
		$data['view']  = "news/V_news_grid";
		$this->load->view('main/utama', $data);
	}

	function rekam() {
		$ruh = 'Rekam'; if ($this->uri->segment(3)=='u') $ruh = 'Ubah'; if ($this->uri->segment(3)=='h') $ruh = 'Hapus';
		$data['ruh']   = $ruh;
		$data['table'] = $this->News_model->get_row( $this->uri->segment(4) );

		$data['menu']  = $this->dsw_model->get_menu( $this->uri->segment(1) );
		$data['bread'] = array('header'=>'Admin News Ticker', 'subheader'=>'Admin');
		$data['view']  = "news/V_news_rekam";
		$this->load->view('main/utama', $data);
	}

	function crud() {
		if ($_POST['ruh']=='Rekam') {
			$this->News_model->save();
		} else {
			if ($_POST['idnews']<>'') {
				$this->News_model->save();
			}
		}
		redirect("news/grid");
	}

	function cari() {
		if ($_POST['search'] == 'search') {
			$this->session->set_userdata('cari', $_POST['cari']);
		} else {
			$this->session->set_userdata('cari', '');
		}

		if ($_POST['nmfunction']=='dash') {
			$this->dash();
		} else {
			$this->grid();
		}
	}

	public function dash() {
		$row = $this->News_model->get_data();
		$config = $this->fc->pagination( site_url("news/dash"), count($row), 10, '3');
		$this->pagination->initialize($config);

		$data['news'] = array_slice($row, $this->uri->segment(3), 10);

		$data['menu']  = $this->dsw_model->get_menu( $this->uri->segment(1) );
		$data['bread'] = array('header'=>'Arsip News', 'subheader'=>'News');
		$data['view'] = "news/V_news_list";
		$this->load->view('main/utama', $data);
		// $this->fc->browse( $data['nilai'] );
	}

}
