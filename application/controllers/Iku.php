<?php
class Iku extends CI_Controller {
	function __construct() {
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
	    $this->load->model('dsw_model');
	    $this->load->model('m_iku');
	}

	public function index() {

    	if (! $this->session->userdata('isLoggedIn') ) redirect("login/show_login");
		//$this->session->set_userdata( array('cari' => '') );
		$cari = '';
		if($this->input->post('cari')){
			$cari = $this->input->post('cari');
		}
		$this->grid_iku_m($cari);
	}

	public function grid_iku_m($cari = '') {
		$row = $this->m_iku->get_data_manual($cari);
		$config = $this->fc->pagination( site_url("iku/grid_iku_m"), count($row), 10, '3');
		$this->pagination->initialize($config);
		$data['nilai']  = array_slice($row,  $this->uri->segment(3), 10);
		//$data['t_user'] = $this->m_paket->json_user();
		$data['judul']  = '';
		$data['menu']  = $this->dsw_model->get_menu( $this->uri->segment(1) );
		$tahun  = $this->m_iku->master( 'PERIODE_TAHUN' );
		foreach ($tahun->result() as $key => $value) {
			$data[$value->name] = $value->value_1;
		}
		if($this->input->get('tahun') == ''){
			$data['existingYear'] = date('Y');
		}else{
			$data['existingYear'] = $this->input->get('tahun');
		}
		$data['bread'] = array('header'=>'Admin - Pelaporan IKU', 'subheader'=>'IKU Manual Admin');

		foreach($data['nilai'] as $key2 => $datas){

			$temp1 = explode("--,--",$data['nilai'][$key2]['sasaran_strategis']);
			$data['nilai'][$key2]['sasaran_strategis'] = array();
			foreach($temp1 as $key3 => $data1){
				$data['nilai'][$key2]['sasaran_strategis'][$key3] = $data1;
				}
		}

		$data['view'] = "iku/iku_manual/V_paket_grid";

		$this->load->view('main/utama', $data);
	}

    function getIku()
    {
        $ss = $this->input->get('ss');
        $id = $this->input->get('id');
        $tmp = '';
        $data = $this->m_iku->getIku($ss, $id);
        if (!empty($data)) {
            $tmp .= "<option value=''>- IKU -</option>";
            foreach ($data->result() as $row) {
                $tmp .=  "<option value='" . $row->id . "'>" . $row->iku . "</option>";
            }
        } else {
            $tmp .= "<option value=''>- IKU -</option>";
        }
        die($tmp);
    }


	public function iku() {
    	if (! $this->session->userdata('isLoggedIn') ) redirect("login/show_login");
		$this->session->set_userdata( array('cari' => '') );
		$this->grid();
	}

	public function grid() {
		$row = $this->m_iku->get_data();
		$config = $this->fc->pagination( site_url("iku/grid"), count($row), 10, '3');
		$this->pagination->initialize($config);

		$data['nilai']  = array_slice($row, $this->uri->segment(3), 10);
		//$data['t_user'] = $this->m_paket->json_user();
		$data['judul']  = '';

		$data['menu']  = $this->dsw_model->get_menu( $this->uri->segment(1) );

		$data['PERSPEKTIF']  = $this->m_iku->master( 'PERSPEKTIF' );
		$data['SASARAN_STRATEGIS']  = $this->m_iku->master( 'SASARAN_STRATEGIS' );
		$data['IKU']  = $this->m_iku->master( 'IKU' );
		$data['SATUAN_PENGUKURAN']  = $this->m_iku->master( 'SATUAN_PENGUKURAN' );
		$data['JENIS_ASPEK_TARGET']  = $this->m_iku->master( 'JENIS_ASPEK_TARGET' );
		$data['TINGKAT_KENDALI_IKU']  = $this->m_iku->master( 'TINGKAT_KENDALI_IKU' );
		$data['TINGKAT_VALIDITAS_IKU']  = $this->m_iku->master( 'TINGKAT_VALIDITAS_IKU' );
		$data['JENIS_CASCADING_IKU']  = $this->m_iku->master( 'JENIS_CASCADING_IKU' );
		$data['METODE_CASCADING']  = $this->m_iku->master( 'METODE_CASCADING' );
		$data['JENIS_KONSOLIDASI_PERIODE']  = $this->m_iku->master( 'JENIS_KONSOLIDASI_PERIODE' );
		$data['JENIS_KONSOLIDASI_LOKASI']  = $this->m_iku->master( 'JENIS_KONSOLIDASI_LOKASI' );
		$data['POLARISASI_INDIKATOR_KINERJA']  = $this->m_iku->master( 'POLARISASI_INDIKATOR_KINERJA' );
		$data['PERIODE_PELAPORAN']  = $this->m_iku->master( 'PERIODE_PELAPORAN' );

		$data['bread'] = array('header'=>'Admin IKU', 'subheader'=>'IKU Admin');
		$data['view'] = "iku/V_paket_grid";
		$this->load->view('main/utama', $data);
	}

	function crud() {

        $this->load->library('upload');
        $config['upload_path'] = './files/file_document_iku/'; //path folder
        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp|pdf|doc|docx|xls|xlsx|odt|rar|zip|csv|txt'; //type yang dapat diakses bisa anda sesuaikan
        $config['max_size'] = '9999'; //maksimum besar file 2M
        $config['file_name'] = $_FILES['userfile']['name']; //nama yang terupload nantinya

        $this->upload->initialize($config);
	 	if($_FILES['userfile']['name']){
            if ($this->upload->do_upload('userfile')) {
                $data = $this->upload->data();
				$datas['file_document'] = $data['file_name'];
            }else{
				$datas['file_document'] = '';
	            print_r($this->upload->display_errors());
	            exit;
            }
		}else if($this->input->post('action') == 'Ubah' AND isset($_FILES['userfile']['name'])){
		        $rows_ubah = $this->m_iku->get_rows($this->input->post('idForm'));
		        foreach($rows_ubah->result() as $row_ubah){
					$datas['file_document'] = $row_ubah->file_document;
		        }
		}else if(empty($_FILES['userfile']['name'])){
				$datas['file_document'] = '';
		}


	//	$id_periode = $this->m_iku->select_id_periode();
	//	foreach ($id_periode->result() as $value) {
			$datas['action'] = $this->input->post('action');
			$datas['idForm'] = $this->input->post('idForm');
			$datas['userForm'] = $this->input->post('userForm');
			$datas['perspektifForm'] = $this->input->post('perspektifForm');
			$datas['sasaranStrategisForm'] = $this->input->post('sasaranStrategisForm');
			$datas['descSasaranStrategisForm'] = $this->input->post('descSasaranStrategisForm');
			$datas['IKUMonitoringForm'] = $this->input->post('IKUMonitoringForm');
			$datas['IKUAtasanForm'] = $this->input->post('IKUAtasanForm');
			$datas['IKUForm'] = $this->input->post('IKUForm');
			$datas['definisiForm'] = $this->input->post('definisiForm');
			$datas['formulaForm'] = $this->input->post('formulaForm');
			$datas['tujuanForm'] = $this->input->post('tujuanForm');
			$datas['satuanPengukuranForm'] = $this->input->post('satuanPengukuranForm');
			$datas['jenisAspekTargetForm'] = $this->input->post('jenisAspekTargetForm');
			$datas['tingkatKendaliIKUForm'] = $this->input->post('tingkatKendaliIKUForm');
			$datas['tingkatValiditasIKUForm'] = $this->input->post('tingkatValiditasIKUForm');
			$datas['pihakPJIKUForm'] = $this->input->post('pihakPJIKUForm');
			$datas['pihakPenyediaJasaForm'] = $this->input->post('pihakPenyediaJasaForm');
			$datas['sumberDataForm'] = $this->input->post('sumberDataForm');
			$datas['jenisCascadingIKUForm'] = $this->input->post('jenisCascadingIKUForm');
			$datas['metodeCascadingForm'] = $this->input->post('metodeCascadingForm');
			$datas['jenisKonsolidasiPeriodeIdForm'] = $this->input->post('jenisKonsolidasiPeriodeIdForm');
			$datas['jenisKonsolidasiLokasiForm'] = $this->input->post('jenisKonsolidasiLokasiForm');
			$datas['polarisasiIndikatorKinerjaForm'] = $this->input->post('polarisasiIndikatorKinerjaForm');
			$datas['periodePelaporanForm'] = $this->input->post('periodePelaporanForm');
			$datas['konversi120Form'] = $this->input->post('konversi120Form');
			$datas['idPeriode'] = '';
			$datas['createdTimeStamp'] = date('Y-m-d H:i:s');
			$this->m_iku->save($datas);

			$getPeriode = $this->m_iku->getPeriode($datas['periodePelaporanForm']);
			$totalcolumn = $this->input->post('total-column');
			$idIku = $this->m_iku->getByAll($datas);
			foreach($idIku->result() as $ikuId){
				if($datas['action'] == 'Ubah'){
					$this->m_iku->deletePeriode($ikuId->id);
				}
				foreach($getPeriode->result() as $periode){
					for($x=1;$x<=$periode->value_2;$x++){
						for($z=1;$z<=$totalcolumn;$z++){
							$seq = $x;
							$tahun = $this->input->post('tahun-'.$z);
							//echo $tahun;  echo '</br>';
							$target = $this->input->post('data-tahun-T-'.$x.'-'.$z);
							$this->m_iku->savePeriode($ikuId->id, $datas['periodePelaporanForm'], $tahun, $seq, $target, 'TARGET');
							//echo 'tahun:'.$tahun.'|seq:'.$seq.'|target:'.$target.'|'.'TARGET';
							$realisasi = $this->input->post('data-tahun-R-'.$x.'-'.$z);
							$this->m_iku->savePeriode($ikuId->id, $datas['periodePelaporanForm'], $tahun, $seq, $realisasi, 'REALISASI');
							//echo 'tahun:'.$tahun.'|seq:'.$seq.'|target:'.$realisasi.'|'.'REALISASI';
							//echo '</br>';
						}
					}
				}
			}

	//	}

		redirect("iku");
	}

	private function set_upload_options(){
		//  upload an image options
	    $config = array();
	    $config['upload_path'] = './fileselif/';
	    $config['allowed_types'] = 'gif|jpg|png|pdf';
	    $config['max_size']      = '0';
	    $config['overwrite']     = FALSE;

	    return $config;
	}

	function crud_master() {

		$id_periode = $this->m_iku->select_seq_master($this->input->post('type'));
		foreach ($id_periode->result() as $value) {
			$datas['action'] = $this->input->post('action');
			$datas['type'] = $this->input->post('type');
			$datas['name'] = $this->input->post('nameForm');
			$datas['idForm'] = $this->input->post('idForm');
			$datas['sequence'] = $value->seq;
			$this->m_iku->save_master($datas);

		}

		redirect("iku/master");
	}

	function cari() {
		if ($_POST['search'] == 'search') {
			$this->session->set_userdata('cari', $_POST['cari']);
		} else {
			$this->session->set_userdata('cari', '');
		}

		if ($_POST['nmfunction']=='dash') {
			$this->dash();
		} else {
			$this->grid();
		}
	}


	public function dash_old() {
		$row = $this->m_iku->get_data();
		$row = $this->m_iku->get_data_dash( $row );
		$config = $this->fc->pagination( site_url("paket/dash"), count($row), 12, '3');
		$this->pagination->initialize($config);

		$data['nilai'] = array_slice($row, $this->uri->segment(3), 12);
		$data['total'] = $this->m_paket->get_total();

		$data['menu']  = $this->dsw_model->get_menu( $this->uri->segment(1) );
		$data['bread'] = array('header'=>'Informasi Paket', 'subheader'=>'Informasi Paket');
		$data['view'] = "paket/V_paket_dash";
		$this->load->view('main/utama', $data);
	}

	public function dash() {
		$row = $this->m_paket->get_data();
		$row = $this->m_paket->get_data_dash( $row );

		$data['total'] = $this->m_paket->get_total();
		print_r($data['total']);
		// $this->fc->browse( $data['total'] );
	}

	public function master() {
    	if (! $this->session->userdata('isLoggedIn') ) redirect("login/show_login");
		$this->session->set_userdata( array('cari' => '') );

		$data['judul']  = '';

		$data['menu']  = $this->dsw_model->get_menu( $this->uri->segment(1) );
		$data['PERSPEKTIF']  = $this->m_iku->master( 'PERSPEKTIF' );
		$data['SASARAN_STRATEGIS']  = $this->m_iku->master( 'SASARAN_STRATEGIS' );
		$data['IKU']  = $this->m_iku->master( 'IKUMONITORING' );
		$data['SATUAN_PENGUKURAN']  = $this->m_iku->master( 'SATUAN_PENGUKURAN' );
		$data['JENIS_ASPEK_TARGET']  = $this->m_iku->master( 'JENIS_ASPEK_TARGET' );
		$data['TINGKAT_KENDALI_IKU']  = $this->m_iku->master( 'TINGKAT_KENDALI_IKU' );
		$data['TINGKAT_VALIDITAS_IKU']  = $this->m_iku->master( 'TINGKAT_VALIDITAS_IKU' );
		$data['JENIS_CASCADING_IKU']  = $this->m_iku->master( 'JENIS_CASCADING_IKU' );
		$data['METODE_CASCADING']  = $this->m_iku->master( 'METODE_CASCADING' );
		$data['JENIS_KONSOLIDASI_PERIODE']  = $this->m_iku->master( 'JENIS_KONSOLIDASI_PERIODE' );
		$data['JENIS_KONSOLIDASI_LOKASI']  = $this->m_iku->master( 'JENIS_KONSOLIDASI_LOKASI' );
		$data['POLARISASI_INDIKATOR_KINERJA']  = $this->m_iku->master( 'POLARISASI_INDIKATOR_KINERJA' );
		$data['PERIODE_PELAPORAN']  = $this->m_iku->master( 'PERIODE_PELAPORAN' );

		$data['bread'] = array('header'=>'Master IKU', 'subheader'=>'IKU Master');
		$data['view'] = "iku/V_master";
		$this->load->view('main/utama', $data);
	}

	public function periode() {
    	if (! $this->session->userdata('isLoggedIn') ) redirect("login/show_login");
		$this->session->set_userdata( array('cari' => '') );

		$data['judul']  = '';

		$periode  = $this->m_iku->periode_table($this->input->post('id') );
		$x = 0;
		if($periode->num_rows() > 0){
			foreach ($periode->result() as $periode_key => $value) {
				$table_data[$value->periode][$value->tahun][$value->tipe] = $value->value;
				$head_data[$value->tahun][$value->tipe] = 1;
			}
			$head =' <tr><th rowspan="2"></th>';
			foreach ($head_data as $key_1 => $value):
       			$head .= '<th colspan="' . count($value) . '">' . $key_1 . '</th>';
			endforeach;
                $head .= '</tr>';
                $head .= '<tr>';
              		foreach ($head_data as $key_1 => $value):
              			foreach ($value as $key_2 => $val):
                			$head .= '<th>' . $key_2 . '</th>';
           				 endforeach;
           			endforeach;
                $head .=  '</tr>';
                $table = '';
	            foreach ($table_data as $key_3 => $value) {
	                 $table .= '<tr>';
	                    $table .= '<td>' . $key_3 .'</td>';
	              		foreach ($head_data as $key_1 => $value):
	              			foreach ($value as $key_2 => $val):
	                			$table .= '<td>' . (isset($table_data[$key_3][$key_1][$key_2])? $table_data[$key_3][$key_1][$key_2] : "Not Set Yet") . '</td>';
	           				endforeach;
	           			endforeach;
	                  $table .= '</tr>';
	            }
	        }else{
	        	$head = '';
	        	$table = '';
	        }
            $datas = $head . '=========JOIN=========' . $table;
            echo($datas);
		/*$data['menu']  = $this->dsw_model->get_menu( $this->uri->segment(1) );
		$data['table'] = $table;
		$data['head'] = $head;
		$data['bread'] = array('header'=>'Master IKU', 'subheader'=>'IKU Master');
		$data['view'] = "iku/test";
		$this->load->view('main/utama', $data);*/
	}

	public function parameter_periode_add() {
    	if (! $this->session->userdata('isLoggedIn') ) redirect("login/show_login");
		$this->session->set_userdata( array('cari' => '') );

		$data['judul']  = '';
		if($this->input->post('type') == 'tahun'){
			$periode  = $this->m_iku->get_data_periode($this->input->post('id'), 'periode');
			$tipe  = $this->m_iku->get_data_periode($this->input->post('id'), 'tipe');
			foreach($periode->result() as $per):
				foreach($tipe->result() as $tip):
					if($periode->num_rows() == 1 AND $tipe->num_rows() == 1):
						$where = "periode = '".$per->periode."' AND tipe = '".$tip->tipe."'";
						 $this->m_iku->update_tabel_periode($this->input->post('id'), $this->input->post('type'), $this->input->post('value'),$where);
					endif;
				endforeach;
			endforeach;
		}
		if($this->input->post('type') == 'tipe'){
			$datas1  = $this->m_iku->get_data_periode($this->input->post('id'), 'tahun');
			$datas2  = $this->m_iku->get_data_periode($this->input->post('id'), 'periode');
			foreach($datas1->result() as $data1):
				foreach($datas2->result() as $data2):
					if($datas1->num_rows() == 1 AND $datas2->num_rows() == 1):
						$where = "tahun = '".$data1->tahun."' AND periode = '".$data2->periode."'";
					echo $where;
						 $this->m_iku->update_tabel_periode($this->input->post('id'), $this->input->post('type'), $this->input->post('value'),$where);
					endif;
				endforeach;
			endforeach;
		}
		if($this->input->post('type') == 'periode'){
			$datas1  = $this->m_iku->get_data_periode($this->input->post('id'), 'tahun');
			$datas2  = $this->m_iku->get_data_periode($this->input->post('id'), 'tipe');
			foreach($datas1->result() as $data1):
				foreach($datas2->result() as $data2):
					if($datas1->num_rows() == 1 AND $datas2->num_rows() == 1):
						$where = "tahun = '".$data1->tahun."' AND tipe = '".$data2->tipe."'";
					echo $where;
						 $this->m_iku->update_tabel_periode($this->input->post('id'), $this->input->post('type'), $this->input->post('value'),$where);
					endif;
				endforeach;
			endforeach;
		}
		//$periode  = $this->m_iku->add_parameter($this->input->post('type'),$this->input->post('value') );
	}

	function form_add() {
    	if (! $this->session->userdata('isLoggedIn') ) redirect("login/show_login");

		$data['judul']  = 'Add New IKU';
		$data['menu']  = $this->dsw_model->get_menu( $this->uri->segment(1) );
		$data['USER']  = $this->m_iku->user(  );
		$data['PERSPEKTIF']  = $this->m_iku->master( 'PERSPEKTIF' );
		$data['SASARAN_STRATEGIS']  = $this->m_iku->master( 'SASARAN_STRATEGIS' );
		$data['IKUMONITORING']  = $this->m_iku->master( 'IKUMONITORING' );
		$data['IKUATASAN']  = $this->m_iku->ikuId();
		$data['SATUAN_PENGUKURAN']  = $this->m_iku->master( 'SATUAN_PENGUKURAN' );
		$data['JENIS_ASPEK_TARGET']  = $this->m_iku->master( 'JENIS_ASPEK_TARGET' );
		$data['TINGKAT_KENDALI_IKU']  = $this->m_iku->master( 'TINGKAT_KENDALI_IKU' );
		$data['TINGKAT_VALIDITAS_IKU']  = $this->m_iku->master( 'TINGKAT_VALIDITAS_IKU' );
		$data['JENIS_CASCADING_IKU']  = $this->m_iku->master( 'JENIS_CASCADING_IKU' );
		$data['METODE_CASCADING']  = $this->m_iku->master( 'METODE_CASCADING' );
		$data['JENIS_KONSOLIDASI_PERIODE']  = $this->m_iku->master( 'JENIS_KONSOLIDASI_PERIODE' );
		$data['JENIS_KONSOLIDASI_LOKASI']  = $this->m_iku->master( 'JENIS_KONSOLIDASI_LOKASI' );
		$data['POLARISASI_INDIKATOR_KINERJA']  = $this->m_iku->master( 'POLARISASI_INDIKATOR_KINERJA' );
		$data['PERIODE_PELAPORAN']  = $this->m_iku->master( 'PERIODE_PELAPORAN' );
		$data['bread'] = array('header'=>'Form Add IKU', 'subheader'=>'Add IKU');
		$data['view'] = "iku/V_form_add";
		$this->load->view('main/utama', $data);
	}


	function form_edit() {
    	if (! $this->session->userdata('isLoggedIn') ) redirect("login/show_login");
    	if (!$this->uri->segment(3) ) redirect("iku");

		$data['judul']  = 'Edit Existing IKU';
		$data['rows']  = $this->m_iku->get_rows( $this->uri->segment(3));
		$data['menu']  = $this->dsw_model->get_menu( $this->uri->segment(1) );
		$data['USER']  = $this->m_iku->user(  );
		$data['PERSPEKTIF']  = $this->m_iku->master( 'PERSPEKTIF' );
		$data['SASARAN_STRATEGIS']  = $this->m_iku->master( 'SASARAN_STRATEGIS' );
		$data['IKUMONITORING']  = $this->m_iku->master( 'IKUMONITORING' );
		$data['IKUATASAN']  = $this->m_iku->ikuId();
		$data['SATUAN_PENGUKURAN']  = $this->m_iku->master( 'SATUAN_PENGUKURAN' );
		$data['JENIS_ASPEK_TARGET']  = $this->m_iku->master( 'JENIS_ASPEK_TARGET' );
		$data['TINGKAT_KENDALI_IKU']  = $this->m_iku->master( 'TINGKAT_KENDALI_IKU' );
		$data['TINGKAT_VALIDITAS_IKU']  = $this->m_iku->master( 'TINGKAT_VALIDITAS_IKU' );
		$data['JENIS_CASCADING_IKU']  = $this->m_iku->master( 'JENIS_CASCADING_IKU' );
		$data['METODE_CASCADING']  = $this->m_iku->master( 'METODE_CASCADING' );
		$data['JENIS_KONSOLIDASI_PERIODE']  = $this->m_iku->master( 'JENIS_KONSOLIDASI_PERIODE' );
		$data['JENIS_KONSOLIDASI_LOKASI']  = $this->m_iku->master( 'JENIS_KONSOLIDASI_LOKASI' );
		$data['POLARISASI_INDIKATOR_KINERJA']  = $this->m_iku->master( 'POLARISASI_INDIKATOR_KINERJA' );
		$data['PERIODE_PELAPORAN']  = $this->m_iku->master( 'PERIODE_PELAPORAN' );

		$table = '';
		foreach($data['rows']->result() as $baris){ $periode = $baris->periodePelaporanId; }
		$getPeriode = $this->m_iku->getPeriode($periode);
		foreach($getPeriode->result() as $period){
			$rentang = $period->value_2;
			$alias = $period->value_3;
		}

		$data['total_column']  = $this->m_iku->total_tahun_periode_pelaporan( $this->uri->segment(3), $periode );

		$rows = $this->m_iku->periode_pelaporan($this->uri->segment(3), $periode);


				if($rows->num_rows() > 0){
					foreach ($rows->result() as $key2 => $value2) {
						$arr[$value2->tahun][$value2->sequence][$value2->tipe] = $value2->VALUE;
					}
					//echo count($arr); echo '<pre>'; print_r($arr);echo '</pre>';
					$table  .= '<div class="fadeIn col-sm-11"><table class="table table-hover table-condensed table-bordered table-striped">';
					$table .= '<thead><tr>
									<th width="40%" rowspan="2">
										<b>Periode</b>
									</th>';
					$tahun_no = 1;
					foreach($arr as $key => $tahun){
					$table .= '		<th colspan="2" width="20%">
										<input class="form-control tahun-table" type="number" placeholder="TAHUN" name="tahun-'.$tahun_no.'" value="'.$key.'"/>
									</th>';
					$tahun_no = $tahun_no + 1;
					}
					$table .= '</tr>';
					$table .= '<tr>';
					foreach($arr as $key => $tahun){
					$table .= '		<th>
										<b>Target</b>
									</th>
									<th>
										<b>Realisasi</b>
									</th>';
					}
					$table .= '</tr></thead>';
					$table .= '<tbody>';

					$totalTarget = 0;
					$totalRealisasi = 0;


					for ($i=1; $i <= $rentang; $i++) {
						if($rows->num_rows() == 0){
						$table .= '<tr>
										<td style="text-align:center;">
											<b>'.$alias.' '.$i.'</b>
										</td>
										<td style="text-align:center;">
											<i>Data Belum Diisi</i>
										</td>
										<td style="text-align:center;">
											<i>Data Belum Diisi</i>
										</td>
								   </tr>';
						}else{
						$table .= '<tr>
										<td style="text-align:center;">
											<b>'.$alias.' '.$i.'</b>
										</td>';
						$seq_no = 1;
						foreach($arr as $key => $tahun){
						$table .= '		<td style="text-align:center;">
											<b><input class="form-control data-tahun-T" name="data-tahun-T-'.$i.'-'.$seq_no.'" value="'.$arr[$key][$i]['TARGET'].'" id="data-tahun-T-'.$i.'-'.$seq_no.'" /></b>
										</th>
										<td style="text-align:center;">
											<b><input class="form-control data-tahun-R" name="data-tahun-R-'.$i.'-'.$seq_no.'" value="'.$arr[$key][$i]['REALISASI'].'" id="data-tahun-R-'.$i.'-'.$seq_no.'" /></b>
										</th>';
										$seq_no = $seq_no  + 1;
						}
						$table .= '</tr>';
					  //  $totalTarget = $totalTarget + $arr[$row->alias][$i]['TARGET'];
					   // $totalRealisasi = $totalRealisasi + $arr[$row->alias][$i]['REALISASI'];
						}
					}
						$table .= '</tbody>';
					$table .= '<thead><tr>
									<th width="40%" rowspan="2">
										<b>Periode</b>
									</th>';
					foreach($arr as $key => $tahun){
					$table .= '		<th colspan="2">
										<b>'.$key.'</b>
									</th>';
					}
					$table .= '</tr></thead>';

					   // $table .= '<th style="text-align:left;padding-left:15%;"><b>Tahunan</b></th><th style="text-align:center;">'.$totalTarget.'</th><th style="text-align:center;">'.$totalRealisasi.'</th></tr>';
					$table .= '</table></div>
					<div class="col-sm-1">
								<button class="btn btn-primary" type="button" id="add-year">ADD</button>
							</div>
							<script>
								$("#add-year").on("click", function(e) {
									$("#total-column").val(+$("#total-column").val() + +1);
									$.post("'.base_url().'iku/getYear?periodeId="+$(\'#periodePelaporanForm\').val()+"&tot="+$("#total-column").val(),{},function(obj){
									$(\'#periode_form\').html(obj);
									$("table").fadeIn();
									});
								});
								</script>';
					$notif = '';
				}else{
					$notif = '<div class="col-sm-12" style="text-align:center"><i>Periode Pelaporan Belum Diinput, Silahkan Input Terlebih Dahulu!</i></div>';
					}

            $data['table'] = $table;

		$data['bread'] = array('header'=>'Form Edit IKU', 'subheader'=>'Edit IKU');
		$data['view'] = "iku/V_form_edit";
		$this->load->view('main/utama', $data);
	}

	function form_delete() {
    	if (! $this->session->userdata('isLoggedIn') ) redirect("login/show_login");
		if($this->uri->segment(3) != ''){
			$this->m_iku->delete( $this->uri->segment(3));
		redirect("iku");
		}else{

		redirect("iku");}
	}

	function form_view() {
    	if (! $this->session->userdata('isLoggedIn') ) redirect("login/show_login");
		if($this->uri->segment(3) != ''){
			$id = $this->uri->segment(3);
			$rows = $this->m_iku->get_data_by_id($id);
	        foreach ($rows->result() as $key1 => $row) {
		        $table  = '<div style="padding:0px 50px 0px 50px"><table id="iku" class="table table-hover table-condensed table-striped">';
	            $table .= '<thead><tr><th width="40%"><b>Parameter</b></th><th width="70%"><b>Value</b></th></tr></thead>';
	            $table .= '<tbody><tr><td width="40%" style="text-align:right"><b>NIP</b></td><td width="70%">'.$row->nipUser.'</td></tr>';
	            $table .= '<tr><td width="40%" style="text-align:right"><b>Full Name</b></td><td width="70%">'.$row->fullname.'</td></tr>';
	            $table .= '<tr><td width="40%" style="text-align:right"><b>Perspektif</b></td><td width="70%">'.$row->perspektif.'</td></tr>';
	            $table .= '<tr><td width="40%" style="text-align:right"><b>Sasaran Strategis</b></td><td width="70%">'.$row->sasaran_strategis.'</td></tr>';
	            $table .= '<tr><td width="40%" style="text-align:right"><b>Desc Sasaran Strategis</b></td><td width="70%">'.$row->descSasaranStrategis.'</td></tr>';
	            $table .= '<tr><td width="40%" style="text-align:right"><b>IKU</b></td><td width="70%">'.$row->iku.'</td></tr>';
	            $table .= '<tr><td width="40%" style="text-align:right"><b>Definisi</b></td><td width="70%">'.$row->definisi.'</td></tr>';
	            $table .= '<tr><td width="40%" style="text-align:right"><b>Formula</b></td><td width="70%">'.$row->formula.'</td></tr>';
	            $table .= '<tr><td width="40%" style="text-align:right"><b>Tujuan</b></td><td width="70%">'.$row->tujuan.'</td></tr>';
	            $table .= '<tr><td width="40%" style="text-align:right"><b>Satuan Pengukuran</b></td><td width="70%">'.$row->satuan_pengukuran.'</td></tr>';
	            $table .= '<tr><td width="40%" style="text-align:right"><b>Jenis Aspek Target</b></td><td width="70%">'.$row->jenis_aspek_target.'</td></tr>';
	            $table .= '<tr><td width="40%" style="text-align:right"><b>Tingkat Kendali IKU</b></td><td width="70%">'.$row->tingkat_kendali_iku.'</td></tr>';
	            $table .= '<tr><td width="40%" style="text-align:right"><b>Tingkat Validitas IKU</b></td><td width="70%">'.$row->tingkat_validitas_iku.'</td></tr>';
	            $table .= '<tr><td width="40%" style="text-align:right"><b>Pihak Penanggung Jawab IKU</b></td><td width="70%">'.$row->pihakPJIku.'</td></tr>';
	            $table .= '<tr><td width="40%" style="text-align:right"><b>Pihak Penyedia Jasa</b></td><td width="70%">'.$row->pihakPenyediaJasa.'</td></tr>';
	            $table .= '<tr><td width="40%" style="text-align:right"><b>Sumber Data</b></td><td width="70%">'.$row->sumberData.'</td></tr>';
	            $table .= '<tr><td width="40%" style="text-align:right"><b>Jenis Cascading IKU</b></td><td width="70%">'.$row->jenis_cascading_iku.'</td></tr>';
	            $table .= '<tr><td width="40%" style="text-align:right"><b>Metode Cascading</b></td><td width="70%">'.$row->metode_cascading.'</td></tr>';
	            $table .= '<tr><td width="40%" style="text-align:right"><b>Jenis Konsolidasi Periode</b></td><td width="70%">'.$row->jenis_konsolidasi_periode.'</td></tr>';
	            $table .= '<tr><td width="40%" style="text-align:right"><b>Jenis Konsolidasi Lokasi</b></td><td width="70%">'.$row->jenis_konsolidasi_lokasi.'</td></tr>';
	            $table .= '<tr><td width="40%" style="text-align:right"><b>Polarisasi Indikator Kinerja</b></td><td width="70%">'.$row->polarisasi_indikator_kinerja.'</td></tr>';
	            $table .= '<tr><td width="40%" style="text-align:right"><b>Periode Pelaporan</b></td><td width="70%">'.$row->periode_pelaporan.'</td></tr>';
	            $table .= '<tr><td width="40%" style="text-align:right"><b>Konversi 120</b></td><td width="70%">'.$row->konversi120.'</td></tr>';
	            $table .= '<tr><td width="40%" style="text-align:right"><b>File Document</b></td><td width="70%">'.$row->file_document.'</td></tr>';
	            $table .= '<tr><td width="40%" style="text-align:right"><b>Tanggal Dibuat</b></td><td width="70%">'.$row->createdTimeStamp.'</td></tr>';
	            $table .= '<tr><td width="40%" style="text-align:right"><b>Dibuat Oleh</b></td><td width="70%">'.$row->createdBy.'</td></tr>';
	            $table .= '<tr><td width="40%" style="text-align:right"><b>Tanggal Diubah</b></td><td width="70%">'.$row->updatedTimeStamp.'</td></tr>';
	            $table .= '<tr><td width="40%" style="text-align:right"><b>Diubah Oleh</b></td><td width="70%">'.$row->updatedBy.'</td></tr></tbody>';
	            $table .= '</table></div></br>';

				$rows = $this->m_iku->periode_pelaporan($id, $row->periodePelaporanId);
				if($rows->num_rows() > 0){
					foreach ($rows->result() as $key2 => $value2) {
						$arr[$value2->tahun][$value2->sequence][$value2->tipe] = $value2->VALUE;
					}
					//echo count($arr); echo '<pre>'; print_r($arr);echo '</pre>';
					$table  .= '<div class="col-sm-12"><label>Periode Pelaporan :</label><table id="periode_pelaporan" class="table table-hover table-striped table-condensed table-bordered">';
					$table .= '<thead><tr>
									<th width="40%" rowspan="2">
										<b>Periode</b>
									</th>';
					foreach($arr as $key => $tahun){
					$table .= '		<th colspan="2">
										<b>'.$key.'</b>
									</th>';
					}
					$table .= '</tr>';
					$table .= '<tr>';
					foreach($arr as $key => $tahun){
					$table .= '		<th>
										<b>Target</b>
									</th>
									<th>
										<b>Realisasi</b>
									</th>';
					}
					$table .= '</tr></thead>';
					$table .= '<tbody>';

					$totalTarget = 0;
					$totalRealisasi = 0;

					for ($i=1; $i <= $row->rentang; $i++) {
						if($rows->num_rows() == 0){
						$table .= '<tr>
										<td style="text-align:center;">
											<b>'.$row->alias.' '.$i.'</b>
										</td>
										<td style="text-align:center;">
											<i>Data Belum Diisi</i>
										</td>
										<td style="text-align:center;">
											<i>Data Belum Diisi</i>
										</td>
								   </tr>';
						}else{
						$table .= '<tr>
										<td style="text-align:center;">
											<b>'.$row->alias.' '.$i.'</b>
										</td>';
						foreach($arr as $key => $tahun){
						$table .= '		<td style="text-align:center;">
											<b>'.$arr[$key][$i]['TARGET'].'</b>
										</th>
										<td style="text-align:center;">
											<b>'.$arr[$key][$i]['REALISASI'].'</b>
										</th>';
						}
						$table .= '</tr>';
					  //  $totalTarget = $totalTarget + $arr[$row->alias][$i]['TARGET'];
					   // $totalRealisasi = $totalRealisasi + $arr[$row->alias][$i]['REALISASI'];
						}
					}
						$table .= '</tbody>';
					$table .= '<thead><tr>
									<th width="40%" rowspan="2">
										<b>Periode</b>
									</th>';
					foreach($arr as $key => $tahun){
					$table .= '		<th colspan="2">
										<b>'.$key.'</b>
									</th>';
					}
					$table .= '</tr></thead>';

					   // $table .= '<th style="text-align:left;padding-left:15%;"><b>Tahunan</b></th><th style="text-align:center;">'.$totalTarget.'</th><th style="text-align:center;">'.$totalRealisasi.'</th></tr>';
					$table .= '</table></div>';
					$notif = '';
				}else{
					$notif = '<div class="col-sm-12" style="text-align:center"><i>Periode Pelaporan Belum Diinput, Silahkan Input Terlebih Dahulu!</i></div>';
					}
	        }
		$data['judul']  = 'View IKU';
		$data['menu']  = $this->dsw_model->get_menu( $this->uri->segment(1) );
        $data['table'] = $table;
		$data['bread'] = array('header'=>'Form View IKU', 'subheader'=>'View IKU');
		$data['view'] = "iku/V_form_view";
		$this->load->view('main/utama', $data);
        }
	}




	function lookup(){
     // memproses hasil pengetikan keyword pada form
     $keyword = $this->input->post('term');
     $type = $this->input->post('type');
     $data['response'] = 'false'; //mengatur default response
     $query = $this->m_iku->seek($keyword['term'], $type); //memanggil fungsi pencarian pada model

     if(! empty($query) ) {
          $data['response'] = 'true'; //mengatur response
          $data['message'] = array(); //membuat array
          foreach( $query as $row ){
           $data['message'][] = array('label' => $row->data, 'value' => $row->data); //mengisi array dengan record yang diperoleh
          }

     }
 		echo json_encode($data); //mencetak json jika merupakan permintaan ajax
     }

	function download_file(){
		 $this->load->helper('download'); //jika sudah diaktifkan di autoload, maka tidak perlu di tulis kembali
		 $data = file_get_contents('files/file_document_iku/'.$this->input->get('file')); // letak file pada aplikasi kita
		 force_download($this->input->get('file'),$data);
     }




     //--------------------------------------------PERIODE PELAPORAN------------------------------------------------//
     //-------------------------------------------------------------------------------------------------------------//



	function body_modal_view(){
		if($this->input->post('iku') != ''){
			$id = $this->input->post('iku');
			$rows = $this->m_iku->get_data_by_id($id);
	        foreach ($rows->result() as $key1 => $row) {
		        $table  = '<div style="padding:0px 50px 0px 50px"><table id="iku" class="table table-hover table-condensed table-striped">';
	            $table .= '<thead><tr><th width="40%"><b>Parameter</b></th><th width="70%"><b>Value</b></th></tr></thead>';
	            $table .= '<tbody><tr><td width="40%" style="text-align:right"><b>NIP</b></td><td width="70%">'.$row->nipUser.'</td></tr>';
	            $table .= '<tr><td width="40%" style="text-align:right"><b>Full Name</b></td><td width="70%">'.$row->fullname.'</td></tr>';
	            $table .= '<tr><td width="40%" style="text-align:right"><b>Perspektif</b></td><td width="70%">'.$row->perspektif.'</td></tr>';
	            $table .= '<tr><td width="40%" style="text-align:right"><b>Sasaran Strategis</b></td><td width="70%">'.$row->sasaran_strategis.'</td></tr>';
	            $table .= '<tr><td width="40%" style="text-align:right"><b>Desc Sasaran Strategis</b></td><td width="70%">'.$row->descSasaranStrategis.'</td></tr>';
	            $table .= '<tr><td width="40%" style="text-align:right"><b>IKU</b></td><td width="70%">'.$row->iku.'</td></tr>';
	            $table .= '<tr><td width="40%" style="text-align:right"><b>Definisi</b></td><td width="70%">'.$row->definisi.'</td></tr>';
	            $table .= '<tr><td width="40%" style="text-align:right"><b>Formula</b></td><td width="70%">'.$row->formula.'</td></tr>';
	            $table .= '<tr><td width="40%" style="text-align:right"><b>Tujuan</b></td><td width="70%">'.$row->tujuan.'</td></tr>';
	            $table .= '<tr><td width="40%" style="text-align:right"><b>Satuan Pengukuran</b></td><td width="70%">'.$row->satuan_pengukuran.'</td></tr>';
	            $table .= '<tr><td width="40%" style="text-align:right"><b>Jenis Aspek Target</b></td><td width="70%">'.$row->jenis_aspek_target.'</td></tr>';
	            $table .= '<tr><td width="40%" style="text-align:right"><b>Tingkat Kendali IKU</b></td><td width="70%">'.$row->tingkat_kendali_iku.'</td></tr>';
	            $table .= '<tr><td width="40%" style="text-align:right"><b>Tingkat Validitas IKU</b></td><td width="70%">'.$row->tingkat_validitas_iku.'</td></tr>';
	            $table .= '<tr><td width="40%" style="text-align:right"><b>Pihak Penanggung Jawab IKU</b></td><td width="70%">'.$row->pihakPJIku.'</td></tr>';
	            $table .= '<tr><td width="40%" style="text-align:right"><b>Pihak Penyedia Jasa</b></td><td width="70%">'.$row->pihakPenyediaJasa.'</td></tr>';
	            $table .= '<tr><td width="40%" style="text-align:right"><b>Sumber Data</b></td><td width="70%">'.$row->sumberData.'</td></tr>';
	            $table .= '<tr><td width="40%" style="text-align:right"><b>Jenis Cascading IKU</b></td><td width="70%">'.$row->jenis_cascading_iku.'</td></tr>';
	            $table .= '<tr><td width="40%" style="text-align:right"><b>Metode Cascading</b></td><td width="70%">'.$row->metode_cascading.'</td></tr>';
	            $table .= '<tr><td width="40%" style="text-align:right"><b>Jenis Konsolidasi Periode</b></td><td width="70%">'.$row->jenis_konsolidasi_periode.'</td></tr>';
	            $table .= '<tr><td width="40%" style="text-align:right"><b>Jenis Konsolidasi Lokasi</b></td><td width="70%">'.$row->jenis_konsolidasi_lokasi.'</td></tr>';
	            $table .= '<tr><td width="40%" style="text-align:right"><b>Polarisasi Indikator Kinerja</b></td><td width="70%">'.$row->polarisasi_indikator_kinerja.'</td></tr>';
	            $table .= '<tr><td width="40%" style="text-align:right"><b>Periode Pelaporan</b></td><td width="70%">'.$row->periode_pelaporan.'</td></tr>';
	            $table .= '<tr><td width="40%" style="text-align:right"><b>Konversi 120</b></td><td width="70%">'.$row->konversi120.'</td></tr>';
	            $table .= '<tr><td width="40%" style="text-align:right"><b>File Document</b></td><td width="70%">'.$row->file_document.'</td></tr>';
	            $table .= '<tr><td width="40%" style="text-align:right"><b>Tanggal Dibuat</b></td><td width="70%">'.$row->createdTimeStamp.'</td></tr>';
	            $table .= '<tr><td width="40%" style="text-align:right"><b>Dibuat Oleh</b></td><td width="70%">'.$row->createdBy.'</td></tr>';
	            $table .= '<tr><td width="40%" style="text-align:right"><b>Tanggal Diubah</b></td><td width="70%">'.$row->updatedTimeStamp.'</td></tr>';
	            $table .= '<tr><td width="40%" style="text-align:right"><b>Diubah Oleh</b></td><td width="70%">'.$row->updatedBy.'</td></tr></tbody>';
	            $table .= '</table></div></br>';

				$rows = $this->m_iku->periode_pelaporan($id, $row->periodePelaporanId);
				if($rows->num_rows() > 0){
					foreach ($rows->result() as $key2 => $value2) {
						$arr[$value2->tahun][$value2->sequence][$value2->tipe] = $value2->VALUE;
					}
					//echo count($arr); echo '<pre>'; print_r($arr);echo '</pre>';
					$table  .= '<div class="col-sm-12"><label>Periode Pelaporan :</label><table id="periode_pelaporan" class="table table-hover table-striped table-condensed table-bordered">';
					$table .= '<thead><tr>
									<th width="40%" rowspan="2">
										<b>Periode</b>
									</th>';
					foreach($arr as $key => $tahun){
					$table .= '		<th colspan="2">
										<b>'.$key.'</b>
									</th>';
					}
					$table .= '</tr>';
					$table .= '<tr>';
					foreach($arr as $key => $tahun){
					$table .= '		<th>
										<b>Target</b>
									</th>
									<th>
										<b>Realisasi</b>
									</th>';
					}
					$table .= '</tr></thead>';
					$table .= '<tbody>';

					$totalTarget = 0;
					$totalRealisasi = 0;

					for ($i=1; $i <= $row->rentang; $i++) {
						if($rows->num_rows() == 0){
						$table .= '<tr>
										<td style="text-align:center;">
											<b>'.$row->alias.' '.$i.'</b>
										</td>
										<td style="text-align:center;">
											<i>Data Belum Diisi</i>
										</td>
										<td style="text-align:center;">
											<i>Data Belum Diisi</i>
										</td>
								   </tr>';
						}else{
						$table .= '<tr>
										<td style="text-align:center;">
											<b>'.$row->alias.' '.$i.'</b>
										</td>';
						foreach($arr as $key => $tahun){
						$table .= '		<td style="text-align:center;">
											<b>'.$arr[$key][$i]['TARGET'].'</b>
										</th>
										<td style="text-align:center;">
											<b>'.$arr[$key][$i]['REALISASI'].'</b>
										</th>';
						}
						$table .= '</tr>';
					  //  $totalTarget = $totalTarget + $arr[$row->alias][$i]['TARGET'];
					   // $totalRealisasi = $totalRealisasi + $arr[$row->alias][$i]['REALISASI'];
						}
					}
						$table .= '</tbody>';
					$table .= '<thead><tr>
									<th width="40%" rowspan="2">
										<b>Periode</b>
									</th>';
					foreach($arr as $key => $tahun){
					$table .= '		<th colspan="2">
										<b>'.$key.'</b>
									</th>';
					}
					$table .= '</tr></thead>';

					   // $table .= '<th style="text-align:left;padding-left:15%;"><b>Tahunan</b></th><th style="text-align:center;">'.$totalTarget.'</th><th style="text-align:center;">'.$totalRealisasi.'</th></tr>';
					$table .= '</table></div>';
					$notif = '';
				}else{
					$notif = '<div class="col-sm-12" style="text-align:center"><i>Periode Pelaporan Belum Diinput, Silahkan Input Terlebih Dahulu!</i></div>';
					}
	        }
            echo $table;
            echo $notif;
        }
     }

	function body_modal_entry(){
		if($this->input->post('id') != ''){
		$rentangs = $this->m_iku->rentang_periode($this->input->post('id'));
	        foreach ($rentangs->result() as $key1 => $value1) {
				$rows = $this->m_iku->periode_pelaporan($this->input->post('id'), $this->input->post('tahun'));
				foreach ($rows->result() as $key2 => $value2) {
					$arr[$value1->alias][$value2->sequence][$value2->tipe] = $value2->VALUE;
				}
		//		print_r($rows);
		        $table   = '<input id="total_rows" value="'.$value1->rentang.'" type="hidden" />';
		        $table  .= '<input id="periodePelaporanId" value="'.$value1->periodePelaporanId.'" type="hidden" />';
		        $table  .= '<table id="periode_pelaporan" class="table table-hover table-striped table-condensed table-bordered">';
	            $table .= '<tr><th width="40%"><h4><b>Periode</b></h4></th><th width="40%"><h4><b>Target</b></h4></th><th width="40%"><h4><b>Realisasi</b></h4></th></tr>';
	        	for ($i=1; $i <= $value1->rentang; $i++) {
	        		if($rows->num_rows() == 0){
	                $table .= '<tr>
	                				<td style="text-align:left;padding-left:15%;"><b>'.$value1->alias.' '.$i.'</b></td>
	                				<td style="text-align:center;"><i><input id="periode-'.$i.'-target" value="" /></i></td>
	                				<td style="text-align:center;"><input id="periode-'.$i.'-realisasi" value="" /></td></tr>';
	        		}else{
	                $table .= '<tr>
	                				<td style="text-align:left;padding-left:15%;"><b>'.$value1->alias.' '.$i.'</b></td>
	                				<td style="text-align:center;"><input id="periode-'.$i.'-target" value="'.$arr[$value1->alias][$i]['TARGET'].'" /></td>
	                				<td style="text-align:center;"><input id="periode-'.$i.'-realisasi" value="'.$arr[$value1->alias][$i]['REALISASI'].'" /></td>
	                			</tr>';
		        	}
		        }
	            $table .= '</table>';
	        }
            echo $table;
        }
     }

	function simpan_periode_pelaporan(){
		if($this->input->post('idIku') != '' and $this->input->post('periodePelaporanId') != '' and $this->input->post('tahun') != '' and $this->input->post('datas') != ''){
			$check = $this->m_iku->rentang_periode($this->input->post('idIku'));
			foreach ($check->result() as $key1 => $value1) {
				if($value1->periodePelaporanId == $this->input->post('periodePelaporanId')){
					$this->m_iku->save_periode_pelaporan($this->input->post('idIku'), $this->input->post('periodePelaporanId'), $this->input->post('tahun'), json_decode($this->input->post('datas')));
					echo 1;
				}else{
					echo 0;
				}
			}
        }
     }
	function getPeriode(){
		if(!empty($this->input->get('id'))){
			$id = $this->input->get('id');
			$check = $this->m_iku->getPeriode($id);
			foreach($check->result() as $row){
				$table  = '<div class="col-sm-11"><table class="table table-hover table-condensed table-bordered table-striped">';
				$table .= '<thead><tr><th>Periode</th></tr></thead><tbody>';
				for($x = 1; $x <= $row->value_2; $x++){
					$table .= '<tr><td>'.$row->value_3.' '.$x.'</td></tr>';
				}
				$table .= '</tbody></table></div>
							<div class="col-sm-1">
								<button class="btn btn-primary" type="button" id="add-year">ADD</button>
							</div>
							<script>
								$("#add-year").on("click", function(e) {
									$("#total-column").val(+$("#total-column").val() + +1);
									$.post("'.base_url().'iku/getYear?periodeId="+$(\'#periodePelaporanForm\').val()+"&tot="+$("#total-column").val(),{},function(obj){
									$(\'#periode_form\').html(obj);
									});
								});
								</script>';
			}
			print_r($table);
			return $table;
		}
	}/*$table .= '<tr style="background-color:lightgray;">
									<th width="40%" rowspan="2">
										<b>Periode</b>
									</th>';
					foreach($arr as $key => $tahun){
					$table .= '		<th colspan="2">
										<b>'.$key.'</b>
									</th>';
					}
					$table .= '</tr>';
					$table .= '<tr>';
					foreach($arr as $key => $tahun){
					$table .= '		<th  style="background-color:lightgray;">
										<b>Target</b>
									</th>
									<th  style="background-color:lightgray;">
										<b>Realisasi</b>
									</th>';
					}
					$table .= '</tr>';*/

	function getYear(){
		if(!empty($this->input->get('periodeId')) AND !empty($this->input->get('tot'))){
			$id = $this->input->get('periodeId');
			$tot = $this->input->get('tot');
			$check = $this->m_iku->getPeriode($id);
			foreach($check->result() as $row){
				$table  = '<div class="col-sm-11"><table class="table table-hover table-condensed table-bordered table-striped">';
				$table .= '<thead><tr><th rowspan="2" width="20px">Periode</th>';
				for($x=1;$x<=$tot;$x++){
					$table .= '<th colspan="2" width="20%"><input class="form-control tahun-table" type="number" placeholder="TAHUN" name="tahun-'.$x.'" /></th>';
				}
				$table .= '</tr><tr>';
				for($x=1;$x<=$tot;$x++){
					$table .= '<th>TARGET</th>';
					$table .= '<th>REALISASI</th>';
				}
				$table .= '</tr></thead><tbody>';
				for($x = 1; $x <= $row->value_2; $x++){
					$table .= '<tr><td>'.$row->value_3.' '.$x.'</td>';
					for($z=1;$z<=$tot;$z++){
						$table .= '<td><input class="form-control data-tahun-T" name="data-tahun-T-'.$x.'-'.$z.'" id="data-tahun-T-'.$x.'-'.$z.'" /></td>';
						$table .= '<td><input class="form-control data-tahun-R" name="data-tahun-R-'.$x.'-'.$z.'" id="data-tahun-R-'.$x.'-'.$z.'" /></td>';
					}
					$table .= '</tr>';
				}
				$table .= '</tbody></table></div>
							<div class="col-sm-1">
								<button class="btn btn-primary" type="button" id="add-year">ADD</button>
							</div>
							<script>
								$("#add-year").on("click", function(e) {
									$("#total-column").val(+$("#total-column").val() + +1);
									$.post("'.base_url().'iku/getYear?periodeId="+$(\'#periodePelaporanForm\').val()+"&tot="+$("#total-column").val(),{},function(obj){
									$(\'#periode_form\').html(obj);
									});
								});
								</script>';
			}
			die($table);
		}
	}

	function getAtasan(){
		if($this->input->get('id', true)){
			$var = $this->m_iku->getAtasan($this->input->get('id'))->result_array();
			print_r(json_encode($var[0]));
			}
		}

	function print_iku_select(){
		///if($this->input->post('id', true)){
		$id = 1;
		$resultall = '';
			$print_iku_select = $this->m_iku->print_iku_select($id);
			$x=0;
			foreach($print_iku_select->result() as $select){
				$result[$select->ss][$x]['iku'] = $select->iku;
				$result[$select->ss][$x]['id'] = $select->id;
				$x++;
			}

			$resultall .= '<div class="col-sm-12"><table class="table table-hover table-condensed table-striped"><tbody>';
			foreach($result as $key => $res){
				$resultall .= '<tr><td><input class="form_control col-sm-1" type="checkbox" name="'.$key.'"/><div class="col-sm-11"> '.$key.'</div></td></tr>';
				foreach($res as $key2 => $hasil){
					$resultall .= '<tr><td><div class="col-sm-1"></div><input class="col-sm-1" type="checkbox" name="check['.$hasil['id'].']" id="'.$hasil['id'].'"/><div class="col-sm-10"> '.$hasil['iku'].'</div></td></tr>';
				}
			}
			$resultall .= "</tbody></div>";
			print_r($resultall);
		//}
	}

	function form_print(){
        $this->load->library('TCPDF');
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);

        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);

        $pdf->AddPage();

		//-------------------------------------------GET DATA--------------------------------------//
		$data['rows']  = $this->m_iku->get_data_by_id( $this->uri->segment(3));
		$data['menu']  = $this->dsw_model->get_menu( $this->uri->segment(1) );
		$data['USER']  = $this->m_iku->user(  );
		$data['PERSPEKTIF']  = $this->m_iku->master( 'PERSPEKTIF' );
		$data['SASARAN_STRATEGIS']  = $this->m_iku->master( 'SASARAN_STRATEGIS' );
		$data['IKUMONITORING']  = $this->m_iku->master( 'IKUMONITORING' );
		$data['IKUATASAN']  = $this->m_iku->ikuId();
		$data['SATUAN_PENGUKURAN']  = $this->m_iku->master( 'SATUAN_PENGUKURAN' );
		$data['JENIS_ASPEK_TARGET']  = $this->m_iku->master( 'JENIS_ASPEK_TARGET' );
		$data['TINGKAT_KENDALI_IKU']  = $this->m_iku->master( 'TINGKAT_KENDALI_IKU' );
		$data['TINGKAT_VALIDITAS_IKU']  = $this->m_iku->master( 'TINGKAT_VALIDITAS_IKU' );
		$data['JENIS_CASCADING_IKU']  = $this->m_iku->master( 'JENIS_CASCADING_IKU' );
		$data['METODE_CASCADING']  = $this->m_iku->master( 'METODE_CASCADING' );
		$data['JENIS_KONSOLIDASI_PERIODE']  = $this->m_iku->master( 'JENIS_KONSOLIDASI_PERIODE' );
		$data['JENIS_KONSOLIDASI_LOKASI']  = $this->m_iku->master( 'JENIS_KONSOLIDASI_LOKASI' );
		$data['POLARISASI_INDIKATOR_KINERJA']  = $this->m_iku->master( 'POLARISASI_INDIKATOR_KINERJA' );
		$data['PERIODE_PELAPORAN']  = $this->m_iku->master( 'PERIODE_PELAPORAN' );

		$table = '';
		foreach($data['rows']->result() as $baris){ $periode = $baris->periodePelaporanId; }
		$getPeriode = $this->m_iku->getPeriode($periode);
		foreach($getPeriode->result() as $period){
			$rentang = $period->value_2;
			$alias = $period->value_3;
		}
		$data['total_column']  = $this->m_iku->total_tahun_periode_pelaporan( $this->uri->segment(3), $periode );
		$rows = $this->m_iku->periode_pelaporan($this->uri->segment(3), $periode);
				if($rows->num_rows() > 0){
					foreach ($rows->result() as $key2 => $value2) {
						$arr[$value2->tahun][$value2->sequence][$value2->tipe] = $value2->VALUE;
					}
					$table  .= '<div>
									<table border="1" cellpadding="2" cellspacing="2" >';
					$table .= '<thead>
								<tr style="background-color:#FFFF00;">
									<th rowspan="2" colspan="1" align="center" >
										<b>Periode</b>
									</th>';
					$tahun_no = 1;
					foreach($arr as $key => $tahun){
						$table .= '	<th colspan="2"  align="center" >
										'.$key.'
									</th>';
						$tahun_no = $tahun_no + 1;
					}
					$table .= '</tr>';

					$table .= '<tr>';
					foreach($arr as $key => $tahun){
					$table .= '		<th  align="center" style="background-color:#FFFF00;" >
										<b>Target</b>
									</th>
									<th  align="center" style="background-color:#FFFF00;" >
										<b>Realisasi</b>
									</th>';
					}
					$table .= '</tr></thead>';
					$table .= '<tbody>';
					$totalTarget = 0;
					$totalRealisasi = 0;

					for ($i=1; $i <= $rentang; $i++) {
						$table .= '<tr>
										<td style="text-align:center;background-color:#ffcc00;">
											<b>'.$alias.' '.$i.'</b>
										</td>';
						$seq_no = 1;
						foreach($arr as $key => $tahun){
						$table .= '		<td style="text-align:center;">
											<b>'.$arr[$key][$i]['TARGET'].'</b>
										</td>
										<td style="text-align:center;">
											<b>'.$arr[$key][$i]['REALISASI'].'</b>
										</td>';
										$seq_no = $seq_no  + 1;
						}
						$table .= '</tr>';
					}
						$table .= '</tbody>';

					$table .= '</table></div>';
					$notif = '';
				}else{
					$notif = '<div class="col-sm-12" style="text-align:center"><i>Periode Pelaporan Belum Diinput, Silahkan Input Terlebih Dahulu!</i></div>';
					}

            $data['table'] = $table;

			//-----------------------------------END GET DATA-------------------------------------------------//





        $html = $this->createIkuPersonal($data);

        $pdf->writeHTML($html, true, false, true, false, '');
        $pdf->Output('PDF' . '-' . 'Reference Letter' . '.pdf', 'I');
	}


    public function createIkuPersonal($data){

        $html= '
		 <p  style="text-align:left; line-height: 1px;  font-family:Cordia New; font-size: 12px; font-family:Cordia New;"  >
			<b>MANUAL INDIKATOR KINERJA UTAMA</b>
		 </p>
		 <p  style="text-align:left; line-height: 1px;  font-family:Cordia New; font-size: 11px; font-family:Cordia New;"  >
			<b>Direktur Jenderal Anggaran</b>
		 </p>
		 <p  style="text-align:left; line-height: 1px;  font-family:Cordia New; font-size: 11px; font-family:Cordia New;"  >
				<b>Menteri Keuangan</b>
		 </p>

				<hr></hr><div></div>
				<table>
					<tbody>
					';

		foreach($data['rows']->result() as $row){
		$html .= '		<tr>
							<td width="30%">
								<span style="line-height: 12px;font-family:Times New Roman; font-size: 9px;text-align:left;">
									<b>Perspektif</b>
								</span>
							</td>
							<td width="70%">
								<span style="line-height: 12px; font-family:Times New Roman; font-size: 9px;text-align:left;">'.$row->perspektif.'
								</span>
							</td>
						</tr>
						<tr>
							<td width="30%">
								<span style="line-height: 12px;font-family:Times New Roman; font-size: 9px;text-align:left;">
									<b>Sasaran Strategis</b>
								</span>
							</td>
							<td width="70%">
								<span style="line-height: 12px; font-family:Times New Roman; font-size: 9px;text-align:left;">'.$row->sasaran_strategis.'
								</span>
							</td>
						</tr>
						<tr>
							<td width="30%">
								<span style="line-height: 12px;font-family:Times New Roman; font-size: 9px;text-align:left;">
									<b>Deskripsi Sasaran Strategis</b>
								</span>
							</td>
							<td width="70%">
								<span style="line-height: 12px; font-family:Times New Roman; font-size: 9px;text-align:left;">'.$row->descSasaranStrategis.'
								</span>
							</td>
						</tr>
						<tr>
							<td width="30%">
								<span style="line-height: 12px;font-family:Times New Roman; font-size: 9px;text-align:left;">
									<b>Indikator Kinerja Utama</b>
								</span>
							</td>
							<td width="70%">
								<span style="line-height: 12px; font-family:Times New Roman; font-size: 9px;text-align:left;">'.$row->iku.'
								</span>
							</td>
						</tr>
						<tr>
							<td width="30%">
								<span style="line-height: 12px;font-family:Times New Roman; font-size: 9px;text-align:left;">
									<b>Deskripsi</b>
								</span>
							</td>
							<td width="70%">
									<table>
										<tr>
											<td style="border-bottom: 1px solid #CCC;" >
												<span style="line-height: 12px; font-family:Times New Roman; font-size: 9px;text-align:left;">
													<b>Definisi</b>
												</span>
											</td>
										</tr>
										<tr border="1">
											<td>
												<span style="line-height: 12px; font-family:Times New Roman; font-size: 9px;text-align:left;">
													'.$row->definisi.'
												</span>
											</td>
										</tr>
										<tr>
											<td style="border-bottom: 1px solid #CCC;" >
												<span style="line-height: 12px; font-family:Times New Roman; font-size: 9px;text-align:left;">
													<b>Formula</b>
												</span>
											</td>
										</tr>
										<tr border="1">
											<td>
												<span style="line-height: 12px; font-family:Times New Roman; font-size: 9px;text-align:left;">
													'.$row->formula.'
												</span>
											</td>
										</tr>
										<tr>
											<td style="border-bottom: 1px solid #CCC;" >
												<span style="line-height: 12px; font-family:Times New Roman; font-size: 9px;text-align:left;">
													<b>Tujuan</b>
												</span>
											</td>
										</tr>
										<tr border="1">
											<td>
												<span style="font-family:Times New Roman; font-size: 9px;text-align:left;">
													'.$row->tujuan.'
												</span>
											</td>
										</tr>
									</table>


							</td>
						</tr>
						<tr>
							<td width="30%">
								<span style="line-height: 12px;font-family:Times New Roman; font-size: 9px;text-align:left;">
									<b>Satuan Pengukuran</b>
								</span>
							</td>
							<td width="70%">
								<span style="line-height: 12px; font-family:Times New Roman; font-size: 9px;text-align:left;">'.$row->satuan_pengukuran.'
								</span>
							</td>
						</tr>
						<tr>
							<td width="30%">
								<span style="line-height: 12px;font-family:Times New Roman; font-size: 9px;text-align:left;">
									<b>Jenis Aspek Target</b>
								</span>
							</td>
								<td>
										<table >
											<tr>';
				foreach($data['JENIS_ASPEK_TARGET']->result() as $param){
					$html .= '						<td style="font-size: 8px;text-align:left;">(';
					if($row->jenis_aspek_target == $param->name){
						$html .= ' <b>X</b> ';
					}else{
						$html .= ' ';
					}
					$html .= ') '
													.$param->name.'
												</td>';
				}
				$html .= '					</tr>
										</table>
								</td>
						</tr>
						<tr>
							<td width="30%">
								<span style="line-height: 12px;font-family:Times New Roman; font-size: 9px;text-align:left;">
									<b>Tingkat Kendali IKU</b>
								</span>
							</td>
							<td width="70%">
									<table>
										<tr>';
			foreach($data['TINGKAT_KENDALI_IKU']->result() as $param){
				$html .= '						<td style="font-size: 8px;text-align:left;">(';
				if($row->tingkat_kendali_iku == $param->name){
					$html .= ' <b>X</b> ';
				}else{
					$html .= ' ';
				}
				$html .= ') '
												.$param->name.'
											</td>';
			}
			$html .= '					</tr>
									</table>
							</td>
						</tr>
						<tr>
							<td width="30%">
								<span style="line-height: 12px;font-family:Times New Roman; font-size: 9px;text-align:left;">
									<b>Tingkat Validitas IKU</b>
								</span>
							</td>
							<td width="70%">
									<table>
										<tr>';
			foreach($data['TINGKAT_VALIDITAS_IKU']->result() as $param){
				$html .= '						<td style="font-size: 8px;text-align:left;">(';
				if($row->tingkat_validitas_iku == $param->name){
					$html .= ' <b>X</b> ';
				}else{
					$html .= ' ';
				}
				$html .= ') '
												.$param->name.'
											</td>';
			}
			$html .= '					</tr>
									</table>
							</td>
						</tr>
						<tr>
							<td width="30%">
								<span style="line-height: 12px;font-family:Times New Roman; font-size: 9px;text-align:left;">
									<b>Unit/Pihak Penanggung Jawab IKU</b>
								</span>
							</td>
							<td width="70%">
								<span style="line-height: 12px; font-family:Times New Roman; font-size: 9px;text-align:left;">'.$row->pihakPJIku.'
								</span>
							</td>
						</tr>
						<tr>
							<td width="30%">
								<span style="line-height: 12px;font-family:Times New Roman; font-size: 9px;text-align:left;">
									<b>Unit/Pihak Penyedia Data</b>
								</span>
							</td>
							<td width="70%">
								<span style="line-height: 12px; font-family:Times New Roman; font-size: 9px;text-align:left;">'.$row->pihakPenyediaJasa.'
								</span>
							</td>
						</tr>
						<tr>
							<td width="30%">
								<span style="line-height: 12px;font-family:Times New Roman; font-size: 9px;text-align:left;">
									<b>Sumber Data</b>
								</span>
							</td>
							<td width="70%">
								<span style="line-height: 12px; font-family:Times New Roman; font-size: 9px;text-align:left;">'.$row->sumberData.'
								</span>
							</td>
						</tr>
						<tr>
							<td width="30%">
								<span style="line-height: 12px;font-family:Times New Roman; font-size: 9px;text-align:left;">
									<b>Jenis Cascading IKU</b>
								</span>
							</td>
							<td width="70%">
									<table>
										<tr>';
			foreach($data['JENIS_CASCADING_IKU']->result() as $param){
				$html .= '						<td style="font-size: 8px;text-align:left;">(';
				if($row->jenis_cascading_iku == $param->name){
					$html .= ' <b>X</b> ';
				}else{
					$html .= ' ';
				}
				$html .= ') '
												.$param->name.'
											</td>';
			}
			$html .= '					</tr>
									</table>
							</td>
						</tr>
						<tr>
							<td width="30%">
								<span style="line-height: 12px;font-family:Times New Roman; font-size: 9px;text-align:left;">
									<b>Metode Cascading</b>
								</span>
							</td>
							<td width="70%">
									<table>
										<tr>';
			foreach($data['METODE_CASCADING']->result() as $param){
				$html .= '						<td style="font-size: 8px;text-align:left;">(';
				if($row->metode_cascading == $param->name){
					$html .= ' <b>X</b> ';
				}else{
					$html .= ' ';
				}
				$html .= ') '
												.$param->name.'
											</td>';
			}
			$html .= '					</tr>
									</table>
							</td>
						</tr>
						<tr>
							<td width="30%">
								<span style="line-height: 12px;font-family:Times New Roman; font-size: 9px;text-align:left;">
									<b>Jenis Konsolidasi Periode </b>
								</span>
							</td>
							<td width="70%">
									<table>
										<tr>';
			foreach($data['JENIS_KONSOLIDASI_PERIODE']->result() as $param){
				$html .= '						<td style="font-size: 8px;text-align:left;">(';
				if($row->jenis_konsolidasi_periode == $param->name){
					$html .= ' <b>X</b> ';
				}else{
					$html .= ' ';
				}
				$html .= ') '
												.$param->name.'
											</td>';
			}
			$html .= '					</tr>
									</table>
							</td>
						</tr>
						<tr>
							<td width="30%">
								<span style="line-height: 12px;font-family:Times New Roman; font-size: 9px;text-align:left;">
									<b>Jenis Konsolidasi Lokasi</b>
								</span>
							</td>
							<td width="70%">
									<table>
										<tr>';
			foreach($data['JENIS_KONSOLIDASI_LOKASI']->result() as $param){
				$html .= '						<td style="font-size: 8px;text-align:left;">(';
				if($row->jenis_konsolidasi_lokasi == $param->name){
					$html .= ' <b>X</b> ';
				}else{
					$html .= ' ';
				}
				$html .= ') '
												.$param->name.'
											</td>';
			}
			$html .= '					</tr>
									</table>
							</td>
						</tr>
						<tr>
							<td width="30%">
								<span style="line-height: 12px;font-family:Times New Roman; font-size: 9px;text-align:left;">
									<b>Polarisasi Indikator Kinerja</b>
								</span>
							</td>
							<td width="70%">
									<table>
										<tr>';
			foreach($data['POLARISASI_INDIKATOR_KINERJA']->result() as $param){
				$html .= '						<td style="font-size: 8px;text-align:left;">(';
				if($row->polarisasi_indikator_kinerja == $param->name){
					$html .= ' <b>X</b> ';
				}else{
					$html .= ' ';
				}
				$html .= ') '
												.$param->name.'
											</td>';
			}
			$html .= '					</tr>
									</table>
							</td>
						</tr>
						<tr>
							<td width="30%">
								<span style="line-height: 12px;font-family:Times New Roman; font-size: 9px;text-align:left;">
									<b>Periode Pelaporan</b>
								</span>
							</td>
							<td width="70%">
									<table>
										<tr>';
			foreach($data['PERIODE_PELAPORAN']->result() as $param){
				$html .= '						<td style="font-size: 8px;text-align:left;">(';
				if($row->periode_pelaporan == $param->name){
					$html .= ' <b>X</b> ';
				}else{
					$html .= ' ';
				}
				$html .= ') '
												.$param->name.'
											</td>';
			}
			$html .= '					</tr>
									</table>
							</td>
						</tr>
						<tr>
							<td width="30%">
								<span style="line-height: 12px;font-family:Times New Roman; font-size: 9px;text-align:left;">
									<b>Konversi 120</b>
								</span>
							</td>
							<td width="70%">
									<table>
										<tr>';

				$html .= '						<td style="font-size: 8px;text-align:left;">(';
				if($row->konversi120 == 'Y'){
					$html .= ' <b>X</b> ';
				}else{
					$html .= ' ';
				}
				$html .= ') Y
											</td>';

				$html .= '						<td style="font-size: 8px;text-align:left;">(';
				if($row->konversi120 == 'N'){
					$html .= ' <b>X</b> ';
				}else{
					$html .= ' ';
				}
				$html .= ') N
											</td>';

			$html .= '					</tr>
									</table>
							</td>
						</tr>
					';
		}

				$html .= '</tbody>
				</table>
			<span style="font-family:Times New Roman; font-size: 9px;text-align:left;">
									<b>Tabel Data :</b>
								</span>';

		$html .= $data['table'];



        return $html;

	}

    public function createFile(){
        $html= '
			<span style="font-family:Times New Roman; font-size: 11px;text-align:center;">
				<b>KONTRAK KINERJA</b></span>
			<div><span style="font-family:Times New Roman; font-size: 11px;text-align:center;">
				<b>NOMOR: 4/AG.113/2015</b></span></div>
			<span style="font-family:Times New Roman; font-size: 11px;text-align:center;">
				<b>PELAKSANA PADA SUBBAGIAN PELAPORAN DAN</b></span>
			<div><span style="font-family:Times New Roman; font-size: 11px;text-align:center;">
				<b>LAYANAN INFORMASI</b></span></div>
			<span style="font-family:Times New Roman; font-size: 11px;text-align:center;">
				<b>SEKRETARIAT DIREKTUR JENDERAL</b></span>
			<div><span style="font-family:Times New Roman; font-size: 11px;text-align:center;">
				<b>DIREKTORAT JENDRAL ANGGARAN</b></span></div>
			<span style="font-family:Times New Roman; font-size: 11px;text-align:center;">
				<b>KEMENTRIAN KEUANGAN</b></span>
			<div><span style="font-family:Times New Roman; font-size: 11px;text-align:center;">
				<b>TAHUN 2015</b></span></div>
				<div></br></div>
				<div><span style="font-family:Times New Roman; font-size: 11px;text-align:center;"> <b>Pernyataan Kesanggupan</b></div>
				<div><span style="font-family:Times New Roman; font-size: 10px;text-align:justify;"> Dalam Melaksanakan tugas sebagai Pelaksana pada Subbagian Pelaporan dan Layanan Informasi saya akan:</div>
				<div><span style="font-family:Times New Roman; font-size: 10px;text-align:justify;"> 1. Melaksanakan tugas dan fungsi dengan penuh kesungguhan untuk mencapai target kinerja sebagaimana tercantum dalam Kontrak Kinerja ini.</span></div>
				<span style="font-family:Times New Roman; font-size: 10px;text-align:justify;"> 2. Bersedia untuk dilakukan evaluasi atas capaian kinerja kinerja kapanpun diperlukan.</span>
				<div><span style="font-family:Times New Roman; font-size: 10px;text-align:justify;"> 3. Menerima segala konsekuensi atas capaian kinerja sesuai dengan peraturan yang berlaku.</span></div>
        ';
        return $html;

	}
}
