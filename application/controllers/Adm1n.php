<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Adm1n extends CI_Controller {

	function __construct() {
		parent::__construct();
        $this->load->model('admin_model');
	}

	public function index() {
        if (! strpos($this->session->userdata('idusergroup'), '001') ) site_url("login/logout_user");
		$this->fc->log('Admin User');
		$this->user_group();
	}

	public function user_group() {
		$data['group'] = $this->admin_model->get_user_group();

		// $arr = $data['group'];
		// $this->fc->browse( $arr['001']['menu'] );

		$data['menu'] = $this->dsw_model->get_menu( $this->uri->segment(1) );
		$data['bread']= array('header'=>'User Group', 'subheader'=>'User Group');
		$data['view'] = "admin/V_user_group";
		$this->load->view('main/utama', $data);
	}


	public function test() {
	}
}
