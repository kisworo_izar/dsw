<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pustaka extends CI_Controller {
	function __construct() {
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
	    $this->load->model('Pustaka_model');
	}

	function index() {
        if (! $this->session->userdata('isLoggedIn') ) redirect("login/show_login");
		if (isset($_GET['q'])) $menu = $_GET['q'];
		else show_404();
		if ($menu == 'pu5t4') $this->pustaka();
		else show_404();
	}

	private function pustaka() {
		$this->fc->log('Perpustakaan');
		$data['buku_baru']  = $this->Pustaka_model->get_baru();
		$data['teraktif']   = $this->Pustaka_model->get_aktif();
		$data['popular']    = $this->Pustaka_model->get_popular();
		$data['teraktif_all']  = $this->Pustaka_model->get_aktif_all();

		$data['menu']  = $this->dsw_model->get_menu( $this->uri->segment(1) );
		$data['bread']= array('header'=>'&nbsp;', 'subheader'=>'Pustaka');
		$data['view'] = "pustaka/pustaka";
		$this->load->view('main/utama', $data);
	}
}
