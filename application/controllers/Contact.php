<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Contact extends CI_Controller {

	function __construct() {
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
	    $this->load->model('m_contact');
	}

	function index() {
		// require_once 'assets/plugins/htmlpurifier/HTMLPurifier.auto.php';
		// $config	  = HTMLPurifier_Config::createDefault();
		// $purifier = new HTMLPurifier($config);

		if (! $this->session->userdata('isLoggedIn') ) redirect("login/show_login");

		if (isset($_GET['q']))  $menu   = $_GET['q'];  else show_404();

		if 		(preg_match("~\bOBfTr\b~", $menu))	$this->grid();
		else if (preg_match("~\bmFnwz\b~", $menu))	$this->lps();
		else show_404();
	}

	private function grid() {
		$this->fc->log('Pegawai DJA');
		$data['tabel'] = $this->m_contact->get_user();
		$data['bread'] = array('view'=>'user/v_contact_grid', 'header'=>'&nbsp;', 'subheader'=>'Pegawai DJA');
		$this->load->view('main/utama', $data);
	}

	private function lps($iduser){
		$this->fc->log('SU - Reset Password');
		$data   = $this->m_contact->respass($iduser);
		redirect("contact?q=OBfTr");
	}

}
