<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Revisi_PA extends CI_Controller {

	private $dbrevisi;

	function __construct() {
		parent::__construct();
		$this->dbrevisi = $this->load->database('revisi'. $this->session->userdata('thang'), TRUE);
		date_default_timezone_set('Asia/Jakarta');
        $this->load->model('dsw_model');
        $this->load->model('Revisi_PA_model');
        $this->load->model('tracking_model');
	}

	public function index() {
        if (! $this->session->userdata('isLoggedIn') ) redirect("login/show_login");
		$this->dash();
	}

	public function dash( $depuni=null, $start=null, $end=null ) {
		$this->fc->log('Revisi PA');
		$month = date('m')-'1';
		$tahun 	  = date('y');
		if ($start==null) { $start = date('01-'.$month.'-'.$this->session->userdata('tahun')); $end = date('d-m-'.$this->session->userdata('tahun')); } else { $start = $this->uri->segment(3); $end = $this->uri->segment(4);  }
		$data = $this->Revisi_PA_model->get_data( $depuni, $this->fc->ustgl($start), $this->fc->ustgl($end) );

		$data['menu']  = $this->dsw_model->get_menu( $this->uri->segment(1) );
		$data['bread'] = array('header'=>'Monitoring Revisi Anggaran', 'subheader'=>'Revisi PA');
		$data['view']  = "revisi_pa/v_dash";
		$this->load->view('main/utama', $data);
	}

	public function form2( $rev_id ) {
		// $data = $this->revisi_23_model->get_row( $rev_id );

		$data['rev_id']= $rev_id;
		$data['menu']  = $this->dsw_model->get_menu( $this->uri->segment(1) );
		$data['bread'] = array('header'=>'&nbsp;', 'subheader'=>'Revisi Anggaran');
		$data['view']  = "revisi_pa/v_form2";
		$this->load->view('main/utama', $data);
	}

	function crud_form2() {
		//echo 'start';
		$rev_id 	= $_POST['rev_id'];
		$ip	   = $this->fc->getUserIP();

		$this->dbrevisi->query( "Update revisi set t2_status =1, rev_tahap = 2, t2_proses_tgl=current_timestamp(), t2_proses_ip='$ip'
							where rev_id='$rev_id' " );
		$this->form2_proses($rev_id);
	}

	public function form2_proses( $rev_id ) {
		//$ruh  = 'Rekam'; if ($this->uri->segment(3)=='u') $ruh = 'Ubah';
		// $data = $this->revisi_23_model->get_row( $ruh );
		$data['rev_id']= $rev_id;
		$data['menu'] = $this->dsw_model->get_menu( $this->uri->segment(1) );
		$data['bread'] = array('header'=>'&nbsp;', 'subheader'=>'Revisi Anggaran');
		$data['proses'] = $this->Revisi_PA_model->get_form2( $rev_id );
		$data['view'] = "revisi_pa/v_form2_proses";
		$this->load->view('main/utama', $data);
	}

	function crud_form2_proses() {
		$this->Revisi_PA_model->save_form2_proses();
		redirect("Revisi_PA/dash");
	}


	public function form6( $rev_id ) {
		// $data = $this->revisi_67_model->get_row( $rev_id );

		$data['rev_id']= $rev_id;
		$data['menu']  = $this->dsw_model->get_menu( $this->uri->segment(1) );
		$data['bread'] = array('header'=>'&nbsp;', 'subheader'=>'Revisi Anggaran');
		$data['view']  = "revisi_pa/v_form6";
		$this->load->view('main/utama', $data);
	}

	function crud_form6() {

		$rev_id 	= $_POST['rev_id'];
		//$rev_id     = $this->uri->segment(3);
		$ip	   = $this->fc->get_client_ip();

		$this->dbrevisi->query( "Update revisi set t6_status =1, rev_tahap = 6, t6_proses_tgl=current_timestamp(), t6_proses_ip='$ip'
							where rev_id='$rev_id' " );

		$this->form6_proses($rev_id);
	}

	public function form6_proses($rev_id) {
		$ruh  = 'Rekam'; if ($this->uri->segment(3)=='u') $ruh = 'Ubah';
		// $data = $this->revisi_67_model->get_row( $ruh );
		$data['rev_id']= $rev_id;
		$data['menu'] = $this->dsw_model->get_menu( $this->uri->segment(1) );
		$data['bread'] = array('header'=>'&nbsp;', 'subheader'=>'Revisi Anggaran');
		$data['proses'] = $this->Revisi_PA_model->get_form2( $rev_id );
		$data['view'] = "revisi_pa/v_form6_proses";
		$this->load->view('main/utama', $data);
	}

	function crud_form6_proses() {
		$this->Revisi_PA_model->save_form6_proses();
		redirect("Revisi_PA/dash");
	}

	public function form7( $rev_id ) {
		$data['rev_id']= $rev_id;
		$data['menu']  = $this->dsw_model->get_menu( $this->uri->segment(1) );
		$data['bread'] = array('header'=>'&nbsp;', 'subheader'=>'Revisi Anggaran');
		$data['view']  = "revisi_pa/v_form7";
		$this->load->view('main/utama', $data);
	}

	function crud_form7($rev_id) {
		$rev_id 		= $_POST['rev_id'];
		$ip	  			= $this->fc->get_client_ip();

		$this->dbrevisi->query( "Update revisi set t7_status =1, rev_tahap = 7, t7_proses_tgl=current_timestamp(), t7_proses_ip='$ip' where rev_id='$rev_id' " );
		redirect("Revisi_PA/form7_proses/$rev_id");
		//$this->form7_proses($rev_id);
	}

	public function form7_proses($rev_id) {
		$data['rev_id']= $rev_id;
		$data['menu'] = $this->dsw_model->get_menu( $this->uri->segment(1) );
		$data['bread'] = array('header'=>'&nbsp;', 'subheader'=>'Revisi Anggaran');
		$data['proses'] = $this->Revisi_PA_model->get_form2( $rev_id );
		$data['view'] = "revisi_pa/v_form7_proses_new";
		$this->load->view('main/utama', $data);
	}

	function crud_form7_proses($rev_id) {

		$rev_id			= $_POST['rev_id'];
		$this->Revisi_PA_model->save_form7_proses();
		redirect("Revisi_PA/dash");
		// $this->form7_proses($rev_id);
	}



	public function fileupload_form7() {
		$path = 'files/revisi_SatuDJA/' . $_POST['rev_id'];
		$old  = umask(0);
		is_dir($path) || mkdir($path, 0777, true);
		umask($old);

		$file = $_FILES[ $_POST['name'] ];
		$resp = array('uploaded' => 'Gagal');
		if ($file) {
			move_uploaded_file($file["tmp_name"], $path .'/'. $file['name']);
			$resp = array('uploaded' => $file['name']);
		}
		echo json_encode($resp);
 	}

	function inputund(){
 		$data['menu'] = $this->dsw_model->get_menu( $this->uri->segment(1) );
		$data['bread'] = array('header'=>'&nbsp;', 'subheader'=>'Revisi Anggaran');
		$data['view'] = "revisi/v_surat_pengesahan";
		$this->load->view('main/utama', $data);
 	}

 	function inputund_tolak(){
 		$data['menu'] = $this->dsw_model->get_menu( $this->uri->segment(1) );
		$data['bread'] = array('header'=>'&nbsp;', 'subheader'=>'Revisi Anggaran');
		$data['view'] = "revisi/v_surat_penolakan";
		$this->load->view('main/utama', $data);
 	}

 	function pdf(){
 		$es=substr($this->session->userdata('kdso'),0,2) ; //echo
 		//$data['und']  = $this->revisi_23_model->get_und();
 		$data['dir']  = $this->stol_model->get_dir($es);
		$data['almt1']  = $this->stol_model->get_alm1($es);
		$data['almt2']  = $this->stol_model->get_alm2($es);
		$data['menu']  = $this->dsw_model->get_menu( $this->uri->segment(1) );
		//$data['bread'] = array('header'=>'Admin Surat Tugas', 'subheader'=>'Admin Surat Tugas');

		$data['view']  = "revisi/cetak_pengesahan";
		$this->load->view('revisi/cetak_pengesahan', $data);

		$html = $this->output->get_output();
		// Convert to PDF
		$this->dompdf->load_html($html);
		$this->dompdf->set_paper('A4','portrait');
		$this->dompdf->render();
		$this->dompdf->stream( 'Und'.'.pdf',array('Attachment'=>0) );
 	}

 	function pdf_penolakan(){
 		$es=substr($this->session->userdata('kdso'),0,2) ; //echo
 		//$data['und']  = $this->revisi_23_model->get_und();
 		$data['dir']  = $this->stol_model->get_dir($es);
		$data['almt1']  = $this->stol_model->get_alm1($es);
		$data['almt2']  = $this->stol_model->get_alm2($es);
		$data['menu']  = $this->dsw_model->get_menu( $this->uri->segment(1) );
		//$data['bread'] = array('header'=>'Admin Surat Tugas', 'subheader'=>'Admin Surat Tugas');

		$data['view']  = "revisi/cetak_penolakan";
		$this->load->view('revisi/cetak_penolakan', $data);

		$html = $this->output->get_output();
		// Convert to PDF
		$this->dompdf->load_html($html);
		$this->dompdf->set_paper('A4','portrait');
		$this->dompdf->render();
		$this->dompdf->stream( 'Und'.'.pdf',array('Attachment'=>0) );
 	}

 	function mundur_status($lvl,$rev_id){
 		$data = $this->Revisi_PA_model->mundur_status($lvl,$rev_id);
		redirect("Revisi_PA/dash");

 	}


 	function create_zip_adk($rev_id){
		//$rev_id='2020.060.01.001';
		$nmfile = $rev_id.'.zip';
		$thang  = $this->session->userdata('thang');
		$jenis  = "ADK";
		$source = "files/revisi_SatuDJA/$rev_id/adk_satker/";
		$destination = "files/files_backup/$nmfile";

		$ipSource = "files/revisi_SatuDJA/$rev_id/adk_satker";
		$ipTarget = "files/files_backup/$rev_id";

		
		$this->Zip($source,$destination);
		$this->getall( $nmfile );
		$this->rrmdir("files/files_backup/$rev_id");

	}

	function getall( $nmfile ) {
		$file = "files/files_backup/$nmfile";

		//echo "$file <br>";
		if (file_exists($file)) {
		  header('Content-Disposition: attachement; filename='.basename($file));
		  header('Content-Type: application/force-download');
		  header('Expires: 0');
		  header('Cache-Control: must-revalidate');
		  header('Content-Length: '.filesize($file));
		  header('Connection: close');
		  ob_get_clean();
		  readfile($file);
		  ob_end_flush();
		  unlink($file);
		} else {
		  header("HTTP/1.0 404 Not Found");
		}
	}

	 function rrmdir($dir) { 
	   	if (is_dir($dir)) { 
	     $objects = scandir($dir); 
	     foreach ($objects as $object) { 
	       if ($object != "." && $object != "..") { 
	         if (is_dir($dir."/".$object))
	           $this->rrmdir($dir."/".$object);
	         else
	           unlink($dir."/".$object); 
	       } 
	     }
	     rmdir($dir); 
	   } 
	} 

	 function Zip($source, $destination) {
		ini_set('memory_limit', '-1');
	    if (!extension_loaded('zip') || !file_exists($source)) {
	        return false;
	    }

	    $zip = new ZipArchive();
	    if (!$zip->open($destination, ZIPARCHIVE::CREATE)) {
	        return false;
	    }

    	$source = str_replace('\\', '/', realpath($source));

	    if (is_dir($source) === true)
	    {
	        $files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($source), RecursiveIteratorIterator::SELF_FIRST);

	        foreach ($files as $file)
	        {
	            $file = str_replace('\\', '/', $file);

	            // Ignore "." and ".." folders
	            if( in_array(substr($file, strrpos($file, '/')+1), array('.', '..')) )
	                continue;

	            $file = realpath($file);

	            if (is_dir($file) === true)
	            {
	                $zip->addEmptyDir(str_replace($source . '/', '', $file . '/'));
	            }
	            else if (is_file($file) === true)
	            {
	                $zip->addFromString(str_replace($source . '/', '', $file), file_get_contents($file));
	            }
	        }
	    }
	    else if (is_file($source) === true)
	    {
	        $zip->addFromString(basename($source), file_get_contents($source));
	    }

	    return $zip->close();
	}
}

// tess
