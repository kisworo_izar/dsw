<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Installer extends CI_Controller {

	function __construct() {
		parent::__construct();
	}

	public function index($idinstaller=1) {
		$data['menu']  = $this->dsw_model->get_menu( $this->uri->segment(1) );
		$data['link']  = 'http://www.anggaran.kemenkeu.go.id/dja/edef-produk-aplikasi-list.asp?sub='. $idinstaller;
		$data['bread'] = array('header'=>'Download Aplikasi', 'subheader'=>'Installer');
		$data['view']  = "installer/rkakl";
		$this->load->view('main/utama', $data);
	}

}
