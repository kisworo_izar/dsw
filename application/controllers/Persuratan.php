<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Persuratan extends CI_Controller {

	var $param;	//Create a variable for the entire controller

	function __construct() {
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
		$this->load->model('persuratan_model');
	}

	public function index() {
    	if (! $this->session->userdata('isLoggedIn') ) redirect("login/show_login");
		$tahun = date('Y');
		$bulan = sprintf('%02d', date("m"));
		$this->param = $this->dsw_model->param("tahun:$tahun, bagian:01, bulan:$bulan");
		$this->session->set_userdata( array('cari' => '') );
		$this->grid();
	}

	function goto_grid() {
		$tahun 	= $this->uri->segment(3);
		$bulan 	= $this->uri->segment(4);
		$bagian	= $this->uri->segment(5);
		$status	= $this->uri->segment(6);
		$start	= $this->uri->segment(7);

		$this->param = $this->dsw_model->param("tahun:$tahun, bulan:$bulan, bagian:$bagian, rowstart:$start, status:$status");
		$this->grid();
	}

	function grid() {
		$grid = $this->persuratan_model->get_data( $this->param );
		if ($this->param->status > 0) $grid = $this->persuratan_model->filter_status( $grid, $this->param->status );

		$url = site_url( "persuratan/goto_grid/". $this->param->link );
		$config = $this->fc->pagination( $url, count($grid), $this->param->rowlimit, '7' );
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
	    $config['cur_tag_close'] = '</a></li>';
		$this->pagination->initialize($config);

		$data['param'] = $this->param;
		$data['badge'] = $this->persuratan_model->get_badge( $grid );
		$data['record']= count($grid);
		$data['surat'] = array_slice($grid, $this->param->rowstart, $this->param->rowlimit);
		$data['tahun'] = $this->dsw_model->tahun();
		$data['bulan'] = $this->dsw_model->bulan();
		$data['bagian']= $this->dsw_model->bagian('dropdown', $this->param, $url);

		$data['menu']  = $this->dsw_model->get_menu( $this->uri->segment(1) );
		$data['bread'] = array('header'=>'Monitoring Disposisi Surat', 'subheader'=>'Persuratan');
		$data['view']  = "persuratan/v_persuratan_list";
		$this->load->view('main/utama', $data);
	}

	function cari() {
		if ($_POST['tombol'] == 'Cari') {
			$tahun = $this->uri->segment(3);
			$bulan = '00';
			$this->session->set_userdata( array('cari' => $_POST['cari']) );
		} else {
			$tahun = date('Y');
			$bulan = sprintf('%02d', date("m"));
			$this->session->set_userdata( array('cari' => '') );
		}

		$bagian	= $this->uri->segment(5);
		$start	= $this->uri->segment(7);

		$this->param = $this->dsw_model->param("tahun:$tahun, bulan:$bulan, bagian:$bagian, status:0, rowstart:$start");
		$this->grid();
	}

	function layanan() {
    	if (! $this->session->userdata('isLoggedIn') ) redirect("login/show_login");
    	if (! isset($this->param)) $this->param = $this->dsw_model->param("tahun:". date('Y') .", bagian:01, bulan:". sprintf('%02d', date("m")) .", model:". $this->uri->segment(3));
		$this->session->set_userdata( array('cari' => '') );

		$grid = $this->persuratan_model->get_layanan( $this->param );
		$model= $this->persuratan_model->nama_model( $this->param->model );
		$url  = site_url( "persuratan/goto_layanan/". $this->param->model ."/". $this->param->link );
		$config = $this->fc->pagination( $url, count($grid), $this->param->rowlimit, '8' );
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
	    $config['cur_tag_close'] = '</a></li>';
		$this->pagination->initialize($config);

		$data['param'] = $this->param;
		$data['record']= count($grid);
		$data['surat'] = array_slice($grid, $this->param->rowstart, $this->param->rowlimit);
		$data['badge'] = $this->persuratan_model->get_badge( $data['surat'] );
		$data['tahun'] = $this->dsw_model->tahun();
		$data['bulan'] = $this->dsw_model->bulan();
		$data['bagian']= $this->dsw_model->bagian('dropdown', $this->param, $url);

		$data['menu']  = $this->dsw_model->get_menu( $this->uri->segment(1) );
		$data['bread'] = array('header'=>"Layanan Unggulan $model", 'subheader'=>'Persuratan');
		$data['view']  = "persuratan/v_persuratan_layanan";
		$this->load->view('main/utama', $data);
		// $this->fc->browse( $data['bagian'] );
		// echo "<pre>"; print_r($this->param); echo "</pre>";
	}

	function goto_layanan() {
		$model 	= $this->uri->segment(3);
		$tahun 	= $this->uri->segment(4);
		$bulan 	= $this->uri->segment(5);
		$bagian	= $this->uri->segment(6);
		$status	= $this->uri->segment(7);
		$start	= $this->uri->segment(8);

		$this->param = $this->dsw_model->param("tahun:$tahun, bulan:$bulan, bagian:$bagian, model:$model, rowstart:$start, status:$status");
		$this->layanan();
		// echo "<pre>"; print_r($this->param); echo "</pre>";

		// $str = "tahun:$tahun, bulan:$bulan, bagian:$bagian, model:$model, rowstart:$start, status:$status";
		// $arr = explode(',', $str);
		// echo "<pre>"; print_r($arr); echo "</pre>";

		// 	for ($i=0; $i<count($arr); $i++) {
		// 		$arm = explode(':', $arr[$i]);
		// 		$var = str_replace(' ','',$arm[0]);
		// 		echo "<pre>"; print_r($var); echo "</pre>";
		// 	}
	}

	function open_pdf() {
		$nmfile = $this->uri->segment(3);
        $ifile  = str_replace(' ', '%20', $nmfile);
        $fldthn = substr($ifile, 0,4);
        $file = '/var/www/html/DSW/files/persuratan/attach/'.$fldthn.'/'.strtolower($ifile);

        if (file_exists($file)) {
           header('Content-Description: File Transfer');
           header('Content-Type: application/pdf');
           header('Content-Disposition: attachment; filename="'.basename($file).'"');
           header('Expires: 0');
           header('Cache-Control: must-revalidate');
           header('Pragma: public');
           header('Content-Length: ' . filesize($file));
           readfile($file);
           exit;
        } else {
             echo "<script>javascript:alert('File tidak tersedia...'); window.close()</script>";
             exit;
        }
	}


	public function json_tree_esl2() {
		$dbsrt = $this->load->database('surat', TRUE);
		$query = $dbsrt->query("select kode, uraian2 from t_esl3 where right(kode,1)='0' order by 1");
		$hasil = array();
		array_push($hasil, array("id"=>"01", "text"=>"Seluruh DJA", "state"=>"closed", "children"=>"") );
		foreach ($query->result() as $row) {
			$node = array(
				"id" => $row->kode, "text" => $row->uraian2, "state" => "closed",
				"children" => $this->json_tree_esl3( $row->kode )
			);
		    array_push($hasil, $node);

		}
		echo json_encode($hasil);
		return ;
	}

	public function json_tree_esl3( $kode ) {
		$dbsrt = $this->load->database('surat', TRUE);
		$id    = substr($kode,0,1);
		$query = $dbsrt->query("select kode, uraian2 from t_esl3 where left(kode,1)='$id' and right(kode,1)<>'0' order by 1");
		$esl_3 = array();
		foreach ($query->result() as $row) {
			$node = array( "id" => $row->kode, "text" => $row->uraian2 );
		    array_push($esl_3, $node);
		}
		return $esl_3;
	}

	public function test() {
		// echo "<pre>"; print_r($this->param); echo "</pre>";
	}
}
