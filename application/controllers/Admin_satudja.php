<?php
class Admin_satudja extends CI_Controller {
	function __construct() {
		parent::__construct();
	    $this->load->model('admin_satudja_model');
	}

	function index() {
		$this->admin_user();
	}

	function admin_user() {
        // if ( !$this->fc->check_akses('admin_panel/admin_user') ) show_404();
		  $this->fc->log('Admin SatuDJA - User');
		$data = $this->admin_satudja_model->get_user();
		// echo '<pre>';print_r($data);exit();
		$data['bread'] = array('view'=>'admin_satudja/v_user_satudja_grid', 'header'=>'Data User', 'subheader'=>'Admin Satu DJA');
		$this->load->view('main/utama', $data);
	}

	function crud_user() {
		echo $this->admin_satudja_model->save_user();
	}

	function admin_user_group() {
        // if ( !$this->fc->check_akses('admin_panel/admin_user_group') ) show_404();
		  $this->fc->log('Admin SatuDJA - User Group');
		$data = $this->admin_satudja_model->get_user_group();
		// echo '<pre>';print_r($data);exit();
		$data['bread'] = array('view'=>'admin_satudja/v_user_group_satudja_grid', 'header'=>'Data User Group', 'subheader'=>'Admin Satu DJA');
		$this->load->view('main/utama', $data);
	}

	function crud_user_group() {
		echo $this->admin_satudja_model->save_user_group();
	}

	function admin_menu() {
        // if ( !$this->fc->check_akses('admin_panel/admin_menu') ) show_404();
		  $this->fc->log('Admin SatuDJA - Menu');
		$data = $this->admin_satudja_model->get_menu();
		$data['bread'] = array('view'=>'admin_satudja/v_menu_satudja_grid', 'header'=>'Data Menu', 'subheader'=>'Admin Satu DJA');
		$this->load->view('main/utama', $data);
	}

	function crud_menu() {
		echo $this->admin_satudja_model->save_menu();
	}

	function test() {
		$this->load->view('admin_ui/v_form_test');
		// echo '<pre>'; print_r( $data['tabel']  );
	}
}
