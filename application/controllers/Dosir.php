<?php
class Dosir extends CI_Controller {
	function __construct() 
	{
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
	}

	public function index() 
	{
		// require_once 'assets/plugins/htmlpurifier/HTMLPurifier.auto.php';
		// $config	  = HTMLPurifier_Config::createDefault();
		// $purifier = new HTMLPurifier($config);

		if (! $this->session->userdata('isLoggedIn') ) redirect("login/show_login");

		if (isset($_GET['q']))    $menu = $_GET['q'];    else show_404();
		if (isset($_GET['opt']))  $opt  = $_GET['opt'];	 else $opt  = '';

		if (preg_match("~\bcRhmt\b~", $menu))	$this->main();
		else show_404();
	}

	private function main()
	{
		$this->fc->log('Dosir - Main');

		$data['menu']  = $this->dsw_model->get_menu( $this->uri->segment(1) );
		$data['bread'] = array('header'=>'Dosir', 'subheader'=>'Dosir');
		$data['view']  = "dosir/dosir_main";

		$this->load->view('main/utama', $data);
	}

	// function group()
	// {
	// 	$this->fc->log('Dosir - Kategori');

	// 	$data['menu']  = $this->dsw_model->get_menu( $this->uri->segment(1) );
	// 	$data['bread'] = array('header'=>'Manajemen Kategori', 'subheader'=>'Dosir');
	// 	$data['view']  = "dosir/dosir_group";

	// 	$this->load->view('main/utama', $data);
	// }
}