<?php
class Paket extends CI_Controller {
	function __construct() {
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
	    $this->load->model('dsw_model');
	    $this->load->model('m_paket');
	}

	public function index() {
    	if (! $this->session->userdata('isLoggedIn') ) redirect("login/show_login");
		$this->session->set_userdata( array('cari' => '') );
		$this->dash();
	}

	public function grid() {
		$this->fc->log('Paket - Rekam Paket');
		$row = $this->m_paket->get_data();
		$config = $this->fc->pagination( site_url("paket/grid"), count($row), 10, '3');
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
	    $config['cur_tag_close'] = '</a></li>';
		$this->pagination->initialize($config);

		$data['nilai']  = array_slice($row, $this->uri->segment(3), 10);
		$data['t_user'] = $this->m_paket->json_user();
		$data['judul']  = '';

		$data['menu']  = $this->dsw_model->get_menu( $this->uri->segment(1) );
		$data['bread'] = array('header'=>'Admin Paket', 'subheader'=>'Paket Admin');
		$data['view'] = "paket/V_paket_grid";
		$this->load->view('main/utama', $data);
	}

	function crud() {
		$this->m_paket->save();
		redirect("paket/grid");
	}

	function cari() {
		if ($_POST['search'] == 'search') {
			$this->session->set_userdata('cari', $_POST['cari']);
		} else {
			$this->session->set_userdata('cari', '');
		}

		if ($_POST['nmfunction']=='dash') {
			$this->dash();
		} else {
			$this->grid();
		}
	}

	public function dash() {
		$this->fc->log('Paket - Informasi Paket');
		$row = $this->m_paket->get_data();
		$row = $this->m_paket->get_data_dash( $row );
		$config = $this->fc->pagination( site_url("paket/dash"), count($row), 12, '3');
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		$this->pagination->initialize($config);

		$data['nilai'] = array_slice($row, $this->uri->segment(3), 12);
		$data['total'] = $this->m_paket->get_total();

		$data['menu']  = $this->dsw_model->get_menu( $this->uri->segment(1) );
		$data['bread'] = array('header'=>'Informasi Paket', 'subheader'=>'Informasi Paket');
		$data['view'] = "paket/V_paket_dash";
		$this->load->view('main/utama', $data);

		// $this->fc->browse( $row );
		// echo '<pre>'; print_r( $row );
	}

	public function dashi() {
		$row = $this->m_paket->get_data();
		$row = $this->m_paket->get_data_dash( $row );

		$data['total'] = $this->m_paket->get_total();
		print_r($data['total']);
		// $this->fc->browse( $data['total'] );
	}
}
