<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Absensi extends CI_Controller {

	function __construct() {
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
		$this->load->model('m_absensi');
	    $this->session->set_userdata(array('cari' => ''));
	    $this->milea = $this->load->database('milea',TRUE);
	}

	public function index() {
    	if (! $this->session->userdata('isLoggedIn') ) redirect("login/show_login");
		$this->fc->log('Presensi Pegawai');
		$this->menu();
	}

	public function menu( $start=null, $end=null ) {
		if ($start==null) {
   			$start = date('Y-m-d', strtotime('-15 days', strtotime( date('Y-m-d') )));
			$end   = date('Y-m-d');
			$data['start'] = date('d-m-Y', strtotime('-15 days', strtotime( date('d-m-Y') )));
			$data['end']   = date('d-m-Y');
		} else {
			$data['start'] = $this->fc->idtgl( $start );
			$data['end']   = $this->fc->idtgl( $end );
		}

		$range = "and tgl>='$start' and tgl<='$end'";
		$data['absensi']= $this->m_absensi->get_data( $range );
		$data['idws']   = $this->m_absensi->get_idws();
		$data['rekap']  = $this->m_absensi->get_rekap();
		$data['maxtgl'] = $this->m_absensi->get_max();

		$data['menu']   = $this->dsw_model->get_menu( $this->uri->segment(1) );
		$data['bread']  = array('header'=>'&nbsp;', 'subheader'=>'Kehadiran');
		$data['view']   = "absensi/v_absensi";
		$this->load->view('main/utama', $data);
	}

	function cari() {
		$start = $this->fc->ustgl( $_GET['start'] );
		$end   = $this->fc->ustgl( $_GET['end'] );
		$this->menu( $start, $end );
	}

	function test( $start=null, $end=null ) {

	}

}
