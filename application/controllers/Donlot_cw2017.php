<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Donlot_cw2017 extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

    function index() {
        $this->csv();
    }

    function all_tables() {
        $dbcw17 = $this->load->database('ora_db2017', TRUE);
        $query  = $dbcw17->query("Select * From all_tables");
        $cwout  = $query->result_array();
        echo '<pre>'; print_r($cwout);
    }

    function dept() {
        $dbcw17 = $this->load->database('ora_db2017', TRUE);
        $table  = 'SPAN_ADK17_D_ITEM_TEMP';

        $query  = $dbcw17->query("Select kddept, count(*) jml From $table Where rownum<=15000  Group By kddept Order By jml");
        $cwout  = $query->result_array();
        $this->fc->browse( $cwout );

        // foreach ($cwout as $row) {
        // 	echo $row['KDDEPT'] .' : '. $row['JML'] .'<br>';
        // 	$this->csv( $row['KDDEPT'] );
        // }
      }


    function csv( $kddept=null ) {
        $dbcw17 = $this->load->database('ora_db2017', TRUE);
        $table  = 'SPAN_ADK17_D_ITEM_TEMP';

        $query  = $dbcw17->query("Select kddept, count(*) jml From $table Group By kddept Order By jml");
        $cwout  = $query->result_array();
        $this->fc->browse($cwout);

        $query  = $dbcw17->query("Select * From $table Where kddept='$kddept'");
        $cwout  = $query->result_array();

        if ($cwout) {
            echo "$kddept <br>";
            $df = fopen("files/d_item17_$kddept.csv", 'w');
            foreach ($cwout as $row) fputcsv($df, $row, '|', '#');
            fclose($df);
        }
      }

    function csv_all() {
        $dbcw17 = $this->load->database('ora_db2017', TRUE);
        $table  = 'SPAN_ADK17_D_ITEM_TEMP';
        $table  = 'P_2017_KEPPRES_KMPNEN';

        for ($i=1; $i<200; $i++) {
            $kddept = sprintf('%03d', $i);

            $query  = $dbcw17->query("Select * From $table Where kddept='$kddept'");
            $cwout  = $query->result_array();

            if ($cwout) {
                echo "$kddept <br>";
                $df = fopen("files/d_kmpnen17_$kddept.csv", 'w');
                foreach ($cwout as $row) fputcsv($df, $row, '|', '#');
                fclose($df);
            }
        }
      }

    function loader() {
        $files = scandir('files/d_output');
        foreach ($files as $row) {
            if (strpos($row, '.csv')) {
                $csv = "c:/xampp/htdocs/dsw/files/d_output/$row";
                $sql = "Load Data InFile '$csv' Into Table d_output Fields Terminated By '|' Optionally Enclosed By '#'";
                $run = $this->db->query( $sql );
                echo "$row : $run <br>";
            }
        }
    }

}
