<?php
class Gallerydja extends CI_Controller {
	function __construct() {
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
	}

	function index() {
    	if (! $this->session->userdata('isLoggedIn') ) redirect("login/show_login");
		if (isset($_GET['q'])) $menu = $_GET['q'];
		else show_404();
		if ($menu == 'g4lrI') $this->gallery();
		else show_404();
	}

	private function gallery() {
		$this->fc->log('Gallery');
		$data['bread'] = array('header'=>'&nbsp;', 'subheader'=>'Gallery DJA');
		$data['view']  = "gallery/V_gallery";
		$this->load->view('main/utama', $data);
	}
}
