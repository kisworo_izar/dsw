<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Rev extends CI_Controller {

	function __construct() {
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
	    // $this->load->model('profile_model');
	    $this->load->model('revisi_model');
	    $this->load->model('adk_dipa_model');
	    $this->session->set_userdata(array('cari' => ''));
	    $this->fc->MemVar();
		$this->index = "jenis='RA' AND kddept='$this->dept' AND kdunit='$this->unit'";
		$this->list  = array('d_output','d_soutput','d_kmpnen','d_skmpnen','d_akun','d_item','d_pdpt','d_cttakun','d_kpa','d_trktrm');
	}

	function index() {
		// echo '<pre>';print_r($this->session);exit;
        // echo 'tes';exit;
		// require_once 'assets/plugins/htmlpurifier/HTMLPurifier.auto.php';
		// $config	  = HTMLPurifier_Config::createDefault();
		// $purifier = new HTMLPurifier($config);
		$month 	  = date('m')-'1';
		$hr       = date('d')-'1';
		$thn 	  = date('Y');

		if (! $this->session->userdata('isLoggedIn') ) redirect("login/show_login");
		// if (! $this->fc->check_akses('rev?q=0f4d2') ) show_404();
		// if (! $this->fc->check_akses('rev?q=0f4d2') ) show_404();

		if (isset($_GET['q'])) 		 	$menu   = $_GET['q']; 				else show_404();
		if (isset($_GET['rev_id'])) 	$rev_id = $_GET['rev_id'];
		if (isset($_POST['rev_id'])) 	$rev_id = $_POST['rev_id'];
		if (isset($_POST['level']))  	$level  = $_POST['level'];
		if (isset($_GET['revid'])) 		$revid  = $_GET['revid'];
		if (isset($_GET['valid']))  	$valid  = $_GET['valid'];			else $valid = '1';
		if (isset($_GET['view']))  		$view   = $_GET['view'];			else $view  = 'false';
		if (isset($_POST['adk'])) 	 	$adk 	= $_POST['adk']; 			else $adk  	= '';
		if (isset($_POST['otp']))	 	$otp 	= $_POST['otp']; 			else $otp  	= 'zonk!';
		if (isset($_GET['unit'])) 	 	$unit   = $_GET['unit']; 			else $unit 	= '00000';
		// if (isset($_GET['start']))  	$start	= $_GET['start']; 			else $start = date("01-$month-$this->thang");
		if (isset($_GET['start']))  	$start	= $_GET['start']; 			elseif($this->session->userdata('idusergroup') == 'admin') $start = date("$hr-m-$thn"); else $start = date("d-$month-$thn");
		if (isset($_GET['end'])) 		$end  	= $_GET['end'];   			else $end   = date("d-m-$thn");
		if (isset($_GET['per_page'])) 	$page   = (int)$_GET['per_page']; 	else $page 	= null;
		if (isset($_GET['fol']))      	$fol  	= $_GET['fol'];
		if (isset($_GET['fil']))      	$fil  	= $_GET['fil'];

		// $this->fc->MemVar('RA', $unit);
		// $this->page = $page;

		if (isset($_POST['search'])) {
			if ($_POST['search'] == 'search') $this->session->set_userdata('cari', strtolower($_POST['cari']));
			else $this->session->set_userdata('cari', '');
		}

		// if 		($menu == '0f4d2') $this->parsing();
		// else if ($menu == 'cd13c') $this->pengajuan();
		// else if ($menu == 'aef2f') $this->fileupload();
		// else if ($menu == '2b7bf') $this->save_pengajuan();
		// else if ($menu == 'xn2bj') $this->pengajuan_perbaikan($rev_id);
		// else if ($menu == 'oub82') $this->save_perbaikan();

		// else if ($menu == 'f1073') $this->form_validasi();
		// else if ($menu == 'b8130') echo $this->revisi_model->validasi_adk($rev_id, $adk);
		// else if ($menu == 'cd015') echo $this->revisi_model->validasi_pmk($rev_id, $level);
		// else if ($menu == 'efc4a') echo $this->revisi_model->validasi_ssb($rev_id);
		// else if ($menu == 'df6a7') $this->save_validasi();
		// else if ($menu == 'b02bb') $this->form_otp();

		// else if ($menu == '4e88a') echo $this->revisi_model->otp_create($rev_id);
		// else if ($menu == '836cf') echo $this->revisi_model->otp_verify($rev_id, $otp);
		// else if ($menu == 'e8d3d') $this->save_otp();

        // else if ($menu == 'bb608') $this->monit_satker($start, $end);
		if ($menu == 'bb608') $this->monit_satker($start, $end);
		// else if ($menu == 'v4l1d') $this->bypass_validasi();
        
		else if ($menu == 'bc007') $this->monit_satker_validasi($revid, $valid, $view);
		// else if ($menu == 'c790f') $this->adk_per_tiket($rev_id);
		// else if ($menu == '0c2d2') $this->arsip_data_revisi();
		// else if ($menu == '28afa') $this->rekap_revisi($unit,$start, $end);
		else if ($menu == 'Xsh5d') $this->header($fol,$fil);
		// else if ($menu == 't333t') $this->t3st();
		else show_404();
	}

	

	private function parsing() {
        // echo 'tes';exit;
		// $this->fc->log('e-Revisi - Pengajuan');
		// $data = $this->revisi_model->get_data();
		// if 	(substr($data['rev_upload_step'],0,1) == '1' or ! $data['izin']) $this->pengajuan();
		// if 	(substr($data['rev_upload_step'],0,1) == '2') $this->form_validasi();
		// if 	(substr($data['rev_upload_step'],0,1) == '3') $this->form_otp();
	}

	private function pengajuan() {
		$this->fc->log('Pengajuan Revisi');
		$data['data']  = $this->revisi_model->get_data();

		if ($data['data']['appl'] == 'sakti') $this->revisi_model->move_adk_sakti($data['data']['rev_id'], $data['data']['doc_adk']);
		$header		   = ($data['data']['t6_status'] == '3') ? '(PERBAIKAN ADK/DOKUMEN)' : '';
		$data['user']  = $this->profile_model->get_data();
		$data['bread'] = array('header'=>"Pengajuan Revisi $header", 'subheader'=>'e-Revisi', 'view'=>'revisi/v_pengajuan_revisi');
		$this->load->view('main/utama', $data);
	}

	private function pengajuan_perbaikan( $rev_id ) {
		$this->fc->log('Pengajuan Perbaikan Revisi');
		$data['data']  = $this->revisi_model->get_data_perbaikan( $rev_id );

		if ($data['data']['appl'] == 'sakti') $this->revisi_model->move_adk_sakti_ok($data['data']['rev_id'], $data['data']['doc_adk']);
		$data['user']  = $this->profile_model->get_data();
		$data['bread'] = array('header'=>"Pengajuan Perbaikan ADK / Dokumen", 'subheader'=>'e-Revisi', 'view'=>'revisi/v_pengajuan_perbaikan');
		$this->load->view('main/utama', $data);
	}

	private function fileupload() {
		$ext  = '.s'.substr($this->thang, 2,2);
		$path = "files/revisi/". $_POST['rev_id'];
		$old  = umask(0);
		is_dir($path) || mkdir($path, 0777, true);
		copy('files/revisi/index.html', "$path/index.html");
		umask($old);

		$file = $_FILES[ $_POST['name'] ];
		$resp = array('uploaded' => 'Gagal');
		if ($file) {
			move_uploaded_file($file["tmp_name"], $path .'/'. $file['name']);
			$resp = array('uploaded' => $file['name']);
		}

		if (strpos($file['name'], $ext)) {
			$valid = $this->adk_dipa_model->extract_adk_load_csv($_POST['rev_id'], $file['name']);
			if ($valid != 'Valid') $resp = array('uploaded' => $valid);
		}

		$this->dbrvs->query("UPDATE revisi SET rev_upload_step='1' WHERE rev_tahun='$this->thang' AND rev_id='". $_POST['rev_id'] ."'");
		echo json_encode($resp);
 	}

	private function save_pengajuan() {
		$data = $this->revisi_model->get_data();
		if 	($data['rev_upload_step'] == '1') $this->revisi_model->save_pengajuan();
		$this->form_validasi();
	}

	private function save_perbaikan() {
		$rev_id = $_POST['rev_id'];
		$adk = $this->revisi_model->validasi_adk($rev_id, '1');
		$pmk = $this->revisi_model->validasi_pmk($rev_id, '1');
		$ssb = $this->revisi_model->validasi_ssb($rev_id);

		if ($adk == 'Valid' and $pmk == 'Valid' and $adk == 'Valid') {
			$this->revisi_model->save_perbaikan();
			redirect('revisi?q=bb608');
		} else {
			redirect("revisi?q=xn2bj&rev_id=$rev_id");
		}
	}

	private function form_validasi() {
		$data['data']  = $this->revisi_model->get_data();
		$data['user']  = $this->profile_model->get_data();
		$data['bread'] = array('header'=>'Validasi Revisi', 'subheader'=>'e-Revisi', 'view'=>'revisi/v_validasi_revisi');
		$this->load->view('main/utama', $data);
	}

	private function save_validasi() {
		if ($_POST['submit'] == 'lanjut') {
			$data = $this->revisi_model->get_data();
			if 	(substr($data['rev_upload_step'],0,1) == '2') $this->revisi_model->save_validasi();
			$this->form_otp();
		} else {
			$this->revisi_model->batal_pengajuan();
			redirect('revisi?q=bb608');
		}
	}

	private function form_otp() {
		$data['data']  = $this->revisi_model->get_data();
		$data['user']  = $this->profile_model->get_data();
		$data['bread'] = array('header'=>'Verifikasi OTP', 'subheader'=>'e-Revisi', 'view'=>'revisi/v_otp_revisi');
		$this->load->view('main/utama', $data);
	}

	private function save_otp() {
		if ($_POST['submit'] == 'lanjut') {
			$data = $this->revisi_model->get_data();
			if 	($data['rev_upload_step'] == '3') $this->revisi_model->save_otp( $data['rev_tahap'] );
			$this->pdf_dan_email( $data['rev_id'] );
		} else {
			$this->revisi_model->batal_pengajuan();
		}
		redirect('revisi?q=bb608');
	}

	private function monit_satker($start, $end) {
		// echo 'tess';exit;
        // $start = '01-06-2020';
        // $end   = '30-12-2020';
        // echo $start;exit;
        // echo 'asada';exit;
		// $this->fc->log('Monitoring Revisi');
        $data = $this->revisi_model->get_monit($this->fc->ustgl($start), $this->fc->ustgl($end));
        // echo '<pre>';print_r($data);exit;
		$data['bread'] = array('header'=>'Monitoring Pengajuan Revisi', 'subheader'=>'e-Revisi', 'view'=>'revisi/v_monit_satker');
		$this->load->view('main/utama', $data);
	}

	private function monit_satker_validasi($rev_id, $valid, $view) {
	    if ($this->thang == '2020') $data['tabel'] = $this->revisi_model->get_monit_validasi_2020($rev_id, $valid, $view);
	    else 						$data['tabel'] = $this->revisi_model->get_monit_validasi_2021($rev_id, $valid, $view);
		$data['valid'] = $valid;
		$data['bread'] = array('header'=>'Hasil Validasi ADK Revisi', 'subheader'=>'e-Revisi', 'view'=>'revisi/v_monit_satker_validasi');
		// echo '<pre>';print_r($data);exit;
		$this->load->view('main/utama', $data);
	}

	private function adk_per_tiket( $rev_id ) {
		$this->fc->log('Tabel ADK');
		$data = $this->revisi_model->adk_per_tiket( $rev_id );
		$data['bread'] = array('header'=>'Data ADK per Tiket', 'subheader'=>'e-Revisi', 'view'=>'revisi/v_adk_per_tiket');
		$this->load->view('main/utama', $data);
	}

	private function rekap_revisi($unit, $start=null, $end=null) {
		$this->fc->log('Rekapitulasi Revisi');
		$data['start'] = $start;
		$data['end']   = $end;
		$data['unit']  = $unit;
		if ($start==null) { $start = date('01-m-Y'); $end = date('d-m-Y'); }
		$data['rekap']  = $this->revisi_model->get_rekap_revisi($this->fc->ustgl($start), $this->fc->ustgl($end) );
		$data['bread'] = array('header'=>'Rekapitulasi Revisi', 'subheader'=>'e-Revisi', 'view'=>'revisi/v_rekap_revisi');
		$this->load->view('main/utama', $data);
	}

	private function arsip_data_revisi() {
		$this->fc->log('Arsip Data Revisi');

		$data   = $this->revisi_model->get_arsip_dipa();
		$config = $this->fc->pagination( site_url("revisi?q=0c2d2&unit=$this->dept$this->unit"), $data['jml'], 25, 4);
        $config['page_query_string'] = TRUE;
        $this->pagination->initialize($config);

		$data['data']  = $data['arr'];
		$data['refr']  = $this->adk_dipa_model->get_unit();
		$data['bread'] = array('header'=>'Arsip Data Anggaran', 'subheader'=>'e-Revisi', 'view'=>'revisi/v_arsip_data');
		$this->load->view('main/utama', $data);
	}

 	private function pdf_dan_email( $rev_id ) {
 		$data = $this->revisi_model->get_pdf($rev_id);
		$this->load->view('revisi/cetak_tanda_terima_baru', $data);

		$html = $this->output->get_output();
		$this->dompdf->load_html($html);
		$this->dompdf->set_paper('A4','portraite');
		$this->dompdf->render();

		$file = "tanda_terima_tiket_$rev_id.pdf";
		file_put_contents("files/revisi/$rev_id/$file", $this->dompdf->output());
		$this->revisi_model->email_revisi($rev_id, $data['revisi'], $file);
	 }
	 
	function bypass_validasi(){
		$id 	= $_POST['id'];
		$valid 	= $this->revisi_model->bypass_validasi($id);
		echo json_encode ($valid);
	}

 	function r3valid ($rev_id=null) {
  		if (is_null($rev_id)) show_404();
		$rev = $this->dbrvs->query("SELECT concat(kl_dept,kl_unit) unit, rev_upload_level level, rev_tahap FROM revisi WHERE rev_id='$rev_id'")->row();

 		$this->fc->MemVar("RA", "$rev->unit");
		echo "<br> VALIDASI ULANG : <b> $rev_id </b>";
		if ($rev->rev_tahap == '1') echo '<br> - ADK : '. $this->revisi_model->validasi_adk($rev_id, $rev->level);
		echo '<br> - PMK : '. $this->revisi_model->validasi_pmk($rev_id, $rev->level);
		echo '<br> - SSB : '. $this->revisi_model->validasi_ssb($rev_id);
	}

 	function r3load ($rev_id=null) {
  		if (is_null($rev_id)) show_404();
		$this->fc->timer();
		$rev = $this->dbrvs->query("SELECT concat(kl_dept,kl_unit) unit, doc_adk adk, rev_tahap FROM revisi WHERE rev_id='$rev_id'")->row();

		$this->fc->MemVar("RA", "$rev->unit");
		echo "<br> Revisi ID  : $rev_id";
		echo "<br> Reload ADK : $rev->adk <br>";

		echo '<br><b>Proses Persiapan : '. $this->fc->timer(1) .'</b/> detik. <br>'; $this->fc->timer();
		$this->session->set_userdata( array('kddept' => substr($rev->unit,0,3), 'kdunit' => substr($rev->unit,3,2)) );

		echo '- '. $this->adk_dipa_model->extract_adk_load_csv($rev_id, $rev->adk); 
		echo '<br><br><b>Proses Upload ADK : '. $this->fc->timer(1) .'</b/> detik. <br>'; $this->fc->timer();

		$this->r3valid($rev_id);
		echo '<br><br><b>Proses Validasi : '. $this->fc->timer(1) .'</b/> detik.';
	}

	function header($fol,$fil){
 		$filepath = 'files/revisi_SatuDJA/'.$fol.'/'.$fil;
 		
 		if(file_exists($filepath)){
 			header('Content-Description: File Transfer');
	        header('Content-Type: application/octet-stream');
	        header('Content-Disposition: attachment; filename="'.basename($filepath).'"');
	        header('Expires: 0');
	        header('Cache-Control: must-revalidate');
	        header('Pragma: public');
	        header('Content-Length: ' . filesize($filepath));
	        flush(); // Flush system output buffer
	        readfile($filepath); 
 		}
 	}

 	function recreate_rekap ($kdsatker=null) {
  		if (is_null($kdsatker)) show_404();
		$this->fc->timer();
		$this->adk_dipa_model->recreate_rekap($kdsatker);
		echo '<br><b>Re-create Rekap : '. $this->fc->timer(1) .'</b/> detik.';
	}

 	function t3st_r3load () {
 		$whr = "month(pus_tgl)=8 AND bValid1=1 AND bValid2=0 AND bValid3=1 AND rev_upload_level='3'";
		$rev = $this->dbrvs->query("SELECT rev_id, rev_upload_level, doc_adk, pus_tgl FROM revisi WHERE $whr")->result_array();
		echo $this->ds->browse($rev);

		foreach ($rev as $k=>$v) {
			echo $v['rev_id'] .'<br>';
			$this->r3load( $v['rev_id'] );
		}
	}

 	function t3st () {
  		$rev_id = '2020.560951.003';
		$rev = $this->dbrvs->query("SELECT concat(kl_dept,kl_unit) unit, rev_upload_level level, rev_tahap FROM revisi WHERE rev_id='$rev_id'")->row();
 		$this->fc->MemVar("RA", "$rev->unit");

		$this->fc->timer();
		echo "<br> VALIDASI ULANG : <b> $rev_id </b>";
		if ($rev->rev_tahap == '1') echo '<br> - ADK : '. $this->revisi_model->validasi_adk_speed($rev_id, $rev->level);
		echo '<br><br><b>Proses validasi_adk : '. $this->fc->timer(1) .'</b/> detik.';
		$this->fc->timer();

		// echo '<br> - PMK : '. $this->revisi_model->validasi_pmk_speed($rev_id, $rev->level);
		// echo '<br><br><b>Proses validasi_pmk_speed : '. $this->fc->timer(1) .'</b/> detik.';
		// $this->fc->timer();
		
		// echo '<br> - SSB : '. $this->revisi_model->validasi_ssb($rev_id);
		// echo '<br><br><b>Proses validasi_ssb : '. $this->fc->timer(1) .'</b/> detik.';
	}


}
