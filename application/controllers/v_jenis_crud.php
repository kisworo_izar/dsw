<style media="screen">
  .form-group { margin: 0px;}
</style>


<div class="box box-widget" style="margin: 0px 0px 0px 0px;">
  <div class="box-body">

    <!-- <div class="col-sm-2 pull-right"><div>&nbsp;</div></div>
    <div class="col-sm-1 pull-left"><div>&nbsp;</div></div> -->

    <div class="col-sm-12 pull-left">
      <div class="form-group">
        <label class="col-sm-1 text-right">Kode</label>
        <div class="col-sm-11">
          <input id="0" name="isian[]" type="text" class="form-control" readonly value="">
        </div>
      </div>
      <div class="form-group">
        <label class="col-sm-1 text-right">Uraian</label>
        <div class="col-sm-11">
          <!-- <input id="1" name="isian[]" type="text" class="form-control" placeholder="" value=""> -->
          <textarea id="1" name="isian[]" type="text" class="form-control" rows="10" placeholder=""></textarea>
        </div>
      </div>

      <div class= "form-group text-center" style="padding-top: 10px">
        <button id="Rekam" class="btn btn-warning" onclick="crud( this.value )" value="Rekam">Rekam</button>
        <button id="Ubah"  class="btn btn-warning" onclick="crud( this.value )" value="Ubah">Ubah</button>
        <button id="Hapus" class="btn btn-warning" onclick="crud( this.value )" value="Hapus" >Hapus</button>
      </div>
    </div>

  </div>
</div>


<script type="text/javascript">
  function crud( aksi ) {
    var isian = new Array(),  kdkey = $('#aktif').val();
    $("#crud_form input, textarea").each(function() { isian.push( $(this).val() ); });

    $.ajax({
      url : "<?php echo site_url('revisi_nd_dja/crud_jenis') ?>",
      type: "POST",
      data: { 'aksi': aksi, 'isian': isian, 'kdkey': kdkey },
      success: function(pesan) {
        if (aksi=='Rekam') { window.location.href = "<?php echo site_url('revisi_nd_dja/admin_jenis') ?>" }
        if (aksi=='Ubah') {
          var arr = pesan.split("#");
          var cls = $('#'+kdkey).attr('class');
          $('#'+kdkey).attr('class', pesan);
          $('#'+kdkey).children( 'td:nth-child(1)' ).text( arr[0] );
          $('#'+kdkey).children( 'td:nth-child(2)' ).text( arr[1] );
          move_row( kdkey );
        }
        if (aksi=='Hapus') { 
          move_row( kdkey );
          $('table#iGrid tr#'+kdkey).remove(); 
        }
      }  
    })
  }
</script>
