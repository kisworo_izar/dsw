<?php
class Adk_dipa extends CI_Controller {

	function __construct() {
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
		$this->load->model('dsw_model');
		$this->load->model('adk_dipa_model');
		$this->session->set_userdata( array('cari' => '') );
	    $this->fc->MemVar();
		$this->list = array('d_output','d_soutput','d_kmpnen','d_skmpnen','d_akun','d_item','d_pdpt','d_cttakun');
	}

	function index() {
		if (! $this->session->userdata('isLoggedIn') ) redirect("login/show_login");

		if (isset($_GET['q']))		   $menu  = $_GET['q']; 			else show_404();
		if (isset($_GET['satker']))    $satk  = $_GET['satker'];		else $satk = null;
		if (isset($_GET['id'])) 	   $id    = $_GET['id'];			else $id   = null;
		if (isset($_GET['mula'])) 	   $mula  = $_GET['mula'];			else $mula = '00';
		if (isset($_GET['unit'])) 	   $unit  = $_GET['unit'];  		else $unit = '00000';
		if (isset($_GET['start']))     $start = $_GET['start']; 		else $start= date("01-01-$this->thang");
		if (isset($_GET['end'])) 	   $end   = $_GET['end'];   		else $end  = date("d-m-$this->thang");
		if (isset($_GET['per_page']))  $page  = (int)$_GET['per_page']; else $page = null;
		$this->fc->MemVar('RA', $unit);
		$this->page = $page;

		if (isset($_POST['search'])) {
			if ($_POST['search'] == 'search') $this->session->set_userdata('cari', strtolower($_POST['cari']));
			else $this->session->set_userdata('cari', '');
		}

		if 	    ($menu == 'caa29') $this->matrik_satker($satk, $this->dbrvs, $mula, $this->dbrvs, $id);
		else if ($menu == 'rvs19') $this->matrik_satker($satk, $this->dbrvs, $mula, $this->dbtmp, $id);
		else if ($menu == 'dgs19') $this->digistamp_grid($satk, $this->dbrvs, $mula, $this->dbtmp, $id);
		else if ($menu == '46606') $this->kertas_kerja($satk, $id, "revisi$this->thang");
		else if ($menu == '45007') $this->kertas_kerja($satk, $id, "temp$this->thang");
		else if ($menu == 'bd05e') $this->kertas_kerja($satk, $id, "telaah$this->thang", $unit);
		else if ($menu == 'bf57c') $this->kertas_kerja($satk, $id, "temp$this->thang", $unit);
		else if ($menu == 'ea2c2') $this->ssb_info($satk, $id);
		else if ($menu == 'jk4ds') $this->pagu_realisasi($satk, $id, "revisi$this->thang", $unit);
		else if ($menu == 'lk9oi') $this->pagu_realisasi($satk, $id, "temp$this->thang", $unit);
		else show_404();
	}

	private function matrik_satker($kdsatker, $dbmula, $idmula, $dbjadi, $idjadi) {
		$data['revid'] = array('idmula'=>$idmula, 'idjadi'=>$idjadi);
		$data['tabel'] = $this->adk_dipa_model->matrik_semula_menjadi($kdsatker, $dbmula, $idmula, $dbjadi, $idjadi);
		
		if (! $data['tabel']) {
			$dbjadi  = $this->load->database("backup$this->thang", TRUE);
			$data['tabel'] = $this->adk_dipa_model->matrik_semula_menjadi($kdsatker, $dbmula, $idmula, $dbjadi, $idjadi);
			if (! $data['tabel']) show_404();
		}

		$data['menu']  = $this->dsw_model->get_menu( $this->uri->segment(1) );
		$data['bread'] = array('header'=>'Matrik Semula Menjadi', 'subheader'=>'e-Revisi', 'sidebar'=>'sidebar-collapse');
		$data['view']  = "adk_dipa/v_matrik_semula_menjadi";
		$this->load->view('main/utama', $data);
	}

	private function kertas_kerja($kode, $id, $db, $unit=null) {
		if ($unit != null) { $this->dept = substr($unit,0,3); $this->unit = substr($unit,3,2); }
		$levl = 'giat'; 
		if (strlen($kode) == 6) $levl = 'satker';
		if (strlen($kode) == 8) $levl = 'output';  
		if (strlen($kode) ==12) $levl = 'soutput';  
		$data = $this->adk_dipa_model->kertas_kerja($levl, $kode, $id, $db);

		if (! $data['tabel']) {
			$db   = "backup$this->thang";
			$data = $this->adk_dipa_model->kertas_kerja($levl, $kode, $id, $db);
			if (! $data['tabel']) show_404();
		}

		$data['level'] = $levl;
		$data['menu']  = $this->dsw_model->get_menu( $this->uri->segment(1) );
		$data['bread'] = array('header'=>'Kertas Kerja', 'subheader'=>'Data', 'sidebar'=>'sidebar-collapse');
		$data['view']  = "adk_dipa/v_kertas_kerja";
		$this->load->view('main/utama', $data);
	}

	private function digistamp_grid($kdsatker, $dbmula, $idmula, $dbjadi, $idjadi) {
		$data['revid'] = array('idmula'=>$idmula, 'idjadi'=>$idjadi);
		$data['tabel'] = $this->adk_dipa_model->digistamp_grid($kdsatker, $dbmula, $idmula, $dbjadi, $idjadi);
		if (! $data['tabel']) show_404();

		$data['menu']  = $this->dsw_model->get_menu( $this->uri->segment(1) );
		$data['bread'] = array('header'=>'Perbandingan Digital Stamp', 'subheader'=>'e-Revisi', 'sidebar'=>'sidebar-collapse');
		$data['view']  = "adk_dipa/v_matrik_digistamp";
		$this->load->view('main/utama', $data);
	}

	private function ssb_info($kdsatker, $id) {
		if (! strpos($this->session->userdata('whrdept'), '015')) show_404();
		if ($id == 'PA' or $id == 'AA') { $dbs = "telaah$this->thang"; $this->jnsforum = $id; }
		else { $dbs = "revisi$this->thang"; $this->jnsforum = 'RA'; }
		$data = $this->adk_dipa_model->validasi_ssb($dbs, $kdsatker, $id);
		if (count($data) == 0) show_404();
		$info = $this->adk_dipa_model->validasi_ssb_kel($dbs, $data);

		$data['data']  = $this->adk_dipa_model->validasi_ssb_referensi($data);
		$data['info']  = $this->adk_dipa_model->validasi_ssb_referensi($info);
		$data['bread'] = array('header'=>'Informasi Validasi SSB', 'subheader'=>'e-Revisi', 'sidebar'=>'sidebar-collapse');
		$data['view']  = "adk_dipa/v_matrik_ssb";
		$this->load->view('main/utama', $data);
	}

	private function pagu_realisasi($kdsatker, $rev_id, $dbs, $unit, $min=null) {
		$data = $this->adk_dipa_model->pagu_realisasi($kdsatker, $rev_id, $dbs, $unit);
		if (! $data['tabel']) show_404();

		$data['menu']  = $this->dsw_model->get_menu( $this->uri->segment(1) );
		$data['bread'] = array('header'=>'Pagu dan Realisasi', 'subheader'=>'Data', 'sidebar'=>'sidebar-collapse');
		$data['view']  = "adk_dipa/v_pagu_realisasi";
		$this->load->view('main/utama', $data);
	}

	private function r3hit_ds() {
		$dept = '054';
		$unit = '01';
		$this->fc->MemVar('RA', $dept.$unit);

		$query = $this->dbrvs->query("SELECT * FROM v_adk_dipa WHERE kddept='$dept' AND kdunit='$unit' AND kdsatker='019209'");
		foreach ($query->result() as $row) {
			echo "$row->kdsatker $row->revisike  -- ";
			$index = "$this->index AND kdsatker='$row->kdsatker'";
			$sele1 = "thang,kdjendok,kdsatker,kddept,kdunit,kdprogram,kdgiat,kdoutput,kdlokasi,kdkabkota,kddekon";
			$sele2 = "kdkppn,kdbeban,kdjnsban,kdctarik,register,kdakun,jumlah,paguphln,pagurmp,pagurkp,blokirphln,blokirrmp,blokirrkp,rphblokir";
			$qry_o = $this->dbrvs->query("SELECT $sele1, Floor(sum(volume)) vol FROM v_rekap WHERE $index AND rev_id='$row->revisike' GROUP BY $sele1");
			$qry_i = $this->dbrvs->query("SELECT $sele1, $sele2 FROM d_item WHERE kdsatker='$row->kdsatker' AND rev_id='$row->revisike'");
			$ds1   = $this->ds->hashing($qry_o->result_array(), $qry_i->result_array());
			echo "$ds1 <br>";
		}
	}

	public function cek_selisih($rev_id=null) {
		if ($rev_id == null) return;
		$p = explode('.', $rev_id);

		$tmp = $this->dbtmp->query("SELECT kdsatker, 0 DIPA, sum(jml_pagu) Revisi, 0 Selisih FROM v_rekap WHERE jenis='RA' AND kddept='$p[1]' AND kdunit='$p[2]' AND rev_id='$rev_id' GROUP BY 1")->result_array();
		$tmp = $this->fc->ToArr($tmp, 'kdsatker');		
		$tmp['TOTAL'] = array('kdsatker'=>'TOTAL', 'DIPA'=>0, 'Revisi'=>0, 'Selisih'=>0);

		foreach ($tmp as $v) 
			$tmp['TOTAL']['Revisi'] += $v['Revisi'];

		$arr = $this->dbrvs->query("SELECT kdsatker, concat(kdsatker,max(revisike)) kode FROM v_adk_dipa WHERE kdsatker ". $this->fc->InVar($tmp, 'kdsatker') ." GROUP BY 1")->result_array();
		$inv = "jenis='RA' AND kddept='$p[1]' AND kdunit='$p[2]' AND concat(kdsatker,rev_id) ". $this->fc->Invar($arr, 'kode');
		$rvs = $this->dbrvs->query("SELECT kdsatker, sum(jml_pagu) DIPA FROM v_rekap WHERE $inv GROUP BY 1")->result_array();

		foreach ($rvs as $v) 
			if (array_key_exists($v['kdsatker'], $tmp)) {
				$tmp[$v['kdsatker']]['DIPA']    = $v['DIPA'];
				$tmp[$v['kdsatker']]['Selisih'] = $tmp[$v['kdsatker']]['DIPA'] - $tmp[$v['kdsatker']]['Revisi'] ;

				$tmp['TOTAL']['DIPA']    += $tmp[$v['kdsatker']]['DIPA'];
				$tmp['TOTAL']['Revisi']  += $tmp[$v['kdsatker']]['Revisi'];
				$tmp['TOTAL']['Selisih'] += $tmp[$v['kdsatker']]['Selisih'];

				$tmp[$v['kdsatker']]['DIPA']    = number_format($tmp[$v['kdsatker']]['DIPA'], 0, ',', '.');
				$tmp[$v['kdsatker']]['Revisi']  = number_format($tmp[$v['kdsatker']]['Revisi'], 0, ',', '.');
				$tmp[$v['kdsatker']]['Selisih'] = number_format($tmp[$v['kdsatker']]['Selisih'], 0, ',', '.');
			}

		$tmp['TOTAL']['DIPA']    = number_format($tmp['TOTAL']['DIPA'], 0, ',', '.');
		$tmp['TOTAL']['Revisi']  = number_format($tmp['TOTAL']['Revisi'], 0, ',', '.');
		$tmp['TOTAL']['Selisih'] = number_format($tmp['TOTAL']['Selisih'], 0, ',', '.');
		echo $this->fc->browse($tmp);
	}

	function t3st() {
		$id = '2020.023.03.011'; //$id='02'; 
		$dir ='files/revisi';
		$dept='023'; $unit='03'; $satker='';
		$whr="kddept='$dept' AND kdunit='$unit' AND rev_id='$id'";
		$this->fc->MemVar("RA", "$dept$unit");

		// V_REKAP
			$col = "'RA' jenis, '$id' rev_id, thang, kdjendok, kddept, kdunit, kdsatker, '00' kdfungsi, '00' kdsfung, kdprogram, kdgiat, kdoutput, kdlokasi, kdkabkota, kddekon, kdsoutput, kdkmpnen, trim(kdskmpnen) kdskmpnen, kdkppn";
			$sel = "kdctarik, register, substr(kdakun,1,2) kdjenbel, kdakun, '0' kdikat";
			$whr = "WHERE $whr";
			$grp = "1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,22,23,24,25,26";

			$rkp = array();
			$sql = "SELECT $col, kdbeban, kdjnsban, $sel, '' kdblokir, 0 volume, '' volkmp, 0 jml_blokir, Sum(jumlah) jml_pagu FROM d_item $whr AND kdbeban NOT IN ('B','I') GROUP BY $grp";
			$qry = $this->dbtmp->query($sql);
			$rkp = array_merge_recursive($rkp, $qry->result_array());

			$sql = "SELECT $col, kdbeban, kdjnsban, $sel, '' kdblokir, 0 volume, '' volkmp, 0 jml_blokir, Sum(paguphln+pagurkp) jml_pagu FROM d_item $whr AND kdbeban IN ('B','I') GROUP BY $grp";
			$qry = $this->dbtmp->query($sql);
			$rkp = array_merge_recursive($rkp, $qry->result_array());

			$sql = "SELECT $col, 'C' kdbeban, '0' kdjnsban, $sel, '' kdblokir, 0 volume, '' volkmp, 0 jml_blokir, Sum(pagurmp) jml_pagu FROM d_item $whr AND kdbeban IN ('B','I') GROUP BY $grp";
			$qry = $this->dbtmp->query($sql);
			$rkp = array_merge_recursive($rkp, $qry->result_array());

			$sql = "SELECT $col, kdbeban, kdjnsban, $sel, '*' kdblokir, 0 volume, '' volkmp, Sum(blokirphln+blokirrmp+blokirrkp+rphblokir) jml_blokir, 0 jml_pagu FROM d_item $whr AND kdblokir='*' GROUP BY $grp";
			$qry = $this->dbtmp->query($sql);
			$rkp = array_merge_recursive($rkp, $qry->result_array());
			if (count($rkp) == 0) return 'Error Satker';

			$qry  = $this->dbref->query("SELECT kdgiat, kdfungsi, kdsfung FROM t_giat WHERE kddept='$dept' AND kdunit='$unit'");
			$keg  = $this->fc->ToArr($qry->result_array(), 'kdgiat');
			$qry  = $this->dbtmp->query("SELECT Concat(thang,kdsatker,kdgiat,kdoutput,kdlokasi,kdsoutput) kode, sum(volsout) volkeg FROM d_soutput $whr GROUP BY 1");
			$vkeg = $this->fc->ToArr($qry->result_array(), 'kode');
			$qry  = $this->dbtmp->query("SELECT Concat(thang,kdsatker,kdgiat,kdoutput,kdlokasi,kdsoutput,kdkmpnen) kode, kdtema volkmp FROM d_kmpnen $whr");
			$vkmp = $this->fc->ToArr($qry->result_array(), 'kode');

			$no = 0;
			foreach ($rkp as $row) {
				$kode = $row['thang'].$row['kdsatker'].$row['kdgiat'].$row['kdoutput'].$row['kdlokasi'].$row['kdsoutput'];
				if (array_key_exists($kode, $vkeg)) { $rkp[$no]['volume'] += $vkeg[$kode]['volkeg']; unset($vkeg[$kode]); }
				$kode = $row['thang'].$row['kdsatker'].$row['kdgiat'].$row['kdoutput'].$row['kdlokasi'].$row['kdsoutput'].$row['kdkmpnen'];
				if (array_key_exists($kode, $vkmp)) { $rkp[$no]['volkmp'] += trim($vkmp[$kode]['volkmp']); unset($vkmp[$kode]); }
				if (array_key_exists($row['kdgiat'], $keg)) {
					$rkp[$no]['kdfungsi'] = $keg[$row['kdgiat']]['kdfungsi'];
					$rkp[$no]['kdsfung']  = $keg[$row['kdgiat']]['kdsfung'];
				}
				if (strpos('kmp 001 002', $row['kdkmpnen'])) $rkp[$no]['kdikat'] = '1';
				$no++;
			}

	 
			$csv = "$dir/rekap.csv";
			$fop = fopen($csv, 'w');
			foreach ($rkp as $row)  fputcsv($fop, $row, '|', '#');
			fclose($fop);
			// echo $this->fc->browse($rkp); exit;

			$this->dbtmp->query("DELETE FROM v_rekap WHERE jenis='RA' AND kddept='$dept' AND kdunit='$unit' AND rev_id='$id'");
			$this->dbtmp->query("LOAD DATA LOCAL INFILE '$csv' INTO TABLE v_rekap FIELDS TERMINATED BY '|' OPTIONALLY ENCLOSED BY '#'");

			// unlink("$dir/rekap.csv");
	}

}
