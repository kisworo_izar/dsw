	<?php
class Djpb_dash_revisi extends CI_Controller {
	function __construct() {
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
	    $this->load->model('dsw_model');
	    $this->load->model('sb_survei_model');
	    $this->load->model('dash_model');
	}

	public function index() {
        if (! $this->session->userdata('isLoggedIn') ) redirect("login/show_login");
		$this->menu();
	}

	public function menu() {
		$data['d_rss'] = $this->dsw_model->get_rss();
		$data['notif'] = $this->sb_survei_model->get_notif();
		$data['jml'] 	= $this->dash_model->get_jml_revisi();
		$data['revisi'] = $this->dash_model->count_revisi();

		$data['bread'] = array('header'=>'Revisi Anggaran <b>'.$this->session->userdata('thang').'</b>', 'subheader'=>'Dashboard');
		$data['view']  = "dash/v_djpb_dashboard";
		$this->load->view('main/utama', $data);
	}

	public function dash_map() {
		$row = $this->dash_model->get_data();
		echo($row);
	}

	function tes(){
		$data['map']	= $this->dash_model->get_data();
		echo "<pre>";print_r($data['map']);

	}


	function cari() {
		if ($_POST['search'] == 'search') {
			$this->session->set_userdata('cari', $_POST['cari']);
		} else {
			$this->session->set_userdata('cari', '');
		}

		if ($_POST['nmfunction']=='dash') {
			$this->dash();
		} else {
			$this->notif_grid();
		}
	}


	public function sb_survei_dja_dash() {
		$data = $this->sb_survei_model->get_survei_data_dja();
		$data['bread'] = array('view'=>'sb_survei/v_sb_dja', 'header'=>'Standar Biaya', 'subheader'=>'Kirim Data');
		$this->load->view('main/utama', $data);
	}

}
