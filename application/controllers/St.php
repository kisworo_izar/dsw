<?php
class St extends CI_Controller {
	function __construct() 
	{
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
	}

	public function index() 
	{
		// require_once 'assets/plugins/htmlpurifier/HTMLPurifier.auto.php';
		// $config	  = HTMLPurifier_Config::createDefault();
		// $purifier = new HTMLPurifier($config);

		if (! $this->session->userdata('isLoggedIn') ) redirect("login/show_login");
		$this->fc->log('Presensi Pegawai');

		if (isset($_GET['q']))  	$menu   = $_GET['q'];  		else show_404();
		if (isset($_GET['st_id']))  $st_id  = $_GET['st_id'];	else $st_id  = '';

		if 		(preg_match("~\bknxY3\b~", $menu))	$this->main();
		else if (preg_match("~\b2FZTv\b~", $menu))	$this->create();
		else if (preg_match("~\bCxtYc\b~", $menu))	$this->edit($st_id);
		else show_404();
	}

	private function main()
	{
		$this->fc->log('ST - Main');

		$data['menu']  = $this->dsw_model->get_menu( $this->uri->segment(1) );
		$data['bread'] = array('header'=>'Surat Tugas', 'subheader'=>'Surat Tugas');
		$data['view']  = "st/st_main";

		$this->load->view('main/utama', $data);
	}

	private function create()
	{
		$this->fc->log('ST - Create');

		$data['st_id'] = false;

		$data['menu']  = $this->dsw_model->get_menu( $this->uri->segment(1) );
		$data['bread'] = array('header'=>'Surat Tugas Baru', 'subheader'=>'Surat Tugas');
		$data['view']  = "st/st_form";

		$this->load->view('main/utama', $data);
	}

	private function edit($st_id)
	{
		$this->fc->log('ST - Edit');

		$data['st_id'] = $st_id;

		$data['menu']  = $this->dsw_model->get_menu( $this->uri->segment(1) );
		$data['bread'] = array('header'=>'Ubah Surat Tugas #'.$st_id, 'subheader'=>'Surat Tugas');
		$data['view']  = "st/st_form";

		$this->load->view('main/utama', $data);
	}
	
}