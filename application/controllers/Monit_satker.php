<?php
class Monit_satker extends CI_Controller {
	
	function __construct() {
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
		$this->load->model('monit_satker_model');
		$this->load->model('dsw_model');
		$this->load->model('adk_dipa_model');
		$this->session->set_userdata( array('cari' => '') );
	    $this->fc->MemVar();
		$this->list = array('d_output','d_soutput','d_kmpnen','d_skmpnen','d_akun','d_item','d_pdpt','d_cttakun');
	}

	function index() {
		//echo $this->thang;exit;
		if (! $this->session->userdata('isLoggedIn') ) redirect("login/show_login");
		// if (! $this->fc->check_akses('adk_dipa?q=0c2d2') ) show_404();

		if (isset($_GET['q']))		   $menu  = $_GET['q']; 	else show_404();
		if (isset($_GET['satker']))    $satk  = $_GET['satker'];
		if (isset($_GET['id'])) 	   $id    = $_GET['id'];
		if (isset($_GET['rev_id']))   $rev_id = $_GET['rev_id'];
		if (isset($_GET['ke'])) 	   $mula  = $_GET['ke'];    else $mula = '00';
		if (isset($_GET['unit'])) 	   $unit  = $_GET['unit'];  else $unit = '00000';
		if (isset($_GET['start']))     $start = $_GET['start']; else $start= date("01-01-$this->thang");
		if (isset($_GET['end'])) 	   $end   = $_GET['end'];   else $end  = date("d-m-$this->thang");
		if (isset($_GET['per_page']))  $page  = (int)$_GET['per_page']; else $page = null;
		$this->fc->MemVar('RA', $unit);
		$this->page = $page;

		if (isset($_POST['search'])) {
			if ($_POST['search'] == 'search') $this->session->set_userdata('cari', strtolower($_POST['cari']));
			else $this->session->set_userdata('cari', '');
		}

		//if 	    ($menu == 'caa29') $this->matrik_satker($satk, $id);
		if ($menu == 'caa29') $this->matrik_satker($satk, $this->dbrvs, $mula, $this->dbtmp, $id);
		else if ($menu == '45007') $this->kertas_kerja($satk, $id, "temp$this->thang");
		else if ($menu == 'ea2c2') $this->ssb_info($satk, $id);
		else if ($menu == 't1k3t') $this->adk_per_tiket($rev_id);

		else show_404();
	}

	// private function matrik_satker($kdsatker, $id) {
	// 	$data['tabel'] = $this->adk_dipa_model->matrik_semula_menjadi($kdsatker, '00', $id);
	// 	$data['menu']  = $this->dsw_model->get_menu( $this->uri->segment(1) );
	// 	$data['bread'] = array('header'=>'Matrik Semula Menjadi', 'subheader'=>'e-Revisi', 'sidebar'=>'sidebar-collapse');
	// 	$data['view']  = "adk_dipa/v_matrik_semula_menjadi";
	// 	$this->load->view('main/utama', $data);
	// }


	public function adk_per_tiket( $rvid ) {
		$this->fc->log('Tabel ADK');
		$data = $this->monit_satker_model->adk_per_tiket( $rvid );
		$data['bread'] = array('header'=>'Data ADK per Tiket', 'subheader'=>'e-Revisi', 'view'=>'monit/v_adk_per_tiket');
		// echo '<pre>'; print_r($data); exit;
		$this->load->view('main/utama', $data);
	}

	private function matrik_satker($kdsatker, $dbmula, $idmula, $dbjadi, $idjadi) {
		$data['revid'] = array('idmula'=>$idmula, 'idjadi'=>$idjadi);
		$data['tabel'] = $this->adk_dipa_model->matrik_semula_menjadi($kdsatker, $dbmula, $idmula, $dbjadi, $idjadi);

		if (! $data['tabel']) {
			$dbjadi  = $this->load->database("backup$this->thang", TRUE);
			$data['tabel'] = $this->adk_dipa_model->matrik_semula_menjadi($kdsatker, $dbmula, $idmula, $dbjadi, $idjadi);
			if (! $data['tabel']) show_404();
		}

		$data['menu']  = $this->dsw_model->get_menu( $this->uri->segment(1) );
		$data['bread'] = array('header'=>'Matrik Semula Menjadi', 'subheader'=>'e-Revisi', 'sidebar'=>'sidebar-collapse');
		$data['view']  = "adk_dipa/v_matrik_semula_menjadi";
		// echo "<pre>";print_r($data['tabel']);exit();
		$this->load->view('main/utama', $data);
	}

	private function kertas_kerja($kode, $id, $db, $unit=null) {
		if ($unit != null) { $this->dept = substr($unit,0,3); $this->unit = substr($unit,3,2); }
		$levl = 'giat'; if (strlen($kode) == 8) $levl = 'output';  if (strlen($kode) == 6) $levl = 'satker';
		$data = $this->adk_dipa_model->kertas_kerja($levl, $kode, $id, $db);
		
		if (! $data['tabel']) {
			$db   = "backup$this->thang";
			$data = $this->adk_dipa_model->kertas_kerja($levl, $kode, $id, $db);
			if (! $data['tabel']) show_404();
		}

		$data['level'] = $levl;
		$data['menu']  = $this->dsw_model->get_menu( $this->uri->segment(1) );
		$data['bread'] = array('header'=>'Kertas Kerja', 'subheader'=>'Data', 'sidebar'=>'sidebar-collapse');
		$data['view']  = "adk_dipa/v_kertas_kerja";
		$this->load->view('main/utama', $data);
	}

	private function ssb_info($satker, $rev_id) {
		$data = $this->adk_dipa_model->validasi_ssb($satker, $rev_id, 'v_rekap');
		if (count($data) == 0) show_404();
		$info = $this->adk_dipa_model->validasi_ssb_kel($data);

		$data['data']  = $this->adk_dipa_model->validasi_ssb_referensi($data);
		$data['info']  = $this->adk_dipa_model->validasi_ssb_referensi($info);
		$data['bread'] = array('header'=>'Informasi Validasi SSB', 'subheader'=>'e-Revisi', 'sidebar'=>'sidebar-collapse');
		$data['view']  = "adk_dipa/v_matrik_ssb";
		$this->load->view('main/utama', $data);
	}



	function test() {
	}
}
