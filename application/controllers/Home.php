<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

	function __construct() {
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
        $this->load->model('dsw_model');
    	$this->load->model('forum_model');
		$this->load->model('notif_model');
		$this->load->model('m_absensi');
		$this->milea = $this->load->database('milea',TRUE);
	}

	// function index() {
	// 	$this->fc->log('Revisi Kanwil');
    //     if (! $this->session->userdata('isLoggedIn') ) redirect("login/show_login");
	// 	if ($this->session->userdata('idusergroup') == '600' || $this->session->userdata('idusergroup') == '601') {
	// 		redirect('/sb_survei');
	// 	} elseif ($this->session->userdata('idusergroup') == '610' || $this->session->userdata('idusergroup') == '611' || $this->session->userdata('idusergroup') == '612') {
	// 		redirect('/djpb_dash_revisi');
	// 	} else {
	// 		$this->menu();
	// 	}
    //     if (isset($_GET['q'])) $menu = $_GET['q']; else show_404();
    //     $month 	  = date('m')-'1';
	// 	$hr       = date('d')-'1';
	// 	$thn 	  = date('Y');
		
    //     if (isset($_GET['rev_id'])) 	$rev_id = $_GET['rev_id'];
    //     if (isset($_POST['rev_id'])) 	$rev_id = $_POST['rev_id'];
    //     if (isset($_GET['unit'])) 	 	$unit   = $_GET['unit']; 			else $unit 	= '00000';
	// 	if (isset($_GET['start']))  	$start	= $_GET['start']; 			elseif($this->session->userdata('idusergroup') == 'admin') $start = date("$hr-m-$thn"); else $start = date("d-$month-$thn");
	// 	if (isset($_GET['end'])) 		$end  	= $_GET['end'];   			else $end   = date("d-m-$thn");

    //     if (isset($_POST['search'])) {
	// 		if ($_POST['search'] == 'search') $this->session->set_userdata('cari', strtolower($_POST['cari']));
	// 		else $this->session->set_userdata('cari', '');
	// 	}


		

	// 	if		($menu == 'rk4w1l') $this->dash();
	// 	else if ($menu == 'rk4w1l2a') $this->form2($rev_id);
	// 	else if ($menu == 'rk4w1l2b') $this->form2_proses($rev_id);
	// 	else if ($menu == 'rk4w1l2c') $this->crud_form2($rev_id);
	// 	else if ($menu == 'rk4w1l6a') $this->form6($rev_id);
	// 	else if ($menu == 'rk4w1l6b') $this->form6_proses($rev_id);
	// 	else if ($menu == 'rk4w1l6c') $this->crud_form6($rev_id);
	// 	else if ($menu == 'rk4w1l7a') $this->form7($rev_id);
	// 	else if ($menu == 'rk4w1l7b') $this->form7_proses($rev_id);
	// 	else if ($menu == 'rk4w1l7c') $this->crud_form7($rev_id);
		
	// 	else show_404();
	// }

	public function index() {
		if (! $this->session->userdata('isLoggedIn') ) redirect("login/show_login");

		if ($this->session->userdata('idusergroup') == '600' || $this->session->userdata('idusergroup') == '601') {
			redirect('/sb_survei');
		} 
		if ($this->session->userdata('idusergroup') == '610' || $this->session->userdata('idusergroup') == '611' || $this->session->userdata('idusergroup') == '612') {
			redirect('/djpb_dash_revisi');
		}
		if (isset($_GET['q'])) $menu = $_GET['q']; else show_404();
		if (isset($_GET['per_page']))  $page 	 = (int)$_GET['per_page']; else $page = 0;


		if		($menu == 'RrPW9') $this->menu();
		else if ($menu == 'Gag2c') $this->pengumuman($page);
		
		// else if ($menu == 'rk4w1l2b') $this->form2_proses($rev_id);
		// else if ($menu == 'rk4w1l2c') $this->crud_form2($rev_id);
		// else if ($menu == 'rk4w1l6a') $this->form6($rev_id);
		// else if ($menu == 'rk4w1l6b') $this->form6_proses($rev_id);
		// else if ($menu == 'rk4w1l6c') $this->crud_form6($rev_id);
		// else if ($menu == 'rk4w1l7a') $this->form7($rev_id);
		// else if ($menu == 'rk4w1l7b') $this->form7_proses($rev_id);
		// else if ($menu == 'rk4w1l7c') $this->crud_form7($rev_id);
		
		else show_404();
	}


	private function menu() {
		$data['d_rss']      = $this->dsw_model->get_rss();
		$data['d_news']     = $this->dsw_model->get_news();
		$data['absensi']    = $this->dsw_model->get_absen();
		$data['slideshow'] 	= $this->dsw_model->get_slideshow();
		$data['pengumuman'] = $this->dsw_model->get_pengumuman();
		$data['idparent'] 	= $this->dsw_model->sql_forum('idparent');
		$data['idchild']	= $this->dsw_model->sql_forum('idchild');
		$data['d_post']		= $this->forum_model->post_reply($data['idparent'], $data['idchild']);
		$data['quote'] 		= null;
		$data['url'] 		= "home/menu";
		$start = 0; if ( count($data['d_post'])>3 ) $start = count($data['d_post'])-3;
		$data['d_post']		= array_slice($data['d_post'], $start, 3);
		$data['menu']  		= $this->dsw_model->get_menu( $this->uri->segment(1) );
		$data['bread'] 		= array('header'=>'Dashboard', 'subheader'=>'Dashboard');
		$data['view']  		= "main/dashboard";

		$this->load->view('main/utama', $data);
	}

	private function pengumuman($page) {
		$this->load->model('Pengumuman_model');
		$row = $this->Pengumuman_model->get_data();
		// $data['pengumuman'] = array_slice($row, $this->uri->segment(3), 10);

		$config = $this->fc->pagination( site_url("home?q=Gag2c"), count($row), 10, '3');
		$this->pagination->initialize($config);

		$config = $this->fc->pagination( site_url("home?q=Gag2c"), count($row), 10, '3');
  		$config['page_query_string'] = TRUE;
  		$this->pagination->initialize($config);

  		$data['pengumuman'] = array_slice($row, (int)$page, 10);

		$data['menu'] = $this->dsw_model->get_menu( $this->uri->segment(1) );
		$data['bread']= array('header'=>'Arsip Pengumuman', 'subheader'=>'Pengumuman');
		$data['view'] = "pengumuman/V_pengumuman_list";
		$this->load->view('main/utama', $data);
	}

	public function tanggapan() {
		$idparent = $this->uri->segment(3);
		$idchild  = $this->uri->segment(4);
		$iduser	  = $this->session->userdata('iduser');
		$comment  = $_POST['comment'];
		$nmfile   = $_POST['nmfile'];

		$query = $this->db->query("insert into d_forum (idparent,idchild,iduser,tglpost,post,attach) values ($idparent,$idchild,'$iduser',current_timestamp(),\"$comment\",\"$nmfile\")");
		redirect('home');
	}

	public function fileupload() {
		if (empty($_FILES['files'])) {
		    echo json_encode( array('error'=>'No files found for upload.') );
		    return;
		}

		$images = $_FILES['files'];
		$success = null;
		$paths = array();
		$filenames = $images['name'];

		// loop and process files
		for($i=0; $i < count($filenames); $i++){
		    $ext = explode('.', basename($filenames[$i]));
		    $target = "files/forum" . DIRECTORY_SEPARATOR . $filenames[$i];
		    if(move_uploaded_file($images['tmp_name'][$i], $target)) {
		        $success = true;
		        array_push($paths, $target);
		    } else {
		        $success = false;
		        break;
		    }
		}

		// check and process based on successful status
		if ($success === true) {
		    $output = array('path' => $paths);
		} elseif ($success === false) {
		    $output = array('error'=>'Error while uploading Excel File. Contact the system administrator');
		    foreach ($paths as $file) {
		        unlink($file);
		    }
		} else {
		    $output = array('error'=>'No files were processed.');
		}
		echo json_encode($output);
 	}

	public function notif() {
		$data['notif'] = $this->notif_model->get_notif();
		$this->load->view('main/notifikasi', $data);
	}

	public function notif_read() {
		$nilai = $_POST['nilai'];
		$this->notif_model->notif_read( $nilai );
	}

	public function notif_clear() {
		$nilai = $_POST['nilai'];
		$this->notif_model->notif_clear( $nilai );
	}

	public function notif_history() {
		$data['notifs'] = $this->notif_model->get_notif_history();

		$data['menu']  = $this->dsw_model->get_menu( $this->uri->segment(1) );
		$data['bread'] = array('header'=>'Dashboard', 'subheader'=>'Notifikasi');
		$data['view']  = "main/notifikasi_history";

		$this->load->view('main/utama', $data);
	}

	public function test() {
	    $query = $this->db->query("Select * From d_forum_room Where idchild=104 ");
	    $d_room= $query->row_array();
		echo '<pre>'; print_r($d_room);
		echo '<br> iduser : '. $this->session->userdata('iduser');
		echo '<br> pic : '. $d_room['pic'];
		// $this->fc->browse( $data['d_forum'] );
	}

}
