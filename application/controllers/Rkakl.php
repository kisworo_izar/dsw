<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Rkakl extends CI_Controller {

	function __construct() {
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
        $this->load->model('dsw_model');
        $this->load->model('m_sanding_ref');
	}

	public function index() {
        if (! $this->session->userdata('isLoggedIn') ) redirect("login/show_login");
		$this->sanding_giat( );
	}

	public function sanding_output() {
		$data = $this->m_sanding_ref->get_output();
		$data['menu'] = $this->dsw_model->get_menu( $this->uri->segment(1) );
		$data['bread']= array('header'=>'Referensi Output', 'subheader'=>'Referensi RKAKL');
		$data['view'] = "rkakl/referensi/sanding_rka_cw_output1";
		$this->load->view('main/utama', $data);
	}

	public function sanding_giat() {
		$data = $this->m_sanding_ref->get_giat();
		$data['menu'] = $this->dsw_model->get_menu( $this->uri->segment(1) );
		$data['bread']= array('header'=>'Referensi Kegiatan', 'subheader'=>'Referensi RKAKL');
		$data['view'] = "rkakl/referensi/sanding_rka_cw_giat";
		$this->load->view('main/utama', $data);
	}

	public function sanding_satker() {
		ini_set('memory_limit', '2048M');
		$data = $this->m_sanding_ref->get_satker();
		$data['menu'] = $this->dsw_model->get_menu( $this->uri->segment(1) );
		$data['bread']= array('header'=>'Referensi Satker', 'subheader'=>'Referensi RKAKL');
		$data['view'] = "rkakl/referensi/sanding_rka_cw_satker";
		$this->load->view('main/utama', $data);
	}

	function test() {
		$dt = $this->m_sanding_ref->get_output( );
		echo '<pre>'; print_r($dt);
		// $this->fc->browse( $dt );

	}

}
