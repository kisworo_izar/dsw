<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tree extends CI_Controller {

	function __construct() {
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
	}

	public function index() {
		// Query ESELON 2 - Direktorat
		$query  = $this->db->query("Select concat('idx',kdso,'1') kdkey, Left(kdso,2) es2, Concat(Left(kdso,2),'00') es3, kdso, nmso, '' gol, 1 level From t_so Where Left(kdso,2)!='00' And Right(kdso,4)='0000' Order By 1");
		$eslon2 = $query->result_array();

		// Query ESELON 3 - Sub Direktorat
		$query  = $this->db->query("Select concat('idx',kdso,'2') kdkey, Left(kdso,2) es2, Substr(kdso,1,4) es3, kdso, nmso, '' gol, 2 level From t_so Where Right(kdso,4)!='0000' And Right(kdso,2)='00'");
		$eslon3 = $query->result_array();

		// Merge Array
		$dja = array_merge_recursive($eslon2, $eslon3);
		$data['dja']  = $this->fc->array_index( array_merge_recursive($eslon2, $eslon3), 'kdkey' );

		$data['menu']  = $this->dsw_model->get_menu( $this->uri->segment(1) );
		$data['bread'] = array('header'=>'Lampiran Surat Tugas','subheader'=>'ST Online');
		$data['view']  = "gallery/v_tree";
		$this->load->view('main/utama', $data);
	}

	public function ajax_eselon3() {
		// Query USER - Sub Direktorat
		$idkey = $_POST['idkey'];
		$query = $this->db->query("Select nmuser, nip, jabatan, golongan, kdso, kdeselon From t_user Where Left(kdso,4)='$idkey' Order By kdso,kdeselon,nmuser");

		$msg = '';
		foreach ($query->result_array() as $row) {
			$jab = 'jab' . $row['kdeselon'];
			$msg .= '
				<tr class="trPeg" id="'. $row['nip'] .'" align="justify" />
					<td class="tdPeg"></td>
					<td class="NmPeg '. $jab .'">'. $row['nmuser'] .'</td>
					<td class="tdPeg '. $jab .' text-center">'. $row['nip'] .'</td>
					<td class="tdPeg '. $jab .'">'. $row['jabatan'] .'</td>
					<td class="tdPeg '. $jab .' text-center">'. $row['golongan'] .'</td>
					<td class="tdPeg '. $jab .'"></td>
					<td class="tdPeg"><input type="checkbox" name="check1"></td>
				</tr>';
		}
		echo $msg; 
	}

	public function test() {
		$query = $this->db->query("Select nmuser, nip, jabatan, golongan, kdso, kdeselon  From t_user Where Left(kdso,4)='0704' Order By kdso,kdeselon,nmuser");
		$hasil = $query->result_array();
		$this->fc->browse( $hasil );
	}

}
