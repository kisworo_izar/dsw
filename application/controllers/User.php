<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Controller {

	function __construct() {
		parent::__construct();
        $this->load->model('user_model');
        $this->load->model('info_model');
	}

	public function index() {
		$this->fc->log('Admin - Manajemen User');
		$row = $this->user_model->get_data();
		$config = $this->fc->pagination( site_url("user/index"), count($row), 15, '3' );
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		$this->pagination->initialize($config);

		$data['menu'] = $this->info_model->get_menu();
		$data['side'] = $this->info_model->get_sidebar('900');
		$data['data']  = array_slice($row, $this->uri->segment(3), 15);
		$data['kdso']  = $this->user_model->get_kdso();
		$data['level'] = $this->user_model->get_level();
		$data['bread'] = array('header'=>'Manajemen User', 'subheader'=>'Data User');
		$data['view']  = "admin/user_grid";
		$this->load->view('main/utama', $data);
	}

	function cari() {
		$persh = $_POST['nmuser'];  if ($_POST['cari']<>'Cari') $persh = '';
		$this->param = $this->mgm->param( $_POST['nilai'] ."persh:$persh" );
		$this->grid();
	}

	function save() {
		$this->user_model->save( $_POST['simpan'] );
		redirect("user");
	}

}
