<?php
class Milea extends CI_Controller {
	function __construct() {
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
	    $this->load->model('milea_model');
	    $this->load->model('milea_laporan_model');
	    $this->dbmilea  = $this->load->database('milea', TRUE);
	}

	public function index() {
    	if (! $this->session->userdata('isLoggedIn') ) redirect("login/show_login");
		$this->manual();
	}

	function manual($nip=null, $start=null, $end=null) {
		$this->fc->log('Presensi - Pengajuan');
		$nip = $this->session->userdata('nip');
		$data['data_izin'] = $this->milea_model->get_absen_manual( $nip );
		$data['data_peg']  = $this->milea_model->get_data_pegawai($nip);
		$data['hdr']       = $this->milea_model->hdr();
		$data['krj']       = $this->milea_model->krj();

		$data['menu']  = $this->dsw_model->get_menu( $this->uri->segment(1) );
		$data['bread'] = array('header'=>'Pengajuan Ijin', 'subheader'=>'Presensi');
		$data['view']  = "absensi/v_absen_manual";
		$this->load->view('main/utama', $data);
	}

	function fileupload() {
		$path = "files/upload_milea/". $_POST['nip'];
		$old  = umask(0);
		is_dir($path) || mkdir($path, 0777, true);
		umask($old);

		$file = $_FILES[ $_POST['name'] ];
		$resp = array('uploaded' => 'Gagal');
		if ($file) {
			move_uploaded_file($file["tmp_name"], $path.'/'.$_POST['tgl'].'_'.$file['name']);
			$resp = array('file'=>$file['name']);
		}
		echo json_encode($resp);
 	}

 	function crud(){
		$this->milea_model->save();
	}

	function del($id,$nip){
		$this->milea_model->del($id);
		redirect(site_url('milea/manual/'.$nip));
	}

	function laporan($es=null, $subdit=null, $seksi=null, $start=null, $end=null){
		$this->fc->log('Presensi - Laporan');
		$usr 		= $this->session->userdata('iduser');
		$idusr  = $this->session->userdata('idusergroup');
		$arr    = $this->db->query("SELECT kodeorganisasi FROM t_user a LEFT JOIN t_so b ON a.kdso=b.kdso WHERE iduser='$usr'")->row_array();
		$grp 		= explode(';', $idusr);

		if ($this->uri->segment(3)==null) {
			if( in_array('002', $grp)  or substr($arr['kodeorganisasi'], 0,6)=='350401' ) { $es = '000'; } else { $es = substr($arr['kodeorganisasi'],0,6); }
		}

		if ($subdit==null) $subdit  = '000';
		if ($seksi==null)  $seksi  	= '000';
		if ($start==null) { $start 	= date('01-m-Y'); $end = date('d-m-Y'); }

		$data['pil'] = $this->milea_laporan_model->get_menu($es, $subdit, $seksi, $start, $end);
		if ($this->uri->segment(3) != null) 	 	$data['pil']['pil2'] = $es;     else $es     = $data['pil']['pil2'];
		if ($subdit != null) $data['pil']['pil3'] = $subdit; else $subdit = $data['pil']['pil3'];
		if ($seksi != null)  $data['pil']['pil4'] = $seksi;  else $seksi  = $data['pil']['pil4'];
		$data['menu']  = $this->dsw_model->get_menu( $this->uri->segment(1) );
		$data['bread'] = array('header'=>'Laporan Presensi', 'subheader'=>'Presensi');
		$data['view']  = "absensi/v_laporan";
		$this->load->view('main/utama', $data);
	}

	function lap_01($es=null,$subdit=null,$seksi=null,$start=null,$end=null){
		$this->fc->log('Presensi - Laporan 01');
		if ($es==null)     $es 		= '000';
		if ($subdit==null) $subdit  = '000';
		if ($seksi==null)  $seksi   = '000';
		if ($start==null) { $start = date('01-m-Y'); $end = date('d-m-Y'); }

		$data['peg']   = $this->milea_laporan_model->get_data($es,$subdit,$seksi, $this->fc->ustgl($start), $this->fc->ustgl($end) );
		$data['pil']   = $this->milea_laporan_model->get_pil($es,$subdit,$seksi);
		if($es=='1') $data['pil']['es2']['namaorganisasi'] = 'Dirjen Anggaran';
		if($es=='000') $data['pil']['es2']['namaorganisasi'] = 'Semua Direktorat';
		if($subdit=='000') $data['pil']['es3']['namaorganisasi'] = 'Semua Eselon 3';
		if($seksi=='000') $data['pil']['es4']['namaorganisasi'] = 'Semua Eselon 4';
		$data['start'] = $start;
		$data['end']   = $end;
		$data['menu']  = $this->dsw_model->get_menu( $this->uri->segment(1) );
		$data['bread'] = array('header'=>'Laporan 01', 'subheader'=>'Presensi');
		$data['view']  = "absensi/v_laporan_01";
		$this->load->view('main/utama', $data);
	}

	function lap_02($es=null,$subdit=null,$seksi=null,$start=null,$end=null){
		$this->fc->log('Presensi - Laporan 02');
		if ($es==null)     $es 		= '000';
		if ($subdit==null) $subdit  = '000';
		if ($seksi==null)  $seksi   = '000';
		if ($start==null) { $start = date('01-m-Y'); $end = date('d-m-Y'); }
		$data['trt'] = $this->milea_laporan_model->get_ketertiban($es, $subdit, $seksi, $this->fc->ustgl($start), $this->fc->ustgl($end) );
		$data['pil']   = $this->milea_laporan_model->get_pil($es,$subdit,$seksi);
		if($es=='1') $data['pil']['es2']['namaorganisasi'] = 'Dirjen Anggaran';
		if($es=='000') $data['pil']['es2']['namaorganisasi'] = 'Semua Direktorat';
		if($subdit=='000') $data['pil']['es3']['namaorganisasi'] = 'Semua Eselon 3';
		if($seksi=='000') $data['pil']['es4']['namaorganisasi'] = 'Semua Eselon 4';
		$data['start'] = $start;
		$data['end']   = $end;
		$data['menu']  = $this->dsw_model->get_menu( $this->uri->segment(1) );
		$data['bread'] = array('header'=>'Laporan 02', 'subheader'=>'Presensi');
		$data['view']  = "absensi/v_laporan_02";
		$this->load->view('main/utama', $data);
	}

	function lap_03($es=null,$subdit=null,$seksi=null,$start=null,$end=null){
		$this->fc->log('Presensi - Laporan 03');
		if ($es==null)     $es 		= '000';
		if ($subdit==null) $subdit  = '000';
		if ($seksi==null)  $seksi   = '000';
		if ($start==null) { $start = date('01-m-Y'); $end = date('d-m-Y'); }

		$data['plg'] = $this->milea_laporan_model->get_pelanggaran($es, $subdit, $seksi, $this->fc->ustgl($start), $this->fc->ustgl($end) );
		// echo "<pre>";print_r($data['plg']);exit();
		$data['pil']   = $this->milea_laporan_model->get_pil($es,$subdit,$seksi);
		if($es=='1') $data['pil']['es2']['namaorganisasi'] = 'Dirjen Anggaran';
		if($es=='000') $data['pil']['es2']['namaorganisasi'] = 'Semua Direktorat';
		if($subdit=='000') $data['pil']['es3']['namaorganisasi'] = 'Semua Eselon 3';
		if($seksi=='000') $data['pil']['es4']['namaorganisasi'] = 'Semua Eselon 4';
		$data['start'] = $start;
		$data['end']   = $end;
		$data['menu']  = $this->dsw_model->get_menu( $this->uri->segment(1) );
		$data['bread'] = array('header'=>'Laporan 03', 'subheader'=>'Presensi');
		$data['view']  = "absensi/v_laporan_03";
		$this->load->view('main/utama', $data);
	}

	function lap_04($es=null,$subdit=null,$seksi=null,$start=null,$end=null){
		$this->fc->log('Presensi - Laporan 04');
		if ($es==null)     $es 		= '000';
		if ($subdit==null) $subdit  = '000';
		if ($seksi==null)  $seksi   = '000';
		if ($start==null) { $start = date('01-m-Y'); $end = date('d-m-Y'); }

		$data['mkn'] = $this->milea_laporan_model->get_makan($es, $subdit, $seksi, $this->fc->ustgl($start), $this->fc->ustgl($end) );
		$data['pil']   = $this->milea_laporan_model->get_pil($es,$subdit,$seksi);
		if($es=='1') $data['pil']['es2']['namaorganisasi'] = 'Dirjen Anggaran';
		if($es=='000') $data['pil']['es2']['namaorganisasi'] = 'Semua Direktorat';
		if($subdit=='000') $data['pil']['es3']['namaorganisasi'] = 'Semua Eselon 3';
		if($seksi=='000') $data['pil']['es4']['namaorganisasi'] = 'Semua Eselon 4';
		$data['start'] = $start;
		$data['end']   = $end;
		$data['menu']  = $this->dsw_model->get_menu( $this->uri->segment(1) );
		$data['bread'] = array('header'=>'Laporan 04', 'subheader'=>'Presensi');
		$data['view']  = "absensi/v_laporan_04";
		$this->load->view('main/utama', $data);
	}

	function lap_05($es=null,$subdit=null,$seksi=null,$start=null,$end=null){
		$this->fc->log('Presensi - Laporan 05');
		if ($es==null)     $es 		= '000';
		if ($subdit==null) $subdit  = '000';
		if ($seksi==null)  $seksi   = '000';
		if ($start==null) { $start = date('01-m-Y'); $end = date('d-m-Y'); }

		$data['lmb'] = $this->milea_laporan_model->get_lembur($es, $subdit, $seksi, $this->fc->ustgl($start), $this->fc->ustgl($end) );
		// echo"<pre>";print_r($data['lmb']);exit();
		$data['pil']   = $this->milea_laporan_model->get_pil($es,$subdit,$seksi);
		if($es=='1') $data['pil']['es2']['namaorganisasi'] = 'Dirjen Anggaran';
		if($es=='000') $data['pil']['es2']['namaorganisasi'] = 'Semua Direktorat';
		if($subdit=='000') $data['pil']['es3']['namaorganisasi'] = 'Semua Eselon 3';
		if($seksi=='000') $data['pil']['es4']['namaorganisasi'] = 'Semua Eselon 4';
		$data['start'] = $start;
		$data['end']   = $end;
		$data['menu']  = $this->dsw_model->get_menu( $this->uri->segment(1) );
		$data['bread'] = array('header'=>'Laporan 05', 'subheader'=>'Presensi');
		$data['view']  = "absensi/v_laporan_05";
		$this->load->view('main/utama', $data);

	}

	function tes(){
		 $id  = $this->session->userdata('idusergroup');
		 $arr = explode(';', $id);
		 var_dump(in_array('053', $arr));
		 echo"<pre>";print_r($arr);

	}


}
