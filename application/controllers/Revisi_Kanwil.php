<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Revisi_Kanwil extends CI_Controller {

	// private $dbrevisi;

	function __construct() {
		parent::__construct();
		$this->dbrevisi = $this->load->database('revisi'. $this->session->userdata('thang'), TRUE);
		date_default_timezone_set('Asia/Jakarta');
		$this->load->model('Revisi_kanwil_model');
        $this->load->model('dsw_model');
        $this->session->set_userdata(array('cari' => ''));
	}

	function index() {
		$this->fc->log('Revisi Kanwil');
        if (! $this->session->userdata('isLoggedIn') ) redirect("login/show_login");
        if (isset($_GET['q'])) $menu = $_GET['q']; else show_404();
        $month 	  = date('m')-'1';
		$hr       = date('d')-'1';
		$thn 	  = date('Y');

        if (isset($_GET['rev_id'])) 	$rev_id = $_GET['rev_id'];
        if (isset($_POST['rev_id'])) 	$rev_id = $_POST['rev_id'];
        if (isset($_GET['unit'])) 	 	$unit   = $_GET['unit']; 			else $unit 	= '00000';
		if (isset($_GET['start']))  	$start	= $_GET['start']; 			elseif($this->session->userdata('idusergroup') == 'admin') $start = date("$hr-m-$thn"); else $start = date("d-$month-$thn");
		if (isset($_GET['end'])) 		$end  	= $_GET['end'];   			else $end   = date("d-m-$thn");
		if (isset($_GET['sta'])) 		$sta = $_GET['sta'];
		if (isset($_GET['lvl'])) 		$lvl = $_GET['lvl'];

        if (isset($_POST['search'])) {
			if ($_POST['search'] == 'search') $this->session->set_userdata('cari', strtolower($_POST['cari']));
			else $this->session->set_userdata('cari', '');
		}


		

		if		($menu == 'rk4w1l') $this->dash();
		else if ($menu == 'rk4w1l2a') $this->form2($rev_id);
		else if ($menu == 'rk4w1l2b') $this->form2_proses($rev_id);
		else if ($menu == 'rk4w1l2c') $this->crud_form2($rev_id);
		else if ($menu == 'rk4w1l2d') $this->crud_form2_proses($rev_id);
		else if ($menu == 'rk4w1l6a') $this->form6($rev_id);
		else if ($menu == 'rk4w1l6b') $this->form6_proses($rev_id);
		else if ($menu == 'rk4w1l6c') $this->crud_form6($rev_id);
		else if ($menu == 'rk4w1l6d') $this->crud_form6_proses($rev_id);
		else if ($menu == 'rk4w1l7a') $this->form7($rev_id);
		else if ($menu == 'rk4w1l7b') $this->form7_proses($rev_id);
		else if ($menu == 'rk4w1l7c') $this->crud_form7($rev_id);
		else if ($menu == 'rk4w1l7d') $this->crud_form7_proses($rev_id);
		else if ($menu == 'rk4w1lup') $this->fileupload_form7();
		else if ($menu == 'r3p0rtkw1') $this->reportkwl($sta,$end);
		else if ($menu == 'm4nd4r')    $this->mundur_status($lvl,$rev_id);
		
		else show_404();
	}

	private function dash( $depuni=null, $start=null, $end=null ) {
		$cari = $this->session->userdata('cari');
		$adm 	  = $this->session->userdata('idusergroup');
		$month 	  = date('m')-'1';
		$hr       = date('d');
		$tahun 	  = date('y');
		if (strpos($adm,'002')) $cek = 1; else $cek = 0;

		if ($cek !=1 and $start == null) { 
			$start = date("01-$month-$tahun"); $end = date("d-m-$tahun"); 
		} elseif($cek==1 and $start == null){
			$start = date("$hr-m-$tahun"); $end = date("d-m-$tahun"); 
		} else{ 
			//$start = $this->uri->segment(3); $end = $this->uri->segment(4);  
			$start = $start; $end=$end;
		}

		$data = $this->Revisi_kanwil_model->get_data( $depuni, $this->fc->ustgl($start), $this->fc->ustgl($end) , $tahun, $cari);

		$data['menu']  = $this->dsw_model->get_menu( $this->uri->segment(1) );
		$data['bread'] = array('header'=>'Proses Revisi Anggaran', 'subheader'=>'Revisi Kanwil');
		$data['view']  = "Revisi_Kanwil/v_dash";
		$this->load->view('main/utama', $data);
	}

	private function reportkwl($sta,$end) {
		// echo 'tess';exit;
		$tahun 	  = date('Y');
		$month 	  = date('m')-'1';
		// if ($this->uri->segment(3) == '') { $sta = date("01-$month-$tahun"); $end = date("d-m-$tahun"); } 
		// else { $sta = $this->uri->segment(3); $end = $this->uri->segment(4); }
		header("Content-type: application/vnd-ms-excel");
		header("Content-Disposition: attachment; filename=report_$sta.$end.xls");

		$data['dat'] 			= $this->Revisi_kanwil_model->reportKanwil($this->fc->ustgl($sta),$this->fc->ustgl($end) );
		$this->load->view('puslay/v_kwlreport', $data);

		// echo '<pre>';print_r($data);exit;
	}

	private function form2( $rev_id ) {
		$data['rev_id']= $rev_id;
		//$data['rev_id'] = $_POST['rev_id'];
		$data['menu']  = $this->dsw_model->get_menu( $this->uri->segment(1) );
		$data['bread'] = array('header'=>'&nbsp;', 'subheader'=>'Revisi Anggaran');
		$data['view']  = "Revisi_Kanwil/v_form2";
		$this->load->view('main/utama', $data);
	}

	private function crud_form2($rev_id) {
		
		$data['rev_id']= $rev_id;
		$ip	   = $this->fc->getUserIP();

		$this->dbrevisi->query( "Update revisi set t2_status =1, rev_tahap = 2, rev_status =1,  t2_proses_tgl=current_timestamp(), t2_proses_ip='$ip'
							where rev_id='$rev_id' " );
		
		$this->form2_proses($rev_id);
	}

	private function form2_proses( $rev_id ) {
		$data['rev_id']= $rev_id;
		$data['menu'] = $this->dsw_model->get_menu( $this->uri->segment(1) );
		$data['bread'] = array('header'=>'&nbsp;', 'subheader'=>'Revisi Anggaran');
		$data['proses'] = $this->Revisi_kanwil_model->get_form2( $rev_id );
		$data['view'] = "Revisi_Kanwil/v_form2_proses";
		$this->load->view('main/utama', $data);
	}

	private function crud_form2_proses() {

		$this->Revisi_kanwil_model->save_form2_proses();
			redirect("Revisi_Kanwil?q=rk4w1l");
	}

	public function adk_per_tiket( $rvid ) {
		//echo "masuk dia"; exit;
		//$this->fc->log('Tabel ADK');
		$data = $this->Revisi_kanwil_model->adk_per_tiket( $rvid );
		$data['bread'] = array('header'=>'Data ADK per Tiket', 'subheader'=>'e-Revisi');
		$data['view'] = "Revisi_Kanwil/v_adk_per_tiket";
		$this->load->view('main/utama', $data);
	}

	public function matrik_satker($rev_id, $kdsatker, $ke) {
		$data['tabel'] = $this->Revisi_kanwil_model->matrik_semula_menjadi($kdsatker, $ke, 'v_ds_pagu', $rev_id, 'v_ds_revisi');
		$data['bread'] = array('header'=>'Matrik Semula Menjadi', 'subheader'=>'e-Revisi', 'sidebar'=>'sidebar-collapse');
		$data['view']  = "Revisi_Kanwil/v_matrik_semula_menjadi";
		$this->load->view('main/utama', $data);
	}

	private function kertas_kerja($rev_id, $kdsatker) {
		$data = $this->adk_dipa_model->kertas_kerja($kdsatker, $rev_id, 'temporar');
		if (! $data['tabel']) show_404();

		$data['menu']  = $this->dsw_model->get_menu( $this->uri->segment(1) );
		$data['bread'] = array('header'=>'Kertas Kerja : '.$data['satkr'], 'subheader'=>'e-Revisi', 'sidebar'=>'sidebar-collapse');
		$data['view']  = "Revisi_Kanwil/v_kertas_kerja";
		$this->load->view('main/utama', $data);
	}

	private function ssb_info($rev_id, $kdsatker) {
		// $data = $this->monit_satker_model->json_ssb_info($rev_id, $kdsatker);
		$data = $this->adk_dipa_model->validasi_ssb($kdsatker, $rev_id, 'v_ds_revisi');
		if (count($data) == 0) show_404();
		$info = $this->adk_dipa_model->validasi_ssb_kel($data);

		$data['data']  = $this->adk_dipa_model->validasi_ssb_referensi($data);
		$data['info']  = $this->adk_dipa_model->validasi_ssb_referensi($info);
		$data['bread'] = array('header'=>'Informasi Validasi SSB', 'subheader'=>'e-Revisi', 'sidebar'=>'sidebar-collapse');
		$data['view']  = "adk_dipa/v_matrik_ssb";
		$this->load->view('main/utama', $data);
	}


	private function form6( $rev_id ) {
		// $data = $this->revisi_67_model->get_row( $rev_id );

		$data['rev_id']= $rev_id;
		$data['menu']  = $this->dsw_model->get_menu( $this->uri->segment(1) );
		$data['bread'] = array('header'=>'&nbsp;', 'subheader'=>'Revisi Anggaran');
		$data['view']  = "Revisi_Kanwil/v_form6";
		$this->load->view('main/utama', $data);
	}

	private function crud_form6() {

		$rev_id 	= $_POST['rev_id'];
		//$rev_id     = $this->uri->segment(3);
		$ip	   = $this->fc->get_client_ip();

		$this->dbrevisi->query( "Update revisi set t6_status =1, rev_tahap = 6, t6_proses_tgl=current_timestamp(), t6_proses_ip='$ip'
							where rev_id='$rev_id' " );

		$this->form6_proses($rev_id);
	}

	private  function form6_proses($rev_id) {
		$ruh  = 'Rekam'; if ($this->uri->segment(3)=='u') $ruh = 'Ubah';
		// $data = $this->revisi_67_model->get_row( $ruh );
		$data['rev_id']= $rev_id;
		$data['menu'] = $this->dsw_model->get_menu( $this->uri->segment(1) );
		$data['bread'] = array('header'=>'&nbsp;', 'subheader'=>'Revisi Anggaran');
		$data['proses'] = $this->Revisi_kanwil_model->get_form2( $rev_id );
		$data['view'] = "Revisi_Kanwil/v_form6_proses";
		$this->load->view('main/utama', $data);
	}

	private function crud_form6_proses() {
		$this->Revisi_kanwil_model->save_form6_proses();
		redirect("Revisi_Kanwil?q=rk4w1l");
	}

	private function form7( $rev_id ) {
		$data['rev_id']= $rev_id;
		$data['menu']  = $this->dsw_model->get_menu( $this->uri->segment(1) );
		$data['bread'] = array('header'=>'&nbsp;', 'subheader'=>'Revisi Anggaran');
		$data['view']  = "Revisi_Kanwil/v_form7";
		$this->load->view('main/utama', $data);
	}

	private function crud_form7($rev_id) {
		$rev_id 		= $_POST['rev_id'];
		$ip	  			= $this->fc->get_client_ip();

		$this->dbrevisi->query( "Update revisi set t7_status =1, rev_tahap = 7, rev_status=1,  t7_proses_tgl=current_timestamp(), t7_proses_ip='$ip' where rev_id='$rev_id' " );

		//redirect("Revisi_Kanwil/form7_proses/$rev_id");
		$this->form7_proses($rev_id);
	}

	private function form7_proses($rev_id) {
		$data['rev_id']= $rev_id;
		$data['menu'] = $this->dsw_model->get_menu( $this->uri->segment(1) );
		$data['bread'] = array('header'=>'&nbsp;', 'subheader'=>'Revisi Anggaran');
		$data['proses'] = $this->Revisi_kanwil_model->get_form2( $rev_id );
		$data['view'] = "Revisi_Kanwil/v_form7_proses_new";
		$this->load->view('main/utama', $data);
	}

	private function crud_form7_proses($rev_id) {

		$rev_id			= $_POST['rev_id'];
		$this->Revisi_kanwil_model->save_form7_proses();
			redirect("Revisi_Kanwil?q=rk4w1l");
		// $this->form7_proses($rev_id);
	}

	private function fileupload_form7() {
		$path = 'files/revisi_SatuDJA/' . $_POST['rev_id'];
		$old  = umask(0);
		is_dir($path) || mkdir($path, 0777, true);
		umask($old);

		$file = $_FILES[ $_POST['name'] ];
		$resp = array('uploaded' => 'Gagal');
		if ($file) {
			move_uploaded_file($file["tmp_name"], $path .'/'. $file['name']);
			$resp = array('uploaded' => $file['name']);
		}
		echo json_encode($resp);
 	}

	private function inputund(){
 		$data['menu'] = $this->dsw_model->get_menu( $this->uri->segment(1) );
		$data['bread'] = array('header'=>'&nbsp;', 'subheader'=>'Revisi Anggaran');
		$data['view'] = "revisi/v_surat_pengesahan";
		$this->load->view('main/utama', $data);
 	}

 	private function inputund_tolak(){
 		$data['menu'] = $this->dsw_model->get_menu( $this->uri->segment(1) );
		$data['bread'] = array('header'=>'&nbsp;', 'subheader'=>'Revisi Anggaran');
		$data['view'] = "revisi/v_surat_penolakan";
		$this->load->view('main/utama', $data);
 	}

 	private function pdf(){
 		$es=substr($this->session->userdata('kdso'),0,2) ; //echo
 		//$data['und']  = $this->revisi_23_model->get_und();
 		$data['dir']  = $this->stol_model->get_dir($es);
		$data['almt1']  = $this->stol_model->get_alm1($es);
		$data['almt2']  = $this->stol_model->get_alm2($es);
		$data['menu']  = $this->dsw_model->get_menu( $this->uri->segment(1) );
		//$data['bread'] = array('header'=>'Admin Surat Tugas', 'subheader'=>'Admin Surat Tugas');

		$data['view']  = "revisi/cetak_pengesahan";
		$this->load->view('revisi/cetak_pengesahan', $data);

		$html = $this->output->get_output();
		// Convert to PDF
		$this->dompdf->load_html($html);
		$this->dompdf->set_paper('A4','portrait');
		$this->dompdf->render();
		$this->dompdf->stream( 'Und'.'.pdf',array('Attachment'=>0) );
 	}

 	private function pdf_penolakan(){
 		$es=substr($this->session->userdata('kdso'),0,2) ; //echo
 		//$data['und']  = $this->revisi_23_model->get_und();
 		$data['dir']  = $this->stol_model->get_dir($es);
		$data['almt1']  = $this->stol_model->get_alm1($es);
		$data['almt2']  = $this->stol_model->get_alm2($es);
		$data['menu']  = $this->dsw_model->get_menu( $this->uri->segment(1) );
		//$data['bread'] = array('header'=>'Admin Surat Tugas', 'subheader'=>'Admin Surat Tugas');

		$data['view']  = "revisi/cetak_penolakan";
		$this->load->view('revisi/cetak_penolakan', $data);

		$html = $this->output->get_output();
		// Convert to PDF
		$this->dompdf->load_html($html);
		$this->dompdf->set_paper('A4','portrait');
		$this->dompdf->render();
		$this->dompdf->stream( 'Und'.'.pdf',array('Attachment'=>0) );
 	}


	private function mundur_status($lvl,$rev_id){
		$this->fc->logRevisi($rev_id, "Turun status ke - $lvl");
 		$data = $this->Revisi_kanwil_model->mundur_status($lvl,$rev_id);
		redirect("Revisi_Kanwil?q=rk4w1l");

 	}

 	private function create_zip_adk($rev_id){
		//$rev_id='2020.060.01.001';
		$nmfile = $rev_id.'.zip';
		$thang  = $this->session->userdata('thang');
		$jenis  = "ADK";
		$source = "files/revisi_SatuDJA/$rev_id/adk_satker/";
		$destination = "files/files_backup/$nmfile";

		$ipSource = "files/revisi_SatuDJA/$rev_id/adk_satker";
		$ipTarget = "files/files_backup/$rev_id";

		
		$this->Zip($source,$destination);
		$this->getall( $nmfile );
		$this->rrmdir("files/files_backup/$rev_id");

	}

	private function getall( $nmfile ) {
		$file = "files/files_backup/$nmfile";

		//echo "$file <br>";
		if (file_exists($file)) {
		  header('Content-Disposition: attachement; filename='.basename($file));
		  header('Content-Type: application/force-download');
		  header('Expires: 0');
		  header('Cache-Control: must-revalidate');
		  header('Content-Length: '.filesize($file));
		  header('Connection: close');
		  ob_get_clean();
		  readfile($file);
		  ob_end_flush();
		  unlink($file);
		} else {
		  header("HTTP/1.0 404 Not Found");
		}
	}

	 private function rrmdir($dir) { 
	   	if (is_dir($dir)) { 
	     $objects = scandir($dir); 
	     foreach ($objects as $object) { 
	       if ($object != "." && $object != "..") { 
	         if (is_dir($dir."/".$object))
	           $this->rrmdir($dir."/".$object);
	         else
	           unlink($dir."/".$object); 
	       } 
	     }
	     rmdir($dir); 
	   } 
	} 

	 private function Zip($source, $destination) {
		ini_set('memory_limit', '-1');
	    if (!extension_loaded('zip') || !file_exists($source)) {
	        return false;
	    }

	    $zip = new ZipArchive();
	    if (!$zip->open($destination, ZIPARCHIVE::CREATE)) {
	        return false;
	    }

    	$source = str_replace('\\', '/', realpath($source));

	    if (is_dir($source) === true)
	    {
	        $files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($source), RecursiveIteratorIterator::SELF_FIRST);

	        foreach ($files as $file)
	        {
	            $file = str_replace('\\', '/', $file);

	            // Ignore "." and ".." folders
	            if( in_array(substr($file, strrpos($file, '/')+1), array('.', '..')) )
	                continue;

	            $file = realpath($file);

	            if (is_dir($file) === true)
	            {
	                $zip->addEmptyDir(str_replace($source . '/', '', $file . '/'));
	            }
	            else if (is_file($file) === true)
	            {
	                $zip->addFromString(str_replace($source . '/', '', $file), file_get_contents($file));
	            }
	        }
	    }
	    else if (is_file($source) === true)
	    {
	        $zip->addFromString(basename($source), file_get_contents($source));
	    }

	    return $zip->close();
	}
}
// tess
