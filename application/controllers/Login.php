<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

	function __construct() {
		parent::__construct();
        $this->load->model('login_model');
	}

    function index() {
        if( $this->session->userdata('isLoggedIn') ) {
            redirect('/home?q=RrPW9'); 
        } else {
            $this->show_login(false);
        }
    }

    function login_user() {
	   $iduser   = $this->security->xss_clean($this->input->post('iduser'));
       $password = $this->security->xss_clean($this->input->post('password'));
       $thang    = $this->security->xss_clean($this->input->post('thang'));

         if( $iduser && $password && $this->login_model->validate_user($iduser, $password, $thang)) echo 1;
         else echo 0;
     }

    function show_login( $show_error = false ) {
        $data['error'] = $show_error;
        $this->load->view('main/utama_login', $data);
		//$this->load->view('main/maintenance_view', $data);
    }

    function logout_user() {
      $this->session->sess_destroy();
      $this->index();
    }

    function showphpinfo() {
        echo phpinfo();
    }

    function test() {
        print_r($this->session->userdata);
    }
}
