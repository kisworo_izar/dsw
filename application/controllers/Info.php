<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Info extends CI_Controller {

	function __construct() {
		parent::__construct();
        $this->load->model('info_model');
	}

	public function index() {
		$row = $this->info_model->get_data();
		$config = $this->fc->pagination( site_url("info/index"), count($row), 15, '3' );
		$this->pagination->initialize($config);
		
		$data['data']  = array_slice($row, $this->uri->segment(3), 15);
		$data['bread'] = array('header'=>'Manajemen Info', 'subheader'=>'Data Info');
		$data['view']  = "admin/info_grid";
		$this->load->view('main/utama', $data);
	}

	public function rekam() {
		$data['data']  = "";
		$data['bread'] = array('header'=>'Manajemen Info', 'subheader'=>'Data Info');
		$data['view']  = "admin/info_rekam";
		$this->load->view('main/utama', $data);
	}

	function cari() {
		$persh = $_POST['nminfo'];  if ($_POST['cari']<>'Cari') $persh = '';
		$this->param = $this->mgm->param( $_POST['nilai'] ."persh:$persh" );
		$this->grid();
	}

	function save() {
		$this->info_model->save( 'Tambah' );
		redirect("info");
	}

}
