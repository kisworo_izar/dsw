<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Revisi_dja extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->dbrevisi = $this->load->database('revisi'. $this->session->userdata('thang'), TRUE);
		date_default_timezone_set('Asia/Jakarta');
        $this->load->model('dsw_model');
		$this->load->model('Revisi_dja_model');
	    $this->session->set_userdata(array('cari' => ''));
	}

	public function index() {
		if (! $this->session->userdata('isLoggedIn') ) redirect("login/show_login");
		if (isset($_POST['search'])) {
			if ($_POST['search'] == 'search') $this->session->set_userdata('cari', strtolower($_POST['cari']));
			else $this->session->set_userdata('cari', '');
		}
		$this->dash();
	}

	public function dash( $depuni=null, $start=null, $end=null ) {
		$cari = $this->session->userdata('cari');
		$adm 	  = $this->session->userdata('idusergroup');
		$month 	  = date('m')-'1';
		$hr       = date('d')-'1';
		$tahun 	  = $this->session->userdata('thang'); //date('Y');
		if (strpos($adm,'002')) $cek = 1; else $cek = 0;

		if ($cek !=1 and $start == null) { 
			$start = date("01-$month-$tahun"); $end = date("d-m-$tahun"); 
		} elseif($cek==1 and $start == null){
			$start = date("$hr-m-$tahun"); $end = date("d-m-$tahun"); 
		} else{ 
			$start = $this->uri->segment(3); $end = $this->uri->segment(4);  
		}

		$data = $this->Revisi_dja_model->get_data( $depuni, $this->fc->ustgl($start), $this->fc->ustgl($end) , $tahun, $cari);

		$data['menu']  = $this->dsw_model->get_menu( $this->uri->segment(1) );
		$data['bread'] = array('header'=>'Monitoring Revisi Anggaran', 'subheader'=>'Revisi Anggaran');
		$data['view']  = "revisi_dja/v_dash";
		$this->load->view('main/utama', $data);
	}

	public function report($start=null, $end=null ) {
		if ($start==null) { $start = date('01-01-Y'); $end = date('d-m-Y'); }
		$data = $this->tracking_model->create_report($this->fc->ustgl($start), $this->fc->ustgl($end));

		$data['menu']  = $this->dsw_model->get_menu( $this->uri->segment(1) );
		$data['bread'] = array('header'=>'Data Revisi Anggaran', 'subheader'=>'Revisi Anggaran');
		$data['view']  = "revisi/v_report";
		$this->load->view('main/utama', $data);
	}

	function test() {
		echo 'ini group : '.$this->session->userdata('thang') .'<br>';
			$query 	= $this->db->query("Select kddept From revisi_wenang Where kdso='". $this->session->userdata('kdso') ."' and tahun ='$tahun' ");
			echo "Select kddept From revisi_wenang Where kdso='". $this->session->userdata('kdso') ."' and tahun ='$tahun'";
			$hasil 	= $query->row_array();
			// echo "$hasil";exit();
			$whr 	= "AND kl_dept IN ('". str_replace(",", "','", $hasil['kddept']) ."')";
		echo $whr;
	}

	public function pdf_routing ($rev_id){
		//echo $rev_id; exit();
 		$data['routing'] = $this->Revisi_dja_model->get_pdf_routing($rev_id);
 		$data['menu']   = $this->dsw_model->get_menu( $this->uri->segment(1) );
 		$data['bread']  = array('header'=>'Monitoring Revisi Anggaran', 'subheader'=>'Revisi Anggaran');
 		$data['view']   = "revisi_dja/cetak_routing_slip";
 		$this->load->view('revisi_dja/cetak_routing_slip', $data);

 		//convert to pdf
 		$html = $this->output->get_output();
		$this->dompdf->load_html($html);
		$this->dompdf->set_paper('A4','portraite');
		$this->dompdf->render();
		$this->dompdf->stream( 'Routing Slip'.'.pdf');
		//$this->dompdf->stream( 'surat_tugas_online'.'.pdf',array('Attachment'=>0) );


		$file = "routing_slip_tiket_$rev_id.pdf";
		file_put_contents("files/revisi_SatuDJA/$rev_id/$file", $this->dompdf->output());
		// $this->dompdf->stream( "routing_slip_tiket_$rev_id.pdf" );
		//$this->revisi_model->email_revisi( $rev_id, $file );
 	}

	public function form2( $rev_id ) {
		// $data = $this->revisi_23_model->get_row( $rev_id );

		$data['rev_id']= $rev_id;
		$data['menu']  = $this->dsw_model->get_menu( $this->uri->segment(1) );
		$data['bread'] = array('header'=>'&nbsp;', 'subheader'=>'Revisi Anggaran');
		$data['view']  = "revisi_dja/v_form2";
		$this->load->view('main/utama', $data);
	}

	function crud_form2() {
		//echo 'start';
		$rev_id 	= $_POST['rev_id'];
		$ip	   = $this->fc->get_client_ip();

		$this->dbrevisi->query( "Update revisi set t2_status =1, rev_tahap = 2, t2_proses_tgl=current_timestamp(), t2_proses_ip='$ip'
							where rev_id='$rev_id' " );
		//redirect("revisi_23/form2_proses");
		$this->form2_proses($rev_id);
	}

	public function form2_proses( $rev_id ) {
		//$ruh  = 'Rekam'; if ($this->uri->segment(3)=='u') $ruh = 'Ubah';
		// $data = $this->revisi_23_model->get_row( $ruh );
		$data['rev_id']= $rev_id;
		$data['menu'] = $this->dsw_model->get_menu( $this->uri->segment(1) );
		$data['bread'] = array('header'=>'&nbsp;', 'subheader'=>'Revisi Anggaran');
		$data['proses'] = $this->Revisi_dja_model->get_row_id( $rev_id );
		$data['view'] = "revisi_dja/v_form2_proses";
		$this->load->view('main/utama', $data);
	}

	function crud_form2_proses() {
		$this->Revisi_dja_model->save_form2_proses();
		redirect("revisi_dja/dash");
	}


	public function form3( $rev_id ) {
		// $data = $this->revisi_23_model->get_row( $rev_id );

		$data['rev_id']= $rev_id;
		$data['menu']  = $this->dsw_model->get_menu( $this->uri->segment(1) );
		$data['bread'] = array('header'=>'&nbsp;', 'subheader'=>'Revisi Anggaran');
		$data['view']  = "revisi_dja/v_form3";
		$this->load->view('main/utama', $data);
	}

	function crud_form3() {
		//echo 'start';
		$rev_id 	= $_POST['rev_id'];
		$ip	   		= $this->fc->get_client_ip();

		$this->dbrevisi->query( "Update revisi set t3_status =1, rev_tahap = 3, t3_proses_tgl=current_timestamp(), t3_proses_ip='$ip'
							where rev_id='$rev_id' " );
		//redirect("revisi_23/form2_proses");
		$this->form3_proses($rev_id);
	}

	public function form3_proses( $rev_id ) {
		//$ruh  = 'Rekam'; if ($this->uri->segment(3)=='u') $ruh = 'Ubah';
		// $data = $this->revisi_23_model->get_row( $ruh );
		$data['rev_id']= $rev_id;
		$data['menu'] = $this->dsw_model->get_menu( $this->uri->segment(1) );
		$data['bread'] = array('header'=>'&nbsp;', 'subheader'=>'Revisi Anggaran');
		$data['proses'] = $this->Revisi_dja_model->get_row_id( $rev_id );
		$data['view'] = "revisi_dja/v_form3_proses";
		$this->load->view('main/utama', $data);
	}

	function crud_form3_proses() {

		$rev_id			= $_POST['rev_id'];
		$file 			= $_POST['t3_und_file'];

		$this->Revisi_dja_model->save_form3_proses();
		$this->Revisi_dja_model->email_und_revisi( $rev_id );
		redirect("Revisi_dja/dash");
	}

	public function fileupload_form3() {
		$path = 'files/revisi_SatuDJA/' . $_POST['rev_id'];
		$old  = umask(0);
		is_dir($path) || mkdir($path, 0777, true);
		umask($old);

		$file = $_FILES[ $_POST['name'] ];
		$resp = array('uploaded' => 'Gagal');
		if ($file) {
			move_uploaded_file($file["tmp_name"], $path .'/'. $file['name']);
			$resp = array('uploaded' => $file['name']);
		}
		echo json_encode($resp);
 	}

 	public function form4($rev_id) {

		$data['rev_id']= $rev_id;
		$data['menu']  = $this->dsw_model->get_menu( $this->uri->segment(1) );
		$data['bread'] = array('header'=>'&nbsp;', 'subheader'=>'Revisi Anggaran');
		$data['view']  = "revisi_dja/v_form4";
		$this->load->view('main/utama', $data);
	}


	public function form4_proses($rev_id) {
			$data['rev_id']= $rev_id;
			$data['menu']  = $this->dsw_model->get_menu( $this->uri->segment(1) );
			$data['bread'] = array('header'=>'&nbsp;', 'subheader'=>'Revisi Anggaran');
			$data['proses'] = $this->Revisi_dja_model->get_row_id( $rev_id );
			$data['view']  = "revisi_dja/v_form4_proses";
			$this->load->view('main/utama', $data);
	}

	function crud_form4() {
		$rev_id 	= $_POST['rev_id'];
		$ip	   		= $this->fc->get_client_ip();

		$this->dbrevisi->query("Update revisi Set rev_tahap=4, t4_status=1, t4_proses_tgl=current_timestamp(), t4_proses_ip='$ip' where rev_id='$rev_id'");

		$this->form4_proses($rev_id);
	}

	function crud_form4_proses() {
		$rev_id= $_POST['rev_id'];
		$this->Revisi_dja_model->save_form4_proses();
		$this->Revisi_dja_model->email_revisi_form4( $rev_id );
		redirect("Revisi_dja/dash");
	}

	public function form5($rev_id) {

		$data['rev_id']	= $rev_id;
		$data['menu'] 	= $this->dsw_model->get_menu( $this->uri->segment(1) );
		$data['bread'] 	= array('header'=>'&nbsp;', 'subheader'=>'Revisi Anggaran');
		$data['view'] 	= "revisi_dja/v_form5";
		$this->load->view('main/utama', $data);
	}


	function crud_form5(){
		$rev_id 	= $_POST['rev_id'];
		$ip	   		= $this->fc->get_client_ip();

		$this->dbrevisi->query("Update revisi Set rev_tahap=5, t5_status=1, t5_proses_tgl=current_timestamp(), t5_proses_ip='$ip' where rev_id='$rev_id'");

		$this->form5_proses($rev_id);
	}

	public function form5_proses($rev_id) {
		$data['rev_id'] = $rev_id;
		$data['menu'] 	= $this->dsw_model->get_menu( $this->uri->segment(1) );
		$data['proses'] = $this->Revisi_dja_model->get_row_id( $rev_id );
		$data['bread'] 	= array('header'=>'&nbsp;', 'subheader'=>'Revisi Anggaran');
		$data['view'] 	= "revisi_dja/v_form5_proses";
		$this->load->view('main/utama', $data);
	}


	public function crud_form5_proses(){
		$rev_id 	= $_POST['rev_id'];
		$this->Revisi_dja_model->save_form5_proses();
		$this->Revisi_dja_model->email_revisi_form5( $rev_id );
		redirect("revisi_dja/dash");
	}

 	// new
 	public function fileupload_form4() {
		$path = 'files/revisi_SatuDJA/' . $_POST['rev_id'];
		$old  = umask(0);
		is_dir($path) || mkdir($path, 0777, true);
		umask($old);

		$file = $_FILES[ $_POST['name'] ];
		$resp = array('uploaded' => 'Gagal');
		if ($file) {
			move_uploaded_file($file["tmp_name"], $path .'/'. $file['name']);
			$resp = array('uploaded' => $file['name']);
		}
		echo json_encode($resp);
 	}

 	public function form6( $rev_id ) {
		// $data = $this->revisi_23_model->get_row( $rev_id );

		$data['rev_id']= $rev_id;
		$data['menu']  = $this->dsw_model->get_menu( $this->uri->segment(1) );
		$data['bread'] = array('header'=>'&nbsp;', 'subheader'=>'Revisi Anggaran');
		$data['view']  = "revisi_dja/v_form6";
		$this->load->view('main/utama', $data);
	}

	function crud_form6() {
		//echo 'start';
		$rev_id 	= $_POST['rev_id'];
		$ip	   = $this->fc->get_client_ip();

		$this->dbrevisi->query( "Update revisi set t6_status =1, rev_tahap = 6, t6_proses_tgl=current_timestamp(), t6_proses_ip='$ip'
							where rev_id='$rev_id'" );
		//redirect("revisi_23/form2_proses");
		$this->form6_proses($rev_id);
	}

	public function form6_proses( $rev_id ) {
		//$ruh  = 'Rekam'; if ($this->uri->segment(3)=='u') $ruh = 'Ubah';
		// $data = $this->revisi_23_model->get_row( $ruh );
		$data['rev_id']= $rev_id;
		$data['menu'] = $this->dsw_model->get_menu( $this->uri->segment(1) );
		$data['bread'] = array('header'=>'&nbsp;', 'subheader'=>'Revisi Anggaran');
		$data['proses'] = $this->Revisi_dja_model->get_row_id( $rev_id );
		$data['view'] = "revisi_dja/v_form6_proses";
		$this->load->view('main/utama', $data);
	}

	function crud_form6_proses() {
		$rev_id 	= $_POST['rev_id'];
		$this->Revisi_dja_model->save_form6_proses();
		redirect("revisi_dja/dash");
	}

	public function fileupload_form6() {
		$path = 'files/revisi_SatuDJA/' . $_POST['rev_id'];
		$old  = umask(0);
		is_dir($path) || mkdir($path, 0777, true);
		umask($old);

		$file = $_FILES[ $_POST['name'] ];
		$resp = array('uploaded' => 'Gagal');
		if ($file) {
			move_uploaded_file($file["tmp_name"], $path .'/'. $file['name']);
			$resp = array('uploaded' => $file['name']);
		}
		echo json_encode($resp);
 	}

 	public function form7( $rev_id ) {
		$data['rev_id']= $rev_id;
		$data['menu']  = $this->dsw_model->get_menu( $this->uri->segment(1) );
		$data['bread'] = array('header'=>'&nbsp;', 'subheader'=>'Revisi Anggaran');
		$data['proses'] = $this->Revisi_dja_model->get_row_id( $rev_id );
		$data['view']  = "revisi_dja/v_form7";
		$this->load->view('main/utama', $data);
	}

	function crud_form7($rev_id) {
		$rev_id 		= $_POST['rev_id'];
		$ip	  			= $this->fc->getUserIP();
		$t7_nd_catatan	= $_POST['t7_nd_catatan'];
		$t7_nd_no		= $_POST['t7_nd_no'];
		$t7_nd_tgl		= $_POST['t7_nd_tgl'];
		$t7_nd_file		= $_POST['t7_nd_file'];



		$this->dbrevisi->query( "Update revisi set t7_status =1, rev_tahap = 7, t7_proses_tgl=current_timestamp(), t7_proses_ip='$ip', t7_nd_no ='$t7_nd_no',  t7_nd_tgl='$t7_nd_tgl', t7_nd_file='$t7_nd_file', t7_nd_catatan='$t7_nd_catatan'
							where rev_id='$rev_id' " );
		redirect("revisi_dja/form7_proses/$rev_id");
		//$this->form7_proses($rev_id);
	}

	public function form7_proses($rev_id) {
		$data['rev_id']= $rev_id;
		$data['menu'] = $this->dsw_model->get_menu( $this->uri->segment(1) );
		$data['bread'] = array('header'=>'&nbsp;', 'subheader'=>'Revisi Anggaran');
		$data['proses'] = $this->Revisi_dja_model->get_row_id( $rev_id );
		$data['view'] = "revisi_dja/v_form7_proses_new";
		$this->load->view('main/utama', $data);
	}

	function crud_form7_proses($rev_id) {

		$rev_id			= $_POST['rev_id'];
		$this->Revisi_dja_model->save_form7_proses();
		redirect("revisi_dja/dash");
		// $this->form7_proses($rev_id);
	}

	public function fileupload_form7() {
		$path = 'files/revisi_SatuDJA/' . $_POST['rev_id'];
		$old  = umask(0);
		is_dir($path) || mkdir($path, 0777, true);
		umask($old);

		$file = $_FILES[ $_POST['name'] ];
		$resp = array('uploaded' => 'Gagal');
		if ($file) {
			move_uploaded_file($file["tmp_name"], $path .'/'. $file['name']);
			$resp = array('uploaded' => $file['name']);
		}
		echo json_encode($resp);
 	}

 	function mundur_status($lvl,$rev_id){
 		$this->fc->logRevisi($rev_id, "Turun status ke - $lvl");
 		$data = $this->Revisi_dja_model->mundur_status($lvl,$rev_id);
		redirect("revisi_dja/dash");

 	}

 	function create_zip_adk($rev_id){
		//$rev_id='2020.060.01.001';
		$nmfile = $rev_id.'.zip';
		$thang  = $this->session->userdata('thang');
		$jenis  = "ADK";
		$source = "files/revisi_SatuDJA/$rev_id/adk_satker/";
		$destination = "files/files_backup/$nmfile";

		$ipSource = "files/revisi_SatuDJA/$rev_id/adk_satker";
		$ipTarget = "files/files_backup/$rev_id";

		
		$this->Zip($source,$destination);
		$this->getall( $nmfile );
		$this->rrmdir("files/files_backup/$rev_id");

	}

	function getall( $nmfile ) {
		$file = "files/files_backup/$nmfile";

		//echo "$file <br>";
		if (file_exists($file)) {
		  header('Content-Disposition: attachement; filename='.basename($file));
		  header('Content-Type: application/force-download');
		  header('Expires: 0');
		  header('Cache-Control: must-revalidate');
		  header('Content-Length: '.filesize($file));
		  header('Connection: close');
		  ob_get_clean();
		  readfile($file);
		  ob_end_flush();
		  unlink($file);
		} else {
		  header("HTTP/1.0 404 Not Found");
		}
	}

	 function rrmdir($dir) { 
	   	if (is_dir($dir)) { 
	     $objects = scandir($dir); 
	     foreach ($objects as $object) { 
	       if ($object != "." && $object != "..") { 
	         if (is_dir($dir."/".$object))
	           $this->rrmdir($dir."/".$object);
	         else
	           unlink($dir."/".$object); 
	       } 
	     }
	     rmdir($dir); 
	   } 
	} 

	 function Zip($source, $destination) {
		ini_set('memory_limit', '-1');
	    if (!extension_loaded('zip') || !file_exists($source)) {
	        return false;
	    }

	    $zip = new ZipArchive();
	    if (!$zip->open($destination, ZIPARCHIVE::CREATE)) {
	        return false;
	    }

    	$source = str_replace('\\', '/', realpath($source));

	    if (is_dir($source) === true)
	    {
	        $files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($source), RecursiveIteratorIterator::SELF_FIRST);

	        foreach ($files as $file)
	        {
	            $file = str_replace('\\', '/', $file);

	            // Ignore "." and ".." folders
	            if( in_array(substr($file, strrpos($file, '/')+1), array('.', '..')) )
	                continue;

	            $file = realpath($file);

	            if (is_dir($file) === true)
	            {
	                $zip->addEmptyDir(str_replace($source . '/', '', $file . '/'));
	            }
	            else if (is_file($file) === true)
	            {
	                $zip->addFromString(str_replace($source . '/', '', $file), file_get_contents($file));
	            }
	        }
	    }
	    else if (is_file($source) === true)
	    {
	        $zip->addFromString(basename($source), file_get_contents($source));
	    }

	    return $zip->close();
	}

	 

}

// tess
