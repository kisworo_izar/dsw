<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Profile extends CI_Controller {

	var $param;	//Create a variable for the entire controller

	function __construct() {
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
	    $this->load->model('m_profile');
	    $this->session->set_userdata(array('cari' => ''));
	}

	public function index() {
        if (! $this->session->userdata('isLoggedIn') ) redirect("login/show_login");
		$this->menu();
	}

	public function menu() {
		$this->fc->log('Profile');
		$data['user']  = $this->m_profile->get_data();
		$data['menu']  = $this->dsw_model->get_menu( $this->uri->segment(1) );
		$data['bread']= array('header'=>'User Profile', 'subheader'=>'Profile');
		$data['view'] = "user/v_profile";
		$this->load->view('main/utama', $data);
		// print_r($data['menu']);
	}

	function crud() {
		$this->m_profile->save();
		redirect("profile/index");
	}

}
