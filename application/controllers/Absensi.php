<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Absensi extends CI_Controller {

	function __construct() {
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
		$this->load->model('m_absensi');
		$this->session->set_userdata(array('cari' => ''));
		$this->dbdsw  = $this->db;
		$this->milea = $this->load->database('milea',TRUE);
		$this->dbmilea = $this->milea;
		$this->load->model('pegawai_model');
		$this->load->model('absensi_model');
	}

	public function index() {
		// require_once 'assets/plugins/htmlpurifier/HTMLPurifier.auto.php';
		// $config	  = HTMLPurifier_Config::createDefault();
		// $purifier = new HTMLPurifier($config);

		if (! $this->session->userdata('isLoggedIn') ) redirect("login/show_login");
		$this->fc->log('Presensi Pegawai');

		if (isset($_GET['q']))  $menu   = $_GET['q'];  else show_404();

		if 		(preg_match("~\bV7X6z\b~", $menu))	$this->menu();
		else if (preg_match("~\bwu2kS\b~", $menu))	$this->cari();
		else if (preg_match("~\bYhdv5\b~", $menu))	$this->pelanggaran();
		else show_404();
	}

	private function menu( $start=null, $end=null ) {
		if ($start==null) {
			$start = date('Y-m-d', strtotime('-15 days', strtotime( date('Y-m-d') )));
			$end   = date('Y-m-d');
			$data['start'] = date('d-m-Y', strtotime('-15 days', strtotime( date('d-m-Y') )));
			$data['end']   = date('d-m-Y');
		} else {
			$data['start'] = $this->fc->idtgl( $start );
			$data['end']   = $this->fc->idtgl( $end );
		}

		$range = "and tgl>='$start' and tgl<='$end'";
		$data['absensi']= $this->m_absensi->get_data( $range );
		$data['idws']   = $this->m_absensi->get_idws();
		$data['rekap']  = $this->m_absensi->get_rekap();
		$data['maxtgl'] = $this->m_absensi->get_max();

		$data['menu']   = $this->dsw_model->get_menu( $this->uri->segment(1) );
		$data['bread']  = array('header'=>'&nbsp;', 'subheader'=>'Kehadiran');
		$data['view']   = "absensi/v_absensi";
		$this->load->view('main/utama', $data);
	}

	private function cari() {
		$start = $this->fc->ustgl( $_GET['start'] );
		$end   = $this->fc->ustgl( $_GET['end'] );
		$this->menu( $start, $end );
	}

	private function pelanggaran() {
		$data['pelanggaranMax'] = 2250;

		$data['pelanggaranHistory'] = $this->m_absensi->getpelanggaranHistory($this->session->userdata('nip'));

		$jabatan = $this->session->userdata('jabatan');
		$isPejabat = $this->pegawai_model->isPejabat($jabatan);
		$isSekretarisEselon2 = (stripos($jabatan, 'sekretaris eselon') !== false);
		$data['isAllowed'] = ($isPejabat or $isSekretarisEselon2);
		

		/* Unit Violation */
		if ($data['isAllowed']) {
			/* ROLE: PEJABAT */
			$kdso = $this->session->userdata('kdso');

			/* ROLE: KEPALA TU & SEKRETASI ESELON 2 */
			if (stripos($jabatan, 'tata usaha') !== false and substr($kdso, 0, 2) != '01') {
				$kdso = substr($kdso, 0, 2).'0000';
			}

			/* ROLE: SEKRETARIS ESELON 2 */
			if ($isSekretarisEselon2) {
				$kdso = substr($kdso, 0, 2).'0000';
			}

			/* ROLE: KABAG KIMRA */
			if ($kdso == '010500') {
				$kdso = '000000';
			}

			$data['units'] = $this->pegawai_model->getSOChilds($kdso);
			$data['poin_max'] = 37.5 * 60;
		}

		$data['menu']   = $this->dsw_model->get_menu( $this->uri->segment(1) );
		$data['bread']  = array('header'=>'&nbsp;', 'subheader'=>'Pelanggaran');
		$data['view']   = "absensi/v_pelanggaran";

		$this->load->view('main/utama', $data);
	}

	/* JAM PELANGGARAN PER UNIT */
	private function pelanggaranJam()
	{
		if (empty($_POST['so'])) {
			$_POST['so'] = ['000000'];
		}

		$rows = [];
		foreach ($_POST['so'] as $so) {
			$rows_tmp = $this->absensi_model->pelanggaranJam(['eselon' => $this->pegawai_model->getEselon($so), 'nama' => $this->pegawai_model->getSO($so)->nmso]);
			$rows = array_merge($rows, $rows_tmp);
		}

		$out = [];
		foreach ($rows as $row) {
			$out[$row->esl] = $row->pelanggaran;
		}

		echo json_encode($out);
	}

	/* JUMLAH PEGAWAI YANG MELANGGAR BERDASARKAN BULAN PER UNIT */
	private function pelanggaranBulanan()
	{
		if (empty($_POST['so'])) {
			$_POST['so'] = ['000000'];
		}

		$rows = [];
		foreach ($_POST['so'] as $so) {
			$rows_tmp = $this->absensi_model->pelanggaranBulanan(['eselon' => $this->pegawai_model->getEselon($so), 'nama' => $this->pegawai_model->getSO($so)->nmso]);
			$rows = array_merge($rows, $rows_tmp);
		}

		$out = [];
		foreach ($rows as $row) {
			$out[$row->esl][$row->bulan - 1] = $row->n_pegawai;
		}

		foreach ($out as $key => $value) {
			for ($i=0; $i < date('n'); $i++) { 
				if (empty($value[$i])) {
					$out[$key][$i] = "0";
				}
			}
			ksort($out[$key]);
		}

		echo json_encode($out);
	}

	/* % PEGAWAI DENGAN POIN PELANGGARAN NOL */
	private function pelanggaranPegawaiNol()
	{
		if (empty($_POST['so'])) {
			$_POST['so'] = ['000000'];
		}

		$nol = 0;
		$total = 0;
		foreach ($_POST['so'] as $so) {
			$tmp = $this->absensi_model->pelanggaranPegawaiNol(['eselon' => $this->pegawai_model->getEselon($so), 'nama' => $this->pegawai_model->getSO($so)->nmso]);
			$nol += $tmp['nol'];
			$total += $tmp['total'];
		}

		echo json_encode(intval($nol/$total * 100));
	}

	/* SUMMARY STATUS IZIN */
	private function pelanggaranIzinStatus()
	{
		if (empty($_POST['so'])) {
			$_POST['so'] = ['000000'];
		}

		$rows = [];
		foreach ($_POST['so'] as $so) {
			$izins = $this->absensi_model->pelanggaranIzinStatus(['eselon' => $this->pegawai_model->getEselon($so), 'nama' => $this->pegawai_model->getSO($so)->nmso]);
			foreach ($izins as $izin) {
				$rows[$izin->nip.'_'.$izin->tanggal] = $izin;
			}
		}

		// echo json_encode($this->db->last_query());

		$out = ['sudah' => 0, 'proses' => 0, 'lewat' => 0, 'belum' => 0];

		foreach ($rows as $row) {
			if ($row->tetap_status == '1') {
				$out['sudah']++;
			} elseif (($row->setuju_status == null or $row->setuju_status == '1') and $row->tetap_status == '3') {
				$out['proses']++;
			} else {
				$tanggalawal = '';
				if ($row->setuju_status == '2') {
					$tanggalawal = $row->setuju_waktu;
				}
				if ($row->tetap_status == '2') {
					$tanggalawal = $row->tetap_waktu;
				}
				if (empty($tanggalawal)) {
					$tanggalawal = $row->tanggal;
				}
				if ($this->absensi_model->jumlahHariKerjaPegawai($row->nip, $tanggalawal, date(('Y-m-d'))) > 4) {
					$out['lewat']++;
				} else {
					$out['belum']++;		
				}
			}
		}

		echo json_encode($out);
	}

	private function pelanggaranHistory()
	{
		if (empty($_POST['so'])) {
			$_POST['so'] = ['000000'];
		}

		$sort = $_POST['sort'];

		$json = [];

		foreach ($_POST['so'] as $so) {
			foreach ($this->absensi_model->pelanggaranHistory(date('Y'), ['eselon' => $this->pegawai_model->getEselon($so), 'nama' => $this->pegawai_model->getSO($so)->nmso]) as $row) {
				$nip = $row->nip;
				$izin = '';
				if ($row->tetap_status == '1') {
					$izin = 'sudah';
				} elseif (($row->setuju_status == null or $row->setuju_status == '1') and $row->tetap_status == '3') {
					$izin = 'proses';
				} else {
					$tanggalawal = '';
					if ($row->setuju_status == '2') {
						$tanggalawal = $row->setuju_waktu;
					}
					if ($row->tetap_status == '2') {
						$tanggalawal = $row->tetap_waktu;
					}
					if (empty($tanggalawal)) {
						$tanggalawal = $row->tanggal;
					}
					if ($this->absensi_model->jumlahHariKerjaPegawai($nip, $tanggalawal, date(('Y-m-d'))) > 4) {
						$izin = 'lewat';
					} else {
						$izin = 'belum';		
					}
				}

				$json[$nip]['nama'] = $row->nama;
				$json[$nip]['jabatan'] = $row->jabatan;
				$json[$nip]['esl1'] = $row->esl1;
				$json[$nip]['esl2'] = $row->esl2;
				$json[$nip]['esl3'] = $row->esl3;
				$json[$nip]['esl4'] = $row->esl4;
				$json[$nip]['poin_total'] = array_key_exists('poin_total', $json[$nip]) ? $json[$nip]['poin_total'] + $row->pelanggaran : $row->pelanggaran;
				$json[$nip]['izin_'.$izin] = array_key_exists('izin_'.$izin, $json[$nip]) ? $json[$nip]['izin_'.$izin] + 1 : 1;
				$json[$nip]['pelanggaran'][] = [
					'tanggal' => $row->tanggal,
					'status' => $row->status,
					'poin' => $row->pelanggaran,
					'izin' => $izin
				];
			}
		}
		

		$out = [];
		foreach ($json as $key => $value) {
			$value['nip'] = $key;
			if (!array_key_exists('izin_belum', $value)) $value['izin_belum'] = 0;
			if (!array_key_exists('izin_proses', $value)) $value['izin_proses'] = 0;
			if (!array_key_exists('izin_sudah', $value)) $value['izin_sudah'] = 0;
			if (!array_key_exists('izin_lewat', $value)) $value['izin_lewat'] = 0;

			$out[] = $value;
		}

		switch ($sort) {
			case '1':
			array_multisort(array_column($out, 'poin_total'), SORT_DESC, $out);
			break;

			case '2':
			array_multisort(array_column($out, 'poin_total'), SORT_ASC, $out);
			break;

			case '3':
			array_multisort(array_column($out, 'izin_belum'), SORT_DESC, $out);
			break;

			case '4':
			array_multisort(array_column($out, 'izin_belum'), SORT_ASC, $out);
			break;
		}

		echo json_encode($out);
	}
}
