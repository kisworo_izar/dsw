<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dash extends CI_Controller {

	var $param;	//Create a variable for the entire controller

	function __construct() {
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
        $this->load->model('dsw_model');
		$this->load->model('dash_model');
	}

	public function index() {
        if (! $this->session->userdata('isLoggedIn') ) redirect("login/show_login");
		$this->dashboard();
	}

	public function dashboard() {
		$menu = $this->uri->segment(1);
		$data['chart']  = array(
			'title'=>'Pagu Anggaran per Jenis Belanja',
			'subtitle'=>'Tahun Anggaran 2016',
			'name'=>'Jenis Belanja',
			'datachart'=>$this->dash_model->get_datachart());

		$data['menu'] = $this->dsw_model->get_menu( $menu );
		$data['bread']= array('header'=>'Infographic', 'subheader'=>'Infographic');
		$data['view'] = "dash/V_dash";
		$this->load->view('main/utama', $data);
	}

	function tes() {
		$db_data = $this->load->database('rkakl16',TRUE);
		$db_ref = $this->load->database('ref',TRUE);

		$query = $db_data->query("select left(kdakun,2) kdgbkpk, '' nmgbkpk, sum(jumlah) jumlah from d_item group by 1 ");
		$tbl_data = $this->fc->ToArr( $query->result_array(), 'kdgbkpk' );

		$this->fc->browse( $tbl_data );
		echo "<br>";

		$query = $db_ref->query("select kdgbkpk, concat(kdgbkpk,' ',nmgbkpk) nmgbkpk from t_gbkpk order by kdgbkpk");
		$tbl_ref = $this->fc->ToArr( $query->result_array(), 'kdgbkpk' );

		$this->fc->browse( $tbl_ref );
		echo "<br>";

		foreach ($tbl_data as $key => $value) {
			$tbl_data[$key]['nmgbkpk'] = $tbl_ref[$key]['nmgbkpk'];
		}

		$this->fc->browse( $tbl_data );

// ============
		// $query = $db_data->query("select kdakun, '' nmakun, sum(jumlah) jumlah from d_item group by 1 ");
		// $tbl_data = $query->result_array();
		//
		// $this->fc->browse( $tbl_data );
		// echo "<br>";
		//
		// $query = $db_ref->query("select kdakun, nmakun from t_akun");
		// $tbl_ref = $query->result_array();
		//
		// $this->fc->browse( $tbl_ref );
		// echo "<br>";
		//
		// foreach ($tbl_data as $key => $value) {
		// 	$tbl_data[$key]['nmakun'] = $tbl_ref[$key]['nmakun'];
		// }
		//
		// $this->fc->browse( $tbl_data );
// ============


		$legend = "";
		foreach ($tbl_data as $row) {
			if(trim($row['nmgbkpk'])!='') {
				if(!strpos($legend, $row['nmgbkpk'])){
					$legend .= "'".$row['nmgbkpk']."'";
				}
			}
		}
		$legend = str_replace("''","','",$legend);

		$data = "";
		foreach ($tbl_data as $row) {
			if(trim($row['nmgbkpk'])!='') {
				if(!strpos($data, $row['nmgbkpk'])){
					$data .= "{value:".$row['jumlah'].", name:'".$row['nmgbkpk']."'}";
				}
			}
		}
		$data = str_replace("}{","},{",$data);

		$datachart = array(
			"legend" => $legend ,
			"data" 	 => $data);
		print_r($datachart);
	}

}
