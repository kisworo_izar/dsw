<?php
class Dapur extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
		if (!$this->session->userdata('isLoggedIn')) redirect("login/show_login");
	}

	function index()
	{
		$this->fc->log('Dapur - Main');

		$data['menu']  = $this->dsw_model->get_menu($this->uri->segment(1));
		$data['bread'] = array('header' => 'Dapur Anggaran', 'subheader' => 'Dapur');
		$data['view']  = "dapur/dapur-index";

		$this->load->view('main/utama', $data);
	}

	function monitoring()
	{
		$this->fc->log('Dapur - Monitoring');

		$data['menu']  = $this->dsw_model->get_menu($this->uri->segment(1));
		$data['bread'] = array('header' => 'Dapur Anggaran', 'subheader' => 'Dapur');
		$data['view']  = "dapur/dapur-monitoring";

		$dbmilea = $this->load->database('milea', TRUE);
		$query = $dbmilea->query("SELECT * FROM pegawai_induk WHERE nip18 = '" . $this->session->userdata('nip') . "'");
		$pegawai = $query->row();
		$query = $dbmilea->query("SELECT id, esl3 AS name FROM dapur_unitpic WHERE esl2 = '" . $pegawai->esl2 . "'");
		$data['unitPICList'] = $query->result();
		$query = $dbmilea->query("SELECT * FROM dapur_jenisproduk WHERE shown = 1 ORDER BY no");
		$data['jenisProdukList'] = $query->result();
		$query = $dbmilea->query("SELECT * FROM dapur_kl");
		$data['kls'] = $query->result();
		$query = $dbmilea->query("SELECT * FROM dapur_monitoring_jenis_rapat");
		$data['jenis_rapats'] = $query->result();

		$this->load->view('main/utama', $data);
	}

	function kronologis($monitoringId)
	{
		$this->fc->log('Dapur - Kronologis');

		$data['menu']  = $this->dsw_model->get_menu($this->uri->segment(1));
		$data['bread'] = array('header' => 'Dapur Anggaran', 'subheader' => 'Dapur');
		$data['view']  = "dapur/dapur-kronologis";
		$data['monitoringId']  = $monitoringId;
		$dbmilea = $this->load->database('milea', TRUE);
		$query = $dbmilea->query("SELECT * FROM dapur_monitoring_jenis_rapat");
		$data['jenisRapatList'] = $query->result();

		$this->load->view('main/utama', $data);
	}

	function produkhukum()
	{
		$this->fc->log('Dapur - Produk Hukum');

		$data['menu']  = $this->dsw_model->get_menu($this->uri->segment(1));
		$data['bread'] = array('header' => 'Dapur Anggaran', 'subheader' => 'Dapur');
		$data['view']  = "dapur/dapur-produkhukum";

		$dbmilea = $this->load->database('milea', TRUE);
		$query = $dbmilea->query("SELECT * FROM pegawai_induk WHERE nip18 = '" . $this->session->userdata('nip') . "'");
		$pegawai = $query->row();
		$query = $dbmilea->query("SELECT id, esl3 AS name FROM dapur_unitpic WHERE esl2 = '" . $pegawai->esl2 . "'");
		$data['unitPICList'] = $query->result();
		$query = $dbmilea->query("SELECT * FROM dapur_jenisproduk WHERE shown = 1 ORDER BY no");
		$data['jenisProdukList'] = $query->result();
		$query = $dbmilea->query("SELECT * FROM dapur_kl");
		$data['kls'] = $query->result();

		$this->load->view('main/utama', $data);
	}
}
