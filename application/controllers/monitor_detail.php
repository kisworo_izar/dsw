<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Monitor_detail extends CI_Controller {

	var $param;	//Create a variable for the entire controller

	function __construct() {
		parent::__construct();
		$this->load->model('monitor_detail_model');
		date_default_timezone_set("Asia/Jakarta");
	}

	public function index() {
        if (! $this->session->userdata('isLoggedIn') ) redirect("login/show_login");
		$this->param = $this->mgm->param();
		$this->param = $this->mgm->param("tahun:'2014', bulan:'12', idpersh:'0002'");
		$this->grid();
	}
	
	function goto_grid() {
		$idpersh= $this->uri->segment(4);	
		$tahun 	= $this->uri->segment(5);
		$bulan 	= $this->uri->segment(6);
		$start	= $this->uri->segment(7);		

		$this->param = $this->mgm->param("idpersh:$idpersh, tahun:$tahun, bulan:$bulan, rowstart:$start");
		$this->grid();
	}

	function grid() {
		$warn = $this->uri->segment(8);
		$this->param->url = "/monitor_rekap/goto_grid/". $this->param->tahun ."/". $this->param->bulan ."/". $this->param->rowstart;
		$data['param'] = $this->param;
		//echo "<pre>"; print_r($data['param']);  echo "</pre>";

		if (strlen(trim($warn))>0) $data['warn'] = $warn;

		$data['data'] = $this->monitor_detail_model->get_data( $this->param );
		$data['cmbbulan'] = $this->fc->bulan('readonly', $this->param);
		$data['cmbtahun'] = $this->fc->tahun('readonly', $this->param);
		$data['view'] = "user/ft_monitor_detail";
		$this->load->view('main/f_utama', $data); //
	}

	function rekam() {
		$ruh	= $this->uri->segment(3);
		$idpersh= $this->uri->segment(4);	
		$tahun 	= $this->uri->segment(5);
		$bulan 	= $this->uri->segment(6);	
		$addbln = $this->uri->segment(7);	
		$addbrs = $this->uri->segment(8);	
		$start	= $this->uri->segment(9);
		
		$url = "/monitor_rekap/goto_grid/$tahun/$bulan/$start;/monitor_detail/goto_grid/1/$idpersh/$tahun/$bulan/$start";
		$data['param'] = $this->mgm->param( "idpersh:$idpersh, tahun:$tahun, bulan:$bulan, addbln:$addbln, addbrs:$addbrs, ruh:$ruh, url:$url" );
		if ($ruh==30) {
			$data['row'] = $this->monitor_detail_model->get_var('bayar');
		} elseif ($ruh==40) {
			$data['row'] = $this->monitor_detail_model->get_var('rekon');
		} else {
			$data['row'] = $this->monitor_detail_model->get_row( $data['param'] );
		}

		$data['view'] = "user/ft_monitor_detail_pembayaran"; if ($ruh>31) $data['view'] = "user/ft_monitor_detail_rekon";
		$this->load->view('main/f_utama',$data);
	}
	
	function save() {
		$param = $this->mgm->param( $_POST['nilai'] ."ruh:". $this->uri->segment(3) );
		$nilai = $this->monitor_detail_model->save($param);
		redirect("monitor_detail/goto_grid/0/$param->idpersh/$param->tahun/$param->bulan/$param->rowstart/$nilai");
	}

	function pdf() {	
		$idpersh= $this->uri->segment(4);	
		$tahun 	= $this->uri->segment(5);
		$bulan 	= $this->uri->segment(6);

		$data['param']= $this->mgm->param( "idpersh:$idpersh, tahun:$tahun, bulan:$bulan" );
		$data['data'] = $this->monitor_detail_model->get_data( $data['param'] );
		$data['lamp'] = $this->monitor_detail_model->get_lamp( $data['param'] );
		$this->load->view('user/ft_monitor_detail_cetak', $data);
		

		// Get output html
		$html = $this->output->get_output();
		
		// Convert to PDF
		$this->dompdf->load_html($html);
		$this->dompdf->set_paper('A4','landscape'); 
		$this->dompdf->render();
		$this->dompdf->stream( $data['lamp']['file'] );
		//
	}

}
