<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Fokus extends CI_Controller {
	var $param;	//Create a variable for the entire controller
	function __construct() {
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
		$this->load->model('dsw_model');
        $this->load->model('fokus_model');
		$this->session->set_userdata(array('cari' => ''));
	}

	public function index() {
        if (! $this->session->userdata('isLoggedIn') ) redirect("login/show_login");
		$this->menu();
	}

	public function menu() {
		$menu = $this->uri->segment(1);

		$data['menu']  = $this->dsw_model->get_menu( $this->uri->segment(1) );
		$data['bread']= array('header'=>'Fokus DJA', 'subheader'=>'Fokus DJA');
		$data['view'] = "fokus/bidang";
		$this->load->view('main/utama', $data);
		// print_r($data['menu']);
	}

	public function bidang(){
		$data['menu']  = $this->dsw_model->get_menu( $this->uri->segment(1) );

		$bid = $this->uri->segment(3);
		$data['bread']= array('header'=>'Fokus DJA', 'subheader'=>'Fokus DJA');
		//$data['anggota']=$this->fokus_model->anggota_tampil($bid);
		$data['bidang']=$this->fokus_model->select_bidang($bid);
		$data['moncap']=$this->fokus_model->moncap_tampil($bid);
		$data['matin']=$this->fokus_model->matin_tampil($bid);
		$data['monkeg']=$this->fokus_model->monkeg_tampil($bid);
		$data['view'] = "fokus/bidang";
		$this->load->view('main/utama', $data);
	}

	public function bidang_admin(){
		$data['menu']  = $this->dsw_model->get_menu( $this->uri->segment(1) );

		$bid = $this->uri->segment(3);
		$data['bread']= array('header'=>'Fokus DJA', 'subheader'=>'Fokus DJA');
		$data['moncap']=$this->fokus_model->moncap_tampil_admin();
		$data['matin']=$this->fokus_model->matin_tampil($bid);
		$data['monkeg']=$this->fokus_model->monkeg_tampil_admin();
		$data['bid']=$this->fokus_model->select_bidang($bid);
		$data['jns']=$this->fokus_model->select_jenis();
		$data['view'] = "fokus/bidang_admin";
		$this->load->view('main/utama', $data);

		/*ADD, EDIT,DELETE*/
		$id_bidang	=$this->input->post('id_bidang');
        $kd_jenis 	=$this->input->post('kd_jenis');
        $output   	=$this->input->post('output');
        $volume  	=$this->input->post('volume');
        $progres 	=$this->input->post('progres');
        $tgl_batas 	=$this->input->post('tgl_batas');
        $catatan 	=$this->input->post('catatan');
        $tahun 		=date("Y");

        $tgl_tkda_a =$this->input->post('tgl_tkda_a');
        $tgl_tkda_z =$this->input->post('tgl_tkda_z');
        $tgl_da_a   =$this->input->post('tgl_da_a');
        $tgl_da_z   =$this->input->post('tgl_da_z');

        $link_img   =$this->input->post('link_img');

        $id_moncap 	=$this->input->post('id_moncap');

        if ($this->uri->segment('3')=="add") {
        	if ($kd_jenis==1) {
        		$input = $this->fokus_model->moncap_input($id_bidang,$kd_jenis,$output,$volume,$progres,$tgl_batas,$catatan,$tahun);
	        	if($input)
	        	{
	        		redirect('fokus/bidang_admin/');
	        	}
	        	/*else
	        	{
	        		echo "gagal";exit;
	        	}*/
        	}
        	/*ini nnti untuk monkeg upload gambar*/


        	elseif ($kd_jenis==3) {
        		$input = $this->fokus_model->matin_input($id_bidang,$kd_jenis,$output,$catatan,$tgl_tkda_a,$tgl_tkda_z,$tgl_da_a,$tgl_da_z);
	        	if($input)
	        	{
	        		redirect('fokus/bidang_admin/'.$id_bidang);
	        	}
	        	/*else
	        	{
	        		echo "gagal";exit;
	        	}*/
        	}

        }

        elseif ($this->uri->segment('3')=="edit") {
        	//echo $kd_jenis;exit();
            if ($kd_jenis==1) {
        		$edit = $this->fokus_model->moncap_edit($id_bidang,$id_moncap,$kd_jenis,$output,$volume,$progres,$tgl_batas,$catatan);
	        	if($edit)
	        	{
	        		redirect('fokus/bidang_admin/'.$id_bidang);
	        	}

        	}
        	elseif ($kd_jenis==3) {
        		$edit = $this->fokus_model->matin_edit($id_bidang,$id_moncap,$kd_jenis,$output,$catatan,$tgl_tkda_a,$tgl_tkda_z,$tgl_da_a,$tgl_da_z);
	        	if($edit)
	        	{
	        		redirect('fokus/bidang_admin/'.$id_bidang);
	        	}
        	}
        }

        elseif ($this->uri->segment('3')=="del") {
        	$del=$this->fokus_model->moncap_del($id_moncap);
        	if($del)
        	{
        		redirect('fokus/bidang_admin/'.$id_bidang);
        	}
        }

	}

	public function upload(){
		$id_bidang	=$this->input->post('id_bidang');
		$this->fokus_model->save();
		redirect('fokus/bidang_admin/'.$id_bidang);
	}


}
