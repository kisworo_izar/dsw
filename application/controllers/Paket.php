<?php
class Paket extends CI_Controller {
	function __construct() {
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
	    $this->load->model('dsw_model');
	    $this->load->model('m_paket');
	}

	function index() {
		// require_once 'assets/plugins/htmlpurifier/HTMLPurifier.auto.php';
		// $config	  = HTMLPurifier_Config::createDefault();
		// $purifier = new HTMLPurifier($config);

		if (! $this->session->userdata('isLoggedIn') ) redirect("login/show_login");
		$this->session->set_userdata( array('cari' => '') );

		if (isset($_GET['q']))  $menu = $_GET['q'];  else $menu = $this->uri->segment(3);

		if 		(preg_match("~\blOX77\b~", $menu))	$this->dash();
		else if (preg_match("~\bZriH8\b~", $menu))	$this->grid();
		else if (preg_match("~\b4X0cB\b~", $menu))	$this->crud();
		else if (preg_match("~\bTlO6k\b~", $menu))	$this->cari();
		else show_404();
	}

	private function dash() {
		$row = $this->m_paket->get_data();
		$row = $this->m_paket->get_data_dash( $row );
		$config = $this->fc->pagination( site_url("paket/index/lOX77"), count($row), 12, '4');
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		$this->pagination->initialize($config);

		$data['nilai'] = array_slice($row, $this->uri->segment(4), 12);
		$data['total'] = $this->m_paket->get_total();

		$data['menu']  = $this->dsw_model->get_menu( $this->uri->segment(1) );
		$data['bread'] = array('header'=>'Informasi Paket', 'subheader'=>'Informasi Paket');
		$data['view'] = "paket/v_paket_dash";
		$this->load->view('main/utama', $data);
	}

	private function grid() {
		$row = $this->m_paket->get_data();
		$config = $this->fc->pagination( site_url("paket/index/ZriH8"), count($row), 10, '4');
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
	    $config['cur_tag_close'] = '</a></li>';
		$this->pagination->initialize($config);

		$data['nilai']  = array_slice($row, $this->uri->segment(4), 10);
		$data['t_user'] = $this->m_paket->json_user();
		$data['judul']  = '';

		$data['menu']  = $this->dsw_model->get_menu( $this->uri->segment(1) );
		$data['bread'] = array('header'=>'Admin Paket', 'subheader'=>'Paket Admin');
		$data['view'] = "paket/v_paket_grid";
		$this->load->view('main/utama', $data);
	}

	private function crud() {
		$this->m_paket->save();
		redirect("paket/index/ZriH8");
	}

	private function cari() {
		if ($_POST['search'] == 'search') {
			$this->session->set_userdata('cari', $_POST['cari']);
		} else {
			$this->session->set_userdata('cari', '');
		}

		if ($_POST['nmfunction']=='dash') {
			$this->dash();
		} else {
			$this->grid();
		}
	}
}
