<?php
class Ekstra extends CI_Controller {
	function __construct() 
	{
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
	}

	public function index() 
	{
		if (! $this->session->userdata('isLoggedIn') ) redirect("login/show_login");
		$this->main();
	}

	function main()
	{
		// $this->fc->log('Dosir - Main');

		// $data['menu']  = $this->dsw_model->get_menu( $this->uri->segment(1) );
		$data['bread'] = array('header'=>'SOP HPP', 'subheader'=>'ZI WBK');
		$data['view']  = "ekstra/zi_wbk";

		$this->load->view('main/utama', $data);
	}
}