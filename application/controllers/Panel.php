<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Panel extends CI_Controller {

    function __construct() {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $this->load->model('dsw_model');
        $this->load->model('panel_model');
    }

    function index() {
        // if (! $this->session->userdata('isLoggedIn') ) redirect("login/show_login");
        $this->grid();
    }

    function grid() {
        $data = $this->panel_model->get_data();
        $data['bread'] = array('header'=>'Dashboard', 'subheader'=>'Dashboard');
        $data['view']  = "panel/cpanel_grid";
        $this->load->view('panel/utama', $data);
    }

    function crud() {
        $isi = $_POST['isian'];
        $msg = '';
        for ($i=0; $i<count($isi) ; $i++) { 
            $msg .= $isi[ $i ]; 
            if ($i<count($isi)-1) $msg .= ":";
        }
        echo $msg;
    }

    function test() {
        $query = $this->db->query("Select * From t_panel_admin Where tabel='t_menu' Order By urutan");
        $nil = $query->result_array();
        echo '<pre>'; print_r($nil);
    }

}
