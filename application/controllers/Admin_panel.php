<?php
class Admin_panel extends CI_Controller {
	function __construct() {
		parent::__construct();
	    $this->load->model('admin_panel_model');
	}

	function index() {
		$this->admin_user();
		$this->fc->log('Admin - User');
	}

	function admin_user() {
      if ( !$this->fc->check_akses('admin_panel/admin_user') ) show_404();
		$data = $this->admin_panel_model->get_user();
		$data['bread'] = array('view'=>'admin/v_user_grid', 'header'=>'Data User', 'subheader'=>'Admin Panel');
		$this->load->view('main/utama', $data);
	}

	function crud_user() {
		echo $this->admin_panel_model->save_user();
	}

	function admin_user_group() {
        if ( !$this->fc->check_akses('admin_panel/admin_user_group') ) show_404();
		  $this->fc->log('Admin - User Group');
		$data = $this->admin_panel_model->get_user_group();
		$data['bread'] = array('view'=>'admin/v_user_group_grid', 'header'=>'Data User Group', 'subheader'=>'Admin Panel');
		$this->load->view('main/utama', $data);
	}

	function crud_user_group() {
		echo $this->admin_panel_model->save_user_group();
	}

	function admin_so() {
        if ( !$this->fc->check_akses('admin_panel/admin_so') ) show_404();
		  $this->fc->log('Admin - Struktur Organisasi');
		$data = $this->admin_panel_model->get_so();
		$data['bread'] = array('view'=>'admin/v_so_grid', 'header'=>'Data SO', 'subheader'=>'Admin Panel');
		$this->load->view('main/utama', $data);
	}

	function crud_so() {
		echo $this->admin_panel_model->save_so();
	}

	function admin_menu() {
        if ( !$this->fc->check_akses('admin_panel/admin_menu') ) show_404();
		  $this->fc->log('Admin - Menu');
		$data = $this->admin_panel_model->get_menu();
		$data['bread'] = array('view'=>'admin/v_menu_grid', 'header'=>'Data Menu', 'subheader'=>'Admin Panel');
		$this->load->view('main/utama', $data);
	}

	function crud_menu() {
		echo $this->admin_panel_model->save_menu();
	}

	function lps($iduser){
		// echo $iduser;exit();
		$this->fc->log('SU - Reset Password');
		$data   = $this->admin_panel_model->respass($iduser);
		redirect("admin_panel/admin_user");
	}

	function test() {
		$menu = $this->dsw_model->get_menu( $this->uri->segment(1) );
		// echo '<pre>'; print_r( $data['tabel']  );
		//echo 'tes1';
		$this->fc->browse( $menu );
	}

}
