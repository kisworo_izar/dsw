<?php
class Sbm extends CI_Controller {
	function __construct() {
		parent::__construct();
	    $this->load->model('sbm_model');
	}

	function index() {
		$this->fc->log('SBM');
		if (! $this->session->userdata('isLoggedIn') ) redirect("login/show_login");
		if (isset($_GET['q'])) $menu = $_GET['q']; else show_404();
		if (! $this->fc->check_akses("sbm?q=aa746") ) show_404();

		if (isset($_GET['kode'])) 	  $kode = (int)$_GET['kode']; else $kode = '101';
		if (isset($_GET['per_page'])) $page = (int)$_GET['per_page']; else $page = null;

		if 		($menu == 'aa746') $this->rekap_sbm();
		else if ($menu == '0558b') $this->detil_sbm( $kode );
		else show_404();

	}

	private function rekap_sbm() {
		$this->fc->log('Referensi - SBM');
		$data['bread'] = array('header'=>'Standar Biaya Masukan'.'&nbsp'.$this->session->userdata('thang'), 'subheader'=>'SBM');
		$data['view']  = "sbm/v_sbm_rekap";

		$data['tabel'] = $this->sbm_model->get_rekap();
		$this->load->view('main/utama', $data);
	}

	private function detil_sbm( $kode ) {
		$data['tabel'] = $this->sbm_model->get_detil( $kode );
		$data['bread'] = array('header'=>'Rincian Satuan dan Biaya', 'subheader'=>'<a href="#" onclick="history.go(-1)">SBM</a>');
		$data['view']  = "sbm/v_sbm_detil";
		$this->load->view('main/utama', $data);
	}
}
