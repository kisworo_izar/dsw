<?php
class Slideshow extends CI_Controller {
	function __construct() {
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
	    $this->load->model('dsw_model');
	    $this->load->model('Slideshow_model');
	}

	public function index() {
    	if (! $this->session->userdata('isLoggedIn') ) redirect("login/show_login");
		$this->grid();
	}

	public function grid() {
		$this->fc->log('Slideshow - Dashboard Admin');
		$row = $this->Slideshow_model->get_data();
		$config = $this->fc->pagination( site_url("slideshow/grid"), count($row), 10, '3');
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
	    $config['cur_tag_close'] = '</a></li>';
		$this->pagination->initialize($config);

		$data['table'] = array_slice($row, $this->uri->segment(3), 10);
		$data['menu']  = $this->dsw_model->get_menu( $this->uri->segment(1) );
		$data['bread'] = array('header'=>'Admin Slideshow', 'subheader'=>'Admin Slideshow');
		$data['view']  = "slideshow/V_slideshow_grid";
		$this->load->view('main/utama', $data);
	}

	function rekam() {
		$this->fc->log('Slideshow - Rekam');
		$ruh = 'Rekam'; if ($this->uri->segment(3)=='u') $ruh = 'Ubah'; if ($this->uri->segment(3)=='h') $ruh = 'Hapus';
		$data['ruh']   = $ruh;
		$data['table'] = $this->Slideshow_model->get_row( $this->uri->segment(4) );

		$data['menu']  = $this->dsw_model->get_menu( $this->uri->segment(1) );
		$data['bread'] = array('header'=>'Admin Slideshow', 'subheader'=>'Admin Slideshow');
		$data['view']  = "slideshow/V_slideshow_rekam";
		$this->load->view('main/utama', $data);
	}

	function crud() {
		$this->Slideshow_model->save();
		redirect("slideshow/grid");

	}

	function cari() {
		if ($_POST['search'] == 'search') {
			$this->session->set_userdata('cari', $_POST['cari']);
		} else {
			$this->session->set_userdata('cari', '');
		}

		if ($_POST['nmfunction']=='dash') {
			$this->dash();
		} else {
			$this->grid();
		}
	}

	public function dash() {
		$this->fc->log('Slideshow');
		$row = $this->Slideshow_model->get_data();
		$config = $this->fc->pagination( site_url("slideshow/dash"), count($row), 10, '3');
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
	    $config['cur_tag_close'] = '</a></li>';
		$this->pagination->initialize($config);

		$data['slideshow'] = array_slice($row, $this->uri->segment(3), 10);

		$data['menu']  = $this->dsw_model->get_menu( $this->uri->segment(1) );
		$data['bread'] = array('header'=>'Arsip slideshow', 'subheader'=>'Slideshow');
		$data['view'] = "slideshow/V_slideshow_list";
		$this->load->view('main/utama', $data);
		// $this->fc->browse( $data['nilai'] );

	}

}
