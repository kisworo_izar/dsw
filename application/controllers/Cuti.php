<?php
class Cuti extends CI_Controller {
	function __construct() 
	{
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
	}

	public function index() 
	{
		if (! $this->session->userdata('isLoggedIn') ) redirect("login/show_login");
		$this->dashboard();
	}

	function dashboard()
	{
		$this->fc->log('Cuti - Dashboard');

		$data['org_id'] = 2;

		$data['menu']  = $this->dsw_model->get_menu( $this->uri->segment(1) );
		$data['bread'] = array('header'=>'Dashboard Cuti', 'subheader'=>'Cuti');
		$data['view']  = "cuti/cuti_dashboard";

		$this->load->view('main/utama', $data);
	}

}