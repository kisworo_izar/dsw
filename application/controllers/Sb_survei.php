	<?php
class Sb_survei extends CI_Controller {
	function __construct() {
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
	    $this->load->model('dsw_model');
	    $this->load->model('sb_survei_model');
	}

	public function index() {
        if (! $this->session->userdata('isLoggedIn') ) redirect("login/show_login");
		$this->menu();
	}

	public function menu() {
		$this->fc->log('Standar Biaya');
		$data['d_rss']    = $this->dsw_model->get_rss();
		$data['notif'] 	  = $this->sb_survei_model->get_notif();
		$data['jml'] 	  = $this->sb_survei_model->get_jml();
		$data['bread'] = array('header'=>'Survei SBM '.$this->session->userdata('thang'), 'subheader'=>'Survei');
		$data['view']  = "sb_survei/dashboard";
		$this->load->view('main/utama', $data);
	}

	public function dash_map() {
		$row = $this->sb_survei_model->get_data();
		echo($row);
	}


	// notifikasi
	public function notif_grid() {
		$row = $this->sb_survei_model->notif_get_data();
		$config = $this->fc->pagination( site_url("sb_survei/notif_grid"), count($row), 10, '3');
		$this->pagination->initialize($config);

		$data['table'] = array_slice($row, $this->uri->segment(3), 10);
		$data['menu']  = $this->dsw_model->get_menu( $this->uri->segment(1) );
		$data['bread'] = array('header'=>'Admin Notifikasi DJPBN', 'subheader'=>'Admin');
		$data['view']  = "sb_survei/notifikasi_grid";
		$this->load->view('main/utama', $data);
	}

	function notif_rekam() {
		$this->fc->log('Standar Biaya - Rekam');
		$ruh = 'Rekam'; if ($this->uri->segment(3)=='u') $ruh = 'Ubah'; if ($this->uri->segment(3)=='h') $ruh = 'Hapus';
		$data['ruh']   = $ruh;
		$data['table'] = $this->sb_survei_model->notif_get_row( $this->uri->segment(4) );

		$data['menu']  = $this->dsw_model->get_menu( $this->uri->segment(1) );
		$data['bread'] = array('header'=>'Admin Notifikasi DJPBN', 'subheader'=>'Admin');
		$data['view']  = "sb_survei/notifikasi_rekam";
		$this->load->view('main/utama', $data);
	}

	function notif_crud() {
		if ($_POST['ruh']=='Rekam') {
			echo "string";
			$this->sb_survei_model->notif_save();
		} else {
			if ($_POST['idnotifikasi']<>'') {
				$this->sb_survei_model->notif_save();
			}
		}
		redirect("sb_survei/notif_grid");
	}

	function cari() {
		if ($_POST['search'] == 'search') {
			$this->session->set_userdata('cari', $_POST['cari']);
		} else {
			$this->session->set_userdata('cari', '');
		}

		if ($_POST['nmfunction']=='dash') {
			$this->dash();
		} else {
			$this->notif_grid();
		}
	}


	// INPUT DATA dan REFERENSI ITEM
	function sb_survei_item() {
        // if ( !$this->fc->check_akses('sb_survei/get_survei_item') ) show_404();
		$data = $this->sb_survei_model->get_survei_item();
		$data['bread'] = array('view'=>'sb_survei/v_sb_item_grid', 'header'=>'Manajemen Item Survei', 'subheader'=>'Panel Item');
		$this->load->view('main/utama', $data);
	}

	function crud_survei_item() {
		echo $this->sb_survei_model->save_survei_item();
	}


	//INPUT DATA REFERENSI INFLASI PER PROV PER ITEM

	public function inflasi(){
		$data = $this->sb_survei_model->get_survei_inflasi();
		$data['bread'] = array('view'=>'sb_survei/v_sb_inflasi_grid', 'header'=>'Manajemen Inflasi Survei', 'subheader'=>'Panel Survei Inflasi');
		$this->load->view('main/utama', $data);
	}

	public function crud_survei_inflasi(){
		echo $this->sb_survei_model->save_survei_inflasi();
	}

	function sb_survei_data() {
		$data['bread'] 			= array('view'=>'sb_survei/v_sb_data_grid', 'header'=>'Data Survei', 'subheader'=>'Data Survei');
		$data 					= $this->sb_survei_model->get_survei_data();
		$this->load->view('main/utama', $data);
	}

	function crud_survei_data() {
		$pesan = $this->sb_survei_model->save_survei_data();
		return $pesan;
	}
	function crud_survei_data_edit() {
		$pesan = $this->sb_survei_model->save_survei_data_edit();
		return $pesan;
	}

	function crud_survei_data_flag(){
		$pesan = $this->sb_survei_model->save_survei_data_flag();
		return $pesan;
	}

	function crud_survei_data_keterangan(){
		$pesan = $this->sb_survei_model->save_survei_data_keterangan();
		return $pesan;
	}

 	public function fileupload() {
		$tahun  = $_POST['tahun'];
		$lokasi = $_POST['lokasi'];
		$kditem = $_POST['kditem'];
		$path = 'files/sb_survei/'. $tahun.'/'.$lokasi;
		$old  = umask(0);
		is_dir($path) || mkdir($path, 0777, true);
		umask($old);

		$file = $_FILES[ $_POST['name'] ];

		$resp = array('uploaded' => 'Gagal');
		if ($file) {
			move_uploaded_file($file["tmp_name"], $path.'/'.$kditem.'_'.$file['name']);
			$resp = array('file'=>$file['name']);
		}
		echo json_encode($resp);
 	}

 	public function fileupload2() {
		$tahun  = $_POST['tahun'];
		$lokasi = $_POST['lokasi'];
		$path = 'files/sb_survei/'. $tahun.'/'.$lokasi;
		$old  = umask(0);
		is_dir($path) || mkdir($path, 0777, true);
		umask($old);

		$file = $_FILES[ $_POST['name'] ];

		$resp = array('uploaded' => 'Gagal');
		if ($file) {
			move_uploaded_file($file["tmp_name"], $path.'/'.$file['name']);
			$resp = array('file'=>$file['name']);
		}
		echo json_encode($resp);
 	}

	public function sb_survei_data0() {
		$kdlokasi = $this->session->userdata('kdso');
		$data['bread'] = array('view'=>'sb_survei/v_sb_data_grid0', 'header'=>'Standar Biaya', 'subheader'=>'Data Survei');
		$data['sb_survei_data'] = $this->sb_survei_model->get_survei_data0($kdlokasi);
		$hasil = $data['sb_survei_data'];
		$this->load->view('main/utama', $data);
	}

	public function sb_survei_kirim() {
		$this->fc->log('Standar Biaya - Kirim');
		$kdlokasi = $this->uri->segment('3');
		$data = $this->sb_survei_model->get_survei_data_kirim($kdlokasi);
		$data['bread'] = array('view'=>'sb_survei/v_sb_kirim', 'header'=>'Standar Biaya', 'subheader'=>'Kirim Data');
		$this->load->view('main/utama', $data);
	}

	public function sb_survei_kirim_rincian(){
		$kdlokasi 		= $this->uri->segment('3');
		$data 			= $this->sb_survei_model->get_survei_data_kirim_rincian($kdlokasi);
		$data['bread'] 	= array('view'=>'sb_survei/v_sb_rincian', 'header'=>'Standar Biaya', 'subheader'=>'Rincian Data');
		$this->load->view('main/utama', $data);
	}

	public function sb_survei_dja_dash() {
		$this->fc->log('Standar Biaya Dashboard DJA');
		$data = $this->sb_survei_model->get_survei_data_dja();
		$data['bread'] = array('view'=>'sb_survei/v_sb_dja', 'header'=>'Standar Biaya', 'subheader'=>'Kirim Data');
		$this->load->view('main/utama', $data);
	}

	public function sb_rincian_dja(){
		$kditem = $this->uri->segment('3');
		$data 	= $this->sb_survei_model->get_survei_data_dja_rincian($kditem);
		$data['bread'] 	= array('view'=>'sb_survei/v_sb_rincian_dja', 'header'=>'Standar Biaya', 'subheader'=>'Rincian Data');
		$this->load->view('main/utama', $data);

	}

	public function kirim_perbenpusat(){
		$this->fc->log('Standar Biaya - Kirim DJPBn');
		$pesan = $this->sb_survei_model->save_survei_perbenpusat();
		return $pesan;
	}

	public function kirim_keterangansubitem(){
		$pesan = $this->sb_survei_model->save_keterangan_subitem();
		return $pesan;
	}

	public function kirim_dja(){
		$this->fc->log('Standar Biaya - Kirim DJA');
		$pesan = $this->sb_survei_model->save_survei_dja();
		return $pesan;
	}

	function print_excel(){
		header("Content-type: application/vnd-ms-excel");
		header("Content-Disposition: attachment; filename=Data_Survei_SBM.xls");

		$kditem 		= $this->uri->segment('3');
		$data 			= $this->sb_survei_model->get_survei_data_dja_rincian($kditem);
		$data['bread'] 	= array('view'=>'sb_survei/v_sb_dja_excel');
		$this->load->view('sb_survei/v_sb_dja_excel', $data);
	}

}
