<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sbk extends CI_Controller {

	function __construct() {
		parent::__construct();
        $this->load->model('sbk_model');
        $this->load->model('rkakl_model');
	}

	public function index() {
        if (! $this->session->userdata('isLoggedIn') ) redirect("login/show_login");
		$this->monitoring();
	}

	public function monitoring( $depuni=null ) {
		$this->fc->log('SBK - Monitoring');
		$data['depuni']= $depuni;
		$data['tabel'] = $this->sbk_model->get_grid();

		$data['menu']  = $this->dsw_model->get_menu( $this->uri->segment(1) );
		$data['bread'] = array('header'=>'Monitoring SBK', 'subheader'=>'Rekap');
		$data['view']  = "sbk/V_monitoring_sbk";
		$this->load->view('main/utama', $data);
	}

	public function kertas_kerja( $thang, $output ) {
		if ($thang=='2017') $dbsbk = $this->load->database('sbk', TRUE);
		if ($thang=='2016') $dbsbk = $this->load->database('sbk16', TRUE);

		$data['tabel'] = $this->sbk_model->get_pok( $dbsbk, $output );
		$data['menu']  = $this->dsw_model->get_menu( $this->uri->segment(1) );
		$data['bread'] = array('header'=>'Monitoring SBK', 'subheader'=>'Kertas Kerja');
		$data['view']  = "sbk/V_kertas_kerja";
		$this->load->view('main/utama', $data);
	}

	public function test() {
		$dbsbk = $this->load->database('sbk', TRUE);
		$data  = $this->sbk_model->get_grid( $dbsbk );
		$this->fc->browse( $data );
	}
}
