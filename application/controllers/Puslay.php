<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Puslay extends CI_Controller {

	var $param;	//Create a variable for the entire controller

	function __construct() {
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
    	$this->load->model('puslay_model');
		$this->session->set_userdata( array('cari' => '') );
		$this->dbsatu  = $this->load->database('dbsatu', TRUE);
	}

	public function index() {
		if (! $this->session->userdata('isLoggedIn') ) redirect("login/show_login");
    	if (isset($_GET['q']))		  $menu 	 = $_GET['q']; else show_404();
		if (isset($_GET['idparent'])) $idparent  = $_GET['idparent']; else $idparent = null;
		if (isset($_GET['idchild']))  $idchild   = $_GET['idchild']; else $idchild = null;
		if (isset($_GET['idgrand']))  $idgrand   = $_GET['idgrand']; else $idgrand = null;
		if (isset($_GET['idforum']))  $idforum   = $_GET['idforum']; else $idforum = null;
		if (isset($_GET['otp']))   	  $otp   	 = $_GET['otp']; else $otp = null;
		if (isset($_GET['stsRev']))   $stsRev    = $_GET['stsRev']; else $stsRev = null;
		if (isset($_GET['userid']))   $userid    = $_GET['userid']; else $userid = null;
		if (isset($_GET['typ']))      $typ       = $_GET['typ']; else $typ = null;
		if (isset($_GET['pil']))      $pil       = $_GET['pil']; else $pil = 0;
		// if (isset($_GET['jns']))      $jns       = $_GET['jns']; else $jns = 0;
		if (isset($_GET['sta']))      $sta       = $_GET['sta']; else $sta = date('d-m-Y', strtotime("-3 week"));
		if (isset($_GET['end']))      $end       = $_GET['end']; else $end = date('d-m-Y');

		if (isset($_POST['search'])) {
			if ($_POST['search'] == 'search') $this->session->set_userdata('cari', strtolower($_POST['cari']));
			else $this->session->set_userdata('cari', '');
		}

		if 		($menu == 'p4sly') $this->forum_grid($idparent, $idchild, $idgrand=null, $otp, $stsRev, $userid, $typ, $sta, $end, $pil);
		else if  ($menu == 'ad47s') $this->add_user($userid);
		else if  ($menu == 'cr4ut') $this->crud_user();
		else if  ($menu == '3d1ts') $this->edit_user($userid);
		else if  ($menu == 'rx3lz') $this->reply($idparent,$idchild,$idforum);
		else if  ($menu == 'r3dits')$this->edit_reply($idparent,$idchild,$idforum);
		else if ($menu == '5Untc') $this->save_reply($idparent,$idchild,$idforum);
		else if ($menu == 'xz3ed') $this->delete($idparent,$idchild,$idforum);
		else if ($menu == '5Untx') $this->save_edit($idparent,$idchild,$idforum);
		else if ($menu == 'test') $this->test($sta,$end);

		else show_404();
	}

	private function forum_grid($idparent, $idchild, $idgrand=null, $otp, $stsRev, $userid, $typ, $sta, $end, $pil) {
		$this->fc->log('Puslay');
		$data['otp']	 = $this->puslay_model->otp($otp);
		$data['stsRev'] = $this->puslay_model->stsRev($stsRev);
		$data['userid'] = $this->puslay_model->surel($userid,$typ);
		$data['forum']  = $this->forum($idparent, $idchild, $sta, $end, $pil);
		// echo '<pre>';print_r($data['forum']);exit;
		$data['sta']    = $sta;
		$data['end']    = $end;
		$data['pil']    = $pil;
		$data['menu']   = $this->dsw_model->get_menu( $this->uri->segment(1) );
		$data['bread']  = array('header'=>'iPuslay','subheader'=>'Forum');
		$data['view']   = "puslay/v_puslay";
		$this->load->view('main/utama', $data);
	}

	private function forum($idparent,$idchild,$sta,$end,$pil){
		if (! $this->session->userdata('isLoggedIn') ) redirect("login/show_login");
		$cari = $this->session->userdata('cari');
		if ($cari != '') { $data['cari'] = $this->session->userdata('cari'); } else $data['cari'] = '';
		$row = $this->puslay_model->post_reply( $this->fc->ustgl($sta), $this->fc->ustgl($end), $cari, $pil );
		if($row) $data['d_post'] = $row;  else $data['d_post'] = array();

		$data['idparent']	= $idparent; $data['idchild'] = $idchild;
		$data['d_post'] 	= $row;
		$data['sta'] 		= $sta;
		$data['end'] 		= $end;
		$data['pil'] 		= $pil;
		// $data['cari'] 		= $

		// echo "<pre>"; print_r($data);exit;
		return $data;
	}

	private function add_user($userid){
		$this->fc->log('puslay');
		$data['table'] = $this->puslay_model->add_user(trim($userid));

		$data['bread'] = array('header'=>'iPuslay','subheader'=>'Forum');
		$data['view']  = "puslay/v_puslay_add";
		$this->load->view('main/utama', $data);
	}

	private function edit_user($iduser){
		$this->fc->log('puslay');
		$data['table'] =  $this->puslay_model->edit_user($iduser);
		$data['bread'] = array('header'=>'iPuslay','subheader'=>'Forum');
		$data['view']  = "puslay/v_puslay_edit";
		$this->load->view('main/utama', $data);

	}

	private function reply($idparent, $idchild, $idforum) {
		if (! $this->session->userdata('isLoggedIn') ) redirect("login/show_login");
		if (! $idparent || ! $idchild ) redirect("forum");

 		$data['quote'] = null;
 		if ($idforum) {
			$query = $this->dbsatu->query("Select a.iduser, nmuser, nip, post, profilepic From d_forum a Left Join t_user_satu b On a.iduser=b.iduser Where idparent=$idparent And idchild=$idchild And idforum=$idforum");
			if(!$query) return redirect("puslay?q=p4sly");
			$data['quote'] = $query->row_array();
		}

		$data['sta'] = $_GET['sta'];
		$data['end'] = $_GET['end'];
		$data['pil'] = $_GET['pil'];
		// echo $sta;exit;
		$data['idparent'] = $idparent;
		$data['idchild']  = $idchild;
		$data['idforum']  = $idforum;
		$data['jns']      = 2;//edit adlaah 3
		
		$data['d_head']  = $this->puslay_model->post_head($idparent, $idchild);
 		if(!$data['d_head']) redirect("puslay?q=p4sly");
 		$data['d_room']  = $this->puslay_model->post_room($idparent);
 		if(!$data['d_room']) redirect("puslay?q=p4sly");
		$data['url'] 	 = "puslay?q=p4sly";
		// echo "<pre>";print_r($data);exit();

		$data['menu']  = $this->dsw_model->get_menu( $this->uri->segment(1) );
		$data['bread'] = array('header'=>'Forum','subheader'=>'Forum');
		$data['view']  = "puslay/v_forum_reply";
		$this->load->view('main/utama', $data);
	}

	private function crud_user(){
		$this->puslay_model->crud_user();
		redirect(site_url("puslay?q=p4sly"));

	}

	private function save_reply($idparent, $idchild, $idforum) {
		if (! $this->session->userdata('isLoggedIn') ) redirect('login/show_login');
		if (trim($_POST['reply']) == '') redirect('puslay?q=p4sly');
		if (! $idparent || ! $idchild )  redirect('forum');

		$iduser	= $this->session->userdata('iduser');
		$jns    = $_POST['jns'];
		$sta    = $_POST['sta'];
		$end    = $_POST['end'];
		$pil    = $_POST['pil'];

		// echo $sta;exit;
		$reply  = str_replace('"', "'", $_POST['reply']);
		$idjwb  = '2_'.$idforum;
		$idtny  = '1_'.$idforum;

		// echo '<pre>';print_r($reply);exit;

		// $sta = $_GET['sta'];
		// echo $sta;exit;
		if($jns == 3){
			$this->dbsatu->query("UPDATE d_forum SET post=\"$reply\" WHERE idparent='$idparent' and idchild='$idchild' and idforum='$idforum'");
		}else{
			$this->dbsatu->query("insert into d_forum (idparent,idchild,iduser,tglpost,post, terjawab ) values ($idparent,$idchild,'$iduser',current_timestamp(),\"$reply\",'$idjwb')");
			$this->dbsatu->query("UPDATE d_forum SET terjawab ='$idtny' WHERE idparent='$idparent' and idchild='$idchild' and idforum='$idforum' ");
		}
		
		redirect("puslay?q=p4sly&sta=$sta&end=$end&pil=$pil");
	}

	private function delete($idparent, $idchild, $idforum){
		$this->puslay_model->delete($idparent,$idchild,$idforum);
		redirect("puslay?q=p4sly");	
	}

	private function edit_reply($idparent, $idchild, $idforum){
		// echo $idparent. '#'. $idchild.'#'.$idforum;
		$data['sta'] = $_GET['sta'];
		$data['end'] = $_GET['end'];
		$data['pil'] = $_GET['pil'];
		// echo $sta;exit;
		$data['idparent'] = $idparent;
		$data['idchild']  = $idchild;
		$data['idforum']  = $idforum;
		$data['jns']      = 3;//edit adlaah 3
		$data['d_head']  = $this->puslay_model->post_head($idparent, $idchild);
 		if(!$data['d_head']) redirect("puslay?q=p4sly");
 		$data['d_room']  = $this->puslay_model->post_room($idparent);
 		if(!$data['d_room']) redirect("puslay?q=p4sly");
		$data['url'] 	 = "puslay?q=p4sly";

		$arr 			= $this->dbsatu->query("SELECT * FROM d_forum WHERE idparent='$idparent' AND idchild='$idchild' AND idforum='$idforum' ")->row_array();

		$data['quote'] = null;
 		if ($idforum) {
			$query = $this->dbsatu->query("SELECT a.iduser, nmuser, nip, post, profilepic From d_forum a Left Join t_user_satu b On a.iduser=b.iduser WHERE idparent=$idparent AND  idchild=$idchild AND idforum=$idforum");
			if(!$query) return redirect("puslay?q=p4sly");
			$data['quote'] = $query->row_array();
		}


		$data['post']  = str_replace('"', "'", $arr['post']);

		$data['menu']  = $this->dsw_model->get_menu( $this->uri->segment(1) );
		$data['bread'] = array('header'=>'Forum','subheader'=>'Forum');
		$data['view']  = "puslay/v_forum_reply";
		$this->load->view('main/utama', $data);

		// $arr = $this->puslay_model->edit_reply($idparent,$idchild,$idforum);

		// echo '<pre>';print_r($reply);exit;

	}

	private function test($sta, $end) {
		// echo $sta. '#'. $end;exit;
		// header("Content-type: application/vnd-ms-excel");
		// header("Content-Disposition: attachment; filename=report.xls");

		$data['dat'] 			= $this->puslay_model->reportKanwil($this->fc->ustgl($sta),$this->fc->ustgl($end) );
		$this->load->view('puslay/v_kwlreport', $data);

		echo '<pre>';print_r($data);
	}

	private function print_excel(){
		header("Content-type: application/vnd-ms-excel");
		header("Content-Disposition: attachment; filename=Data_Survei_SBM.xls");

		$kditem 		= $this->uri->segment('3');
		$data 			= $this->sb_survei_model->get_survei_data_dja_rincian($kditem);
		$data['bread'] 	= array('view'=>'sb_survei/v_sb_dja_excel');
		$this->load->view('sb_survei/v_sb_dja_excel', $data);
	}

}
