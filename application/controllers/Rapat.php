<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Rapat extends CI_Controller {

	function __construct() {
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
        $this->load->model('dsw_model');
        $this->load->model('rapat_model');
        $this->load->model('rapat_ruang_model');
		$this->session->set_userdata(array('cari' => ''));
	}

	function index() {
		// require_once 'assets/plugins/htmlpurifier/HTMLPurifier.auto.php';
		// $config	  = HTMLPurifier_Config::createDefault();
		// $purifier = new HTMLPurifier($config);

        if (! $this->session->userdata('isLoggedIn') ) redirect("login/show_login");

		if (isset($_GET['q']))  $menu   = $_GET['q'];	else $menu = $this->uri->segment(3);

		if 		(preg_match("~\bW06or\b~", $menu))	$this->dash();
		else if (preg_match("~\bkQFP5\b~", $menu))	$this->dashboard();
		else if (preg_match("~\b9Cxkg\b~", $menu))	$this->caridash();
		else if (preg_match("~\bwIxda\b~", $menu))	$this->grid();
		else if (preg_match("~\bpqYHV\b~", $menu))	$this->cari();
		else if (preg_match("~\bGsm5e\b~", $menu))	$this->rekam();
		else if (preg_match("~\bjpWHi\b~", $menu))	$this->crud();
		else if (preg_match("~\beBV1c\b~", $menu))	$this->check_nound();
		else if (preg_match("~\bhIFru\b~", $menu))	$this->check_ruang();
		else if (preg_match("~\b2UD9h\b~", $menu))	$this->info_ruang();
		else if (preg_match("~\bhc1xY\b~", $menu))	$this->ref();
		else if (preg_match("~\b7HmSU\b~", $menu))	$this->ref_rekam();
		else if (preg_match("~\bof8AP\b~", $menu))	$this->ref_crud();
		else if (preg_match("~\bbjnvo\b~", $menu))	$this->cariruang();
		else if (preg_match("~\b9LY7x\b~", $menu))	$this->setting_rapat();
		else show_404();
	}

	private function dash( $date ) {
		$this->fc->log('Ruang Rapat - Dashboard');
		$data['rapat']= $this->rapat_model->get_dash( $date );
		$data['prm']  = array('tanggal'=>$date);

		$data['menu'] = $this->dsw_model->get_menu( $this->uri->segment(1) );
		$data['view'] = "rapat/v_rapat_dash";
		// $this->load->view('main/utama_rapat', $data);
	}

	private function dashboard( $start=null, $end=null ) {
		$this->fc->log('Ruang Rapat - Dashboard Slide');
		if ($start==null) {
			$start = date('Y-m-d');
			$end   = date ("Y-m-d", strtotime ($start ."+20 days"));
			$data['start'] = date('d-m-Y');
			$data['end']   = date ("d-m-Y", strtotime ($start ."+20 days"));
		} else {
			$data['start'] = $this->fc->idtgl( $start );
			$data['end']   = $this->fc->idtgl( $end );
		}

		$range = " tglawal>='$start' and tglawal<='$end'";
		$data['rpt']   = $this->rapat_model->get_dashboard( $range );
		$data['menu']  = $this->dsw_model->get_menu( $this->uri->segment(1) );
		$data['bread'] = array('header'=>'Dashboard Ruang Rapat', 'subheader'=>'Ruang Rapat');
		$data['view']  = "rapat/v_rapat_dashboard";
		$this->load->view('main/utama', $data);
	}

	private function caridash() {
		$start = $this->fc->ustgl( $_GET['start'] );
		$end   = $this->fc->ustgl( $_GET['end'] );
		$this->dashboard( $start, $end );
	}

	private function grid($date=null) {
		$this->fc->log('Ruang Rapat - Rekam');
		if ( !$this->fc->check_akses('rapat/grid') ) show_404();
		$row = $this->rapat_model->get_data();
		$config = $this->fc->pagination( site_url("rapat/index/wIxda"), count($row), 15, '4');
		$this->pagination->initialize($config);

		$data['prm']  = array('tanggal'=>$date);
		$data['rapat']= array_slice($row, $this->uri->segment(4), 15);
		$data['menu'] = $this->dsw_model->get_menu( $this->uri->segment(1) );
		$data['bread']= array('header'=>'Pemesanan Ruang Rapat', 'subheader'=>'Data Rapat');
		$data['view'] = "rapat/v_rapat_grid";
		$this->load->view('main/utama', $data);
	}

	private function cari() {
		$mod = $_GET['mod'];
		$tgl = $this->fc->ustgl( $_GET['tgl'], 'hari' );
		if ( $mod=='dash' ) { $this->dash( $tgl ); }
			else { $this->grid( $tgl ); };
	}

	private function rekam() {
		$ruh = 'Rekam'; if ($this->uri->segment(4)=='u') $ruh = 'Ubah'; if ($this->uri->segment(4)=='h') $ruh = 'Hapus';
		$data['ruh']  = $ruh;
		$data['rapat']= $this->rapat_model->get_row( $this->uri->segment(5) ); 

		$data['menu'] = $this->dsw_model->get_menu( $this->uri->segment(1) );
		$data['bread']= array('header'=>'Admin Rapat', 'subheader'=>'Admin Rapat');
		$data['view'] = "rapat/v_rapat_rekam";
		// echo '<pre>'; print_r($data); exit;
		$this->load->view('main/utama', $data);
	}

	private function crud() {
		$this->rapat_model->save();
		redirect("rapat/index/wIxda");
	}

	private function check_nound() {
		$nound = $_POST['nound'];
		$query = $this->db->query("Select * From d_rapat Where nound='$nound'");
		$hasil = $query->result_array();
		if ($hasil) { echo 'no' ;} else  { echo 'ok' ;}
	}

	private function check_ruang() {
		$jnsrapat= $_POST['jnsrapat'];
		$tglawal = $_POST['tglawal'];
		$tglakhir= $_POST['tglakhir'];
		$jamawal = $_POST['jamawal'];
		$ruang   = $this->rapat_model->get_ruang( $jnsrapat, $tglawal, $tglakhir, $jamawal );

		$msg  = '<select name="kdrapat" class="form-control select2" style="width: 100%;" onchange="info_ruang(this.value);">';
		foreach ($ruang as $row) { $msg .= '<option value="'. $row['kdrapat'] .'"><p>'. $row['nmrapat'] .' &ensp; <span id="tab"> ('. $row['kapasitas'] .' org)</span></p>'  .'</option>'; }
		$msg .= '</select>';
        echo $msg;
	}

	private function info_ruang( $kdrapat ) {
		$query = $this->db->query("Select * From t_rapat_ruang Where kdrapat='$kdrapat'");
		$ruang = $query->row();

        $msg = "<div class=\"box-header with-border\"><h3 class=\"box-title\">$ruang->nmrapat</h3></div>
            <div class=\"box-body\">
                <p class=\"\"> <b>Lokasi</b><br> $ruang->letak </p>
                <p class=\"\"> <b>Kapasitas</b><br> $ruang->kapasitas Org </p>
                <p class=\"\"> <b>Luas</b><br> $ruang->luas M2 </p>
                <p class=\"\"> <b>Pengelola</b><br> $ruang->unit </p>
                <p class=\"\"> <b>Fasilitas</b><br> $ruang->fasilitas </p>
            </div>";
        echo $msg;
	}

	private function ref() {
		 $row 	 = $this->rapat_ruang_model->get_data();
		 $config = $this->fc->pagination( site_url("rapat/index/hc1xY"), count($row), 10, '4');
		 $config['cur_tag_open'] = '<li class="active"><a href="#">';
   	     $config['cur_tag_close'] = '</a></li>';
		 $this->pagination->initialize($config);

		 $data['table'] = array_slice($row, $this->uri->segment(4), 10);
		 $data['menu']  = $this->dsw_model->get_menu( $this->uri->segment(1) );
		 $data['bread'] = array('header'=>'Perekaman Ruang Rapat', 'subheader'=>'rapat Admin');
		 $data['view']  = "rapat/v_rapat_ref";
		 $this->load->view('main/utama', $data);
	}

	private function ref_rekam() {
		$ruhi			= 'Rekam'; if ($this->uri->segment(4)=='u') $ruhi = 'Ubah'; if ($this->uri->segment(4)=='h') $ruhi = 'Hapus';
		$data['ruhi']   = $ruhi;

		$data['table']  = $this->rapat_ruang_model->get_row( $this->uri->segment(5) );
		$data['menu']   = $this->dsw_model->get_menu( $this->uri->segment(1) );
		$data['bread']  = array('header'=>'Referensi Ruang Rapat', 'subheader'=>'Rapat Admin');
		$data['view']   = "rapat/v_rapat_rekam_ref";
		// echo '<pre>'; print_r($data['table']); exit;
		$this->load->view('main/utama', $data);
	}

	private function ref_crud() {
		$this->rapat_ruang_model->save2();
		redirect("rapat/index/hc1xY");
	}

	private function cariruang() {
		if ($_POST['search'] == 'search') {
			$this->session->set_userdata('cari', $_POST['cari']);
		} else {
			$this->session->set_userdata('cari', '');
		}
		$this->ref();
	}

	private function pdf( $tgl ) {
		$this->fc->log('Ruang Rapat - Cetak Laporan');
		$data['rpt']= $this->rapat_model->get_cetak_rapat( $tgl);
		$data['sum']=$this->rapat_model->get_sum_cetak_rapat($tgl);
		$data['jbtan'] = $this->rapat_model->get_jbt_cetak_rapat();
		$data['menu']  = $this->dsw_model->get_menu( $this->uri->segment(1) );
		$data['bread'] = array('header'=>'Admin Cuti', 'subheader'=>'Cuti Admin');
		$data['view']  = "rapat/cetak_rapat";
		$this->load->view('rapat/cetak_rapat', $data);

		$html = $this->output->get_output();

		// Convert to PDF
		$this->dompdf->load_html($html);
		$this->dompdf->set_paper('A4','portraite');
		$this->dompdf->render();
		$this->dompdf->stream( 'cetak_rapat'.'.pdf',array('Attachment'=>0) );
		//echo '<pre>'; print_r($data['sum']);
		//echo $tgl;
	}

	private function setting_rapat() {
		$nip 		= $this->uri->segment(3);
		$jabatan 	= str_replace('%20', ' ', $this->uri->segment(4));
		$jam 		= $this->uri->segment(5);
		$query 		= $this->db->query("Update t_rapat_seting Set nip='$nip', jabatan=\"$jabatan\", jam='$jam' ");
		//redirect("rapat/V_rapat_dashboard");
	}

}
