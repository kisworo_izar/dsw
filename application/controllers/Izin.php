<?php
class Izin extends CI_Controller {
	function __construct() {
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
		$this->load->model('izin_model');
		$this->load->model('milea_model');
		$this->load->model('milea_laporan_model');
		$this->dbmilea  = $this->load->database('milea', TRUE);
	}

	function index() {
		// require_once 'assets/plugins/htmlpurifier/HTMLPurifier.auto.php';
		// $config	  = HTMLPurifier_Config::createDefault();
		// $purifier = new HTMLPurifier($config);

		if (! $this->session->userdata('isLoggedIn') ) redirect("login/show_login");

		if (isset($_GET['q']))    $menu = $_GET['q'];    else show_404();
		if (isset($_GET['opt']))  $opt  = $_GET['opt'];	 else $opt  = '';

		if 		(preg_match("~\bhITfo\b~", $menu))	$this->list_();
		else if (preg_match("~\bLeo5i\b~", $menu))	$this->getList_($opt);
		else if (preg_match("~\btNGPu\b~", $menu))	$this->getList($opt);
		else if (preg_match("~\bzlwiH\b~", $menu))	$this->isMatchStatusTanggal_();
		else if (preg_match("~\bhDPMa\b~", $menu))	$this->isMatchStatusTanggal();
		else if (preg_match("~\bfTK59\b~", $menu))	$this->ajuValidation();
		else if (preg_match("~\bxi9Ug\b~", $menu))	$this->aju();
		else if (preg_match("~\bvR3ys\b~", $menu))	$this->setuju();
		else if (preg_match("~\bZ7EbR\b~", $menu))	$this->delete();
		else if (preg_match("~\bX5bIe\b~", $menu))	$this->fileupload();
		else if (preg_match("~\bJDaU8\b~", $menu))	$this->test();
		else show_404();
	}

	private function list_() {
		$this->fc->log('Presensi - Izin');
		$this->load->model('pegawai_model');

		$nip = $this->session->userdata('nip');
		$kdso = $this->session->userdata('kdso');
		$jabatan = $this->session->userdata('jabatan');

		$data['isTUPim'] = ($kdso == '010404') ? TRUE : FALSE;
		$data['atasan'] = $this->pegawai_model->getAtasan((object) array('nip' => $nip, 'kdso' => $kdso, 'jabatan' => $jabatan));
		$data['atasan2'] = $this->pegawai_model->getAtasan($data['atasan']);
		$nIzinBawahan = count($this->izin_model->list_('*', '*', '', $this->session->userdata('nip'), ''));
		$data['isAtasan'] = ($this->pegawai_model->isPejabat($this->session->userdata('jabatan')) or $nIzinBawahan > 0);

		$data['data_peg']  = $this->milea_model->get_data_pegawai($nip);
		$data['hdr']       = $this->milea_model->hdr();
		$data['krj']       = $this->milea_model->harikerjaToHarikalender($nip, 3);

		$data['menu']  = $this->dsw_model->get_menu( $this->uri->segment(1) );
		$data['bread'] = array('header'=>'IZIN', 'subheader'=>'Presensi');
		$data['view']  = "izin/izin_pegawai";

		// echo '<pre>'; print_r($data); exit;
		$this->load->view('main/utama', $data);
	}

	private function getList_($option) {
		switch ($option) {
			case 'semua':
			$izin = $this->izin_model->list_('*', '*', $this->session->userdata('nip'), '', '');
			break;

			case 'proses':
			$izin = $this->izin_model->list_('*', '*', $this->session->userdata('nip'), '', '((setuju_status IS NULL) OR (setuju_status = 1 AND approval = 3))');
			break;

			case 'bawahan_semua':
			$izin = $this->izin_model->list_('*', '*', '', $this->session->userdata('nip'), '');
			break;

			default:
			$izin = $this->izin_model->list_('*', '*', '', $this->session->userdata('nip'), '(setuju_status IS NULL)');
		}

		return $izin;
	}

	private function getList($option) {
		$list_ = $this->getList_($option);

		for ($i=0; $i < count($list_); $i++) { 
			unset($list_[$i]->sdm_nama);
			unset($list_[$i]->sdm_nip);
			unset($list_[$i]->sdm_jabatan);
		}

		echo json_encode($list_);
	}

	/**
     * Fungsi ini untuk mengececk apakah status dan tanggal izin yang diajukan sesuai dengan data presensi yang ada
     */
	private function isMatchStatusTanggal_($status, $tanggalmulai, $tanggalselesai) {
		$json['valid'] = true;

		if (!empty($status) && ($status != 'TMH') && !empty($tanggalmulai) && !empty($tanggalselesai)) {
			$tanggalmulai = $this->fc->ustgl($tanggalmulai);
			$tanggalselesai = $this->fc->ustgl($tanggalselesai);

			/* Validasi Kesesuain jenis izin dg status kehadiran */ 
			$presensis = $this->milea_model->getPresensi($this->session->userdata('nip'), $tanggalmulai, $tanggalselesai);
			if (!empty($presensis)) {
				foreach ($presensis as $presensi) {
					if ($status != $presensi->status) {
						if ($status == 'IP' && $presensi->status == 'TK') {
							continue;
						}
						$json['valid'] = false;
						$json['message'] = 'Status Izin ('.$status.') tidak sama dengan Status Kehadiran pada tanggal '.$presensi->tgl.' ('.$presensi->status.')';
						break;
					}
				}
			}

			/* Validasi Lama izin >= 1 HK */ 
			$jmlHK = $this->milea_model->jumlahHariKerja($tanggalmulai, $tanggalselesai);
			if ($jmlHK < 1) {
				$json['valid'] = false;
				$json['message'] = 'Rentang tanggal izin tidak pada hari kerja';
			}

			/* Validasi Lama izin pribadi max 2 HK */ 
			if ($status == 'IP' && $jmlHK > 2) {
				$json['valid'] = false;
				$json['message'] = 'Lama Izin Pribadi maksimal 2 hari kerja';
			}
		}

		return $json;
	}

	private function isMatchStatusTanggal() {
		echo json_encode($this->isMatchStatusTanggal_($_POST['status'], $_POST['tanggalmulai'], $_POST['tanggalselesai']));
	}

	/* Validasi pengajuan */
	private function ajuValidation($izinInfo) {
		$tanggalmulai = date('Y-m-d', strtotime($izinInfo['tanggalmulai']));
		$tanggalselesai = date('Y-m-d', strtotime($izinInfo['tanggalselesai']));

		/* Kesesuain jenis izin dg status kehadiran + Lama Izin >= 1 HK + Lama izin IP max 2HK */ 
		$isMatchStatusTanggal = $this->isMatchStatusTanggal_($izinInfo['status'], $this->fc->ustgl($tanggalmulai), $this->fc->ustgl($tanggalselesai));
		if (!$isMatchStatusTanggal['valid']) {
			return $isMatchStatusTanggal;
		}

		/* Kesesuaian izin sebelumnya dengan izin sekarang */
		$prevIzin = $this->izin_model->getIzin($izinInfo['prevIzin_id']);
		if (!empty($prevIzin)) {
			if ($prevIzin->nip != $izinInfo['nip']) {
				return ['valid' => false, 'message' => 'Izin sebelumnya tidak diajukan oleh Pegawai'];
			}

			if ($prevIzin->setuju_status == '2') {
				$izinInfo['tanggalmulai'] = $prevIzin->setuju_waktu;
			}
			if ($prevIzin->approval == '2') {
				$izinInfo['tanggalmulai'] = $prevIzin->tanggalapproval;
			}
		}

		/* Batas pengajuan 3HK setelah kejadian atau setelah ditolak, pengecualian untuk TMH  */ 
		$hk = $this->milea_model->jumlahHariKerjaPegawai($izinInfo['nip'], date('Y-m-d', strtotime($izinInfo['tanggalmulai'])), $izinInfo['tanggalsurat']);
		if ($hk > 4 and $izinInfo['status'] != 'TMH') {
			return ['valid' => false, 'message' => 'Pengajuan melebihi 3 Hari Kerja setelah kejadian atau ditolak'];
		}

		/* TMH: tanggal selesai max hari ini. Selain ajudan/TU pim max 1 */
		if ($izinInfo['status'] == 'TMH') {
			if (date('Y-m-d', strtotime($izinInfo['tanggalselesai'])) > date('Y-m-d')) {
				return ['valid' => false, 'message' => 'Tanggal selesai TMH maksimal hari ini'];
			}
			$n_hk = $this->milea_model->jumlahHariKerja($tanggalmulai, $tanggalselesai);
			if (!$izinInfo['isTUPim'] && $n_hk > 1) {
				return ['valid' => false, 'message' => 'Lama TMH maksimal 1 hari'];
			}
		} 

		return ['valid' => true];
	} 

	/* Fungsi ini untuk pengajuan */ 
	private function aju() {
		/* Mapping request */ 
		$kdso = $this->session->userdata('kdso');
		$jabatan = $this->session->userdata('jabatan');
		$namapegawai = $this->session->userdata('nmuser');

		$isTUPim = ($kdso == '010404') ? TRUE : FALSE;
		$nip = $this->session->userdata('nip');
		$iduser = $this->session->userdata('iduser');
		$tglsurat 		= date("Y-m-d");
		$sts 			= $_POST['sts'];  
		if($sts=='CAPB' or $sts=='CAPH' or $sts=='CAPI' or $sts=='CAPM' or $sts=='CAPP' or $sts=='CAPS') $sts = 'CP';
		$tglmulai 		= $this->fc->ustgl($_POST['tglmulai']);  
		$tglselesai		= array_key_exists('tglselesai', $_POST) ? $this->fc->ustgl($_POST['tglselesai']) : $tglmulai;
		if ($sts == 'TMH') {
			foreach ($_POST['datangpulang'] as $datangpulang) {
				if ($datangpulang == 'datang') {
					$tglmulai .= ' '.$_POST['datang_jam'].':00';
				} elseif ($datangpulang == 'pulang') {
					$tglselesai .= ' '.$_POST['pulang_jam'].':00';
				}
			}
		}

		$ket			= $_POST['ket'];
		$penandatangan	= $_POST['penandatangan'];
		$jab			= $_POST['jab'];
		$dok			= $_POST['nmfile'];

		$izinInfo = [
			'prevIzin_id' => array_key_exists('prevIzin_id', $_POST) ? $_POST['prevIzin_id'] : '',
			'nip' => $nip,
			'iduser' => $iduser,
			'tanggalsurat' => date("Y-m-d"),
			'status' => $_POST['sts'],
			'tanggalmulai' => $tglmulai,
			'tanggalselesai' => $tglselesai,
			'keterangan' => $ket,
			'penandatangan' => $penandatangan,
			'jabatan' => $jab,
			'dokumen' => $dok,
			'approval' => '3',
			'isTUPim' => $isTUPim
		];

		/* Validasi Backend */
		$validation = $this->ajuValidation($izinInfo);
		if (!$validation['valid']) {
			echo json_encode($validation);
			return;
		}

		unset($izinInfo['prevIzin_id']);
		unset($izinInfo['isTUPim']);

		if (!empty($_POST['id'])) {
			$izin = $this->izin_model->getIzin($_POST['id']);
			
			if ($izin->nip != $nip) {
				echo json_encode(['valid' => false, 'message' => 'Anda tidak mengajukan Izin ini sebelumnya']);
				return;
			}

			if (!empty($izin->setuju_status)) {
				echo json_encode(['valid' => false, 'message' => 'Izin telah disetujui/ditetapkan']);
				return;
			}

			/* UPDATE */
			$result = $this->izin_model->updateIzin($izinInfo, $_POST['id']);
			$pesan = 'Dari '.$namapegawai.'. Perubahan Pengajuan';
		} else {
			/* INSERT */ 
			$result = $this->izin_model->addIzin($izinInfo);
			$pesan = 'Dari '.$namapegawai.'. Pengajuan Baru';
		}

		if($result > 0)
		{
			$this->load->model('notif_model');
			$this->load->model('pegawai_model');
			$atasan	= $this->pegawai_model->getUserId($_POST['penandatangan']);
			$notifInfo = array('iduser' => $atasan, 
				'judul' => 'Izin Pemberitahuan #'.$result, 
				'pesan' => $pesan, 
				'waktu' => date('Y-m-d H:i:s'), 
				'link' => base_url('izin'), 
				'status' => '0');
			$this->notif_model->add($notifInfo);
			echo json_encode(['valid' => true]);
		}
		else
		{
			echo json_encode(['valid' => false, 'message' => 'Data tidak tersimpan']);
		}
	}

	/* Fungsi ini untuk persetujuan */ 
	private function setuju() {
		$id = $_POST['id'];
		$value = $_POST['value'];
		$catatan = array_key_exists('catatan', $_POST) ? $_POST['catatan'] : null;

		/* Validasi apakah User memiliki otoritas untuk update status Izin */
		if (!$this->izin_model->isAtasan($this->session->userdata('nip'), $id)) {
			echo json_encode(['state' => false, 'message' => 'Anda tidak memiliki otoritas untuk melakukan Persetujuan terhadapa Izin ini!']);
			return;
		}

		/* Validasi setuju_status=null*/
		$izin = $this->izin_model->getIzin($id); 
		if (!empty($izin->setuju_status)) {
			echo json_encode(['state' => false, 'message' => 'Izin telah dilakukan Persetujuan sebelumnya']);
			return;
		}

		/* Update status */
		if ($this->izin_model->setuju($id, $value, $catatan) > 0){
			/* Notification */ 
			$this->load->model('pegawai_model');
			$this->load->model('notif_model');
			$action = '';
			$izin = $this->izin_model->getIzin($id);
			if ($value == '1') {
				$action = 'disetujui';
			} elseif ($value == '2') {
				$action = 'ditolak';
			}
			$notifInfo = array('iduser' => $izin->iduser, 
				'judul' => 'Izin Pemberitahuan', 
				'pesan' => 'Izin Anda (ID: '.$izin->id.') '.$action.' oleh '.$this->pegawai_model->getPegawai($izin->penandatangan)->nmuser, 
				'waktu' => date('Y-m-d H:i:s'), 
				'link' => 'izin', 
				'status' => '0');

			$this->notif_model->add($notifInfo);

			echo json_encode(['state' => true, 'message' => 'Persetujuan berhasil!']);
			return;
		}

		echo json_encode(0);
	}

	/* Delete Izin */
	private function delete() {
		$json = ['state' => true, 'message' => 'Izin berhasil dihapus'];
		$izin = $this->izin_model->getIzin($_POST['id']);
		
		if ($izin->nip != $this->session->userdata('nip')) {
			$json = ['state' => false, 'message' => 'Anda tidak mengajukan Izin ini sebelumnya'];
		}

		if (!empty($izin->setuju_status)) {
			$json = ['state' => false, 'message' => 'Izin tidak boleh dihapus'];
		}

		if (!$this->izin_model->deleteIzin($_POST['id'])) {
			$json = ['state' => false, 'message' => 'Izin tidak berhasil dihapus'];
		}

		echo json_encode($json);
	} 

	private function fileupload() {
		$path = "files/upload_milea/".$this->session->userdata('nip');
		$old  = umask(0);
		is_dir($path) || mkdir($path, 0777, true);
		umask($old);

		$file = $_FILES[ $_POST['name'] ];
		$resp = array('uploaded' => 'Gagal');
		if ($file) {
			move_uploaded_file($file["tmp_name"], $path.'/'.date("d-m-Y").'_'.$file['name']);
			$resp = array('file'=>$file['name']);
		}
		echo json_encode($resp);
	}

	private function test()
	{
		echo json_encode($this->milea_model->harikerjaToHarikalender($_POST['nip'], $_POST['n']));
	}
}