<?php
class Wfh extends CI_Controller {
	function __construct() 
	{
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
		if (! $this->session->userdata('isLoggedIn') ) redirect("login/show_login");
	}

	public function index() 
	{
		$this->main();
	}

	function main()
	{
		$this->fc->log('WFH - Main');

		$data['menu']  = $this->dsw_model->get_menu( $this->uri->segment(1) );
		$data['bread'] = array('header'=>'Work From Home/Office', 'subheader'=>'WFH/WFO');
		$data['view']  = "wfh/wfh";

		$data['wfhnow']  = array();
		$data['wfhhistory']  = array();

		$nip = $this->session->userdata('nip');

		$d = array('nip' => $nip);
		$response = json_decode( $this->CallAPI('GET', $this->config->item('api_server').'wfh', $d), true );

		if ( $response['status'] == 200 and count($response['values']) > 0 ) {
			$data['wfhnow']  = $response['values'][0];
			$data['wfhhistory']  = array_slice($response['values'], 1);

			$d = array('wfh_id' => $data['wfhnow']['id']);
			$response = json_decode( $this->CallAPI('GET', $this->config->item('api_server').'wfh/task', $d), true );

			$data['wfhtasks'] = ($response['status'] == 200) ? $response['values'] : array();
		}

		$this->load->view('main/utama', $data);
	}

	function start()
	{

		$this->load->model('pegawai_model');

		$nip = $this->session->userdata('nip');
		$kdso = $this->session->userdata('kdso');
		$jabatan = $this->session->userdata('jabatan');
		$atasannip = $this->pegawai_model->getAtasan((object) array('nip' => $nip, 'kdso' => $kdso, 'jabatan' => $jabatan))->nip;

		$d = array('nip' => $nip, 'atasannip' => $atasannip);
		$response = $this->CallAPI('POST', $this->config->item('api_server').'wfh/start', $d);

		redirect('/wfh');

	}

	function finish()
	{

		$nip = $this->session->userdata('nip');
		$d = array('nip' => $nip);
		$response = $this->CallAPI('POST', $this->config->item('api_server').'wfh/finish', $d);

		redirect('/wfh');

	}

	function createtask()
	{

		$d = array(
			'wfh_id' 	=> $this->input->post('wfh_id'),
			'title' 	=> $this->input->post('title'),
			'filename' 	=> $this->input->post('filename')
		);

		$response = $this->CallAPI('POST', $this->config->item('api_server').'wfh/task', $d);

		echo $response;
	}

	function upload() {

		if ( empty($_FILES) ) {
			echo json_encode(array('status' => 200, 'values' => ''));
		}

		$nip = $this->session->userdata('nip');
		$path = 'files/upload_milea/'.$nip.'/wfh/';

		$config['upload_path']=$path; 
		$config['allowed_types']='*'; 
		$config['encrypt_name'] = TRUE; 

		$this->load->library('upload',$config); 
		$this->upload->initialize($config);

		if($this->upload->do_upload("file")){ 

			$data = array('upload_data' => $this->upload->data()); 
			$filename= $data['upload_data']['file_name']; 

			echo json_encode(array('status' => 200, 'values' => $filename));

		} else {
			echo json_encode(array('status' => 401, 'values' => $this->upload->display_errors() ));
		}

	}

	function CallAPI($method, $url, $data = false)
	{
		$curl = curl_init();

		switch ($method)
		{
			case "POST":
			curl_setopt($curl, CURLOPT_POST, 1);

			if ($data){
				curl_setopt($curl, CURLOPT_HTTPHEADER, array("Content-type: application/x-www-form-urlencoded"));
				curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
			}
			break;

			case "PUT":
			curl_setopt($curl, CURLOPT_PUT, 1);
			break;

			default:
			if ($data)
				$url = sprintf("%s?%s", $url, http_build_query($data));
		}

		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

		$result = curl_exec($curl);
		curl_close($curl);

		return $result;
	}

}