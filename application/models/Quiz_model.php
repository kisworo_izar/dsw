<?php
class Quiz_model extends CI_Model {

	public function dit_data( $so ) {
		$quest = array();
		if ($so=='00') {
			$query = $this->db->query("Select distinct kdso From quiz Where kdso!='' Order By 1");
			foreach ($query->result_array() as $row) {
				$query = $this->db->query("Select * From quiz Where kdso='". $row['kdso'] ."' Order By Rand()");
				$quest = $this->get_data( $quest, $row['kdso'], $query->result_array() ); 
			}
		} else {
			$query = $this->db->query("Select * From quiz Where kdso='$so' Order By Rand() Limit 10");
			$quest = $this->get_data_dit( $quest, $so, $query->result_array() ); 
		}

		$hasil = array();
		foreach ($this->array_rand( $quest, count($quest) ) as $r) array_push($hasil, $quest[$r]);

		$dit = array('01'=>'Sekretariat', 'Dit. APBN'=>'1', '03'=>'EKONTIM', '04'=>'Dit. PMK', '05'=>'POLHUK HAMKAM & BABUN', '06'=>'Dit. PNBP', '07'=>'Dit. SP', '08'=>'Dit. HPP');
		$nm  = "Olimpiade DJA Cerdas";
		if ($so!='00') $nm = "Latihan Kuis untuk Kategori ". $dit[$so] ." (non skor) ";

		$quiz['questions'] = $hasil;
		$quiz['title'] = $nm;
		$quiz['url'] = 'DSW';
		return $quiz;
	}

	public function get_data( $quest, $kdso, $quiz ) {
		$jatah = array('01'=>3, '02'=>1, '03'=>1, '04'=>1, '05'=>1, '06'=>1, '07'=>1, '08'=>1);
		$jawab = array('a'=>0,'b'=>1,'c'=>2,'d'=>3,'e'=>4);
		foreach ($this->array_rand( $quiz, $jatah[$kdso] ) as $r) {
			$acak = $this->array_jawab( strtolower($quiz[$r]['jawaban']) );
			$arr = array(
				'number' => $kdso,
				'prompt' => $quiz[$r]['soal'],
				'answers'=> array($quiz[$r][$acak['0']], $quiz[$r][$acak['1']], $quiz[$r][$acak['2']], $quiz[$r][$acak['3']]),
				'correct'=> array('index'=> $acak['9'] )
			);
			array_push($quest, $arr);
		}
		return $quest;
	}

	public function get_data_dit( $quest, $kdso, $quiz ) {
		$jawab = array('a'=>0,'b'=>1,'c'=>2,'d'=>3,'e'=>4);
		foreach ($quiz as $row) {
			$acak = $this->array_jawab( strtolower($row['jawaban']) );
			$arr = array(
				'number' => $kdso,
				'prompt' => $row['soal'],
				'answers'=> array($row[ $acak['0'] ], $row[ $acak['1'] ], $row[ $acak['2'] ], $row[ $acak['3'] ]),
				'correct'=> array('index'=> $acak['9'] )
			);
			array_push($quest, $arr);
		}
		return $quest;
	}

	public function array_jawab( $jwb='b' ) {
		$str  = array('a','b','c','d'); 
		$pil  = array('pil_a','pil_b','pil_c','pil_d'); 
		$quiz = $this->array_rand($str, 4);
		$rand = array();  $soal = array();  $jawab = '';
		
		foreach ($quiz as $r) { array_push($rand, $str[$r]); array_push($soal, $pil[$r]); }
		for ($i=0; $i<count($rand) ; $i++)  if ($rand[$i]==$jwb) $jawab = $i;
		$soal['9'] = $jawab;
		return $soal;
	}

	public function array_rand( $quiz, $jml ) {
		$hit = array();  $num = range(0, count($quiz)-1); shuffle($num);
		foreach ($num as $row) array_push($hit, $row);
		return array_slice($hit, 0, $jml);
	}

	public function quiz_submit() {
		$nilai = $_POST['nilai'];
		$waktu = $_POST['waktu'];
		$kdso  = $_POST['kdso'];
		$iduser= $this->session->userdata('iduser');

		$query = $this->db->query("Select nilai, waktu From quiz_submit Where idkuis=1 And iduser='$iduser'");
		$hasil = $query->row_array();

		if (!$hasil) {
			if ($kdso=='00') { 
				$query = $this->db->query("Insert Into quiz_submit (idkuis, iduser, nilai, waktu, aktivitas, tglupdate) Values ('1', '$iduser', $nilai, $waktu, concat(current_timestamp(),' = ',$nilai,';'), current_timestamp())");
			} else {
				$query = $this->db->query("Insert Into quiz_submit (idkuis, iduser, nilai, waktu, aktivitas, tglupdate) Values ('1', '$iduser', 0, $waktu, concat(current_timestamp(),' = ','$nilai *)',';'), current_timestamp())");
			}
		} else {
			if ($kdso=='00') { 
				if (($hasil['nilai']<$nilai) || ($hasil['nilai']==$nilai && $hasil['waktu']>$waktu)) {
					$query = $this->db->query("Update quiz_submit Set nilai=$nilai, waktu=$waktu, aktivitas = concat(aktivitas,'\n',current_timestamp(),' = ',$nilai,' = ',$waktu,';') Where iduser='$iduser'");
				}
			} else {
				$query = $this->db->query("Update quiz_submit Set aktivitas = concat(aktivitas,'\n',current_timestamp(),' = ','$nilai *)',' = ',$waktu,';') Where iduser='$iduser'");
			}
		}
	}

	public function papan_skor() {
		$iduser= $this->session->userdata('iduser');
		$query = $this->db->query("Select * From quiz_submit Where idkuis='1' And iduser='$iduser'");
		$hasil = $query->row_array();
		$aktiv  = explode(';', $hasil['aktivitas']);

		$user = array(); $no=count($aktiv);
		foreach ($aktiv as $row) {
		 	$arr = explode('=', $row); 
            if ($arr[0]!='') {
            	if (count($arr)==2) $arr[2] = 180;
            	$mnt = floor(($arr[2] / 60) % 60); $dtk = $arr[2] % 60;
            	array_push($user, array('no'=>$no, 'tanggal'=>$arr[0], 'nilai'=>$arr[1], 'waktu'=>"$mnt' $dtk\"") );
            }
            $no--;
		} 
		$data['user'] = array_slice($this->fc->array_index($user, 'no'), 0, 10);

		$iduser= $this->session->userdata('iduser');
		$query = $this->db->query("Set @rank:=0;");
		$query = $this->db->query("Select @rank:=@rank+1 as rank, a.iduser, nmuser, nilai, waktu From quiz_submit a Left Join t_user b On a.iduser=b.iduser Where idkuis='1' And nilai>0 Order By nilai Desc, waktu;");
		$hasil = $this->fc->ToArr($query->result_array() ,'iduser');
		foreach ($hasil as $key=>$value) {
			$mnt = floor(($value['waktu'] / 60) % 60); $dtk = $value['waktu'] % 60;
			$hasil[$key]['waktu'] = "$mnt' $dtk\"";
		}

		if (array_key_exists($iduser, $hasil)) $data['rank'] = $hasil[$iduser]['rank'];
		else $data['rank'] = ' ... ';
		
		$data['jml']  = count($hasil);
		$data['skor'] = array_slice($hasil, 0, 10);
		return $data;
	}

	public function get_data2( $quest, $kdso, $quiz ) {
		$jatah = array('01'=>3, '02'=>1, '03'=>1, '04'=>1, '05'=>1, '06'=>1, '07'=>1, '08'=>1);
		$jawab = array('a'=>0,'b'=>1,'c'=>2,'d'=>3,'e'=>4);
		foreach ($this->array_rand( $quiz, $jatah[$kdso] ) as $r) {
			$arr = array(
				'number' => $kdso,
				'prompt' => $quiz[$r]['soal'],
				'answers'=> array($quiz[$r]['pil_a'],$quiz[$r]['pil_b'],$quiz[$r]['pil_c'],$quiz[$r]['pil_d']),
				'correct'=> array('index'=> $jawab[ $quiz[$r]['jawaban'] ])
			);
			array_push($quest, $arr);
		}
		return $quest;
	}

	public function get_data_dit2( $quest, $kdso, $quiz ) {
		$jawab = array('a'=>0,'b'=>1,'c'=>2,'d'=>3,'e'=>4);
		foreach ($quiz as $row) {
			$arr = array(
				'number' => $kdso,
				'prompt' => $row['soal'],
				'answers'=> array($row['pil_a'],$row['pil_b'],$row['pil_c'],$row['pil_d']),
				'correct'=> array('index'=> $jawab[ $row['jawaban'] ])
			);
			array_push($quest, $arr);
		}
		return $quest;
	}
}
