<?php
class Puslay_model extends CI_Model {

	private $dbrevisi;

	public function __construct()
		{
		parent::__construct();
		        //$this->dbrevisi = $this->load->database('dbsatu', TRUE);
				$this->dbrevisi = $this->load->database('revisi'. $this->session->userdata('thang'), TRUE);
				$this->dbref 	= $this->load->database('ref'. $this->session->userdata('thang'), TRUE);
				
		}

	function sql_room() {
		$query = $this->db->query("Select a.*, nmuser, nip From d_forum_room a Left Join t_user b On a.iduser=b.iduser Where idparent=1 Order By 1,2");
		return $query->result_array();
	}

	function sql_head($idparent, $idchild) {
		$query = $this->db->query("Select a.*, nmuser, nip From d_forum_room a Left Join t_user b On a.iduser=b.iduser Where idparent=$idparent and idchild=$idchild");
		return $query->row_array();
	}

	function sql_sub($idparent, $idchild) {
		$query = $this->db->query("Select a.*, concat(idparent,'.',idchild) idkey, '' nmpic, nmuser, nip From d_forum_room a Left Join t_user b On a.iduser=b.iduser Where idparent=$idchild Order By 1,2");
		$hasil = $this->fc->ToArr( $query->result_array(), 'idkey');
		foreach ($hasil as $key=>$value) {
            if ($value['pic']!='') {
                $arr = explode(',', trim($value['pic']));
                for ($i=0; $i<count($arr); $i++) {
                    $query = $this->db->query("Select fullname nama From t_user Where iduser='". $arr[$i] ."'");
                    $nilai = $query->row_array();
                    $hasil[$key]['nmpic'] .= str_replace('PIC ', '', $nilai['nama']);
                    if ( $i<count($arr)-1 ) $hasil[$key]['nmpic'] .= ",";
                }
            }
		}
		return $hasil;
	}

	function sql_thread($idparent, $idchild) {
		$query = $this->db->query("Select idparent, idchild From d_forum_room Where idparent=$idchild");
		$idkey = $this->fc->InVar( $query->result_array(), 'idchild' );

		$query = $this->db->query("Select concat(idparent,'.',idchild) idkey, idparent, idchild, thread, tglpost, a.iduser, nmuser, nip From d_forum a Left Join t_user b On a.iduser=b.iduser Where idparent $idkey and idforum=1 Order By tglpost Desc");
		$hasil = $this->fc->ToArr( $query->result_array(), 'idkey');

		$query = $this->db->query("Select concat(idparent,'.',idchild) idkey, count(*)-1 replies From d_forum Where concat(idparent,'.',idchild) ". $this->fc->InVar($hasil, 'idkey') ." Group By 1");
		$repli = $this->fc->ToArr( $query->result_array(), 'idkey');

		$query = $this->db->query("Select idparent, idchild, concat(idparent, idchild, max(idforum)) idkey From d_forum Where concat(idparent,'.',idchild) ". $this->fc->InVar($hasil, 'idkey') ." Group By 1,2");
		$idkey = $this->fc->InVar( $query->result_array(), 'idkey' );
		$query = $this->db->query("Select concat(idparent,'.',idchild) idkey, a.iduser, nmuser, tglpost From d_forum a Left Join t_user b On a.iduser=b.iduser Where concat(idparent,idchild,idforum) $idkey");
		$post  = $this->fc->ToArr( $query->result_array(), 'idkey');

		foreach ($hasil as $key=>$value) {
			$hasil[ $key ]['replies'] = $repli[ $key ]['replies'];
			$hasil[ $key ]['nmreplies'] = $post[ $key ]['nmuser'];
			$hasil[ $key ]['tglreplies'] = $post[ $key ]['tglpost'];
		}

		$query  = $this->db->query("Select iduser, nmuser, nip, jabatan, a.kdso, nmso From t_user a left join t_so b on a.kdso=b.kdso where iduser " . $this->fc->InVar($hasil, 'iduser'));
		$t_user = $this->fc->ToArr( $query->result_array(), 'iduser');
		$hasil  = $this->fc->Left_Join( $hasil, $t_user, 'iduser,nmuser=nmuser,nip=nip,jabatan=jabatan,nmso=nmso' );
		return $hasil;
	}

	function sql_top( $limit=null ) {	// NEED TO BE DELETED
		if ($limit==null) $limit=5;
		$query = $this->db->query("Select concat(idparent,'.',idchild) idkey, idparent, idchild, count(*) reply From d_forum Group By 1,2,3 Order By reply Desc Limit $limit");
		$hasil = $this->fc->ToArr( $query->result_array(), 'idkey');

		foreach ($hasil as $key=>$value) {
			// Forum Room
			$query = $this->db->query("Select nmroom From d_forum_room where idparent=". $value['idparent'] ." and idchild=". $value['idchild']);
			$room  = $query->row_array();
			$hasil[ $key ]['nmroom'] = $room['nmroom'];

			// Forum Home of Room
			$query = $this->db->query("Select thread From d_forum where idparent=". $value['idparent'] ." and idchild=". $value['idchild'] ." and idforum=1");
			$home  = $query->row_array();
			$hasil[ $key ]['thread'] = $home['thread'];
		}
		return $hasil;
	}

	function sql_last( $limit=null ) {	// NEED TO BE DELETED
		if ($limit==null) $limit=5;
		$query = $this->db->query("Select concat(idparent,'.',idchild) idkey, idparent, idchild, max(tglpost) tglpost From d_forum Group By 1,2,3 Order By tglpost Desc Limit $limit");
		$hasil = $this->fc->ToArr( $query->result_array(), 'idkey');

		foreach ($hasil as $key=>$value) {
			// Forum Room
			$query = $this->db->query("Select nmroom From d_forum_room where idparent=". $value['idparent'] ." and idchild=". $value['idchild']);
			$room  = $query->row_array();
			$hasil[ $key ]['nmroom'] = $room['nmroom'];

			// Forum Home of Room
			$query = $this->db->query("Select thread From d_forum where idparent=". $value['idparent'] ." and idchild=". $value['idchild'] ." and idforum=1");
			$home  = $query->row_array();
			$hasil[ $key ]['thread'] = $home['thread'];
		}
		return $hasil;
	}

	function post_room($idparent) {
		$query = $this->dbsatu->query("Select * From d_forum_room Where idchild=$idparent ");
		if(!$query) return redirect("puslay?q=p4sly");

		return $query->row_array();
	}

	function post_head($idparent, $idchild) {
		$query = $this->dbsatu->query("Select a.*, nmuser, nip, fullname, jabatan, nmso, pic, profilepic From d_forum a Left Join t_user_satu b On a.iduser=b.iduser Left Join t_so c On b.kdso=c.kdso Left Join d_forum_room d On a.idparent=d.idparent and a.idchild=d.idchild Where a.idparent=$idparent and a.idchild=$idchild and idforum=1");
		if(!$query) return redirect("puslay?q=p4sly");

		return $query->row_array();
	}

	function post_reply( $sta, $end, $cari, $pil) {
		$whr  = "";
		if ($cari != '') { $whr = " AND (post LIKE '%$cari%')";}
		$sta = "$sta 00:00:00";
		$end = "$end 23:59:59";
		if($pil) $and = "  AND SUBSTR(terjawab,'1') <> '0'  "; else $and =  " AND SUBSTR(terjawab,'1') = '0' ";
		$query = $this->dbsatu->query("SELECT a.*, nmuser, nip, fullname, jabatan, nmso, profilepic, (999999 - idforum) lastest FROM d_forum a LEFT JOIN t_user_satu b ON a.iduser=b.iduser LEFT JOIN t_so c ON b.kdso=c.kdso WHERE idforum!=1 AND tglpost BETWEEN '$sta' AND '$end' $whr $and ORDER BY a.tglpost DESC");
		if(!$query) return redirect("puslay?q=p4sly");
		$hsl   = $query->result_array();
		return $hsl;
	}

	function room_option($idparent=null, $idchild=null) {
		if ( $idparent!=null ) {
			$query = $this->db->query("Select *, concat(idparent,'.',idchild) kdkey, idparent*1000+idchild urut, 'selected' selected From d_forum_room Where idparent=$idparent And idchild=$idchild");
		} else {
			$query = $this->db->query("Select *, if(idparent=1, concat(idchild,'.0'), concat(idparent,'.',idchild)) kdkey, if(idparent=1, idchild*1000, idparent*1000+idchild) urut, '' selected From d_forum_room Where idparent>0 And kategori='1' Order By 1,2");
		}
		$hasil = $this->fc->ToArr( $query->result_array(), 'kdkey');
		return $this->fc->array_index($hasil, 'urut');
	}

	function get_pic( $pic=null ) {
		$whr = '';  if ($pic!=null) $whr = "and iduser in ('". str_replace(',', "','", $pic) ."')" ;
		$query = $this->db->query("Select iduser, fullname, kdso, concat(kdso,'2',fullname,'Y') kdkey From t_user Where fullname Like 'PIC%' and kdso!='' $whr Order By 3");
		$subdit= $query->result_array();

		if ( count($subdit)==1 ) return $subdit;

		$query = $this->db->query("Select 'dit' iduser, nmso fullname, kdso, concat(kdso,'1','X') kdkey From t_so Where kdso ". $this->fc->InVar($subdit, 'kdso'));
		$dit   = $this->fc->ToArr( $query->result_array(), 'kdkey');
		$hasil = array_merge_recursive($dit, $subdit);
		return $this->fc->array_index($hasil, 'kdkey');
	}

	function ajax_pic( $idkey) {
		$query = $this->db->query("Select pic From d_forum_room Where concat(idparent,'.',idchild)='$idkey'");
		$nilai = $query->row_array();
		$nilai = $this->get_pic( $nilai['pic'] );

        $msg = '';
        if ( count($nilai)>1 ) $msg = '<option hidden>** Pilih PIC untuk Thread **</option>';
        foreach ($nilai as $row) {
	        if ( substr($row['kdkey'],-1)=='1') {
	            if ( substr($row['kdkey'],-1)=='2') $msg .= '</optgroup>'; // Clossing Group
	            $msg .= '<optgroup label="'. $row['fullname'] .'">';
	        } else {
	            $msg .= '<option value="'. $row['iduser'] .'">&nbsp;&nbsp;&nbsp;'.  $row['fullname'] .'</option>';
	        }
	    }
        return $msg;
	}

	function otp($otp){
		if($otp){
			$thang  = $this->session->userdata('thang');
			$qry 	= $this->dbsatu->query("SELECT * FROM d_otp WHERE rev_id LIKE '%$otp%' AND status='1' AND thang='$thang'");
			$otp 	= $qry->result_array();
			return $otp;
		}
	}

	function stsRev($stsRev){
		if($stsRev){
			$thang  = $this->session->userdata('thang');
			$qry 	= $this->dbrevisi->query("  SELECT '$stsRev' kdsatker, rev_id, rev_level, rev_tahap FROM revisi WHERE  rev_tahun='$thang'
    		AND rev_id IN (SELECT rev_id FROM revisi_satker WHERE kdsatker LIKE '%$stsRev%') AND rev_status = '9'");
			$stsRev 	= $qry->result_array();
			return $stsRev;
		}
	}

	function generateRandomString($length = 6) {
	    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < $length; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
    	return $randomString;
	}

	function surel($userid,$typ){
		// echo $typ;exit();
		if($userid or $typ =='form'){
			$qry = $this->dbsatu->query("SELECT * FROM t_user_satu WHERE iduser='$userid' ");
			$dat = $qry->row_array();

			$dat['tgllogin'] = $this->dbsatu->query("SELECT MAX(waktu) tgllogin FROM t_user_satu_aktifitas WHERE iduser='$userid' LIMIT 1")->row_array();
			// print_r($dat['tgllogin']['tgllogin']);exit();

			if($typ == 'chekuser'){
				if($dat){
					$arr['txt1']="User : ".$dat['iduser'].' '.$dat['nmuser']."\nNama: ".$dat['fullname']."\nNIP: ".$dat['nip']."\nNo HP: ". $dat['nohp']."\nEmail: ". $dat['email']."\n"."Login: ".$dat['tgllogin']['tgllogin'];
					$arr['txt2']="Berikut Kami Sampaikan userid dan password SATU DJA (https://satudja.kemenkeu.go.id):\n\n"."User : ".$dat['iduser'].' '.$dat['nmuser']."\nUserid : ".$dat['iduser']."\nPassword Asli : ".$dat['passasli']."\n\n"."Saat login pertama kali, mohon update data user, alamt email kedinasan, nomor HP (untuk sarana Otentifikasi melalui SMS) dan segera lakukan perubahan password.\n\nTerima Kasih";
				} else {
					$arr['txt1'] = "Userid Tidak ada di Database !";
					$arr['txt2'] = "Userid Tidak ada di Database !";
				}

				$arr['userid'] = $userid;
				return $arr;
			}

			if($typ == 'ceklogin'){
				if($dat){
					$arr['txt1'] ="User : ".$dat['iduser'].' '.$dat['nmuser']."\nNama : ".$dat['fullname']."\nNIP : ".$dat['nip']."\n"."Email : ". $dat['email']."\n"."Login : ".$dat['tgllogin']['tgllogin'];
					$arr['txt2'] = "Userid dan password telah terdaftar dalam sistem dan tercatat sudah pernah digunakan sehingga password awal tidak berlaku lagi.\n\nBerikut data pada server kami :\n";
					$arr['txt2'].="\nUser : ".$dat['iduser'].' '.$dat['nmuser']."\nNama : ".$dat['fullname']."\nNIP : ".$dat['nip']."\n"."Email : ". $dat['email']."\n"."Login : ".$dat['tgllogin']['tgllogin'];
					$arr['txt2'] .="\n \nMohon dicek kembali. Apabila lupa password, silahkan gunakan fasilitas 'Lupa Password' pada halaman login. \nLengkapi isian userid, alamat email yang didaftarkan saat pertama kali login, dan kode pengaman. \nUserid dan password akan dikirim oleh sistem ke alamat email yang didaftarkan tersebut.\n\n Terima kasih.";
				} else {
					$arr['txt1'] = "Userid Tidak Ditemukan !";
					$arr['txt2'] = "Userid Tidak Ditemukan !";
				}
				$arr['userid'] = $userid;
				return $arr;
			}

			if($typ == 'passbaru'){

				if($dat){
					$num = $this->generateRandomString();
					$this->dbsatu->query("UPDATE t_user_satu SET passasli='$num', password = md5('$num') WHERE iduser='$userid' ");
					$qry = $this->dbsatu->query("SELECT * FROM t_user_satu WHERE iduser='$userid' ");
					$dat = $qry->row_array();


					$arr['txt1'] ="User : ".$dat['iduser'].' '.$dat['nmuser']."\nNama : ".$dat['nmuser']."\nNIP : ".$dat['nip']."\n"."Email : ". $dat['email']."\n"."Login : ".$dat['tgllogin'];
					$arr['txt2'] = "Berikut kami sampaikan userid dan password Satu DJA (https://satudja.kemenkeu.go.id) :";
					$arr['txt2'].="\nUser : ".$dat['iduser'].' '.$dat['nmuser']."\nUserid : ".$dat['iduser']."\nPassword : ".$dat['passasli'];
					$arr['txt2'] .="\n \n Saat login pertama kali, silahkan update data user, alamat email kedinasan, nomor HP (sebagai sarana otentifikasi melalui SMS) dan segera lakukan perubahan password.\n\nTerima Kasih";
				} else {
					$arr['txt1'] = "Userid Tidak Ditemukan !";
					$arr['txt2'] = "Userid Tidak Ditemukan !";
				}
				$arr['userid'] = $userid;
				return $arr;
			}

			if($typ == 'respas'){
				if($dat){
					$this->dbsatu->query("UPDATE t_user_satu SET password = md5(passasli) WHERE iduser='$userid' ");
					$arr['txt1'] ="User : ".$dat['iduser'].' '.$dat['nmuser']."\nNama : ".$dat['nmuser']."\nNIP : ".$dat['nip']."\n"."Email : ". $dat['email']."\n"."Login : ".$dat['tgllogin'];
					$arr['txt2'] = "Berikut kami sampaikan userid dan password Satu DJA (https://satudja.kemenkeu.go.id) :";
					$arr['txt2'].="\nUser : ".$dat['iduser'].' '.$dat['nmuser']."\nUserid : ".$dat['iduser']."\nPassword : ".$dat['passasli'];
					$arr['txt2'] .="\n \n Saat login pertama kali, silahkan update data user, alamat email kedinasan, nomor HP (sebagai sarana otentifikasi melalui SMS) dan segera lakukan perubahan password.\n\nTerima Kasih";
				} else {
					$arr['txt1'] = "Userid Tidak Ditemukan !";
					$arr['txt2'] = "Userid Tidak Ditemukan !";
				}
				$arr['userid'] = $userid;
				return $arr;
			}

			if($typ == 'form'){
				$arr['txt2'] = "Silahkan lengkapi formulir terlampir.\n\nHarap dilengkapi dengan tanda tangan, kop surat, dan stempel resmi.\n\nTerima Kasih.";
				$arr['txt1']   ="";
				$arr['userid'] = $userid;
				return $arr;
			}

		}
		// echo $userid;

	}

	function add_user($userid){

		if($userid){
			// $db 		=
			$ref 		= $this->load->database('ref'. $this->session->userdata('thang'), TRUE);
			$qry 		= $this->dbsatu->query("SELECT * FROM t_user_satu WHERE iduser='$userid' ")->row_array();
			if($qry) {
				echo "<script>alert('UserID Sudah Pernah Terdaftar, silahkan ubah kewenangan via menu ubah');window.location='puslay?q=p4sly'</script>";
				return;
			}

			$qry 		 		 = $ref->query("SELECT * FROM t_satker WHERE kdsatker='$userid' ");
			$hsl['data'] 		 = $qry->row_array();
		}

		$hsl['data']['hash'] = $this->generateRandomString();

		$qry 		= $this->dbsatu->query("SELECT idusergroup, nmusergroup FROM t_user_group_satu WHERE idusergroup NOT LIKE 'admin%' ");
		$hsl['sel'] = $qry->result_array();

		return $hsl;
	}

	function crud_user(){
		$iduser   = trim($_POST['iduser']);
		$nmuser   = trim($_POST['nmuser']);
		$nip 	  = $_POST['nip'];
		$jab 	  = $_POST['jab'];
		$kddept   = $_POST['kddept'];
		$kdunit   = $_POST['kdunit'];
		$kdsatker = $_POST['kdsatker'];
		$kdlokasi = $_POST['kdlokasi'];
		$nohp     = $_POST['nohp'];
		$email    = $_POST['email'];
		$idusrgrp = $_POST['idusergroup'];
		$passwd   = $_POST['pswd'];
		$jenis 	  = $_POST['jenis'];

		if($jenis == 'edit'){
			$this->dbsatu->query("UPDATE t_user_satu SET iduser=\"$iduser \", nmuser= \"$nmuser\", fullname= \"$nmuser\", nip='$nip', jabatan=\"$jab\", password = md5('$passwd'), idusergroup = '$idusrgrp', kddept='$kddept', kdunit='$kdunit', kdsatker='$kdsatker', kdlokasi = '$kdlokasi', nohp='$nohp', email='$email' WHERE iduser='$iduser' ");
		}

		if($jenis == 'add') {
			$this->dbsatu->query("INSERT INTO t_user_satu (iduser,nmuser,fullname,nip,jabatan,password,idusergroup,kddept,kdunit,kdsatker,kdlokasi,nohp,email,counter,passasli,stslogin,profilepic) VALUES (\"$iduser \", \"$nmuser\", \"$nmuser\", '$nip', \"$jab\", md5('$passwd'), '$idusrgrp', '$kddept', '$kdunit', '$kdsatker', '$kdlokasi', '$nohp', '$email', '1', '$passwd', '0', '999.png') ");
		}
	}

	function edit_user($iduser){
		$qry        = $this->dbsatu->query("SELECT *,'edit' jenis FROM t_user_satu WHERE iduser='$iduser' ");
		$hsl['dat'] = $qry->row_array();

		$qry 		= $this->dbsatu->query("SELECT idusergroup, nmusergroup FROM t_user_group_satu WHERE idusergroup NOT LIKE 'admin%' ");
		$hsl['sel'] = $qry->result_array();

		return $hsl;
	}

	function edit_reply($idparent, $idchild, $idforum){
		
		echo '<pre>';print_r($arr);exit;
	}

	function delete($idparent, $idchild, $idforum){
		$this->dbsatu->query("DELETE FROM d_forum WHERE idparent='$idparent' AND idchild='$idchild' AND idforum='$idforum'");
	}

	function reportKanwil($sta, $end){
		//cari kodesatkernya yg sudah selesai *belum dibatasi per kanwil*//
		$qry 	= $this->dbrevisi->query("SELECT kl_satker,'' nmsatker,'' kppn, kl_surat_no, kl_surat_tgl, kl_satker, rev_ke,rev_jns_revisi, rev_tahap, rev_status, t7_sp_tgl, t7_sp_no FROM revisi WHERE rev_upload_level=3 AND rev_upload_step=3 AND rev_tahap=7 AND rev_status=2 AND pus_tgl>='$sta 00:00:00' AND pus_tgl<='$end 23:59:59' ORDER BY pus_tgl ");
		$arr 	= $this->fc->ToArr($qry->result_array(),'kl_satker');
		$qry 	= $this->dbref->query("Select a.kdsatker, a.nmsatker,a.kdkppn,b.nmkppn FROM t_satker a LEFT JOIN t_kppn b ON a.kdkppn=b.kdkppn WHERE a.kdsatker ".$this->fc->InVar($arr,'kl_satker'));
		$satk   = $this->fc->ToArr($qry->result_array(),'kdsatker');
		foreach($arr as $row) if(array_key_exists($row['kl_satker'],$satk)) { 
			$arr[$row['kl_satker']]['nmsatker'] = $satk[$row['kl_satker']]['nmsatker'];
			$arr[$row['kl_satker']]['kppn']     = $satk[$row['kl_satker']]['nmkppn'];
		}
		// echo '<pre>';print_r($arr);exit;

		// $query = $dbref->query("Select kdsatker,nmsatker kode, nmunit uraian, '2' level From t_unit Where concat(kddept,'.',kdunit) ". $this->fc->InVar($hasil, 'kl_unit') );
		// $unit  = $this->fc->ToArr( $query->result_array(), 'kode');

		// foreach ($hasil as $key=>$value) {
		// 	if (array_key_exists($value['kl_dept'], $dept)) $hasil[$key]['nmdept'] = $dept[ $value['kl_dept'] ]['uraian'];
		// 	if (array_key_exists($value['kl_unit'], $unit)) $hasil[$key]['nmunit'] = $unit[ $value['kl_unit'] ]['uraian'];
		// }


		return $arr;
		// echo "hallo";
	}

}
