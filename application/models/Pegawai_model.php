<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Pegawai_model extends CI_Model
{
	/*
	This function is used to get Pegawai
	*/
	function getPegawai($nip){
		$this->db->select('*');
		$this->db->from('t_user');
		$this->db->where('nip', $nip);

		$res = $this->db->get()->result();

		if (empty($res)) {
			return null;
		}

		foreach ($res as $pegawai) {
			return $pegawai;
		}
	}
	
	/*
	Return user_id of pegawai
	*/
	function getUserId($nip)
	{
		$this->db->select('iduser');
		$this->db->from('t_user');
		$this->db->where('nip', $nip);

		$res = $this->db->get()->result();

		if (empty($res)) {
			return null;
		}

		foreach ($res as $pegawai) {
			return $pegawai->iduser;
		}
	}

	/*
	Return jabatan of pegawai
	*/
	function getJabatan($nip)
	{
		$this->db->select('jabatan');
		$this->db->from('t_user');
		$this->db->where('nip', $nip);

		$res = $this->db->get()->result();

		if (empty($res)) {
			return null;
		}

		foreach ($res as $pegawai) {
			return $pegawai->jabatan;
		}
	}

	/*
	Return: atasan of pegawai
	*/
	function getAtasan($pegawai){
		if (empty($pegawai)) {
			return null;
		}
		
		$atasan_jabatan = array('kepala', 'direktur', 'sekretaris');
		$pejabat = false;

		$this->db->select('*');
		$this->db->from('t_user');

		/* Check if pegawai is pejabat or not */
		foreach ($atasan_jabatan as $prefix) {
			if (stripos($pegawai->jabatan, $prefix) !== false) {
				$pejabat = true;
				break;
			}
		}

		/* Set kdso of atasan */
		if ($pejabat) {
			if (substr($pegawai->kdso, 4, 2) != '00') {
				$this->db->where('kdso', substr($pegawai->kdso, 0, 4) . '00');
			} else if (substr($pegawai->kdso, 2, 2) != '00') {
				$this->db->where('kdso', substr($pegawai->kdso, 0, 2) . '0000');
			} else {
				$this->db->where('kdso', '000000');
			}
		} else {
			$this->db->where('kdso', $pegawai->kdso);
		}

		foreach ($this->db->get()->result() as $atasan_candidate) {
			foreach ($atasan_jabatan as $prefix) {
				if (stripos($atasan_candidate->jabatan, $prefix) > -1) {
					return $atasan_candidate;
				}
			}
		}

		return null;
	}

	/* MENGAMBIL DAFTAR BAWAHAN (NIP) DARI ATASAN $nip */
	function getBawahan($nip)
	{
		$atasan = $this->getPegawai($nip);

		if (empty($atasan)) {
			return null;
		}

		$atasan_jabatan = array('kepala', 'direktur', 'sekretaris');
		$pejabat = false;

		/* Check if pegawai is pejabat or not */
		foreach ($atasan_jabatan as $prefix) {
			if (stripos($atasan->jabatan, $prefix) !== false) {
				$pejabat = true;
				break;
			}
		}

		if (!$pejabat) {
			return null;
		}

		$eselon = '';

		$this->db->select('*');
		$this->db->from('t_user');
		$this->db->where('nip <>', $atasan->nip);

		if (substr($atasan->kdso, 0, 2) == '00') {
			$eselon = '1';
			$this->db->where('kdso LIKE', '%0000');
		} else if (substr($atasan->kdso, 2, 2) == '00') {
			$eselon = '2';
			$this->db->where('kdso LIKE', substr($atasan->kdso, 0, 2) . '%00');
		} else if (substr($atasan->kdso, 4, 2) == '00') {
			$eselon = '3';
			$this->db->where('kdso LIKE', substr($atasan->kdso, 0, 4) . '%');
		} else {
			$eselon = '4';
			$this->db->where('kdso LIKE', $atasan->kdso);
		}

		$query = '';

		switch ($eselon) {
			case '2':
			$query .= "jabatan LIKE '%Pranata Komputer Madya%' OR ";
			break;

			case '3':
			$query .= "jabatan LIKE '%Pranata Komputer%' OR jabatan LIKE '%Analis Anggaran Ahli Madya%' OR ";
			break;

			case '4':
			$query .= "jabatan LIKE '%Analis Anggaran Ahli%' OR ";
			break;
		}

		if ($eselon != '4') {
			foreach ($atasan_jabatan as $prefix) {
				$query .= "jabatan LIKE '%".$prefix."%' OR "; 
			}
		}

		if (!empty($eselon)) {
			$query = substr($query, 0, strlen($query)-3);
			$this->db->where('('.$query.')');
		}

		$bawahans = [];
		foreach ($this->db->get()->result() as $bawahan) {
			$bawahans[] = $bawahan->nip;
		}

		return $bawahans;
	}

	/* RETURN DAFTAR STRUKTUR ORGANISASI (ARRAY) */
	function getSO($kode_so=null)
	{
		$this->dbdsw->select('*');
		$this->dbdsw->from('t_so');
		if (!empty($kode_so)) {
			$this->dbdsw->where('kdso', $kode_so);
		}
		$this->dbdsw->order_by('kdso', 'asc');

		$res = $this->dbdsw->get()->result();

		if (empty($res)) {
			return null;
		}

		if (!empty($kode_so)) {
			return $res[0];
		} else {
			return $res;
		}
	}

	function getSOChilds($kode_so)
	{
		switch ($this->getEselon($kode_so)) {
			case 2:
			$code = substr($kode_so, 0, 2);
			break;

			case 3:
			$code = substr($kode_so, 0, 4);
			break;

			case 4:
			$code = $kode_so;
			break;
			
			default:
			$code = '';
			break;
		}

		$this->dbdsw->select('*');
		$this->dbdsw->from('t_so');
		$this->dbdsw->where('kdso LIKE ', $code.'%');
		$this->dbdsw->order_by('kdso');
		
		return $this->dbdsw->get()->result();
	}

	/* RETURN LEVEL STRUKTUR ORGANISASI */
	function getEselon($kode_so)
	{
		if (substr($kode_so, 0, 6) == '000000') {
			return 1;
		} elseif (substr($kode_so, 2, 4) == '0000') {
			return 2;
		} elseif (substr($kode_so, 4, 2) == '00') {
			return 3;
		} else {
			return 4;
		}
	}

	/* RETURN BOOLEAN */
	function isPejabat($jabatan)
	{
		$atasan_jabatan = array('kepala', 'direktur', 'sekretaris direktorat');
		$pejabat = false;

		/* Check if pegawai is pejabat or not */
		foreach ($atasan_jabatan as $prefix) {
			if (stripos($jabatan, $prefix) !== false) {
				$pejabat = true;
				break;
			}
		}

		return $pejabat;
	}
}

