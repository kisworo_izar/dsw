<?php 
class Harmoni_model extends CI_Model {

	public function get_data() {
		$tahun = date('Y');
		$query = $this->db->query("Select idchild From d_harmoni Where idparent=1 And judul like '%$tahun%'");
		$paren = $query->row();
		
		$sql = "Concat(Lpad(idchild,4,'0'), '.0000.0000') kdkey, idchild pID, judul pName, start pStart, end pEnd, class pClass, '' pLink, 0 pMile, kdso pRes, 0 pComp, 0 pGroup, 0 pParent, 0 pOpen, '' pDepend, iduser pCaption, catatan pNotes, 1 layer";
		$query = $this->db->query("Select $sql From d_harmoni Where idparent=$paren->idchild");
		$induk = $this->fc->ToArr( $query->result_array(), 'pID');

		$sql = "Concat(Lpad(idparent,4,'0'), '.', Lpad(idchild,4,'0'), '.0000') kdkey, idchild pID, judul pName, start pStart, end pEnd, '' pClass, '' pLink, 0 pMile, '' pRes, 0 pComp, 0 pGroup, idparent pParent, 1 pOpen, '' pDepend, iduser pCaption, catatan pNotes, 2 layer";
		$query = $this->db->query("Select $sql From d_harmoni Where idparent ". $this->fc->InVar($induk, 'pID'));
		$anak  = $this->fc->ToArr( $query->result_array(), 'pID');

		$sql = "Concat('0000.', Lpad(idparent,4,'0'), '.', Lpad(idchild,4,'0')) kdkey, idchild pID, judul pName, start pStart, end pEnd, '' pClass, '' pLink, 0 pMile, '' pRes, progres pComp, 0 pGroup, idparent pParent, 1 pOpen, '' pDepend, iduser pCaption, catatan pNotes, 3 layer";
		$query = $this->db->query("Select $sql From d_harmoni Where idparent ". $this->fc->InVar($anak, 'pID'));
		$cucu  = $this->fc->ToArr( $query->result_array(), 'pID');

		$t_so = array ('010000'=>'SETDJA','020000'=>'APBN','030000'=>'A-1','040000'=>'A-2','050000'=>'A-3','060000'=>'PNBP','070000'=>'SP','080000'=>'HPP',);
		foreach ($induk as $key=>$value) { 
			$induk[ $key ]['pRes'] = $t_so[ $value['pRes'] ];
		}
		foreach ($anak as $key=>$value) { 
			$induk[ $value['pParent'] ]['pGroup'] = 1;
			$anak[ $key ]['pClass'] = $induk[ $value['pParent'] ]['pClass'];
		}
		foreach ($cucu as $key=>$value) { 
			$anak[ $value['pParent'] ]['pGroup'] = 1;
			// $anak[ $value['pParent'] ]['pClass'] = 'gtaskblue';
			$cucu[ $key ]['pClass'] = $anak[ $value['pParent'] ]['pClass'];
			$cucu[ $key ]['kdkey'] = substr($anak[ $value['pParent'] ]['kdkey'], 0, 10) . sprintf('%04d', $value['pID']);
		}

		$hasil = array_merge($induk, $anak, $cucu);
		$hasil = $this->fc->array_index($hasil, 'kdkey');
		$hasil = $this->fc->ToArr($hasil, 'kdkey');

		$xml = new SimpleXMLElement('<?xml version="1.0" encoding="UTF-8" standalone="yes"?><project xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"></project>');
		$no=0;
		foreach ($hasil as $row) {
			if ($row['layer']==1) { $no++; $norut = sprintf('%02d', $no).'. '; } else {  $norut = '';}

			$task = $xml->addChild('task');
			$task->addChild('pID', $row['pID']);
			$task->addChild('pName', $norut . $row['pName']);
			$task->addChild('pStart', $row['pStart']);
			$task->addChild('pEnd', $row['pEnd']);
			$task->addChild('pClass', $row['pClass']);
			$task->addChild('pLink', $row['pLink']);
			$task->addChild('pMile', $row['pMile']);
			$task->addChild('pRes', $row['pRes']);
			$task->addChild('pComp', $row['pComp']);
			$task->addChild('pGroup', $row['pGroup']);
			$task->addChild('pParent', $row['pParent']);
			$task->addChild('pOpen', $row['pOpen']);
			$task->addChild('pDepend', $row['pDepend']);
			$task->addChild('pCaption', $row['pCaption']);
			$task->addChild('pNotes', $row['pNotes']);
		}
		$this->load->helper('file');
		$file_name = 'assets/plugins/jsgantt/project.xml';
		write_file($file_name, $xml->asXML());

		return $hasil;
	}

	public function get_grid() {
		$hasil = $this->get_data();
		$nomor = 1; 
		foreach ($hasil as $key=>$value) {
			if ( substr($key,4,4)!='0000' ) { 
				unset( $hasil[$key] );
			} else { 
				// $hasil[$key]['pName'] = sprintf('%02d', $nomor) .". ". $hasil[$key]['pName'];
				$hasil[$key]['nomor'] = sprintf('%02d', $nomor);
				$nomor++;  
			}
		}
		return $hasil;
	}

	public function get_anak( $idparent ) {
		$sql = "Concat(Lpad(idparent,4,'0'), '.', Lpad(idchild,4,'0'), '.0000') kdkey, idchild pID, judul pName, start pStart, end pEnd, '' pClass, '' pLink, 0 pMile, '' pRes, 0 pComp, 0 pGroup, idparent pParent, 1 pOpen, '' pDepend, iduser pCaption, catatan pNotes, 2 layer";
		$query = $this->db->query("Select $sql From d_harmoni Where idparent=$idparent");
		$anak  = $this->fc->ToArr( $query->result_array(), 'pID');

		$sql = "Concat(Lpad($idparent,4,'0'), '.', Lpad(idparent,4,'0'), '.', Lpad(idchild,4,'0')) kdkey, idchild pID, judul pName, start pStart, end pEnd, '' pClass, '' pLink, 0 pMile, '' pRes, progres pComp, 0 pGroup, idparent pParent, 1 pOpen, '' pDepend, iduser pCaption, catatan pNotes, 3 layer";
		$query = $this->db->query("Select $sql From d_harmoni Where idparent ". $this->fc->InVar($anak, 'pID'));
		$cucu  = $this->fc->ToArr( $query->result_array(), 'pID');

		foreach ($cucu as $key=>$value) { 
			$anak[ $value['pParent'] ]['pGroup'] = 1;
		}

		$hasil = array_merge($anak, $cucu);
		$hasil = $this->fc->array_index($hasil, 'kdkey');

		$msg='';
		foreach ($hasil as $row) {
			if ($row['layer']==2) {
			  $msg .= '
		        <tr class="child">
		        	<td id="'. substr($row['kdkey'], 0,4) .'" class="text-center"><small> &nbsp; </small></td>
	                <td id="'. substr($row['kdkey'], 5,4) .'"><strong><small>'. $row['pName'] .'</small></strong></td>
	                <td id="'. $row['pID'] .'"><small> &nbsp; </small></td>
	                <td><strong><small>'. $this->fc->idtgl($row['pStart']) .'</small></strong></td>
	                <td><strong><small>'. $this->fc->idtgl($row['pEnd']) .'</small></strong></td>
                    <td>&nbsp;</td>
	                <td class="Tahapan"><strong><small>'. $row['pNotes'] .'</small></strong></td>
                </tr>
                ';
			}
			if ($row['layer']==3) {
			  $msg .= '
		        <tr class="child">
		        	<td id="'. substr($row['kdkey'], 0,4) .'" class="text-center"><small> &nbsp; </small></td>
	                <td id="'. substr($row['kdkey'], 5,4) .'"><div class="layer-3"><small>'. $row['pName'] .'</small></div></td>
	                <td id="'. $row['pID'] .'"><small> &nbsp; </small></td>
	                <td><small>'. $this->fc->idtgl($row['pStart']) .'</small></td>
	                <td><small>'. $this->fc->idtgl($row['pEnd']) .'</small></td>
                    <td class="text-right"><small>'. $row['pComp'] .'</small></td>
	                <td class="Aktivitas"><small>'. $row['pNotes'] .'</small></td>
                </tr>
                ';
			}
			if ( $row['layer']==2 and $row['pGroup']==0 ) {
			  $msg .= '
		        <tr class="child">
		        	<td id="'. substr($row['kdkey'], 0,4) .'"><small> &nbsp; </small></td>
		        	<td id="'. substr($row['kdkey'], 5,4) .'"><div class="layer-3 text-muted"><small><i> click <b>Rekam</b> untuk tambah aktivitas </i></small></div></td>
	                <td id="'. $row['pID'] .'"><small> &nbsp; </small></td>
	                <td><small> &nbsp; </small></td>
	                <td><small> &nbsp; </small></td>
                    <td class="text-right"><small> &nbsp; </small></td>
	                <td class="Aktivitas"><small> &nbsp; </small></td>
                </tr>
                ';
			}
		}
		return $msg;
	}

	public function crud() {
		$layer   = $_POST['layer']; 
		$action  = $_POST['action']; 
		$idparent= $_POST['idparent'];
		$idchild = $_POST['idchild'];
		$judul 	 = $_POST['judul'];
		$start 	 = $this->fc->ustgl($_POST['start']);
		$end 	 = $this->fc->ustgl($_POST['end']);
		$progres = $_POST['progres'];
		$catatan = $_POST['catatan'];
		
		if ($layer=='Peraturan' or $layer=='Tahapan' or $progres==null) $progres=0;

		if ($action=='Rekam' and $layer=='Aktivitas') {
			$sql = "insert into d_harmoni (idparent,judul,start,end,progres,catatan) values ($idchild,'$judul','$start','$end',$progres,'$catatan')";
			$query = $this->db->query( $sql );		
		}
		if ($action=='Ubah') {
			$sql = "update d_harmoni set judul='$judul', start='$start', end='$end', progres=$progres, catatan='$catatan' where idchild=$idchild";
			$query = $this->db->query( $sql );		
		}
		if ($action=='Hapus' and $layer=='Aktivitas') {
			$sql = "delete from d_harmoni where idchild=$idchild";
			$query = $this->db->query( $sql );		
		}
		return;
	}
}
