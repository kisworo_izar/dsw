<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Referensi_model extends CI_Model {

	private $sp15 = '<span style="padding-left: 15px">', $sp30 = '<span style="padding-left: 30px">',
			$sp45 = '<span style="padding-left: 45px">', $sp60 = '<span style="padding-left: 60px">',
			$sp99 = '</span>', $color = 'color: #2E6CB5;';

	function get_depuni() {
		$ref = $this->load->database('ref'. $this->session->userdata('thang'), TRUE);
		$whr = "Where ". $this->session->userdata('whrdept');

		$table = array(); $arr = array();
		$qry = $ref->query("Select kddept id, kddept kode, nmdept uraian, 1 as pilih From t_dept $whr Order By 1");
		foreach ($qry->result_array() as $nil) $arr[ $nil['id'] ] = $nil;
		$table['dept'] = $this->fc->array_index($arr, 'id');

		$arr = array();
		$qry = $ref->query("Select concat(kddept,'.',kdunit) id, concat(kddept,'.',kdunit) kode, nmunit uraian, 1 as pilih From t_unit $whr Order By 1");
		foreach ($qry->result_array() as $nil) $arr[ $nil['id'] ] = $nil;
		$table['unit'] = $this->fc->array_index($arr, 'id');

		return $table;
	}

	function get_prinas_prod( $kddept ) {
		$ref = $this->load->database('ref'. $this->session->userdata('thang'), TRUE);

		$tag = $this->tag_prinas( $kddept );
		$tagoutp = $tag['outp']; $tagprio = $tag['prio']; $tagnawa = $tag['nawa']; $tagjanj = $tag['janj']; $tagtema = $tag['tema'];
		// return $tag;

		if ($tagoutp) {
			$arr = array();
			$qry = $ref->query("Select concat('kp',kdkp) recid, '' kode, concat(kdkp,'.',kdproy) kd, concat('(',kdproy,') ',nmproy) uraian From t_priproy Where kdproy ". $this->fc->InVar($tagoutp, 'kdproy') ." Order By kdkp,kdproy");
			foreach ($qry->result_array() as $row) {
				if (! array_key_exists($row['recid'], $arr)) {
					$arr[ $row['recid'] ] = array();
					array_push($arr[ $row['recid'] ], array('recid'=>'pn00', 'kode'=>'', 'uraian'=>'<b><i>Prioritas Proyek</i></b>', 'w2ui'=>array('style'=>'color: grey; text-indent: 80px')));
				}
				$row['w2ui'] = array('style'=>'color: grey; text-indent: 80px');
				array_push($arr[ $row['recid'] ], $row);
				if (array_key_exists($row['kd'], $tagprio))
					foreach ($tagprio[$row['kd']] as $tmp) array_push($arr[ $row['recid'] ], $tmp);
			}

			$ars = array();
			$qry = $ref->query("Select concat('kp',kdkp) id, concat('pp',kdpp) recid, '' kode, concat(kdkp,'. ',nmkp) uraian From t_prigiat Where kdkp ". $this->fc->InVar($tagoutp, 'kdkp') ." Order By 1,2");
			foreach ($qry->result_array() as $row) {
				if (! array_key_exists($row['recid'], $ars)) {
					$ars[ $row['recid'] ] = array();
					array_push($ars[ $row['recid'] ], array('recid'=>'pn00', 'kode'=>'', 'uraian'=>'<b><i>Prioritas Kegiatan</i></b>', 'w2ui'=>array('style'=>'text-indent: 50px')));
				}
				if (array_key_exists($row['id'], $arr)) $str = $arr[ $row['id'] ]; else $str = array();
				$row['w2ui'] = array('style'=>'text-indent: 50px');
				array_push($ars[ $row['recid'] ], $row);
				if ($str) foreach ($str as $val) array_push($ars[ $row['recid'] ], $val);
			}

			$art = array();
			$qry = $ref->query("Select concat('pp',kdpp) id, concat('pn',kdpn) recid, '' kode, concat('<b>',kdpp,'. ',nmpp,'</b>') uraian From t_priprog Where kdpp ". $this->fc->InVar($tagoutp, 'kdpp') ." Order By 1,3");
			foreach ($qry->result_array() as $row) {
				if (! array_key_exists($row['recid'], $art)) {
					$art[ $row['recid'] ] = array();
					array_push($art[ $row['recid'] ], array('recid'=>'pn00', 'kode'=>'', 'uraian'=>'<b><i>Prioritas Program</i></b>', 'w2ui'=>array('style'=>'text-indent: 20px')));
				}
				if (array_key_exists($row['id'], $ars)) $str = $ars[ $row['id'] ]; else $str = array();
				$row['w2ui'] = array('style'=>'text-indent: 20px');
				array_push($art[ $row['recid'] ], $row);
				if ($str) foreach ($str as $val) array_push($art[ $row['recid'] ], $val);
			}
		} else {
			$art = array();
		}

			$tbl = array();
			array_push($tbl, array('recid'=>'pn00', 'kode'=>'', 'uraian'=>'<b>PRIORITAS NASIONAL</b>', 'w2ui'=>''));
			$qry = $ref->query("Select concat('pn',kdpn) recid, concat('<b>',kdpn,'</b>') kode, concat('<b>',upper(nmpn),'</b>') uraian From t_prinas Order By 1");
			foreach ($qry->result_array() as $row) {
				if (array_key_exists($row['recid'], $art)) $str = $art[ $row['recid'] ]; else $str = array();
				$row['w2ui'] = array('children' => $str);
				array_push($tbl, $row);
			}

		$table['prioritas'] = $tbl;


		$tbl = array();
		$qry = $ref->query("Select kdnawacita recid, kdnawacita kode, nmnawacita uraian From t_nawacita Order By 1");
		foreach ($qry->result_array() as $row) {
			$kode = $row['kode']; $str = ''; $txt = '';
			if (array_key_exists($kode, $tagnawa)) $str = $tagnawa[$kode]; else $txt = 'style="padding-left: 15px"';
			array_push($tbl, array('recid'=>$row['recid'], 'kode'=>"<span $txt>$kode</span>", 'uraian'=>$row['uraian'], 'w2ui'=>array('style'=>'font-weight: bold;', 'children'=>$str)));
		}
		$table['nawacita'] = $tbl;

		$tbl = array();
		$qry = $ref->query("Select kdjanpres recid, kdjanpres kode, nmjanpres uraian From t_janpres Order By 1");
		foreach ($qry->result_array() as $row) {
			$kode = $row['kode']; $str = ''; $txt = '';
			if (array_key_exists($kode, $tagjanj)) $str = $tagjanj[$kode]; else $txt = 'style="padding-left: 15px"';
			array_push($tbl, array('recid'=>$row['recid'], 'kode'=>"<span $txt>$kode</span>", 'uraian'=>$row['uraian'], 'w2ui'=>array('style'=>'font-weight: bold;', 'children'=>$str)));
		}
		$table['janpres'] = $tbl;

		$tbl = array();
		$qry = $ref->query("Select kdtema recid, kdtema kode, nmtema uraian From t_tema Order By 1");
		foreach ($qry->result_array() as $row) {
			$kode = $row['kode']; $str = ''; $txt = '';
			if (array_key_exists($kode, $tagtema)) $str = $tagtema[$kode]; else $txt = 'style="padding-left: 15px"';
			array_push($tbl, array('recid'=>$row['recid'], 'kode'=>"<span $txt>$kode</span>", 'uraian'=>$row['uraian'], 'w2ui'=>array('style'=>'font-weight: bold;', 'children'=>$str)));
		}
		$table['tema'] = $tbl;
		$table['list'] = $table;
		return $table;
	}

	function tag_prinas_prod( $kddept ) {
		$ref  = $this->load->database('ref'. $this->session->userdata('thang'), TRUE);
		$tag  = array(); $tagprio = array(); $tagnawa = array(); $tagjanj = array(); $tagtema = array();
		$wi1  = array('style'=>"$this->color font-weight: bold; text-indent: 20px");
		$wi2  = array('style'=>"$this->color font-weight: bold; text-indent: 53px");
		$wi3  = array('style'=>"$this->color text-indent: 53px");
		$wi4  = array('style'=>"$this->color text-indent: 93px");

		$whr  = "WHERE kddept='$kddept' AND ". $this->session->userdata('whrdept');
		$qry  = $ref->query("Select concat(kddept,'.',kdunit,'.',kdprogram) kode, nmprogram uraian From t_program $whr");
		$prog = $this->fc->ToArr($qry->result_array(), 'kode');
		$qry  = $ref->query("Select concat(kddept,'.',kdunit,'.',kdprogram) kdprogram, kdgiat kode, nmgiat uraian From t_giat $whr");
		$giat = $this->fc->ToArr($qry->result_array(), 'kode');

		$whr  = "Where kdgiat ". $this->fc->InVar($giat, 'kode');
		if ($this->thang >= '2022') 
			$qry  = $ref->query("Select kdgiat, concat(kdgiat,'.',kdoutput,'.',kdsoutput) kdsoutput, nmsoutput uraian, concat(kdkp,'.',kdproy) kode, kdpn, kdpp, kdkp, kdproy From t_soutput $whr And kdpn>='1' And aktif=1 Order By kdpn,kdpp,kdkp,kdproy");
		elseif ($this->thang == '2021') 
			$qry  = $ref->query("Select kdgiat, concat(kdgiat,'.',kdoutput,'.',kdsoutput) kdsoutput, nmsoutput uraian, concat(kdkp,'.',kdproy) kode, kdpn, kdpp, kdkp, kdproy From t_soutput $whr And kdpn>='1' Order By kdpn,kdpp,kdkp,kdproy");
		else
			$qry  = $ref->query("Select kdgiat, concat(kdgiat,'.',kdoutput) kdoutput, nmoutput uraian, concat(kdkp,'.',kdproy) kode, kdpn, kdpp, kdkp, kdproy From t_output $whr And kdpn>='1' Order By kdpn,kdpp,kdkp,kdproy");

		$str  = 'string ';
		foreach ($qry->result_array() as $row) {
			$kode = $row['kode']; $kdgiat = $row['kdgiat']; $kdprogram = $giat[$kdgiat]['kdprogram'];
			if (! array_key_exists($kode, $tagprio)) $tagprio[$kode] = array();
			if (! strpos($str, $kdprogram)) {
				array_push($tagprio[$kode], array('recid'=>'000', 'kode'=>'', 'uraian'=>$prog[$kdprogram]['kode'] .' '. $prog[$kdprogram]['uraian'], 'w2ui'=>$wi2));
				$str .= $kdprogram . ' ';
			}
			if (! strpos($str, $kdgiat)) {
				array_push($tagprio[$kode], array('recid'=>'000', 'kode'=>'', 'uraian'=>$giat[$kdgiat]['kode'] .' '. $giat[$kdgiat]['uraian'], 'w2ui'=>$wi4));
				$str .= $kdgiat . ' ';
			}
			array_push($tagprio[$kode], array('recid'=>'000', 'kode'=>'', 'uraian'=>$row['kdoutput'] .' '. $row['uraian'], 'w2ui'=>$wi4));
		}

		if ($this->thang >= '2022') 
			$qry  = $ref->query("Select kdgiat, concat(kdgiat,'.',kdoutput,'.',kdsoutput) kdsoutput, nmsoutput uraian, kdnawacita kode From t_soutput $whr And kdnawacita<>'00' And aktif=1 Order By kode,kdoutput");
		elseif ($this->thang == '2021') 
			$qry  = $ref->query("Select kdgiat, concat(kdgiat,'.',kdoutput,'.',kdsoutput) kdsoutput, nmsoutput uraian, kdnawacita kode From t_soutput $whr And kdnawacita<>'00' Order By kode,kdoutput");
		else
			$qry  = $ref->query("Select kdgiat, concat(kdgiat,'.',kdoutput) kdoutput, nmoutput uraian, kdnawacita kode From t_output $whr And kdnawacita<>'00' Order By kode,kdoutput");

		$str  = 'string ';
		foreach ($qry->result_array() as $row) {
			$kode = $row['kode']; $kdgiat = $row['kdgiat']; $kdprogram = $giat[$kdgiat]['kdprogram'];
			if (! array_key_exists($kode, $tagnawa)) $tagnawa[$kode] = array();
			if (! strpos($str, $kdprogram)) {
				$str .= $kdprogram . ' ';
				array_push($tagnawa[$kode], array('recid'=>'000', 'kode'=>'', 'uraian'=>$prog[$kdprogram]['kode'] .' '. $prog[$kdprogram]['uraian'], 'w2ui'=>$wi1));
			}
			if (! strpos($str, $kdgiat)) {
				$str .= $kdgiat . ' ';
				array_push($tagnawa[$kode], array('recid'=>'000', 'kode'=>'', 'uraian'=>$giat[$kdgiat]['kode'] .' '. $giat[$kdgiat]['uraian'], 'w2ui'=>$wi2));
			}
			array_push($tagnawa[$kode], array('recid'=>'000', 'kode'=>'', 'uraian'=>$row['kdoutput'] .' '. $row['uraian'], 'w2ui'=>$wi3));
		}

		if ($this->thang >= '2022') 
			$qry  = $ref->query("Select kdgiat, concat(kdgiat,'.',kdoutput,'.',kdsoutput) kdsoutput, nmsoutput uraian, kdjanpres kode From t_soutput $whr And kdjanpres<>'000' And aktif=1 Order By 2");
		elseif ($this->thang == '2021') 
			$qry  = $ref->query("Select kdgiat, concat(kdgiat,'.',kdoutput,'.',kdsoutput) kdsoutput, nmsoutput uraian, kdjanpres kode From t_soutput $whr And kdjanpres<>'000' Order By 2");
		else
			$qry  = $ref->query("Select kdgiat, concat(kdgiat,'.',kdoutput) kdoutput, nmoutput uraian, kdjanpres kode From t_output $whr And kdjanpres<>'000' Order By 2");

		$str  = 'string ';
		foreach ($qry->result_array() as $row) {
			$kode = $row['kode']; $kdgiat = $row['kdgiat']; $kdprogram = $giat[$kdgiat]['kdprogram'];
			if (! array_key_exists($kode, $tagjanj)) $tagjanj[$kode] = array();
			if (! strpos($str, $kdprogram)) {
				$str .= $kdprogram . ' ';
				array_push($tagjanj[$kode], array('recid'=>'000', 'kode'=>'', 'uraian'=>$prog[$kdprogram]['kode'] .' '. $prog[$kdprogram]['uraian'], 'w2ui'=>$wi1));
			}
			if (! strpos($str, $kdgiat)) {
				$str .= $kdgiat . ' ';
				array_push($tagjanj[$kode], array('recid'=>'000', 'kode'=>'', 'uraian'=>$giat[$kdgiat]['kode'] .' '. $giat[$kdgiat]['uraian'], 'w2ui'=>$wi2));
			}
			array_push($tagjanj[$kode], array('recid'=>'000', 'kode'=>'', 'uraian'=>$row['kdoutput'] .' '. $row['uraian'], 'w2ui'=>$wi3));
		}

		if ($this->thang >= '2022') 
			$qry  = $ref->query("Select kdgiat, concat(kdgiat,'.',kdoutput,'.',kdsoutput) kdsoutput, nmsoutput uraian, kdtema kode From t_soutput $whr And kdtema<>'000' And aktif=1 Order By 4,2");
		elseif ($this->thang == '2021') 
			$qry  = $ref->query("Select kdgiat, concat(kdgiat,'.',kdoutput,'.',kdsoutput) kdsoutput, nmsoutput uraian, kdtema kode From t_soutput $whr And kdtema<>'000' Order By 4,2");
		else
			$qry  = $ref->query("Select kdgiat, concat(kdgiat,'.',kdoutput) kdoutput, nmoutput uraian, kdtema kode From t_output $whr And kdtema<>'000' Order By 4,2");
		
		$str  = 'string ';
		foreach ($qry->result_array() as $row) {
			$kode = $row['kode']; $kdgiat = $row['kdgiat']; $kdprogram = $giat[$kdgiat]['kdprogram'];
			if (! array_key_exists($kode, $tagtema)) $tagtema[$kode] = array();
			if (! strpos($str, $kdprogram)) {
				$str .= $kdprogram . ' ';
				array_push($tagtema[$kode], array('recid'=>'000', 'kode'=>'', 'uraian'=>$prog[$kdprogram]['kode'] .' '. $prog[$kdprogram]['uraian'], 'w2ui'=>$wi1));
			}
			if (! strpos($str, $kdgiat)) {
				$str .= $kdgiat . ' ';
				array_push($tagtema[$kode], array('recid'=>'000', 'kode'=>'', 'uraian'=>$giat[$kdgiat]['kode'] .' '. $giat[$kdgiat]['uraian'], 'w2ui'=>$wi2));
			}
			array_push($tagtema[$kode], array('recid'=>'000', 'kode'=>'', 'uraian'=>$row['kdoutput'] .' '. $row['uraian'], 'w2ui'=>$wi3));
		}

		$tag['outp'] = $tagoutp;
		$tag['prio'] = $tagprio;
		$tag['nawa'] = $tagnawa;
		$tag['janj'] = $tagjanj;
		$tag['tema'] = $tagtema;
		return $tag;
	}

	function get_prinas( $kddept ) {
		$ref = $this->load->database('ref'. $this->session->userdata('thang'), TRUE);

		// $tag = $this->tag_prinas( $kddept );
		// $tagoutp = $tag['outp']; $tagprio = $tag['prio']; $tagnawa = $tag['nawa']; $tagjanj = $tag['janj']; $tagtema = $tag['tema'];
		// return $tag;
		$tagoutp = $tagprio = $tagnawa =$tagjanj = $tagtema = array();

		if ($tagoutp) {
			$arr = array();
			$qry = $ref->query("Select concat('kp',kdkp) recid, '' kode, concat(kdkp,'.',kdproy) kd, concat('(',kdproy,') ',nmproy) uraian From t_priproy Where kdproy ". $this->fc->InVar($tagoutp, 'kdproy') ." Order By kdkp,kdproy");
			foreach ($qry->result_array() as $row) {
				if (! array_key_exists($row['recid'], $arr)) {
					$arr[ $row['recid'] ] = array();
					array_push($arr[ $row['recid'] ], array('recid'=>'pn00', 'kode'=>'', 'uraian'=>'<b><i>Prioritas Proyek</i></b>', 'w2ui'=>array('style'=>'color: grey; text-indent: 80px')));
				}
				$row['w2ui'] = array('style'=>'color: grey; text-indent: 80px');
				array_push($arr[ $row['recid'] ], $row);
				if (array_key_exists($row['kd'], $tagprio))
					foreach ($tagprio[$row['kd']] as $tmp) array_push($arr[ $row['recid'] ], $tmp);
			}

			$ars = array();
			$qry = $ref->query("Select concat('kp',kdkp) id, concat('pp',kdpp) recid, '' kode, concat(kdkp,'. ',nmkp) uraian From t_prigiat Where kdkp ". $this->fc->InVar($tagoutp, 'kdkp') ." Order By 1,2");
			foreach ($qry->result_array() as $row) {
				if (! array_key_exists($row['recid'], $ars)) {
					$ars[ $row['recid'] ] = array();
					array_push($ars[ $row['recid'] ], array('recid'=>'pn00', 'kode'=>'', 'uraian'=>'<b><i>Prioritas Kegiatan</i></b>', 'w2ui'=>array('style'=>'text-indent: 50px')));
				}
				if (array_key_exists($row['id'], $arr)) $str = $arr[ $row['id'] ]; else $str = array();
				$row['w2ui'] = array('style'=>'text-indent: 50px');
				array_push($ars[ $row['recid'] ], $row);
				if ($str) foreach ($str as $val) array_push($ars[ $row['recid'] ], $val);
			}

			$art = array();
			$qry = $ref->query("Select concat('pp',kdpp) id, concat('pn',kdpn) recid, '' kode, concat('<b>',kdpp,'. ',nmpp,'</b>') uraian From t_priprog Where kdpp ". $this->fc->InVar($tagoutp, 'kdpp') ." Order By 1,3");
			foreach ($qry->result_array() as $row) {
				if (! array_key_exists($row['recid'], $art)) {
					$art[ $row['recid'] ] = array();
					array_push($art[ $row['recid'] ], array('recid'=>'pn00', 'kode'=>'', 'uraian'=>'<b><i>Prioritas Program</i></b>', 'w2ui'=>array('style'=>'text-indent: 20px')));
				}
				if (array_key_exists($row['id'], $ars)) $str = $ars[ $row['id'] ]; else $str = array();
				$row['w2ui'] = array('style'=>'text-indent: 20px');
				array_push($art[ $row['recid'] ], $row);
				if ($str) foreach ($str as $val) array_push($art[ $row['recid'] ], $val);
			}
		} else {
			$art = array();
		}

			$tbl = array();
			array_push($tbl, array('recid'=>'pn00', 'kode'=>'', 'uraian'=>'<b>PRIORITAS NASIONAL</b>', 'w2ui'=>''));
			$qry = $ref->query("Select concat('pn',kdpn) recid, concat('<b>',kdpn,'</b>') kode, concat('<b>',upper(nmpn),'</b>') uraian From t_prinas Order By 1");
			foreach ($qry->result_array() as $row) {
				if (array_key_exists($row['recid'], $art)) $str = $art[ $row['recid'] ]; else $str = array();
				$row['w2ui'] = array('children' => $str);
				array_push($tbl, $row);
			}

		$table['prioritas'] = $tbl;


		$tbl = array();
		$qry = $ref->query("Select kdnawacita recid, kdnawacita kode, nmnawacita uraian From t_nawacita Order By 1");
		foreach ($qry->result_array() as $row) {
			$kode = $row['kode']; $str = ''; $txt = '';
			if (array_key_exists($kode, $tagnawa)) $str = $tagnawa[$kode]; else $txt = 'style="padding-left: 15px"';
			array_push($tbl, array('recid'=>$row['recid'], 'kode'=>"<span $txt>$kode</span>", 'uraian'=>$row['uraian'], 'w2ui'=>array('style'=>'font-weight: bold;', 'children'=>$str)));
		}
		$table['nawacita'] = $tbl;

		$tbl = array();
		$qry = $ref->query("Select kdjanpres recid, kdjanpres kode, nmjanpres uraian From t_janpres Order By 1");
		foreach ($qry->result_array() as $row) {
			$kode = $row['kode']; $str = ''; $txt = '';
			if (array_key_exists($kode, $tagjanj)) $str = $tagjanj[$kode]; else $txt = 'style="padding-left: 15px"';
			array_push($tbl, array('recid'=>$row['recid'], 'kode'=>"<span $txt>$kode</span>", 'uraian'=>$row['uraian'], 'w2ui'=>array('style'=>'font-weight: bold;', 'children'=>$str)));
		}
		$table['janpres'] = $tbl;

		$tbl = array();
		$qry = $ref->query("Select kdtema recid, kdtema kode, nmtema uraian From t_tema Order By 1");
		foreach ($qry->result_array() as $row) {
			$kode = $row['kode']; $str = ''; $txt = '';
			if (array_key_exists($kode, $tagtema)) $str = $tagtema[$kode]; else $txt = 'style="padding-left: 15px"';
			array_push($tbl, array('recid'=>$row['recid'], 'kode'=>"<span $txt>$kode</span>", 'uraian'=>$row['uraian'], 'w2ui'=>array('style'=>'font-weight: bold;', 'children'=>$str)));
		}
		$table['tema'] = $tbl;
		$table['list'] = $table;
		return $table;
	}

	function tag_prinas( $kddept ) {
		$ref  = $this->load->database('ref'. $this->session->userdata('thang'), TRUE);
		$tag  = array(); $tagprio = array(); $tagnawa = array(); $tagjanj = array(); $tagtema = array();
		$wi1  = array('style'=>"$this->color font-weight: bold; text-indent: 20px");
		$wi2  = array('style'=>"$this->color font-weight: bold; text-indent: 53px");
		$wi3  = array('style'=>"$this->color text-indent: 53px");
		$wi4  = array('style'=>"$this->color text-indent: 93px");

		$whr  = "WHERE kddept='$kddept' AND ". $this->session->userdata('whrdept');
		$qry  = $ref->query("Select concat(kddept,'.',kdunit,'.',kdprogram) kode, nmprogram uraian From t_program $whr");
		$prog = $this->fc->ToArr($qry->result_array(), 'kode');
		$qry  = $ref->query("Select concat(kddept,'.',kdunit,'.',kdprogram) kdprogram, kdgiat kode, nmgiat uraian From t_giat $whr");
		$giat = $this->fc->ToArr($qry->result_array(), 'kode');

		$whr  = "Where kdgiat ". $this->fc->InVar($giat, 'kode');
		if ($this->thang >= '2022') 
			$qry  = $ref->query("Select kdgiat, concat(kdgiat,'.',kdoutput,'.',kdsoutput) kdsoutput, nmsoutput uraian, concat(kdkp,'.',kdproy) kode, kdpn, kdpp, kdkp, kdproy From t_soutput $whr And kdpn>='1' And aktif=1 Order By kdpn,kdpp,kdkp,kdproy");
		elseif ($this->thang == '2021') 
			$qry  = $ref->query("Select kdgiat, concat(kdgiat,'.',kdoutput,'.',kdsoutput) kdsoutput, nmsoutput uraian, concat(kdkp,'.',kdproy) kode, kdpn, kdpp, kdkp, kdproy From t_soutput $whr And kdpn>='1' Order By kdpn,kdpp,kdkp,kdproy");
		else
			$qry  = $ref->query("Select kdgiat, concat(kdgiat,'.',kdoutput) kdoutput, nmoutput uraian, concat(kdkp,'.',kdproy) kode, kdpn, kdpp, kdkp, kdproy From t_output $whr And kdpn>='1' Order By kdpn,kdpp,kdkp,kdproy");

		$str  = 'string ';
		foreach ($qry->result_array() as $row) {
			$kode = $row['kode']; $kdgiat = $row['kdgiat']; $kdprogram = $giat[$kdgiat]['kdprogram'];
			if (! array_key_exists($kode, $tagprio)) $tagprio[$kode] = array();
			if (! strpos($str, $kdprogram)) {
				array_push($tagprio[$kode], array('recid'=>'000', 'kode'=>'', 'uraian'=>$prog[$kdprogram]['kode'] .' '. $prog[$kdprogram]['uraian'], 'w2ui'=>$wi2));
				$str .= $kdprogram . ' ';
			}
			if (! strpos($str, $kdgiat)) {
				array_push($tagprio[$kode], array('recid'=>'000', 'kode'=>'', 'uraian'=>$giat[$kdgiat]['kode'] .' '. $giat[$kdgiat]['uraian'], 'w2ui'=>$wi4));
				$str .= $kdgiat . ' ';
			}
			array_push($tagprio[$kode], array('recid'=>'000', 'kode'=>'', 'uraian'=>$row['kdoutput'] .' '. $row['uraian'], 'w2ui'=>$wi4));
		}
		// echo 'Hallo';
		// echo '<pre>'; print_r($tagprio);
		// echo $this->fc->browse($tagprio); exit;

		if ($this->thang >= '2022') 
			$qry  = $ref->query("Select kdgiat, concat(kdgiat,'.',kdoutput,'.',kdsoutput) kdsoutput, nmsoutput uraian, kdnawacita kode From t_soutput $whr And kdnawacita<>'00' And aktif=1 Order By kode,kdoutput");
		elseif ($this->thang == '2021') 
			$qry  = $ref->query("Select kdgiat, concat(kdgiat,'.',kdoutput,'.',kdsoutput) kdsoutput, nmsoutput uraian, kdnawacita kode From t_soutput $whr And kdnawacita<>'00' Order By kode,kdoutput");
		else
			$qry  = $ref->query("Select kdgiat, concat(kdgiat,'.',kdoutput) kdoutput, nmoutput uraian, kdnawacita kode From t_output $whr And kdnawacita<>'00' Order By kode,kdoutput");

		$a = $qry->result_array();
		echo $this->fc->browse($a); exit;

		$str  = 'string ';
		foreach ($qry->result_array() as $row) {
			$kode = $row['kode']; $kdgiat = $row['kdgiat']; $kdprogram = $giat[$kdgiat]['kdprogram'];
			if (! array_key_exists($kode, $tagnawa)) $tagnawa[$kode] = array();
			if (! strpos($str, $kdprogram)) {
				$str .= $kdprogram . ' ';
				array_push($tagnawa[$kode], array('recid'=>'000', 'kode'=>'', 'uraian'=>$prog[$kdprogram]['kode'] .' '. $prog[$kdprogram]['uraian'], 'w2ui'=>$wi1));
			}
			if (! strpos($str, $kdgiat)) {
				$str .= $kdgiat . ' ';
				array_push($tagnawa[$kode], array('recid'=>'000', 'kode'=>'', 'uraian'=>$giat[$kdgiat]['kode'] .' '. $giat[$kdgiat]['uraian'], 'w2ui'=>$wi2));
			}
			array_push($tagnawa[$kode], array('recid'=>'000', 'kode'=>'', 'uraian'=>$row['kdoutput'] .' '. $row['uraian'], 'w2ui'=>$wi3));
		}
		echo $this->fc->browse($tagnawa); exit;

		if ($this->thang >= '2022') 
			$qry  = $ref->query("Select kdgiat, concat(kdgiat,'.',kdoutput,'.',kdsoutput) kdsoutput, nmsoutput uraian, kdjanpres kode From t_soutput $whr And kdjanpres<>'000' And aktif=1 Order By 2");
		elseif ($this->thang == '2021') 
			$qry  = $ref->query("Select kdgiat, concat(kdgiat,'.',kdoutput,'.',kdsoutput) kdsoutput, nmsoutput uraian, kdjanpres kode From t_soutput $whr And kdjanpres<>'000' Order By 2");
		else
			$qry  = $ref->query("Select kdgiat, concat(kdgiat,'.',kdoutput) kdoutput, nmoutput uraian, kdjanpres kode From t_output $whr And kdjanpres<>'000' Order By 2");

		$str  = 'string ';
		foreach ($qry->result_array() as $row) {
			$kode = $row['kode']; $kdgiat = $row['kdgiat']; $kdprogram = $giat[$kdgiat]['kdprogram'];
			if (! array_key_exists($kode, $tagjanj)) $tagjanj[$kode] = array();
			if (! strpos($str, $kdprogram)) {
				$str .= $kdprogram . ' ';
				array_push($tagjanj[$kode], array('recid'=>'000', 'kode'=>'', 'uraian'=>$prog[$kdprogram]['kode'] .' '. $prog[$kdprogram]['uraian'], 'w2ui'=>$wi1));
			}
			if (! strpos($str, $kdgiat)) {
				$str .= $kdgiat . ' ';
				array_push($tagjanj[$kode], array('recid'=>'000', 'kode'=>'', 'uraian'=>$giat[$kdgiat]['kode'] .' '. $giat[$kdgiat]['uraian'], 'w2ui'=>$wi2));
			}
			array_push($tagjanj[$kode], array('recid'=>'000', 'kode'=>'', 'uraian'=>$row['kdoutput'] .' '. $row['uraian'], 'w2ui'=>$wi3));
		}

		if ($this->thang >= '2022') 
			$qry  = $ref->query("Select kdgiat, concat(kdgiat,'.',kdoutput,'.',kdsoutput) kdsoutput, nmsoutput uraian, kdtema kode From t_soutput $whr And kdtema<>'000' And aktif=1 Order By 4,2");
		elseif ($this->thang == '2021') 
			$qry  = $ref->query("Select kdgiat, concat(kdgiat,'.',kdoutput,'.',kdsoutput) kdsoutput, nmsoutput uraian, kdtema kode From t_soutput $whr And kdtema<>'000' Order By 4,2");
		else
			$qry  = $ref->query("Select kdgiat, concat(kdgiat,'.',kdoutput) kdoutput, nmoutput uraian, kdtema kode From t_output $whr And kdtema<>'000' Order By 4,2");
		
		$str  = 'string ';
		foreach ($qry->result_array() as $row) {
			$kode = $row['kode']; $kdgiat = $row['kdgiat']; $kdprogram = $giat[$kdgiat]['kdprogram'];
			if (! array_key_exists($kode, $tagtema)) $tagtema[$kode] = array();
			if (! strpos($str, $kdprogram)) {
				$str .= $kdprogram . ' ';
				array_push($tagtema[$kode], array('recid'=>'000', 'kode'=>'', 'uraian'=>$prog[$kdprogram]['kode'] .' '. $prog[$kdprogram]['uraian'], 'w2ui'=>$wi1));
			}
			if (! strpos($str, $kdgiat)) {
				$str .= $kdgiat . ' ';
				array_push($tagtema[$kode], array('recid'=>'000', 'kode'=>'', 'uraian'=>$giat[$kdgiat]['kode'] .' '. $giat[$kdgiat]['uraian'], 'w2ui'=>$wi2));
			}
			array_push($tagtema[$kode], array('recid'=>'000', 'kode'=>'', 'uraian'=>$row['kdoutput'] .' '. $row['uraian'], 'w2ui'=>$wi3));
		}

		$tag['outp'] = $tagoutp;
		$tag['prio'] = $tagprio;
		$tag['nawa'] = $tagnawa;
		$tag['janj'] = $tagjanj;
		$tag['tema'] = $tagtema;
		return $tag;
	}

	function get_output( $kddept ) {
		$ref  = $this->load->database('ref'. $this->session->userdata('thang'), TRUE);
		$list = array(
			't_dept' 	=> array('id'=>'kddept', 'kd'=>'kddept', 'nm'=>'nmdept', 'tag'=>''),
			't_unit' 	=> array('id'=>'kddept,kdunit', 'kd'=>'kddept,kdunit', 'nm'=>'nmunit', 'tag'=>''),
			't_program' => array('id'=>'kddept,kdunit,kdprogram', 'kd'=>'kddept,kdunit,kdprogram', 'nm'=>'nmprogram', 'tag'=>''),
			't_giat' 	=> array('id'=>'kddept,kdunit,kdprogram,kdgiat', 'kd'=>'kdgiat', 'nm'=>'nmgiat', 'tag'=>''),
			't_output' 	=> array('id'=>'kdgiat,kdoutput', 'kd'=>'kdgiat,kdoutput', 'nm'=>'nmoutput',
								 'tag'=>'sat,kdsum,kdjanpres,kdnawacita,kdpn,kdpp,kdkp,kdproy,kdtema,'),
			't_soutput' => array('id'=>'kdgiat,kdoutput,kdsoutput', 'kd'=>'kdgiat,kdoutput,kdsoutput', 'nm'=>'nmsoutput', 'tag'=>''),
			't_kmpnen' 	=> array('id'=>'kdgiat,kdoutput,kdsoutput,kdkmpnen', 'kd'=>'kdkmpnen', 'nm'=>'nmkmpnen', 'tag'=>'')
		);

		if ($this->thang >= '2021') {
			$list['t_output']  = array('id'=>'kdgiat,kdoutput', 'kd'=>'kdgiat,kdoutput', 'nm'=>'nmoutput', 'tag'=>'sat,kdsum,kdjanpres,kdnawacita,');
			$list['t_soutput'] = array('id'=>'kdgiat,kdoutput,kdsoutput', 'kd'=>'kdgiat,kdoutput,kdsoutput', 'nm'=>'nmsoutput', 'tag'=>'sat,kdpn,kdpp,kdkp,kdproy,kdtema,kdpen,');
		}

		$table = array(); $arr = array();
		foreach ($list as $key=>$row) {
			if ($this->thang >= '2022' and strpos('table t_program t_giat', $key)) 
				$whr = "WHERE a.kddept='$kddept' AND a.aktif=1 AND a.". $this->session->userdata('whrdept');
			else
				$whr = "WHERE a.kddept='$kddept' AND a.". $this->session->userdata('whrdept');


			$id = 'a.'.str_replace(",", ",'.',a.", $row['id']); $kd = 'a.'.str_replace(",", ",'.',a.", $row['kd']); $nm = $row['nm']; $tag = $row['tag'];
			if (strpos('table t_dept t_unit t_program t_giat', $key)) {
				$qry = $ref->query("Select concat($id) id, concat($kd) kode, $nm uraian, 1 as pilih, '$key' as level From $key a $whr Order By 2");
			} else {
				$str = "b.kddept,'.',b.kdunit,'.',b.kdprogram,'.',";
				$whr = "Where b.kddept='$kddept'";
				if ($this->thang >= '2022') $whr .= " And a.aktif=1";
				
				$qry = $ref->query("Select concat($str$id) id, concat($kd) kode, $nm uraian, 1 as pilih, $tag '$key' as level From $key a Left Join t_giat b On a.kdgiat=b.kdgiat $whr Order By 2");
			}

			foreach ($qry->result_array() as $nil) $arr[ $nil['id'] ] = $nil;
		}

		$arr = $this->fc->array_index($arr, 'id');
		$tag = $this->tag_output( $kddept );
		$arr = $this->tag_indi_output($arr, $kddept);
		if ($this->thang >= '2021') $arr = $this->tag_indi_soutput($arr, $kddept);

		$arr = $this->style_output($arr, $tag, $kddept);
		$table['list'] = $arr;
		// echo $this->fc->browse($arr); exit;
		return $table;
	}

	function tag_output( $kddept ) {
		$ref = $this->load->database('ref'. $this->session->userdata('thang'), TRUE);
		$tag = array(); $whr = "Where a.kddept='$kddept'";

		// Ambil Data VISI, MISI dan RENSTRA
		$tag[$kddept] = array();
		// array_push($tag[$kddept], array('recid'=>'000', 'kode'=>'A', 'uraian'=>'<b><i> Visi </i></b>'));
		// $qry = $ref->query("Select kdvisi recid, '' kode, nmvisi uraian From t_visi a $whr");
		// foreach ($qry->result_array() as $row) { $row['uraian'] = $this->sp30. $row['uraian']. $this->sp99; array_push($tag[$kddept], $row); }

		// array_push($tag[$kddept], array('recid'=>'000', 'kode'=>'B', 'uraian'=>'<b><i> Misi </i></b>'));
		// $qry = $ref->query("Select kdmisi recid, '' kode, concat(kdmisi,'. ',nmmisi) uraian From t_misi a $whr");
		// foreach ($qry->result_array() as $row) { $row['uraian'] = $this->sp30. $row['uraian']. $this->sp99; array_push($tag[$kddept], $row); }

		array_push($tag[$kddept], array('recid'=>'000', 'kode'=>'A', 'uraian'=>'<b><i> Rencana Strategis </i></b>'));
		$qry = $ref->query("Select kdsasaran recid, '' kode, concat(kdsasaran,'. ',nmsasaran) uraian From t_sasaran a $whr");
		foreach ($qry->result_array() as $row) { $row['uraian'] = $this->sp30. $row['uraian']. $this->sp99; array_push($tag[$kddept], $row); }

		// Ambil Data MISI UNIT
		$qry = $ref->query("Select concat(kddept,'.',kdunit) id, kdmisiunit recid, '' kode, concat(kdmisiunit,'. ',nmmisiunit) uraian From t_misiunit a $whr");
		foreach ($qry->result_array() as $row) {
			if (!array_key_exists($row['id'], $tag)) {
				$tag[ $row['id'] ] = array();
				array_push($tag[ $row['id'] ], array('recid'=>'000', 'kode'=>'A', 'uraian'=>'<b><i> Misi Eselon I </i></b>'));
			}
			$row['uraian'] = $this->sp30. $row['uraian']. $this->sp99;
			array_push($tag[ $row['id'] ], $row);
		}

		// Ambil Data PROGRAM OUTCOME dan INDIKATOR
		$ind = array();
		$qry = $ref->query("Select concat(kddept,'.',kdunit,'.',kdprogram,kdprogout) id, kdindi recid, '' kode, concat('- ',nmindi) uraian From t_progoutin a $whr Order By 1,2");
		foreach ($qry->result_array() as $row) {
			if (!array_key_exists($row['id'], $ind)) {
				$ind[ $row['id'] ] = array();
				array_push($ind[ $row['id'] ], array('recid'=>'000', 'kode'=>'', 'uraian'=>"$this->sp60 <i> Indikator </i> $this->sp99"));
			}
			$row['uraian'] = $this->sp60. $row['uraian']. $this->sp99;
			array_push($ind[ $row['id'] ], $row);
		}

		$qry = $ref->query("Select concat(kddept,'.',kdunit,'.',kdprogram) id, kdprogout recid, '' kode, concat(kdprogout,'. ',nmprogout) uraian From t_progout a $whr Order By 1,2");
		foreach ($qry->result_array() as $row) {
			if (!array_key_exists($row['id'], $tag)) {
				$tag[ $row['id'] ] = array();
				array_push($tag[ $row['id'] ], array('recid'=>'000', 'kode'=>'A', 'uraian'=>'<b><i> Program Outcome </i></b>'));
			}
			$row['uraian'] = $this->sp30. $row['uraian']. $this->sp99;
			array_push($tag[ $row['id'] ], $row);
			if (array_key_exists($row['id'].$row['recid'], $ind)) foreach ($ind[ $row['id'].$row['recid'] ] as $nil) array_push($tag[ $row['id'] ], $nil);
		}

				//Start Ambil Data PROGRAM SASARAN dan INDIKATOR
		$ind = array();
		$pgs = array();
		// $pgs[$kddept] = array();
		$qry = $ref->query("Select concat(kddept,'.',kdunit,'.',kdprogram,kdprogsas) id, kdindi recid, '' kode, concat('- ',nmindi) uraian From t_progsasin a $whr Order By 1,2");
		foreach ($qry->result_array() as $row) {
			if (!array_key_exists($row['id'], $ind)) {
				$ind[ $row['id'] ] = array();
				array_push($ind[ $row['id'] ], array('recid'=>'000', 'kode'=>'', 'uraian'=>"$this->sp60 <i> Indikator </i> $this->sp99"));	
			} 
			$row['uraian'] = $this->sp60. $row['uraian']. $this->sp99; 
			array_push($ind[ $row['id'] ], $row);
		}

		$qry = $ref->query("Select concat(kddept,'.',kdunit,'.',kdprogram) id, kdprogsas recid, '' kode, concat(kdprogsas,'. ',nmprogsas) uraian From t_progsas a $whr Order By 1,2");
		foreach ($qry->result_array() as $row) {
			if (!array_key_exists($row['id'], $pgs)) {
				$pgs[ $row['id'] ] = array();
				array_push($pgs[$row['id']], array('recid'=>'000', 'kode'=>'B', 'uraian'=>'<b><i> Program Sasaran </i></b>')); 
				// $otp = false; 
			}
			$row['uraian'] = $this->sp30. $row['uraian']. $this->sp99; 
			array_push($pgs[ $row['id'] ], $row);
			if (array_key_exists($row['id'].$row['recid'], $ind)) foreach ($ind[ $row['id'].$row['recid'] ] as $nil) array_push($pgs[ $row['id'] ], $nil);
		}
		$tag = array_merge_recursive($tag,$pgs);

		// Ambil Data KEGIATAN SASARAN dan INDIKATOR
		$qry = $ref->query("Select concat(kddept,'.',kdunit,'.',kdprogram,'.',kdgiat) id, kdgiatsas recid, '' kode, concat(kdgiatsas,'. ',nmgiatsas) uraian From t_giatsas a $whr Order By 1,2");
		foreach ($qry->result_array() as $row) {
			if (!array_key_exists($row['id'], $tag)) {
				$tag[ $row['id'] ] = array();
				array_push($tag[ $row['id'] ], array('recid'=>'000', 'kode'=>'A', 'uraian'=>'<b><i> Kegiatan Sasaran </i></b>'));
			}
			$row['uraian'] = $this->sp30. $row['uraian']. $this->sp99;
			array_push($tag[ $row['id'] ], $row);
		}

		return $tag;
	}

	function tag_indi_output($tabel, $kddept) {
		$ref = $this->load->database('ref'. $this->session->userdata('thang'), TRUE);
		$qry = $ref->query("Select kdjanpres kd, nmjanpres nm From t_janpres"); 	$jan = $this->fc->ToArr($qry->result_array(), 'kd');
		$qry = $ref->query("Select kdnawacita kd, nmnawacita nm From t_nawacita");	$naw = $this->fc->ToArr($qry->result_array(), 'kd');

		if ($this->thang >= '2021') {
			$ind = array();
			$qry = $ref->query("Select concat(kdgiat,'.',kdoutput) id, kdindi, nmindi, satuan, target_0, target_1, target_2, target_3 From t_outputin Where kddept='$kddept' Order By 1,2");
			foreach ($qry->result_array() as $row) {
				if (!array_key_exists($row['id'], $ind)) $ind[ $row['id'] ] = array();
				array_push($ind[ $row['id'] ], $row);
			}

			foreach ($tabel as $key=>$row) {
				if ($row['level'] == 't_output') {
					$info = array();
					$info[0] = array('recid'=>0, 'name'=>'Kode', 'value'=>$row['kode']);
					$info[1] = array('recid'=>1, 'name'=>'Uraian', 'value'=>$row['uraian']);
					$info[2] = array('recid'=>2, 'name'=>'Satuan', 'value'=>$row['sat']);

					$nil = '('. $row['kdsum'] .') Dijumlahkan'; if ($row['kdsum'] == '0') $nil = '('. $row['kdsum'] .') Tidak Dijumlahkan';
					$info[3] = array('recid'=>3, 'name'=>'Kode Sum', 'value'=>$nil);
					$nil = ' -'; if (array_key_exists($row['kdjanpres'], $jan)) $nil = '('. $row['kdjanpres'] .') '. $jan[$row['kdjanpres']]['nm'];
					$info[4] = array('recid'=>4, 'name'=>'Janji Presiden', 'value'=>$nil);
					$nil = ' -'; if (array_key_exists($row['kdnawacita'], $naw)) $nil = '('. $row['kdnawacita'] .') '. $naw[$row['kdnawacita']]['nm'];
					$info[5] = array('recid'=>5, 'name'=>'Nawacita', 'value'=>$nil);
	
					$tabel[$key]['info'] = $info;
	
					$i = 6;
					if (array_key_exists($row['kode'], $ind)) {
						foreach ($ind[ $row['kode'] ] as $val) {
							$info[$i] = array('recid'=>$i, 'name'=>'Indikator', 'value'=>$val['kdindi'] .' - '. $val['nmindi']); $i++;
							$info[$i] = array('recid'=>$i, 'name'=>'Target #1', 'value'=>$val['target_0'] .' '. $val['satuan']); $i++;
							$info[$i] = array('recid'=>$i, 'name'=>'Target #2', 'value'=>$val['target_1'] .' '. $val['satuan']); $i++;
							$info[$i] = array('recid'=>$i, 'name'=>'Target #3', 'value'=>$val['target_2'] .' '. $val['satuan']); $i++;
							$info[$i] = array('recid'=>$i, 'name'=>'Target #4', 'value'=>$val['target_3'] .' '. $val['satuan']); $i++;
						}
					}

					$tabel[$key]['info'] = $info;
					$tabel[$key]['uraian'] .= " <span style='font-size: smaller' class='text-primary'>(". $row['sat'] .")</span>";

					if (strpos('-PQRSTUVWXY', substr($row['kode'],5,1)))
					$tabel[$key]['uraian'] .= " <span style=''><kbd>[PN]</kbd></span>";
				} else {
					$tabel[$key]['info'] = array();
				}
			}
		} else {
			$qry = $ref->query("Select kdjanpres kd, nmjanpres nm From t_janpres"); 	$jan = $this->fc->ToArr($qry->result_array(), 'kd');
			$qry = $ref->query("Select kdnawacita kd, nmnawacita nm From t_nawacita");	$naw = $this->fc->ToArr($qry->result_array(), 'kd');
			$qry = $ref->query("Select kdpn kd, nmpn nm From t_prinas"); 				$pn  = $this->fc->ToArr($qry->result_array(), 'kd');
			$qry = $ref->query("Select kdpp kd, nmpp nm From t_priprog"); 				$pp  = $this->fc->ToArr($qry->result_array(), 'kd');
			$qry = $ref->query("Select kdkp kd, nmkp nm From t_prigiat"); 				$kp  = $this->fc->ToArr($qry->result_array(), 'kd');
			$qry = $ref->query("Select kdproy kd, nmproy nm From t_priproy"); 			$pro = $this->fc->ToArr($qry->result_array(), 'kd');
			$qry = $ref->query("Select kdtema kd, nmtema nm From t_tema"); 				$tem = $this->fc->ToArr($qry->result_array(), 'kd');
			$qry = $ref->query("Select concat(kddept,kdunit,kdes2) kd, kdes2, nmes2 nm From t_es2 Where kddept='$kddept'"); $es2 = $this->fc->ToArr($qry->result_array(), 'kd');

			$ind = array();
			$qry = $ref->query("Select concat(kdgiat,'.',kdoutput) id, concat(kddept,kdunit,kdes2) kdes2, kdindi, nmindi, satuan, target_0, target_1, target_2, target_3
			 From t_outputin Where kddept='$kddept' Order By 1,3");
			foreach ($qry->result_array() as $row) {
				if (!array_key_exists($row['id'], $ind)) $ind[ $row['id'] ] = array();
				array_push($ind[ $row['id'] ], $row);
			}

			foreach ($tabel as $key=>$row) {
				if ($row['level'] == 't_output') {
					$info = array();
					$info[0] = array('recid'=>0, 'name'=>'Kode', 'value'=>$row['kode']);
					$info[1] = array('recid'=>1, 'name'=>'Uraian', 'value'=>$row['uraian']);
					$info[2] = array('recid'=>2, 'name'=>'Satuan', 'value'=>$row['sat']);

					$nil = '('. $row['kdsum'] .') Dijumlahkan'; if ($row['kdsum'] == '0') $nil = '('. $row['kdsum'] .') Tidak Dijumlahkan';
					$info[3] = array('recid'=>3, 'name'=>'Kode Sum', 'value'=>$nil);

					$nil = ' -'; if (array_key_exists($row['kdjanpres'], $jan)) $nil = '('. $row['kdjanpres'] .') '. $jan[$row['kdjanpres']]['nm'];
					$info[4] = array('recid'=>4, 'name'=>'Janji Presiden', 'value'=>$nil);
					$nil = ' -'; if (array_key_exists($row['kdnawacita'], $naw)) $nil = '('. $row['kdnawacita'] .') '. $naw[$row['kdnawacita']]['nm'];
					$info[5] = array('recid'=>5, 'name'=>'Nawacita', 'value'=>$nil);
					$nil = ' -'; if (array_key_exists($row['kdpn'], $pn)) $nil = '('. $row['kdpn'] .') '. $pn[$row['kdpn']]['nm'];
					$info[6] = array('recid'=>6, 'name'=>'Prioritas Nasional', 'value'=>$nil);
					$nil = ' -'; if (array_key_exists($row['kdpp'], $pp)) $nil = '('. $row['kdpp'] .') '. $pp[$row['kdpp']]['nm'];
					$info[7] = array('recid'=>7, 'name'=>'Prioritas Program', 'value'=>$nil);
					$nil = ' -'; if (array_key_exists($row['kdkp'], $kp)) $nil = '('. $row['kdkp'] .') '. $kp[$row['kdkp']]['nm'];
					$info[8] = array('recid'=>8, 'name'=>'Prioritas Kegiatan', 'value'=>$nil);
					$nil = ' -'; if (array_key_exists($row['kdproy'], $pro)) $nil = '('. $row['kdproy'] .') '. $pro[$row['kdproy']]['nm'];
					$info[9] = array('recid'=>9, 'name'=>'Prioritas Proyek', 'value'=>$nil);

					$nil = '('. $row['kdtema'] .') '; $arr = explode(',', $row['kdtema']);
					for ($i=0; $i < count($arr); $i++) {
						if (array_key_exists($arr[$i], $tem)) $nil .= $tem[$arr[$i]]['nm'] .' ';
					}
					$info[10] = array('recid'=>10, 'name'=>'Tema', 'value'=>$nil);

					$i = 11;
					if (array_key_exists($row['kode'], $ind)) {
						foreach ($ind[ $row['kode'] ] as $val) {
							$info[$i] = array('recid'=>$i, 'name'=>'Indikator', 'value'=>$val['kdindi'] .' - '. $val['nmindi']); $i++;
							$info[$i] = array('recid'=>$i, 'name'=>'Target #1', 'value'=>$val['target_0'] .' '. $val['satuan']); $i++;
							$info[$i] = array('recid'=>$i, 'name'=>'Target #2', 'value'=>$val['target_1'] .' '. $val['satuan']); $i++;
							$info[$i] = array('recid'=>$i, 'name'=>'Target #3', 'value'=>$val['target_2'] .' '. $val['satuan']); $i++;
							$info[$i] = array('recid'=>$i, 'name'=>'Target #4', 'value'=>$val['target_3'] .' '. $val['satuan']); $i++;
						}
						if (array_key_exists($val['kdes2'], $es2)) $info[$i] = array('recid'=>$i, 'name'=>'Penanggung Jawab', 'value'=>$es2[$val['kdes2']]['nm']);
					}

					$tabel[$key]['info'] = $info;
				} else {
					$tabel[$key]['info'] = array();
				}
			}
		}

		return $tabel;
	}

	function tag_indi_soutput($tabel, $kddept) {
		$ref = $this->load->database('ref'. $this->session->userdata('thang'), TRUE);

		$qry = $ref->query("Select kdpn kd, nmpn nm From t_prinas"); 				$pn  = $this->fc->ToArr($qry->result_array(), 'kd');
		$qry = $ref->query("Select kdpp kd, nmpp nm From t_priprog"); 				$pp  = $this->fc->ToArr($qry->result_array(), 'kd');
		$qry = $ref->query("Select kdkp kd, nmkp nm From t_prigiat"); 				$kp  = $this->fc->ToArr($qry->result_array(), 'kd');
		$qry = $ref->query("Select kdproy kd, nmproy nm From t_priproy"); 			$pro = $this->fc->ToArr($qry->result_array(), 'kd');
		$qry = $ref->query("Select kdtema kd, nmtema nm From t_tema"); 				$tem = $this->fc->ToArr($qry->result_array(), 'kd');
		$qry = $ref->query("Select kdpen kd, nmpen nm From t_pen_cluster");         $cpen= $this->fc->ToArr($qry->result_array(),'kd');
		$qry = $ref->query("Select concat(kddept,kdunit,kdes2) kd, kdes2, nmes2 nm From t_es2 Where kddept='$kddept'"); $es2 = $this->fc->ToArr($qry->result_array(), 'kd');

		foreach ($tabel as $key=>$row) {
			if ($row['level'] == 't_soutput') {
				$info = array();
				$info[0] = array('recid'=>0, 'name'=>'Kode', 'value'=>$row['kode']);
				$info[1] = array('recid'=>1, 'name'=>'Uraian', 'value'=>$row['uraian']);
				$info[2] = array('recid'=>2, 'name'=>'Satuan', 'value'=>$row['sat']);

				$nil = ' -'; if (array_key_exists($row['kdpn'], $pn)) $nil = '('. $row['kdpn'] .') '. $pn[$row['kdpn']]['nm'];
				$info[3] = array('recid'=>3, 'name'=>'Prioritas Nasional', 'value'=>$nil);
				$nil = ' -'; if (array_key_exists($row['kdpp'], $pp)) $nil = '('. $row['kdpp'] .') '. $pp[$row['kdpp']]['nm'];
				$info[4] = array('recid'=>4, 'name'=>'Prioritas Program', 'value'=>$nil);
				$nil = ' -'; if (array_key_exists($row['kdkp'], $kp)) $nil = '('. $row['kdkp'] .') '. $kp[$row['kdkp']]['nm'];
				$info[5] = array('recid'=>5, 'name'=>'Prioritas Kegiatan', 'value'=>$nil);
				$nil = ' -'; if (array_key_exists($row['kdproy'], $pro)) $nil = '('. $row['kdproy'] .') '. $pro[$row['kdproy']]['nm'];
				$info[6] = array('recid'=>6, 'name'=>'Prioritas Proyek', 'value'=>$nil);

				$nil = '('. $row['kdtema'] .') '; $arr = explode(',', $row['kdtema']);
				for ($i=0; $i < count($arr); $i++) {
					if (array_key_exists($arr[$i], $tem)) $nil .= $tem[$arr[$i]]['nm'] .' ';
				}
				$info[7] = array('recid'=>7, 'name'=>'Tema', 'value'=>$nil);
				$nil = '-';if(array_key_exists($row['kdpen'],$cpen)) $nil = '('. $row['kdpen'] .') '. $cpen[$row['kdpen']]['nm'];//blm jadi
				$info[8] = array('recid'=>8,'name'=>'Cluster PEN','value'=>$nil);

				$tabel[$key]['info'] = $info;
				$tabel[$key]['uraian'] .= " <span style='font-size: smaller' class='text-primary'>(". $row['sat'] .")</span>";
				
				if ((int)$row['kdpn'])
					$tabel[$key]['uraian'] .= " <span style='font-size: smaller' class='text-danger'>[PN-". $row['kdpn'] ."]</span>";
			}
		}

		return $tabel;
	}

	function tag_indi_output_old($tabel, $kddept) {
		$ref = $this->load->database('ref'. $this->session->userdata('thang'), TRUE);

		$qry = $ref->query("Select kdjanpres kd, nmjanpres nm From t_janpres"); 	$jan = $this->fc->ToArr($qry->result_array(), 'kd');
		$qry = $ref->query("Select kdnawacita kd, nmnawacita nm From t_nawacita");	$naw = $this->fc->ToArr($qry->result_array(), 'kd');
		$qry = $ref->query("Select kdpn kd, nmpn nm From t_prinas"); 				$pn  = $this->fc->ToArr($qry->result_array(), 'kd');
		$qry = $ref->query("Select kdpp kd, nmpp nm From t_priprog"); 				$pp  = $this->fc->ToArr($qry->result_array(), 'kd');
		$qry = $ref->query("Select kdkp kd, nmkp nm From t_prigiat"); 				$kp  = $this->fc->ToArr($qry->result_array(), 'kd');
		$qry = $ref->query("Select kdproy kd, nmproy nm From t_priproy"); 			$pro = $this->fc->ToArr($qry->result_array(), 'kd');
		$qry = $ref->query("Select kdtema kd, nmtema nm From t_tema"); 				$tem = $this->fc->ToArr($qry->result_array(), 'kd');
		$qry = $ref->query("Select concat(kddept,kdunit,kdes2) kd, kdes2, nmes2 nm From t_es2 Where kddept='$kddept'"); $es2 = $this->fc->ToArr($qry->result_array(), 'kd');

		$ind = array();
		$qry = $ref->query("Select concat(kdgiat,'.',kdoutput) id, concat(kddept,kdunit,kdes2) kdes2, kdindi, nmindi, satuan, target_0, target_1, target_2, target_3
		 From t_outputin Where kddept='$kddept' Order By 1,3");
		foreach ($qry->result_array() as $row) {
			if (!array_key_exists($row['id'], $ind)) $ind[ $row['id'] ] = array();
			array_push($ind[ $row['id'] ], $row);
		}

		foreach ($tabel as $key=>$row) {
			if ($row['level'] == 't_output') {
				$info = array();
				$info[0] = array('recid'=>0, 'name'=>'Kode', 'value'=>$row['kode']);
				$info[1] = array('recid'=>1, 'name'=>'Uraian', 'value'=>$row['uraian']);
				$info[2] = array('recid'=>2, 'name'=>'Satuan', 'value'=>$row['sat']);

				$nil = '('. $row['kdsum'] .') Dijumlahkan'; if ($row['kdsum'] == '0') $nil = '('. $row['kdsum'] .') Tidak Dijumlahkan';
				$info[3] = array('recid'=>3, 'name'=>'Kode Sum', 'value'=>$nil);

				$nil = ' -'; if (array_key_exists($row['kdjanpres'], $jan)) $nil = '('. $row['kdjanpres'] .') '. $jan[$row['kdjanpres']]['nm'];
				$info[4] = array('recid'=>4, 'name'=>'Janji Presiden', 'value'=>$nil);
				$nil = ' -'; if (array_key_exists($row['kdnawacita'], $naw)) $nil = '('. $row['kdnawacita'] .') '. $naw[$row['kdnawacita']]['nm'];
				$info[5] = array('recid'=>5, 'name'=>'Nawacita', 'value'=>$nil);
				$nil = ' -'; if (array_key_exists($row['kdpn'], $pn)) $nil = '('. $row['kdpn'] .') '. $pn[$row['kdpn']]['nm'];
				$info[6] = array('recid'=>6, 'name'=>'Prioritas Nasional', 'value'=>$nil);
				$nil = ' -'; if (array_key_exists($row['kdpp'], $pp)) $nil = '('. $row['kdpp'] .') '. $pp[$row['kdpp']]['nm'];
				$info[7] = array('recid'=>7, 'name'=>'Prioritas Program', 'value'=>$nil);
				$nil = ' -'; if (array_key_exists($row['kdkp'], $kp)) $nil = '('. $row['kdkp'] .') '. $kp[$row['kdkp']]['nm'];
				$info[8] = array('recid'=>8, 'name'=>'Prioritas Kegiatan', 'value'=>$nil);
				$nil = ' -'; if (array_key_exists($row['kdproy'], $pro)) $nil = '('. $row['kdproy'] .') '. $pro[$row['kdproy']]['nm'];
				$info[9] = array('recid'=>9, 'name'=>'Prioritas Proyek', 'value'=>$nil);

				$nil = '('. $row['kdtema'] .') '; $arr = explode(',', $row['kdtema']);
				for ($i=0; $i < count($arr); $i++) {
					if (array_key_exists($arr[$i], $tem)) $nil .= $tem[$arr[$i]]['nm'] .' ';
				}
				$info[10] = array('recid'=>10, 'name'=>'Tema', 'value'=>$nil);

				$i = 11;
				if (array_key_exists($row['kode'], $ind)) {
					foreach ($ind[ $row['kode'] ] as $val) {
						$info[$i] = array('recid'=>$i, 'name'=>'Indikator', 'value'=>$val['kdindi'] .' - '. $val['nmindi']); $i++;
						$info[$i] = array('recid'=>$i, 'name'=>'Target #1', 'value'=>$val['target_0'] .' '. $val['satuan']); $i++;
						$info[$i] = array('recid'=>$i, 'name'=>'Target #2', 'value'=>$val['target_1'] .' '. $val['satuan']); $i++;
						$info[$i] = array('recid'=>$i, 'name'=>'Target #3', 'value'=>$val['target_2'] .' '. $val['satuan']); $i++;
						$info[$i] = array('recid'=>$i, 'name'=>'Target #4', 'value'=>$val['target_3'] .' '. $val['satuan']); $i++;
					}
					if (array_key_exists($val['kdes2'], $es2)) $info[$i] = array('recid'=>$i, 'name'=>'Penanggung Jawab', 'value'=>$es2[$val['kdes2']]['nm']);
				}

				$tabel[$key]['info'] = $info;
			} else {
				$tabel[$key]['info'] = array();
			}
		}
		return $tabel;
	}

	function style_output($tabel, $tag, $kddept) {
		$hasil = array(); $no = 0;
		foreach ($tabel as $key=>$row) {
			$row['recid'] = $no; $no++; $info = array();

			if ($row['level'] == 't_dept') 	 $sty = 'color: green; font-weight: bold; font-size: 1.875em;';
			if ($row['level'] == 't_unit') 	 $sty = 'color: brown; font-weight: bold;';
			if ($row['level'] == 't_program')$sty = 'color: brown; font-weight: bold;';
			if ($row['level'] == 't_giat') {
				$sty = 'font-weight: bold;';
				$row['kode'] = $this->sp15. $row['kode'] .$this->sp99;
				$row['uraian'] = $this->sp15. $row['uraian'] .$this->sp99;
			}
			if ($row['level'] == 't_output') {
				$sty = 'font-weight: normal;';
				$row['kode'] = $this->sp15. $row['kode'] .$this->sp99;
				$row['uraian'] = $this->sp30. $row['uraian'] .$this->sp99;
			}
			if ($row['level'] == 't_soutput') {
				$sty = 'color: gray;';
				$row['kode'] = $this->sp15. $row['kode'] .$this->sp99;
				$row['uraian'] = $this->sp45. $row['uraian'] .$this->sp99;
			}
			if ($row['level'] == 't_kmpnen') {
				$sty = 'color: gray;';
				$row['kode'] = $this->sp30. $row['kode'] .$this->sp99;
				$row['uraian'] = "$this->sp60<i>". $row['uraian'] ."</i>$this->sp99";
			}

			if (array_key_exists($row['id'], $tag)) $chd = $tag[ $row['id'] ]; else $chd = array();
			$row['w2ui'] = array('children'=> $chd, 'style' => $sty);
			array_push($hasil, $row);
		}
		return $hasil;
	}

	function get_satker( $kddept ) {
		$ref  = $this->load->database('ref'. $this->session->userdata('thang'), TRUE);
		$tag  = "'' kddept, '' kdunit, '' kddekon, '' kdlokasi, '' kdkabkota, '' kdkppn, '' kdjnssat";
		$list = array(
			't_dept' 	=> array('id'=>'kddept', 'kd'=>'kddept', 'nm'=>'nmdept', 'tag'=>$tag),
			't_unit' 	=> array('id'=>'kddept,kdunit', 'kd'=>'kddept,kdunit', 'nm'=>'nmunit', 'tag'=>$tag),
			't_satker' 	=> array('id'=>'kddept,kdunit,kdsatker', 'kd'=>'kdsatker', 'nm'=>'nmsatker', 'tag'=>str_replace("''", "", $tag))
		);

		$table = array(); $arr = array(); $whr = "WHERE kddept='$kddept' AND ". $this->session->userdata('whrdept');
		foreach ($list as $key=>$row) {
			// $wh2 = ''; if ($key == 't_satker') $wh2 = "And kdupdate<>'x'";
			$wh2 = '';
			$id  = str_replace(",", ",'.',", $row['id']); $kd = str_replace(",", ",'.',", $row['kd']); $nm = $row['nm']; $tag = $row['tag'];
			$qry = $ref->query("Select concat($id) id, concat($kd) kode, $nm uraian, $tag, '$key' as level From $key $whr $wh2");
			foreach ($qry->result_array() as $nil) $arr[ $nil['id'] ] = $nil;
		}

		$arr = $this->fc->array_index($arr, 'id');
		$arr = $this->tag_satker($arr);
		$table['list'] = $this->style_satker($arr);
		return $table;
	}

	function tag_satker( $arr ) {
		$ref = $this->load->database('ref'. $this->session->userdata('thang'), TRUE);

		$qry = $ref->query("Select kddept kode, nmdept uraian From t_dept");
		$dep = $this->fc->ToArr($qry->result_array(), 'kode');
		$qry = $ref->query("Select concat(kddept,'.',kdunit) kode, nmunit uraian From t_unit");
		$uni = $this->fc->ToArr($qry->result_array(), 'kode');
		$qry = $ref->query("Select kdlokasi kode, nmlokasi uraian From t_lokasi");
		$lok = $this->fc->ToArr($qry->result_array(), 'kode');
		$qry = $ref->query("Select concat(kdlokasi,kdkabkota) kode, nmkabkota uraian From t_kabkota");
		$kab = $this->fc->ToArr($qry->result_array(), 'kode');
		$qry = $ref->query("Select kdkppn kode, nmkppn uraian From t_kppn");
		$kpn = $this->fc->ToArr($qry->result_array(), 'kode');
		$qry = $ref->query("Select kddekon kode, nmdekon uraian From t_dekon");
		$dek = $this->fc->ToArr($qry->result_array(), 'kode');
		$qry = $ref->query("Select kdjnssat kode, nmjnssat uraian From t_jnssat");
		$jns = $this->fc->ToArr($qry->result_array(), 'kode');

		foreach ($arr as $key=>$row) {
			if ($row['level'] == 't_satker') {
				$kddep = substr($row['id'],0,3); $kduni = substr($row['id'],4,2); $kddek = $row['kddekon'];
				$kdlok = $row['kdlokasi']; $kdkab = $row['kdkabkota']; $kdkpn = $row['kdkppn']; $kdjns = $row['kdjnssat'];

				if (array_key_exists($kddep, $dep)) 		$arr[$key]['kddept'] 	= "($kddep) <tab7>". $dep[$kddep]['uraian'];
				if (array_key_exists("$kddep.$kduni", $uni))$arr[$key]['kdunit']	= "($kduni) <tab7>". $uni["$kddep.$kduni"]['uraian'];
				if (array_key_exists($kddek, $dek)) 		$arr[$key]['kddekon'] 	= "($kddek) <tab7>". $dek[$kddek]['uraian'];
				if (array_key_exists($kdlok, $lok)) 		$arr[$key]['kdlokasi'] 	= "($kdlok) <tab7>". $lok[$kdlok]['uraian'];
				if (array_key_exists($kdlok.$kdkab, $kab))	$arr[$key]['kdkabkota'] = "($kdkab) <tab7>". $kab[$kdlok.$kdkab]['uraian'];
				if (array_key_exists($kdkpn, $kpn)) 		$arr[$key]['kdkppn'] 	= "($kdkpn) <tab7> KPPN ". $kpn[$kdkpn]['uraian'];
				if (array_key_exists($kdjns, $jns)) 		$arr[$key]['kdjnssat'] 	= "($kdjns) <tab7>". $jns[$kdjns]['uraian'];
			}
		}
		return $arr;

	}

	function style_satker( $tabel ) {
		$hasil = array();  $no=0;
		$sp15  = '<span style="padding-left: 15px">'; $sp30  = '<span style="padding-left: 30px">'; $sp99 = '</span>';
		foreach ($tabel as $key=>$row) {
			$row['recid'] = $no; $no++;

			if ($row['level'] == 't_dept') {
				$sty = 'color: green; font-weight: bold; font-size: 1.875em;';
			}
			if ($row['level'] == 't_unit') {
				$sty = 'color: brown; font-weight: bold;';
				$row['uraian'] = $sp15. $row['uraian'] .$sp99;
			}
			if ($row['level'] == 't_satker') {
				$sty = 'font-weight: normal';
				$tag = array();
				$tag[0] = array('recid'=>0, 'name'=>'Kode', 'value'=>$row['kode']);
				$tag[1] = array('recid'=>0, 'name'=>'Uraian', 'value'=>$row['uraian']);
				$tag[2] = array('recid'=>0, 'name'=>'K/L', 'value'=>$row['kddept']);
				$tag[3] = array('recid'=>0, 'name'=>'Unit', 'value'=>$row['kdunit']);
				$tag[4] = array('recid'=>0, 'name'=>'Dekon', 'value'=>$row['kddekon']);
				$tag[5] = array('recid'=>0, 'name'=>'Provinsi', 'value'=>$row['kdlokasi']);
				$tag[6] = array('recid'=>0, 'name'=>'Kab / Kota', 'value'=>$row['kdkabkota']);
				$tag[7] = array('recid'=>0, 'name'=>'KPPN', 'value'=>$row['kdkppn']);
				$tag[8] = array('recid'=>0, 'name'=>'Jenis Satker', 'value'=>$row['kdjnssat']);
				$row['tag'] = $tag;
				$row['kode'] = $sp15. $row['kode'] .$sp99;
				$row['uraian'] = $sp30. $row['uraian'] .$sp99;
			}

			$row['w2ui'] = array('style' => $sty);
			array_push($hasil, $row);
		}
		return $hasil;
	}

	function get_register() {
		$ref = $this->load->database('ref'. $this->session->userdata('thang'), TRUE);
		// $qry = $ref->query("Select *, 0 recid From t_register Order By tglnpln Desc");

		$cari = $this->session->userdata('cari');

		$ref->like('register',$cari);
		$ref->or_like('nonpln',$cari);
		$ref->or_like('kdvalas',$cari);
		$ref->or_like('nmdonor',$cari);
		$ref->order_by("tglnpln", "desc");

		$qry = $ref->get('t_register');
		// $hasil = $query->result_array();

		$arr = array(); $recid=1;
		foreach ($qry->result_array() as $row) {
			$row['recid']  = $recid; $recid++;
			$row['pagu']   = number_format($row['pagu'], 0, ',', '.');
			array_push($arr, $row);
		}
		return $arr;
	}

	function detil_ssb_satker($unit, $satk,$search){
		$qry  	 = $this->dbrvs->query("SELECT kdunit, kelssb FROM t_ssb_satker WHERE  kdsatker='$satk' ");
		$ssb  	 = $qry->row();
		$kdunit  = $ssb->kdunit;
		$where   = "AND kdunit=$kdunit";
		$inv     = explode(',',$ssb->kelssb);
		$ssb_z   = [];
		foreach($inv as $key => $row){
			if(substr($row,0,1)=='Z'){
				array_push($ssb_z,$row);
				unset($inv[$key]);
			}
		}
		// echo '<pre>';print_r($ssb_z);exit;
		$inv     = implode("','",$inv);
		$inv     = "('".$inv."')";
		$input   = $search;
		$temp_z  = [];
		$dat_z   = [];
		if($ssb_z){
			$ssb_z     = implode("','",$ssb_z);
			$ssb_z     = "('".$ssb_z."')";

			$all     = $this->dbrvs->query("SELECT *,concat(kelssb,'.',kdoutput,'.',kdsoutput,'.',kdkmpnen,'.',kdskmpnen,'.',kdakun) as kdkey,concat(kelssb,'.',kdoutput,'.',kdsoutput) as kode , concat(kelssb,'.',kdoutput) as kodekel, kelssb, '' valout FROM t_ssb WHERE  kelssb IN " . $ssb_z )->result_array();

			$utama   = $this->dbrvs->query("SELECT kelssb kdkey, 'Kelompok_SSB' type, kelssb kode,concat(kelssb,'.',kdgiat) idhide, 'color:green;padding-left : 0px;' styl, 'ssb' class FROM t_ssb WHERE  kelssb IN ".$ssb_z)->result_array();

			$giat   = $this->dbrvs->query("SELECT  concat(kelssb,'.',kdgiat) as kdkey, 'kdgiat' type, concat(kelssb,'.',kdgiat) kode,concat(kelssb,'.',kdgiat) idhide, 'font-size:14px;color:blue;padding-left : 15px!important; font-weight: bold' styl, 'output' class FROM t_ssb WHERE kelssb IN ".$ssb_z)->result_array();

			$output   = $this->dbrvs->query("SELECT  concat(kelssb,'.',kdgiat,'.',kdoutput) as kdkey, 'KRO' type, concat(kdgiat,'.',kdoutput) kode,concat(kelssb,'.',kdgiat) idhide, 'font-size:14px;color:blue;padding-left : 15px!important; font-weight: bold' styl, 'output' class FROM t_ssb WHERE kelssb IN ".$ssb_z)->result_array();

			$soutput   = $this->dbrvs->query("SELECT  concat(kelssb,'.',kdgiat,'.',kdoutput,'.',kdsoutput) as kdkey, 'RO' type, kdsoutput kode,concat(kelssb,'.',kdgiat) idhide, 'color:black;padding-left : 20px!important; font-weight: bold' styl, 'soutput' class FROM t_ssb WHERE kelssb IN ".$ssb_z)->result_array();

			$kmpnen   = $this->dbrvs->query("SELECT  concat(kelssb,'.',kdgiat,'.',kdoutput,'.',kdsoutput,'.',kdkmpnen) as kdkey, 'Kompnen' type, kdkmpnen kode,concat(kelssb,'.',kdgiat) idhide, 'padding-left : 25px!important' styl, 'komponen' class FROM t_ssb WHERE kelssb IN ".$ssb_z)->result_array();

			$skmpnen   = $this->dbrvs->query("SELECT  concat(kelssb,'.',kdgiat,'.',kdoutput,'.',kdsoutput,'.',kdkmpnen,'.',kdskmpnen) as kdkey,concat(kdgiat,'.',kdoutput) kode,concat(kelssb,'.',kdgiat) idhide, 'Subkomponen' type, kdskmpnen kode, 'color:grey;padding-left : 30px' styl, 'skmpnen' class FROM t_ssb WHERE kelssb IN ".$ssb_z)->result_array();

			$akun   = $this->dbrvs->query("SELECT kdakun, concat(kelssb,'.',kdgiat,'.',kdoutput,'.',kdsoutput,'.',kdkmpnen,'.',kdskmpnen,'.',kdakun) as kdkey,concat(kdgiat,'.',kdoutput) kode,concat(kelssb,'.',kdgiat) idhide, 'Akun' type, '' nmakun, kdakun kode, 'color:black; font-style: italic;padding-left : 35px; font-size:13px' styl, 'akun' class FROM t_ssb WHERE kelssb IN ".$ssb_z)->result_array();

			$qry    = $this->dbref->query("SELECT kdakun,nmakun FROM t_akun ")->result_array();
			$t_akun = $this->fc->ToArr($qry,'kdakun');
			$no = 0;

			foreach($akun as $key => $row){
				if(array_key_exists($row['kdakun'],$t_akun)){
					$akun[$key]['nmakun'] = $t_akun[ $row['kdakun'] ]['nmakun'];
				}
			}

			if(isset($input)){
				$temp_z  = [];
				$akun = array_filter($akun, function ($item) use ($input) {
					if (stripos($item['nmakun'], $input) !== false) {
						return true;
					}
					return false;
				});
				if($akun){
					foreach($akun as $row){
						array_push($temp_z,substr($row['kdkey'],0,3));
					}
					$temp_z = array_values(array_unique($temp_z));
					$temp_z = array_flip($temp_z);
				}
			}

		// echo '<pre>';print_r($akun);exit;


			$dat_z = $this->fc->array_index(array_merge($utama,$giat,$output,$soutput,$kmpnen,$skmpnen,$akun),'kdkey');
			$dat_z = $this->fc->ToArr($dat_z,'kdkey');
			// echo '<pre>';print_r($dat_z);exit;
		}

		$all     = $this->dbrvs->query("SELECT *,concat(kelssb,'.',kdoutput,'.',kdsoutput,'.',kdkmpnen,'.',kdskmpnen,'.',kdakun) as kdkey,concat(kelssb,'.',kdoutput,'.',kdsoutput) as kode , concat(kelssb,'.',kdoutput) as kodekel, kelssb, '' valout FROM t_ssb WHERE kdunit='$kdunit' AND  kelssb IN " . $inv )->result_array();

		$utama   = $this->dbrvs->query("SELECT kelssb kdkey, 'Kelompok_SSB' type, kelssb kode,concat(kelssb,'.',kdgiat) idhide, 'color:green;padding-left : 0px;' styl, 'ssb' class FROM t_ssb WHERE kdunit='$kdunit' AND kdunit='$kdunit' AND kelssb IN ".$inv)->result_array();


		$giat   = $this->dbrvs->query("SELECT  concat(kelssb,'.',kdgiat) as kdkey, 'kdgiat' type, concat(kelssb,'.',kdgiat) kode,concat(kelssb,'.',kdgiat) idhide, 'font-size:14px;color:blue;padding-left : 15px!important; font-weight: bold' styl, 'output' class FROM t_ssb WHERE kdunit='$kdunit' AND kelssb IN ".$inv)->result_array();

		$output   = $this->dbrvs->query("SELECT  concat(kelssb,'.',kdgiat,'.',kdoutput) as kdkey, 'KRO' type, concat(kdgiat,'.',kdoutput) kode,concat(kelssb,'.',kdgiat) idhide, 'font-size:14px;color:blue;padding-left : 15px!important; font-weight: bold' styl, 'output' class FROM t_ssb WHERE kdunit='$kdunit' AND kelssb IN ".$inv)->result_array();

		$soutput   = $this->dbrvs->query("SELECT  concat(kelssb,'.',kdgiat,'.',kdoutput,'.',kdsoutput) as kdkey, 'RO' type, kdsoutput kode,concat(kelssb,'.',kdgiat) idhide, 'color:black;padding-left : 20px!important; font-weight: bold' styl, 'soutput' class FROM t_ssb WHERE kdunit='$kdunit' AND kelssb IN ".$inv)->result_array();

		$kmpnen   = $this->dbrvs->query("SELECT  concat(kelssb,'.',kdgiat,'.',kdoutput,'.',kdsoutput,'.',kdkmpnen) as kdkey, 'Kompnen' type, kdkmpnen kode,concat(kelssb,'.',kdgiat) idhide, 'padding-left : 25px!important' styl, 'komponen' class FROM t_ssb WHERE kdunit='$kdunit' AND kelssb IN ".$inv)->result_array();

		$skmpnen   = $this->dbrvs->query("SELECT  concat(kelssb,'.',kdgiat,'.',kdoutput,'.',kdsoutput,'.',kdkmpnen,'.',kdskmpnen) as kdkey,concat(kdgiat,'.',kdoutput) kode,concat(kelssb,'.',kdgiat) idhide, 'Subkomponen' type, kdskmpnen kode, 'color:grey;padding-left : 30px' styl, 'skmpnen' class FROM t_ssb WHERE kdunit='$kdunit' AND kelssb IN ".$inv)->result_array();

		$akun   = $this->dbrvs->query("SELECT kdakun, concat(kelssb,'.',kdgiat,'.',kdoutput,'.',kdsoutput,'.',kdkmpnen,'.',kdskmpnen,'.',kdakun) as kdkey,concat(kdgiat,'.',kdoutput) kode,concat(kelssb,'.',kdgiat) idhide, 'Akun' type, '' nmakun, kdakun kode, 'color:black; font-style: italic;padding-left : 35px; font-size:13px' styl, 'akun' class FROM t_ssb WHERE kdunit='$kdunit' AND kelssb IN ".$inv)->result_array();
		$qry    = $this->dbref->query("SELECT kdakun,nmakun FROM t_akun ")->result_array();
		$t_akun = $this->fc->ToArr($qry,'kdakun');

		foreach($akun as $key => $row){
			if(array_key_exists($row['kdakun'],$t_akun)){
				$akun[$key]['nmakun'] = $t_akun[ $row['kdakun'] ]['nmakun'];
			}
		}

		if(isset($input)){
			$temp  = [];
			$akun = array_filter($akun, function ($item) use ($input) {
				if (stripos($item['nmakun'], $input) !== false) {
					return true;
				}
				return false;
			});
			if($akun){
				foreach($akun as $row){
					array_push($temp,substr($row['kdkey'],0,3));
				}
				$temp = array_values(array_unique($temp));
				$temp = array_flip($temp);
			}
		$temp_all = array_merge_recursive($temp_z,$temp);

		}

		$dat = $this->fc->array_index(array_merge($utama,$giat,$output,$soutput,$kmpnen,$skmpnen,$akun),'kdkey');
		$dat = $this->fc->ToArr($dat,'kdkey');



		$arr = array_merge_recursive($dat,$dat_z);
		// $dat = $this->fc->ToArr($arr,'kdkey');

		// echo $input;exit;
		// echo '<pre>';print_r($temp);exit;



		// if(isset($input)){
		// 	$temp  = [];
		// 	$akun = array_filter($akun, function ($item) use ($input) {
		// 		if (stripos($item['nmakun'], $input) !== false) {
		// 			return true;
		// 		}
		// 		return false;
		// 	});
		// 	if($akun){
		// 		foreach($akun as $row){
		// 			array_push($temp,substr($row['kdkey'],0,3));
		// 		}
		// 		$temp = array_values(array_unique($temp));
		// 		$temp = array_flip($temp);
		// 	}
		// }


		// $dat = $this->fc->array_index(array_merge($utama,$giat,$output,$soutput,$kmpnen,$skmpnen,$akun),'kdkey');
		// $dat = $this->fc->ToArr($dat,'kdkey');

		if(isset($input)){
			foreach($arr as $key => $row){
				if(!array_key_exists( substr($key,0,3), $temp_all ) ){
					unset($arr[$key]);
				}
			}
		}
		// $dat = $this->fc->array_index($dat,'kdkey');
		// echo '<pre>'; print_r($dat);exit;
		
		return $arr;
	}
	
	function get_depuni_satker() {
		$ref        = $this->load->database('ref'. $this->session->userdata('thang'), TRUE);
		$grp 	    = $this->session->userdata('idusergroup');

		$qry        = $ref->query("SELECT concat(kddept,'.',kdunit,'.',kdsatker) id, concat(kddept,'.',kdunit,'.',kdsatker) kode, kdsatker, nmsatker uraian, 1 as pilih FROM t_satker WHERE kddept='015' ");
		$data_satker= $this->fc->ToArr($qry->result_array(),'kode');

		if (isset($_GET['kddepuni'])) $kdunit = substr($_GET['kddepuni'],4,2); else $kdunit = '01';
		if (isset($_GET['sat'])) 	  $kdsatker = substr($_GET['sat'],7,6); else $kdsatker = '117109';

		if($grp =='usersatker'){
			$kdunit   = $this->session->userdata('kdunit');
			$qry      = $ref->query("SELECT concat(kddept,'.',kdunit) id, concat(kddept,'.',kdunit) kode, nmunit uraian, 1 as pilih FROM t_unit WHERE kddept='015' AND kdunit='$kdunit' ");
			$unit     = $qry->result_array();

			$kdsatker = $this->session->userdata('kdsatker');
			// echo "SELECT concat(kddept,'.',kdunit,'.',kdsatker) id, concat(kddept,'.',kdunit,'.',kdsatker) kode, '' uraian, 1 as pilih FROM t_ssb_satker WHERE kddept='015' and kdsatker='$kdsatker'";exit;
			$qry      = $this->dbrvs->query("SELECT concat(kddept,'.',kdunit,'.',kdsatker) id, concat(kddept,'.',kdunit,'.',kdsatker) kode, '' uraian, 1 as pilih FROM t_ssb_satker WHERE kddept='015' and kdsatker='$kdsatker' ");
			$satker   = $qry->result_array();
			foreach($satker as $key => $row){
				if(array_key_exists($row['kode'], $data_satker )){
					$satker[$key]['uraian'] = $data_satker[$row['kode']]['uraian'];
				}
			}

		}else {
			$qry      = $ref->query("SELECT concat(kddept,'.',kdunit) id, concat(kddept,'.',kdunit) kode, nmunit uraian, 1 as pilih FROM t_unit WHERE kddept='015'  ");
			$unit     = $qry->result_array();

			$qry      = $this->dbrvs->query("SELECT concat(kddept,'.',kdunit,'.',kdsatker) id, concat(kddept,'.',kdunit,'.',kdsatker) kode, kdsatker, '' uraian, 1 as pilih FROM t_ssb_satker WHERE kddept='015' and kdunit='$kdunit' order by id ");
			$satker   = $qry->result_array();
			foreach($satker as $key => $row){
				if(array_key_exists($row['kode'], $data_satker )){
					$satker[$key]['uraian'] = $data_satker[$row['kode']]['uraian'];
				}
			}
			if($kdunit !== null && $kdsatker == null) $kdsatker = $satker[0]['kdsatker'];


		}

		$table['unit']   = $unit;
		$table['satker'] = $satker;

		$table['pilih']  = "015.$kdunit.$kdsatker";

		// echo '<pre>'; print_r($table);exit;

		return $table;
	}

}
