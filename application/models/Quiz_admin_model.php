<?php
class Quiz_admin_model extends CI_Model {

  function get_data() {
    $whr = "";  $cari = $this->session->userdata('cari');
    if ($cari) {
      $whr = "Where " ;
      $arr = explode(' ', $cari);
      for ($i=0; $i<count($arr); $i++) {
        $whr .= "soal like '%". $arr[0] ."%'";
        if ($i<count($arr)-1) $whr .= " and ";
      }
      $whr = "Where soal like '%". $cari ."%' Or pil_a like '%". $cari ."%' Or pil_b like '%". $cari ."%' Or pil_c like '%". $cari ."%' Or pil_d like '%". $cari ."%'" ;
    }

    $query = $this->db->query("Select * From quiz $whr Order By kdso, idquiz");
    return $query->result_array();
  }

  function get_so() {
    $query = $this->db->query("Select Left(kdso,2) kdso, nmso1 From t_so Where Right(kdso,4)='0000' Order By 1");
    return $this->fc->ToArr($query->result_array(), 'kdso');
  }

  function write_data() {
    $isi    = $_POST['isian'];
    $aksi   = $_POST['aksi'];
    $idquiz = $isi[0];
    $soal   = $isi[1];
    $pil_a  = $isi[2];
    $pil_b  = $isi[3];
    $pil_c  = $isi[4];
    $pil_d  = $isi[5];
    $jawaban= $isi[6];
    $kdso   = $isi[7];
    $tampil = $isi[8];

    switch( $aksi ) {
    case "Rekam":
      $query = $this->db->query("Insert Into quiz (soal, pil_a, pil_b, pil_c, pil_d, jawaban, kdso, tampil) Values (\"$soal\", \"$pil_a\", \"$pil_b\", \"$pil_c\", \"$pil_d\", '$jawaban', '$kdso', '$tampil')");
      break;

    case "Ubah":
      $query = $this->db->query("Update quiz Set soal=\"$soal\", pil_a=\"$pil_a\", pil_b=\"$pil_b\", pil_c=\"$pil_c\", pil_d=\"$pil_d\", jawaban='$jawaban', kdso='$kdso', tampil='$tampil' Where idquiz=$idquiz");
      break;

    case "Hapus":
      $query = $this->db->query("Delete From quiz Where idquiz=$idquiz");
      break;

    default:
      break;
    }
  }

  public function get_skor() {
    $query = $this->db->query("Select idkuis, a.iduser, nmuser, nip, nmso, nilai, waktu, tglupdate, aktivitas From quiz_submit a Left Join t_user b On a.iduser=b.iduser Left Join t_so c On b.kdso=c.kdso Where idkuis='1' Order By nilai Desc, waktu");
    $hasil = $query->result_array();

    $data['table'] = $this->fc->ToArr($hasil, 'iduser');
    return $data;
  }

  public function save_skor(  ) {
    $aksi  = $_POST['aksi'];
    $isian = $_POST['isian'];

    // Konversi ARRAY ISIAN ke Variabel
    $iduser   = $isian[0];  
    $nmuser   = $isian[1];  
    $fullname   = $isian[2];  
    $nip    = $isian[4];  
    $pangkat  = $isian[5];
    $golongan = $isian[6];
    $kdso   = $isian[8];
    $email    = $isian[9];
    $nohp   = $isian[10];
    $idusergroup= $isian[12];
    $jabatan  = $isian[17];
    $kdeselon = $isian[18];

    if ($aksi=='Rekam') {
      $this->db->query( "Insert Into t_user (iduser, nmuser, nip, pangkat, golongan, kdso, email, nohp, idusergroup, jabatan, kdeselon) Values ('$iduser', '$nmuser', '$nip', '$pangkat', '$golongan', '$kdso', '$email', '$nohp', '$idusergroup', '$jabatan', '$kdeselon')" );
    }
    if ($aksi=='Ubah') {
      $this->db->query( "Update t_user Set nmuser='$nmuser', fullname='$fullname', nip='$nip', pangkat='$pangkat', golongan='$golongan', kdso='$kdso', email='$email', nohp='$nohp', idusergroup='$idusergroup', jabatan='$jabatan', kdeselon='$kdeselon' Where iduser='$iduser'" );
    }
    if ($aksi=='Hapus') {
      $this->db->query( "Delete From t_user where iduser='$iduser'" );
    }
    return implode('#', $isian);
  }

}
