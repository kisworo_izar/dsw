<?php
class Revisi_Kanwil_model extends CI_Model {

	private $dbrevisi;

	public function __construct()
		{
		parent::__construct();

		        //$this->dbrevisi = $this->load->database('dbsatu', TRUE);
				$this->dbrevisi = $this->load->database('revisi'. $this->session->userdata('thang'), TRUE);
				$this->dbref 	= $this->load->database('ref'. $this->session->userdata('thang'), TRUE);

		}
	

	public function get_data( $depuni=null, $start, $end, $tahun, $cari ) {
		
		if (! (strrpos($this->session->userdata('idusergroup'), '001') or strrpos($this->session->userdata('idusergroup'), '002') or strrpos($this->session->userdata('idusergroup'), '503')) ) {
			$lokasi = $this->session->userdata('kdlokasi'); 
			$query = $this->dbrevisi->query("SELECT rev_id FROM revisi_satker WHERE kdlokasi = '$lokasi' "); 
			$hasil = $query->result_array();
			if ($hasil != null) { 
				$whr 	= "and rev_id ". $this->fc->InVar($hasil, 'rev_id'); 
				$range 	= " AND pus_tgl >= '$start 00:00:00' AND pus_tgl <= '$end 23:59.59' ";
			} else { $whr = "and rev_id in ('')" ; }
		} else { 
			$whr = ""; 
			$range = " AND pus_tgl >= '$start 00:00:00' AND pus_tgl <= '$end 23:59.59' ";
		}

		if ($cari != '') {
			$whr.= " AND (rev_id LIKE '%$cari%' OR kl_satker LIKE '%$cari%' ) ";
			$range = "";
		}

		//$whr='';
		$query = $this->dbrevisi->query("Select rev_tahun, rev_id, rev_tahap, rev_status, kl_dept, '' nmdept, concat(kl_dept,'.',kl_unit) kl_unit, '' nmunit, kl_satker, kl_surat_no, kl_surat_tgl, kl_surat_hal,
			pus_status, pus_tgl, kl_pjb_jab, kl_pjb_nama, kl_pjb_nip,kl_telp, t2_catatan, t7_catatan,
			t2_status, if(t2_status='1',t2_proses_tgl,t2_selesai_tgl) t2_tgl,
			t6_status, if(t6_status='1',t6_proses_tgl,t6_selesai_tgl) t6_tgl,
			t7_status, if(t7_status='1',t7_proses_tgl,t7_selesai_tgl) t7_tgl, appl, bypass_ctt, bypass_file
			From revisi where rev_level ='3' $whr  $range Order By rev_tahun Desc, pus_tgl Desc");
		$hasil = $this->fc->ToArr( $query->result_array(), 'rev_id');
		//echo '<pre>';print_r($hasil);exit;
		if ($hasil) {
			$dbref = $this->load->database('ref'. $this->session->userdata('thang'), TRUE);
			$query = $dbref->query("Select kdsatker kode, nmsatker uraian, '1' level From t_satker Where kdsatker ". $this->fc->InVar($hasil, 'kl_satker') );
			$satker  = $this->fc->ToArr( $query->result_array(), 'kode');

			// $query = $dbref->query("Select concat(kddept,'.',kdunit) kode, nmunit uraian, '2' level From t_unit Where concat(kddept,'.',kdunit) ". $this->fc->InVar($hasil, 'kl_unit') );
			// $unit  = $this->fc->ToArr( $query->result_array(), 'kode');


			foreach ($hasil as $key=>$value) {
				if (array_key_exists($value['kl_satker'], $satker)) $hasil[$key]['nmsatker'] = $satker[ $value['kl_satker'] ]['uraian'];
				// if (array_key_exists($value['kl_unit'], $unit)) $hasil[$key]['nmunit'] = $unit[ $value['kl_unit'] ]['uraian'];
			}

			$data['depuni'] = $this->fc->array_index( array_merge_recursive($satker), 'kode');
			$data['kode']   = $depuni;
		} else {
			$data['depuni'] = array();
			$data['kode']   = array();
		}

		$data['revisi'] = $hasil;
		$data['start'] 	= $this->fc->idtgl( $start );
		$data['end']   	= $this->fc->idtgl( $end );
		return $data;
	}

	public function get_form2( $rev_id ) {
		$query = $this->dbrevisi->query("Select * From revisi Where rev_id= '$rev_id' ");
		$hasil = $query->row_array();
		return $hasil;
	}

	function adk_per_tiket($rev_id) {
		$ref  = $this->load->database('ref', TRUE);
		$kode = substr($rev_id,5,6);

		if (strpos($kode, '.')) $qry = $ref->query("SELECT nmunit uraian FROM t_unit WHERE concat(kddept,'.',kdunit)='$kode' LIMIT 1");
		else $qry = $ref->query("SELECT nmsatker uraian FROM t_satker WHERE kdsatker='$kode' LIMIT 1");
		$tbl = $qry->row_array();


		$whr = "rev_id='$rev_id'";
		$qry = $this->dbrevisi->query("SELECT kdsatker kode, '' uraian, revisike, indikasi FROM revisi_satker WHERE $whr ORDER BY 1");
		$dat = $this->fc->ToArr($qry->result_array(), 'kode');

		$qry = $ref->query("SELECT kdsatker kode, nmsatker uraian FROM t_satker WHERE kdsatker ". $this->fc->InVar($dat, 'kode'));
		foreach ($qry->result_array() as $row) $dat[ $row['kode'] ]['uraian'] = $row['uraian'];

		foreach ($dat as $key=>$row) {
			$ke = sprintf('%02d', ((int)$row['revisike'])-1);
			$dat[$key]['link_matrik'] = site_url("Revisi_Kanwil/matrik_satker/". $row['kode'] .'/'. $rev_id .'/'.$ke);
				
			$dat[$key]['link_pok'] 	= site_url("Revisi_Kanwil/matrik_satker=". $row['kode'] ."&revid=$rev_id");
			$dat[$key]['link_ssb'] 	= site_url("Revisi_Kanwil/matrik_satker=". $row['kode'] ."&revid=$rev_id");
		}

		$data['head']  = ($tbl) ? ":: $rev_id - ".$tbl['uraian'] : ":: $rev_id";
		$data['tabel'] = $dat;
		// echo '<pre>'; print_r($data['tabel']); exit;
		return $data;
	}

	public function save_form2_proses( ) {
		$rev_id			= $_POST['rev_id']; 
		$this->fc->logRevisi($rev_id, "save_form2_proses - $rev_id");
		
		$ip	   			= $this->fc->get_client_ip(); 
		$t2_catatan		= str_replace(array("'", "\"", "&quot;"), "", htmlspecialchars($_POST['t2_catatan'] ) );
		$totim			= $_POST['radio-group'];
		
		if ($totim=='1') { $t2 = '2'; $tahap='6'; $rev_status='9'; $next = ', t6_status=9'; } else {$t2='0'; $tahap='2'; $rev_status='0'; $next = ''; } 
				
		$this->dbrevisi->query( "Update revisi Set t2_catatan=\"$t2_catatan\", t2_selesai_tgl=current_timestamp(),t2_selesai_ip='$ip',t2_status='$t2',
							rev_tahap='$tahap', rev_status='$rev_status' $next	Where rev_id='$rev_id' " );
		//if ($totim=='1') { $this->email_proses_revisi( $rev_id );  }
		//if ($totim=='2') { $this->email_pengembalian_revisi( $rev_id );  } 
		
		return;
	}

	public function email_pengembalian_revisi( $rev_id ) {
		//query_sender_email awal
	
		$query = $this->dbrevisi->query("Select *, kl_email email  From revisi Where rev_id = '$rev_id'");
	 	$hasilemail = $query->result_array();
	 	//print_r($hasilemail); exit;

		 $email = array(); $attach = array();
		 foreach ($hasilemail as $row) if ( $row['email'] ) $email[ $row['email'] ] = $row['kl_dept'];
		//print_r($email); exit;

		//ambil file lampiran
		$query = $this->dbrevisi->query("Select * From revisi Where rev_id='$rev_id'");
		$hasil = $query->row_array();
		$file  = array();

		//query ambil data email
		//$query = $this->dbrevisi->query("Select *, '' nmdept, '' nmunit, nmso, DATE(pus_tgl) From revisi a Left Join t_so b On a.rev_kdso=b.kdso Where rev_id='$rev_id'");
		$query = $this->dbrevisi->query("Select * From revisi Where rev_id='$rev_id'");
		$hasil = $query->row_array(); 

		$dbref = $this->load->database('ref'. $this->session->userdata('thang'), TRUE);
		$query = $dbref->query("Select nmdept From t_dept Where kddept='". $hasil['kl_dept'] ."'");
		$dept  = $query->row_array();

		$query = $dbref->query("Select nmunit From t_unit Where kddept='". $hasil['kl_dept'] ."' And kdunit='". $hasil['kl_unit'] ."'");
		$unit  = $query->row_array();

		$hasil['nmdept'] = $dept['nmdept'];
		$hasil['nmunit'] = $unit['nmunit'];
		//ending ambil data

		$query = $this->db->query("Select * From revisi_redaksi Where tahap = 19 ");
		$hasilbody = $query->row_array();
		$subject = $hasilbody['email_subject'];
		$body    = $hasilbody['email_narasi'];
		$body    = str_replace('[kl_dept]', ucwords(strtolower($hasil['nmdept'])), $body);
		$body    = str_replace('[kl_unit]', ucwords(strtolower($hasil['nmunit'])), $body);
		$body    = str_replace('[kl_pjb_jab]', $hasil['kl_pjb_jab'], $body);
		$body    = str_replace('[kl_surat_tgl]', $this->fc->idtgl($hasil['kl_surat_tgl'],'tglfull'),$body);
		$body    = str_replace('[kl_surat_no]', $hasil['kl_surat_no'], $body);
		$body    = str_replace('[pus_tgl]', $this->fc->idtgl($hasil['pus_tgl'],'tglfull'), $body);
		$body    = str_replace('[rev_id]', $hasil['rev_id'], $body);
		$body    = str_replace('[t2_catatan]', $hasil['t2_catatan'], $body);
		//$body    = str_replace('http://www.rkakldipaonline.kemenkeu.go.id','http://rkakldipa.anggaran.depkeu.go.id/index.php/tracking/'.$hasil['qrcode'], $body);
		
				$attach = array();
				
				$this->fc->send_mail( 'satudja', $email, $subject, $body, $attach ); 
		
	 	return $attach;
	}

	public function email_proses_revisi( $rev_id ) {
		//query_sender_email awal
	
		$query = $this->dbrevisi->query("Select *, kl_email email  From revisi Where rev_id = '$rev_id'");
	 	$hasilemail = $query->result_array();
	 	//print_r($hasilemail); exit;

		 $email = array(); $attach = array();
		 foreach ($hasilemail as $row) if ( $row['email'] ) $email[ $row['email'] ] = $row['kl_dept'];
		//print_r($email); exit;

		//ambil file lampiran
		$query = $this->dbrevisi->query("Select * From revisi Where rev_id='$rev_id'");
		$hasil = $query->row_array();
		$file  = array();

		//query ambil data email
		//$query = $this->dbrevisi->query("Select *, '' nmdept, '' nmunit, nmso, DATE(pus_tgl) From revisi a Left Join t_so b On a.rev_kdso=b.kdso Where rev_id='$rev_id'");
		$query = $this->dbrevisi->query("Select * From revisi Where rev_id='$rev_id'");
		$hasil = $query->row_array(); 

		$dbref = $this->load->database('ref'. $this->session->userdata('thang'), TRUE);
		$query = $dbref->query("Select nmdept From t_dept Where kddept='". $hasil['kl_dept'] ."'");
		$dept  = $query->row_array();

		$query = $dbref->query("Select nmunit From t_unit Where kddept='". $hasil['kl_dept'] ."' And kdunit='". $hasil['kl_unit'] ."'");
		$unit  = $query->row_array();
		$query = $dbref->query("Select nmsatker From t_satker Where kdsatker='". $hasil['kl_satker']."'");
		$unit  = $query->row_array();


		$hasil['nmdept'] = $dept['nmdept'];
		$hasil['nmunit'] = $unit['nmunit'];
		//ending ambil data

		$query = $this->db->query("Select * From revisi_redaksi Where tahap = 12 ");
		$hasilbody = $query->row_array();
		$subject = $hasilbody['email_subject'];
		$body    = $hasilbody['email_narasi'];
		$body    = str_replace('[kl_dept]', ucwords(strtolower($hasil['nmdept'])), $body);
		$body    = str_replace('[kl_unit]', ucwords(strtolower($hasil['nmunit'])), $body);
		$body    = str_replace('[kl_pjb_jab]', $hasil['kl_pjb_jab'], $body);
		$body    = str_replace('[kl_surat_tgl]', $this->fc->idtgl($hasil['kl_surat_tgl'],'tglfull'),$body);
		$body    = str_replace('[kl_surat_no]', $hasil['kl_surat_no'], $body);
		$body    = str_replace('[pus_tgl]', $this->fc->idtgl($hasil['pus_tgl'],'tglfull'), $body);
		$body    = str_replace('[rev_id]', $hasil['rev_id'], $body);
		$body    = str_replace('[t2_catatan]', $hasil['t2_catatan'], $body);
		//$body    = str_replace('http://www.rkakldipaonline.kemenkeu.go.id','http://rkakldipa.anggaran.depkeu.go.id/index.php/tracking/'.$hasil['qrcode'], $body);
		
				$attach = array();
				
				$this->fc->send_mail( 'satudja', $email, $subject, $body, $attach ); 
		
	 	return $attach;
	}


	public function save_form6_proses( ) {
		$rev_id			= $_POST['rev_id'];
		$this->fc->logRevisi($rev_id, "save_form6_proses - $rev_id");
		 
		$ip	   			= $this->fc->get_client_ip();
		$t6_catatan		= str_replace(array("'", "\"", "&quot;"), "", htmlspecialchars($_POST['t6_catatan'] ) );
		$totim			= $_POST['radio-group'];


		if ($totim=='1') {
		$this->dbrevisi->query( "Update revisi Set t6_catatan=\"$t6_catatan\", t6_selesai_tgl=current_timestamp(),t6_selesai_ip='$ip',t6_status=2,
						rev_tahap='7', rev_status='9', t7_status='9' Where rev_id='$rev_id'" );
		
		//$this->email_lengkap( $rev_id );

		}

		if ($totim=='2') {
		$this->dbrevisi->query( "Update revisi Set t6_catatan=\"$t6_catatan\", t6_selesai_tgl=current_timestamp(),t6_selesai_ip='$ip',t6_status=0,
						 rev_tahap='7', rev_status=9, t7_status='9' Where rev_id='$rev_id'" );
		
		//$this->email_perbaikan_form6( $rev_id );
		}


		return;
	}

	public function save_form7_proses( ) {
		$rev_id 		= $_POST['rev_id'];
		$this->fc->logRevisi($rev_id, "save_form7_proses - $rev_id");

		$ip	   			= $this->fc->get_client_ip();
		$t7_sp_no		= $_POST['t7_sp_no']; 
		$t7_sp_tgl		= $_POST['t7_sp_tgl']; 
		$t7_sp_file_u	= $_POST['t7_sp_file']; 
		$rev_ke			= $_POST['rev_ke']; 
		$t7_catatan		= str_replace(array("'", "\"", "&quot;"), "", htmlspecialchars($_POST['t7_catatan'] ) );
		$totim			= $_POST['radio-group'];

		//echo $totim; exit;
		
		if ($totim=='1') { $t7 = '2'; $rev_status='2';  } else {$t7='0'; $rev_ke=''; $rev_status='0';}
		if ($totim=='1') {$this->email_pengesahan_revisi( $rev_id );}
		if ($totim=='2') {$this->email_penolakan_revisi( $rev_id );}  

		
		$this->dbrevisi->query( "Update revisi set t7_status ='$t7', rev_tahap = 7, t7_selesai_tgl=current_timestamp(), t7_selesai_ip='$ip', 
							t7_sp_tgl = '$t7_sp_tgl', t7_sp_no = '$t7_sp_no',t7_sp_file = '$t7_sp_file_u', t7_catatan = \"$t7_catatan\", rev_ke = '$rev_ke', rev_status='$rev_status'  
							where rev_id='$rev_id' " );
	
		return;
	}

	public function email_pengesahan_revisi( $rev_id ) {
		//query_sender_email awal
		$query = $this->dbrevisi->query("Select *, kl_email email  From revisi Where rev_id = '$rev_id'");
	 	$hasilemail = $query->result_array();

		$email = array(); $attach = array();
		foreach ($hasilemail as $row) if ( $row['email'] ) $email[ $row['email'] ] = $row['kl_dept'];

		//ambil file lampiran
		$query = $this->dbrevisi->query("Select * From revisi Where rev_id='$rev_id'");
		$hasil = $query->row_array();
		$file  = $hasil['t7_sp_file'];

		//query ambil data email
		//$query=$this->dbrevisi->query("Select *, '' nmdept, '' nmunit, nmso, DATE(pus_tgl) From revisi a Left Join t_so b On a.rev_kdso=b.kdso Where rev_id='$rev_id'");
		$query = $this->dbrevisi->query("Select * From revisi Where rev_id='$rev_id'");
		$hasil = $query->row_array();
		$hasil = $query->row_array();

		$dbref = $this->load->database('ref'. $this->session->userdata('thang'), TRUE);
		$query = $dbref->query("Select nmdept From t_dept Where kddept='". $hasil['kl_dept'] ."'");
		$dept  = $query->row_array();

		$query = $dbref->query("Select nmunit From t_unit Where kddept='". $hasil['kl_dept'] ."' And kdunit='". $hasil['kl_unit'] ."'");
		$unit  = $query->row_array();

		$hasil['nmdept'] = $dept['nmdept'];
		$hasil['nmunit'] = $unit['nmunit'];
		//ending ambil data

		$query = $this->db->query("Select * From revisi_redaksi Where tahap = 71 ");
		$hasilbody = $query->row_array();
		$subject = $hasilbody['email_subject'];
		$body    = $hasilbody['email_narasi'];
		$body    = str_replace('[kl_dept]', ucwords(strtolower($hasil['nmdept'])), $body);
		$body    = str_replace('[kl_unit]', ucwords(strtolower($hasil['nmunit'])), $body);
		$body    = str_replace('[kl_pjb_jab]', $hasil['kl_pjb_jab'], $body);
		$body    = str_replace('[kl_surat_tgl]', $this->fc->idtgl($hasil['kl_surat_tgl'],'tglfull'),$body);
		$body    = str_replace('[kl_surat_no]', $hasil['kl_surat_no'], $body);
		$body    = str_replace('[pus_tgl]', $this->fc->idtgl($hasil['pus_tgl'],'tglfull'), $body);
		$body    = str_replace('[rev_id]', $hasil['rev_id'], $body);
		$body    = str_replace('[t7_sp_no]', $hasil['t7_sp_no'], $body);
		$body    = str_replace('[t7_sp_tgl]', $this->fc->idtgl($hasil['t7_sp_tgl'],'tglfull'), $body);
		//$body    = str_replace('http://www.rkakldipaonline.kemenkeu.go.id','http://rkakldipa.anggaran.depkeu.go.id/index.php/tracking/'.$hasil['qrcode'], $body);

		$attach  = array( $file => "http://10.242.142.52/files/revisi_SatuDJA/$rev_id/" .'/'. $file );
		if ($file!='') { 
			$this->fc->send_mail( 'satudja', $email, $subject, $body, $attach); 
		} else {
				$attach = array();
				$this->fc->send_mail( 'satudja', $email, $subject, $body, $attach ); 
		}
	 	return $attach;
	}

	public function email_penolakan_revisi( $rev_id ) {
		//query_sender_email awal
		$query = $this->dbrevisi->query("Select *, kl_email email  From revisi Where rev_id = '$rev_id'");
	 	$hasilemail = $query->result_array();

		$email = array(); $attach = array();
		foreach ($hasilemail as $row) if ( $row['email'] ) $email[ $row['email'] ] = $row['kl_dept'];

		//ambil file lampiran
		$query = $this->dbrevisi->query("Select * From revisi Where rev_id='$rev_id'");
		$hasil = $query->row_array();
		$file  = $hasil['t7_sp_file'];

		//query ambil data email
		//$query=$this->dbrevisi->query("Select *, '' nmdept, '' nmunit, nmso, DATE(pus_tgl) From revisi a Left Join t_so b On a.rev_kdso=b.kdso Where rev_id='$rev_id'");
		$query = $this->dbrevisi->query("Select * From revisi Where rev_id='$rev_id'");
		$hasil = $query->row_array();

		$dbref = $this->load->database('ref'. $this->session->userdata('thang'), TRUE);
		$query = $dbref->query("Select nmdept From t_dept Where kddept='". $hasil['kl_dept'] ."'");
		$dept  = $query->row_array();

		$query = $dbref->query("Select nmunit From t_unit Where kddept='". $hasil['kl_dept'] ."' And kdunit='". $hasil['kl_unit'] ."'");
		$unit  = $query->row_array();

		$hasil['nmdept'] = $dept['nmdept'];
		$hasil['nmunit'] = $unit['nmunit'];
		//ending ambil data

		$query = $this->db->query("Select * From revisi_redaksi Where tahap = 79 ");
		$hasilbody = $query->row_array();
		$subject = $hasilbody['email_subject'];
		$body    = $hasilbody['email_narasi'];
		$body    = str_replace('[kl_dept]', ucwords(strtolower($hasil['nmdept'])), $body);
		$body    = str_replace('[kl_unit]', ucwords(strtolower($hasil['nmunit'])), $body);
		$body    = str_replace('[kl_pjb_jab]', $hasil['kl_pjb_jab'], $body);
		$body    = str_replace('[kl_surat_tgl]', $this->fc->idtgl($hasil['kl_surat_tgl'],'tglfull'),$body);
		$body    = str_replace('[kl_surat_no]', $hasil['kl_surat_no'], $body);
		$body    = str_replace('[pus_tgl]', $this->fc->idtgl($hasil['pus_tgl'],'tglfull'), $body);
		$body    = str_replace('[rev_id]', $hasil['rev_id'], $body);
		$body    = str_replace('[t7_sp_no]', $hasil['t7_sp_no'], $body);
		$body    = str_replace('[t7_sp_tgl]', $hasil['t7_sp_tgl'], $body);
		//$body    = str_replace('http://www.rkakldipaonline.kemenkeu.go.id','http://rkakldipa.anggaran.depkeu.go.id/index.php/tracking/revisi/'.$hasil['qrcode'], $body);


		// $subject = "Revisi - Undangan Penelaahan #$rev_id";
		// $body	 = "Telampir Undangan Penelaahan Terkait Revisi No. $rev_id";
		$attach  = array( $file => "http://10.242.142.52/files/revisi_SatuDJA/$rev_id/" .'/'. $file );
		if ($file!='') { 
			$this->fc->send_mail( 'satudja', $email, $subject, $body, $attach); 
		} else {
				$attach = array();
				$this->fc->send_mail( 'satudja', $email, $subject, $body, $attach ); 
		}
	 	return $attach;
	}

	public function email_lengkap($rev_id){
		//query_sender_email awal
		$query = $this->db->query("Select *, kl_email email  From revisi Where rev_id = '$rev_id'");
	 	$hasilemail = $query->result_array();
		$email = array(); $attach = array();
		foreach ($hasilemail as $row) if ( $row['email'] ) $email[ $row['email'] ] = $row['kl_dept'];

		//query ambil data email
		$query=$this->db->query("Select *, '' nmdept, '' nmunit, nmso, DATE(pus_tgl) From revisi a Left Join t_so b On a.rev_kdso=b.kdso Where rev_id='$rev_id'");
		$hasil = $query->row_array();

		$dbref = $this->load->database('ref'. $this->session->userdata('thang'), TRUE);
		$query = $dbref->query("Select nmdept From t_dept Where kddept='". $hasil['kl_dept'] ."'");
		$dept  = $query->row_array();

		$query = $dbref->query("Select nmunit From t_unit Where kddept='". $hasil['kl_dept'] ."' And kdunit='". $hasil['kl_unit'] ."'");
		$unit  = $query->row_array();

		$hasil['nmdept'] = $dept['nmdept'];
		$hasil['nmunit'] = $unit['nmunit'];
		//ending ambil data
		$query = $this->db->query("Select * From revisi_redaksi Where tahap = 50 ");
		$hasilbody = $query->row_array();
		$subject = $hasilbody['email_subject'];
		$body    = $hasilbody['email_narasi'];
		$body    = str_replace('[kl_dept]', ucwords(strtolower($hasil['nmdept'])), $body);
		$body    = str_replace('[kl_unit]', ucwords(strtolower($hasil['nmunit'])), $body);
		$body    = str_replace('[kl_surat_tgl]', $this->fc->idtgl($hasil['kl_surat_tgl'],'tglfull'),$body);
		$body    = str_replace('[kl_surat_no]', $hasil['kl_surat_no'], $body);
		$body    = str_replace('[kl_pjb_jab]', $hasil['kl_pjb_jab'], $body);
		$body    = str_replace('[pus_tgl]', $this->fc->idtgl($hasil['pus_tgl'],'tglfull'), $body);
		$body    = str_replace('[rev_id]', $hasil['rev_id'], $body);
		$body    = str_replace('http://www.rkakldipaonline.kemenkeu.go.id','http://rkakldipa.anggaran.depkeu.go.id/index.php/tracking/revisi/'.$hasil['qrcode'], $body);
		$body    = str_replace('[t5_hasil_tgl]', $this->fc->idtgl($hasil['t5_hasil_tgl'],'tglfull'), $body);
		$this->fc->send_mail( 'revisi', $email, $subject, $body);
	 	return $attach ;
	}

	public function email_perbaikan_form6(  $rev_id ){
		//query_sender_email awal
		$query = $this->db->query("Select *, kl_email email  From revisi Where rev_id = '$rev_id'");
	 	$hasilemail = $query->result_array();

		$email = array();
		foreach ($hasilemail as $row) if ( $row['email'] ) $email[ $row['email'] ] = $row['kl_dept'];

		//ambil file lampiran
		$query = $this->db->query("Select * From revisi Where rev_id='$rev_id'");
		$undfile = $query->row_array();
		$file  = $undfile['t6_surat_file'];

		//query ambil data email
		$query=$this->db->query("Select *, '' nmdept, '' nmunit, nmso, DATE(pus_tgl) From revisi a Left Join t_so b On a.rev_kdso=b.kdso Where rev_id='$rev_id'");
		$hasil = $query->row_array();

		$dbref = $this->load->database('ref'. $this->session->userdata('thang'), TRUE);
		$query = $dbref->query("Select nmdept From t_dept Where kddept='". $hasil['kl_dept'] ."'");
		$dept  = $query->row_array();

		$query = $dbref->query("Select nmunit From t_unit Where kddept='". $hasil['kl_dept'] ."' And kdunit='". $hasil['kl_unit'] ."'");
		$unit  = $query->row_array();

		$hasil['nmdept'] = $dept['nmdept'];
		$hasil['nmunit'] = $unit['nmunit'];
		//ending ambil data
		$query = $this->db->query("Select * From revisi_redaksi Where tahap = 40 ");
		$hasilbody = $query->row_array();
		$subject = $hasilbody['email_subject'];
		$body    = $hasilbody['email_narasi'];
		$body    = str_replace('[kl_dept]', ucwords(strtolower($hasil['nmdept'])), $body);
		$body    = str_replace('[kl_unit]', ucwords(strtolower($hasil['nmunit'])), $body);
		$body    = str_replace('[kl_pjb_jab]', $hasil['kl_pjb_jab'], $body);
		$body    = str_replace('[kl_surat_tgl]', $this->fc->idtgl($hasil['kl_surat_tgl'],'tglfull'),$body);
		$body    = str_replace('[kl_surat_no]', $hasil['kl_surat_no'], $body);
		$body    = str_replace('[pus_tgl]', $this->fc->idtgl($hasil['pus_tgl'],'tglfull'), $body);
		$body    = str_replace('[rev_id]', $hasil['rev_id'], $body);
		$body    = str_replace('[t4_ba_no]', $hasil['t4_ba_no'], $body);
		$body    = str_replace('[t4_ba_tgl]', $hasil['t4_ba_tgl'], $body);
		$body    = str_replace('http://www.rkakldipaonline.kemenkeu.go.id','http://rkakldipa.anggaran.depkeu.go.id/index.php/tracking/revisi/'.$hasil['qrcode'], $body);


		// $subject = "Revisi - Undangan Penelaahan #$rev_id";
		// $body	 = "Telampir Undangan Penelaahan Terkait Revisi No. $rev_id";
		$attach  = array( $file => "http://10.100.159.45/files/revisi/$rev_id/" .'/'. $file );
		$this->fc->send_mail( 'revisi', $email, $subject, $body, $attach);
	 	return $attach;
	}

	public function email_revisi_form6($rev_id){
		//query_sender_email awal
		$query = $this->db->query("Select *, kl_email email  From revisi Where rev_id = '$rev_id'");
	 	$hasilemail = $query->result_array();

		$email = array();
		foreach ($hasilemail as $row) if ( $row['email'] ) $email[ $row['email'] ] = $row['kl_dept'];

		$query = $this->db->query("Select * From revisi Where rev_id='$rev_id'");
		$hasil = $query->row_array();
		$file  = $hasil['t6_surat_file'];

		//query ambil data email
		$query=$this->db->query("Select *, '' nmdept, '' nmunit, nmso, DATE(pus_tgl) From revisi a Left Join t_so b On a.rev_kdso=b.kdso Where rev_id='$rev_id'");
		$hasil = $query->row_array();

		$dbref = $this->load->database('ref'. $this->session->userdata('thang'), TRUE);
		$query = $dbref->query("Select nmdept From t_dept Where kddept='". $hasil['kl_dept'] ."'");
		$dept  = $query->row_array();

		$query = $dbref->query("Select nmunit From t_unit Where kddept='". $hasil['kl_dept'] ."' And kdunit='". $hasil['kl_unit'] ."'");
		$unit  = $query->row_array();

		$hasil['nmdept'] = $dept['nmdept'];
		$hasil['nmunit'] = $unit['nmunit'];
		//ending ambil data

		$query = $this->db->query("Select * From revisi_redaksi Where tahap = 40 ");
		$hasilbody = $query->row_array();
		$subject = $hasilbody['email_subject'];
		$body    = $hasilbody['email_narasi'];
		$body    = str_replace('[kl_dept]', ucwords(strtolower($hasil['nmdept'])), $body);
		$body    = str_replace('[kl_unit]', ucwords(strtolower($hasil['nmunit'])), $body);
		$body    = str_replace('[kl_pjb_jab]', $hasil['kl_pjb_jab'], $body);
		$body    = str_replace('[kl_surat_tgl]', $this->fc->idtgl($hasil['kl_surat_tgl'],'tglfull'),$body);
		$body    = str_replace('[kl_surat_no]', $hasil['kl_surat_no'], $body);
		$body    = str_replace('[pus_tgl]', $this->fc->idtgl($hasil['pus_tgl'],'tglfull'), $body);
		$body    = str_replace('[rev_id]', $hasil['rev_id'], $body);
		$body    = str_replace('[t5_hasil_no]', $hasil['t6_surat_no'], $body);
		$body    = str_replace('[t6_catatan]', $hasil['t6_catatan'], $body);
		$body    = str_replace('[t5_hasil_tgl]', $this->fc->idtgl($hasil['t6_surat_tgl'],'tglfull'), $body);
		$body    = str_replace('http://www.rkakldipaonline.kemenkeu.go.id','http://rkakldipa.anggaran.depkeu.go.id/index.php/tracking/revisi/'.$hasil['qrcode'], $body);

		$attach  = array( $file => "http://10.242.142.52/files/revisi/$rev_id/" .'/'. $file );
		if ($file!='') { 
			$this->fc->send_mail( 'revisi', $email, $subject, $body, $attach); 
		} else {
				$this->fc->send_mail( 'revisi', $email, $subject, $body ); 
		}
	 	return $attach;
	}

	function matrik_semula_menjadi($kdsatker, $id1, $db1, $id2, $db2) {
		$matrx = array();
		if ($db2 == 'v_ds_revisi') $id = 'rev_id'; else $id = 'revisike';

		$this->dbrevisi->where('kdsatker', $kdsatker)->where($id, $id2);
		$matrx = $this->matrik_leveling($matrx, $this->dbrevisi->get($db2)->result_array());

		$this->dbrevisi->where('kdsatker', $kdsatker)->where('revisike', $id1);
		$matrx = $this->matrik_leveling($matrx, $this->dbrevisi->get($db1)->result_array());

		foreach ($matrx as $key=>$row) {
			$matrx[$key]['vol3']  = $row['vol2']  - $row['vol1'];
			$matrx[$key]['jml3']  = $row['jml2']  - $row['jml1'];
			$matrx[$key]['blok3'] = $row['blok2'] - $row['blok1'];
			if ($row['level'] == 'output') {
				if ($matrx[$key]['vol3'] < 0 And ! strpos($matrx[$key]['indikasi'], 'Vol-')) $matrx[$key]['indikasi'] .= 'Vol- ';
				if ($matrx[$key]['vol3'] > 0 And ! strpos($matrx[$key]['indikasi'], 'Vol+')) $matrx[$key]['indikasi'] .= 'Vol+ ';
			}
			if ($row['level'] == 'jenbel') {
				if ($matrx[$key]['jml3'] < 0 And ! strpos($matrx[$key]['indikasi'], $row['kode'].'-')) $matrx[$key]['indikasi'] .= $row['kode'].'- ';
				if ($matrx[$key]['jml3'] > 0 And ! strpos($matrx[$key]['indikasi'], $row['kode'].'+')) $matrx[$key]['indikasi'] .= $row['kode'].'+ ';
			}
			if ($row['level'] == 'akun') {
				if ($matrx[$key]['jml3'] == 0 And $matrx[$key]['blok3'] == 0) unset($matrx[$key]);
				else { $matrx[ substr($key,0,06) ]['sel'] = '1'; $matrx[ substr($key,0,16) ]['sel'] = '1'; $matrx[ substr($key,0,21) ]['sel'] = '1';
					   $matrx[ substr($key,0,25) ]['sel'] = '1'; $matrx[ substr($key,0,29) ]['sel'] = '1'; $matrx[ substr($key,0,33) ]['sel'] = '1';
					   $matrx[ substr($key,0,38) ]['sel'] = '1'; $matrx[$key]['sel'] = '1'; }
			}
		}
		foreach ($matrx as $key=>$row) {
			if ($row['level'] != 'satker' AND $row['level'] != 'digist')
			if ($matrx[$key]['sel'] <> '1') unset($matrx[$key]);
		}

		if (! $matrx) return array();
		$matrx = $this->matrik_ds_uraian($matrx);

		// Update KODE DIGITAL STAMP Data Semula
		$sele1 = "thang,kdjendok,kdsatker,kddept,kdunit,kdprogram,kdgiat,kdoutput,kdlokasi,kdkabkota,kddekon";
		$sele2 = "kdkppn,kdbeban,kdjnsban,kdctarik,register,kdakun,jumlah,paguphln,pagurmp,pagurkp,blokirphln,blokirrmp,blokirrkp,rphblokir";
		$qry_o = $this->dbrevisi->query("SELECT $sele1, Round(sum(volkeg),0) vol FROM $db1 WHERE kdsatker='$kdsatker' AND revisike='$id1' GROUP BY $sele1");
		$qry_i = $this->dbrevisi->query("SELECT $sele1, $sele2 FROM $db1 WHERE kdsatker='$kdsatker' AND revisike='$id1'");
		$ds1   = $this->ds->hashing($qry_o->result_array(), $qry_i->result_array());

		$qry_o = $this->dbrevisi->query("SELECT $sele1, Round(sum(volkeg),0) vol FROM $db2 WHERE kdsatker='$kdsatker' AND $id='$id2'  GROUP BY $sele1");
		$qry_i = $this->dbrevisi->query("SELECT $sele1, $sele2 FROM $db2 WHERE kdsatker='$kdsatker' AND $id='$id2'");
		$ds2   = $this->ds->hashing($qry_o->result_array(), $qry_i->result_array());

		if ($matrx[1]['uraian'] == 'Kode Digital Stamp') { $matrx[1]['jml1'] = $ds1; $matrx[1]['jml2'] = $ds2; }
		return $matrx;
	}

	function matrik_leveling($matrx, $arr) {
		$detil = $matrx;
		$std = array('recid'=>'', 'kode'=>'', 'uraian'=>'', 'vol1'=>0, 'jml1'=>0, 'blok1'=>0, 'vol2'=>0, 'jml2'=>0, 'blok2'=>0, 'vol3'=>0, 'jml3'=>0, 'blok3'=>0, 'beban'=>'', 'sat'=>'', 'sel'=>'', 'level'=>'', 'indikasi'=>'');

		if (count($detil) == 0) { $v = 'vol2'; $j = 'jml2'; $b = 'blok2'; $p = 'pagu2'; $d = 'ds2'; $s = 'sat';}
		else { $v = 'vol1'; $j = 'jml1'; $b = 'blok1'; $p = 'pagu1'; $d = 'ds1'; $s = 'sat';}


		foreach ($arr as $row) {
			$thang=$row['thang']; $kdjendok=$row['kdjendok']; $kdsatker=$row['kdsatker']; $kddept=$row['kddept']; $kdunit=$row['kdunit']; $kdprogram=$row['kdprogram']; $kdgiat=$row['kdgiat']; $kdoutput=$row['kdoutput']; $kdsoutput=$row['kdsoutput']; $kdkmpnen=$row['kdkmpnen']; $kdlokasi=$row['kdlokasi']; $kdkabkota=$row['kdkabkota']; $kddekon=$row['kddekon']; $kdkppn=$row['kdkppn']; $kdbeban=$row['kdbeban']; $kdjnsban=$row['kdjnsban']; $kdctarik=$row['kdctarik']; $register=$row['register']; $kdakun=$row['kdakun']; $kdjenbel=substr($row['kdakun'],0,2);

			$jml = $row['jumlah']; $blok = $row['blokirphln']+$row['blokirrmp']+$row['blokirrkp']+$row['rphblokir'];
			$arr=$std; $arr[$j] = $jml; $arr[$b] = $blok;
			$rkp['recid'] = $kdsatker; $rkp['kode'] = $kdsatker; $rkp[$p] = $jml;

			$dsout = array('thang'=>$thang, 'kdjendok'=>$kdjendok, 'kdsatker'=>$kdsatker, 'kddept'=>$kddept, 'kdunit'=>$kdunit, 'kdprogram'=>$kdprogram, 'kdgiat'=>$kdgiat, 'kdoutput'=>$kdoutput, 'kdlokasi'=>$kdlokasi, 'kdkabkota'=>$kdkabkota, 'kddekon'=>$kddekon, 'vol'=>$row['volkeg']);

			// Level 1 - SATKER
			$key = $kdsatker; $arr['recid'] = $key; $arr['kode'] = $kdsatker; $arr['level'] = 'satker';
			if (! array_key_exists($key, $detil)) { $arr['recid'] = "$kdsatker.000.01"; $detil[$key] = $arr; }
			else { $detil[$key][$j] += $jml; $detil[$key][$b] += $blok; }

			// Level 1b - DIGIST
			if (! array_key_exists("$kdsatker.0", $detil)) {
				$arr['level'] = 'digist'; $arr['recid'] = "$kdsatker.000.02"; $arr['kode'] = ''; $arr['uraian'] = 'Kode Digital Stamp';
				$detil["$kdsatker.0"] = $arr; $detil["$kdsatker.0"][$j] = 0; $arr['uraian'] = '';
			}

			// Level 2 - PROGRAM
			$key .= ".$kddept.$kdunit.$kdprogram"; $arr['recid'] = $key; $arr['kode'] = "$kddept.$kdunit.$kdprogram"; $arr['level'] = 'program';
			if (! array_key_exists($key, $detil)) $detil[$key] = $arr;
			else { $detil[$key][$j] += $jml; $detil[$key][$b] += $blok; }

			// Level 3 - KEGIATAN
			$key .= ".$kdgiat"; $arr['recid'] = $key; $arr['kode'] = $kdgiat; $arr['level'] = 'kegiatan';
			if (! array_key_exists($key, $detil)) $detil[$key] = $arr;
			else { $detil[$key][$j] += $jml; $detil[$key][$b] += $blok; }

			// Level 4 - OUTPUT
			$key .= ".$kdoutput"; $arr['recid'] = $key; $arr['kode'] = "$kdgiat.$kdoutput"; $arr['level'] = 'output';
			if (! array_key_exists($key, $detil)) { $detil[$key] = $arr; $detil[$key][$v] = $row['volkeg']; }
			else { $detil[$key][$j] += $jml; $detil[$key][$b] += $blok; $detil[$key][$v] += $row['volkeg']; }

			// Level 5 - SUB OUTPUT
			$key .= ".$kdsoutput"; $arr['recid'] = $key; $arr['kode'] = "$kdgiat.$kdoutput.$kdsoutput"; $arr['level'] = 'soutput';
			if (! array_key_exists($key, $detil)) { $detil[$key] = $arr; $detil[$key][$v] = $row['volkeg']; }
			else { $detil[$key][$j] += $jml; $detil[$key][$b] += $blok; $detil[$key][$v] += $row['volkeg']; }

			// Level 6 - KOMPONEN
			$key .= ".$kdkmpnen"; $arr['recid'] = $key; $arr['kode'] = $kdkmpnen; $arr['level'] = 'komponen';
			$vkmp = explode(',00', $row['volkmp']);
			if (! array_key_exists($key, $detil)) { $detil[$key] = $arr; $detil[$key][$v]  = (int)$vkmp[0]; }
			else { $detil[$key][$j] += $jml; $detil[$key][$b] += $blok;  $detil[$key][$v] += (int)$vkmp[0]; }
			if ((int)$vkmp[0] > 0) $detil[$key][$s] = $vkmp[1];

			// Level 8 - JENIS BELANJA
			$key .= ".$kdbeban.$kdjenbel"; $arr['recid'] = $key; $arr['kode'] = "$kdbeban.$kdjenbel"; $arr['level'] = 'jenbel';
			if (! array_key_exists($key, $detil)) { $detil[$key] = $arr; $detil[$key]['beban'] = $kdbeban; }
			else { $detil[$key][$j] += $jml; $detil[$key][$b] += $blok; }

			// Level 7 - AKUN
			$key .= ".$kdakun"; $arr['recid'] = $key; $arr['kode'] = $kdakun; $arr['level'] = 'akun';
			if (! array_key_exists($key, $detil)) $detil[$key] = $arr;
			else { $detil[$key][$j] += $jml; $detil[$key][$b] += $blok; }

		}
		return $detil;
	}

	function matrik_ds_uraian($matrx) {
		// Ambil URAIAN dari TABEL REFERENSI
		$ref = $this->load->database('ref', TRUE);
		$inStk = "kdsatker in ("; $inPrg = "concat(kddept,'.',kdunit,'.',kdprogram) in ("; $inKeg = "kdgiat in ("; $inOut = "concat(kdgiat,'.',kdoutput) in ("; $inSOu = "concat(kdgiat,'.',kdoutput,'.',kdsoutput) in ("; $inKmp = "concat(kdgiat,'.',kdoutput,'.',kdsoutput,'.',kdkmpnen) in ("; $inBel = "kdgbkpk in ("; $inAkn = "kdakun in (";
		foreach ($matrx as $row) {
			if ($row['level'] == 'satker'   And ! strpos($inStk, $row['kode']))  $inStk .= "'". $row['kode'] ."',";
			if ($row['level'] == 'program'  And ! strpos($inPrg, $row['kode']))  $inPrg .= "'". $row['kode'] ."',";
			if ($row['level'] == 'kegiatan' And ! strpos($inKeg, $row['kode']))  $inKeg .= "'". $row['kode'] ."',";
			if ($row['level'] == 'output'   And ! strpos($inOut, $row['kode']))  $inOut .= "'". $row['kode'] ."',";
			if ($row['level'] == 'soutput'  And ! strpos($inSOu, $row['kode']))  $inSOu .= "'". $row['kode'] ."',";
			if ($row['level'] == 'komponen' And ! strpos($inKmp, substr($row['recid'],-16)))  $inKmp .= "'". substr($row['recid'],-16) ."',";
			if ($row['level'] == 'jenbel'   And ! strpos($inBel, substr($row['kode'], 2,2)))  $inBel .= "'". substr($row['kode'], 2,2) ."',";
			if ($row['level'] == 'akun'     And ! strpos($inAkn, $row['kode']))  $inAkn .= "'". $row['kode'] ."',";
		}
		$inStk .= "'ok')";  $inPrg .= "'ok')"; $inKeg .= "'ok')"; $inOut .= "'ok')"; $inSOu .= "'ok')"; $inKmp .= "'ok')"; $inBel .= "'ok')"; $inAkn .= "'ok')";

		$query = $ref->query("Select kdsatker kode, nmsatker uraian From t_satker Where $inStk");
		$t_stk = $this->fc->ToArr($query->result_array(), 'kode');
		$query = $ref->query("Select concat(kddept,'.',kdunit,'.',kdprogram) kode, nmprogram uraian From t_program Where $inPrg");
		$t_prg = $this->fc->ToArr($query->result_array(), 'kode');
		$query = $ref->query("Select kdgiat kode, nmgiat uraian From t_giat Where $inKeg");
		$t_keg = $this->fc->ToArr($query->result_array(), 'kode');
		$query = $ref->query("Select concat(kdgiat,'.',kdoutput) kode, nmoutput uraian, sat From t_output Where $inOut");
		$t_out = $this->fc->ToArr($query->result_array(), 'kode');
		$query = $ref->query("Select concat(kdgiat,'.',kdoutput,'.',kdsoutput) kode, nmsoutput uraian From t_soutput Where $inSOu");
		$t_sou = $this->fc->ToArr($query->result_array(), 'kode');
		$query = $ref->query("Select concat(kdgiat,'.',kdoutput,'.',kdsoutput,'.',kdkmpnen) kode, nmkmpnen uraian From t_kmpnen Where $inKmp");
		$t_kmp = $this->fc->ToArr($query->result_array(), 'kode');
		$query = $ref->query("Select kdbeban kode, concat('[',trim(nmbeban2),'] ') uraian From t_beban");
		$t_bbn = $this->fc->ToArr($query->result_array(), 'kode');
		$query = $ref->query("Select kdgbkpk kode, nmgbkpk uraian From t_gbkpk Where $inBel");
		$t_bel = $this->fc->ToArr($query->result_array(), 'kode');
		$query = $ref->query("Select kdakun kode, nmakun uraian From t_akun Where $inAkn");
		$t_akn = $this->fc->ToArr($query->result_array(), 'kode');

		$matrx = $this->fc->array_index($matrx, 'recid');
		foreach ($matrx as $key=>$row) {
			// Update URAIAN sesuai Table Referensi untuk DATA DETIL
			if ($row['level'] == 'satker'   And array_key_exists($row['kode'], $t_stk)) $matrx[$key]['uraian'] = $t_stk[ $row['kode'] ]['uraian'];
			if ($row['level'] == 'program'  And array_key_exists($row['kode'], $t_prg)) $matrx[$key]['uraian'] = $t_prg[ $row['kode'] ]['uraian'];
			if ($row['level'] == 'kegiatan' And array_key_exists($row['kode'], $t_keg)) $matrx[$key]['uraian'] = $t_keg[ $row['kode'] ]['uraian'];
			if ($row['level'] == 'output'   And array_key_exists($row['kode'], $t_out)) { $matrx[$key]['uraian'] = $t_out[ $row['kode'] ]['uraian'] .'<span style="font-style: italic"> ['. $t_out[ $row['kode'] ]['sat'] .']</span>'; $matrx[$key]['sat'] = $t_out[ $row['kode'] ]['sat']; }
			if ($row['level'] == 'soutput'  And array_key_exists($row['kode'], $t_sou)) { $matrx[$key]['uraian'] = $t_sou[ $row['kode'] ]['uraian'] .'<span style="font-style: italic"> ['. $t_out[ substr($row['kode'],0,8) ]['sat'] .']</span>'; $matrx[$key]['sat'] = $t_out[ substr($row['kode'],0,8) ]['sat']; }
			if ($row['level'] == 'komponen' And array_key_exists(substr($row['recid'],-16), $t_kmp)) $matrx[$key]['uraian'] = $t_kmp[ substr($row['recid'],-16) ]['uraian'] .'<span style="font-style: italic"> ['. $matrx[$key]['sat'] .']</span>';
			if ($row['level'] == 'jenbel'   And array_key_exists(substr($row['kode'], 0,1), $t_bbn)) $matrx[$key]['uraian'] = $t_bbn[ substr($row['kode'], 0,1) ]['uraian'];
			if ($row['level'] == 'jenbel'   And array_key_exists(substr($row['kode'], 2,2), $t_bel)) $matrx[$key]['uraian'].= $t_bel[ substr($row['kode'], 2,2) ]['uraian'];
			if ($row['level'] == 'akun'     And array_key_exists($row['kode'], $t_akn)) $matrx[$key]['uraian'] = $t_akn[ $row['kode'] ]['uraian'];

			$V = '';  if ($row['vol3'] <  0) $V = '- '; $J = '';  if ($row['jml3'] <  0) $J = '- '; $B = '';  if ($row['blok3'] <  0) $B = '- ';
			if ($row['vol1'] == 0)  $matrx[$key]['vol1'] = ''; else $matrx[$key]['vol1'] = 	  number_format($row['vol1'], 0, ',', '.');
			if ($row['vol2'] == 0)  $matrx[$key]['vol2'] = ''; else $matrx[$key]['vol2'] =    number_format($row['vol2'], 0, ',', '.');
			if ($row['vol3'] == 0)  $matrx[$key]['vol3'] = ''; else $matrx[$key]['vol3'] = $V.number_format(abs($row['vol3']), 0, ',', '.');
			if ($row['jml1'] == 0)  $matrx[$key]['jml1'] = ''; else $matrx[$key]['jml1'] =    number_format($row['jml1']/1000, 0, ',', '.');
			if ($row['jml2'] == 0)  $matrx[$key]['jml2'] = ''; else $matrx[$key]['jml2'] =    number_format($row['jml2']/1000, 0, ',', '.');
			if ($row['jml3'] == 0)  $matrx[$key]['jml3'] = ''; else $matrx[$key]['jml3'] = $J.number_format(abs($row['jml3']/1000), 0, ',', '.');
			if ($row['blok1'] == 0) $matrx[$key]['blok1']= ''; else $matrx[$key]['blok1']=    number_format($row['blok1']/1000,0, ',', '.');
			if ($row['blok2'] == 0) $matrx[$key]['blok2']= ''; else $matrx[$key]['blok2']=    number_format($row['blok2']/1000,0, ',', '.');
			if ($row['blok3'] == 0) $matrx[$key]['blok3']= ''; else $matrx[$key]['blok3']= $B.number_format(abs($row['blok3']/1000),0, ',', '.');
		}

		return $matrx;
	}

	function mundur_status($lvl,$rev_id){
		$this->fc->logRevisi($rev_id, "mundur_status - $rev_id");

		$sql = " t6_selesai_tgl=null, t6_selesai_ip=null, t6_check_status=null, t6_surat_no=null, t6_surat_tgl=null, t6_surat_file=null, t6_catatan=null, t7_status=null, t7_proses_tgl=null, t7_proses_ip=null, t7_selesai_tgl=null, t7_selesai_ip=null, t7_nd_no=null, t7_nd_tgl=null, t7_nd_file=null, t7_nd_catatan=null, t7_sp_tgl=null, t7_sp_file=null, t7_penetapan_tgl=null, t7_catatan=null  WHERE rev_id='$rev_id'";

		if($lvl == '2') $this->dbrevisi->query("UPDATE revisi SET rev_tahap='2', rev_status='9', t2_status='1', t2_selesai_tgl=null, t2_selesai_ip =null, t2_tolak_no=null,t2_tolak_tgl=null, t2_tolak_hal=null, t2_tolak_file=null, t2_catatan=null, t3_status=null,t3_proses_tgl=null, t3_proses_ip=null, t3_selesai_tgl=null, t3_selesai_ip=null, t3_und_no=null, t3_und_tgl=null, t3_und_file=null, t3_catatan=null, t3_penelaahan_tgl=null, t4_status=null,t4_proses_tgl=null,t4_proses_ip=null, t4_selesai_tgl=null, t4_selesai_ip=null, t4_catatan=null, t4_ba_no=null, t4_ba_tgl=null, t4_ba_file=null, t4_menteri_no=null, t4_menteri_tgl=null, t4_menteri_file=null, t4_perbaikan_status=null, t4_perbaikan_catatan=null, t5_status=null,t5_proses_tgl=null, t5_proses_ip=null, t5_selesai_tgl=null, t5_selesai_ip=null, t5_hasil_status=null, t5_hasil_no=null, t5_hasil_tgl=null, t5_hasil_file=null, t5_hasil_catatan=null, t6_status=null, t6_proses_tgl=null, t6_proses_ip=null,$sql ");

		if($lvl == '3') $this->dbrevisi->query("UPDATE revisi SET rev_tahap='6',rev_status='9', t3_status=null,t3_proses_tgl=null,t3_proses_ip=null, t3_selesai_tgl=null, t3_selesai_ip=null, t3_und_no=null, t3_und_tgl=null, t3_und_file=null, t3_catatan=null, t3_penelaahan_tgl=null, t4_status=null,t4_proses_tgl=null,t4_proses_ip=null, t4_selesai_tgl=null, t4_selesai_ip=null, t4_catatan=null, t4_ba_no=null, t4_ba_tgl=null, t4_ba_file=null, t4_menteri_no=null, t4_menteri_tgl=null, t4_menteri_file=null, t4_perbaikan_status=null, t4_perbaikan_catatan=null, t5_status=null,t5_proses_tgl=null, t5_proses_ip=null, t5_selesai_tgl=null, t5_selesai_ip=null, t5_hasil_status=null, t5_hasil_no=null, t5_hasil_tgl=null, t5_hasil_file=null, t5_hasil_catatan=null, t6_status='1', $sql ");
	}
	
	function reportKanwil($sta, $end){
		// echo $sta.'#'.$end;exit;
		//cari kodesatkernya yg sudah selesai *belum dibatasi per kanwil*//
		$lokasi = $this->session->userdata('kdlokasi'); 
		// echo $lokasi;exit;
		$query = $this->dbrevisi->query("SELECT rev_id FROM revisi_satker WHERE kdlokasi = '$lokasi' "); 
		$hasil = $query->result_array();
		// echo '<pre>';print_r($hasil);exit;
		if ($hasil != null){
			$whr 	= " and rev_id ". $this->fc->InVar($hasil, 'rev_id'); 
		} 
		// } else { $whr = "and rev_id in ('')" ; }

		$qry 	= $this->dbrevisi->query("SELECT kl_satker,'' nmsatker,'' kppn, '' nmdept,'' nmunit, kl_dept, kl_unit, kl_surat_no, kl_surat_tgl, kl_satker, rev_ke,rev_jns_revisi, rev_tahap, rev_status, t7_sp_tgl, t7_sp_no,kl_dept,kl_unit FROM revisi WHERE rev_upload_level=3 AND rev_upload_step=3 AND rev_tahap=7 AND rev_status=2 AND pus_tgl>='$sta 00:00:00' AND pus_tgl<='$end 23:59:59' $whr ORDER BY pus_tgl ");
		$arr 	= $this->fc->ToArr($qry->result_array(),'kl_satker');
		if(!$arr) return 'No Data Available';
		$qry 	= $this->dbref->query("SELECT a.kdsatker, a.nmsatker,a.kdkppn,b.nmkppn,a.kddept kddept,a.kdunit kdunit FROM t_satker a LEFT JOIN t_kppn b ON a.kdkppn=b.kdkppn WHERE a.kdsatker ".$this->fc->InVar($arr,'kl_satker'));
		$satk   = $this->fc->ToArr($qry->result_array(),'kdsatker');
		$qry   = $this->dbref->query("SELECT a.kddept, a.nmdept,b.kddept,b.kdunit,b.nmunit FROM t_dept a LEFT JOIN t_unit b ON a.kddept=b.kddept WHERE a.kddept ".$this->fc->InVar($qry->result_array(),'kddept'));
		$dept   =  $this->fc->ToArr($qry->result_array(),'kddept');

		foreach($arr as $row){
			if(array_key_exists($row['kl_satker'],$satk)){
				$arr[$row['kl_satker']]['nmsatker'] = $satk[$row['kl_satker']]['nmsatker'];
				$arr[$row['kl_satker']]['kppn']     = $satk[$row['kl_satker']]['nmkppn'];
			}
			if(array_key_exists($row['kl_dept'], $dept)){
				$arr[$row['kl_satker']]['nmdept']  	= $dept[$row['kl_dept']]['nmdept'];
				$arr[$row['kl_satker']]['nmdept']  	= $dept[$row['kl_dept']]['nmdept'];
				$arr[$row['kl_satker']]['nmunit'] 	= $dept[$row['kl_dept']]['nmunit'];
			}	
		}
		// echo '<pre>';print_r($arr);exit;
		return $arr;
	}
}
