<?php
class M_profile extends CI_Model {
	public function get_data() {
		$this->satu = $this->load->database('dbsatu', TRUE);
		$iduser = $this->session->userdata('iduser');
		$query  = $this->db->query("select a.*, b.intern, b.ekstern from t_user a left join t_so b on a.kdso = b.kdso where a.iduser='$iduser'");
		$data   = $query->row_array();

		$query  = $this->satu->query("select idws from ws_user where script='$iduser'");
		$link   = $query->row_array();
		$data['link'] = $link['idws'];
		$this->create_qr($iduser, $link['idws']);
		return $data;
	}

	// membuat function 'save' untuk penyimpanan
	public function save(  ) {
		$iduser    = $_POST['iduser'];
		$nmalias   = $_POST['nmalias'];
		$password  = $_POST['password'];
		$password0 = $_POST['password0'];
		$password1 = $_POST['password1'];
		$email     = $_POST['email'];
		$nohp      = $_POST['nohp'];

		// QRcode Gen
		$name	   = $this->session->userdata('nmuser');

		require_once APPPATH."/third_party/phpqrcode/qrlib.php";
		require_once APPPATH."/third_party/phpqrcode/qrconfig.php";

		$tempDir = 'files/QR/';
		$phone   = '';
		$orgName = '';

		// we building raw data
		$codeContents  = 'BEGIN:VCARD'."\n";
		$codeContents .= 'VERSION:2.1'."\n";
		$codeContents .= 'N:'.$nmalias."\n";
		$codeContents .= 'FN:'.$name."\n";
		$codeContents .= 'ORG:'.$orgName."\n";

		$codeContents .= 'TEL;WORK;VOICE:'.$phone."\n";
		$codeContents .= 'TEL;TYPE=cell:'.$nohp."\n";
		$codeContents .= 'EMAIL:'.$email."\n";

		$codeContents .= 'END:VCARD';

		QRcode::png($codeContents, $tempDir.$iduser.'.png', QR_ECLEVEL_L, 2);

		// jika ubah paket melakukan ini.......
		$sql = 	"update t_user set nmalias='$nmalias',email='$email',nohp='$nohp' where iduser='$iduser'";

		$query = $this->db->query( $sql );

		if ( $password != '' and $password==$password1 ) 	 	$this->db->query("update t_user set password=md5('". $password ."') where iduser='$iduser'" );
		// if ( $password != '' and $password==$password1 and md5($password0)==$user['password'] ) 	 	$this->db->query("update t_user set password=md5('". $password ."') where iduser='$iduser'" );
		// if ( $password != '' ) 	 	$this->db->query("update t_user set password=md5('". $password ."') where iduser='$iduser'" );

		return;
	}

	public function create_qr($iduser, $link) {
		require_once APPPATH."/third_party/phpqrcode/qrlib.php";
		require_once APPPATH."/third_party/phpqrcode/qrconfig.php";

		$logopath = site_url("files/images/depkeu_round.png");
		$filepath = "files/QR/$iduser"."_presensi.png";
		$codelink = "https://satudja.kemenkeu.go.id/presensi?q=$link";
		QRcode::png("$codelink", $filepath, "L", 10);
	}

}
