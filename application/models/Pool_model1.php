<?php
class Pool_model1 extends CI_Model {

	public function get_grid() {
		$query = $this->db->query("Select * From d_pool");
		$hasil = $query->result_array();
		return $hasil;
	}

	public function get_row() {
		$idpool = $this->uri->segment(5) 
		if ($idpool==null) $idpool=1969;
		$query = $this->db->query("Select * From d_pool Where idpool=$idpool");
		$hasil = $query->row_array();
		return $hasil;
	}

	public function save() {
		$action		= $_POST['ruh'];
		$idpool		= $_POST['idpool'];
		$nmpool		= $_POST['nmpool'];
		$deskripsi 	= $_POST['deskripsi'];
		$owner		= $_POST['owner'];
		$kdaktif	= $_POST['kdaktif'];
		$tglopen	= $this->fc->ustgl( $_POST['tglopen'], 'hari');
		$tglclose	= $this->fc->ustgl( $_POST['tglclose'], 'hari');

		if ($kdaktif==1) { $this->db->query( "update d_pool set kdaktif='0'" ); } else { $kdaktif='0'; }

		if ($action=='Rekam') {
			$this->db->query( "insert into d_pool (nmpool, deskripsi, tglopen, tglclose, owner, kdaktif) values ('$nmpool', '$deskripsi', $tglopen, '$tglclose', '$owner', '$kdaktif')" );
		}
		if ($action=='Ubah') {
			$this->db->query( "update d_pool set nmpool='$nmpool', deskripsi='$deskripsi', tglopen='$tglopen', tglclose='$tglclose', owner='$owner', kdaktif='$kdaktif' where idpool=$idpool" );
		}
		if ($action=='Hapus') {
			$this->db->query( "delete from d_pool where idpool=$idpool" );
		}
		return;
	}

	public function get_child( $idpool ) {
		$query = $this->db->query("Select * From d_pool_item Where idpool=$idpool");
		$child = $query->result_array();

		$msg = '';
		foreach ($child as $row) {
			$msg .= '
		        <tr class="child">
		        	<td id="'. $row['idpool'] .'" class="text-right"><small>'. $row['iditem'] .'</small></td>
	                <td id="'. $row['idpool'] .'"><strong><small>'. $row['nmitem'] .'</small></strong></td>
	                <td><small>'. $row['deskripsi'] .'</small></td>
	                <td class="text-right"><small>'. trim($row['thumbnail']) .'</small></td>
	                <td class="text-right"><small>'. $row['file'] .'</small></td>
                    <td>&nbsp;</td>
                    <td class="Item">&nbsp;</td>
                </tr>';
		}
		return $msg;
	}

}
