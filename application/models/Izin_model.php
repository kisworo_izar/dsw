<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Izin_model extends CI_Model
{
	/*
	This function is used to get tanggalmulai, tanggalselesai
	*/
	function getIzin($izin_id)
	{
		$this->dbmilea->select('*');
		$this->dbmilea->from('absen_manual');
		$this->dbmilea->where('id', $izin_id);

        $res = $this->dbmilea->get()->result();

        if (empty($res)) {
        	return null;
        }

        return $res[0];
	} 

	/**
	* This function is used to get the izin pemberitahuan list
	* @param $setuju_status
	* @param $tetap_status  
	*/
	function list_($setuju_status, $tetap_status='3', $bawahan_nip, $atasan_nip, $custom='')
	{
		$this->dbmilea->select('absen_manual.*, bawahan.nip18 bawahan_nip, bawahan.nama bawahan_nama, bawahan.jabatan bawahan_jabatan, atasan.nip18 atasan_nip, atasan.nama atasan_nama, atasan.jabatan atasan_jabatan, sdm.nip sdm_nip, sdm.nmuser sdm_nama, sdm.jabatan sdm_jabatan, COUNT(t_harikerja.tgl) jml');
		$this->dbmilea->from('absen_manual');
		$this->dbmilea->join('t_harikerja', 't_harikerja.tgl >= CAST(absen_manual.tanggalmulai as date) AND t_harikerja.tgl <= CAST(absen_manual.tanggalselesai as date) AND t_harikerja.kerja = "1"', 'left');
		$this->dbmilea->join('pegawai_induk as bawahan', 'bawahan.nip18 = absen_manual.nip', 'left');
		$this->dbmilea->join('pegawai_induk as atasan', 'atasan.nip18 = absen_manual.penandatangan', 'left');
		$this->dbmilea->join('t_user as sdm', 'sdm.iduser = absen_manual.idappoval', 'left');
		if ($setuju_status != '*') {
			$this->dbmilea->where('absen_manual.setuju_status', $setuju_status);
		}
		if ($tetap_status != '*') {
			$this->dbmilea->where('absen_manual.approval', $tetap_status);
		}
		if (!empty($bawahan_nip)) {
			$this->dbmilea->where('bawahan.nip18', $bawahan_nip);
		}
		if (!empty($atasan_nip)) {
			$this->dbmilea->where('atasan.nip18', $atasan_nip);
		}
		if (!empty($custom)) {
			$this->dbmilea->where($custom);
		}
		$this->dbmilea->order_by("absen_manual.tanggalsurat", "desc");
		$this->dbmilea->order_by("absen_manual.id", "desc");
		$this->dbmilea->group_by("absen_manual.id");

        return $this->dbmilea->get()->result();
	}

	/*
	Return izin list of $nip's bawahan
	*/
	function bawahanList($nip)
	{
		$this->dbmilea->select('absen_manual.*, pegawai_induk.nip18, pegawai_induk.nama, pegawai_induk.jabatan bawahan_jabatan, DATEDIFF(absen_manual.tanggalmulai, absen_manual.tanggalselesai)+1 jml');
		$this->dbmilea->from('absen_manual');
		$this->dbmilea->join('pegawai_induk', 'pegawai_induk.nip18 = absen_manual.nip', 'left');
		$this->dbmilea->where('absen_manual.penandatangan', $nip);
		$this->dbmilea->order_by("absen_manual.tanggalsurat", "desc");

        return $this->dbmilea->get()->result();
	}

	/* Fungsi ini untuk menyimpan pengajuan (Izin Baru) */ 
	function addIzin($izinInfo){
        $this->dbmilea->trans_start();

		$this->dbmilea->insert('absen_manual', $izinInfo);

        $insert_id = $this->dbmilea->insert_id();

        $this->dbmilea->trans_complete();

        return $insert_id;
	}

	/* Update Izin */
	function updateIzin($izinInfo, $izinId)
	{
		$this->dbmilea->where('id', $izinId);
		$this->dbmilea->update('absen_manual', $izinInfo);

		return (int)$izinId;
	}

	function deleteIzin($id)
	{
		return $this->dbmilea->delete('absen_manual', ['id' => $id]); 
	}

	function setuju($id, $status, $catatan = null)
	{
		$this->dbmilea->where('id', $id);
		$izin = array(
			'setuju_status' => $status,
			'setuju_waktu' => date("Y-m-d"),
			'catatan' => $catatan
		);
		$this->dbmilea->update('absen_manual', $izin);

        return $this->dbmilea->affected_rows();
	}

	function penetapan($id, $status, $catatan = null)
	{
		$this->dbmilea->where('id', $id);
		$izin = array(
			'approval' => $status,
			'tanggalapproval' => date("Y-m-d"),
			'idappoval' => $this->session->userdata('iduser'),
			'catatan' => $catatan
		);
		$this->dbmilea->update('absen_manual', $izin);

        return $this->dbmilea->affected_rows();
	}

	/* Fungsi ini untuk mengecek apakah User merupakan atasan dari User yang mengajukan Izin */
	function isAtasan($user_id, $izin_id){
		$res = $this->dbmilea->get_where('absen_manual', array('id' => $izin_id, 'penandatangan' => $user_id));
		if (!empty($res)) {
			return TRUE;
		}
		return FALSE;
	}
}