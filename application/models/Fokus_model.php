<?php
class Fokus_model extends CI_Model {

    function anggota_tampil($bid){
        $query = $this->db->query("Select anggota From t_moncap_bidang where id_bidang=$bid");
        $array = $query->row_array();

        $query = $this->db->query("Select iduser, nmuser From t_user where iduser in ('". str_replace(',', "','", $array['anggota']) ."')");
        $user  = $this->fc->ToArr( $query->result_array(), 'iduser');

        $team  = explode(',', $array['anggota']);
        for ($i=0; $i<count($team); $i++) {
            if ( array_key_exists($team[$i], $user) ) {
                $hasil[ $team[$i] ] = $user[ $team[$i] ];
            } else {
                $hasil[ $team[$i] ] = array('iduser'=>$team[$i], 'nmuser'=>$team[$i]);
            }
        }
        return $hasil;
    }

    function moncap_tampil_admin(){
        $tahun      =date("Y");
        $sql="SELECT * FROM d_moncap WHERE  kd_jenis='1' and tahun='$tahun' ORDER BY  id_moncap ASC   ";
        return $this->db->query($sql)->result();
    }

    function moncap_tampil($bid){
        $tahun      =date("Y");
        $sql="SELECT * FROM d_moncap WHERE id_bidang='$bid' AND kd_jenis='1' and tahun='$tahun' ORDER BY id_moncap ASC   ";
        return $this->db->query($sql)->result();
    }

    function moncap_input($id_bidang,$kd_jenis,$output,$volume,$progres,$tgl_batas,$catatan,$tahun){
        //echo $this->uri->segment(3);exit();
        $query="INSERT INTO d_moncap (id_bidang,kd_jenis,output,volume,progres,tgl_batas,catatan,tahun) VALUES ('$id_bidang','$kd_jenis','$output','$volume','$progres','$tgl_batas','$catatan','$tahun')"; //echo $query; exit();
        $sql=$this->db->query($query);
        if ($sql) {
            return true;
        }
        else{
            return false;
        }
    }

    function moncap_edit($id_bidang,$id_moncap,$kd_jenis,$output,$volume,$progres,$tgl_batas,$catatan){
        $id_moncap=$this->input->post('id_moncap');//echo $output;exit();
        $query="UPDATE d_moncap SET     output     ='$output',
                                        volume     ='$volume',
                                        progres    ='$progres',
                                        tgl_batas  ='$tgl_batas',
                                        catatan    ='$catatan'
            WHERE id_moncap=$id_moncap "; //echo $query;exit();
        $sql=$this->db->query($query);
        if ($sql) {
            return true;
        }
        else{
            return false;
        }
    }

    function moncap_del($id_moncap,$id_bidang){
        $query="DELETE FROM d_moncap WHERE id_moncap=$id_moncap";

        $sql=$this->db->query($query);

        if ($sql) {
            return true;
        }
        else{
            return false;
        }
    }

    function matin_tampil($bid){
        $sql="SELECT * FROM d_moncap WHERE id_bidang='$bid' AND kd_jenis='3'  ";
        return $this->db->query($sql)->result();
    }

    function matin_input($id_bidang,$kd_jenis,$output,$catatan,$tgl_tkda_a,$tgl_tkda_z,$tgl_da_a,$tgl_da_z){

        $query="INSERT INTO d_moncap (id_bidang,kd_jenis,output,catatan,tgl_tkda_a,tgl_tkda_z,tgl_da_a,tgl_da_z) VALUES ('$id_bidang','$kd_jenis','$output','$catatan','$tgl_tkda_a','$tgl_tkda_z','$tgl_da_a','$tgl_da_z')"; //echo $query; exit();
        $sql=$this->db->query($query);
        if ($sql) {
            return true;
        }
        else{
            return false;
        }
    }
    function matin_edit($id_bidang,$id_moncap,$kd_jenis,$output,$catatan,$tgl_tkda_a,$tgl_tkda_z,$tgl_da_a,$tgl_da_z){
        $id_moncap=$this->input->post('id_moncap');//echo $output;exit();
        $query="UPDATE d_moncap SET output     ='$output',
                                    catatan    ='$catatan',
                                    tgl_tkda_a ='$tgl_tkda_a',
                                    tgl_tkda_z ='$tgl_tkda_z',
                                    tgl_da_a   ='$tgl_da_a',
                                    tgl_da_z   ='$tgl_da_z'
            WHERE id_moncap=$id_moncap "; //echo $query;exit();
        $sql=$this->db->query($query);
        if ($sql) {
            return true;
        }
        else{
            return false;
        }
    }

    function monkeg_tampil($bid){
        $sql="SELECT * FROM d_moncap WHERE id_bidang='$bid' AND kd_jenis='2'  ";
        return $this->db->query($sql)->result();
    }

    function monkeg_tampil_admin(){
        $sql="SELECT * FROM d_moncap WHERE kd_jenis='2' ";
        return $this->db->query($sql)->result();
    }

    public function save(  ) {
        $id_bidang  =$this->input->post('id_bidang');
        $kd_jenis   =$this->input->post('kd_jenis');
        $output     =$_POST['output'];
        $gambar     =$_FILES['gambar'];
        //$file         = $_FILES['file'];

        if ($gambar) move_uploaded_file($gambar["tmp_name"], "files/moncap/".$gambar['name']);
        //if ($file)   move_uploaded_file($file["tmp_name"], "files/tk_gambar/".$file['name']);

        $sql =  "INSERT INTO d_moncap (id_bidang,kd_jenis,output,link_img) values ('$id_bidang','$kd_jenis','$output','". $gambar['name'] ."')";
        //echo $sql; exit();
        $query = $this->db->query( $sql );
        return;
    }

    function select_bidang($bid){
        if (empty($bid)) {
            $where ="";
        } else {
            $where = "where id_bidang='$bid'";
        }

        $sql="SELECT * FROM t_moncap_bidang ".$where." ORDER BY id_bidang ASC";
        return $this->db->query($sql)->result();
    }

    function select_jenis(){
        $sql="SELECT * FROM t_jenis ORDER BY kd_jenis ASC";
        return $this->db->query($sql)->result();
    }

    function bidang_link(){
        $sql="SELECT * FROM t_menu WHERE idmenu in ('020201','020202','020203','020204','020205','020206') ORDER BY menu asc ";
        return $this->db->query($sql)->result();
    }
}


for ($i=1; $i <=10 ; $i++) { 
    echo "
    <tr>
      <td>Peraturan Menteri Keuangan Nomor XI tahun 2017</td>
      <td align='center'> Dokumen </td> 
      <td align='right'>Download</td>
    </tr>
    ";
}
