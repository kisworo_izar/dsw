<?php 
class Pengumuman_model extends CI_Model {
	public function get_data() {
		$whr  = "";
		$cari = $this->session->userdata('cari');

		if ($cari) {
			$whr = " where " ;
			$arr = explode(' ', $cari);
			for ($i=0; $i<count($arr); $i++) {
				$str  = $arr[$i];
				$whr .= "judul like '%$str%' or pengumuman like '%$str%' or gambar like '%$str%' or file like '%$str%' ";
				if ($i<count($arr)-1) {
					$whr .= " or ";
				}
			}
		}

		// cari query data dari d_paket
		$query = $this->db->query("select *  from d_pengumuman $whr order by idpengumuman desc ");
		$hasil = $this->fc->ToArr( $query->result_array(), 'idpengumuman');
		return $hasil;
	}

	public function get_row( $idpengumuman=null ) {
		if ( is_null($idpengumuman) ) $idpengumuman='999999';
		$query = $this->db->query("select *  from d_pengumuman where idpengumuman=$idpengumuman");
		$hasil = $query->row_array();
		return $hasil;
	}

	public function save(  ) {
		$action		  = $_POST['ruh'];
		$idpengumuman = $_POST['idpengumuman'];
		$judul		  = $_POST['judul'];
		$pengumuman   = $_POST['pengumuman'];
		$toplist   	  = $_POST['toplist'];
		$gambar		  = $_FILES['gambar'];		  
		$file		  = $_FILES['file'];		  

		if ($gambar) move_uploaded_file($gambar["tmp_name"], "files/pengumuman/".$gambar['name']);
		if ($file)   move_uploaded_file($file["tmp_name"], "files/pengumuman/".$file['name']);
		if ($toplist==1) { $this->db->query( "update d_pengumuman set toplist='0'" ); } else { $toplist='0'; }

		if ($action=='Rekam') {
			$this->db->query( "insert into d_pengumuman (tglrekam,judul,pengumuman,toplist,gambar,file) values (current_timestamp(),'$judul','$pengumuman','$toplist','". $gambar['name'] ."','". $file['name'] ."')" );
		}
		if ($action=='Ubah') {
			$this->db->query( "update d_pengumuman set judul='$judul', pengumuman='$pengumuman', toplist='$toplist' where idpengumuman=$idpengumuman" );
			if ( $gambar['name'] != '' )  	$this->db->query("update d_pengumuman set gambar='". $gambar['name'] ."' where idpengumuman=$idpengumuman" );
			if ( $file['name'] != '' ) 	 	$this->db->query("update d_pengumuman set file='". $file['name'] ."' where idpengumuman=$idpengumuman" );
		}
		if ($action=='Hapus') {
			$this->db->query( "delete from d_pengumuman where idpengumuman=$idpengumuman" );
		}
		return;
	}
}
