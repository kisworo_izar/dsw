<?php
class Absensi_model extends CI_Model {
	/* GET JUMLAH HARI PEGAWAI BERKERJA */
	function jumlahHariKerjaPegawai($nip, $tanggalawal, $tanggalakhir)
	{
		$this->dbmilea->select('*');
		$this->dbmilea->from('t_kehadiran');
		$this->dbmilea->where('sts', '1');
		$statuss = ['HN', 'TK'];
		foreach ($this->dbmilea->get()->result() as $row) {
			array_push($statuss, $row->kode);
		}

		$this->dbmilea->select('*');
		$this->dbmilea->from('absen_net');
		$this->dbmilea->where('nip', $nip);
		$this->dbmilea->where('tgl >=', $tanggalawal);
		$this->dbmilea->where('tgl <=', $tanggalakhir);
		$this->dbmilea->where_in('status', $statuss);

		return $this->dbmilea->count_all_results();
	}

	function pelanggaranHistory($year, $so)
	{
		// $this->dbmilea->select('kode');
		// $this->dbmilea->from('t_kehadiran');
		// $this->dbmilea->where('sts', '1');
		// $statuss = ['TK'];
		// foreach ($this->dbmilea->get()->result() as $row) {
		// 	array_push($statuss, $row->kode);
		// }
		//
		//
		// $this->dbmilea->select(
		// 	'MAX(absen_manual.id) izin_id,
		// 	absen_net.nip nip,
		// 	absen_net.tgl tanggal,
		// 	absen_net.status status,
		// 	absen_net.pelanggaran pelanggaran,
		// 	pegawai_induk.nama nama,
		// 	pegawai_induk.jabatan jabatan,
		// 	pegawai_induk.esl1 esl1,
		// 	pegawai_induk.esl2 esl2,
		// 	pegawai_induk.esl3 esl3,
		// 	pegawai_induk.esl4 esl4,
		// 	absen_manual.setuju_status setuju_status,
		// 	absen_manual.setuju_waktu setuju_waktu,
		// 	absen_manual.approval tetap_status,
		// 	absen_manual.tanggalapproval tetap_waktu'
		// );
		// $this->dbmilea->from('absen_net');
		// $this->dbmilea->join('pegawai_induk', 'pegawai_induk.nip18 = absen_net.nip', 'left');
		// $this->dbmilea->join('absen_manual', "(absen_manual.nip = absen_net.nip and absen_net.tgl >= CAST(absen_manual.tanggalmulai as DATE)  and absen_net.tgl <= CAST(absen_manual.tanggalselesai as DATE) and (absen_manual.status = absen_net.status or (absen_net.status = 'TK' and absen_manual.status = 'IP')))", 'left');
		// $this->dbmilea->where_in('absen_net.status', $statuss);
		// $this->dbmilea->where('YEAR(absen_net.tgl)', $year);
		// $this->dbmilea->where('pegawai_induk.esl'.$so['eselon'], trim(str_replace('\n', '', $so['nama'])));
		// $this->dbmilea->where('pegawai_induk.esl3 <>', '');
		// $this->dbmilea->where_in('pegawai_induk.uraianstatus', ['Aktif','Calon Pegawai Negeri Sipil']);
		// $this->dbmilea->where('pegawai_induk.updatedtime >=', 'NOW() - INTERVAL 1 DAY');
		// $this->dbmilea->group_by(['absen_net.nip', 'absen_net.tgl']);
		// $this->dbmilea->order_by('pegawai_induk.nama', 'asc');
		// $this->dbmilea->order_by('absen_net.tgl', 'desc');
		//
		// return $this->dbmilea->get()->result();

		$query = "SELECT MAX(absen_manual.id) izin_id, absen_net.nip nip, absen_net.tgl tanggal, absen_net.status status, absen_net.pelanggaran pelanggaran, pegawai_induk.nama nama, pegawai_induk.jabatan jabatan, pegawai_induk.esl1 esl1, pegawai_induk.esl2 esl2, pegawai_induk.esl3 esl3, pegawai_induk.esl4 esl4, absen_manual.setuju_status setuju_status, absen_manual.setuju_waktu setuju_waktu, absen_manual.approval tetap_status, absen_manual.tanggalapproval tetap_waktu FROM absen_net LEFT JOIN pegawai_induk ON pegawai_induk.nip18 = absen_net.nip LEFT JOIN absen_manual ON absen_manual.nip = absen_net.nip and absen_net.tgl >= CAST(absen_manual.tanggalmulai as DATE) and absen_net.tgl <= CAST(absen_manual.tanggalselesai as DATE) and (absen_manual.status = absen_net.status or (absen_net.status = 'TK' and absen_manual.status = 'IP')) WHERE absen_net.status IN('TK', 'CTPP3', 'CTPP4', 'CTPT1', 'CTPT1P1', 'CTPT1P2', 'CTPT1P3', 'CTPT1P4', 'CTPT2', 'CTPT2P1', 'CTPT2P2', 'CTPT2P3', 'CTPT2P4', 'CTPT3', 'CTPT3P1', 'CTPT3P2', 'CTPT3P3', 'CTPT3P4', 'CTSP1', 'CTSP2', 'CTSP3', 'CTSP4', 'CTST1', 'CTST1P1', 'CTST1P2', 'CTST1P3', 'CTST1P4', 'CTST2', 'CTST2P1', 'CTST2P2', 'CTST2P3', 'CTST2P4', 'CTST3', 'CTST3P1', 'CTST3P2', 'CTST3P3', 'CTST3P4', 'IP', 'PSW1', 'PSW2', 'PSW3', 'PSW4', 'T1P1', 'T1P2', 'T1P3', 'T1P4', 'T2P1', 'T2P2', 'T2P3', 'T2P4', 'T3P1', 'T3P2', 'T3P3', 'T3P4', 'TL1', 'TL2', 'TL3', 'TMH') AND YEAR(absen_net.tgl) = '2019' AND pegawai_induk.esl".$so['eselon']." = '".trim(str_replace('\n', '', $so['nama']))."' AND pegawai_induk.esl3 <> '' AND pegawai_induk.uraianstatus IN('Aktif', 'Calon Pegawai Negeri Sipil') AND pegawai_induk.updatedtime >= 'NOW() - INTERVAL 1 DAY' GROUP BY absen_net.nip, absen_net.tgl ORDER BY pegawai_induk.nama ASC, absen_net.tgl DESC";

		return $this->dbmilea->query($query)->result();

	}

	function pelanggaranBulanan($so)
	{
		$this->dbmilea->select('kode');
		$this->dbmilea->from('t_kehadiran');
		$this->dbmilea->where('sts', '1');
		$statuss = ['TK'];
		foreach ($this->dbmilea->get()->result() as $row) {
			array_push($statuss, $row->kode);
		}

		$eselon = ($so['eselon'] < 5) ? $so['eselon'] : 4;
		$eselon_bawah = ($so['eselon'] < 4) ? $eselon+1 : 4;

		$this->dbmilea->select('
			pegawai_induk.esl'.$eselon_bawah.' esl,
			month(absen_net.tgl) bulan,
			count(distinct absen_net.nip) n_pegawai
			');
		$this->dbmilea->from('absen_net');
		$this->dbmilea->join('pegawai_induk', 'pegawai_induk.nip18 = absen_net.nip', 'left');
		$this->dbmilea->where_in('absen_net.status', $statuss);
		$this->dbmilea->where('YEAR(absen_net.tgl)', date('Y'));
		$this->dbmilea->where('MONTH(absen_net.tgl) <=', (int) date('n'));
		$this->dbmilea->where('pegawai_induk.esl'.$eselon, trim(str_replace('\n', '', $so['nama'])));
		$this->dbmilea->where('pegawai_induk.esl'.$eselon_bawah.' <>', '');
		$this->dbmilea->where('pegawai_induk.esl3 <>', '');
		$this->dbmilea->where_in('pegawai_induk.uraianstatus', ['Aktif','Calon Pegawai Negeri Sipil']);
		$this->dbmilea->where('pegawai_induk.updatedtime >=', 'NOW() - INTERVAL 1 DAY');
		$this->dbmilea->group_by(['esl', 'bulan']);

		return $this->dbmilea->get()->result();
	}

	function pelanggaranJam($so)
	{
		$eselon = ($so['eselon'] < 5) ? $so['eselon'] : 4;
		$eselon_bawah = ($so['eselon'] < 4) ? $eselon+1 : 4;

		$this->dbmilea->select('
			pegawai_induk.esl'.$eselon_bawah.' esl,
			sum(absen_net.pelanggaran) pelanggaran,
			');
		$this->dbmilea->from('absen_net');
		$this->dbmilea->join('pegawai_induk', 'pegawai_induk.nip18 = absen_net.nip', 'left');
		$this->dbmilea->where('YEAR(absen_net.tgl)', date('Y'));
		$this->dbmilea->where('pegawai_induk.esl'.$eselon, trim(str_replace('\n', '', $so['nama'])));
		$this->dbmilea->where('pegawai_induk.esl'.$eselon_bawah.' <>', '');
		$this->dbmilea->where('pegawai_induk.esl3 <>', '');
		$this->dbmilea->where_in('pegawai_induk.uraianstatus', ['Aktif','Calon Pegawai Negeri Sipil']);
		$this->dbmilea->where('pegawai_induk.updatedtime >=', 'NOW() - INTERVAL 1 DAY');
		$this->dbmilea->group_by(['esl']);
		$this->dbmilea->order_by('pelanggaran', 'desc');

		return $this->dbmilea->get()->result();
	}

	function pelanggaranPegawaiNol($so)
	{
		$this->dbmilea->select('
			(CASE WHEN SUM(absen_net.pelanggaran)=0 THEN 1 ELSE 0 END) is_nol
			');
		$this->dbmilea->from('absen_net');
		$this->dbmilea->join('pegawai_induk', 'pegawai_induk.nip18 = absen_net.nip', 'left');
		$this->dbmilea->where('YEAR(absen_net.tgl)', date('Y'));
		$this->dbmilea->where('pegawai_induk.esl'.$so['eselon'], trim(str_replace('\n', '', $so['nama'])));
		$this->dbmilea->where('pegawai_induk.esl3 <>', '');
		$this->dbmilea->where_in('pegawai_induk.uraianstatus', ['Aktif','Calon Pegawai Negeri Sipil']);
		$this->dbmilea->where('pegawai_induk.updatedtime >=', 'NOW() - INTERVAL 1 DAY');
		$this->dbmilea->group_by(['absen_net.nip']);
		$n_pegawai = count($this->dbmilea->get()->result());

		$this->dbmilea->select('is_nol');
		$this->dbmilea->from('('.$this->dbmilea->last_query().') as T');
		$this->dbmilea->where('is_nol', 1);
		$n_pegawai_nol = $this->dbmilea->count_all_results();

		return ['nol' => $n_pegawai_nol, 'total' => $n_pegawai];
	}

	function pelanggaranIzinStatus($so)
	{
		// $this->dbmilea->select('kode');
		// $this->dbmilea->from('t_kehadiran');
		// $this->dbmilea->where('sts', '1');
		// $statuss = ['TK'];
		// foreach ($this->dbmilea->get()->result() as $row) {
		// 	array_push($statuss, $row->kode);
		// }
		//
		// $this->dbmilea->select('
		// 	absen_net.nip nip,
		// 	absen_net.tgl tanggal,
		// 	absen_manual.setuju_status setuju_status,
		// 	absen_manual.setuju_waktu setuju_waktu,
		// 	absen_manual.approval tetap_status,
		// 	absen_manual.tanggalapproval tetap_waktu
		// 	');
		// $this->dbmilea->from('absen_net');
		// $this->dbmilea->join('absen_manual', "absen_manual.nip = absen_net.nip and absen_net.tgl >= CAST(absen_manual.tanggalmulai as DATE)  and absen_net.tgl <= CAST(absen_manual.tanggalselesai as DATE) and (absen_manual.status = absen_net.status or (absen_net.status = 'TK' and absen_manual.status = 'IP'))", 'left');
		// $this->dbmilea->join('pegawai_induk', 'pegawai_induk.nip18 = absen_net.nip', 'left');
		// $this->dbmilea->where_in('absen_net.status', $statuss);
		// $this->dbmilea->where('YEAR(absen_net.tgl)', date('Y'));
		// $this->dbmilea->where('pegawai_induk.esl'.$so['eselon'], trim(str_replace('\n', '', $so['nama'])));
		// $this->dbmilea->where('pegawai_induk.esl3 <>', '');
		// $this->dbmilea->where_in('pegawai_induk.uraianstatus', ['Aktif','Calon Pegawai Negeri Sipil']);
		// $this->dbmilea->where('pegawai_induk.updatedtime >=', 'NOW() - INTERVAL 1 DAY');
		// $this->dbmilea->group_by(['absen_net.nip', 'absen_net.tgl']);
		// $this->dbmilea->order_by('absen_manual.id', 'desc');
		//
		// return $this->dbmilea->get()->result();

		$query = "SELECT absen_net.nip nip, absen_net.tgl tanggal, absen_manual.setuju_status setuju_status, absen_manual.setuju_waktu setuju_waktu, absen_manual.approval tetap_status, absen_manual.tanggalapproval tetap_waktu FROM absen_net LEFT JOIN absen_manual ON absen_manual.nip = absen_net.nip and absen_net.tgl >= CAST(absen_manual.tanggalmulai as DATE) and absen_net.tgl <= CAST(absen_manual.tanggalselesai as DATE) and (absen_manual.status = absen_net.status or (absen_net.status = 'TK' and absen_manual.status = 'IP')) LEFT JOIN pegawai_induk ON pegawai_induk.nip18 = absen_net.nip WHERE absen_net.status IN('TK', 'CTPP3', 'CTPP4', 'CTPT1', 'CTPT1P1', 'CTPT1P2', 'CTPT1P3', 'CTPT1P4', 'CTPT2', 'CTPT2P1', 'CTPT2P2', 'CTPT2P3', 'CTPT2P4', 'CTPT3', 'CTPT3P1', 'CTPT3P2', 'CTPT3P3', 'CTPT3P4', 'CTSP1', 'CTSP2', 'CTSP3', 'CTSP4', 'CTST1', 'CTST1P1', 'CTST1P2', 'CTST1P3', 'CTST1P4', 'CTST2', 'CTST2P1', 'CTST2P2', 'CTST2P3', 'CTST2P4', 'CTST3', 'CTST3P1', 'CTST3P2', 'CTST3P3', 'CTST3P4', 'IP', 'PSW1', 'PSW2', 'PSW3', 'PSW4', 'T1P1', 'T1P2', 'T1P3', 'T1P4', 'T2P1', 'T2P2', 'T2P3', 'T2P4', 'T3P1', 'T3P2', 'T3P3', 'T3P4', 'TL1', 'TL2', 'TL3', 'TMH') AND YEAR(absen_net.tgl) = '2019' AND pegawai_induk.esl".$so['eselon']." = '".trim(str_replace('\n', '', $so['nama']))."' AND pegawai_induk.esl3 <> '' AND pegawai_induk.uraianstatus IN('Aktif', 'Calon Pegawai Negeri Sipil') AND pegawai_induk.updatedtime >= 'NOW() - INTERVAL 1 DAY' GROUP BY absen_net.nip, absen_net.tgl ORDER BY absen_manual.id DESC";

		return $this->dbmilea->query($query)->result();
	}
}
