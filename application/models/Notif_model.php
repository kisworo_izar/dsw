<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Notif_model extends CI_Model {

	public function get_notif() {
		$iduser = $this->session->userdata('iduser');
		$query  = $this->db->query("Select * From d_notifikasi Where iduser='$iduser' And status='0'");
		return $query->result_array();
	}

	public function get_notif_history() {
		$iduser = $this->session->userdata('iduser');
		$query  = $this->db->query("Select * From d_notifikasi Where iduser='$iduser' ORDER BY waktu DESC");
		return $query->result_array();
	}

	public function notif_read( $idnotif ) {
		$iduser = $this->session->userdata('iduser');
		$query  = $this->db->query("Update d_notifikasi Set status='1' Where iduser='$iduser' And idnotif=$idnotif");
	}

	public function notif_clear( $idnotif ) {
		$iduser = $this->session->userdata('iduser');
		$query  = $this->db->query("Update d_notifikasi Set status='1' Where iduser='$iduser' And idnotif In ($idnotif)");
	}

	/*
	Insert new notif into database
	*/
	function add($notifInfo){
		$this->db->trans_start();
        $this->db->insert('d_notifikasi', $notifInfo);
        
        $insert_id = $this->db->insert_id();
        
        $this->db->trans_complete();
        
        return $insert_id;
	}
}
