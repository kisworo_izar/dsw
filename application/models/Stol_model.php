<?php
class Stol_model extends CI_Model {

	public function get_dash( $direktorat, $jenis, $start, $end ) {
		$whr  = "Where tglawal Between '$start' And '$end' ";
		if ($direktorat != '00') $whr .= "And eselon2='$direktorat' ";
		if ($jenis != '000') $whr .= "And jnsst='$jenis'";

		$query = $this->db->query("Select a.*, b.nmso, b.nmso1, '' lamp From st a Left Join t_so b On concat(a.eselon2,'0000')=b.kdso $whr Order By idst Desc");
		$hasil = $this->fc->ToArr( $query->result_array(), 'idst');
		// Melengkapi Lampiran setiap ST pada $hasil
		foreach ($hasil as $idst=>$row) {
			$query = $this->db->query("Select nip, nama, direktorat, '' nip2, '' nama2, '' direktorat2,'' tglawal,'' tglakhir ,'' jmplg From st_peg Where idst=$idst");
			$draft = $this->fc->ToArr( $query->result_array(), 'nip');
			if($row['jnsst']=='302'){
				$tgl   = $row['tglakhir'];
				$arr   = $this->milea->query("SELECT nip, tglpulang FROM absen_net WHERE tgl='$tgl'")->result_array();
				$dat   = $this->fc->ToArr($arr,'nip');
			}
			$query = $this->db->query("Select nip, nama, direktorat,tglawal,tglakhir From st_peg_final Where idst=$idst");
			foreach ($query->result_array() as $row) {
				if (array_key_exists($row['nip'], $draft) ) {
					$draft[$row['nip']]['nip2'] 					= $row['nip'];
					$draft[$row['nip']]['nama2'] 					= $row['nama'];
					$draft[$row['nip']]['direktorat2'] 		= $row['direktorat'];
					$draft[$row['nip']]['tglawal'] 				= $row['tglawal'];
					$draft[$row['nip']]['tglakhir'] 			= $row['tglakhir'];
					if ($hasil[$idst]['jnsst']=='302'){ if (array_key_exists($row['nip'], $dat)) $draft[$row['nip']]['jmplg'] =  $dat[$row['nip']]['tglpulang']; } 
				} else {
					if (array_key_exists($row['nip'], $dat)) $plg2 = $dat[$row['nip']]['tglpulang'];
					$draft[$row['nip']] = array('nip'=>'', 'nama'=>'', 'direktorat'=>'', 'nip2'=>$row['nip'], 'nama2'=>$row['nama'], 'direktorat2'=>$row['direktorat'], 'jmplg'=>$plg2 ,'tglawal'=>$row['tglawal'], 'tglakhir'=>$row['tglakhir']);
				}
			}
			$hasil[$idst]['lamp'] = $draft;
		}

		// Tabel Eselon II untuk Select Option
		$query = $this->db->query("Select Distinct eselon2, nmso From st a Left Join t_so b On Concat(eselon2,'0000')=b.kdso Where tahun<='". date('Y') ."' Order By 1");
		$es2['00'] = array('eselon2'=>'00', 'nmso'=>'DIREKTORAT JENDERAL ANGGARAN');
		foreach ($query->result_array() as $row) $es2[ $row['eselon2'] ] = array('eselon2'=>$row['eselon2'], 'nmso'=>$row['nmso']);

		// Tabel ST jenis untuk Referensi Info Lampiran
		$invar = "And jnsst='000'"; if ($hasil)  $invar = "And jnsst ". $this->fc->Invar($hasil, 'jnsst');
		$query = $this->db->query("Select jnsst, kategori From st_jenis Where Right(jnsst,1)!='1' $invar Union Select '000', 'Semua Surat Tugas'");
		$jnsst = $this->fc->ToArr( $this->fc->array_index($query->result_array(), 'jnsst'), 'jnsst');

		$data['st']	   = $hasil;
		$data['es2']   = $es2;
		$data['jns']   = $jenis;
		$data['jnsst'] = $jnsst;
		$data['start'] = $this->fc->idtgl( $start );
		$data['end']   = $this->fc->idtgl( $end );
		return $data;
	}

	public function get_data() {
		// Pembatasan DATA ST Per Direktorat, Kecuali 002 "SUPER-USER" atau 300 "STOL-ADMIN"
		if (strpos($this->session->userdata('idusergroup'), '002') or strpos($this->session->userdata('idusergroup'), '300') ) $whr = "Where tahun>='2016'";
		else $whr = "Where eselon2='". substr($this->session->userdata('kdso'),0,2) ."'";

		// Condition untuk KATA-KATA dalam PENCARIan
		if ($this->session->userdata('cari')) {
			$whr.= " And ";
			$arr = explode(' ', $this->session->userdata('cari'));
			for ($i=0; $i<count($arr); $i++) {
				$whr .= "perihal Like '%". $arr[$i] ."%'";
				if ($i<count($arr)-1)  $whr .= " And ";
			}
		}


		// Query Data sesuai Condition $WHR
		$query = $this->db->query("Select a.*, nmso1 From st a Left Join t_so b On concat(eselon2,'0000')=b.kdso $whr Order By idst Desc");
		$hasil = $this->fc->ToArr( $query->result_array(), 'idst');
		foreach ($hasil as $key=>$value) {
			if ($value['distribusi']) {
				$file = site_url('files/stol/'.$value['idst']) .'/'. $value['distribusi']; $title = $value['distribusi'];
				$hasil[$key]['distribusi'] = "<a href='$file' data-toggle='tooltip' data-placement='bottom' title='$title'>ST</a>";
			}
			if ($value['finalisasi']) {
				$file = site_url('files/stol/'.$value['idst']) .'/'. $value['finalisasi']; $title = $value['finalisasi'];
				$hasil[$key]['finalisasi'] = "<a href='$file' data-toggle='tooltip' data-placement='bottom' title='$title'>ST</a>";
			}

		}
		return $hasil;
	}

	public function get_row( $ruh ) {
		$idst =' 99999'; if (!is_null($this->uri->segment(4))) $idst = $this->uri->segment(4);
		$query = $this->db->query("Select * From st Where idst=$idst");
		$data['table'] = $query->row_array();

		$query = $this->db->query("Select * From st_jenis Order By 1");
		$data['jnsst'] = $this->fc->ToArr( $query->result_array(), 'jnsst');

		$query = $this->db->query("Select nip, nmuser, jabatan, Left(kdso,2) kdso From t_user Where jabatan Like 'direktur%' Or jabatan Like 'sekretaris direktorat%' ");
		$data['ttdnip'] = $this->fc->ToArr( $query->result_array(), 'nip');

		$query = $this->db->query("Select Left(kdso,2) kdso, nip From t_user Where jabatan Like 'direktur%' Or jabatan Like 'sekretaris direktorat%' ");
		$data['ttdso'] = $this->fc->ToArr( $query->result_array(), 'kdso');

		$query = $this->db->query("Select nmrapat, letak From t_rapat_ruang Order By 1");
		$data['ruangrapat'] = $this->fc->ToArr( $query->result_array(), 'nmrapat');

		$data['ruh'] = $ruh;
		return $data;
	}

	public function save(  ) {
		$action		= $_POST['ruh'];
		$idst		= $_POST['idst'];
		$tahun		= date("Y");
		$tglst		= $this->fc->ustgl( $_POST['tglst'], 'hari');
		$nost		= $_POST['nost'];
		$jnsst		= $_POST['jnsst'];
		$perihal	= $_POST['perihal'];
		$kegiatan	= $_POST['kegiatan'];
		$institusi	= $_POST['institusi'];
		$tglawal	= $this->fc->ustgl( $_POST['tglawal'], 'hari');
		if($jnsst=='302') $tglakhir=$tglawal;
		else $tglakhir 	= $this->fc->ustgl( $_POST['tglakhir'], 'hari');

		$lokasi		= $_POST['lokasi'];
		$paragraf1	= $_POST['paragraf1'];
		$waktu		= $_POST['waktu'];
		if($jnsst=='302') $waktu = $_POST['waktu-rdk'];

		$tempat		= $_POST['tempat'];
		$paragraf2	= $_POST['paragraf2'];
		$ttdlokasi	= $_POST['ttdlokasi'];
		$ttdjabatan	= $_POST['ttdjabatan'];
		$ttdnama	= $_POST['ttdnama'];
		$ttdnip		= $_POST['ttdnip'];
		$tembusan	= $_POST['tembusan'];
		$eselon2	= substr($this->session->userdata('kdso'),0,2);
		$iduser		= $this->session->userdata('iduser');

		if ($action=='Rekam') {
			$this->db->query( "Insert Into st (tahun,tglst,lokasi,nost,jnsst,perihal,kegiatan,institusi,paragraf1,tglawal,tglakhir,waktu,tempat,paragraf2,ttdlokasi,ttdjabatan,ttdnama,ttdnip,tembusan,eselon2,iduser,status,tglupdate) Values ('$tahun','$tglst',\"$lokasi\",'$nost',$jnsst,\"$perihal\",\"$kegiatan\",'$institusi',\"$paragraf1\",'$tglawal','$tglakhir','$waktu',\"$tempat\",\"$paragraf2\",'$ttdlokasi','$ttdjabatan','$ttdnama','$ttdnip','$tembusan','$eselon2','$iduser','1',current_timestamp())" );
		}
		if ($action=='Ubah') {
			$this->db->query( "Update st Set tglst='$tglst', nost='$nost', perihal=\"$perihal\", kegiatan=\"$kegiatan\", institusi=\"$institusi\", lokasi=\"$lokasi\", paragraf1=\"$paragraf1\", waktu='$waktu', tempat=\"$tempat\", paragraf2=\"$paragraf2\", ttdlokasi='$ttdlokasi',tglawal='$tglawal',tglakhir='$tglakhir', ttdjabatan='$ttdjabatan', ttdnama='$ttdnama', ttdnip='$ttdnip', tembusan='$tembusan', tglupdate=current_timestamp() Where idst=$idst" );
			$this->db->query("UPDATE st_peg SET tglawal='$tglawal', tglakhir='$tglakhir' WHERE idst='$idst' ");
		}
		if ($action=='Hapus') {
			$this->db->query( "Delete From st where idst=$idst" );
		}
		return;
	}

	public function save_distribusi() {
		$file = str_replace(' ', '_', $_POST['file_st']) ;
		$idst = $_POST['idst'];
		$no   = $_POST['nost'];
		$ket  = $_POST['noag'];

		$nosur= 'ST-'.$no.$ket;
		$this->db->query("Update st Set distribusi='$file', status='2', nost='$nosur' Where idst=$idst");
		$this->db->query("Insert Into st_peg_final Select * From st_peg Where idst=$idst");
		$this->email_distribusi( $idst );
		$this->notif_distribusi( $idst );
	}

	public function save_finalisasi() {
		$file = $_POST['file_st'];
		$idst = $_POST['idst'];
		$this->db->query("Update st Set finalisasi='$file', status='3' Where idst=$idst ");
	}

	public function save_verifikasi($idst,$status,$jnsverif,$es,$sta,$end){
		// $idst     = $_POST['idst'];
		// $status   = $_POST['status'];
		// $jnsverif = $_POST['jnsverif'];

		$qry = $this->db->query("SELECT * FROM st_peg_final WHERE idst = '$idst' ");
		$arr = $qry->row_array();

		// if($arr) $tbl = 'st_peg_final'; else $tbl = 'st_peg';
		$tbl = 'st_peg_final'; 

		if ($jnsverif=='Terima') {
			$this->db->query("Update st Set status='4' Where idst=$idst ");
			$this->notif_verifikasi( $idst );
			$this->ke_milea($idst, $tbl);
			// redirect("stol/dash/$es/$jns/$sta/$end");

			// return;
		}

		if (trim($jnsverif)=='Tolak') { 
			$this->db->query("Update st Set status='2' Where idst=$idst ");
			$this->notif_verifikasi( $idst );
		}

		if ($jnsverif =='Mundur' && $status == '2') {
			$this->db->query("Update st Set status='1' Where idst=$idst ");
			$this->db->query("Delete From st_peg_final Where idst=$idst ");
		}

		if ($jnsverif =='Mundur' && $status == '3') {
			$this->db->query("Update st Set status='2' Where idst=$idst ");
		}

		if ($jnsverif =='Mundur' && $status == '4') {
			$this->db->query("Update st Set status='3' Where idst=$idst ");
		}
	}

	function ke_milea($idst, $tbl){
		$qry  = $this->db->query("SELECT nip FROM $tbl WHERE idst='$idst'");
		$peg  = array();
		foreach ($qry->result_array() as $row) array_push($peg, $row['nip']);

		$qry  = $this->db->query("SELECT a.jnsst,tglawal, tglakhir, deskripsi FROM st a LEFT JOIN st_jenis b ON a.jnsst=b.jnsst WHERE idst='$idst' ");
		$jns  = $qry->row_array();
		$this->fc->milea_absen_net($peg, $jns['tglawal'], $jns['tglakhir'], $jns['deskripsi'], $idst );

	}

	public function notif_verifikasi( $idst) {
		$sql   = "Select a.idst, b.iduser, b.nmuser, a.distribusi From st a Left Join t_user b On a.iduser=b.iduser Left Join st c On a.idst=c.idst Where a.idst=$idst";
		$query = $this->db->query( $sql );
		foreach ($query->result_array() as $row) {
			$iduser= $row['iduser']; $judul='Penolakan finalisasi Surat Tugas';
			$link  = site_url('files/stol/'.$row['idst']) .'/'. $row['distribusi'];
			$query = $this->db->query("Insert Into d_notifikasi (iduser,judul,waktu,link,status) Values ('$iduser','$judul',current_timestamp(),\"$link\",'0')");
		}
	 	return;
	}

	public function email_distribusi( $idst ) {
	 	$query = $this->db->query("Select email, nmuser From st_peg a Left Join t_user b On a.nip=b.nip Where a.idst=$idst");
	 	$hasil = $query->result_array();

		$email = array();  $attach = array();
		foreach ($hasil as $row) if ( $row['email'] ) $email[ $row['email'] ] = $row['nmuser'];

		$query = $this->db->query("Select tglst, nost, perihal, distribusi From st Where idst=$idst");
		$hasil = $query->row_array();
		$hasil['distribusi'] = str_replace('%20', ' ', $hasil['distribusi']);
		$subject = 'Surat Tugas perihal : '. $hasil['perihal'];
		$body	 = 'Anda mendapat surat tugas perihal : '. $hasil['perihal'] . ' tanggal '. $this->fc->idtgl($hasil['tglst'],'hari');
		$attach  = array( $hasil['distribusi'] => "http://10.242.142.52/files/stol/$idst/" .'/'. $hasil['distribusi'] );
		$this->fc->send_mail( 'stol', $email, $subject, $body, $attach);
	 	return $attach;
	}

	public function notif_distribusi( $idst ) {
		$sql   = "Select a.idst, a.nip, b.iduser, b.nmuser, perihal, distribusi, a.lokasi, a.tglawal, a.tglakhir From st_peg a Left Join t_user b On a.nip=b.nip Left Join st c On a.idst=c.idst Where a.idst=$idst";
		$query = $this->db->query( $sql );
		foreach ($query->result_array() as $row) {
			$iduser= $row['iduser']; $judul='Surat Tugas'; $pesan=$row['perihal'];
			// $waktu = $this->fc->idtgl($row['tglawal'],'hari');
			$link  = site_url('files/stol/'.$row['idst']) .'/'. $row['distribusi'];
			$query = $this->db->query("Insert Into d_notifikasi (iduser,judul,pesan,waktu,link,status) Values ('$iduser','$judul','$pesan',current_timestamp(),\"$link\",'0')");
		}
	 	return;
	}

	public function get_st($idst){
	 	$query=$this->db->query("select * from st where idst=$idst");
	 	return $query->row_array();
	}

	public function get_dir($es){
	 	$query=$this->db->query("Select nmso From l_so Where SubString(kdso,1,2)='$es' And nmso1!='' ");
	 	return $query->row_array();
	}

	public function get_alm1($es){
	 	$query=$this->db->query("Select header1 From l_so Where SubString(kdso,1,2)='$es' And nmso1!='' ");
	 	return $query->row_array();

	}

	public function get_alm2($es){
	 	$query=$this->db->query("Select header2 From l_so Where SubString(kdso,1,2)='$es' And nmso1!='' ");
	 	return $query->row_array();
	}

	public function get_par1($idst){
		$query=$this->db->query("Select paragraf1 From st Where idst=$idst");
		return $query->row_array();
	}
	public function get_par2($idst){
		$query=$this->db->query("Select paragraf2 From st Where idst=$idst");
		return $query->row_array();
	}

	public function get_lamp($idst,$status){
		if ($status=='1') $table='st_peg'; else $table='st_peg_final';
		$query=$this->db->query("Select a.nip, a.nama nmuser,if(b.kdeselon!='',b.kdeselon,'aa') kdeselon,concat(Left(b.kdso,4),'00') kdso , a.jabatan, pangkat,if(b.golongan!='',b.golongan,a.gol) golongan,a.direktorat direktorat ,nmso ,honor, lokasi, idstlain, tglawal, tglakhir, '' tooltip,a.direktorat,a.kdeselon eselong
				From $table a Left Join t_user b On a.nip=b.nip
				Left Join t_so d On Concat(Left(b.kdso,2),'0000')=d.kdso
				Where idst=$idst Order By kdeselon, golongan Desc, direktorat");
		return $query->result_array();
	}

	public function get_nmppk($es){
	 	$query=$this->db->query("Select nmuser From t_user Where nip In (Select nip From l_so Where SubString(kdso,1,2)='$es' And nmso1!='') ");
	 	return $query->row_array();
	}

	public function get_nipppk($es){
	 	$query=$this->db->query("Select nip From l_so Where SubString(kdso,1,2)='$es' And nmso1!='' ");
	 	return $query->row_array();
	}

	public function get_honor($idst, $status){
		if ($status=='1') $table='st_peg'; else $table='st_peg_final';
		$query=$this->db->query("Select a.nama,a.gol,a.nip,a.jabatan,a.direktorat,b.golongan,b.pangkat,c.gol,c.honor,c.pajak From $table a Left Join t_user b ON a.nip=b.nip Left Join st_honor c ON b.golongan=c.gol Where idst=$idst");
		return $query->result_array();
	}

}
