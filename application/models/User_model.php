<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class User_model extends CI_Model {
	
	public function get_data() {
		$whr = ""; 
		// if ( strlen(trim($param->persh))>0 ) { $whr .= "where kduser like '%$param->persh%' or nmuser like '%$param->persh%'"; }
		$sql = "Select *, nmso From t_user a Left Join t_so b On kdso=concat(kdes2,kdes3,kdes4) $whr order by kduser";
		$query = $this->db->query($sql);
		return $query->result();
	}

	public function get_level() {
		$query = $this->db->query("select * from t_level order by kdlevel");
		return $this->fc->ToArr($query->result_array(), 'kdlevel');
	}

	public function get_kdso() {
		$new = $this->db->query("select '00' kdso, '-- None --' nmso");
		$sql = $this->db->query("select concat(kdes2,kdes3,kdes4) kdso, nmso from t_so");
		$str = array_merge($new->result_array(), $sql->result_array());
		return $this->fc->array_index($str, 'kdso');
	}

	function save($param){
		$kduser = $_POST['kduser'];
		$pass = $_POST['passuser1']; if (trim($pass)=='') $pass = 'nochange';
		$nmuser = $_POST['nmuser'];
		$kdlevel = $_POST['kdlevel'];
		$kdso = $_POST['kdso'];
		$kdwenang = $_POST['kdwenang'];
		$nohp = $_POST['nohp'];
		$kduseredit = $this->session->userdata('kduser');

		if ($param=='Tambah') {
			$sql = 	"(kduser,passuser,nmuser,kdlevel,kdso,kdwenang,nohp,kduseredit,jnsupdate,tglupdate) values 
					('$kduser',md5('$pass'),'$nmuser','$kdlevel','$kdso','$kdwenang','$nohp','$kduseredit','R',current_timestamp())";
			$query = $this->db->query("insert into t_user $sql");
		} elseif ($param=='Ubah') {
			$sql = 	"nmuser='$nmuser',kdlevel='$kdlevel',kdwenang='$kdwenang',nohp='$nohp',
					kduseredit='$kduseredit', jnsupdate='U', tglupdate=current_timestamp()";
			if ($pass<>'nochange') $sql .= ",passuser=md5('$pass')";
			$query = $this->db->query("update t_user set $sql where kduser='$kduser'");
		} elseif ($param=='Hapus') {
			$query = $this->db->query("delete from t_user where kduser='$kduser'");
		}
		return;
	}

}
?>