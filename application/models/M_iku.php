<?php
class M_iku extends CI_Model {
	public function get_data_manual($cari) {
	$whr  = '';
		if($cari != ''){
			$whr  = "
			  AND (
				a.nipUser LIKE '%".$cari."%'
				OR UPPER(b.fullname) LIKE '%".$cari."%'
				OR a.iku LIKE '%".$cari."%'
			  ) ";
		}
		$usrgrp = explode(";", $this->session->userdata('idusergroup'));
		$filteruser = '';
		foreach($usrgrp as $usr){
			if($usr != '001' and $usr != '002' and $usr != '022' ){
				if($filteruser == ''){
					$filteruser = " AND (";
				}else{
					$filteruser .= " OR ";
				}
				$filteruser  .= "nip = '".$this->session->userdata('nip')."' ";
			}else{
				$filteruser = '';
				break;
			}
		}
			if($filteruser != ''){
				$filteruser .= ") ";
			}
		$whr .= $whr . $filteruser;
		// cari query data dari d_paket
		$SET = "SET SESSION group_concat_max_len = 100000000000;";
		$this->db->query($SET);
		$query  = $this->db->query("
			SELECT
			  (@row := @row + 1) AS NO,
			  tab.id,
			  tab.nipUser,
			  tab.fullname,
			  GROUP_CONCAT(DISTINCT(tab.sasaran_strategis) SEPARATOR '--,--') as 'sasaran_strategis',
			  /*GROUP_CONCAT(CONCAT(tab.id,';;;', tab.iku) SEPARATOR '--,--') as 'iku',*/
			  tab.dateStamp,
			  tab.rentangWaktu,
			  tab.terakhirDientry
			FROM
			  (SELECT
				a.id,
				a.nipUser,
				b.fullname,
				CONCAT(e.id,';;;', e.name) AS 'sasaran_strategis',
				a.iku,
				DATE(a.`createdTimeStamp`) AS 'dateStamp',
				c.value_2 AS 'rentangWaktu',
				d.createdTimeStamp AS 'terakhirDientry'
			  FROM
				t_iku a
				LEFT JOIN t_user b
				  ON a.nipUser = b.nip
				LEFT JOIN t_parameter c
				  ON c.id = a.periodePelaporanId
				LEFT JOIN t_parameter e
				  ON e.id = a.sasaranStrategisId
				  AND e.idMaster = '2'
				LEFT JOIN t_periode_pelaporan d
				  ON a.id = d.idIku
			   WHERE
			     a.state = 'Y'

				".$whr."
			  GROUP BY d.idIku
			  ORDER BY nipUser) tab,
			  (SELECT
				@row := 0) z
			  GROUP BY tab.nipUser");

		// masukkan ke array dengan id (group by) 'idpaket'
		$paket  = $this->fc->ToArr( $query->result_array(), 'id');

		// jika paket kosong
		if (! ($paket)) return $paket;

		// buat function kumpulan iduser dari array 'paket'
	/*	$invar1 = $this->fc->Invar( $paket, 'id');
		// $invar2 = $this->fc->Invar( $paket, 'idambil');

		// buat query iduser,nmuser dari t_user yang idusernya ada di array invar1
		$query  = $this->db->query("select iduser, nmuser from t_user where iduser $invar1");

		// masukkan ke array dengan id (group by) 'iduser'
		$t_user = $this->fc->ToArr( $query->result_array(), 'iduser');

		// masukkan nama user di array
		foreach ($paket as $row) {
			$key = $row['id'];
			if ( array_key_exists($key, $t_user) ){
				$paket[ $row['idpaket'] ]['nmterima'] = $t_user[$key]['nmuser'];
			}
		}*/
		return $paket;
	}

	public function getIku( $id, $nip ) {

		$sql = "SELECT a.id, a.iku FROM t_iku a WHERE a.nipUser = (SELECT nipUser FROM t_iku WHERE id = '".$nip." ') AND a.state = 'Y'";

		if($id != 'all'){
			$sql = $sql . " AND sasaranStrategisId = '".$id."'";}

		$query  = $this->db->query($sql);

			return $query;
	}

	public function get_data() {
		$whr  = "";
		$cari = $this->session->userdata('cari');

		if ($cari) {
			$whr = " where " ;
			$ref = " where " ;
			$arr = explode(' ', $cari);
			for ($i=0; $i<count($arr); $i++) {
				$str  = $arr[$i];
				$whr .= "keterangan like '%$str%' or nmambil like '%$str%' ";
				$ref .= "nmuser like '%$str%' ";
				if ($i<count($arr)-1) {
					$whr .= " or ";
					$ref .= " or ";
				}
			}
			$query = $this->db->query("select iduser from t_user $ref");
			$arr   = $query->result_array();
			if ($arr) {
				$whr .= " or ";
			}
			for ($i=0; $i<count($arr); $i++) {
				$str  = $arr[$i]['iduser'];
				$whr .= "idterima like '%$str%' ";
				if ($i<count($arr)-1) {
					$whr .= " or ";
				}
			}
		}

		// cari query data dari d_paket
		$query  = $this->db->query("
			SELECT
			  a.id,
			  q.fullname,
			  b.`name` 'perspektif',
			  c.`name` 'sasaran_strategis',
			  p.`fullname` 'iku_atasan',
			  a.iku,
			  a.definisi,
			  a.formula,
			  a.tujuan,
			  e.`name` 'satuan_pengukuran',
			  f.`name` 'jenis_aspek_target',
			  g.`name` 'tingkat_kendali_iku',
			  h.`name` 'tingkat_validitas_iku',
			  a.`pihakPJIku` 'pihakPJIku',
			  a.`pihakPenyediaJasa` 'pihakPenyediaJasa',
			  a.`sumberData` 'sumberData',
			  i.`name` 'jenis_cascading_iku',
			  j.`name` 'metode_cascading',
			  k.`name` 'jenis_konsolidasi_periode',
			  l.`name` 'jenis_konsolidasi_lokasi',
			  m.`name` 'polarisasi_indikator_kinerja',
			  n.`name` 'periode_pelaporan',
			  a.konversi120,
			  a.`descSasaranStrategis` 'descSasaranStrategis',
			  a.`file_document`,
			  a.`createdTimeStamp`,
			  a.`idPeriode`
			FROM
			  t_iku a
			  LEFT JOIN t_parameter b
			    ON a.`perspektifId` = b.`id`
			  LEFT JOIN t_parameter c
			    ON a.`sasaranStrategisId` = c.`id`
			  LEFT JOIN t_parameter e
			    ON a.`satuanPengukuranId` = e.`id`
			  LEFT JOIN t_parameter f
			    ON a.`jenisAspekTargetId` = f.`id`
			  LEFT JOIN t_parameter g
			    ON a.`tingkatKendaliIkuId` = g.`id`
			  LEFT JOIN t_parameter h
			    ON a.`tingkatValiditasIkuId` = h.`id`
			  LEFT JOIN t_parameter i
			    ON a.`jenisCascadingIkuId` = i.`id`
			  LEFT JOIN t_parameter j
			    ON a.`metodeCascadingId` = j.`id`
			  LEFT JOIN t_parameter k
			    ON a.`jenisKonsolidasiPeriodeId` = k.`id`
			  LEFT JOIN t_parameter l
			    ON a.`jenisKonsolidasiLokasiId` = l.`id`
			  LEFT JOIN t_parameter m
			    ON a.`polarisasiIndikatorKinerjaId` = m.`id`
			  LEFT JOIN t_parameter n
			    ON a.`periodePelaporanId` = n.`id`
			  LEFT JOIN t_iku o
			    ON a.`ikuAtasanId` = o.`id`
			  LEFT JOIN t_user p
				ON o.`nipUser` = p.`nip`
			  LEFT JOIN t_user q
				ON a.`nipUser` = q.`nip`
			WHERE a.state = 'Y'  ");

		// masukkan ke array dengan id (group by) 'idpaket'
		$paket  = $this->fc->ToArr( $query->result_array(), 'id');

		// jika paket kosong
		if (! ($paket)) return $paket;

		// buat function kumpulan iduser dari array 'paket'
	/*	$invar1 = $this->fc->Invar( $paket, 'id');
		// $invar2 = $this->fc->Invar( $paket, 'idambil');

		// buat query iduser,nmuser dari t_user yang idusernya ada di array invar1
		$query  = $this->db->query("select iduser, nmuser from t_user where iduser $invar1");

		// masukkan ke array dengan id (group by) 'iduser'
		$t_user = $this->fc->ToArr( $query->result_array(), 'iduser');

		// masukkan nama user di array
		foreach ($paket as $row) {
			$key = $row['id'];
			if ( array_key_exists($key, $t_user) ){
				$paket[ $row['idpaket'] ]['nmterima'] = $t_user[$key]['nmuser'];
			}
		}*/
		return $paket;
	}

	public function get_data_by_id($id) {

		// cari query data dari d_paket
		$query  = $this->db->query("
			SELECT
			  a.id,
			  a.nipUser,
			  q.fullname,
			  b.`name` 'perspektif',
			  c.`name` 'sasaran_strategis',
			  p.`fullname` 'iku_atasan',
			  a.iku,
			  a.definisi,
			  a.formula,
			  a.tujuan,
			  e.`name` 'satuan_pengukuran',
			  f.`name` 'jenis_aspek_target',
			  g.`name` 'tingkat_kendali_iku',
			  h.`name` 'tingkat_validitas_iku',
			  a.`pihakPJIku` 'pihakPJIku',
			  a.`pihakPenyediaJasa` 'pihakPenyediaJasa',
			  a.`sumberData` 'sumberData',
			  i.`name` 'jenis_cascading_iku',
			  j.`name` 'metode_cascading',
			  k.`name` 'jenis_konsolidasi_periode',
			  l.`name` 'jenis_konsolidasi_lokasi',
			  m.`name` 'polarisasi_indikator_kinerja',
			  a.periodePelaporanId,
			  n.`name` 'periode_pelaporan',
			  n.`value_2` 'rentang',
			  n.`value_3` 'alias',
			  a.konversi120,
			  a.`descSasaranStrategis` 'descSasaranStrategis',
			  a.`file_document`,
			  a.`createdTimeStamp`,
			  a.`createdBy`,
			  a.`updatedTimeStamp`,
			  a.`updatedBy`,
			  a.`idPeriode`
			FROM
			  t_iku a
			  LEFT JOIN t_parameter b
			    ON a.`perspektifId` = b.`id`
			  LEFT JOIN t_parameter c
			    ON a.`sasaranStrategisId` = c.`id`
			  LEFT JOIN t_parameter e
			    ON a.`satuanPengukuranId` = e.`id`
			  LEFT JOIN t_parameter f
			    ON a.`jenisAspekTargetId` = f.`id`
			  LEFT JOIN t_parameter g
			    ON a.`tingkatKendaliIkuId` = g.`id`
			  LEFT JOIN t_parameter h
			    ON a.`tingkatValiditasIkuId` = h.`id`
			  LEFT JOIN t_parameter i
			    ON a.`jenisCascadingIkuId` = i.`id`
			  LEFT JOIN t_parameter j
			    ON a.`metodeCascadingId` = j.`id`
			  LEFT JOIN t_parameter k
			    ON a.`jenisKonsolidasiPeriodeId` = k.`id`
			  LEFT JOIN t_parameter l
			    ON a.`jenisKonsolidasiLokasiId` = l.`id`
			  LEFT JOIN t_parameter m
			    ON a.`polarisasiIndikatorKinerjaId` = m.`id`
			  LEFT JOIN t_parameter n
			    ON a.`periodePelaporanId` = n.`id`
			  LEFT JOIN t_iku o
			    ON a.`ikuAtasanId` = o.`id`
			  LEFT JOIN t_user p
				ON o.`nipUser` = p.`nip`
			  LEFT JOIN t_user q
				ON a.`nipUser` = q.`nip`
			WHERE a.state = 'Y'
			AND a.id = '".$id."'");


		return $query;
	}

	public function master( $data ) {

		// cari query data dari d_paket
		$query  = $this->db->query("
			SELECT a.id, a.name, a.value_1
			FROM t_parameter a
			LEFT JOIN t_master b
			ON a.`idMaster` = b.`id`
			WHERE b.name = '".$data."'
			AND b.`state` = 'Y'
			AND a.`state` = 'Y'
			ORDER BY a.`sequence`");

			return $query;
	}

	public function ikuId() {

		// cari query data dari d_paket
		$query  = $this->db->query("
			SELECT t_iku.id as \"id\", t_user.`fullname`, iku AS \"name\"
			FROM t_iku
			INNER JOIN t_user
			ON t_iku.`nipUser` = t_user.`nip`
			WHERE t_iku.state = 'Y' AND t_iku.iku != ''
			ORDER BY t_user.`fullname`");

			return $query;
	}

	public function user() {
		$query  = $this->db->query("SELECT nip, fullname FROM t_user WHERE kdpeg = 1 ORDER BY fullname");
		return $query;
	}

	public function select_seq_master($type) {
		$query  = $this->db->query("SELECT MAX(sequence) + 1 as 'seq' FROM t_parameter WHERE idMaster = '".$type."'");
		return $query;
	}

		// public function select_id_periode() {
		// 	$query  = $this->db->query("SELECT MAX(idTable) + 1 as 'id' FROM t_periode_pelaporan");
		// 	return $query;
		// }
	public function insert_id_periode($id) {
		$query  = $this->db->query("INSERT INTO t_periode_pelaporan(idTable, createdBy) VALUES('".$id."', '".$this->session->userdata('iduser')."')");
		return $query;
	}
	// membuat function 'save' untuk penyimpanan
	public function save($datas) {
		// jika reakam maka melakukan ini.......
		if ($datas['action']=='Rekam') {
			$tahun = date("Y");
			$sql = 	"INSERT INTO `t_iku` (
					  `nipUser`, `perspektifId`,`sasaranStrategisId`,`descSasaranStrategis`,
					  `ikuAtasanId`, `iku`,`definisi`,`formula`,
					  `tujuan`,`satuanPengukuranId`,`jenisAspekTargetId`,
					  `tingkatKendaliIkuId`,`tingkatValiditasIkuId`,`pihakPJIku`,
					  `pihakPenyediaJasa`,`sumberData`,`jenisCascadingIkuId`,
					  `metodeCascadingId`,`jenisKonsolidasiPeriodeId`,`jenisKonsolidasiLokasiId`,
					  `polarisasiIndikatorKinerjaId`,`periodePelaporanId`,`konversi120`, `idPeriode`, `file_document`,
					  `createdTimeStamp`,`createdBy`
					)
					VALUES
					  (
					    \"".$datas['userForm']."\", \"".$datas['perspektifForm']."\",\"".$datas['sasaranStrategisForm']."\",\"".$datas['descSasaranStrategisForm']."\",
					     \"".$datas['IKUAtasanForm']."\", \"".$datas['IKUForm']."\",\"".$datas['definisiForm']."\",\"".$datas['formulaForm']."\",
					    \"".$datas['tujuanForm']."\",\"".$datas['satuanPengukuranForm']."\",\"".$datas['jenisAspekTargetForm']."\",
					    \"".$datas['tingkatKendaliIKUForm']."\",\"".$datas['tingkatValiditasIKUForm']."\",\"".$datas['pihakPJIKUForm']."\",
					    \"".$datas['pihakPenyediaJasaForm']."\",\"".$datas['sumberDataForm']."\",\"".$datas['jenisCascadingIKUForm']."\",
					    \"".$datas['metodeCascadingForm']."\", \"".$datas['jenisKonsolidasiPeriodeIdForm']."\",\"".$datas['jenisKonsolidasiLokasiForm']."\",
					    \"".$datas['polarisasiIndikatorKinerjaForm']."\",\"".$datas['periodePelaporanForm']."\",\"".$datas['konversi120Form']."\",\"".$datas['idPeriode']."\",\"".$datas['file_document']."\",
					    \"".$datas['createdTimeStamp']."\",\"".$this->session->userdata('iduser')."\"
					  ) ;

					";
		}
		// jika ubah paket melakukan ini.......
		if ($datas['action']=='Ubah') {

			$sql = 	"UPDATE
					  `t_iku`
					SET
					  `nipUser` = \"".$datas['userForm']."\",
					  `perspektifId` = \"".$datas['perspektifForm']."\",
					  `sasaranStrategisId` = \"".$datas['sasaranStrategisForm']."\",
					  `descSasaranStrategis` = \"".$datas['descSasaranStrategisForm']."\",
					  `ikuAtasanId` = \"".$datas['IKUAtasanForm']."\",
					  `iku` = \"".$datas['IKUForm']."\",
					  `definisi` = \"".$datas['definisiForm']."\",
					  `formula` = \"".$datas['formulaForm']."\",
					  `tujuan` = \"".$datas['tujuanForm']."\",
					  `satuanPengukuranId` = \"".$datas['satuanPengukuranForm']."\",
					  `jenisAspekTargetId` = \"".$datas['jenisAspekTargetForm']."\",
					  `tingkatKendaliIkuId` = \"".$datas['tingkatKendaliIKUForm']."\",
					  `tingkatValiditasIkuId` = \"".$datas['tingkatValiditasIKUForm']."\",
					  `pihakPJIku` = \"".$datas['pihakPJIKUForm']."\",
					  `pihakPenyediaJasa` = \"".$datas['pihakPenyediaJasaForm']."\",
					  `sumberData` = \"".$datas['sumberDataForm']."\",
					  `jenisCascadingIkuId` = \"".$datas['jenisCascadingIKUForm']."\",
					  `metodeCascadingId` = \"".$datas['metodeCascadingForm']."\",
					  `jenisKonsolidasiPeriodeId` = \"".$datas['jenisKonsolidasiPeriodeIdForm']."\",
					  `jenisKonsolidasiLokasiId` = \"".$datas['jenisKonsolidasiLokasiForm']."\",
					  `polarisasiIndikatorKinerjaId` = \"".$datas['polarisasiIndikatorKinerjaForm']."\",
					  `periodePelaporanId` = \"".$datas['periodePelaporanForm']."\" ,
					  `konversi120` = \"".$datas['konversi120Form']."\",
					  `file_document` = \"".$datas['file_document']."\",
					  `updatedTimeStamp` = CURRENT_TIMESTAMP(),
					  `updatedBy` = \"".$this->session->userdata('iduser')."\"
					WHERE `id` = \"".$datas['idForm']."\" ";



		}
		// jika hapus data melakukan ini........
		if ($datas['action']=='Hapus') {
			$sql = "update t_iku set state='N', deletedBy='".$this->session->userdata('iduser')."',
			deletedTimeStamp = CURRENT_TIMESTAMP() where id='".$datas['idForm']."'";
		}

		// jalankan query-nya
		$query = $this->db->query( $sql );
		return;
	}


	// membuat function 'save' untuk penyimpanan
	public function save_master($datas) {
		// jika reakam maka melakukan ini.......
		if ($datas['action']=='Rekam') {
			$tahun = date("Y");
			$sql = 	"INSERT INTO `t_parameter` (
					  `idMaster`, `name`, `value_1`, `sequence`, `createdBy`
					)
					VALUES
					  (
					    \"".$datas['type']."\",\"".$datas['name']."\",\"".$tahun."\",
					    \"".$datas['sequence']."\",\"".$this->session->userdata('iduser')."\"
					  ) ;

					";
		}
		// jika ubah paket melakukan ini.......
		if ($datas['action']=='Ubah') {
			$sql = 	"UPDATE
					  `t_parameter`
					SET
					  `name` = \"".$datas['name']."\"
					WHERE `idMaster` = \"".$datas['type']."\"
					AND `id` = \"".$datas['idForm']."\"";
		}
		// jika hapus data melakukan ini........
		if ($datas['action']=='Hapus') {
			$sql = "update t_parameter SET
					  `state` = \"N\"
					WHERE `id` = \"".$datas['idForm']."\"";
		}

		// jalankan query-nya
		$query = $this->db->query( $sql );
		return;
	}

	// untuk tampilan autocomplete user
	function json_user(){
		$query = $this->db->query("select iduser, nmuser from t_user order by nmuser");
		$result = $query->result();
		if(count($result)>0){
			$json = "[";
			foreach ($result as $row)
				$json .= '{ value: "'. trim($row->nmuser) .'", data: "'. trim($row->iduser) .'" },';
			$json .= "]";
		}
		return $json;
	}

	public function get_total() {
		$tahun = date("Y");
		$query  = $this->db->query("select $tahun tahun, count(*) total, sum(if(trim(nmambil)='', 1, 0)) baru from d_paket where tahun=$tahun");
		return $query->row_array();
	}

	public function get_data_dash( $array ) {
		foreach ($array as $key=>$value) {
			if ( trim($value['nmambil'])!='' ) unset( $array[$key] );
		}
		return $array;
	}



	public function periode_table( $id ) {
		$query  =
		$this->db->query("
			SELECT a.idTable, a.periode, a.tahun, a.tipe, a.value
			FROM t_periode_pelaporan a
			WHERE status = 'Y'
			AND idTable = '".$id."'");

		return $query;
	}

	public function get_data_periode($id, $type) {
		$query  = $this->db->query("SELECT ".$type." FROM t_periode_pelaporan WHERE idTable = '".$id."'");
		return $query;
	}

	public function update_tabel_periode($id, $type, $value, $where) {
		$query  = $this->db->query("UPDATE t_periode_pelaporan SET ".$type." = '".$value."' WHERE ".$where." AND idTable = '".$id."'");
		return $query;
	}

	public function seek($keyword, $type){
		$keyword = strtoupper($keyword);
		$type_alias = $type.' AS `data` ';
        $this->db->select($type_alias )->from('t_iku');
 		$this->db->group_by('tujuan');
        $this->db->like('UPPER('.$type.')',$keyword);
        $this->db->where('state', 'Y');
        $this->db->where($type.' !=', '');
        $query = $this->db->get();

        return $query->result();
	}

	public function get_rows($id){
		return	$this->db->query("
			SELECT *
			FROM t_iku
			WHERE state = 'Y'
			AND id = '".$id."'");
	}


     //--------------------------------------------PERIODE PELAPORAN------------------------------------------------//
     //-------------------------------------------------------------------------------------------------------------//

	public function rentang_periode($id){
		return	$this->db->query("
			SELECT a.periodePelaporanId, b.value_2 as \"rentang\", b.name, b.value_3 as \"alias\"
			FROM t_iku a
			LEFT JOIN t_parameter b
			ON a.periodePelaporanId = b.id
			WHERE a.id = '$id'");
	}

	public function periode_pelaporan($id, $periode){
		return	$this->db->query("
			SELECT tahun, tipe, sequence, VALUE
			FROM t_periode_pelaporan
			WHERE idIku = '$id' AND periodePelaporanId = '$periode' AND status = 'Y'");
	}

	public function total_tahun_periode_pelaporan($id, $periode){
		return	$this->db->query("
			SELECT tahun, tipe, sequence, VALUE
			FROM t_periode_pelaporan
			WHERE idIku = '$id' AND periodePelaporanId = '$periode' AND status = 'Y' GROUP BY tahun");
	}

	public function save_periode_pelaporan($id, $periodePelaporan, $tahun, $datas){
		$this->db->query("
			DELETE FROM t_periode_pelaporan
			WHERE idIku = '$id' AND periodePelaporanId = '$periodePelaporan' AND tahun = '$tahun'");

		foreach($datas as $key1 => $value1){
			//foreach ($value1 as $key2 => $value2) {
			$seq = $key1;
				$this->db->query("
					INSERT INTO t_periode_pelaporan(idIku, periodePelaporanId, tahun, tipe, sequence, value, createdTimeStamp, createdBy) VALUES('$id', '$periodePelaporan', '$tahun', 'TARGET', '$seq', '".$value1->TARGET."', CURRENT_TIMESTAMP(), '".$this->session->userdata('iduser')."')");
				$this->db->query("
					INSERT INTO t_periode_pelaporan(idIku, periodePelaporanId, tahun, tipe, sequence, value, createdTimeStamp, createdBy) VALUES('$id', '$periodePelaporan', '$tahun', 'REALISASI', '$seq', '".$value1->REALISASI."', CURRENT_TIMESTAMP(), '".$this->session->userdata('iduser')."')");
			//}
		}
	}


	//---------------------------------------



	public function getByAll($datas){

		if($datas['action'] == 'Ubah'){
			$waktu = 'updatedTimeStamp';
		}elseif($datas['action'] == 'Rekam'){
			$waktu = 'createdTimeStamp';
		}

		return	$this->db->query("
			SELECT id
			FROM t_iku
			WHERE
				nipUser = '".$datas['userForm']."' AND
				perspektifId = '".$datas['perspektifForm']."' AND
				sasaranStrategisId = '".$datas['sasaranStrategisForm']."' AND
				descSasaranStrategis = '".$datas['descSasaranStrategisForm']."' AND
				IKUAtasanId = '".$datas['IKUAtasanForm']."' AND
				iku = '".$datas['IKUForm']."' AND
				definisi = '".$datas['definisiForm']."' AND
				formula = '".$datas['formulaForm']."' AND
				tujuan = '".$datas['tujuanForm']."' AND
				satuanPengukuranId = '".$datas['satuanPengukuranForm']."' AND
				jenisAspekTargetId = '".$datas['jenisAspekTargetForm']."' AND
				tingkatKendaliIkuId = '".$datas['tingkatKendaliIKUForm']."' AND
				pihakPJIku = '".$datas['pihakPJIKUForm']."' AND
				pihakPenyediaJasa = '".$datas['pihakPenyediaJasaForm']."' AND
				sumberData = '".$datas['sumberDataForm']."' AND
				jenisCascadingIkuId = '".$datas['jenisCascadingIKUForm']."' AND
				metodeCascadingId = '".$datas['metodeCascadingForm']."' AND
				jenisKonsolidasiPeriodeId = '".$datas['jenisKonsolidasiPeriodeIdForm']."' AND
				jenisKonsolidasiLokasiId = '".$datas['jenisKonsolidasiLokasiForm']."' AND
				polarisasiIndikatorKinerjaId = '".$datas['polarisasiIndikatorKinerjaForm']."' AND
				periodePelaporanId = '".$datas['periodePelaporanForm']."' AND
				konversi120 = '".$datas['konversi120Form']."' AND
				".$waktu." = '".$datas['createdTimeStamp']."' AND
				state = 'Y'
			LIMIT 0,1");
	}


	public function getPeriode($id){
		return	$this->db->query("
			SELECT value_2, value_3
			FROM t_parameter
			WHERE state = 'Y'
			AND id = '".$id."'");
	}

	public function savePeriode($id, $periode, $tahun, $seq, $val, $tipe){
		return	$this->db->query("
			INSERT INTO t_periode_pelaporan(idIku, periodePelaporanId, tahun, tipe, sequence, value, createdTimeStamp, createdBy)
			VALUES
			('".$id."', '".$periode."', '".$tahun."', '".$tipe."', '".$seq."', '".$val."', CURRENT_TIMESTAMP, '".$this->session->userdata('id')."')");
	}

	public function deletePeriode($id){
		return	$this->db->query("
			DELETE FROM t_periode_pelaporan WHERE idIku = '".$id."'");
	}

	public function delete($id){
		return	$this->db->query("
			UPDATE  t_iku SET state = 'N' WHERE id = '".$id."'");
	}
	public function getAtasan($id){
		return	$this->db->query("
			SELECT *
			FROM t_iku
			WHERE id = '".$id."'");
	}
	public function print_iku_select($id){
		return	$this->db->query("
			SELECT
				a.id, b.name as 'ss', a.iku
			FROM
				t_iku a LEFT JOIN t_parameter b
			ON
				a.`sasaranStrategisId` = b.id
			WHERE
				a.nipUser = (SELECT nipUser FROM t_iku WHERE id = '".$id."')");
	}
}
