<?php
class Tracking_model extends CI_Model {

	private $dbrevisi;

		public function __construct()
		{
		parent::__construct();
			$this->dbrevisi = $this->load->database('revisi'. $this->session->userdata('thang'), TRUE);
			$this->dbdown   = $this->load->database('down'. $this->session->userdata('thang'), TRUE);
		}

	function get_tracking( $nil ) {
		if (strlen($nil) == 32) $whr = "qrcode = '$nil'"; else $whr = "rev_id = '$nil'";

		$dbrev = $this->load->database('default', TRUE);
		$query = $dbrev->query("Select rev_tahun, rev_id, rev_tahap, rev_status, kl_dept, '' nmdept, concat(kl_dept,'.',kl_unit) kl_unit, '' nmunit, kl_surat_no, kl_surat_tgl, kl_surat_hal, kl_pjb_jab,
			pus_status, pus_tgl, qrcode, 
			t2_status, t2_proses_tgl, t2_selesai_tgl,
			t3_status, t3_proses_tgl, t3_selesai_tgl,
			t4_status, t4_proses_tgl, t4_selesai_tgl,
			t5_status, t5_proses_tgl, t5_selesai_tgl,
			t6_status, t6_proses_tgl, t6_selesai_tgl,
			t7_status, t7_proses_tgl, t7_selesai_tgl
			From revisi Where $whr");
		$hasil = $query->row_array();
		if (! $hasil) return $hasil;

		$dbref = $this->load->database('ref', TRUE);
		$query = $dbref->query("Select kddept, nmdept From t_dept Where kddept='". $hasil['kl_dept'] ."'" );
		$dept  = $this->fc->ToArr( $query->result_array(), 'kddept');

		$query = $dbref->query("Select concat(kddept,'.',kdunit) kdunit, nmunit From t_unit Where concat(kddept,'.',kdunit)='". $hasil['kl_unit'] ."'");
		$unit  = $this->fc->ToArr( $query->result_array(), 'kdunit');

		if (array_key_exists($hasil['kl_dept'], $dept)) $hasil['nmdept'] = $dept[ $hasil['kl_dept'] ]['nmdept'];
		if (array_key_exists($hasil['kl_unit'], $unit)) $hasil['nmunit'] = $unit[ $hasil['kl_unit'] ]['nmunit'];
		return $hasil;
	}

	function qr_logo( $rev_id ) {
		require_once APPPATH."/third_party/phpqrcode/qrlib.php";
		require_once APPPATH."/third_party/phpqrcode/qrconfig.php";

		$logopath = site_url("files/images/depkeu_round.png");
		$filepath = "files/revisi/$rev_id/qrcode_$rev_id.png";
		//$codelink = "http://intranet.anggaran.depkeu.go.id/tracking/revisi";
		$codelink = "http://rkakldipa.anggaran.depkeu.go.id/index.php/tracking/revisi";
		$qrcode   = md5("$codelink/$rev_id");

		$dbrev = $this->load->database('default', TRUE);
		$query = $dbrev->query("Update revisi Set qrcode='$qrcode' Where rev_id='$rev_id'");

		QRcode::png("$codelink/$qrcode", $filepath, "L", 10);

		// // Start DRAWING LOGO IN QRCODE
		// $QR = imagecreatefrompng($filepath);
		// $logo = imagecreatefromstring(file_get_contents($logopath));

		// // Fix for the transparent background
		// imagecolortransparent($logo , imagecolorallocatealpha($logo, 0, 0, 0, 127));
		// imagealphablending($logo , false);
		// imagesavealpha($logo , true);

		// $QR_width = imagesx($QR);
		// $QR_height = imagesy($QR);

		// $logo_width = imagesx($logo);
		// $logo_height = imagesy($logo);

		// // Scale logo to fit in the QR Code
		// $logo_qr_width = $QR_width/5;
		// $scale = $logo_width/$logo_qr_width;
		// $logo_qr_height = $logo_height/$scale;

		// imagecopyresampled($QR, $logo, $QR_width/5, $QR_height/5, 0, 0, $logo_qr_width, $logo_qr_height, $logo_width, $logo_height);
		// imagepng($QR, $filepath);

		// echo '<img src="http://intranet.anggaran.depkeu.go.id/'.$filepath.'" />';
	}

	function create_report($start, $end) {
		$range = "t7_selesai_tgl Between '$start' And '$end' ";
		$query = $this->dbrevisi->query("Select Concat(Left(rev_kdso,2),'0000') kdso, kl_dept kddept, rev_id, t6_selesai_tgl, '' t6_hari, t7_selesai_tgl, '' t7_hari, 0 lama, rev_status From revisi Where rev_upload_level ='1' And $range And rev_tahap='7'");
		$hasil = $this->fc->ToArr($query->result_array(), 'rev_id');
		foreach ($hasil as $key=>$row) {
			$lama = ceil((StrToTime($row['t7_selesai_tgl']) - StrToTime($row['t6_selesai_tgl'])) / (60 * 60 * 24));

			$day = date('D', StrToTime($row['t6_selesai_tgl']));
			if ($day == 'Fri' And $lama >= 3) $lama -= 2;
			if ($day == 'Thu' And $lama >= 4) $lama -= 2;
			if ($day == 'Wed' And $lama >= 5) $lama -= 2;
			if ($day == 'Tue' And $lama >= 6) $lama -= 2;
			if ($day == 'Mon' And $lama >= 7) $lama -= 2;

			// echo "$lama <br>";
			$hasil[$key]['lama'] = $lama;
			$hasil[$key]['t6_hari'] = date('D', StrToTime($row['t6_selesai_tgl']));
			$hasil[$key]['t7_hari'] = date('D', StrToTime($row['t7_selesai_tgl']));
		}

		$query = $this->db->query("Select kdso, nmso, 0 jml, 0 h1, 0 h2, 0 h3, 0 h4, 0 jml4, 0 h5, 0 h6, 0 h7, 0 h8, 0 jml8 From t_so Where kdso In ('030000','040000','050000')");
		$rekap = $this->fc->ToArr($query->result_array(), 'kdso');
		$rekap['total'] = array('kdso'=>'Total', 'nmso'=>'Total', 'jml'=>0, 'h1'=>0, 'h2'=>0, 'h3'=>0, 'h4'=>0, 'jml4'=>0, 'h5'=>0, 'h6'=>0, 'h7'=>0, 'h8'=>0, 'jml8'=>0);

		foreach ($hasil as $key=>$row) {
			$kddit = $row['kdso'];
			$rekap[$kddit]['jml']++; $rekap['total']['jml']++;

			if ($row['lama'] == 1) 		{ $rekap[$kddit]['h1']++; $rekap[$kddit]['jml4']++; $rekap['total']['h1']++; $rekap['total']['jml4']++; }
			else if ($row['lama'] == 2) { $rekap[$kddit]['h2']++; $rekap[$kddit]['jml4']++; $rekap['total']['h2']++; $rekap['total']['jml4']++; }
			else if ($row['lama'] == 3) { $rekap[$kddit]['h3']++; $rekap[$kddit]['jml4']++; $rekap['total']['h3']++; $rekap['total']['jml4']++; }
			else if ($row['lama'] == 4) { $rekap[$kddit]['h4']++; $rekap[$kddit]['jml4']++; $rekap['total']['h4']++; $rekap['total']['jml4']++; }
			else if ($row['lama'] == 5) { $rekap[$kddit]['h5']++; $rekap['total']['h5']++; }
			else if ($row['lama'] == 6) { $rekap[$kddit]['h6']++; $rekap[$kddit]['jml8']++; $rekap['total']['h6']++; $rekap['total']['jml8']++; }
			else if ($row['lama'] == 7) { $rekap[$kddit]['h7']++; $rekap[$kddit]['jml8']++; $rekap['total']['h7']++; $rekap['total']['jml8']++; }
			else 						{ $rekap[$kddit]['h8']++; $rekap[$kddit]['jml8']++; $rekap['total']['h8']++; $rekap['total']['jml8']++; }
		}

		$data['rekap'] = $rekap;
		$data['start'] = $this->fc->idtgl( $start );
		$data['end']   = $this->fc->idtgl( $end );
		return $data;
	}

	function difrev(){
		$qry 	= $this->dbdown->query("SELECT concat(kddept,kdunit,kdsatker,IF( LENGTH(revisike) < 2 , concat(0,revisike) , revisike) )  as kdkey,kddept,kdunit,kdsatker,thang,revisike,tgl_jam,statusrev FROM d_adkpdf2021 ");
		$dat 	= $qry->result_array();
		foreach($dat as $key => $row){
			if(strlen($row['revisike']) == '1') $dat[$key]['revisike'] = '0'.$row['revisike'];
		}
		$res = $this->fc->ToArr($dat,'kdkey');
		// echo '<pre>';print_r($res);exit;
		$data   = array();

		$qry 	= $this->dbrevisi->query("SELECT concat(kddept,kdunit,kdsatker,revisike) as kdkey,kddept,kdunit,kdsatker,thang,revisike,statusrev FROM v_adk_dipa ");
		$rev 	= $this->fc->ToArr($qry->result_array(),'kdkey');

		$qry 	= $this->dbrevisi->query("SELECT concat(kddept,kdunit,kdsatker) kdkey,kddept,kdunit,kdsatker,MAX(revisike) revisike FROM v_adk_dipa GROUP BY 2,3,4 ");
		$max 	= $this->fc->ToArr($qry->result_array(),'kdkey');


		foreach($res as $key => $row){
			if(!array_key_exists($row['kdkey'],$rev)) {
				if(array_key_exists($row['kddept'].$row['kdunit'].$row['kdsatker'],$max)) {
					$ins = $max[$row['kddept'].$row['kdunit'].$row['kdsatker']]['revisike'];
				}else $ins = '-';
				$dat = [
					'thang' => $row['thang'],
					'kddept'=> $row['kddept'],
					'kdunit'=> $row['kdunit'],
					'kdsatker'=> $row['kdsatker'],
					'revisike_span' => $row['revisike'],
					'tgl_span' => $row['tgl_jam'],
					'revisike_1dja' => $ins
					// 'revisike_1dja' => '-'


				];
				array_push($data,$dat);
			}
		}
		// echo '<pre>';print_r($data); a
		// exit;
		return $data;
	}
}
