<?php
class Rapat_model extends CI_Model {

	public function get_data( $date=null ) {
		if ($date==null) { $whr=''; } else { $whr = "Where tglawal Like '$date %'"; }
		$query = $this->db->query("Select a.*, Date_Format(tglawal, '%Y-%m-%d') tgl, Date_Format(tglawal,'%H:%i') jam, nmrapat, letak, nmso1 From d_rapat a left join t_rapat_ruang b On a.kdrapat=b.kdrapat left join t_so c On concat(left(a.kdso,2),'0000')=c.kdso $whr Order By tgl Desc, jam, kdrapat");
		$hasil = $query->result_array();
		return $hasil;
	}

	public function get_dash( $date=null ) {
		if ($date==null) { $whr="Where tayang='1'"; } else { $whr = "Where tayang = '1' and tglawal Like '$date %'"; }
		$query = $this->db->query("Select a.*, Date_Format(tglawal, '%Y-%m-%d') tgl, Date_Format(tglawal,'%H:%i') jam, nmrapat, gambar, letak, nmso1 From d_rapat a left join t_rapat_ruang b On a.kdrapat=b.kdrapat left join t_so c On concat(left(a.kdso,2),'0000')=c.kdso $whr Order By tgl Desc, jam, kdrapat");
		$hasil = $query->result_array();
		return $hasil;
	}

	public function get_dashboard( $range=null ){
		if ( $range==null ) $range=''; $whr='';
		// print_r( $this->session->userdata() );exit();
		$kdso = $this->session->userdata('kdso');

		if ( $this->session->userdata('kdso')=='010000' || $this->session->userdata('idusergroup')=='100;002') { $whr=''; } else { $whr = "And a.kdso=$kdso"; }

		$query = $this->db->query("Select date(tglawal) tgl, count(*) jml, sum(if(jnsrapat='1',1,0)) rapat, sum(if(jnsrapat='2',1,0)) rdk, sum(if(jnsrapat='3',1,0)) khusus,sum(jmlpeserta) jumlah,sum(snack_p) snack_p,sum(snack_s) snack_s,sum(makan) makan,'' detail From d_rapat WHERE $range Group By 1 Order By tgl Asc");
		$hasil = $this->fc->ToArr( $query->result_array(), 'tgl');

		foreach ($hasil as $key=>$row) {
			$query = $this->db->query("Select a.*,time_format(tglawal,'%H:%i') tgl, a.kdso, b.nmrapat,c.nmso From d_rapat a Left Join t_rapat_ruang b On a.kdrapat=b.kdrapat Left Join t_so c On a.kdso=c.kdso Where date(tglawal)='$key' $whr Order By tglawal");
			$detail= $query->result_array();
			$hasil[$key]['detail'] = $detail;

		}
		// $data['rpt']	= $hasil;
		return $hasil;
	}

	public function get_dash_rapat( $date=null ) {
		if ($date==null) { $whr=''; } else { $whr = "Where tglawal Like '$date %'"; }
		$query = $this->db->query("Select a.*, Date_Format(tglawal, '%Y-%m-%d') tgl, Date_Format(tglawal,'%H:%i') jam, nmrapat, gambar, letak From d_rapat a left join t_rapat_ruang b On a.kdrapat=b.kdrapat $whr Order By tgl Desc, jam");
		$hasil = $query->result_array();
		return $hasil;
	}

	public function get_ruang( $jnsrapat, $tglawal, $tglakhir, $jamawal ) {
		if ($jnsrapat==3) {
			$tglXA = $this->fc->ustgl( $tglawal, 'hari' );
			$tglXZ = $this->fc->ustgl( $tglakhir,'hari' );

			$query = $this->db->query("Select kdrapat From d_rapat Where Date_Format(tglawal, '%Y-%m-%d') Between '$tglXA' And '$tglXZ'");
			$ruang = $this->fc->ToArr( $query->result_array(), 'kdrapat');
			if ($ruang) { $whr = "Where kdrapat Not ". $this->fc->InVar($ruang, 'kdrapat'); } else { $whr=""; }

			$query = $this->db->query("Select * From t_rapat_ruang $whr Order By kdrapat");
			$hasil = $query->result_array();

		} else {
			$tglX = $this->fc->ustgl( $tglawal, 'hari' );
			$tglXA = date("Y-m-d H:i:s", strtotime('-179 minutes', strtotime($tglX.' '.$jamawal.':00')) );
			$tglXZ = date("Y-m-d H:i:s", strtotime('+179 minutes', strtotime($tglX.' '.$jamawal.':00')) );

			$query = $this->db->query("Select kdrapat From d_rapat Where jnsrapat=3 And '$tglXA' Between Date_Format(tglawal, '%Y-%m-%d') And Date_Format(tglakhir, '%Y-%m-%d')");
			$inap  = $this->fc->ToArr( $query->result_array(), 'kdrapat');

			$query = $this->db->query("Select kdrapat From d_rapat Where tglawal Between '$tglXA' And '$tglXZ'");
			$rapat = $this->fc->ToArr( $query->result_array(), 'kdrapat');

			$ruang = array_merge($inap, $rapat);
			if ($ruang) {
				$whr = "Where kdrapat Not In (";
				for ($i=0; $i<count($ruang); $i++) { $whr .= "'".$ruang[$i]['kdrapat'] ."'"; if ($i<count($ruang)-1) $whr .= ","; else $whr .= ")"; }
			} else { $whr="Where kdrapat > '0'"; }

			$query = $this->db->query("Select * From t_rapat_ruang $whr And kdaktif='1' Order By kdrapat");
			$hasil = $query->result_array();
		}
		return $hasil;
	}

	public function get_row( $idrapat=null ) {
		if ( is_null($idrapat) ) $idrapat='999999';
		$hasil = $this->db->query("SELECT a.*, nmrapat FROM d_rapat a LEFT JOIN t_rapat_ruang b ON a.kdrapat=b.kdrapat WHERE idrapat=$idrapat")->row_array();
		
		if (! $hasil) {
			$hasil = $this->db->query("SELECT a.*, nmrapat FROM d_rapat a LEFT JOIN t_rapat_ruang b ON a.kdrapat=b.kdrapat LIMIT 1")->row_array();
			foreach ($hasil as $k=>$v) 
				$hasil[$k] = '';
		}
		return $hasil;
	}

	public function save() {
		// SETTING NILAI - RUANG RAPAT ONLINE
		$Jam_Awal_Istirahat  = 12;				// Artinya jam 11:30 dg hitungan 11 + 30/60
		$Jam_Akhir_Istirahat = 13;				// Artinya jam 13:00

		// BATAS LIMIT BOOKING RUANG RAPAT
		$tgl_ruh	  = $this->fc->ustgl( $_POST['tglawal'], 'hari');
		// $dt = new DateTime("now", new DateTimeZone('Asia/Jakarta'));
		// date_default_timezone_set("Asia/Jakarta");
		// echo date("H");exit();

		// echo $dt->format('m/d/Y, H:i:s');exit();
		// echo date("h");exit();
		// $a = 
		// echo $a;exit();
		// echo date('H');exit();

		$Selisih_hari = (strtotime($tgl_ruh) - strtotime(date('Y-m-d'))) / (60*60*24);
		if (date('l',strtotime($tgl_ruh))=='Monday') $Selisih_hari = $Selisih_hari - 2;		// Hari SENIN dan JUM'AT, selisih 3 Hari - 2 jadi 1 Hari
		// if (date("H")>17 and $Selisih_hari<=1) return;

		// Gak bisa RUH untuk HARI INI dan KEMARIN
		if ($tgl_ruh <= date('Y-m-d')) return;

		$action	   = $_POST['ruh'];
		$kdedit	   = $_POST['kdedit'];
		$kdaktif   = 0;
		$iduser	   = $this->session->userdata('iduser');
		$kdso	   = $this->session->userdata('kdso');
		$idrapat   = $_POST['idrapat'];
		$pic	   = $_POST['pic'];
		$agenda	   = $_POST['agenda'];
		$jmlpeserta= $_POST['jmlpeserta'];
		$keterangan= $_POST['keterangan'];

		if ($action=='Rekam' or $action=='Ubah') {
			$kdaktif   = 0;
			$jnsrapat  = $_POST['jnsrapat'];
			$nound	   = $_POST['nound'];
			$jamawal   = str_replace('.', ':', $_POST['jamawal']) . ':00';
			$tglawal   = $this->fc->ustgl( $_POST['tglawal'], 'hari') .' '. $jamawal;
			$tglakhir  = $this->fc->ustgl( $_POST['tglakhir'], 'hari') .' '. $jamawal;
			$kdrapat   = $_POST['kdrapat'];

			if ($kdaktif=='0'){$snack_p=0;$snack_s=0;$makan=0;} else {
				if ($jmlpeserta==null) $jmlpeserta=0;
				if ($jnsrapat==1) {		// RAPAT BIASA
					if (Substr($jamawal,0,2)<'12') { $snack_p = $jmlpeserta; $snack_s = 0; }
					else { $snack_p = 0; $snack_s = $jmlpeserta; }

					$makan  = 0;
					$JamSta = ((int)Substr($jamawal,0,2) + 0) + ((int)Substr($jamawal,3,2) / 60);
					$JamEnd = ((int)Substr($jamawal,0,2) + 3) + ((int)Substr($jamawal,3,2) / 60);
					if (($JamSta>=$Jam_Awal_Istirahat and $JamSta<$Jam_Akhir_Istirahat) or ($JamEnd>=$Jam_Awal_Istirahat and $JamEnd<=$Jam_Akhir_Istirahat)) $makan = $jmlpeserta;
				}
				if ($jnsrapat==3) {$snack_p = $jmlpeserta; $snack_s = $jmlpeserta; $makan = $jmlpeserta; }	// RAPAT KHUSUS
			}
		}

		if ($action=='Rekam' and $this->ruangan_ok($kdrapat, $tglawal)) {
			$this->db->query( "insert into d_rapat (iduser, kdso, jnsrapat, nound, tglawal, kdrapat, pic, agenda, jmlpeserta, snack_p, snack_s, makan, keterangan, tglrekam) values ('$iduser','$kdso','$jnsrapat','$nound','$tglawal',$kdrapat,'$pic','$agenda', $jmlpeserta, $snack_p, $snack_s, $makan, '$keterangan', now())" );
		}
		if ($action=='Ubah' and ($this->session->userdata('kdso')==$kdedit or $this->session->userdata('kdso')=='001')) {
			$this->db->query( "update d_rapat set agenda='$agenda', pic='$pic', jmlpeserta='$jmlpeserta', snack_p=$snack_p, snack_s=$snack_s, makan=$makan, keterangan='$keterangan', tglrekam=now() where idrapat=$idrapat" );
		}
		if ($action=='Hapus' and $this->session->userdata('kdso')==$kdedit) {
			$this->db->query( "delete from d_rapat where idrapat=$idrapat" );
		}
		return;
	}

	public function ruangan_ok($idruang, $tglawal){
		// $tglX = $this->fc->ustgl( $tglawal, 'hari' );
		$tglXA = date("Y-m-d H:i:s", strtotime('-179 minutes', strtotime($tglawal)) );
		$tglXZ = date("Y-m-d H:i:s", strtotime('+179 minutes', strtotime($tglawal)) );

		$query = $this->db->query("Select kdrapat From d_rapat Where jnsrapat=3 And '$tglXA' Between Date_Format(tglawal, '%Y-%m-%d') And Date_Format(tglakhir, '%Y-%m-%d')");
		$inap  = $this->fc->ToArr( $query->result_array(), 'kdrapat');

		$query = $this->db->query("Select kdrapat From d_rapat Where tglawal Between '$tglXA' And '$tglXZ'");
		$rapat = $this->fc->ToArr( $query->result_array(), 'kdrapat');

		$ruang = array_merge($inap, $rapat);
		$ruang = $this->fc->ToArr( $ruang, 'kdrapat');
		if (array_key_exists($idruang, $ruang)) return FALSE;
		else return TRUE;
	}

	public function get_cetak_rapat($tgl){
		$query = $this->db->query("Select DATE(tglawal) tgl,pic,agenda,jmlpeserta,makan,snack_p,snack_s,b.nmso, tglrekam From d_rapat a Left Join t_so b On a.kdso=b.kdso Where DATE(tglawal)='$tgl' ");
		return $query->result_array();
	}

	public function get_sum_cetak_rapat($tgl){
		$query = $this->db->query("Select DATE(tglawal) tgl,pic,SUM(makan) makan, SUM(snack_p) snack_p, SUM(snack_s) snack_s FROM d_rapat Where DATE(tglawal)='$tgl'");
		return $query->row_array();
	}

	public function get_jbt_cetak_rapat(){
		$query = $this->db->query("Select a.*, b.nmuser From t_rapat_seting a Left Join t_user b On a.nip=b.nip");
		return $query->row_array();
	}

}
