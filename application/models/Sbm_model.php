<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Sbm_model extends CI_Model {

	function get_rekap() {
		$ref = $this->load->database('ref', TRUE);
		$qry = $ref->query("Select left(kdsbu,3) kode, nmsbu uraian, '' ket From t_sbu Where Right(kdsbu,5)='00001' Order By 1");
		$sbm = $this->fc->ToArr($qry->result_array(), 'kode');
		return $sbm;
	}

	function get_detil( $kode ) {
		$ref = $this->load->database('ref', TRUE);
		$qry = $ref->query("Select * From t_sbu Where Left(kdsbu,3)='$kode' Order By 1");
		$sbm = $this->fc->ToArr($qry->result_array(), 'kdsbu');
		return $sbm;
	}

}
