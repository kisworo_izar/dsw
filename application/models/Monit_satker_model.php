<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Monit_satker_model extends CI_Model {

	function adk_per_tiket($rev_id) {
		$kode = substr($rev_id,5,6);

		if (strpos($kode, '.')) $qry = $this->dbref->query("SELECT nmunit uraian FROM t_unit WHERE concat(kddept,'.',kdunit)='$kode' LIMIT 1");
		else $qry = $this->dbref->query("SELECT nmsatker uraian FROM t_satker WHERE kdsatker='$kode' LIMIT 1");
		$tbl = $qry->row_array();


		$whr = "rev_id='$rev_id'";
		$qry = $this->dbrvs->query("SELECT kddept, kdunit, kdsatker kode, '' uraian, revisike, val_pagu, val_blokir, val_ds, val_ssb, indikasi FROM revisi_satker WHERE $whr ORDER BY 1");
		$dat = $this->fc->ToArr($qry->result_array(), 'kode');

		$qry = $this->dbref->query("SELECT kdsatker kode, nmsatker uraian FROM t_satker WHERE kdsatker ". $this->fc->InVar($dat, 'kode'));
		foreach ($qry->result_array() as $row) $dat[ $row['kode'] ]['uraian'] = $row['uraian'];

		foreach ($dat as $key=>$row) {
			$ke = sprintf('%02d', ((int)$row['revisike'])-1);
			$link = 'unit='. $row['kddept'].$row['kdunit'] .'&satker='. $row['kode'];
			$dat[$key]['link_matrik']= site_url("monit_satker?q=caa29&$link&id=$rev_id&ke=$ke");
			$dat[$key]['link_pok'] 	 = site_url("monit_satker?q=45007&$link&id=$rev_id");
			$dat[$key]['link_ssb'] 	 = site_url("revisi?q=e1d8c&$link&revid=$rev_id");
		}

		$data['head']  = ($tbl) ? ":: $rev_id - ".$tbl['uraian'] : ":: $rev_id";
		$data['tabel'] = $dat;
		return $data;
	}
}
