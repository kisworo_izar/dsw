<?php
class Photography_model extends CI_Model {

	function get_gallery() {
	    // Table GALLERY untuk Content Utama
		$query = $this->db->query("Select a.*, nip, nmuser From photo_gallery a Left Join t_user b On a.iduser=b.iduser Order By tglupload Desc");
		$photo = $this->fc->ToArr($query->result_array(), 'idgallery');
		foreach ($photo as $key=>$row) $photo[$key]['comment'] = array(); 

	    // Query ambil Array COMMENT dan Insert
		$query = $this->db->query("Select a.*, nip, nmuser From photo_comment a Left Join t_user b On a.iduser=b.iduser Where modul='gallery'");
		foreach ($query->result_array() as $row) {
			$key = $row['idmodul']; 
			if(array_key_exists($key, $photo)) {
				$arr = $photo[$key]['comment'];
				array_push($arr, $row);
				$photo[$key]['comment'] = $arr;
			}
		}

		$data['photo'] = $photo;
		return $data;
	}

	function save_gallery_comment() {
		$iduser		= $this->session->userdata('iduser');
		$idgallery	= $_POST['idgallery'];
		$pesan		= $_POST['pesan'];
		$this->db->query( "Insert Into photo_comment (modul, idmodul, iduser, pesan, tglkirim) Values ('gallery', '$idgallery', '$iduser', \"$pesan\", current_timestamp())" );
		return 'Ok';
	}

	function save_gallery_like( $like ) {
		$nmuser		= trim($this->session->userdata('nmuser'));
		$idgallery	= $_POST['idgallery'];
		if ($like == 'like') $this->db->query( "Update photo_gallery Set likes=concat(likes,'$nmuser, ') Where idgallery=$idgallery" );
		// else $this->db->query( "Update photo_gallery Set likes = Replace(likes,'$nmuser, ','') Where idgallery=$idgallery" );
		else $this->db->query( "Update photo_gallery Set likes = Replace(likes,'$nmuser, ','') Where idgallery=$idgallery" );
		return 'Ok';
	}


	function get_blog( $idblog=null ) {
	    // Condition untuk Prolog 5 ARTICLE atau Full Teks 1 ARTICLE Pilihan
		$whr = "Order By tglcreate Desc Limit 5";  if ($idblog != null) $whr = "Where idblog=$idblog Limit 1";
		
	    // Table ARTICLE untuk Content Utama
		$query = $this->db->query("Select a.*, nmuser, nip, jabatan, kdso nmso, '' pesan, tglcreate tanggal From photo_article a Left Join t_user b On a.iduser=b.iduser $whr");
		$blog  = $this->fc->ToArr( $query->result_array(), 'idblog');
		foreach ($blog as $key=>$row) $blog[$key]['comment'] = array(); 

	    // Query ambil Array COMMENT dan Insert
		$query = $this->db->query("Select a.*, nmuser, nip, jabatan, kdso nmso, '' judul, tglkirim tanggal From photo_comment a Left Join t_user b On a.iduser=b.iduser Where modul='article' and idmodul ". $this->fc->Invar($blog, 'idblog'));
		foreach ($query->result_array() as $row) {
			$key = $row['idmodul']; 
			if(array_key_exists($key, $blog)) {
				$arr = $blog[$key]['comment'];
				array_push($arr, $row);
				$blog[$key]['comment'] = $arr;
			}
		}

	    // Table ARTICLE untuk SideBar
	    $query = $this->db->query("Select idblog, judul, gambar From photo_article Order By tglcreate Desc");
	    $data['side'] = $query->result_array();

	    // Proses Table ARTICLE menjadi Multi Array atau Single Array
		$data['blog'] = $blog;  if ($idblog != null) $data['blog'] = $blog[$idblog]; 
		return $data;
	}


	// ADMIN CONTROL PANEL //
	function admin_blog_get() {
		$query = $this->db->query("Select a.*, nip, nmuser From photo_article a Left Join t_user b On a.iduser=b.iduser Order By idblog Desc");
		return $this->fc->ToArr($query->result_array(), 'idblog');
	}

	function admin_blog_save(  ) {
		$aksi  	= $_POST['aksi'];
		$idblog	= $_POST['idblog'];  
		$judul 	= $_POST['judul'];  
		$gambar = $_POST['gambar'];  
		$artikel= $_POST['artikel'];
		$tags	= $_POST['tags'];
		$iduser	= $this->session->userdata('iduser');
		$sumber	= $_POST['sumber'];

		if ($aksi=='Rekam') $this->db->query( "Insert Into photo_article (judul, gambar, artikel, tags, iduser, sumber, tglcreate) Values ('$judul', '$gambar', \"$artikel\", '$tags', '$iduser', '$sumber', current_timestamp())" );
		if ($aksi=='Ubah')  $this->db->query( "Update photo_article Set judul='$judul', gambar='$gambar', artikel=\"$artikel\", tags='$tags', sumber='$sumber' Where idblog=$idblog" );
		if ($aksi=='Hapus') $this->db->query( "Delete From photo_article where idblog=$idblog" );

		return 'ok';
	}

	function admin_gallery_get() {
		$query = $this->db->query("Select a.*, nip, nmuser From photo_gallery a Left Join t_user b On a.iduser=b.iduser Order By tglupload Desc");
		return $this->fc->ToArr($query->result_array(), 'idgallery');
	}

	function admin_gallery_save(  ) {
		$aksi  		= $_POST['aksi'];
		$idgallery	= $_POST['idgallery'];  
		$photo		= $_POST['photo'];
		$judul		= $_POST['judul'];
		$tag		= $_POST['tag'];
		$iduser		= $this->session->userdata('iduser');

		if ($aksi=='Rekam') $this->db->query( "Insert Into photo_gallery (photo, judul, tag, iduser, tglupload) Values ('$photo', \"$judul\", \"$tag\", '$iduser', current_timestamp())" );
		if ($aksi=='Ubah')  $this->db->query( "Update photo_gallery Set photo='$photo', judul=\"$judul\", tag=\"$tag\" Where idgallery=$idgallery" );
		if ($aksi=='Hapus') $this->db->query( "Delete From photo_gallery where idgallery=$idgallery" );

		return $aksi;
	}

	function admin_komentar_get() {
		$query = $this->db->query("Select a.*, '' judul, '' gambar, nmuser From photo_comment a Left Join t_user b On a.iduser=b.iduser Order By tglkirim Desc");
		$hasil = $this->fc->ToArr($query->result_array(), 'idcomment');

	    // Query ambil Judul PHOTO
		$query = $this->db->query("Select * From photo_gallery Where idgallery ". $this->fc->Invar($hasil, 'idmodul'));
		$photo = $this->fc->ToArr($query->result_array(), 'idgallery');

		foreach ($hasil as $key=>$value) {
			$id = $value['idmodul']; 
			if ($value['modul'] == 'gallery') {
				if(array_key_exists($id, $photo)) { 
					$hasil[$key]['judul']  = $photo[$id]['judul'];
					$hasil[$key]['gambar'] = 'gallery/'. $photo[$id]['photo'];
				}
			}
		}
		return $hasil;
	}

	function admin_komentar_save(  ) {
		$aksi  		= $_POST['aksi'];
		$idcomment	= $_POST['idcomment'];  
		$nama		= $_POST['nama'];
		$pesan		= $_POST['pesan'];

		if ($aksi=='Ubah')  $this->db->query( "Update photo_comment Set pesan=\"$pesan\" Where idcomment=$idcomment" );
		if ($aksi=='Hapus') $this->db->query( "Delete From photo_comment where idcomment=$idcomment" );
		return 'ok';
	}


}