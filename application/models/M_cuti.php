<?php 
class M_cuti extends CI_Model {
	public function get_data() {
		$whr  = "";

		$cari = $this->session->userdata('cari');
		if ($cari) {
			$whr = " where " ;
			$ref = " where " ;
			$arr = explode(' ', $cari);
			for ($i=0; $i<count($arr); $i++) {
				$str  = $arr[$i];
				$whr .= "nmuser like '%$str%' or nip like '%$str%' ";
				$ref .= "nmuser like '%$str%'  ";
				if ($i<count($arr)-1) {
					$whr .= " or ";
					$ref .= " or ";
				}
			}
			$query = $this->db->query("select iduser from t_user $ref");
			$arr   = $query->result_array();
			if ($arr) {
				$whr .= " or ";
			}
			for ($i=0; $i<count($arr); $i++) {
				$str  = $arr[$i]['iduser'];
				$whr .= "nmuser like '%$str%' ";
				if ($i<count($arr)-1) {
					$whr .= " or ";
				}
			}
		}

	$query = $this->db->query("select a.*, nmuser, nip, b.kdso, nmso1 from d_cuti a Left Join t_user b On a.iduser=b.iduser Left Join t_so c On concat(Left(b.kdso,2),'0000')=c.kdso $whr Order By nmuser");
	$hasil = $this->fc->ToArr( $query->result_array(), 'iduser');
	return $hasil;
	}

	public function get_row( $action, $iduser=NULL ) {
		if ( is_null($iduser) ) $iduser='999999';

		if ($action=='Rekam') {
			//$query = $this->db->query("select a.*, nmuser, '' nosurat, date('Y-m-d') tglawal, date('Y-m-d') tglakhir, 0 jumlah, date('Y-m-d') tglsurat from d_cuti a Left Join t_user b On a.iduser=b.iduser where a.iduser=$iduser ");Date_Format(tglawal, '%Y-%m-%d') tgl,date('d-m-Y') tglawal
			$query = $this->db->query("select a.*,  nmuser, '' nosurat, date('Y-m-d') tglawal, date('Y-m-d') tglakhir, 0 jumlah,0 cuti_panggil , date('Y-m-d') tglsurat,keterangan, idcuti from d_cuti a Left Join t_user b On a.iduser=b.iduser Left Join d_cuti_ambil c On a.iduser=c.iduser where a.iduser=$iduser"); 
			$hasil = $query->row_array();
		}

		
		if ($action=='Ubah') {
			$query = $this->db->query("select a.*, nmuser from d_cuti_ambil a Left Join t_user b On a.iduser=b.iduser where a.idcuti=$iduser ");
			$hasil = $query->row_array();
		}

		if ($action=='Hapus') {
			$query = $this->db->query("select a.*, nmuser from d_cuti_ambil a Left Join t_user b On a.iduser=b.iduser where a.idcuti=$iduser ");
			$hasil = $query->row_array();
		}

		return $hasil;
	}

	public function save() {
		$jenis			  	= $_POST['jnscuti'];
		$action			  	= $_POST['ruh'];
		$iduser				= $_POST['iduser'];
		$idcuti				= $_POST['idcuti'];
		$nosurat  			= $_POST['nosurat'];
		$tglawal   			= $_POST['tglawal'];
		$tglakhir			= $_POST['tglakhir'];
		$tglsurat			= $_POST['tglsurat'];
		$jumlah				= $_POST['jumlah'];
		$keterangan 		= $_POST['keterangan'];
		if ($jenis == '2') {
			$jumlah = (-1 * $jumlah);
		}

		
		if ($action=='Rekam') {
			$tahun = date("Y");
			$query = $this->db->query("insert into d_cuti_ambil (iduser,tahun,nosurat,tglawal,tglakhir,tglsurat,jumlah,keterangan) values ('$iduser','$tahun','$nosurat','$tglawal', '$tglakhir','$tglsurat',$jumlah,'$keterangan') ");
			$query = $this->db->query("update d_cuti set cuti_ambil=cuti_ambil+$jumlah Where iduser=$iduser");
		}

		if ($action=='Ubah') {
		 	$query = $this->db->query("update d_cuti_ambil set nosurat='$nosurat', tglawal='$tglawal', tglakhir='$tglakhir',tglsurat='$tglsurat',jumlah=$jumlah, keterangan='$keterangan' Where idcuti=$idcuti ");
			$query = $this->db->query("update d_cuti_ambil set jumlah='$jumlah' Where idcuti=$idcuti");
			$query = $this->db->query("update d_cuti set cuti_ambil=$jumlah Where iduser=$iduser");
		}

		if ($action=='Hapus') {
			$query = $this->db->query("delete from d_cuti_ambil where idcuti=$idcuti");
			$query = $this->db->query("update d_cuti set cuti_ambil=cuti_ambil-$jumlah Where iduser=$iduser");


		}
		return;
	}

	public function get_anak( $idkey ) {
		$query = $this->db->query("Select * From d_cuti_ambil Where concat(tahun,'_',iduser)='$idkey' Order By tglsurat");
		$hasil = $query->result_array();
		$msg =	'<tr class="child">
					<td>&nbsp;</td>
		            <td><strong>Nomor SK Cuti </strong></td>
		            <td><strong>Tanggal Mulai Cuti</strong></td>
		            <td><strong>Tanggal Selesai Cuti</strong></td>  
		            <td><strong>Lama Cuti</strong></td>
		            <td colspan="3"><strong><center>Keterangan</center></strong></td>
		        </tr>';

		$ms =	'<tr class="child">
					<td>&nbsp;</td>
					<td colspan="6" ><strong>Data Cuti Tidak Ditemukan...</strong></td>
				</tr>';        

		if ($hasil) {
			foreach ($hasil as $row) {
					$msg .='<tr class="child">
					<td id="'. $row['iduser'] .'">&nbsp;</td>
					<td id="'. $row['idcuti'] .'">'.$row['nosurat'].'</td>
					<td>'.$this->fc->idtgl($row['tglawal'],'hari').'</td>
					<td>'.$this->fc->idtgl($row['tglakhir'],'hari').'</td>
					<td >'.ABS($row['jumlah']).' Hari</td>
					<td colspan="3">'.$row['keterangan'].'</td>
					</tr>';
							}
		        } else  {
			       return $ms;	
		        }
		return $msg;
	}

	public function get_cuti_st( $idcuti ) {
		$query = $this->db->query("Select *, nmuser, nip, pangkat, jabatan, b.kdso, nmso1 From d_cuti_ambil a Left Join t_user b On a.iduser=b.iduser Left Join t_so c On concat(Left(b.kdso,2),'0000')=c.kdso Where idcuti=$idcuti");
		return $query->row_array();
	}
}