<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Rkakl_model extends CI_Model {

	public function get_pagu( $db, $kdgiat=null, $lvl=null ) {
		$p = $this->setting();
		$str = "And kdgiat='$kdgiat'";  if ($kdgiat=='0000' or $kdgiat==null) $str = '';
		$dbref = $this->load->database('ref', TRUE);
		if ($lvl==null) $lvl = 8; 

		$hasil = array();
		for ($i=1; $i<=$lvl; $i++) {
			$tb = $p[$i]['tbl'];  $kd = 'concat('. $p[$i]['kd'] .')';  $kode = 'concat('. $p[$i]['kode'] .')';  $nm = $p[$i]['nm'];  $whr=$p['whr'];  

			if ($tb!='d_item') { 
				$query = $db->query("Select $kd kdkey, concat($kode) kode, '' uraian, 0 volkeg, '' satkeg, 0 hargasat, Sum(jumlah) jumlah, '' atrib, '' sdcp, $i level From d_item Where $whr $str Group By 1");
			} else {
				$query = $db->query("Select $kd kdkey, '' kode, nmitem uraian, volkeg, satkeg, hargasat, jumlah, '' atrib, '' sdcp, $i level From d_item Where $whr $str");
			}
			$nilai = $this->fc->ToArr( $query->result_array(), 'kdkey');

			if ( $tb=='t_program' or $tb=='t_giat' ) { 
				$query = $dbref->query("Select $kd kdkey, $nm uraian From $tb where $kd " . $this->fc->InVar($nilai, 'kdkey')); 
				$t_ref = $this->fc->ToArr( $query->result_array(), 'kdkey');
				$nilai = $this->fc->Left_Join( $nilai, $t_ref, "kdkey, uraian=uraian" );
			}

			if ( $tb=='d_soutput' or $tb=='d_skmpnen' ) { 
				$query = $db->query("Select $kd kdkey, $nm uraian From $tb where $kd " . $this->fc->InVar($nilai, 'kdkey')); 
				$t_ref = $this->fc->ToArr( $query->result_array(), 'kdkey');
				$nilai = $this->fc->Left_Join( $nilai, $t_ref, "kdkey, uraian=uraian" );
			}

			if ($tb=='t_output') { 
				$query = $dbref->query("Select $kode kdkey, $nm uraian, sat From $tb where $kode " . $this->fc->InVar($nilai, 'kode')); 
				$t_ref = $this->fc->ToArr( $query->result_array(), 'kdkey');
				$nilai = $this->fc->Left_Join( $nilai, $t_ref, "kode, uraian=uraian, satkeg=sat" );

				$query = $db->query("Select $kd kdkey, vol From d_output where $kd " . $this->fc->InVar($nilai, 'kdkey')); 
				$t_ref = $this->fc->ToArr( $query->result_array(), 'kdkey');
				$nilai = $this->fc->Left_Join( $nilai, $t_ref, "kdkey, volkeg=vol" );
			}

			if ( $tb=='d_kmpnen' ) { 
				$query = $db->query("Select $kd kdkey, $nm uraian, if(kdsbiaya='1','U','') atrib From $tb where $kd " . $this->fc->InVar($nilai, 'kdkey')); 
				$t_ref = $this->fc->ToArr( $query->result_array(), 'kdkey');
				$nilai = $this->fc->Left_Join( $nilai, $t_ref, "kdkey, uraian=uraian,atrib=atrib" );
			}

			if ($tb=='t_akun') { 
				$query = $dbref->query("Select $kode kdkey, $nm uraian From $tb where $kode " . $this->fc->InVar($nilai, 'kode')); 
				$t_ref = $this->fc->ToArr( $query->result_array(), 'kdkey');
				$nilai = $this->fc->Left_Join( $nilai, $t_ref, "kode,uraian=uraian" );
			}

			$hasil = array_merge($hasil, $nilai);
		}
		$hasil = $this->fc->array_index($hasil, 'kdkey');
		$hasil = $this->fc->ToArr( $hasil, 'kdkey');
 		return $hasil;
	}

	public function filter( $tabel, $output=null ) {
		if ( $output!=null ) {
			foreach ($tabel as $key=>$value) {
				if ( substr($value['kdkey'],0,18) != $output ) unset( $tabel[$key] );
			}
		}
		return $tabel;
	}

	public function setting() {
		$param['whr'] = "thang='2016' and kdstatus='2'";
		$param['1']   = array('tbl'=>'t_program', 'kd'=>"kddept,'.',kdunit,'.',kdprogram", 'kode'=>"kddept,'.',kdunit,'.',kdprogram", 'nm'=>'nmprogram');
		$param['2']   = array('tbl'=>'t_giat', 'kd'=>$param['1']['kd'] . ",'.',kdgiat", 'kode'=>"kdgiat", 'nm'=>'nmgiat');
		$param['3']   = array('tbl'=>'t_output', 'kd'=>$param['2']['kd'] . ",'.',kdoutput", 'kode'=>"kdgiat,'.',kdoutput", 'nm'=>'nmoutput');
		$param['4']   = array('tbl'=>'d_soutput', 'kd'=>$param['3']['kd'] . ",'.',kdsoutput", 'kode'=>"kdgiat,'.',kdoutput,'.',kdsoutput", 'nm'=>'ursoutput');
		$param['5']   = array('tbl'=>'d_kmpnen', 'kd'=>$param['4']['kd'] . ",'.',kdkmpnen", 'kode'=>"kdkmpnen", 'nm'=>'urkmpnen');
		$param['6']   = array('tbl'=>'d_skmpnen', 'kd'=>$param['5']['kd'] . ",'.',trim(kdskmpnen)", 'kode'=>"trim(kdskmpnen)", 'nm'=>'urskmpnen');
		$param['7']   = array('tbl'=>'t_akun', 'kd'=>$param['6']['kd'] . ",'.',kdakun", 'kode'=>"kdakun", 'nm'=>'nmakun');
		$param['8']   = array('tbl'=>'d_item', 'kd'=>$param['7']['kd'] . ",'.',noitem", 'kode'=>"noitem", 'nm'=>'');
		return $param;
	}

}
