<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Dash_model extends CI_Model {

	public function get_data() {
		#persiapan variabel
		$year  = $this->session->userdata("thang");
		$dbrvs = $this->load->database('revisi'.$year, TRUE);
		$dbref = $this->load->database('ref'.$year, TRUE);
		$date  = date("d-m-$year");
		$start = $this->fc->ustgl($date);

		#kaplingan array untuk semua kanwil
		$qry   = $dbref->query("SELECT  CONVERT(kdlokasi, UNSIGNED INTEGER) AS ID, kdlokasi, nmkanwil PROVINSI, '0' JML  FROM t_kanwil WHERE kdkanwil <> '00' group by 1");
		$kwl   = $this->fc->ToArr($qry->result_array(), 'kdlokasi');

		#hitung jumalh revisi per kanwil
		$qry   = $dbrvs->query("SELECT kdlokasi, count(rev_id) jml FROM revisi_satker WHERE rev_id IN (SELECT rev_id FROM revisi WHERE pus_tgl >= '$start 00:00:00' AND pus_tgl <= '$start 23:59:59' AND rev_level='3') group by 1 ");
		$jml   = $this->fc->ToArr($qry->result_array(), 'kdlokasi');

		#penggabungan array isian ke aray kwl
		foreach($kwl as $key => $row){
			if(array_key_exists($row['kdlokasi'], $jml)) $kwl[$row['kdlokasi']]['JML'] = $jml[$row['kdlokasi']]['jml'];
		}
		#menormalkan bentuk array
		$arr = $this->fc->array_index($kwl,'ID');
		#encode untuk memparsing data ke json maps
		$data = '{"provinsi":'.json_encode($arr).'}';

		return $data;
	}

	public function get_jml_revisi() {
		$lokasi = $this->session->userdata('kdlokasi');

		$query  = $this->db->query("SELECT *, (jumlah_revisi/(SELECT sum(jumlah_revisi) FROM d_tes_rev)) * 100 AS persen FROM d_tes_rev WHERE kdlokasi = '$lokasi' ORDER BY kdstatus");
		return $query->result_array();
	}

	function count_revisi(){
		$year  = $this->session->userdata("thang");
		$dbrvs = $this->load->database('revisi'.$year, TRUE);
		$dbref = $this->load->database('ref'.$year, TRUE);
		$date  = date("d-m-$year");
		$start = $this->fc->ustgl($date);
 
		#kaplingan data semua kanwil
		$qry   = $dbref->query("SELECT kdkanwil, nmkanwil, kdlokasi, '0' jmlrevisi, '' sub ,'0' blmproses, '0' dikembalikan, '0' sdgproses, '0' setuju, '0' tolak   FROM t_kanwil WHERE kdkanwil <> '00' group by 1");
		$kwl   = $this->fc->ToArr($qry->result_array(), 'kdlokasi');

		#data hitungan jumlah revisi total
		$qry   = $dbrvs->query("SELECT kdlokasi, count(rev_id) jml FROM revisi_satker WHERE rev_id IN (SELECT rev_id FROM revisi WHERE pus_tgl >= '$start 00:00:00' AND pus_tgl <= '$start 23:59:59' AND rev_level='3') group by 1 ");
		$jml   = $this->fc->ToArr($qry->result_array(), 'kdlokasi');

		#kaplingan array bentukan untuk maps
		$arr   = array('0'=>array( 'kdlokasi'=>'', 'kdstatus' =>'1', 'nmstatus'=>'Belum Proses','warna'=>'progress-bar-info', 'jumlah_revisi'=>'', 'persen'=>'0'), '1'=>array( 'kdlokasi'=>'', 'kdstatus' =>'2', 'nmstatus'=>'Dikembalikan','warna'=>'progress-bar-yellow', 'jumlah_revisi'=>'', 'persen'=>'0'),  '3'=>array( 'kdlokasi'=>'', 'kdstatus' =>'3', 'nmstatus'=>'Sedang Proses','warna'=>'progress-bar-blue', 'jumlah_revisi'=>'', 'persen'=>'0'), '4'=>array( 'kdlokasi'=>'', 'kdstatus' =>'4', 'nmstatus'=>'Disetujui','warna'=>'progress-bar-green', 'jumlah_revisi'=>'', 'persen'=>'0'), '5'=>array( 'kdlokasi'=>'', 'kdstatus' =>'5', 'nmstatus'=>'Ditolak','warna'=>'progress-bar-red', 'jumlah_revisi'=>'', 'persen'=>'0'));

		#hitung jumlah revisi
		$qry   = $dbrvs->query("SELECT a.rev_id, rev_tahap, rev_status, t2_status,  kdlokasi, '' status, '' kdstatus FROM revisi a LEFT JOIN revisi_satker b ON a.rev_id=b.rev_id WHERE a.pus_tgl >= '$start 00:00:00' AND a.pus_tgl <= '$start 23:59:59' AND a.rev_level='3' ");
		$thp   = $this->fc->ToArr($qry->result_array(),'rev_id');

		// echo "<pre>";print_r($thp);exit();

		foreach ($thp as $row){
			if 	   ($row['rev_tahap'] == '2' and $row['rev_status'] == '9' and $row['t2_status'] == '9') { $thp[$row['rev_id']]['status'] = 'Belum Proses';  $thp[$row['rev_id']]['kdstatus'] = '1';}
			elseif ($row['rev_tahap'] == '2' and $row['rev_status'] == '0') { $thp[$row['rev_id']]['status'] = 'Dikembalikan'; 	$thp[$row['rev_id']]['kdstatus'] = '2';}
			elseif ($row['rev_tahap'] == '7' and $row['rev_status'] == '2') { $thp[$row['rev_id']]['status'] = 'Disetujui'; 	$thp[$row['rev_id']]['kdstatus'] = '4'; }
			elseif ($row['rev_tahap'] == '7' and $row['rev_status'] == '0') { $thp[$row['rev_id']]['status'] = 'Ditolak'; 		$thp[$row['rev_id']]['kdstatus'] = '5'; }
			else { $thp[$row['rev_id']]['status'] = 'Diproses'; $thp[$row['rev_id']]['kdstatus'] = '3'; }

			if($row['rev_tahap']<'2') unset($thp[$row['rev_id']]);
		}
		// echo "<pre>";print_r($thp);exit();

		#mengisi array jumlah revisi per kanwil, harus dipisah karena sub array
		foreach($kwl as $key => $row){
			if(array_key_exists($row['kdlokasi'], $jml)) $kwl[$row['kdlokasi']]['jmlrevisi'] = $jml[$row['kdlokasi']]['jml'];
			$kwl[$key]['sub'] = array();
			$kwl[$key]['sub'] = $arr;
			$sub 			  = $kwl[$row['kdlokasi']]['sub'];
		}
		#menghitung tiap2 jenis revisi 
		foreach($thp as $row){
			if(array_key_exists($row['kdlokasi'], $kwl)) {
				if($row['kdstatus'] == '1') $kwl[$row['kdlokasi']]['blmproses'] 	+= 1;
				if($row['kdstatus'] == '2') $kwl[$row['kdlokasi']]['dikembalikan'] 	+= 1;
				if($row['kdstatus'] == '3') $kwl[$row['kdlokasi']]['sdgproses'] 	+= 1;
				if($row['kdstatus'] == '4') $kwl[$row['kdlokasi']]['setuju'] 		+= 1;
				if($row['kdstatus'] == '5') $kwl[$row['kdlokasi']]['tolak'] 		+= 1;
			}

		}
		#mdnghitung prosentase
		foreach($kwl as $row){
			$sub = $kwl[$row['kdlokasi']]['sub'];
			foreach($sub as $key => $row2){
				$sub[$key]['kdlokasi'] = $row['kdlokasi'];
				if($row2['kdstatus'] == '1') { $sub[$key]['jumlah_revisi'] = $row['blmproses']; 	if($row['blmproses']>0) $sub[$key]['persen'] = ($row['blmproses']/$row['jmlrevisi']) *100 ;}
				if($row2['kdstatus'] == '2') { $sub[$key]['jumlah_revisi'] = $row['dikembalikan']; 	if($row['dikembalikan'])$sub[$key]['persen'] = ($row['dikembalikan']/$row['jmlrevisi']) *100 ;} 
				if($row2['kdstatus'] == '3') { $sub[$key]['jumlah_revisi'] = $row['sdgproses']; 	if($row['sdgproses']) $sub[$key]['persen'] = ($row['sdgproses']/$row['jmlrevisi']) *100 ;} 
				if($row2['kdstatus'] == '4') { $sub[$key]['jumlah_revisi'] = $row['setuju']; 		if($row['setuju']) $sub[$key]['persen'] = ($row['setuju']/$row['jmlrevisi']) *100 ;}
				if($row2['kdstatus'] == '5') { $sub[$key]['jumlah_revisi'] = $row['tolak']; 		if($row['tolak']) $sub[$key]['persen'] = ($row['tolak']/$row['jmlrevisi']) *100 ;} 
			}
			$arr = $sub;
			$kwl[$row['kdlokasi']]['sub'] = $arr;
		}
		
		return $kwl;

	}


}
