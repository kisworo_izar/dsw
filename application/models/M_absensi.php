<?php
class M_absensi extends CI_Model {

    public function get_data( $range=null ) {
        if ( $range==null ) $range='';
        $nip   = $this->session->userdata('nip');
        // $query = $this->db->query("select * from d_absensi where nip='$nip' $range order by tanggal desc ");
        $query = $this->milea->query("SELECT * FROM absen_net WHERE nip='$nip' $range order by tgl desc ");
        $data  = $query->result_array();

        return $data;
    }

    public function get_idws() {
        $this->satu = $this->load->database('dbsatu', TRUE);
        $iduser = $this->session->userdata('iduser');
        
        $query  = $this->satu->query("select $iduser user, idws link from ws_absen where script='$iduser'");
        $idws   = $query->row_array();
        if (empty($idws)) {
            $this->addWsUser($iduser);
            $query  = $this->satu->query("select $iduser user, idws link from ws_absen where script='$iduser'");
            $idws   = $query->row_array();
        }
        $this->create_qr($iduser, $idws['link']);

        return $idws;
    }

    function addWsUser($iduser)
    {
        $idws = '';
        while (TRUE) {
            $idws = $this->randomstring(5);
            $this->satu->select('*');
            $this->satu->from('ws_absen');
            $this->satu->where('idws', $idws);
            $ret = $this->satu->get()->result();
            if (empty($ret)) {
                break;
            }
        }

        $this->satu->trans_start();
        $this->satu->insert('ws_absen', array('idws' => $idws, 'script' => $iduser, 'fungsi' => 'absen'));
        $insert_id = $this->satu->insert_id();
        $this->satu->trans_complete();

        return $insert_id;
    }

    function randomstring($len)
    {
        $string = "";
        $chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        for($i=0;$i<$len;$i++)
            $string.=substr($chars,rand(0,strlen($chars)),1);
        return $string;
    }

    public function get_rekap() {
        $nip   = $this->session->userdata('nip');
        $tahun = date('Y');
        $query = $this->db->query("select deskripsi, count(*) jumlah from d_absensi where nip='$nip' and year(tanggal) = '$tahun' group by 1 ");
        return $query->result_array();
    }

    public function get_max() {
        $nip   = $this->session->userdata('nip');
        $tahun = date('Y');
        $query = $this->db->query("select max(tanggal) tanggal from d_absensi where nip='$nip' and year(tanggal) = '$tahun'");
        return $query->row_array();
    }

    public function create_qr($iduser, $link) {
        require_once APPPATH."/third_party/phpqrcode/qrlib.php";
        require_once APPPATH."/third_party/phpqrcode/qrconfig.php";

        $logopath = site_url("files/images/depkeu_round.png");
        $filepath = FCPATH."files/QR/$iduser"."_presensi.png";
        // $im = @imagecreate(200, 200);
        // imagepng($im, $filepath);
        // imagedestroy($im);
        $codelink = "https://satudja.kemenkeu.go.id/presensi?q=$link";
        QRcode::png("$codelink", $filepath, 10, 10, TRUE);
    }

    /* Fungsi ini untuk mengambil (array) daftar kode pelanggaran kehadiran */ 
    function getPelanggaranStatus()
    {
        $this->milea->select('kode');
        $this->milea->from('t_kehadiran');
        $this->milea->where('sts', '1');
        $res = $this->milea->get()->result();
        $ret = array();
        foreach ($res as $r) {
            $ret[] = $r->kode;
        }
        $ret[] = 'TK';

        return $ret;
    }

    /* Fungsi ini untuk mengambil daftar pelanggaran dari pegawai dengan $nip yang diberikan dan pada tahun $year */ 
    function getPelanggaran($nip, $year)
    {
        $status = $this->getPelanggaranStatus();

        $this->milea->select('*');
        $this->milea->from('absen_net');
        $this->milea->where('nip', $nip);
        $this->milea->where('YEAR(tgl)', $year);
        $this->milea->where('tgl <=', date('Y-m-d'));
        $this->milea->where_in('status', $status);
        $this->milea->order_by('tgl', 'desc');

        return $this->milea->get()->result();
    }

    function getPelanggaranHistory($nip)
    {
        $status = $this->getPelanggaranStatus();

        $this->milea->select('*');
        $this->milea->from('absen_net');
        $this->milea->where('nip', $nip);
        $this->milea->where('tgl <=', date('Y-m-d'));
        $this->milea->where_in('status', $status);
        $this->milea->order_by('tgl', 'desc');

        $results = $this->milea->get()->result();

        $output = array();

        $this->milea->select('DISTINCT(YEAR(tgl)) year');
        $this->milea->from('absen_net');
        $this->milea->where('nip', $nip);
        $this->milea->where('tgl <=', date('Y-m-d'));
        $this->milea->order_by('tgl', 'desc');

        $years = $this->milea->get()->result();

        foreach ($years as $year) {
            $output[$year->year]['minutetotal'] = 0;
            $output[$year->year]['times'] = 0;
            $output[$year->year]['detail'] = array();
        }

        foreach ($results as $result) {
            $year = date("Y", strtotime($result->tgl));

            $output[$year]['minutetotal'] += $result->pelanggaran;
            $output[$year]['times']++;
            $output[$year]['detail'][] = $result;
        }

        return $output;
    }

}
