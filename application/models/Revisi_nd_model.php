<?php
class Revisi_nd_model extends CI_Model {

	function get_data( $rev_id ) {
		// cari query data dari REVISI
		$query = $this->db->query("Select rev_id, rev_kdso, kl_dept, kl_unit, kl_pjb_jab, kl_surat_no, kl_surat_tgl, kl_surat_hal, t3_penelaahan_tgl, doc_usulan_revisi, doc_matrix, doc_rka, doc_adk, doc_dukung_sepakat, doc_dukung_hal4, doc_dukung_lainnya, t6_selesai_tgl From revisi Where rev_id='$rev_id'");
		$revisi= $query->row_array();

		// Direktorat dan Sub Direktorat
		$query = $this->db->query("Select nmso, kdsosurat, header1, header2 From t_so Where Left(kdso,2)='". substr($revisi['rev_kdso'],0,2) ."' And Right(kdso,4)='0000'");
		$hasil = $query->row_array();
		$revisi['direktur'] = 'Direktur '. $hasil['nmso'];
		$revisi['kdsosurat'] = $hasil['kdsosurat'];
		$revisi['header1'] = $hasil['header1'];
		$revisi['header2'] = $hasil['header2'];
		$query = $this->db->query("Select nmso From t_so Where kdso='". $revisi['rev_kdso'] ."'");
		$hasil = $query->row_array();
		$revisi['subdit'] = 'Kepala '. $hasil['nmso'];

		// Uraian KL dan Unit Eselon I
		$dbref = $this->load->database('ref', TRUE);
		$query = $dbref->query("Select nmdept From t_dept Where kddept='". $revisi['kl_dept'] ."'");
		$hasil = $query->row_array();
		$revisi['nmdept'] = $hasil['nmdept'];
		$query = $dbref->query("Select nmunit From t_unit Where kddept='". $revisi['kl_dept'] ."' And kdunit='". $revisi['kl_unit'] ."'");
		$hasil = $query->row_array();
		$revisi['nmunit'] = $hasil['nmunit'];


		// Surat Revisi KL dan Surat Pengantar Perbaikan
		$query = $this->db->query("Select rev_id, kl_dept, kl_unit, kl_pjb_jab, kl_surat_no, kl_surat_tgl, kl_surat_hal, doc_usulan_revisi, doc_matrix, doc_rka, doc_adk, doc_dukung_sepakat, doc_dukung_hal4, doc_dukung_lainnya From revisi Where rev_id='$rev_id'");
		$surat = $query->result_array();

		$kddept= $revisi['kl_dept']; $kdunit = $revisi['kl_unit'];
		$query = $this->db->query("Select rev_id, '$kddept' kl_dept, '$kdunit' kl_unit, kl_pjb_jab, kl_surat_no, kl_surat_tgl, kl_surat_hal, doc_usulan_revisi, doc_matrix, doc_rka, doc_adk, doc_dukung_sepakat, doc_dukung_hal4, doc_dukung_lainnya From revisi_perbaikan Where rev_id='$rev_id'");
		$hasil = $query->result_array();

		if ($hasil) foreach($hasil as $row) array_push($surat, $row);
		$nd['surat'] = $surat;
		//print_r( $nd['surat']);exit();


		// Update $REVISI Terkait Dokumen Revisi
		foreach($hasil as $row) {
			if ($row['doc_usulan_revisi']) $revisi['doc_usulan_revisi'] = $row['doc_usulan_revisi'];
			if ($row['doc_matrix']) $revisi['doc_matrix'] = $row['doc_matrix'];
			if ($row['doc_rka']) $revisi['doc_rka'] = $row['doc_rka'];
			if ($row['doc_adk']) $revisi['doc_adk'] = $row['doc_adk'];
			if ($row['doc_dukung_sepakat']) $revisi['doc_dukung_sepakat'] = $row['doc_dukung_sepakat'];
			if ($row['doc_dukung_hal4']) $revisi['doc_dukung_hal4'] = $row['doc_dukung_hal4'];
			if ($row['doc_dukung_lainnya']) $revisi['doc_dukung_lainnya'] = $row['doc_dukung_lainnya'];
		}
		$nd['revisi'] = $revisi;
		//print_r($nd['revisi']);exit();


		// Pejabat Kepala Sub Direktorat
		$query = $this->db->query("Select trim(nmuser) nmuser, nip From t_user Where Left(kdso,2)='". substr($revisi['rev_kdso'],0,2) ."' And kdeselon='31' And nip!=''");
		$nd['nmsubdit'] = $query->result_array();

		// cari query data dari NOTA DINAS
		$query = $this->db->query("Select * From revisi_nd Where rev_id='$rev_id'");
		$nd['nota'] = $query->row_array();

		// cari query data dari NOTA DINAS
		$query = $this->db->query("Select * From revisi_nd_jenis Order By kdjnsrev");
		$nd['jnsrev'] = $query->result_array();

		// cari query data dari Ref NOTA PENDAPAT
		$query = $this->db->query("Select * From revisi_nd_pendapat");
		$nd['pendapat'] = $query->row_array();
		return $nd;
	}

	function save() {
		$iduser			 = $this->session->userdata('iduser');
		$rev_id			 = $_POST['rev_id'];
		$nd_dit_pgs		 = $_POST['nd_dit_pgs'];
		$nd_subdit_pgs	 = $_POST['nd_subdit_pgs'];
		$nd_lamp_jml	 = $_POST['nd_lamp_jml'];
		$nd_hal			 = $_POST['nd_hal'];
		$nd_tgl			 = $_POST['nd_tgl'];
		$nd_subdit_nip 	 = substr($_POST['nd_subdit'], 0, 18);
		$nd_subdit_nama	 = trim(str_replace($nd_subdit_nip, '', $_POST['nd_subdit']));
		$f1_tujuan		 = $_POST['f1_tujuan'];

		$f2_latar_blkg	 = $_POST['f2_latar_blkg'];
		$f2_jnsrev1_a	 = $_POST['f2_jnsrev1_a'];
		$f2_jnsrev1_b	 = $_POST['f2_jnsrev1_b'];
		$f2_jnsrev1_c	 = $_POST['f2_jnsrev1_c'];
		$f2_jnsrev1_ket	 = $_POST['f2_jnsrev1_ket'];
		$f2_jnsrev2_a	 = $_POST['f2_jnsrev2_a'];
		$f2_jnsrev2_b	 = $_POST['f2_jnsrev2_b'];
		$f2_jnsrev2_c	 = $_POST['f2_jnsrev2_c'];
		$f2_jnsrev2_ket	 = $_POST['f2_jnsrev2_ket'];
		$f2_jnsrev3_a	 = $_POST['f2_jnsrev3_a'];
		$f2_jnsrev3_b	 = $_POST['f2_jnsrev3_b'];
		$f2_jnsrev3_c	 = $_POST['f2_jnsrev3_c'];
		$f2_jnsrev3_ket	 = $_POST['f2_jnsrev3_ket'];
		$f2_jnsrev4_a	 = $_POST['f2_jnsrev4_a'];
		$f2_jnsrev4_b	 = $_POST['f2_jnsrev4_b'];
		$f2_jnsrev4_c	 = $_POST['f2_jnsrev4_c'];
		$f2_jnsrev4_ket	 = $_POST['f2_jnsrev4_ket'];
		$f2_jnsrev5_a	 = $_POST['f2_jnsrev5_a'];
		$f2_jnsrev5_b	 = $_POST['f2_jnsrev5_b'];
		$f2_jnsrev5_c	 = $_POST['f2_jnsrev5_c'];
		$f2_jnsrev5_ket	 = $_POST['f2_jnsrev5_ket'];
		$f2_jnsrev5_ket	 = $_POST['f2_jnsrev5_ket'];
		$f2_telaah_catatan	 = $_POST['f2_telaah_catatan'];

		$f3_dasarhukum	 = $_POST['f3_dasarhukum'];
		$f3_batasan_rev	 = $_POST['f3_batasan_rev'];
		$f3_jns_wenang	 = $_POST['f3_jns_wenang'];
		$f3_dok_usulan	 = $_POST['f3_dok_usulan'];
		$f3_dok_pendukung= $_POST['f3_dok_pendukung'];
		$f3_ket_catatan	 = $_POST['f3_ket_catatan'];

		$pagu_rm_mula	 = (int)str_replace('.', '', $_POST['pagu_rm_mula']);
		$pagu_rm_jadi	 = (int)str_replace('.', '', $_POST['pagu_rm_jadi']);
		$pagu_pnbp_mula	 = (int)str_replace('.', '', $_POST['pagu_pnbp_mula']);
		$pagu_pnbp_jadi	 = (int)str_replace('.', '', $_POST['pagu_pnbp_jadi']);
		$pagu_phln_mula	 = (int)str_replace('.', '', $_POST['pagu_phln_mula']);
		$pagu_phln_jadi	 = (int)str_replace('.', '', $_POST['pagu_phln_jadi']);
		$pagu_sbsn_mula	 = (int)str_replace('.', '', $_POST['pagu_sbsn_mula']);
		$pagu_sbsn_jadi	 = (int)str_replace('.', '', $_POST['pagu_sbsn_jadi']);
		$pagu_pdpt_pajak_mula= (int)str_replace('.', '', $_POST['pagu_pdpt_pajak_mula']);
		$pagu_pdpt_pajak_jadi= (int)str_replace('.', '', $_POST['pagu_pdpt_pajak_jadi']);
		$pagu_pdpt_pnbp_mula = (int)str_replace('.', '', $_POST['pagu_pdpt_pnbp_mula']);
		$pagu_pdpt_pnbp_jadi = (int)str_replace('.', '', $_POST['pagu_pdpt_pnbp_jadi']);
		$pagu_hal_4_mula = (int)str_replace('.', '', $_POST['pagu_hal_4_mula']);
		$pagu_hal_4_jadi = (int)str_replace('.', '', $_POST['pagu_hal_4_jadi']);
		$f4_usulan_rev	 = $_POST['f4_usulan_rev'];
		$f4_catatan	 	 = $_POST['f4_catatan'];

		// Insert ke Table REVISI_ND for First Time !
		$query = $this->db->query("Select rev_id From revisi_nd Where rev_id='$rev_id'");
		if ($query->num_rows() == 0) $this->db->query("Insert Into revisi_nd (rev_id) Values ('$rev_id')");

		// Update VALUE pada Table REVISI_ND sesuai REV_ID
		$sql0 = "nd_dit_pgs='$nd_dit_pgs', nd_subdit_pgs='$nd_subdit_pgs', nd_lamp_jml='$nd_lamp_jml', nd_hal=\"$nd_hal\", nd_tgl='$nd_tgl', nd_subdit_nip='$nd_subdit_nip', nd_subdit_nama='$nd_subdit_nama'";
		$sql1 = "f1_tujuan=\"$f1_tujuan\", f2_latar_blkg=\"$f2_latar_blkg\", f2_telaah_catatan=\"$f2_telaah_catatan\"";
		$sql2 = "f2_jnsrev1_a=\"$f2_jnsrev1_a\", f2_jnsrev1_b=\"$f2_jnsrev1_b\", f2_jnsrev1_c=\"$f2_jnsrev1_c\", f2_jnsrev1_ket=\"$f2_jnsrev1_ket\", f2_jnsrev2_a=\"$f2_jnsrev2_a\", f2_jnsrev2_b=\"$f2_jnsrev2_b\", f2_jnsrev2_c=\"$f2_jnsrev2_c\", f2_jnsrev2_ket=\"$f2_jnsrev2_ket\", f2_jnsrev3_a=\"$f2_jnsrev3_a\", f2_jnsrev3_b=\"$f2_jnsrev3_b\", f2_jnsrev3_c=\"$f2_jnsrev3_c\", f2_jnsrev3_ket=\"$f2_jnsrev3_ket\",
		f2_jnsrev3_a=\"$f2_jnsrev4_a\", f2_jnsrev3_b=\"$f2_jnsrev4_b\", f2_jnsrev3_c=\"$f2_jnsrev4_c\", f2_jnsrev3_ket=\"$f2_jnsrev4_ket\", f2_jnsrev4_a=\"$f2_jnsrev5_a\", f2_jnsrev4_b=\"$f2_jnsrev5_b\", f2_jnsrev4_c=\"$f2_jnsrev5_c\", f2_jnsrev4_ket=\"$f2_jnsrev5_ket\"";
		$sql3 = "f3_dasarhukum='$f3_dasarhukum', f3_batasan_rev='$f3_batasan_rev', f3_jns_wenang='$f3_jns_wenang', f3_dok_usulan='$f3_dok_usulan', f3_dok_pendukung='$f3_dok_pendukung', f3_ket_catatan='$f3_ket_catatan'";
		$sql4 = "pagu_rm_mula=$pagu_rm_mula, pagu_rm_jadi=$pagu_rm_jadi, pagu_pnbp_mula=$pagu_pnbp_mula, pagu_pnbp_jadi=$pagu_pnbp_jadi, pagu_phln_mula=$pagu_phln_mula, pagu_phln_jadi=$pagu_phln_jadi, pagu_sbsn_mula=$pagu_sbsn_mula, pagu_sbsn_jadi=$pagu_sbsn_jadi, pagu_pdpt_pajak_mula=$pagu_pdpt_pajak_mula, pagu_pdpt_pajak_jadi=$pagu_pdpt_pajak_jadi, pagu_pdpt_pnbp_mula=$pagu_pdpt_pnbp_mula,  pagu_pdpt_pnbp_jadi=$pagu_pdpt_pnbp_jadi, pagu_hal_4_mula=$pagu_hal_4_mula, pagu_hal_4_jadi=$pagu_hal_4_jadi, f4_usulan_rev='$f4_usulan_rev', f4_catatan=\"$f4_catatan\"";

		$msg = $this->db->query( "Update revisi_nd Set $sql0, $sql1, $sql2, $sql3, $sql4, iduser='$iduser', tglupdate=current_timestamp() Where rev_id='$rev_id'" );
		return $msg;
	}

	function get_jenis() {
		$query = $this->db->query("Select * From revisi_nd_jenis Order By 1");
		$data['tabel'] = $this->fc->ToArr($query->result_array(), 'kdjnsrev');
		return $data;
	}

	function get_data_1($rev_id){
		$query=$this->db->query("Select * From revisi_nd Where rev_id='$rev_id' ");
	 	return $query->row_array();
	}

	function get_data_2($rev_id){
		$query=$this->db->query("Select * From revisi Where rev_id='$rev_id' ");
	 	return $query->row_array();
	}

	function save_jenis() {
		$aksi  = $_POST['aksi'];
		$isian = $_POST['isian'];

		// Konversi ARRAY ISIAN ke Variabel
		$kdjnsrev = $isian[0];
		$nmjnsrev = $isian[1];

		if ($aksi=='Rekam') $this->db->query( "Insert Into revisi_nd_jenis (kdjnsrev, nmjnsrev) Values ('$kdjnsrev', '$nmjnsrev')" );
		if ($aksi=='Ubah')  $this->db->query( "Update revisi_nd_jenis Set nmjnsrev='$nmjnsrev' Where kdjnsrev='$kdjnsrev'" );
		if ($aksi=='Hapus') $this->db->query( "Delete From revisi_nd_jenis where kdjnsrev='$kdjnsrev'" );

		return implode('#', $isian);
	}

	function get_user() {
		$query = $this->db->query("Select * From revisi_wenang Order By 1,2");
		$data['tabel'] = $this->fc->ToArr($query->result_array(), 'kdso');

		$query = $this->db->query("Select kdso, nmso From t_so");
		$data['so'] = $this->fc->ToArr($query->result_array(), 'kdso');

		$query = $this->db->query("Select iduser, kdso From t_user Where idusergroup Like '%501%' Or idusergroup Like '%502%'");
		$data['user'] = $this->fc->ToArr($query->result_array(), 'kdso');

		// Referensi KL
		$dbref = $this->load->database("ref$this->thang", TRUE);
		$query = $dbref->query("Select kddept, nmdept From t_dept");
		$data['dept'] = $this->fc->ToArr($query->result_array(), 'kddept');
		return $data;
	}

	function save_user() {
		$aksi  = $_POST['aksi'];
		$isian = $_POST['isian'];

		// Konversi ARRAY ISIAN ke Variabel
		$tahun 	= $isian[0];
		$kdso 	= $isian[1];
		$kddept = $isian[2];
		$cp1 	= $isian[3];
		$hp1 	= $isian[4];
		$cp2 	= $isian[5];
		$hp2 	= $isian[6];
		$intern	= $isian[7];
		$email	= $isian[8];
		$phone	= $isian[9];

		if ($aksi=='Rekam') $this->db->query( "Insert Into revisi_wenang (tahun, kdso, kddept, cp1, hp1, cp2, hp2, intern, email, phone) Values ('$tahun', '$kdso', '$kddept', '$cp1', '$hp1', '$cp2', '$hp2', '$intern', '$email', '$phone')" );
		if ($aksi=='Ubah')  $this->db->query( "Update revisi_wenang Set kddept='$kddept', cp1='$cp1', hp1='$hp1', cp2='$cp2', hp2='$hp2', intern='$intern', email='$email', phone='$phone' Where kdso='$kdso'" );
		if ($aksi=='Hapus') $this->db->query( "Delete From revisi_wenang where kdso='$kdso'" );

		return implode('#', $isian);
	}

	function get_mitra() {
		$query = $this->db->query("Select *, Concat(kddept,'.',kdunit) kdkey, '' nmunit, '' nmdept From revisi_mitra Order By kdkey");
		$hasil = $this->fc->ToArr($query->result_array(), 'kdkey');

		// Uraian KL dan Unit Eselon I
		$dbref = $this->load->database("ref$this->thang", TRUE);
		$query = $dbref->query("Select kddept, nmdept From t_dept Where kddept ". $this->fc->Invar($hasil, 'kddept') );
		$dept  = $this->fc->ToArr($query->result_array(), 'kddept');
		foreach ($hasil as $key=>$value) if (array_key_exists($value['kddept'], $dept)) $hasil[$key]['nmdept'] = $dept[$value['kddept']]['nmdept'];
		$query = $dbref->query("Select Concat(kddept,'.',kdunit) kdkey, nmunit From t_unit Where Concat(kddept,'.',kdunit) ". $this->fc->Invar($hasil, 'kdkey') );
		$unit  = $this->fc->ToArr($query->result_array(), 'kdkey');
		foreach ($hasil as $key=>$value) if (array_key_exists($key, $unit)) $hasil[$key]['nmunit'] = $unit[$key]['nmunit'];

		// No Repeating
		$nmdept = '';
		foreach ($hasil as $key=>$value) {
			if ($nmdept == $value['nmdept']) $hasil[$key]['nmdept'] = '';
			else $nmdept = $value['nmdept'];
		}
		$data['tabel'] = $hasil;
		return $data;
	}

	function save_mitra() {
		$aksi  = $_POST['aksi'];
		$isian = $_POST['isian'];

		// Konversi ARRAY ISIAN ke Variabel
		$tahun 	= $isian[0];
		$kddept = $isian[1];
		$kdunit = $isian[2];
		$email 	= $isian[3];
		$cp		= $isian[4];
		$phone	= $isian[5];

		if ($aksi=='Rekam') $this->db->query( "Insert Into revisi_mitra (tahun, kddept, kdunit, cp, email, phone) Values ('$tahun', '$kddept', '$kdunit', '$cp', '$email', '$phone')" );
		if ($aksi=='Ubah')  $this->db->query( "Update revisi_mitra Set cp='$cp', email='$email', phone='$phone' Where tahun='$tahun' And kddept='$kddept' And kdunit='$kdunit'" );
		if ($aksi=='Hapus') $this->db->query( "Delete From revisi_mitra Where tahun='$tahun' And kddept='$kddept' And kdunit='$kdunit'" );
		return implode('#', $isian);
	}

	function get_redaksi() {
		$query = $this->db->query("Select * From revisi_redaksi Order By 1,2");
		$data['tabel'] = $this->fc->ToArr($query->result_array(), 'tahap');
		return $data;
	}

	function save_redaksi() {
		$aksi  = $_POST['aksi'];
		$isian = $_POST['isian'];

		// Konversi ARRAY ISIAN ke Variabel
		$tahun 			= $isian[0];
		$tahap 			= $isian[1];
		$email_subject 	= $isian[2];
		$email_narasi	= $isian[3];
		$sms_narasi		= $isian[4];

		if ($aksi=='Rekam') $this->db->query( "Insert Into revisi_redaksi (tahun, tahap, email_subject, email_narasi, sms_narasi) Values ('$tahun', '$tahap', '$email_subject', '$email_narasi', '$sms_narasi')" );
		if ($aksi=='Ubah')  $this->db->query( "Update revisi_redaksi Set email_subject='$email_subject', email_narasi='$email_narasi', sms_narasi='$sms_narasi' Where tahun='$tahun' And tahap='$tahap'" );
		if ($aksi=='Hapus') $this->db->query( "Delete From revisi_redaksi Where tahun='$tahun' And tahap='$tahap'" );
		return implode('#', $isian);
	}

	function nd_to_text_simpan($rev_id){
		$query 	   = $this->db->query("Select nd_editor From revisi_nd Where rev_id='$rev_id' ");
		$text      = $query->row_array();
		return $text['nd_editor'];
	}

	function nd_to_text( $rev_id ) {
		$nd  = $this->get_data($rev_id);
		$nd1 = $this->get_data_1($rev_id);
		$nd2 = $this->get_data_2($rev_id);

		// echo "<pre>";print_r($nd); exit;

		$str   = $nd1['f3_ket_catatan'];
		$pot   = explode("\n", $str);

		$i=0;
		if($nd['nota']['f2_jnsrev1_a'] !=='' OR $nd['nota']['f2_jnsrev1_b'] !=='' OR $nd['nota']['f2_jnsrev1_c'] !=='') {
			echo ++$i.') '. 'Perubahan Anggaran Termasuk Perubahan Rinciannya' ;
		  }
			$psml = $nd['nota']['pagu_rm_mula']+$nd['nota']['pagu_pnbp_mula']+$nd['nota']['pagu_phln_mula']+$nd['nota']['pagu_sbsn_mula'];
			$psjd = $nd['nota']['pagu_rm_jadi']+$nd['nota']['pagu_pnbp_jadi']+$nd['nota']['pagu_phln_jadi']+$nd['nota']['pagu_sbsn_jadi'];
			$tarsml = $nd['nota']['pagu_pdpt_pajak_mula']+$nd['nota']['pagu_pdpt_pnbp_mula'];
			$tarsjd = $nd['nota']['pagu_pdpt_pajak_jadi']+$nd['nota']['pagu_pdpt_pnbp_jadi'];

		$html = '
		<div class="F4" style="line-height:13pt">
			<div style="border-bottom:1px solid;">
				<img src="'.base_url('files/images/logo_kemenkeu.jpg').'" width="85px">
				<div style="margin: 10px 0px 7px 85px">
					<div class="text-center" styles="display:inline"><strong><span style="font-size:13pt;">KEMENTERIAN KEUANGAN REPUBLIK INDONESIA&nbsp;</span></strong></div>
					<div class="text-center"><strong>DIREKTORAT JENDERAL ANGGARAN</strong></div>
					<div class="text-center" style="padding-bottom:0"><strong>'. strtoupper(substr ($nd['revisi']['direktur'], 9)) .'</strong></div>

					<div class="text-center" style="line-height: 10px;padding-top:3px"><span style="font-size:7pt; ">
						'.$nd['revisi']['header1'].' <br /> '.$nd['revisi']['header2'].'</span></div>
				</div>
			</div>

			<div class="text-center" style="padding:17px">
				NOTA DINAS <br> NOMOR&nbsp;ND-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;/AG.'
					.$nd['revisi']['kdsosurat'].'/'.date('Y') .'
			</div>


			<div style="border-bottom:1px solid; padding-bottom:5px">

				<div class="hnd">Yth</div><span style="float:left">:</span>
				<div class="hnd_isi">'.str_replace('Direktorat','',$nd['revisi']['direktur']).'</div>

				<div class="hnd">Dari</div><span style="float:left">:</span>
				<div class="hnd_isi">'.$nd['revisi']['subdit'].'</div>

				<div class="hnd">Sifat</div><span style="float:left">:</span>
				<div class="hnd_isi">Segera</div>

				<div class="hnd">Lampiran</div><span style="float:left">:</span>
				<div class="hnd_isi">'.$nd['nota']['nd_lamp_jml'].' berkas</div>

				<div class="hnd">Hal</div><span style="float:left">:</span>
				<div class="hnd_isi">'.$nd['nota']['nd_hal'].'</div>

				<div class="hnd">Tanggal</div><span style="float:left">:</span>
				<div class="hnd_isi">'."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$nd['nota']['nd_tgl'].'</div>
			</div>
					';
		$html .='
			<br />

			<div style="text-indent: 2.5em; text-align: justify;">
				<p>Sehubungan dengan usulan revisi anggaran pada '.$nd['revisi']['nmunit'].', '.str_replace("Dan","dan",ucwords(strtolower($nd['revisi']['nmdept']))).', dapat kami sampaikan hal-hal sebagai berikut:</p>
			</div>

			<ol style="padding-left: 20px; list-style-type: upper-alpha;"">
				<li style="text-align: justify;">Tujuan<br />
					'.$nd['nota']['f1_tujuan'].'
				</li>
					';
		$html .='
				<li style="padding-top: 5px; text-align: justify;">Pembahasan
					<ol style="padding-left: 20px; padding-top: 5px;">
						<li style="text-align: justify;">DHP Sebelum Revisi<br />
							'.$nd['nota']['f2_latar_blkg'].'
						</li>
						<li style="padding-top: 5px; text-align: justify;">Revisi yang Diusulkan<br />
							<ol style="padding-left: 20px; list-style-type: lower-alpha;">

					';
							 if($nd['nota']['f2_jnsrev1_a'] !=='' OR $nd['nota']['f2_jnsrev1_b'] !=='' OR $nd['nota']['f2_jnsrev1_c'] !==''){
							 	$html .=  "<li>Perubahan anggaran termasuk perubhan rinciannya.</li><ol style='padding-left: 20px; list-style-type: lower-roman;'>";
									if ($nd['nota']['f2_jnsrev1_a']) $html .= '<li>'.substr($nd['nota']['f2_jnsrev1_a'],4,strlen($nd['nota']['f2_jnsrev1_a'])-4).'</li>';
									if ($nd['nota']['f2_jnsrev1_b']) $html .= '<li>'.substr($nd['nota']['f2_jnsrev1_b'],4,strlen($nd['nota']['f2_jnsrev1_b'])-4).'</li>';
									if ($nd['nota']['f2_jnsrev1_c']) $html .= '<li>'.substr($nd['nota']['f2_jnsrev1_c'],4,strlen($nd['nota']['f2_jnsrev1_c'])-4).'</li>';
									$html .= '</ol>';}
							 if($nd['nota']['f2_jnsrev2_a'] !=='' OR $nd['nota']['f2_jnsrev2_b'] !=='' OR $nd['nota']['f2_jnsrev2_c'] !==''){
							 	$html .=  "<li>Pergeseran anggaran termasuk perubahan rincian dalam hal pagu tetap.</li><ol style='padding-left: 20px; list-style-type: lower-roman;'>";
									if ($nd['nota']['f2_jnsrev2_a']) $html .= '<li>'.substr($nd['nota']['f2_jnsrev2_a'],4,strlen($nd['nota']['f2_jnsrev2_a'])-4).'</li>';
									if ($nd['nota']['f2_jnsrev2_b']) $html .= '<li>'.substr($nd['nota']['f2_jnsrev2_b'],4,strlen($nd['nota']['f2_jnsrev2_b'])-4).'</li>';
									if ($nd['nota']['f2_jnsrev2_c']) $html .= '<li>'.substr($nd['nota']['f2_jnsrev2_c'],4,strlen($nd['nota']['f2_jnsrev2_c'])-4).'</li>';
								$html .= '</ol>';}
							 if($nd['nota']['f2_jnsrev3_a'] !=='' OR $nd['nota']['f2_jnsrev3_b'] !=='' OR $nd['nota']['f2_jnsrev3_c'] !==''){
							 	$html .=  "<li>Revisi administrasi.</li><ol style='padding-left: 20px; list-style-type: lower-roman;'>";
									if ($nd['nota']['f2_jnsrev3_a']) $html .= '<li>'.substr($nd['nota']['f2_jnsrev3_a'],4,strlen($nd['nota']['f2_jnsrev3_a'])-4).'</li>';
									if ($nd['nota']['f2_jnsrev3_b']) $html .= '<li>'.substr($nd['nota']['f2_jnsrev3_b'],4,strlen($nd['nota']['f2_jnsrev3_b'])-4).'</li>';
									if ($nd['nota']['f2_jnsrev3_c']) $html .= '<li>'.substr($nd['nota']['f2_jnsrev3_c'],4,strlen($nd['nota']['f2_jnsrev3_c'])-4).'</li>';
								$html .= '</ol>';}
							 if($nd['nota']['f2_jnsrev4_a'] !=='' OR $nd['nota']['f2_jnsrev4_b'] !=='' OR $nd['nota']['f2_jnsrev4_c'] !==''){
							 	$html .=  "<li>Usulan revisi anggaran yang memuat substansi kewenangan Direktorat Pelaksanaan Anggaran Ditjen Perbedaharaan.</li><ol style='padding-left: 20px; list-style-type: lower-roman;'>";
									if ($nd['nota']['f2_jnsrev4_a']) $html .= '<li>'.substr($nd['nota']['f2_jnsrev4_a'],4,strlen($nd['nota']['f2_jnsrev4_a'])-4).'</li>';
									if ($nd['nota']['f2_jnsrev4_b']) $html .= '<li>'.substr($nd['nota']['f2_jnsrev4_b'],4,strlen($nd['nota']['f2_jnsrev4_b'])-4).'</li>';
									if ($nd['nota']['f2_jnsrev4_c']) $html .= '<li>'.substr($nd['nota']['f2_jnsrev4_c'],4,strlen($nd['nota']['f2_jnsrev4_c'])-4).'</li>';
									$html .= '</ol>';}
							 if($nd['nota']['f2_jnsrev5_a'] !=='' OR $nd['nota']['f2_jnsrev5_b'] !=='' OR $nd['nota']['f2_jnsrev5_c'] !==''){
							 	$html .=  "<li>Usulan revisi anggaran yang memuat substansi kewenangan Kantor WIlayah Ditjen Perbendaharaan.</li><ol style='padding-left: 20px; list-style-type: lower-roman;'>";
									if ($nd['nota']['f2_jnsrev5_a']) $html .= '<li>'.substr($nd['nota']['f2_jnsrev5_a'],4,strlen($nd['nota']['f2_jnsrev5_a'])-4).'</li>';
									if ($nd['nota']['f2_jnsrev5_b']) $html .= '<li>'.substr($nd['nota']['f2_jnsrev5_b'],4,strlen($nd['nota']['f2_jnsrev5_b'])-4).'</li>';
									if ($nd['nota']['f2_jnsrev5_c']) $html .= '<li>'.substr($nd['nota']['f2_jnsrev5_c'],4,strlen($nd['nota']['f2_jnsrev5_c'])-4).'</li>';
									$html .= '</ol>';}
		$html .='
							</ol>
						</li>
						<li style="padding-top: 5px; text-align: justify;">Dokumen Pendukung yang Disampaikan<br />
							<ol style="list-style-type: lower-alpha; padding-left: 20px;">
								<li style="text-align: justify;">
					';
								if ($nd2['doc_usulan_revisi']) $html .= '<li style="text-align: justify;"> Surat usulan revisi yang ditandatangani pejabat eselon I</li> ';
								if ($nd2['doc_matrix']) $html .= '<li style="text-align: justify;"> Matriks Perubahan (semula menjadi) </li>';
								if ($nd2['doc_adk']) $html .= '<li style="text-align: justify;"> Arsip Data Komputer (ADK) RKA-K/L DIPA revisi Satker </li>';
								if ($nd2['doc_dukung_lainnya']) $html .= '<li style="text-align: justify;"> Dokumen pendukung usul revisi</li>';
		$html .='
								</li>
							</ol>
						</li>
						<li style="padding-top: 5px; text-align: justify;">Penelaahan Usulan Revisi Aggaran<br />
					';
						if($nd2['t3_penelaahan_tgl'] =='') {
							$html.= 'Terhadap usulan revisi, tidak dilakukan Penelaahan';}
						else {
							$html.= 'Penelaahan usulan revisi anggaran telah dilaksanakan pada tanggal  '.$this->fc->idtgl($nd2['t3_penelaahan_tgl'],'tglfull');
							if (empty($nd['nota']['f2_telaah_catatan'])) { $html.= '.';} else {$html.= ', dengan kesepakatan sebagai berikut:<br />';}
							$html.='
							<ol style="list-style-type: lower-alpha; padding-left: 20px;">
								<li style="text-align: justify;">'.$nd['nota']['f2_telaah_catatan'].'</li>
							</ol>'
							;}

		$html .='
					</ol>
				</li>

				<li style="padding-top: 5px;">Pendapat
					<ol style="padding-left: 20px; padding-top: 5px;">
						<li style="text-align: justify;">Dasar Hukum<br />'.$nd['nota']['f3_dasarhukum'].'
					';
		$html .='<li style="padding-top: 5px; text-align: justify;">';

		if( $nd1['f4_usulan_rev']=='usulan revisi tidak dapat ditetapkan' )
			{$stat_rev1= 'Penolakan'; $stat_rev2= 'Menolak';}
		elseif ($nd1['f4_usulan_rev']=='usulan revisi dapat ditetapkan')
			{$stat_rev1= 'Penetapan'; $stat_rev2= 'Menetapkan';}
		elseif($nd1['f4_usulan_rev']=='usulan revisi dapat ditetapkan sebagian, dan sebagian ditolak')
			{$stat_rev1= 'Penetapan dan Penolakan'; $stat_rev2= 'Menetapkan sebagian ';}
		else {
			{$stat_rev1= ''; $stat_rev2= '';}
		}

		$html .=	' '.$stat_rev1.' Revisi Anggaran<br />Berdasarkan hal-hal tersebut diatas, maka '.$nd1['f4_usulan_rev'].'.
						<br />'.$nd['nota']['f3_ket_catatan'].'</li>
					</ol>
				</li>

				<li style="padding-top: 5px; text-align: justify;">Rekomendasi
					<ol style="padding-left: 20px; padding-top: 5px;">
						<li style="padding-top: 5px; text-align: justify;">'.$stat_rev2.' Revisi Anggaran pada '.$nd['revisi']['nmunit'].', '.str_replace("Dan","dan",ucwords(strtolower($nd['revisi']['nmdept']))).'.</li>
						<li style="padding-top: 5px; text-align: justify;">Dengan '.strtolower($stat_rev1).' revisi anggaran dimaksud, DHP RKA-K/L menjadi sebagai berikut:<br />
						<div style="text-align:right; font-size:9pt;margin-top:5px;">Ribuan rupiah</div>
						<table border="1px" style="width:100%;margin-bottom: 10px;">
						  <thead >
							 <tr>
								<td style="font-size: 10pt; font-weight: bold; text-align: center; background: #ECF0F5; padding: 3px;">Uraian</td>
								<td style="font-size: 10pt; font-weight: bold; text-align: center; background: #ECF0F5; padding: 3px;">Semula</td>
								<td style="font-size: 10pt; font-weight: bold; text-align: center; background: #ECF0F5; padding: 3px;">Menjadi</td>
								<td style="font-size: 10pt; font-weight: bold; text-align: center; background: #ECF0F5; padding: 3px;">Selisih</td></b>
							 </tr>
						  </thead>
						  <tbody style="font-variant-numeric: all;line-height:15px;">
							 <tr>
								<td style="font-size: 10pt; padding: 3px; text-align: left;"><b>Pagu</b></td>
								<td style="font-size: 10pt; padding: 3px; text-align: right;">'.number_format($psml).'</td>
								<td style="font-size: 10pt; padding: 3px; text-align: right;">'.number_format($psjd).'</td>
								<td style="font-size: 10pt; padding: 3px; text-align: right;">'.number_format($psjd-$psml).'</td>
							 </tr>
							 <tr>
								<td colspan="4" style="font-size: 10pt; padding: 3px; text-align: left;">Rincian Sumber Dana :</td>
							 </tr>
							 <tr>
								<td style="font-size: 10pt; padding: 3px; text-align: left;">RM</td>
								<td style="font-size: 10pt; padding: 3px; text-align: right;">'.number_format($nd['nota']['pagu_rm_mula']).'</td>
								<td style="font-size: 10pt; padding: 3px; text-align: right;">'.number_format($nd['nota']['pagu_rm_jadi']).'</td>
								<td style="font-size: 10pt; padding: 3px; text-align: right;">'.number_format($nd['nota']['pagu_rm_jadi']-$nd['nota']['pagu_rm_mula']).'</td>
							 </tr>
							 <tr>
								<td style="font-size: 10pt; padding: 3px; text-align: left;">PNBP/BLU</td>
								<td style="font-size: 10pt; padding: 3px; text-align: right;">'.number_format($nd['nota']['pagu_pnbp_mula']).'</td>
								<td style="font-size: 10pt; padding: 3px; text-align: right;">'.number_format($nd['nota']['pagu_pnbp_jadi']).'</td>
								<td style="font-size: 10pt; padding: 3px; text-align: right;">'.number_format($nd['nota']['pagu_pnbp_jadi']-$nd['nota']['pagu_pnbp_mula']).'</td>
							 </tr>
							 <tr>
								<td style="font-size: 10pt; padding: 3px; text-align: left;">PHLN/PHDN</td>
								<td style="font-size: 10pt; padding: 3px; text-align: right;">'.number_format($nd['nota']['pagu_phln_mula']).'</td>
								<td style="font-size: 10pt; padding: 3px; text-align: right;">'.number_format($nd['nota']['pagu_phln_jadi']).'</td>
								<td style="font-size: 10pt; padding: 3px; text-align: right;">'.number_format($nd['nota']['pagu_phln_jadi']-$nd['nota']['pagu_phln_mula']).'</td>
							 </tr>
							 <tr>
								<td style="font-size: 10pt; padding: 3px; text-align: left;">SBSN-PBS</td>
								<td style="font-size: 10pt; padding: 3px; text-align: right;">'.number_format($nd['nota']['pagu_sbsn_mula']).'</td>
								<td style="font-size: 10pt; padding: 3px; text-align: right;">'.number_format($nd['nota']['pagu_sbsn_jadi']).'</td>
								<td style="font-size: 10pt; padding: 3px; text-align: right;">'.number_format($nd['nota']['pagu_sbsn_jadi']-$nd['nota']['pagu_sbsn_mula']).'</td>
							 </tr>
							 <tr>
								<td style="font-size: 10pt; padding: 3px; text-align: left;"><b>Target Pendapatan</b></td>
								<td style="font-size: 10pt; padding: 3px; text-align: right;">'.number_format($tarsml).'</td>
								<td style="font-size: 10pt; padding: 3px; text-align: right;">'.number_format($tarsjd).'</td>
								<td style="font-size: 10pt; padding: 3px; text-align: right;">'.number_format($tarsjd-$tarsml).'</td>
							 </tr>
							 <tr>
								<td style="font-size: 10pt; padding: 3px; text-align: left;">Perpajakan</td>
								<td style="font-size: 10pt; padding: 3px; text-align: right;">'.number_format($nd['nota']['pagu_pdpt_pajak_mula']).'</td>
								<td style="font-size: 10pt; padding: 3px; text-align: right;">'.number_format($nd['nota']['pagu_pdpt_pajak_jadi']).'</td>
								<td style="font-size: 10pt; padding: 3px; text-align: right;">'.number_format($nd['nota']['pagu_pdpt_pajak_jadi']-$nd['nota']['pagu_pdpt_pajak_mula']).'</td>
							 </tr>
							 <tr>
								<td style="font-size: 10pt; padding: 3px; text-align: left;">PNBP</td>
								<td style="font-size: 10pt; padding: 3px; text-align: right;">'.number_format($nd['nota']['pagu_pdpt_pnbp_mula']).'</td>
								<td style="font-size: 10pt; padding: 3px; text-align: right;">'.number_format($nd['nota']['pagu_pdpt_pnbp_jadi']).'</td>
								<td style="font-size: 10pt; padding: 3px; text-align: right;">'.number_format($nd['nota']['pagu_pdpt_pnbp_jadi']-$nd['nota']['pagu_pdpt_pnbp_mula']).'</td>
							 </tr>
							 <tr>
								<td style="font-size: 10pt; padding: 3px; text-align: left;"><b>Catatan Halaman IV DIPA</b></td>
								<td style="font-size: 10pt; padding: 3px; text-align: right;">'.number_format($nd['nota']['pagu_hal_4_mula']).'</td>
								<td style="font-size: 10pt; padding: 3px; text-align: right;">'.number_format($nd['nota']['pagu_hal_4_jadi']).'</td>
								<td style="font-size: 10pt; padding: 3px; text-align: right;">'.number_format($nd['nota']['pagu_hal_4_jadi']-$nd['nota']['pagu_hal_4_mula']).'</td>
							 </tr>
						  </tbody>
						</table>
						'.$nd['nota']['f4_catatan'].'</li>
					</ol>
				</li>

			</ol>

			<div style="text-indent: 2.5em; text-align: justify;">
				<p>Demikian disampaikan, mohon penetapan dan arahan lebih lanjut.</p>
			</div>';

		if ( ($nd['nota']['nd_dit_pgs'])=='Plt.') $html.= $nd['revisi']['subdit']; else $html.= "";
		$html .= '<div class="text-left ttd" style="padding-top:75px">'.$nd1['nd_subdit_nama'];
		// $html .= '<br>NIP '.substr($nd1['nd_subdit_nip'], 0,8);
		// $html .= substr($nd1['nd_subdit_nip'], 8,6);
		// $html .= substr($nd1['nd_subdit_nip'], -4,1);
		// $html .= substr($nd1['nd_subdit_nip'], -3);
		$html .= '</div>';
		$html .= '<br />Tembusan:<ol style="padding-left:16px"><li>Direktur Jenderal Anggaran</li></ol>
		</div>';
		return $html;
	}

	function get_status($rev_id){
		$query=$this->db->query("Select f4_usulan_rev From revisi_nd Where rev_id='$rev_id' ");
	 	return $query->row_array();
	}

	function tolak_to_text( $rev_id ) {
		$nd  = $this->get_data($rev_id);

		// echo "<pre>";print_r($nd); exit;

		$html = '
		<div class="F4" style="line-height:13pt;">
			<div style="border-bottom:1px solid;">
				<img src="'.base_url('files/images/logo_kemenkeu.jpg').'" width="85px">
				<div style="margin: 10px 0px 7px 85px; padding-top:13px;">
					<div class="text-center" styles="display:inline;">
						<strong><span style="font-size:14pt;">KEMENTERIAN KEUANGAN REPUBLIK INDONESIA</span></strong></div>
					<div class="text-center" style="font-size:13pt;"><strong>DIREKTORAT JENDERAL ANGGARAN</strong></div>
					<div class="text-center" style="line-height: 10px;padding-top:7px"><span style="font-size:7pt; ">
						'.$nd['revisi']['header1'].' <br /> '.$nd['revisi']['header2'].'</span></div>
				</div>
			</div>


			<div style="padding-top:20px;">

				<div class="hnd">Nomor</div><span style="float:left">:</span>
				<div class="hnd_isi">S-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;/AG/'.date('Y').'
					<span class="pull-right">'.$nd['nota']['nd_tgl'].'</span>
				</div>
				<div class="hnd">Sifat</div><span style="float:left">:</span><div class="hnd_isi">Segera</div>
				<div class="hnd">Lampiran</div><span style="float:left">:</span><div class="hnd_isi">-</div>
				<div class="hnd">Hal</div><span style="float:left">:</span><div class="hnd_isi">Penolakan Usulan Revisi Anggaran</div>
				<br /><br />
				<div class="hny">Yth.</div><div class="hny_isi">'.$nd['revisi']['nmunit'].', '.str_replace("Dan","dan",ucwords(strtolower($nd['revisi']['nmdept']))).'</div>
				di Jakarta
			</div>
					';
		$html .='
			<br />
			<div style="text-indent: 2.5em; text-align: justify;">
				Sehubungan dengan usulan revisi anggaran pada '.$nd['revisi']['nmunit'].' Nomor ... tanggal ... hal ..., dengan ini disampaikan:
			</div>

			<ol style="padding-left:16px">
				<li style="text-align: justify;">
					Usulan Revisi Anggaran pada unit '.$nd['revisi']['nmunit'].', '.str_replace("Dan","dan",ucwords(strtolower($nd['revisi']['nmdept']))).' tidak dapat ditetapkan karena ...
				</li>
			</ol>
			<div style="text-indent: 2.5em; text-align: justify;">
				Demikian untuk dimaklumi.
			</div>
					';


		$html .= '<div class="text-left" style="padding-top:15px; padding-left:27em">a.n. Direktur Jenderal,</div>
		<div class="text-left" style="padding-left:28.9em">'.str_replace('Direktorat','',$nd['revisi']['direktur']).'<div style="padding-top:70px">Nama <br /> NIP </div>';
		$html .= '</div><br />Tembusan:<ol style="padding-left:16px"><li>Direktur Jenderal Anggaran</li></ol></div>';
		return $html;
	}

	function save_penolakan(){
		$rev_id = $_POST['rev_id'];
		$text 	= htmlentities($_POST['isian']);

		$this->db->query("Update revisi_nd Set spra_editor=\"$text\" Where rev_id='$rev_id' ");
		return $text;
	}

	function spra_to_text_simpan($rev_id){
		$query 	   = $this->db->query("Select spra_editor From revisi_nd Where rev_id='$rev_id' ");
		$text      = $query->row_array();
		return $text['spra_editor'];
	}

	function spra_to_text( $rev_id ) {
		$nd  = $this->get_data($rev_id);

		// echo "<pre>";print_r($nd); exit;

		$html = '
		<div class="F4" style="line-height:13pt;">
			<div style="border-bottom:1px solid;">
				<img src="'.base_url('files/images/logo_kemenkeu.jpg').'" width="85px">
				<div style="margin: 10px 0px 7px 85px; padding-top:13px;">
					<div class="text-center" styles="display:inline;">
						<strong><span style="font-size:14pt;">KEMENTERIAN KEUANGAN REPUBLIK INDONESIA</span></strong></div>
					<div class="text-center" style="font-size:13pt;"><strong>DIREKTORAT JENDERAL ANGGARAN</strong></div>
					<div class="text-center" style="line-height: 10px;padding-top:7px"><span style="font-size:7pt; ">
						'.$nd['revisi']['header1'].' <br /> '.$nd['revisi']['header2'].'</span></div>
				</div>
			</div>


			<div style="padding-top:20px;">

				<div class="hnd">Nomor</div><span style="float:left">:</span>
				<div class="hnd_isi">S-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;/AG/'.date('Y').'
					<span class="pull-right">'.$nd['nota']['nd_tgl'].'</span>
				</div>
				<div class="hnd">Sifat</div><span style="float:left">:</span><div class="hnd_isi">Segera</div>
				<div class="hnd">Lampiran</div><span style="float:left">:</span><div class="hnd_isi">-</div>
				<div class="hnd">Hal</div><span style="float:left">:</span><div class="hnd_isi">Pengesahan Revisi Anggaran</div>
				<br /><br />
				<div class="hny">Yth.</div><div class="hny_isi"><ol style="padding-left:0px"><li>'.$nd['revisi']['nmunit'].', '.str_replace('Dan','dan',ucwords(strtolower($nd['revisi']['nmdept']))).'.</li>
				<li>Direktur Jenderal Perbendaharaan c.q. Direktur Sistem Perbendaharaan.</li></ol></div>
				di Jakarta
			</div>
					';
		$html .='
			<br />
			<div style="text-indent: 2.5em; text-align: justify;">
				Sehubungan dengan surat '.$nd['revisi']['nmunit'].' Nomor ... tanggal ... hal ..., dengan ini disampaikan:
			</div>

			<ol style="padding-left:16px">
				<li style="text-align: justify;">
					Usulan Revisi Anggaran pada unit '.$nd['revisi']['nmunit'].', '.str_replace("Dan","dan",ucwords(strtolower($nd['revisi']['nmdept']))).' telah disahkan dan Database RKA-K/L DIPA pada Kementerian Keuangan telah diperbaharui.
				</li>
				<li style="text-align: justify;">
					Dengan pengesahan Revisi Anggaran ini, Kode Pengaman (Digital Stamp) DIPA Petikan yang digunakan sebagai dasar transaksi berubah sebagaimana terlampir.
				</li>
				<li style="text-align: justify;">
					Dalam rangka memenuhi kebutuhan administrasi, Kuasa Pengguna Anggaran dan Kepala KPPN agar mengunduh PDF File DIPA Petikan Revisi sebagai dasar untuk mencetak DIPA. Petikan Revisi sebagaimana tercantum dalam notifikasi terlampir.
				</li>

			</ol>
			<div style="text-indent: 2.5em; text-align: justify;">
				Demikian untuk dimaklumi.
			</div>
					';


		$html .= '<div class="text-left" style="padding-top:15px; padding-left:27em">a.n. Direktur Jenderal,</div>
		<div class="text-left" style="padding-left:28.9em">'.str_replace('Direktorat','',$nd['revisi']['direktur']).'<div style="padding-top:70px">Nama <br /> NIP </div>';
		$html .= '</div><br />Tembusan:<ol style="padding-left:16px">
				<li style="text-align: justify;">Menteri ...</li>
				<li style="text-align: justify;">Ketua Badan Pemeriksa Keuangan.</li>
				<li style="text-align: justify;">Direktur Jenderal Perbendaharaan c.q. Direktur Akuntansi dan Pelaporan Keuangan dan Direktur Pelaksanaan Anggaran.</li>
			</ol></div>';
		return $html;
	}

	function save_spra(){
		$rev_id = $_POST['rev_id'];
		$text 	= htmlentities($_POST['isian']);

		$this->db->query("Update revisi_nd Set spra_editor=\"$text\" Where rev_id='$rev_id' ");
		return $text;
	}

	function save_nd(){
		$rev_id = $_POST['rev_id'];
		$text   = htmlentities($_POST['isian']);

		$this->db->query("Update revisi_nd Set nd_editor=\"$text\" Where rev_id='$rev_id' ");
		return $text;
	}

	function generate_data($rev_id){
		$this->db->query("UPDATE revisi_nd SET nd_editor=NULL WHERE rev_id='$rev_id' ");
		return $text;
	}
}
