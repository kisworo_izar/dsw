<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Sbk_model extends CI_Model {

	public function get_grid() {
		$dbsbk = $this->load->database('sbk16', TRUE);
		$sbk16 = $this->get_sbk($dbsbk);

		$dbsbk = $this->load->database('sbk', TRUE);
		$sbk17 = $this->get_sbk($dbsbk);

		foreach ($sbk16 as $key=>$value) {
			if ( array_key_exists($key, $sbk17) and $value['level']==4) {
				$sbk16[$key]['exercise'] = $sbk17[$key]['jumlah'];
				$sbk16[$key]['persen']   = ($sbk16[$key]['exercise'] - $sbk16[$key]['jumlah']) / $sbk16[$key]['jumlah'] * 100;
			}
		}
		return $sbk16;
	}

	public function get_sbk( $dbsbk ) {
		$dbref = $this->load->database('ref', TRUE);

		// Data KL 
		$query = $dbsbk->query("Select distinct kddept kdkey, '' nmdept From d_output");
		$kl_0  = $this->fc->ToArr( $query->result_array(), 'kdkey');
		$query = $dbref->query("Select kddept kdkey, nmdept From t_dept where kddept " . $this->fc->InVar($kl_0, 'kdkey')); 
		$t_ref = $this->fc->ToArr( $query->result_array(), 'kdkey');
		$kl_0  = $this->fc->Left_Join( $kl_0, $t_ref, "kdkey, nmdept=nmdept" );

		// Data Unit Es.1
		$query = $dbsbk->query("Select distinct concat(kddept,'.',kdunit) kdkey, '' nmunit From d_output");
		$es_1  = $this->fc->ToArr( $query->result_array(), 'kdkey');
		$query = $dbref->query("Select concat(kddept,'.',kdunit) kdkey, nmunit From t_unit where concat(kddept,'.',kdunit) " . $this->fc->InVar($es_1, 'kdkey')); 
		$t_ref = $this->fc->ToArr( $query->result_array(), 'kdkey');
		$es_1  = $this->fc->Left_Join( $es_1, $t_ref, "kdkey, nmunit=nmunit" );


		// Data SBK dari D_ITEM
		$fld = "concat(kddept,'.',kdunit,'.',Left(kdsoutput,1),'.',kdgiat,'.',kdoutput,'.',Left(kdsoutput,1),'00')";
		$query = $dbsbk->query("Select $fld kdkey, concat(kdgiat,'.',kdoutput) kode, '' uraian, 0 vol, '' sat, Sum(jumlah) jumlah, 4 level From d_item Where kdstatus='2' And Left(kdsoutput,1) In ('K','M') Group By 1,5");
		$out_1 = $this->fc->ToArr( $query->result_array(), 'kdkey');

		// Uraian OUTPUT dan SATUAN dari T_OUTPUT
		$query = $dbref->query("Select concat(kdgiat,'.',kdoutput) kode, nmoutput, sat From t_output where concat(kdgiat,'.',kdoutput) " . $this->fc->InVar($out_1, 'kode')); 
		$t_ref = $this->fc->ToArr( $query->result_array(), 'kode');
		$out_1 = $this->fc->Left_Join( $out_1, $t_ref, "kode, uraian=nmoutput, sat=sat" );

		// Nilai VOLUME dari D_OUTPUT
		$query = $dbsbk->query("Select concat(kdgiat,'.',kdoutput) kode, vol From d_output where kdstatus='2' And concat(kdgiat,'.',kdoutput) " . $this->fc->InVar($out_1, 'kode')); 
		$t_ref = $this->fc->ToArr( $query->result_array(), 'kode');
		$out_1 = $this->fc->Left_Join( $out_1, $t_ref, "kode, vol=vol" );


		// Data SBSK dari D_ITEM
		$fld = "concat(kddept,'.',kdunit,'.',Left(kdsoutput,1),'.',kdgiat,'.',kdoutput,'.',kdsoutput)";
		$sql = "$fld kdkey, kdjendok,kddept,kdunit,kdgiat,kdoutput,kdsoutput,Sum(jumlah) jumlah";
		$query = $dbsbk->query("Select $fld kdkey, concat(kdgiat,'.',kdoutput,'.',kdsoutput) kode, '' uraian, 0 vol, '' sat, Sum(jumlah) jumlah, 4 level, concat(kdgiat,'.',kdoutput) exercise From d_item Where kdstatus='2' And Left(kdsoutput,1) In ('S','U') Group By 1,5");
		$sub_1 = $this->fc->ToArr( $query->result_array(), 'kdkey');

		// Nilai SATUAN dari D_OUTPUT
		$query = $dbref->query("Select concat(kdgiat,'.',kdoutput) exercise, sat From t_output where concat(kdgiat,'.',kdoutput) " . $this->fc->InVar($sub_1, 'exercise')); 
		$t_ref = $this->fc->ToArr( $query->result_array(), 'exercise');
		$sub_1 = $this->fc->Left_Join( $sub_1, $t_ref, "exercise, sat=sat" );

		// Uraian SUB OUTPUT dan Nilai VOLUME dari D_SOUTPUT
		$query = $dbsbk->query("Select concat(kdgiat,'.',kdoutput,'.',kdsoutput) kode, ursoutput, volsout From d_soutput where kdstatus='2' And concat(kdgiat,'.',kdoutput,'.',kdsoutput) " . $this->fc->InVar($sub_1, 'kode')); 
		$t_ref = $this->fc->ToArr( $query->result_array(), 'kode');
		$sub_1 = $this->fc->Left_Join( $sub_1, $t_ref, "kode, uraian=ursoutput, vol=volsout" );


		$sbk = array_merge($out_1, $sub_1);
		$sbk = $this->fc->array_index($sbk, 'kdkey');
		
		$dept=array(); $unit=array(); $tipe=array();
		$type = array('K'=>'SBK Total', 'M'=>'SBK Indeks', 'S'=>'SBSK Total', 'U'=>'SBSK Indeks');
		foreach ($sbk as $key=>$value) {
			$kdkey = substr($value['kdkey'],0,3);
			if ( !array_key_exists($kdkey, $dept) ) { $dept[$kdkey] = array('kdkey'=>$kdkey, 'kode'=>$kdkey, 'uraian'=>$kl_0[$kdkey]['nmdept'], 'vol'=>'', 'sat'=>'', 'jumlah'=>1, 'level'=>1, 'exercise'=>'', 'persen'=>''); }
				else { $dept[$kdkey]['jumlah'] += 1; }

			$kdkey = substr($value['kdkey'],0,6);
			if ( !array_key_exists($kdkey, $unit) ) { $unit[$kdkey] = array('kdkey'=>$kdkey, 'kode'=>$kdkey, 'uraian'=>$es_1[$kdkey]['nmunit'], 'vol'=>'', 'sat'=>'', 'jumlah'=>1, 'level'=>2, 'exercise'=>'', 'persen'=>''); }
				else { $unit[$kdkey]['jumlah'] += 1; }

			$kdkey = substr($value['kdkey'],0,8);
			if ( !array_key_exists($kdkey, $tipe) ) { $tipe[$kdkey] = array('kdkey'=>$kdkey, 'kode'=>'', 'uraian'=>$type[ substr($value['kdkey'],7,1) ], 'vol'=>'', 'sat'=>'', 'jumlah'=>1, 'level'=>3, 'exercise'=>'', 'persen'=>''); }
				else { $tipe[$kdkey]['jumlah'] += 1; }
		}

		$hasil = array_merge($dept, $unit, $tipe, $sbk);
		$hasil = $this->fc->array_index($hasil, 'kdkey');
 		return $hasil;
	}

	public function get_pok( $dbsbk, $output ) {
		$kode  = explode('.', $output);
		$hasil = $this->rkakl_model->get_pagu( $dbsbk, $kode[0] );

		if ( count($kode)==2 ) {
			foreach ($hasil as $key=>$value) {
				if ( substr($value['kdkey'],10,8) != $output ) unset( $hasil[$key] );
			}
		}
		if ( count($kode)==3 ) {
			foreach ($hasil as $key=>$value) {
				if ( substr($value['kdkey'],10,12) != $output ) unset( $hasil[$key] );
			}
		}
		return $hasil;
	}

}
