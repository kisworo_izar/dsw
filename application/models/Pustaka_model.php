<?php
class Pustaka_model extends CI_Model {
	public function get_data() {
		$whr  = "";
		$cari = $this->session->userdata('cari');

		if ($cari) {
			$whr = " and " ;
			$arr = explode(' ', $cari);
			for ($i=0; $i<count($arr); $i++) {
				$str  = $arr[$i];
				$whr .= "nmuser like '%$str%' or nip like '%$str%' ";
				if ($i<count($arr)-1) {
					$whr .= " or ";
				}
			}
		}

		// cari query data dari t_user
		$query = $this->db->query("Select *, b.nmso, b.intern, b.ekstern From t_user a Left Join t_so b On a.kdso=b.kdso where kdpeg='1' $whr order by a.nmuser ");
		$hasil = $this->fc->ToArr( $query->result_array(), 'iduser');
		return $hasil;
	}

	public function get_baru() {
		// cari query data buku baru
		$query = $this->db->query("Select b.biblio_id, b.title, b.edition, b.isbn_issn, b.publish_year, b.call_number, b.collation, b.image, a.author_name, pl.place_name, p.publisher_name from dbpustaka.biblio as b left join dbpustaka.biblio_author as ba on b.biblio_id=ba.biblio_id left join dbpustaka.mst_author as a on ba.author_id = a.author_id left join dbpustaka.mst_place as pl on b.publish_place_id=pl.place_id left join dbpustaka.mst_publisher as p on b.publisher_id=p.publisher_id WHERE b.image IS NOT NULL order by b.input_date desc limit 20 ");
		$hasil = $this->fc->ToArr( $query->result_array(), 'biblio_id');
		return $hasil;
	}

	public function get_popular() {
		// cari query data buku Terpopuler
		$thang = $this->session->userdata('thang');
		$query = $this->db->query("
			SELECT l.item_code item_code, COUNT(l.item_code) freq, i.biblio_id, a.author_name, b.title, b.edition, b.isbn_issn, b.publish_year, b.call_number, b.collation, b.image, l.due_date, l.is_return, l.return_date, pl.place_name, p.publisher_name  FROM dbpustaka.loan l
			LEFT JOIN dbpustaka.item i ON i.item_code = l.item_code
			LEFT JOIN dbpustaka.biblio b ON b.biblio_id = i.biblio_id
			LEFT JOIN dbpustaka.biblio_author ba ON b.biblio_id=ba.biblio_id
			LEFT JOIN dbpustaka.mst_author AS a ON ba.author_id = a.author_id
			LEFT JOIN dbpustaka.mst_place AS pl ON b.publish_place_id=pl.place_id
			LEFT JOIN dbpustaka.mst_publisher AS p ON b.publisher_id=p.publisher_id
			WHERE YEAR(loan_date) = '$thang' and b.image IS NOT NULL
			GROUP BY 1 ORDER BY 2 DESC LIMIT 30
		");
		$hasil = $this->fc->ToArr( $query->result_array(), 'biblio_id');
		return $hasil;
	}

	public function get_aktif() {
		// cari query anggota teraktif
		$thang = $this->session->userdata('thang');
		$query = $this->db->query("select a.member_id, b.member_name, count(*) jml from dbpustaka.loan a left join dbpustaka.member b on a.member_id=b.member_id WHERE YEAR(a.loan_date) ='$thang' group by a.member_id order by jml desc limit 10");
		$hasil = $this->fc->ToArr( $query->result_array(), 'member_id');
		return $hasil;
	}

	public function get_aktif_all() {
		// cari query anggota teraktif all year
		$query = $this->db->query("select a.member_id, b.member_name, count(*) jml from dbpustaka.loan a left join dbpustaka.member b on a.member_id=b.member_id group by a.member_id order by jml desc limit 10");
		$hasil = $this->fc->ToArr( $query->result_array(), 'member_id');
		return $hasil;
	}

}
