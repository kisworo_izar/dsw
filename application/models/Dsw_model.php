<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Dsw_model extends CI_Model {

	public function get_menu( $str ) {
		if ( ! $this->menu_group() ) redirect( site_url("login/show_login") );
		$idmenu= $this->fc->InVar( $this->menu_group(), 'menu');
		$query = $this->db->query("Select *, if(right(idmenu,4)='0000','1', if(right(idmenu,2)='00','2', '3')) lvl, '' active From t_menu Where onoff='1' and idmenu $idmenu Order By urutan, idmenu");
		$menu  = $this->fc->ToArr( $query->result_array(), 'idmenu');

		if ($this->uri->segment(2)) {
			$kode  = '';
			$link  = $this->uri->segment(1) .'/'. $this->uri->segment(2);
			foreach ($menu as $key=>$value) if ($value['link'] == $link) { $kode = $value['idmenu']; $menu[ $key ]['active'] = 'active'; }

			if ($kode == '') {
				$link  = $this->uri->segment(1);
				foreach ($menu as $key=>$value) if ($value['link'] == $link) { $kode = $value['idmenu']; $menu[ $key ]['active'] = 'active'; }
			}

			foreach ($menu as $key=>$value) {
				if ($value['lvl'] == '2' and $value['idmenu'] == substr($kode,0,4).'00')   $menu[ $key ]['active'] = 'active';
				if ($value['lvl'] == '1' and $value['idmenu'] == substr($kode,0,2).'0000') $menu[ $key ]['active'] = 'active';
			}
		}
		return $menu;
	}

	public function menu_group() {
		$whr   = "idusergroup in ('". str_replace(';', "','", $this->session->userdata('idusergroup')) ."')";
		$query = $this->db->query("Select * From t_user_group Where $whr");
		$group = $this->fc->ToArr( $query->result_array(), 'idusergroup');

		$arr = array();
		if ($group) {
			foreach ($group as $row) {
				$str = explode(';', $row['menu']);
				for ($i=0; $i<count($str); $i++) {
					$nil = explode('-', $str[$i]);
					if ( ! array_key_exists($nil[0], $arr) ) {
						$arr[ $nil[0] ] = array('menu'=>$nil[0], 'rwx'=>$nil[0]); // Cegah error, mestinya 'rwx'=>$nil[1]
					}
				}
			}
		}
		return $arr;
	}

	public function get_rss() {
		// $query = $this->db->query("Select * From d_rss Order By pubdate Desc");
		$query = $this->db->query("Select * From d_rss");
		$hasil = $query->result_array();
		return $hasil;
	}
	public function get_news() {
		// $query = $this->db->query("Select * From d_rss Order By pubdate Desc");
		$query = $this->db->query("Select * From d_news where toplist='1'");
		$hasil = $query->result_array();
		return $hasil;
	}

	public function get_absen() {
		$nip   = $this->session->userdata('nip');
        $query = $this->milea->query("SELECT * FROM absen_net WHERE nip='$nip' order by tgl desc Limit 2 ");
        // echo "<pre>";print_r($query->result_array());exit();

		// $query = $this->db->query("Select * From d_absensi Where nip='$nip' Order By tanggal Desc Limit 2");
		return $query->result_array();
	}

	public function get_slideshow() {
		$query = $this->db->query("Select * From d_slideshow Where kdaktif ='1'");
		return $query->result_array();
	}

	public function get_pengumuman() {
		$query = $this->db->query("Select * From d_pengumuman Where toplist='1' Order By idpengumuman Desc Limit 1");
		$topls = $this->fc->ToArr( $query->result_array(), 'idpengumuman');
		$query = $this->db->query("Select * From d_pengumuman Where toplist='0' Order By idpengumuman Desc Limit 3");
		$list  = $this->fc->ToArr( $query->result_array(), 'idpengumuman');
		$hasil = array_merge($topls, $list);
		return $hasil;
	}

	public function get_hotthread() {
		$query = $this->db->query("Select concat(idparent,'.',idchild) idkey, idparent, idchild, count(*) thread From d_forum Group By 1,2,3 Order By thread Desc Limit 3");
		$hasil = $this->fc->ToArr( $query->result_array(), 'idkey');

		foreach ($hasil as $key=>$value) {
			// Forum Room
			$query = $this->db->query("Select nmroom From d_forum_room where idparent=". $value['idparent'] ." and idchild=". $value['idchild']);
			$room  = $query->row_array();
			$hasil[ $key ]['nmroom'] = $room['nmroom'];

			// Forum Home of Room
			$query = $this->db->query("Select nmroom From d_forum_room where idchild=".$value['idparent']);
			$home  = $query->row_array();
			$hasil[ $key ]['nmhome'] = $home['nmroom'];
		}
		return $hasil;
	}

	public function sql_forum( $str ) {
		// Find Lastest Thread
		$query = $this->db->query("Select idparent, idchild From d_forum Order By tglpost Desc Limit 1");
		$hasil = $query->row_array();
		if ( $str=='idparent' ) return $hasil['idparent'];
		if ( $str=='idchild' )  return $hasil['idchild'];
		return;
	}

	public function get_forum() {
		// Find Lastest Thread And 3 Last Post
		$query = $this->db->query("Select concat(idparent,'.',idchild) kdkey, idparent, idchild, max(tglpost) tglpost, count(*) jumlah From d_forum Group By 1,2,3 Order By tglpost Desc Limit 1");

		$hasil = array();
		foreach ($query->result_array() as $row) {
			$kdkey = $row['kdkey'] ;
			$where = 'where idparent='. $row['idparent'] .' and idchild='. $row['idchild'];

			// Forum Room
			$query = $this->db->query("Select *, concat(idparent,'.',idchild) kdkey From d_forum_room $where");
			$room  = $query->row_array();
			$hasil[ $kdkey ] = $room;

			// Forum Home of Room
			$query = $this->db->query("Select nmroom From d_forum_room where idchild=".$row['idparent']);
			$home  = $query->row_array();
			$hasil[ $kdkey ]['nmhome'] = $home['nmroom'];
			$hasil[ $kdkey ]['jumlah'] = $row['jumlah'];


			// Prakata Room
			$query = $this->db->query("Select concat(idparent,'.',idchild) kdkey, thread, tglpost, post, attach From d_forum $where and idforum=1");
			$post  = $query->row_array();
			$hasil[ $kdkey ]['judul'] = $post['thread'];
			$hasil[ $kdkey ]['post']  = $post['post'];
			$hasil[ $kdkey ]['attach']= $post['attach'];
		}
		$hasil  = $this->fc->ToArr( $hasil, 'kdkey');
		$query  = $this->db->query("Select iduser, nmuser, nip, jabatan, a.kdso, nmso From t_user a left join t_so b on a.kdso=b.kdso where iduser " . $this->fc->InVar($hasil, 'iduser'));
		$t_user = $this->fc->ToArr( $query->result_array(), 'iduser');
		$hasil  = $this->fc->Left_Join( $hasil, $t_user, 'iduser,nmuser=nmuser,nip=nip,jabatan=jabatan,nmso=nmso' );

		// Last Thread
		foreach ($hasil as $row) {
			$where = 'where idparent='. $row['idparent'] .' and idchild='. $row['idchild'];
			$query = $this->db->query("Select concat(idparent,'.',idchild,'.',idforum) kdkey, iduser, idforum, tglpost, post, attach From d_forum $where and idforum!=1 Order By tglpost Desc Limit 3");
			$thread= $this->fc->ToArr( $query->result_array(), 'kdkey');
			if ($thread) {
				$thread = $this->fc->array_index( $thread, 'idforum');
				$query  = $this->db->query("Select iduser, nmuser, nip, jabatan, a.kdso, nmso From t_user a left join t_so b on a.kdso=b.kdso where iduser " . $this->fc->InVar($thread, 'iduser'));
				$t_user = $this->fc->ToArr( $query->result_array(), 'iduser');
				$thread = $this->fc->Left_Join( $thread, $t_user, 'iduser,nmuser=nmuser,nip=nip,jabatan=jabatan,nmso=nmso' );

				$hasil[ $row['kdkey'] ]['thread'] = $thread;
			} else {
				$hasil[ $row['kdkey'] ]['thread'] = '';
			}
		}
		return $hasil;
	}


	// == Milik PERSURATAN ==
	public function get_surat() {
		$kdbagian = substr($this->session->userdata('kdso'),1,1);
		if ( strlen(trim($kdbagian))==0 ) { $kdbagian='10'; } else { $kdbagian.='0'; }
		$where = "where kdbagian='$kdbagian'";

		$dbsrt = $this->load->database('surat', TRUE);
		$sql   = "Select Distinct Concat(golsurat,noagenda,trim(noagenda1)) idkey, tgagenda, golsurat, noagenda, noagenda1, jenis, perihal, kddari kode From d_induk $where Order By tgagenda Desc Limit 5";
		$query = $dbsrt->query( $sql );
		$hasil = $this->fc->ToArr( $query->result_array(), 'idkey');
		$hasil = $this->fc->Ref_Join( $dbsrt, $hasil, 't_esl3', 'kode' );
		return $hasil;
	}

	public function param( $str=null ) {
		$dbsurat = $this->load->database('surat', TRUE);
		$query = $dbsurat->query("select '' nilai, '' tahun, '' bulan, '' bagian, '' cari, '0' status, 10 rowlimit, '' link, '' model, 0 rowstart");
		$query = $query->row();
		$query->tahun = date('Y');
		$query->bulan = sprintf('%02d', date("m"));

		if ($str<>null) {
			$arr = explode(',', $str);
			for ($i=0; $i<count($arr); $i++) {
				$arm = explode(':', $arr[$i]);
				$var = str_replace(' ','',$arm[0]);
				if ($var=='tahun') 	 { $query->tahun = $arm[1]; }
				if ($var=='bulan') 	 { $query->bulan = $arm[1]; }
				if ($var=='bagian')  { $query->bagian = $arm[1]; }
				if ($var=='status')  { $query->status = $arm[1]; }
				if ($var=='model')   { $query->model = $arm[1]; }
				if ($var=='rowstart' and $arm[1]<>''){ $query->rowstart = $arm[1]; }
			}
		}

		if (!$query->status) $query->status = '0';
		$query->cari  = $this->session->userdata('cari');
		$query->link  = "$query->tahun/$query->bulan/$query->bagian/$query->status";
		$query->nilai = "tahun:$query->tahun, bulan:$query->bulan, bagian:$query->bagian, cari:$query->cari, status:$query->status, link:$query->link, model:$query->model, rowstart:$query->rowstart";
		return $query;
	}

	public function bagian($string, $param, $url=null) {
		$dbsurat = $this->load->database('surat', TRUE);
		$bagian['01'] = array('kode'=>'01', 'uraian2'=>'Seluruh DJA');
		$query = $dbsurat->query("select kode, uraian2 from t_esl3 order by kode");
		foreach ($query->result_array() as $row) { $bagian[ $row['kode'] ] = array('kode'=>$row['kode'], 'uraian2'=>$row['uraian2']); }

		// // $query = $dbsurat->query("select kode, uraian from t_esl3 order by kode");
		// // $bagian = $this->fc->ToArr($query->result_array(), 'kode');

		// if ($string=='dropdown') {
		// 	$var = str_replace("'", "", $param->bagian);
		// 	$nil = trim($bagian["$var"]['uraian']);
		// 	$str = "<div class=\"dropdown\">
		// 			<button class=\"btn btn-default dropdown-toggle\" type=\"button\" id=\"bagian\" value=\"$param->bagian\" data-toggle=\"dropdown\">$nil
		// 			<span class=\"caret\"></span></button>
		// 			<ul class=\"dropdown-menu\" role=\"menu\" aria-labelledby=\"bagian\">
		// 		   ";
		// 	foreach($bagian as $row) {
		// 		$site = str_replace("/$param->bagian", "/".sprintf('%02d', $row['kode']), $url);
		// 		$str .= "<li role=\"presentation\"><a role=\"menuitem\" tabindex=\"-1\" href=\"$site\"> ". $row['uraian'] ."</a></li>";
		// 	}
		// 	$str .="</ul>
		// 			</div>
		// 		   ";
		// 	return $str;
		// }
		return $bagian;
	}

	public function tahun() {
		$dbsurat = $this->load->database('surat', TRUE);
		$query = $dbsurat->query("select distinct year(tgagenda) tahun, count(*) rec from d_induk group by 1");
		return $query->result();
	}

	public function bulan() {
		$bulan = array('Semua Bulan', 'Januari', 'Pebruari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'Nopember', 'Desember');
		return $bulan;
	}

	public function jml($str) {
		if ($str==0) {
			$nil = "zero\"> &nbsp; ";
		} elseif ($str>0) {
			$nil = "plus\">". number_format($str, 0, ',', '.') ;
		} else {
			$nil = "minus\">(". number_format(abs($str), 0, ',', '.') .")";
		}
		return $nil;
	}

}
