<?php
class M_poll extends CI_Model {
	public function get_judul() {
		$query = $this->db->query("select * from d_poll where kdaktif='1'");
		return $query->row_array();
	}

	public function get_data() {
		$query = $this->db->query("Select *, Concat(idpoll,'.',iditem) as idkey From d_poll_item where idpoll in (select idpoll from d_poll where kdaktif='1') Order By rand()");
		$hasil = $this->fc->ToArr( $query->result_array(), 'idkey');
		return $hasil;
	}

	public function get_vote() {
		$iduser = $this->session->userdata('iduser');
		$query  = $this->db->query("Select * From d_poll_submit where iduser='$iduser' And idpoll in (select idpoll from d_poll where kdaktif='1')");
		return $query->result_array();
	}

	public function get_hasil() {
		$query = $this->db->query("Select b.nmitem pilihan, Count(a.iditem) jumlah From d_poll_submit a Left Join d_poll_item b On a.idpoll=b.idpoll And a.iditem=b.iditem Group By a.iditem Order By jumlah Desc");
		return $query->result_array();
	}

	public function get_nama() {
		$query = $this->db->query("Select a.iduser, nmuser, nip, nmso, tglsubmit From d_poll_submit a Left Join t_user b On a.iduser=b.iduser Left Join t_so c On Concat(Left(b.kdso,2),'0000')=c.kdso Group By 1 Order By tglsubmit");
		return $query->result_array();
	}

	public function get_item( $idpoll=1 ) {
		$query = $this->db->query("Select a.idpoll, a.iditem, nmitem, count(iduser) jml From d_poll_item a Left Join d_poll_submit b On a.idpoll=b.idpoll And a.iditem=b.iditem Where a.idpoll=$idpoll Group By 1,2 Order By jml Desc");
		return $query->result_array();
	}

	public function get_total() {
		$query = $this->db->query("Select idpoll, iditem, count(*) total From d_poll_submit Group By 1,2 Order By total Desc Limit 1");
		return $query->row_array();
	}

	public function get_jml( $idpoll=1 ) {
		$query = $this->db->query("SELECT COUNT(DISTINCT(iduser)) jml FROM d_poll_submit where idpoll=$idpoll");
		return $query->row_array();
	}

	public function save_vote( $pil ) {
		if ( count($pil)==3 ) {
			$iduser = $this->session->userdata('iduser');
			$pilih  = explode('.', $pil[0]);

			$query  = $this->db->query("Select * From d_poll_submit Where iduser='$iduser' And idpoll=$pilih[0]");
			$hasil  = $query->result_array();
			if ( count( $query->result_array() )>0 ) {return "Mohon maaf, Anda sudah pernah memilih.";}

			foreach ($pil as $row) {
				$pilih  = explode('.', $row);
				$query  = $this->db->query("Insert Into d_poll_submit values ('$iduser', $pilih[0], $pilih[1], current_timestamp())");
			}
			return 'Terima kasih atas pilihan Anda.';
		}
		return;
	}

	public function get_grid() {
		$query = $this->db->query("Select * From d_poll");
		$hasil = $query->result_array();
		return $hasil;
	}

	public function get_row() {
		$idpoll = $this->uri->segment(5);
		if ($idpoll==null) $idpoll=1969;
		$query = $this->db->query("Select * From d_poll Where idpoll=$idpoll");
		$hasil = $query->row_array();
		return $hasil;
	}

	public function save_poll() {
		$action		= $_POST['ruh'];
		$idpoll		= $_POST['idpoll'];
		$nmpoll		= $_POST['nmpoll'];
		$deskripsi 	= $_POST['deskripsi'];
		$owner		= $_POST['owner'];
		$kdaktif	= $_POST['kdaktif'];
		$tglopen	= $this->fc->ustgl( $_POST['tglopen'], 'hari');
		$tglclose	= $this->fc->ustgl( $_POST['tglclose'], 'hari');

		if ($kdaktif==1) { $this->db->query( "update d_poll set kdaktif='0'" ); } else { $kdaktif='0'; }

		if ($action=='Rekam') {
			$this->db->query( "insert into d_poll (nmpoll, deskripsi, tglopen, tglclose, owner, kdaktif) values ('$nmpoll', '$deskripsi', $tglopen, '$tglclose', '$owner', '$kdaktif')" );
		}
		if ($action=='Ubah') {
			$this->db->query( "update d_poll set nmpoll='$nmpoll', deskripsi='$deskripsi', tglopen='$tglopen', tglclose='$tglclose', owner='$owner', kdaktif='$kdaktif' where idpoll=$idpoll" );
		}
		if ($action=='Hapus') {
			$this->db->query( "delete from d_poll where idpoll=$idpoll" );
		}
		return;
	}

	public function get_child( $idpoll ) {
		$query = $this->db->query("Select * From d_poll_item Where idpoll=$idpoll");
		$child = $query->result_array();

		$msg = '';
		foreach ($child as $row) {
			$msg .= '
		        <tr class="child">
		        	<td id="'. $row['idpoll'] .'" class="text-right"><small>'. $row['iditem'] .'</small></td>
	                <td id="'. $row['idpoll'] .'"><strong><small>'. $row['nmitem'] .'</small></strong></td>
	                <td><small>'. $row['deskripsi'] .'</small></td>
	                <td class="text-right"><small>'. trim($row['thumbnail']) .'</small></td>
	                <td class="text-right"><small>'. $row['file'] .'</small></td>
                    <td>&nbsp;</td>
                    <td class="Item">&nbsp;</td>
                </tr>';
		}
		return $msg;
	}


	public function get_cetak(){
		$query = $this->db->query("Select a.iduser, nmuser, nip, nmso, tglsubmit FROM d_poll_submit a LEFT JOIN t_user b ON a.iduser=b.iduser LEFT JOIN t_so c ON CONCAT(LEFT(b.kdso,2),'0000')=c.kdso GROUP BY 1 ORDER BY tglsubmit");
		$hasil = $query->result_array();
		return $hasil;

	}

}
