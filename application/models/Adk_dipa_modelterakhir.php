<?php
class Adk_dipa_model extends CI_Model {

	function get_unit( $type=null ) {
		$whrA = "WHERE $this->whrdept "; 
		if ($type == 'keu') $whrA .= "AND kddept='015' ";
		if (strpos('grup userunit usersatker', $this->group)) $whrB = "AND kdunit ='$this->unit'"; else $whrB = '';

		$qry = $this->dbref->query("SELECT CONCAT(kddept,'00') kode, concat(kddept,' ',nmdept) uraian FROM t_dept $whrA UNION ALL SELECT CONCAT(kddept,kdunit) kode, CONCAT(' &nbsp;&nbsp;&nbsp; ',kdunit,' ',nmunit) uraian FROM t_unit $whrA $whrB");
		$hsl = $this->fc->array_index($qry->result_array(), 'kode');
		foreach ($hsl as $key=>$row) if ($row['kode'] == $this->dept.$this->unit) $hsl[$key]['selected'] = 'selected'; else $hsl[$key]['selected'] = '';
		$hsl = $this->fc->ToArr($hsl, 'kode');
		return $hsl;
	}

	function matrik_rekap( $rev_id ) {
		$qry = $this->dbtmp->query("SELECT kddept, kdunit, kdsatker, '' uraian, '' lokasi, '' revisike, 0 pagu1, 0 blokir1, '' ds1, SUM(jml_pagu) pagu2, sum(jml_blokir) blokir2, '' ds2, '' indikasi, '' rev_last FROM v_rekap WHERE $this->index AND rev_id='$rev_id' GROUP BY 1,2,3");
		$rkp = $this->fc->ToArr($qry->result_array(), 'kdsatker');
		if (! $rkp) return array();

		$inv = $this->fc->InVar($rkp, 'kdsatker');
		$qry = $this->dbref->query("SELECT kdsatker, b.kdlokasi lokasi, nmsatker uraian FROM t_satker a LEFT JOIN t_kppn b ON a.kdkppn=b.kdkppn  WHERE kdsatker $inv");
		foreach ($qry->result_array() as $row) {
			$rkp[ $row['kdsatker'] ]['uraian'] = $row['uraian'];
			$rkp[ $row['kdsatker'] ]['lokasi'] = $row['lokasi'];
		}

		$inv = "$this->index AND kdsatker $inv";
		$qry = $this->dbrvs->query("SELECT kdsatker, concat(kdsatker,max(rev_id)) kode FROM v_rekap WHERE $inv GROUP BY 1");
		$arr = $qry->result_array();
		if ($arr) $inv = "$inv AND concat(kdsatker,rev_id) ". $this->fc->InVar($arr, 'kode');
		else $inv = "$inv";

		$qry = $this->dbrvs->query("SELECT kdsatker, rev_id, SUM(jml_pagu) pagu1, sum(jml_blokir) blokir1 FROM v_rekap WHERE $inv GROUP BY 1,2");
		foreach ($qry->result_array() as $row) {
			$rkp[ $row['kdsatker'] ]['pagu1']   = $row['pagu1'];
			$rkp[ $row['kdsatker'] ]['blokir1'] = $row['blokir1'];
			$rkp[ $row['kdsatker'] ]['rev_last']= $row['rev_id'];
		}

		foreach ($rkp as $key=>$row) {
			$kddept   = $row['kddept'];
			$kdunit   = $row['kdunit'];
			$kdsatker = $row['kdsatker'];
			$revisike = $row['rev_last'];

			if ($this->thang <= '2020') $ssb = $this->validasi_ssb_2020("temp$this->thang", $kdsatker, $rev_id);
			else 						$ssb = $this->validasi_ssb_2021("temp$this->thang", $kdsatker, $rev_id);
			$indks = $rkp[$key]['indikasi'];
			$array = $this->matrik_semula_menjadi($kdsatker, $this->dbrvs, $revisike, $this->dbtmp, $rev_id);

			$matrx = $array['matrx'];
			if ($matrx) {
				foreach ($matrx as $val) {
					if ($val['indikasi'] != '' And ! strpos("text $indks", $val['indikasi'])) $indks .= $val['indikasi'] .' ';
				}
				if ($matrx[1]['uraian'] == 'Kode Digital Stamp') { $rkp[$key]['ds1'] = $matrx[1]['jml1']; $rkp[$key]['ds2'] = $matrx[1]['jml2']; }
			}
			$catt = $array['catt'];
			if ($catt) {
				foreach ($catt as $val) 
					if ($val['level'] == 'catatan' And ($val['uraian'] != $val['muraian'] OR $val['pagu'] != $val['mpagu'])) 
						if (! strpos("text $indks", 'Hal4')) $indks .= 'Hal4 '; 
			}
			$blok = $array['blok'];
			if ($blok) {
				foreach ($blok as $val) 
					if ($val['level'] == 'blokir' And $val['blok'] != $val['mblok']) 
						if (! strpos("text $indks", 'Blokir')) $indks .= 'Blokir '; 
			}

			// Update KODE DIGITAL STAMP Data Menjadi
			$rkp[$key]['pagu1']    = $row['pagu1'];
			$rkp[$key]['pagu2']    = $row['pagu2'];
			$rkp[$key]['indikasi'] = $indks;
			$rkp[$key]['matrik']   = $matrx;
			$rkp[$key]['ssb-akun'] = count($ssb);
			$rkp[$key]['ssb-data'] = $ssb;
		}

		return $rkp; 
	}

	function matrik_semula_menjadi($kdsatker, $dbmula, $idmula, $dbjadi, $idjadi) {
		$matrx = array();
		$dbjadi->where('jenis', 'RA')->where('kddept', $this->dept)->where('kdunit', $this->unit)->where('kdsatker', $kdsatker)->where('rev_id', $idjadi);
		$matrx = $this->matrik_leveling($matrx, $dbjadi->get('v_rekap')->result_array());
		if (! $matrx) return array();

		$dbmula->where('jenis', 'RA')->where('kddept', $this->dept)->where('kdunit', $this->unit)->where('kdsatker', $kdsatker)->where('rev_id', $idmula);
		$matrx = $this->matrik_leveling($matrx, $dbmula->get('v_rekap')->result_array());

		$volsele = "concat(kdsatker,'.',kddept,'.',kdunit,'.',kdprogram,'.',kdgiat,'.',kdoutput) kode, sum(vol) vol";
		$voljadi = $dbjadi->query("SELECT $volsele FROM d_output  WHERE kdsatker='$kdsatker' AND rev_id='$idjadi' GROUP BY 1")->result_array();
		$volmula = $dbmula->query("SELECT $volsele FROM d_output  WHERE kdsatker='$kdsatker' AND rev_id='$idmula' GROUP BY 1")->result_array();
		$voljadi = $this->fc->ToArr($voljadi, 'kode');  $volmula = $this->fc->ToArr($volmula, 'kode');

		foreach ($matrx as $key=>$row) {
			if (array_key_exists($key, $voljadi)) $matrx[$key]['vol2']  = $row['vol2'] = $voljadi[$key]['vol'];
			if (array_key_exists($key, $volmula)) $matrx[$key]['vol1']  = $row['vol1'] = $volmula[$key]['vol'];
			$matrx[$key]['vol3']  = $row['vol2']  - $row['vol1'];
			$matrx[$key]['jml3']  = $row['jml2']  - $row['jml1'];
			$matrx[$key]['blok3'] = $row['blok2'] - $row['blok1'];
			if ($row['level'] == 'output') {
				if ($matrx[$key]['vol3'] < 0 And ! strpos($matrx[$key]['indikasi'], 'Vol-')) $matrx[$key]['indikasi'] .= 'Vol- ';
				if ($matrx[$key]['vol3'] > 0 And ! strpos($matrx[$key]['indikasi'], 'Vol+')) $matrx[$key]['indikasi'] .= 'Vol+ ';
			}
			if ($row['level'] == 'jenbel') {
				if ($matrx[$key]['jml3'] < 0 And ! strpos($matrx[$key]['indikasi'], $row['kode'].'-')) $matrx[$key]['indikasi'] .= $row['kode'].'- ';
				if ($matrx[$key]['jml3'] > 0 And ! strpos($matrx[$key]['indikasi'], $row['kode'].'+')) $matrx[$key]['indikasi'] .= $row['kode'].'+ ';
			}
			if ($row['level'] == 'akun') {
				if ($matrx[$key]['jml3'] == 0 And $matrx[$key]['blok3'] == 0) unset($matrx[$key]);
				else { $matrx[ substr($key,0,06) ]['sel'] = '1'; $matrx[ substr($key,0,16) ]['sel'] = '1'; $matrx[ substr($key,0,21) ]['sel'] = '1';
					   $matrx[ substr($key,0,25) ]['sel'] = '1'; $matrx[ substr($key,0,29) ]['sel'] = '1'; $matrx[ substr($key,0,33) ]['sel'] = '1';
					   $matrx[ substr($key,0,38) ]['sel'] = '1'; $matrx[$key]['sel'] = '1'; }
			}
		}
		foreach ($matrx as $key=>$row) {
			if ($row['level'] != 'satker' AND $row['level'] != 'digist')
				if ($matrx[$key]['sel'] <> '1' and $matrx[$key]['indikasi'] == '') unset($matrx[$key]);
		}

		if (! $matrx) return array();
		$matrx = $this->matrik_ds_uraian($matrx);

		// Update KODE DIGITAL STAMP Data Semula
		$index = "$this->index AND kdsatker='$kdsatker'";
		$sele1 = "thang,kdjendok,kdsatker,kddept,kdunit,kdprogram,kdgiat,kdoutput,kdlokasi,kdkabkota,kddekon";
		$sele2 = "kdkppn,kdbeban,kdjnsban,kdctarik,register,kdakun,jumlah,paguphln,pagurmp,pagurkp,blokirphln,blokirrmp,blokirrkp,rphblokir";
		// $qry_o = $dbmula->query("SELECT $sele1, Floor(sum(volume)) vol FROM v_rekap WHERE $index AND rev_id='$idmula' GROUP BY $sele1");
		$qry_o = $dbmula->query("SELECT $sele1, Floor(sum(vol)) vol FROM d_output WHERE kdsatker='$kdsatker' AND rev_id='$idmula' GROUP BY $sele1");
		$qry_i = $dbmula->query("SELECT $sele1, $sele2 FROM d_item WHERE kdsatker='$kdsatker' AND rev_id='$idmula'");
		$q_o1  = $qry_o->result_array();
		$q_o2  = $qry_i->result_array();
		if ($q_o1 and $q_o2)  $ds1 = $this->ds->hashing($q_o1, $q_o2);
		else $ds1 = '0000 0000 0000 0000';

		// $qry_o = $dbjadi->query("SELECT $sele1, Floor(sum(volume)) vol FROM v_rekap WHERE $index AND rev_id='$idjadi' GROUP BY $sele1");
		$qry_o = $dbjadi->query("SELECT $sele1, Floor(sum(vol)) vol FROM d_output  WHERE kdsatker='$kdsatker' AND rev_id='$idjadi' GROUP BY $sele1");
		$qry_i = $dbjadi->query("SELECT $sele1, $sele2 FROM d_item WHERE kdsatker='$kdsatker' AND rev_id='$idjadi'");
		$ds2   = $this->ds->hashing($qry_o->result_array(), $qry_i->result_array());

		if ($matrx[1]['uraian'] == 'Kode Digital Stamp') { $matrx[1]['jml1'] = $ds1; $matrx[1]['jml2'] = $ds2; }

		$hal3 = $this->matrik_kpa_hal3($kdsatker, $dbmula, $idmula, $dbjadi, $idjadi);
		$hal4 = $this->catatan_dipa_hal_4($kdsatker, $dbmula, $idmula, $dbjadi, $idjadi);
		return array('matrx'=>$matrx, 'kpa'=>$hal3['kpa'], 'hal3'=>$hal3['hal3'], 'blok'=>$hal4['blok'], 'catt'=>$hal4['catt']);
	}

	function matrik_leveling($matrx, $arr) {
		$detil = $matrx;
		$std = array('recid'=>'', 'kode'=>'', 'uraian'=>'', 'vol1'=>0, 'jml1'=>0, 'blok1'=>0, 'vol2'=>0, 'jml2'=>0, 'blok2'=>0, 'vol3'=>0, 'jml3'=>0, 'blok3'=>0, 'beban'=>'', 'sat'=>'', 'sel'=>'', 'level'=>'', 'indikasi'=>'');

		if (count($detil) == 0) { $v = 'vol2'; $j = 'jml2'; $b = 'blok2'; $p = 'pagu2'; $d = 'ds2'; $s = 'sat';}
		else { $v = 'vol1'; $j = 'jml1'; $b = 'blok1'; $p = 'pagu1'; $d = 'ds1'; $s = 'sat';}


		foreach ($arr as $row) {
			$thang=$row['thang']; $kdjendok=$row['kdjendok']; $kdsatker=$row['kdsatker']; $kddept=$row['kddept']; $kdunit=$row['kdunit']; $kdprogram=$row['kdprogram']; $kdgiat=$row['kdgiat']; $kdoutput=$row['kdoutput']; $kdsoutput=$row['kdsoutput']; $kdkmpnen=$row['kdkmpnen']; $kdlokasi=$row['kdlokasi']; $kdkabkota=$row['kdkabkota']; $kddekon=$row['kddekon']; $kdkppn=$row['kdkppn']; $kdbeban=$row['kdbeban']; $kdjnsban=$row['kdjnsban']; $kdctarik=$row['kdctarik']; $register=$row['register']; $kdakun=$row['kdakun']; $kdjenbel=substr($row['kdakun'],0,2);

			$jml = $row['jml_pagu']; $blok = $row['jml_blokir'];
			$arr=$std; $arr[$j] = $jml; $arr[$b] = $blok;
			$rkp['recid'] = $kdsatker; $rkp['kode'] = $kdsatker; $rkp[$p] = $jml;

			$dsout = array('thang'=>$thang, 'kdjendok'=>$kdjendok, 'kdsatker'=>$kdsatker, 'kddept'=>$kddept, 'kdunit'=>$kdunit, 'kdprogram'=>$kdprogram, 'kdgiat'=>$kdgiat, 'kdoutput'=>$kdoutput, 'kdlokasi'=>$kdlokasi, 'kdkabkota'=>$kdkabkota, 'kddekon'=>$kddekon, 'vol'=>$row['volume']);

			// Level 1 - SATKER
			$key = $kdsatker; $arr['recid'] = $key; $arr['kode'] = $kdsatker; $arr['level'] = 'satker';
			if (! array_key_exists($key, $detil)) { $arr['recid'] = "$kdsatker.000.01"; $detil[$key] = $arr; }
			else { $detil[$key][$j] += $jml; $detil[$key][$b] += $blok; }

			// Level 1b - DIGIST
			if (! array_key_exists("$kdsatker.0", $detil)) {
				$arr['level'] = 'digist'; $arr['recid'] = "$kdsatker.000.02"; $arr['kode'] = ''; $arr['uraian'] = 'Kode Digital Stamp';
				$detil["$kdsatker.0"] = $arr; $detil["$kdsatker.0"][$j] = 0; $arr['uraian'] = '';
			}

			// Level 2 - PROGRAM
			$key .= ".$kddept.$kdunit.$kdprogram"; $arr['recid'] = $key; $arr['kode'] = "$kddept.$kdunit.$kdprogram"; $arr['level'] = 'program';
			if (! array_key_exists($key, $detil)) $detil[$key] = $arr;
			else { $detil[$key][$j] += $jml; $detil[$key][$b] += $blok; }

			// Level 3 - KEGIATAN
			$key .= ".$kdgiat"; $arr['recid'] = $key; $arr['kode'] = $kdgiat; $arr['level'] = 'kegiatan';
			if (! array_key_exists($key, $detil)) $detil[$key] = $arr;
			else { $detil[$key][$j] += $jml; $detil[$key][$b] += $blok; }

			// Level 4 - OUTPUT
			$key .= ".$kdoutput"; $arr['recid'] = $key; $arr['kode'] = "$kdgiat.$kdoutput"; $arr['level'] = 'output';
			if (! array_key_exists($key, $detil)) { $detil[$key] = $arr; $detil[$key][$v] = $row['volume']; }
			else { $detil[$key][$j] += $jml; $detil[$key][$b] += $blok; $detil[$key][$v] += $row['volume']; }

			// Level 5 - SUB OUTPUT
			$key .= ".$kdsoutput"; $arr['recid'] = $key; $arr['kode'] = "$kdgiat.$kdoutput.$kdsoutput"; $arr['level'] = 'soutput';
			if (! array_key_exists($key, $detil)) { $detil[$key] = $arr; $detil[$key][$v] = $row['volume']; }
			else { $detil[$key][$j] += $jml; $detil[$key][$b] += $blok; $detil[$key][$v] += $row['volume']; }

			// Level 6 - KOMPONEN
			$key .= ".$kdkmpnen"; $arr['recid'] = $key; $arr['kode'] = $kdkmpnen; $arr['level'] = 'komponen';
			$vkmp = explode(' ', $row['volkmp']);
			if ((int)$vkmp[0] > 0) foreach ($vkmp as $ky=>$vl) if ($ky > 1) $vkmp[1] .= " $vl";
			if (! array_key_exists($key, $detil)) { $detil[$key] = $arr; $detil[$key][$v]  = (int)$vkmp[0]; }
			else { $detil[$key][$j] += $jml; $detil[$key][$b] += $blok;  $detil[$key][$v] += (int)$vkmp[0]; }
			if (isset($vkmp) and $detil[$key][$s])
				if ((int)$vkmp[0] > 0) $detil[$key][$s] = $vkmp[1];

			// Level 8 - JENIS BELANJA
			$key .= ".$kdbeban.$kdjenbel"; $arr['recid'] = $key; $arr['kode'] = "$kdbeban.$kdjenbel"; $arr['level'] = 'jenbel';
			if (! array_key_exists($key, $detil)) { $detil[$key] = $arr; $detil[$key]['beban'] = $kdbeban; }
			else { $detil[$key][$j] += $jml; $detil[$key][$b] += $blok; }

			// Level 7 - AKUN
			$key .= ".$kdakun"; $arr['recid'] = $key; $arr['kode'] = $kdakun; $arr['level'] = 'akun';
			if (! array_key_exists($key, $detil)) $detil[$key] = $arr;
			else { $detil[$key][$j] += $jml; $detil[$key][$b] += $blok; }

		}
		return $detil;
	}

	function matrik_ds_uraian( $matrx ) {
		// Ambil URAIAN dari TABEL REFERENSI
		$inStk = "kdsatker in ("; $inPrg = "concat(kddept,'.',kdunit,'.',kdprogram) in ("; $inKeg = "kdgiat in ("; $inOut = "concat(kdgiat,'.',kdoutput) in ("; $inSOu = "concat(kdgiat,'.',kdoutput,'.',kdsoutput) in ("; $inKmp = "concat(kdgiat,'.',kdoutput,'.',kdsoutput,'.',kdkmpnen) in ("; $inBel = "kdgbkpk in ("; $inAkn = "kdakun in (";
		foreach ($matrx as $row) {
			if ($row['level'] == 'satker'   And ! strpos($inStk, $row['kode']))  $inStk .= "'". $row['kode'] ."',";
			if ($row['level'] == 'program'  And ! strpos($inPrg, $row['kode']))  $inPrg .= "'". $row['kode'] ."',";
			if ($row['level'] == 'kegiatan' And ! strpos($inKeg, $row['kode']))  $inKeg .= "'". $row['kode'] ."',";
			if ($row['level'] == 'output'   And ! strpos($inOut, $row['kode']))  $inOut .= "'". $row['kode'] ."',";
			if ($row['level'] == 'soutput'  And ! strpos($inSOu, $row['kode']))  $inSOu .= "'". $row['kode'] ."',";
			if ($row['level'] == 'komponen' And ! strpos($inKmp, substr($row['recid'],-16)))  $inKmp .= "'". substr($row['recid'],-16) ."',";
			if ($row['level'] == 'jenbel'   And ! strpos($inBel, substr($row['kode'], 2,2)))  $inBel .= "'". substr($row['kode'], 2,2) ."',";
			if ($row['level'] == 'akun'     And ! strpos($inAkn, $row['kode']))  $inAkn .= "'". $row['kode'] ."',";
		}
		$inStk .= "'ok')";  $inPrg .= "'ok')"; $inKeg .= "'ok')"; $inOut .= "'ok')"; $inSOu .= "'ok')"; $inKmp .= "'ok')"; $inBel .= "'ok')"; $inAkn .= "'ok')";

		$query = $this->dbref->query("Select kdsatker kode, nmsatker uraian From t_satker Where $inStk");
		$t_stk = $this->fc->ToArr($query->result_array(), 'kode');
		$query = $this->dbref->query("Select concat(kddept,'.',kdunit,'.',kdprogram) kode, nmprogram uraian From t_program Where $inPrg");
		$t_prg = $this->fc->ToArr($query->result_array(), 'kode');
		$query = $this->dbref->query("Select kdgiat kode, nmgiat uraian From t_giat Where $inKeg");
		$t_keg = $this->fc->ToArr($query->result_array(), 'kode');
		$query = $this->dbref->query("Select concat(kdgiat,'.',kdoutput) kode, nmoutput uraian, sat, kdpn From t_output Where $inOut");
		$t_out = $this->fc->ToArr($query->result_array(), 'kode');
		$query = $this->dbref->query("Select concat(kdgiat,'.',kdoutput,'.',kdsoutput) kode, nmsoutput uraian, sat, kdpn From t_soutput Where $inSOu");
		$t_sou = $this->fc->ToArr($query->result_array(), 'kode');
		$query = $this->dbref->query("Select concat(kdgiat,'.',kdoutput,'.',kdsoutput,'.',kdkmpnen) kode, nmkmpnen uraian From t_kmpnen Where $inKmp");
		$t_kmp = $this->fc->ToArr($query->result_array(), 'kode');
		$query = $this->dbref->query("Select kdbeban kode, concat('[',trim(nmbeban2),'] ') uraian From t_beban");
		$t_bbn = $this->fc->ToArr($query->result_array(), 'kode');
		$query = $this->dbref->query("Select kdgbkpk kode, nmgbkpk uraian From t_gbkpk Where $inBel");
		$t_bel = $this->fc->ToArr($query->result_array(), 'kode');
		$query = $this->dbref->query("Select kdakun kode, nmakun uraian From t_akun Where $inAkn");
		$t_akn = $this->fc->ToArr($query->result_array(), 'kode');

		$matrx = $this->fc->array_index($matrx, 'recid');
		foreach ($matrx as $key=>$row) {
			// Update URAIAN sesuai Table Referensi untuk DATA DETIL
			if ($row['level'] == 'satker'   And array_key_exists($row['kode'], $t_stk)) $matrx[$key]['uraian'] = $t_stk[ $row['kode'] ]['uraian'];
			if ($row['level'] == 'program'  And array_key_exists($row['kode'], $t_prg)) $matrx[$key]['uraian'] = $t_prg[ $row['kode'] ]['uraian'];
			if ($row['level'] == 'kegiatan' And array_key_exists($row['kode'], $t_keg)) $matrx[$key]['uraian'] = $t_keg[ $row['kode'] ]['uraian'];
			if ($row['level'] == 'output'   And array_key_exists($row['kode'], $t_out)) { 
				$matrx[$key]['uraian'] = $t_out[ $row['kode'] ]['uraian'] .'<span style="font-style: italic"> ['. $t_out[ $row['kode'] ]['sat'] .']</span>'; 
				$matrx[$key]['sat'] = $t_out[ $row['kode'] ]['sat'];
				if ($t_out[ $row['kode'] ]['kdpn'] == '' or $t_out[ $row['kode'] ]['kdpn'] == '00') $matrx[$key]['kdpn'] = '';
				else $matrx[$key]['kdpn'] = '<span style="font-style: italic" class="text-orange"> [PN-'. $t_out[ $row['kode'] ]['kdpn'] .']</span>';
			}
			if ($row['level'] == 'soutput'  And array_key_exists($row['kode'], $t_sou)) { 
				$matrx[$key]['uraian'] = $t_sou[ $row['kode'] ]['uraian'] .'<span style="font-style: italic"> ['. $t_sou[ $row['kode'] ]['sat'] .']</span>'; 
				$matrx[$key]['sat'] = $t_sou[ $row['kode'] ]['sat']; 
				if ((int)$t_sou[ $row['kode'] ]['kdpn'] > 0) 
					$matrx[$key]['uraian'] .= ' <span style="font-style: italic" class="text-orange"> [PN-'. $t_sou[ $row['kode'] ]['kdpn'] .']</span>';
			}
			if ($row['level'] == 'komponen' And array_key_exists(substr($row['recid'],-16), $t_kmp)) {
				$matrx[$key]['uraian'] = $t_kmp[ substr($row['recid'],-16) ]['uraian'];
				if ($matrx[$key]['sat'] != '') $matrx[$key]['uraian'] .= '<span style="font-style: italic"> ['. $matrx[$key]['sat'] .']</span>';
			}
			if ($row['level'] == 'jenbel'   And array_key_exists(substr($row['kode'], 0,1), $t_bbn)) $matrx[$key]['uraian'] = $t_bbn[ substr($row['kode'], 0,1) ]['uraian'];
			if ($row['level'] == 'jenbel'   And array_key_exists(substr($row['kode'], 2,2), $t_bel)) $matrx[$key]['uraian'].= $t_bel[ substr($row['kode'], 2,2) ]['uraian'];
			if ($row['level'] == 'akun'     And array_key_exists($row['kode'], $t_akn)) $matrx[$key]['uraian'] = $t_akn[ $row['kode'] ]['uraian'];

			$V = '';  if ($row['vol3'] <  0) $V = '- '; $J = '';  if ($row['jml3'] <  0) $J = '- '; $B = '';  if ($row['blok3'] <  0) $B = '- ';
			if ($row['vol1'] == 0)  $matrx[$key]['vol1'] = ''; else $matrx[$key]['vol1'] = 	  number_format($row['vol1'], 0, ',', '.');
			if ($row['vol2'] == 0)  $matrx[$key]['vol2'] = ''; else $matrx[$key]['vol2'] =    number_format($row['vol2'], 0, ',', '.');
			if ($row['vol3'] == 0)  $matrx[$key]['vol3'] = ''; else $matrx[$key]['vol3'] = $V.number_format(abs($row['vol3']), 0, ',', '.');
			if ($row['jml1'] == 0)  $matrx[$key]['jml1'] = ''; else $matrx[$key]['jml1'] =    number_format($row['jml1']/1000, 0, ',', '.');
			if ($row['jml2'] == 0)  $matrx[$key]['jml2'] = ''; else $matrx[$key]['jml2'] =    number_format($row['jml2']/1000, 0, ',', '.');
			if ($row['jml3'] == 0)  $matrx[$key]['jml3'] = ''; else $matrx[$key]['jml3'] = $J.number_format(abs($row['jml3']/1000), 0, ',', '.');
			if ($row['blok1'] == 0) $matrx[$key]['blok1']= ''; else $matrx[$key]['blok1']=    number_format($row['blok1']/1000,0, ',', '.');
			if ($row['blok2'] == 0) $matrx[$key]['blok2']= ''; else $matrx[$key]['blok2']=    number_format($row['blok2']/1000,0, ',', '.');
			if ($row['blok3'] == 0) $matrx[$key]['blok3']= ''; else $matrx[$key]['blok3']= $B.number_format(abs($row['blok3']/1000),0, ',', '.');
		}

		return $matrx;
	}

	function matrik_kpa_hal3($kdsatker, $dbmula, $idmula, $dbjadi, $idjadi) {
		$whs  = "kdsatker='$kdsatker' AND rev_id='$idmula'";
		$whm  = "kdsatker='$kdsatker' AND rev_id='$idjadi'";

		$this->adk_dipa_model->update_data_kpa($kdsatker, $dbmula, $idmula);
		$this->adk_dipa_model->update_data_kpa($kdsatker, $dbjadi, $idjadi, 'temp');

		$qry  = $dbmula->query("SELECT * FROM d_kpa WHERE $whs LIMIT 1");
		$skpa = $qry->row();
		$qry  = $dbjadi->query("SELECT * FROM d_kpa WHERE $whm LIMIT 1");
		$mkpa = $qry->row();
		if (! $skpa) $skpa = $mkpa;

		$kpa    = array();
		$kpa[0] = array('kode'=>'Kuasa Pengguna Anggaran', 'mula'=>$skpa->kpa, 'jadi'=>$mkpa->kpa); 
		$kpa[1] = array('kode'=>'Bendahara Pengeluaran', 'mula'=>$skpa->bendahara, 'jadi'=>$mkpa->bendahara); 
		$kpa[2] = array('kode'=>'Pejabat Penanda Tangan SPM', 'mula'=>$skpa->ppspm, 'jadi'=>$mkpa->ppspm); 

		$fld  = "kddept,'.',kdunit,'.',kdprogram,'.',kdgiat";
		$whb  = "WHERE $whs AND kdtrktrm <= '2' AND length(jnsbelanja)=2 GROUP BY 1,2,3,4,5"; 
		$whp  = "WHERE $whs AND kdtrktrm >= '2' AND length(jnsbelanja)=6"; 
		$sel  = "sum(jml01) jml01, sum(jml02) jml02, sum(jml03) jml03, sum(jml04) jml04, sum(jml05) jml05, sum(jml06) jml06,
				 sum(jml07) jml07, sum(jml08) jml08, sum(jml09) jml09, sum(jml10) jml10, sum(jml11) jml11, sum(jml12) jml12,
				 0 jad01, 0 jad02, 0 jad03, 0 jad04, 0 jad05, 0 jad06, 0 jad07, 0 jad08, 0 jad09, 0 jad10, 0 jad11, 0 jad12 ";

		$qry  = $dbmula->query("SELECT concat(kdsatker,'.00') kdkey, kdsatker kode, '' kdgiat, '' jnsbelanja, '' kdtrktrm, $sel, '1' level FROM d_trktrm $whb");
		$strk = $qry->result_array();
		$qry  = $dbmula->query("SELECT concat(kdsatker,'.',jnsbelanja) kdkey, jnsbelanja kode, '' kdgiat, jnsbelanja, '' kdtrktrm, $sel, '2' level FROM d_trktrm $whb");
		$strk = array_merge($strk, $qry->result_array());
		$qry  = $dbmula->query("SELECT concat(kdsatker,'.88.',$fld) kdkey, concat($fld) kode, kdgiat, '' jnsbelanja, '' kdtrktrm, $sel, '3' level FROM d_trktrm $whb");
		$strk = array_merge($strk, $qry->result_array());
		$qry  = $dbmula->query("SELECT concat(kdsatker,'.88.',$fld,'.',jnsbelanja) kdkey, jnsbelanja kode, kdgiat, jnsbelanja, kdtrktrm, $sel, '4' level FROM d_trktrm $whb");
		$strk = array_merge($strk, $qry->result_array());
		$qry  = $dbmula->query("SELECT concat(kdsatker,'.98.') kdkey, '00' kode, '' kdgiat, '' jnsbelanja, kdtrktrm, $sel, '5' level FROM d_trktrm $whp GROUP BY 1");
		$strk = array_merge($strk, $qry->result_array());
		$qry  = $dbmula->query("SELECT concat(kdsatker,'.99.',jnsbelanja) kdkey, jnsbelanja kode, '' kdgiat, jnsbelanja, kdtrktrm, $sel, '6' level FROM d_trktrm $whp GROUP BY 1,2,3,4,5");
		$strk = $this->fc->Toarr(array_merge($strk, $qry->result_array()), 'kdkey');

		$whb  = "WHERE $whm AND kdtrktrm <= '2' AND length(jnsbelanja)=2 GROUP BY 1,2,3,4,5"; 
		$whp  = "WHERE $whm AND kdtrktrm >= '2' AND length(jnsbelanja)=6 "; 
		$sel  = "0 jml01, 0 jml02, 0 jml03, 0 jml04, 0 jml05, 0 jml06, 0 jml07, 0 jml08, 0 jml09, 0 jml10, 0 jml11, 0 jml12,
				 sum(jml01) jad01, sum(jml02) jad02, sum(jml03) jad03, sum(jml04) jad04, sum(jml05) jad05, sum(jml06) jad06,
				 sum(jml07) jad07, sum(jml08) jad08, sum(jml09) jad09, sum(jml10) jad10, sum(jml11) jad11, sum(jml12) jad12";

		$qry  = $dbjadi->query("SELECT concat(kdsatker,'.00') kdkey, kdsatker kode, '' kdgiat, '' jnsbelanja, '' kdtrktrm, $sel, '1' level FROM d_trktrm $whb");
		$mtrk = $qry->result_array();
		$qry  = $dbjadi->query("SELECT concat(kdsatker,'.',jnsbelanja) kdkey, jnsbelanja kode, '' kdgiat, jnsbelanja, '' kdtrktrm, $sel, '2' level FROM d_trktrm $whb");
		$mtrk = array_merge($mtrk, $qry->result_array());
		$qry  = $dbjadi->query("SELECT concat(kdsatker,'.88.',$fld) kdkey, concat($fld) kode, kdgiat, '' jnsbelanja, '' kdtrktrm, $sel, '3' level FROM d_trktrm $whb");
		$mtrk = array_merge($mtrk, $qry->result_array());
		$qry  = $dbjadi->query("SELECT concat(kdsatker,'.88.',$fld,'.',jnsbelanja) kdkey, jnsbelanja kode, kdgiat, jnsbelanja, kdtrktrm, $sel, '4' level FROM d_trktrm $whb");
		$mtrk = array_merge($mtrk, $qry->result_array());
		$qry  = $dbjadi->query("SELECT concat(kdsatker,'.98.') kdkey, '00' kode, '' kdgiat, '' jnsbelanja, kdtrktrm, $sel, '5' level FROM d_trktrm $whp GROUP BY 1");
		$mtrk = array_merge($mtrk, $qry->result_array());
		$qry  = $dbjadi->query("SELECT concat(kdsatker,'.99.',jnsbelanja) kdkey, jnsbelanja kode, '' kdgiat, jnsbelanja, kdtrktrm, $sel, '6' level FROM d_trktrm $whp GROUP BY 1,2,3,4,5");
		$mtrk = $this->fc->Toarr(array_merge($mtrk, $qry->result_array()), 'kdkey');

		foreach ($mtrk as $key=>$row) {
		 	if (array_key_exists($key, $strk)) {
		 		$strk[$key]['jad01'] = $row['jad01']; $strk[$key]['jad02'] = $row['jad02'];
		 		$strk[$key]['jad03'] = $row['jad03']; $strk[$key]['jad04'] = $row['jad04'];
		 		$strk[$key]['jad05'] = $row['jad05']; $strk[$key]['jad06'] = $row['jad06'];
		 		$strk[$key]['jad07'] = $row['jad07']; $strk[$key]['jad08'] = $row['jad08'];
		 		$strk[$key]['jad09'] = $row['jad09']; $strk[$key]['jad10'] = $row['jad10'];
		 		$strk[$key]['jad11'] = $row['jad11']; $strk[$key]['jad12'] = $row['jad12'];
		 	} else {
		  		$strk[$key] = $row;
		 	}
		} 
		// echo $this->fc->browse($strk); 

		$qry  = $this->dbref->query("SELECT kdsatker kode, nmsatker uraian FROM t_satker WHERE kdsatker='$kdsatker'");
		$satk = $this->fc->ToArr($qry->result_array(), 'kode');

		if (count($strk) == 0) { $giat = $jbel = $akun = array(); }
		else {
			$invr = "kdgiat ". $this->fc->InVar($strk,'kdgiat');
			$qry  = $this->dbref->query("SELECT kdgiat kode, nmgiat uraian FROM t_giat WHERE $invr");
			$giat = $this->fc->ToArr($qry->result_array(), 'kode'); 
			$invr = "kdgbkpk ". $this->fc->InVar($strk,'jnsbelanja');
			$qry  = $this->dbref->query("SELECT kdgbkpk kode, nmgbkpk uraian FROM t_gbkpk WHERE $invr");
			$jbel = $this->fc->ToArr($qry->result_array(), 'kode');
			$invr = "kdakun ". $this->fc->InVar($strk,'jnsbelanja');
			$qry  = $this->dbref->query("SELECT kdakun kode, nmakun uraian FROM t_akun WHERE $invr");
			$akun = $this->fc->ToArr($qry->result_array(), 'kode');

			foreach ($strk as $key=>$row) {
				$strk[$key]['uraian'] = '';
			 	if ( $row['level'] == '1' and array_key_exists($row['kode'], $satk))   $strk[$key]['uraian'] = $satk[ $row['kode'] ]['uraian'];
			 	if ( $row['level'] == '3' and array_key_exists($row['kdgiat'], $giat)) $strk[$key]['uraian'] = $giat[ $row['kdgiat'] ]['uraian'];
			 	if (($row['level'] == '2' or $row['level'] == '4') and array_key_exists($row['jnsbelanja'], $jbel)) 
			 		$strk[$key]['uraian'] = $jbel[ $row['jnsbelanja'] ]['uraian'];
			} 
			$strk = $this->fc->array_index($strk,'kdkey');
		}

		return array('kpa'=>$kpa, 'hal3'=>$strk);
	}

	function catatan_dipa_hal_4($kdsatker, $dbmula, $idmula, $dbjadi, $idjadi) {
		$indx = "$this->index AND kdsatker='$kdsatker'";
		$prog = "kddept,'.',kdunit,'.',kdprogram";
		$outp = "kdgiat,'.',kdoutput";
		$zero = "0 blok, 0 mblok, 0 pagu, 0 mpagu";


		// HAL IV A (BLOKIR)
		$kode  = "$prog,'.',$outp,'.',kdakun";
		$sele  = "concat($prog) kdprogram, kdgiat, concat($outp) kdoutput, kdakun, concat($kode) kdkey, '' kode";
		$whr   = "kdsatker='$kdsatker' AND NOT (isnull(kdblokir) OR kdblokir='')";
		$query = $dbmula->query("SELECT $sele, uraiblokir uraian, '' muraian, $zero, 'blokir' level FROM d_akun WHERE $whr AND rev_id='$idmula' ORDER BY $outp,kdakun");
		$blok  = $this->fc->ToArr($query->result_array(), 'kdkey');

		$query = $dbjadi->query("SELECT $sele, '' uraian, uraiblokir muraian, $zero, 'blokir' level FROM d_akun WHERE $whr AND rev_id='$idjadi' ORDER BY $outp,kdakun");
		$bjadi  = $this->fc->ToArr($query->result_array(), 'kdkey');

		foreach ($bjadi as $key=>$row) {
			if (array_key_exists($key, $blok)) $blok[$key]['muraian'] = $row['muraian'];  
			else $blok[$key] = $row;
		}

		if ($blok) {
			$whr   = "$indx AND concat($kode) ". $this->fc->InVar($blok, 'kdkey');
			$query = $dbmula->query("SELECT concat($kode) kdkey, sum(jml_blokir) blok  FROM v_rekap WHERE $whr AND rev_id='$idmula' GROUP BY 1");
			$bmula = $this->fc->ToArr($query->result_array(), 'kdkey');
			$query = $dbjadi->query("SELECT concat($kode) kdkey, sum(jml_blokir) mblok FROM v_rekap WHERE $whr AND rev_id='$idjadi' GROUP BY 1");
			$bjadi = $this->fc->ToArr($query->result_array(), 'kdkey');
		}


		// HAL IV B (CATATAN)
		$kode = "$prog,'.',$outp,'.',kdsoutput,'.',kdkmpnen,'.',trim(kdskmpnen),'.',kdakun";
		$sele = "concat($prog) kdprogram, kdgiat, concat($outp) kdoutput, kdakun, concat($kode) kdkey, '' kode";
		$query = $dbmula->query("SELECT $sele, concat(ket, if(isnull(ket2),'',ket2)) uraian, '' muraian, $zero, 'catatan' level FROM d_cttakun WHERE kdsatker='$kdsatker' AND rev_id='$idmula' ORDER BY $outp,kdakun");
		$catt  = $this->fc->ToArr($query->result_array(), 'kdkey');

		$query = $dbjadi->query("SELECT $sele, '' uraian, concat(ket, if(isnull(ket2),'',ket2)) muraian, $zero, 'catatan' level FROM d_cttakun WHERE kdsatker='$kdsatker' AND rev_id='$idjadi' ORDER BY $outp,kdakun");
		$cjadi = $this->fc->ToArr($query->result_array(), 'kdkey');

		foreach ($cjadi as $key=>$row) {
			if (array_key_exists($key, $catt)) $catt[$key]['muraian'] = $row['muraian']; 
			else $catt[$key] = $row;
		}

		if ($catt) {
			$whr   = "$indx AND concat($kode) ". $this->fc->InVar($catt, 'kdkey');
			$query = $dbmula->query("SELECT concat($kode) kdkey, sum(jml_pagu) pagu  FROM v_rekap WHERE $whr AND rev_id='$idmula' GROUP BY 1");
			$pmula = $this->fc->ToArr($query->result_array(), 'kdkey');
			$query = $dbjadi->query("SELECT concat($kode) kdkey, sum(jml_pagu) mpagu FROM v_rekap WHERE $whr AND rev_id='$idjadi' GROUP BY 1");
			$pjadi = $this->fc->ToArr($query->result_array(), 'kdkey');
		}


		if (! $blok and ! $catt) return array('blok'=>array(), 'catt'=>array());

		$grup  = array_merge($blok, $catt);
		$query = $this->dbref->query("SELECT concat($prog) kode, nmprogram uraian FROM t_program WHERE concat($prog) ". $this->fc->InVar($grup, 'kdprogram'));
		$t_prg = $this->fc->ToArr($query->result_array(), 'kode');
		$query = $this->dbref->query("SELECT kdgiat kode, nmgiat uraian FROM t_giat WHERE kdgiat ". $this->fc->InVar($grup, 'kdgiat'));
		$t_keg = $this->fc->ToArr($query->result_array(), 'kode');
		$query = $this->dbref->query("SELECT concat($outp) kode, nmoutput uraian, sat FROM t_output WHERE concat($outp) ". $this->fc->InVar($grup, 'kdoutput'));
		$t_out = $this->fc->ToArr($query->result_array(), 'kode');
		$query = $this->dbref->query("SELECT kdakun kode, nmakun uraian FROM t_akun WHERE kdakun ". $this->fc->InVar($grup, 'kdakun'));
		$t_aku = $this->fc->ToArr($query->result_array(), 'kode');



		if ($blok) {
			foreach ($blok as $key=>$row) {
				$blok[$key]['kdkey'] .= '.blokir';
				if (array_key_exists($key, $bmula)) $blok[$key]['blok']  = $bmula[$key]['blok'];
				if (array_key_exists($key, $bjadi)) $blok[$key]['mblok'] = $bjadi[$key]['mblok'];
				if ($blok[$key]['blok'] == 0 and $blok[$key]['mblok'] == 0) unset($blok[$key]);
			}
			foreach ($blok as $key=>$row) {
				$p = $row['kdprogram']; $g = $row['kdgiat']; $o = $row['kdoutput']; $a = $row['kdakun']; 
				if (! array_key_exists("$p", $blok) and array_key_exists($p, $t_prg)) 
					$blok[$p] = array('kdkey'=>$p, 'kode'=>$p, 'uraian'=>$t_prg[$p]['uraian'], 'level'=>'program');
				if (! array_key_exists("$p.$g", $blok) and array_key_exists($g, $t_keg)) 
					$blok["$p.$g"] = array('kdkey'=>"$p.$g", 'kode'=>$g, 'uraian'=>$t_keg[$g]['uraian'], 'level'=>'kegiatan');
				if (! array_key_exists("$p.$o", $blok) and array_key_exists($o, $t_out)) 
					$blok["$p.$o"] = array('kdkey'=>"$p.$o", 'kode'=>$o, 'uraian'=>$t_out[$o]['uraian'], 'level'=>'output'); 

				if (! array_key_exists("$key.akun", $blok) and array_key_exists($a, $t_aku)) 
					$blok["$key.akun"] = array('kdkey'=>"$key.akun", 'kode'=>$a, 'uraian'=>$t_aku[$a]['uraian'], 'blok'=>$blok[$key]['blok'], 'mblok'=>$blok[$key]['mblok'], 'level'=>'akun');
				else  
					{ $blok["$key.akun"]['blok'] += $blok[$key]['blok']; $blok["$key.akun"]['mblok'] += $blok[$key]['mblok']; }
			}
			if ($blok) $blok = $this->fc->array_index($blok, 'kdkey');
		}

		if ($catt) {
			foreach ($catt as $key=>$row) {
				$catt[$key]['kdkey'] = $row['kdprogram'].'.'.$row['kdoutput'].'.'.$row['kdakun'].'.catatan';
				if (array_key_exists($key, $pmula) and $row['uraian'] != '')  $catt[$key]['pagu']  = $pmula[$key]['pagu']; 
				if (array_key_exists($key, $pjadi) and $row['muraian'] != '') $catt[$key]['mpagu'] = $pjadi[$key]['mpagu']; 
				// if ($catt[$key]['pagu'] == 0 and $catt[$key]['mpagu'] == 0) unset($catt[$key]);
			}
			foreach ($catt as $key=>$row) {
				$p  = $row['kdprogram']; $g = $row['kdgiat']; $o = $row['kdoutput']; $a = $row['kdakun'];
				if (! array_key_exists("$p", $catt) and array_key_exists($p, $t_prg)) 
					$catt[$p] = array('kdkey'=>$p, 'kode'=>$p, 'uraian'=>$t_prg[$p]['uraian'], 'level'=>'program');
				if (! array_key_exists("$p.$g", $catt) and array_key_exists($g, $t_keg)) 
					$catt["$p.$g"] = array('kdkey'=>"$p.$g", 'kode'=>$g, 'uraian'=>$t_keg[$g]['uraian'], 'level'=>'kegiatan');
				if (! array_key_exists("$p.$o", $catt) and array_key_exists($o, $t_out)) 
					$catt["$p.$o"] = array('kdkey'=>"$p.$o", 'kode'=>$o, 'uraian'=>$t_out[$o]['uraian'], 'pagu'=>$catt[$key]['pagu'], 'mpagu'=>$catt[$key]['mpagu'], 'level'=>'output');
				else  
					{ $catt["$p.$o"]['pagu'] += $catt[$key]['pagu']; $catt["$p.$o"]['mpagu'] += $catt[$key]['mpagu']; }
				if (! array_key_exists("$p.$o.$a.akun", $catt) and array_key_exists($a, $t_aku)) 
					$catt["$p.$o.$a.akun"] = array('kdkey'=>"$p.$o.$a.akun", 'kode'=>$a, 'uraian'=>$t_aku[$a]['uraian'], 'level'=>'akun'); 
			}
			if ($catt) $catt = $this->fc->array_index($catt, 'kdkey');
		}
		// echo $this->fc->browse($catt); exit;

		return array('blok'=>$blok, 'catt'=>$catt); 
	}

	function kertas_kerja($levl, $konds, $id, $dbs, $lvl=null) {
		$db  = $this->load->database($dbs, TRUE);
		if ($lvl == null) $lvl = 9; 
		$p = $this->kertas_kerja_setting();

		$hasil = array();
		for ($i=1; $i<=$lvl; $i++) {
			$tb = $p[$i]['tbl'];  $kd = 'concat('. $p[$i]['kd'] .')';  $kode = 'concat('. $p[$i]['kode'] .')';  $nm = $p[$i]['nm'];  $vol = $p[$i]['vol'];

			if ($tb=='d_item') {
				$nilai = $db->select("$kd kdkey, '' kode, nmitem uraian, volkeg, satkeg, hargasat, jumlah, '' atrib, '' sdcp, kdblokir, kdsbu, 9 level", FALSE);
			} else {
				$nilai = $db->select("$kd kdkey, concat($kode) kode, '' uraian, 0 volkeg, '' satkeg, 0 hargasat, Sum(jumlah) jumlah, '' atrib, '' sdcp, '' kdblokir, '' kdsbu, $i level", FALSE)->group_by('kdkey,kode');
			}

			if ($levl == 'satker') {
				$whr = "kdsatker='$konds' AND";
				$db->where('kdsatker', $konds);
			} else {
				$whr = "";
				$db->where('kddept',$this->dept)->where('kdunit', $this->unit);
				if ($levl == 'giat')    $db->where('kdgiat', $konds);
				if ($levl == 'output')  $db->where("concat(kdgiat,'.',kdoutput)", $konds);
				if ($levl == 'soutput') $db->where("concat(kdgiat,'.',kdoutput,'.',kdsoutput)", $konds);
			}

			if (substr($dbs,0,6) == 'telaah')	$db->where('substr(rev_id,1,2)', substr($id,0,2));
				else 	 						$db->where('rev_id', $id);

			$nilai = $db->get('d_item')->result_array();
			if (! $nilai) { $data['tabel'] = array(); return $data; };
			$nilai = $this->fc->ToArr($nilai, 'kdkey');

			if (strpos('tbl t_satker t_program t_giat t_akun', $tb)) {
				$query = $this->dbref->query("SELECT $kode kdkey, $nm uraian FROM $tb WHERE $kode ". $this->fc->InVar($nilai, 'kode'));
				$t_ref = $this->fc->ToArr( $query->result_array(), 'kdkey');
				$nilai = $this->fc->Left_Join( $nilai, $t_ref, "kode, uraian=uraian" );
			}

			if ($tb == 't_output') {
				// $query = $this->dbref->query("SELECT $kode kdkey, if(kdpn>'00', concat(nmoutput,' (PN-',kdpn,')'), nmoutput) uraian, sat, if(kdpn>'00','PN','') kdpn FROM $tb WHERE $kode ". $this->fc->InVar($nilai, 'kode'));
				$query = $this->dbref->query("SELECT $kode kdkey, nmoutput uraian, sat, '' kdpn FROM $tb WHERE $kode ". $this->fc->InVar($nilai, 'kode'));

				$t_out = $this->fc->ToArr( $query->result_array(), 'kdkey');
				$nilai = $this->fc->Left_Join( $nilai, $t_out, "kode, uraian=uraian, satkeg=sat, sdcp=kdpn" );

				$tbl   = str_replace('t_', 'd_', $tb);
				$query = $db->query("SELECT $kd kdkey, vol FROM $tbl WHERE $whr $kd " . $this->fc->InVar($nilai, 'kdkey'));
				$t_ref = $this->fc->ToArr( $query->result_array(), 'kdkey');
				$nilai = $this->fc->Left_Join( $nilai, $t_ref, "kdkey, volkeg=vol" );
			}

			if ($tb == 't_soutput') {
				$query = $this->dbref->query("SELECT $kode kdkey, $nm uraian FROM $tb WHERE $kode ". $this->fc->InVar($nilai, 'kode'));
				$t_ref = $this->fc->ToArr( $query->result_array(), 'kdkey');
				$nilai = $this->fc->Left_Join( $nilai, $t_ref, "kode, uraian=uraian" );

				$tbl   = str_replace('t_', 'd_', $tb);
				$query = $db->query("SELECT $kd kdkey, volsout FROM $tbl WHERE $whr $kd " . $this->fc->InVar($nilai, 'kdkey'));
				$t_ref = $this->fc->ToArr( $query->result_array(), 'kdkey');
				foreach($t_ref as $key=>$row) $t_ref[$key]['sat'] = $t_out[ substr($row['kdkey'],17,8) ]['sat'];
				$nilai = $this->fc->Left_Join( $nilai, $t_ref, "kdkey, volkeg=volsout, satkeg=sat" );
			}

			if ($tb == 't_kmpnen') {
				$tbl   = str_replace('t_', 'd_', $tb);
				$query = $db->query("SELECT $kd kdkey, $nm uraian, kdtema, if(kdsbiaya='1','U','') atrib FROM $tbl WHERE $whr $kd ". $this->fc->InVar($nilai, 'kdkey'));
				$t_ref = $this->fc->ToArr( $query->result_array(), 'kdkey');
				foreach($t_ref as $key=>$row) $t_ref[$key]['kdtema'] = str_replace(',00', ' ', $row['kdtema']);
				$nilai = $this->fc->Left_Join( $nilai, $t_ref, "kdkey, uraian=uraian, satkeg=kdtema, atrib=atrib" );
			}

			if (strpos('tbl d_skmpnen', $tb)) {
				$query = $db->query("SELECT $kd kdkey, $nm uraian FROM $tb WHERE $kd " . $this->fc->InVar($nilai, 'kdkey'));
				$t_ref = $this->fc->ToArr( $query->result_array(), 'kdkey');
				$nilai = $this->fc->Left_Join( $nilai, $t_ref, "kdkey, uraian=uraian" );
			}

			if ($tb == 't_akun') {
				$query = $this->dbref->query("SELECT $kode kdkey, $nm uraian FROM $tb WHERE $kode ". $this->fc->InVar($nilai, 'kode'));
				$t_ref = $this->fc->ToArr( $query->result_array(), 'kdkey');
				$nilai = $this->fc->Left_Join( $nilai, $t_ref, "kode, uraian=uraian" );

				$tbl   = str_replace('t_', 'd_', $tb);
				$query = $db->query("SELECT $kd kdkey, concat(kdbeban,kdjnsban,kdctarik) sdcp FROM $tbl WHERE $whr $kd " . $this->fc->InVar($nilai, 'kdkey'));
				$t_ref = $this->fc->ToArr( $query->result_array(), 'kdkey');
				$nilai = $this->fc->Left_Join( $nilai, $t_ref, "kdkey, sdcp=sdcp" );
			}

			if ($tb == 't_program t_giat') {
				$query = $this->dbref->query("SELECT $kode kdkey, $nm uraian FROM $tb WHERE $kode " . $this->fc->InVar($nilai, 'kode'));
				$t_ref = $this->fc->ToArr( $query->result_array(), 'kdkey');
				$nilai = $this->fc->Left_Join( $nilai, $t_ref, "kode,uraian=uraian" );
			}

			$hasil = array_merge($hasil, $nilai);
		}
		$hasil = $this->fc->array_index($hasil, 'kdkey');
		$hasil = $this->fc->ToArr( $hasil, 'kdkey');

		$data['tabel'] = $hasil;
 		return $data;
	}

	function kertas_kerja_setting() {
		$param['1'] = array('tbl'=>'t_satker', 'kd'=>"kdsatker", 'kode'=>"kdsatker", 'nm'=>'nmsatker', 'vol'=>'');
		$param['2'] = array('tbl'=>'t_program', 'kd'=>$param['1']['kd'] . ",'.',kddept,'.',kdunit,'.',kdprogram", 'kode'=>"kddept,'.',kdunit,'.',kdprogram", 'nm'=>'nmprogram', 'vol'=>'');
		$param['3'] = array('tbl'=>'t_giat', 'kd'=>$param['2']['kd'] . ",'.',kdgiat", 'kode'=>"kdgiat", 'nm'=>'nmgiat', 'vol'=>'');
		$param['4'] = array('tbl'=>'t_output', 'kd'=>$param['3']['kd'] . ",'.',kdoutput", 'kode'=>"kdgiat,'.',kdoutput", 'nm'=>'nmoutput', 'vol'=>'vol');
		$param['5'] = array('tbl'=>'t_soutput', 'kd'=>$param['4']['kd'] . ",'.',kdsoutput", 'kode'=>"kdgiat,'.',kdoutput,'.',kdsoutput", 'nm'=>'nmsoutput', 'vol'=>'');
		$param['6'] = array('tbl'=>'t_kmpnen', 'kd'=>$param['5']['kd'] . ",'.',kdkmpnen", 'kode'=>"kdgiat,'.',kdoutput,'.',kdsoutput,'.',kdkmpnen", 'nm'=>'urkmpnen', 'vol'=>'');
		$param['7'] = array('tbl'=>'d_skmpnen', 'kd'=>$param['6']['kd'] . ",'.',trim(kdskmpnen)", 'kode'=>"trim(kdskmpnen)", 'nm'=>'urskmpnen', 'vol'=>'');
		$param['8'] = array('tbl'=>'t_akun', 'kd'=>$param['7']['kd'] . ",'.',kdakun", 'kode'=>"kdakun", 'nm'=>'nmakun', 'vol'=>'');
		$param['9'] = array('tbl'=>'d_item', 'kd'=>$param['8']['kd'] . ",'.',lpad(noitem,5,0)", 'kode'=>"noitem", 'nm'=>'', 'vol'=>'');
		return $param;
	}

	function validasi_ssb_2020($dbs, $kdsatker, $id) {
		$db  = $this->load->database($dbs, TRUE);
		$rvs = $this->load->database("revisi$this->thang", TRUE);

		$kel = $rvs->query("SELECT kddept, kdunit, kelssb, kdsatker FROM t_ssb_satker WHERE thang='$this->thang' AND kdsatker='$kdsatker'")->row();
		if (! $kel) return array();

		$whr = "kddept='$kel->kddept' AND kdunit='$kel->kdunit' AND kelssb IN ('". str_replace(",", "','", $kel->kelssb) ."') AND kdaktif='1'";
		$qry = $rvs->query("SELECT DISTINCT CONCAT(kdgiat,kdoutput) kdoutput, '' outgene, kelssb FROM t_ssb WHERE $whr")->result_array();
		$out = $this->fc->ToArr($qry, 'kdoutput');
		if (! $out) return array();

		$set = "thang='$this->thang' AND kddept='$kel->kddept' AND kdunit='00' AND kdaktif='1'";
		$qry = $rvs->query("SELECT CONCAT(kdgiat,kdoutput) kdoutput, kdoutput outgene, CONCAT(kdoutput,kdsoutput,kdkmpnen,trim(kdskmpnen),kdakun) kdssb FROM t_ssb WHERE $set AND kdsoutput!='000'");
		$aaa = $this->fc->ToArr($qry->result_array(), 'kdssb');
		$gen = $this->fc->ToArr($aaa, 'outgene');

		// Range Output Yg di-Filter SSB
		$all = array_merge_recursive($out, $aaa);
		$who = "CONCAT(kdgiat,kdoutput) ". $this->fc->InVar($all, 'kdoutput') ." OR kdoutput ". $this->fc->InVar($all, 'outgene');

		$qry = $rvs->query("SELECT CONCAT(kdgiat,kdoutput,kdsoutput,kdkmpnen,trim(kdskmpnen),kdakun) kdssb FROM t_ssb WHERE $whr AND kdsoutput!='000' AND kdskmpnen!='00'");
		$ssb = $this->fc->ToArr($qry->result_array(), 'kdssb');

		$qry = $rvs->query("SELECT DISTINCT CONCAT(kdgiat,kdoutput,kdkmpnen,trim(kdskmpnen),kdakun) kdssb FROM t_ssb WHERE $whr AND kdsoutput='000'");
		$sso = $this->fc->ToArr($qry->result_array(), 'kdssb');

		$qry = $rvs->query("SELECT DISTINCT CONCAT(kdgiat,kdoutput,kdsoutput,kdkmpnen,kdakun) kdssb FROM t_ssb WHERE $whr AND kdskmpnen='00'");
		$ssk = $this->fc->ToArr($qry->result_array(), 'kdssb');

		$qry = $rvs->query("SELECT DISTINCT CONCAT(kdgiat,kdoutput,kdkmpnen,kdakun) kdssb FROM t_ssb WHERE $whr AND kdsoutput='000' AND kdskmpnen='00'");
		$sss = $this->fc->ToArr($qry->result_array(), 'kdssb');

		$qry = $rvs->query("SELECT DISTINCT kdakun kdssb FROM t_ssb WHERE kddept='$kel->kddept' AND kdaktif='1' AND kdoutput='000' AND kdsoutput='000' AND kdskmpnen='00'");
		$cov = $this->fc->ToArr($qry->result_array(), 'kdssb');

		if (substr($dbs,0,6) == 'telaah') $whr = "kdsatker='$kdsatker' AND LEFT(rev_id,2)='$this->jnsforum'"; else $whr = "kdsatker='$kdsatker' AND rev_id='$id'";
		$dat = $db->query("SELECT thang,kddept,kdunit,'' kelssb,kdgiat,kdoutput,kdsoutput,kdkmpnen,trim(kdskmpnen) kdskmpnen,kdakun, sum(jumlah) jumlah FROM d_item WHERE $whr GROUP BY 1,2,3,4,5,6,7,8,9,10")->result_array();
		foreach ($dat as $key=>$row) {
			if (array_key_exists($row['kdgiat'].$row['kdoutput'], $out)) $dat[$key]['kelssb'] = $out[$row['kdgiat'].$row['kdoutput']]['kelssb'];
			else 
				if (! array_key_exists($row['kdoutput'], $gen)) unset( $dat[$key] );
		}

		foreach ($dat as $key=>$row) {
			$kdgiat = $row['kdgiat']; $kdoutput = $row['kdoutput']; $kdsoutput = $row['kdsoutput']; $kdkmpnen = $row['kdkmpnen']; $kdskmpnen = $row['kdskmpnen']; $kdakun = $row['kdakun'];
			if (strpos('SBK K_S_M_U', substr($kdsoutput,0,1))) unset($dat[$key]);
			else {
				if ($aaa) if (array_key_exists("$kdoutput$kdsoutput$kdkmpnen$kdskmpnen$kdakun", $aaa)) unset($dat[$key]);
				if ($ssb) if (array_key_exists("$kdgiat$kdoutput$kdsoutput$kdkmpnen$kdskmpnen$kdakun", $ssb)) unset($dat[$key]);
				if ($sso) if (array_key_exists("$kdgiat$kdoutput$kdkmpnen$kdskmpnen$kdakun", $sso)) unset($dat[$key]);
				if ($ssk) if (array_key_exists("$kdgiat$kdoutput$kdsoutput$kdkmpnen$kdakun", $ssk)) unset($dat[$key]);
				if ($sss) if (array_key_exists("$kdgiat$kdoutput$kdsoutput$kdakun", $sss)) unset($dat[$key]);
				if ($cov) if (array_key_exists("$kdakun", $cov)) unset($dat[$key]);
			}
		}		
		return $dat;
	}

	function validasi_ssb_2021_ok($dbs, $kdsatker, $id) {
		$db  = $this->load->database($dbs, TRUE);
		$rvs = $this->load->database("revisi$this->thang", TRUE);

		$kel = $rvs->query("SELECT kddept, kdunit, kelssb, kdsatker FROM t_ssb_satker WHERE thang='$this->thang' AND kdsatker='$kdsatker'")->row();
		if (! $kel) return array();

		$whr = "kddept='$kel->kddept' AND kdunit='$kel->kdunit' AND kelssb IN ('". str_replace(",", "','", $kel->kelssb) ."') AND kdaktif='1'";
		$qry = $rvs->query("SELECT DISTINCT CONCAT(kdgiat,kdoutput) kdoutput, '' outgene, kelssb FROM t_ssb WHERE $whr")->result_array();
		$out = $this->fc->ToArr($qry, 'kdoutput');
		if (! $out) return array();

		$set = "thang='$this->thang' AND kddept='$kel->kddept' AND kdunit='00' AND kdaktif='1'";
		$qry = $rvs->query("SELECT CONCAT(kdgiat,kdoutput) kdoutput, kdoutput outgene, CONCAT(kdoutput,kdsoutput,kdkmpnen,trim(kdskmpnen),kdakun) kdssb FROM t_ssb WHERE $set AND kdsoutput!='000'");
		$aaa = $this->fc->ToArr($qry->result_array(), 'kdssb');
		$gen = $this->fc->ToArr($aaa, 'outgene');

		$qry = $rvs->query("SELECT CONCAT(kdgiat,kdoutput) kdoutput, kdoutput outgene, CONCAT(kdoutput,kdkmpnen,kdakun) kdssb FROM t_ssb WHERE $set AND kdgiat='0000' AND kdsoutput='000'");
		$krs = $this->fc->ToArr($qry->result_array(), 'kdssb');

		// Range Output Yg di-Filter SSB
		$all = array_merge_recursive($out, $aaa);
		$who = "CONCAT(kdgiat,kdoutput) ". $this->fc->InVar($all, 'kdoutput') ." OR kdoutput ". $this->fc->InVar($all, 'outgene');

		$qry = $rvs->query("SELECT CONCAT(kdgiat,kdoutput,kdsoutput,kdkmpnen,trim(kdskmpnen),kdakun) kdssb FROM t_ssb WHERE $whr AND kdsoutput!='000' AND kdskmpnen!='00'");
		$ssb = $this->fc->ToArr($qry->result_array(), 'kdssb');

		$qry = $rvs->query("SELECT DISTINCT CONCAT(kdgiat,kdoutput,kdkmpnen,trim(kdskmpnen),kdakun) kdssb FROM t_ssb WHERE $whr AND kdsoutput='000'");
		$sso = $this->fc->ToArr($qry->result_array(), 'kdssb');

		$qry = $rvs->query("SELECT DISTINCT CONCAT(kdgiat,kdoutput,kdsoutput,kdkmpnen,kdakun) kdssb FROM t_ssb WHERE $whr AND kdskmpnen='00'");
		$ssk = $this->fc->ToArr($qry->result_array(), 'kdssb');

		$qry = $rvs->query("SELECT DISTINCT CONCAT(kdgiat,kdoutput,kdkmpnen,kdakun) kdssb FROM t_ssb WHERE $whr AND kdsoutput='000' AND kdskmpnen='00'");
		$sss = $this->fc->ToArr($qry->result_array(), 'kdssb');

		$qry = $rvs->query("SELECT DISTINCT kdakun kdssb FROM t_ssb WHERE kddept='$kel->kddept' AND kdaktif='1' AND kdoutput='000' AND kdsoutput='000' AND kdskmpnen='00'");
		$cov = $this->fc->ToArr($qry->result_array(), 'kdssb');

		if (substr($dbs,0,6) == 'telaah') $whr = "kdsatker='$kdsatker' AND LEFT(rev_id,2)='$this->jnsforum'"; else $whr = "kdsatker='$kdsatker' AND rev_id='$id'";
		$dat = $db->query("SELECT thang,kddept,kdunit,'' kelssb,kdgiat,kdoutput,kdsoutput,kdkmpnen,trim(kdskmpnen) kdskmpnen,kdakun, sum(jumlah) jumlah FROM d_item WHERE $whr GROUP BY 1,2,3,4,5,6,7,8,9,10")->result_array();
		foreach ($dat as $key=>$row) {
			if (array_key_exists($row['kdgiat'].$row['kdoutput'], $out)) $dat[$key]['kelssb'] = $out[$row['kdgiat'].$row['kdoutput']]['kelssb'];
			else 
				if (! array_key_exists($row['kdoutput'], $gen)) unset( $dat[$key] );
		}

		foreach ($dat as $key=>$row) {
			$kdgiat = $row['kdgiat']; $kdoutput = $row['kdoutput']; $kdsoutput = $row['kdsoutput']; $kdkmpnen = $row['kdkmpnen']; $kdskmpnen = $row['kdskmpnen']; $kdakun = $row['kdakun'];
			if (strpos('SBK K_S_M_U', substr($kdsoutput,0,1))) unset($dat[$key]);
			else {
				if ($aaa) if (array_key_exists("$kdoutput$kdsoutput$kdkmpnen$kdskmpnen$kdakun", $aaa)) unset($dat[$key]);
				if ($ssb) if (array_key_exists("$kdgiat$kdoutput$kdsoutput$kdkmpnen$kdskmpnen$kdakun", $ssb)) unset($dat[$key]);
				if ($sso) if (array_key_exists("$kdgiat$kdoutput$kdkmpnen$kdskmpnen$kdakun", $sso)) unset($dat[$key]);
				if ($ssk) if (array_key_exists("$kdgiat$kdoutput$kdsoutput$kdkmpnen$kdakun", $ssk)) unset($dat[$key]);
				if ($sss) if (array_key_exists("$kdgiat$kdoutput$kdsoutput$kdakun", $sss)) unset($dat[$key]);
				if ($cov) if (array_key_exists("$kdakun", $cov)) unset($dat[$key]);
				if ($krs) if (array_key_exists("$kdoutput$kdkmpnen$kdakun", $krs)) unset($dat[$key]);
			}
		}		
		return $dat;
	}

	function validasi_ssb_2021($dbs, $kdsatker, $id) {
		$db  = $this->load->database($dbs, TRUE);
		$rvs = $this->load->database("revisi$this->thang", TRUE);

		$kel = $rvs->query("SELECT kddept, kdunit, kelssb, kdsatker FROM t_ssb_satker WHERE thang='$this->thang' AND kdsatker='$kdsatker'")->row();
		if (! $kel) return array();

		$whr = "kddept='$kel->kddept' AND ((kdunit='$kel->kdunit' AND kelssb IN ('". str_replace(",", "','", $kel->kelssb) ."')) OR kdunit='00') AND kdaktif='1'";
		$ssb = $rvs->query("SELECT * FROM t_ssb WHERE $whr")->result_array();
		if (! $ssb) return array();

		$g = 'kdgiat';  $o = 'kdoutput';  $s = 'kdsoutput';  $k = 'kdkmpnen';  $u = 'kdskmpnen';  $a = 'kdakun';
		$arr[0] = array('col'=>"$g,$o,$s,$k,$u", 'whr'=>"$g<>'0000' AND $o<>'000' AND $s<>'000' AND $k<>'000' AND $u<>'00'");
		$arr[1] = array('col'=>"$g,$o,$s,$k", 'whr'=>"$g<>'0000' AND $o<>'000' AND $s<>'000' AND $k<>'000' AND $u='00'");
		$arr[2] = array('col'=>"$o,$k", 'whr'=>"$g='0000' AND $o<>'000' AND $s='000' AND $k<>'000' AND $u='00'");

		$dat = array();
		foreach ($arr as $k=>$v) {
			$cols = $v['col'];  $whrs = $v['whr']; 
			$ssb  = $rvs->query("SELECT CONCAT($cols) kode, kelssb, CONCAT($cols,kdakun) akun FROM t_ssb WHERE $whr AND $whrs")->result_array();
			$kel  = $this->fc->ToArr($ssb, 'kode');
			$ssb  = $this->fc->ToArr($ssb, 'akun');

			if ($ssb) {
				$inv  = "CONCAT($cols)". $this->fc->InVar($ssb, 'kode');
				$whri = (substr($dbs,0,6) == 'telaah') ? "kdsatker='$kdsatker' AND LEFT(rev_id,2)='$this->jnsforum'" : "kdsatker='$kdsatker' AND rev_id='$id'";
				$itm  = $db->query("SELECT CONCAT($cols) kode, CONCAT($cols,kdakun) ssb, thang,kddept,kdunit,'' kelssb,kdgiat,kdoutput,kdsoutput,kdkmpnen,trim(kdskmpnen) kdskmpnen,kdakun, sum(jumlah) jumlah FROM d_item WHERE $whri AND $inv GROUP BY 1,2,3,4,5,6,7,8,9,10,11")->result_array();

				foreach ($itm as $ky=>$vl) {
					if (array_key_exists($vl['kode'], $kel)) $itm[$ky]['kelssb'] =  $kel[ $vl['kode'] ]['kelssb']; 
					if (array_key_exists($vl['ssb'],  $ssb)) unset($itm[$ky]); 
				}

				if ($itm) $dat = array_merge_recursive($dat, $itm);
			}
		}

		if ($dat) {
			foreach ($arr as $k=>$v) {
				$cols = $v['col'];  $whrs = $v['whr']; 
				$ssb  = $rvs->query("SELECT CONCAT($cols) kode, kelssb, CONCAT($cols,kdakun) akun FROM t_ssb WHERE $whr AND $whrs")->result_array();
				$ssb  = $this->fc->ToArr($ssb, 'akun');

				if ($ssb) 
					foreach ($dat as $ky=>$vl) {
						$txt = '';
						foreach (explode(',', $cols) as $key=>$val) $txt .= $vl[$val];
						if (array_key_exists($txt . $vl['kdakun'],  $ssb)) unset($dat[$ky]); 
					}
			}
		}
		// echo $this->fc->browse($dat); // exit;
		return $dat;
	}

	function validasi_ssb_kel($dbs,  $ssb) {
		$db  = $this->load->database("revisi$this->thang", TRUE);
		$arr = array();
		foreach ($ssb as $row) {
			$arr[ $row['kddept'].$row['kdunit'].$row['kelssb'] ]['kelssb'] = $row['kddept'].$row['kdunit'].$row['kelssb'];
			$arr[ $row['kddept'].'00'.$row['kelssb'] ]['kelssb'] = $row['kddept'].'00'.$row['kelssb'];
		}

		$qry = $db->query("SELECT * FROM t_ssb WHERE kdaktif='1' AND CONCAT(kddept,kdunit,kelssb) ". $this->fc->InVar($arr, 'kelssb') ." ORDER BY 1,2,3,4 DESC,5,6,7,8,9,10");
		return $qry->result_array();
	}

	function validasi_ssb_referensi( $ssb ) {
		$set['0'] = array('key'=>"kddept,kdunit,kelssb", 'kd'=>"kddept,'.',kdunit,'.',kelssb", 'nm'=>'nmkelssb', 'tbl'=>'t_ssb_kel');
		$set['1'] = array('key'=>$set['0']['key'].',kdgiat,kdoutput', 'kd'=>"kdgiat,'.',kdoutput", 'nm'=>'nmoutput', 'tbl'=>'t_output');
		$set['2'] = array('key'=>$set['1']['key'].',kdsoutput', 'kd'=>$set['1']['kd'].",'.',kdsoutput", 'nm'=>'nmsoutput', 'tbl'=>'t_soutput');
		$set['3'] = array('key'=>$set['2']['key'].',kdkmpnen', 'kd'=>$set['2']['kd'].",'.',kdkmpnen", 'nm'=>'nmkmpnen', 'tbl'=>'t_kmpnen');
		$set['4'] = array('key'=>$set['3']['key'].',kdskmpnen', 'kd'=>$set['3']['kd'].",'.',kdskmpnen", 'nm'=>"''", 'tbl'=>'t_ssb');
		$set['5'] = array('key'=>$set['4']['key'].',kdakun', 'kd'=>"kdakun", 'nm'=>'nmakun', 'tbl'=>'t_akun');

		$kel = array(); $out = array(); $sout = array(); $kmp = array(); $skmp = array(); $akun = array();
		foreach ($ssb as $key=>$row) {
			$kddept = $row['kddept']; $kdunit = $row['kdunit']; $kelssb = $row['kelssb']; $kdgiat = $row['kdgiat']; $kdoutput = $row['kdoutput']; $kdsoutput = $row['kdsoutput']; $kdkmpnen = $row['kdkmpnen']; $kdskmpnen = $row['kdskmpnen']; $kdakun = $row['kdakun'];

			$kode = "$kddept.$kdunit.$kelssb";  $kdkey = "$kddept$kdunit$kelssb";
			if (! array_key_exists($kdkey, $kel)) $kel[$kdkey] = array('level'=>'Kelompok', 'kode'=>$kode, 'uraian'=>'', 'kdkey'=>$kdkey);

			$kode = "$kdgiat.$kdoutput";  $kdkey .= ".$kdgiat$kdoutput";
			if (! array_key_exists($kdkey, $out)) $out[$kdkey] = array('level'=>($this->thang<='2020') ? 'Output':'K R O', 'kode'=>$kode, 'uraian'=>'', 'kdkey'=>$kdkey);

			$kode .= ".$kdsoutput";  $kdkey .= ".$kdsoutput";
			if (! array_key_exists($kdkey, $sout)) $sout[$kdkey] = array('level'=>($this->thang<='2020') ? 'Sub Output':'Rincian Output', 'kode'=>$kode, 'uraian'=>'', 'kdkey'=>$kdkey);

			$kode .= ".$kdkmpnen";  $kdkey .= ".$kdkmpnen";
			if (! array_key_exists($kdkey, $kmp)) $kmp[$kdkey] = array('level'=>'Komponen', 'kode'=>$kode, 'uraian'=>'', 'kdkey'=>$kdkey);

			$kode = "$kdskmpnen";  $kdkey .= ".$kdskmpnen";
			if (! array_key_exists($kdkey, $skmp)) $skmp[$kdkey] = array('level'=>'Sub Komponen', 'kode'=>$kode, 'uraian'=>'', 'kdkey'=>$kdkey);

			$kode = "$kdakun";  $kdkey .= ".$kdakun";
			if (! array_key_exists($kdkey, $akun)) $akun[$kdkey] = array('level'=>'Akun', 'kode'=>$kode, 'uraian'=>'', 'kdkey'=>$kdkey);
		}

		$ssb = array();
		for ($i=0; $i < count($set)-0; $i++) {
			$kd = $set[$i]['kd']; $nm = $set[$i]['nm']; $key = $set[$i]['key']; $tbl = $set[$i]['tbl'];
			if ($i == 0) $tmp = $kel; if ($i == 1) $tmp = $out; if ($i == 2) $tmp = $sout; if ($i == 3) $tmp = $kmp; if ($i == 4) $tmp = $skmp; if ($i == 5) $tmp = $akun;

			if (count($tmp) > 0) {
				if  ($tbl == 't_ssb_kel') 
					$qry = $this->dbrvs->query("SELECT CONCAT($kd) kode, $nm uraian FROM $tbl WHERE CONCAT($kd) ". $this->fc->InVar($tmp, 'kode'));
				elseif ($tbl == 't_ssb')
					$qry = $this->dbrvs->query("SELECT CONCAT($kd) kode, $nm uraian FROM $tbl WHERE CONCAT($kd) ". $this->fc->InVar($tmp, 'kode'));
				else 
					$qry = $this->dbref->query("SELECT CONCAT($kd) kode, $nm uraian FROM $tbl WHERE CONCAT($kd) ". $this->fc->InVar($tmp, 'kode'));

				$tbl = $this->fc->ToArr($qry->result_array(), 'kode');
				$tmp = $this->fc->Left_Join($tmp, $tbl, "kode, uraian=uraian");
				$ssb = array_merge($ssb, $tmp);
			}
		}
		return $this->fc->array_index($ssb, 'kdkey');
	}

	function extract_adk_load_csv($id, $file) {
		$dir = "files/revisi/$id";
		if (! file_exists("$dir/$file")) return 'ADK Empty lo';

		$adk  = $this->fc->extract_rar($dir, $file, $this->list, 'csv');
		$dept = $this->dept; // $this->session->userdata('kddept');
		$unit = $this->unit; // $this->session->userdata('kdunit');
		$whr  = "kddept='$dept' AND kdunit='$unit' AND rev_id='$id'";
		if (! $adk) return 'Not Found';

		$sas = $this->fc->extract_rar($dir, $file, array('d_item'), 'xml');
		if ($sas) {
			$xml = "$dir/tmp/". $sas[0]['file'];
			$xml = simplexml_load_file($xml);
			foreach($xml->c_item[1] as $k=>$v) 
				if ($k == 'tidak_valid') return 'ADK tidak valid (ADK SAS)';
		}

		foreach ($adk as $row) {
			$tbl = $row['type'];
			$csv = "$dir/tmp/". $row['file'];

			file_put_contents($csv, str_replace('\\', '/', file_get_contents($csv)));
			file_put_contents($csv, str_replace("^$this->thang^|^01^", "^$id^|^$this->thang^|^01^", file_get_contents($csv)));

			if ($tbl == 'd_output' or $tbl == 'd_soutput') file_put_contents($csv, str_replace(',', '.', file_get_contents($csv)));
			if ($tbl == 'd_pdpt') file_put_contents($csv, str_replace("^$this->thang^|^", "^$id^|^$this->thang^|^", file_get_contents($csv)));
		
			$this->dbtmp->query("DELETE FROM $tbl WHERE kddept='$dept' AND kdunit='$unit' AND rev_id='$id'");
			$this->dbtmp->query("LOAD DATA LOCAL INFILE '$csv' INTO TABLE $tbl FIELDS TERMINATED BY '|' OPTIONALLY ENCLOSED BY '^' LINES TERMINATED BY '\r\n'");
			unlink($csv);
		}


		$col = "'RA' jenis, '$id' rev_id, thang, kdjendok, kddept, kdunit, kdsatker, '00' kdfungsi, '00' kdsfung, kdprogram, kdgiat, kdoutput, kdlokasi, kdkabkota, kddekon, kdsoutput, kdkmpnen, trim(kdskmpnen) kdskmpnen, kdkppn";
		$sel = "kdctarik, register, substr(kdakun,1,2) kdjenbel, kdakun, '0' kdikat";
		$whr = "WHERE kddept='$dept' AND kdunit='$unit' AND rev_id='$id'";
		$grp = "1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,22,23,24,25,26";

		$rkp = array();
		$sql = "SELECT $col, kdbeban, kdjnsban, $sel, '' kdblokir, 0 volume, '' volkmp, 0 jml_blokir, Sum(jumlah) jml_pagu FROM d_item $whr AND kdbeban NOT IN ('B','I') GROUP BY $grp";
		$qry = $this->dbtmp->query($sql);
		$rkp = array_merge_recursive($rkp, $qry->result_array());

		$sql = "SELECT $col, kdbeban, kdjnsban, $sel, '' kdblokir, 0 volume, '' volkmp, 0 jml_blokir, Sum(paguphln+pagurkp) jml_pagu FROM d_item $whr AND kdbeban IN ('B','I') GROUP BY $grp";
		$qry = $this->dbtmp->query($sql);
		$rkp = array_merge_recursive($rkp, $qry->result_array());

		$sql = "SELECT $col, 'C' kdbeban, '0' kdjnsban, $sel, '' kdblokir, 0 volume, '' volkmp, 0 jml_blokir, Sum(pagurmp) jml_pagu FROM d_item $whr AND kdbeban IN ('B','I') GROUP BY $grp";
		$qry = $this->dbtmp->query($sql);
		$rkp = array_merge_recursive($rkp, $qry->result_array());

		$sql = "SELECT $col, kdbeban, kdjnsban, $sel, '*' kdblokir, 0 volume, '' volkmp, Sum(blokirphln+blokirrmp+blokirrkp+rphblokir) jml_blokir, 0 jml_pagu FROM d_item $whr AND kdblokir='*' GROUP BY $grp";
		$qry = $this->dbtmp->query($sql);
		$rkp = array_merge_recursive($rkp, $qry->result_array());
		if (count($rkp) == 0) return 'Error Satker';


		$qry  = $this->dbref->query("SELECT kdgiat, kdfungsi, kdsfung FROM t_giat WHERE kddept='$dept' AND kdunit='$unit'");
		$keg  = $this->fc->ToArr($qry->result_array(), 'kdgiat');
		$qry  = $this->dbtmp->query("SELECT Concat(thang,kdsatker,kdgiat,kdoutput,kdlokasi,kdsoutput) kode, sum(volsout) volkeg FROM d_soutput $whr GROUP BY 1");
		$vkeg = $this->fc->ToArr($qry->result_array(), 'kode');
		$qry  = $this->dbtmp->query("SELECT Concat(thang,kdsatker,kdgiat,kdoutput,kdlokasi,kdsoutput,kdkmpnen) kode, kdtema volkmp FROM d_kmpnen $whr");
		$vkmp = $this->fc->ToArr($qry->result_array(), 'kode');

		$no = 0;
	    foreach ($rkp as $row) {
	         $kode = $row['thang'].$row['kdsatker'].$row['kdgiat'].$row['kdoutput'].$row['kdlokasi'].$row['kdsoutput'];
	         if (array_key_exists($kode, $vkeg)) { $rkp[$no]['volume'] += $vkeg[$kode]['volkeg']; unset($vkeg[$kode]); }
	         $kode = $row['thang'].$row['kdsatker'].$row['kdgiat'].$row['kdoutput'].$row['kdlokasi'].$row['kdsoutput'].$row['kdkmpnen'];
	         if (array_key_exists($kode, $vkmp)) { $rkp[$no]['volkmp'] += trim($vkmp[$kode]['volkmp']); unset($vkmp[$kode]); }
	         if (array_key_exists($row['kdgiat'], $keg)) {
	            $rkp[$no]['kdfungsi'] = $keg[$row['kdgiat']]['kdfungsi'];
	            $rkp[$no]['kdsfung']  = $keg[$row['kdgiat']]['kdsfung'];
	         }
	         if (strpos('kmp 001 002', $row['kdkmpnen'])) $rkp[$no]['kdikat'] = '1';
	         $no++;
	    }
 
		$csv = "$dir/tmp/rekap.csv";
		$fop = fopen($csv, 'w');
		foreach ($rkp as $row)  fputcsv($fop, $row, '|', '#');
		fclose($fop);

		$this->dbtmp->query("DELETE FROM v_rekap WHERE jenis='RA' AND kddept='$dept' AND kdunit='$unit' AND rev_id='$id'");
		$this->dbtmp->query("LOAD DATA LOCAL INFILE '$csv' INTO TABLE v_rekap FIELDS TERMINATED BY '|' OPTIONALLY ENCLOSED BY '#'");

		unlink("$dir/tmp/rekap.csv");
		unlink("$dir/tmp/index.html");
		rmdir("$dir/tmp");
		return 'Valid';
	}

	function pagu_realisasi($kdsatker, $rev_id, $dbs, $unit, $min=null) {
		$db  = $this->load->database($dbs, TRUE);
		$this->dept = substr($unit,0,3); 
		$this->unit = substr($unit,3,2);

		$key = "kddept, kdunit, kdsatker, kdgiat, kdoutput, kdlokasi, kdkabkota, kdkppn, kdakun";
		$whr = "jenis='RA' AND kddept='$this->dept' AND kdunit='$this->unit' AND kdsatker='$kdsatker' AND rev_id='$rev_id'";
		$qry = $db->query("SELECT concat($key) kdkey, $key, kdprogram, sum(jml_pagu) pagu, 0 spm, 0 sisa FROM v_rekap WHERE $whr GROUP BY 1");
		$dat = $this->fc->ToArr($qry->result_array(), 'kdkey');

		$key = "baes1, kdsatker, kegiatan, output, lokasi, kppn_lra, akun";
		$qry = $this->dbrvs->query("SELECT concat($key) kdkey, sum(rphreal) spm FROM d_realisasi WHERE kdsatker='$kdsatker'GROUP BY 1");
		$spm = $this->fc->ToArr($qry->result_array(), 'kdkey');

		$min = array();
		foreach ($dat as $key=>$row) {
			if (array_key_exists($key, $spm)) $dat[$key]['spm'] = $spm[$key]['spm'];
			$dat[$key]['sisa'] = $dat[$key]['pagu'] - $dat[$key]['spm'];
			if ($dat[$key]['sisa'] < 0) array_push($min, $row);
		}

		$arr['tabel'] = $this->pagu_realisasi_leveling($dat);
		return $min ? $min : $arr;
	}

	function pagu_realisasi_leveling($pagu) {
		$detil = array();
		$std   = array('recid'=>'', 'kode'=>'', 'uraian'=>'', 'pagu'=>0, 'spm'=>0, 'sisa'=>0, 'level'=>'');

		$query = $this->dbref->query("SELECT kdsatker kode, nmsatker uraian FROM t_satker WHERE kdsatker ". $this->fc->InVar($pagu, 'kdsatker'));
		$t_stk = $this->fc->ToArr($query->result_array(), 'kode');
		$query = $this->dbref->query("SELECT concat(kddept,'.',kdunit,'.',kdprogram) kode, nmprogram uraian FROM t_program WHERE kddept ". $this->fc->InVar($pagu, 'kddept'));
		$t_prg = $this->fc->ToArr($query->result_array(), 'kode');
		$query = $this->dbref->query("SELECT kdgiat kode, nmgiat uraian FROM t_giat WHERE kdgiat ". $this->fc->InVar($pagu, 'kdgiat'));
		$t_keg = $this->fc->ToArr($query->result_array(), 'kode');
		$query = $this->dbref->query("SELECT concat(kdgiat,'.',kdoutput) kode, nmoutput uraian FROM t_output WHERE kdgiat ". $this->fc->InVar($pagu, 'kdgiat'));
		$t_out = $this->fc->ToArr($query->result_array(), 'kode');
		$query = $this->dbref->query("SELECT kdakun kode, nmakun uraian FROM t_akun WHERE kdakun ". $this->fc->InVar($pagu, 'kdakun'));
		$t_aku = $this->fc->ToArr($query->result_array(), 'kode');

		foreach ($pagu as $row) {
			$kddept=$row['kddept']; $kdunit=$row['kdunit']; $kdsatker=$row['kdsatker']; $kdprogram=$row['kdprogram']; $kdgiat=$row['kdgiat']; $kdoutput=$row['kdoutput']; $kdlokasi=$row['kdlokasi']; $kdkabkota=$row['kdkabkota']; $kdkppn=$row['kdkppn']; $kdakun=$row['kdakun']; $pagu=$row['pagu']; $spm=$row['spm']; $sisa=$row['sisa'];

			$arr=$std; $arr['pagu']=$row['pagu']; $arr['spm']=$row['spm']; $arr['sisa']=$row['sisa'];


			// Level 1 - SATKER
			$key = $kdsatker; $arr['recid'] = $key; $arr['kode'] = $kdsatker; $arr['level'] = '1';
			if (! array_key_exists($key, $detil)) {
				$detil[$key] = $arr;
				$detil[$key]['uraian'] = array_key_exists($arr['kode'], $t_stk) ? $t_stk[$arr['kode']]['uraian'] : ''; 
			} else { $detil[$key]['pagu'] += $pagu; $detil[$key]['spm'] += $spm; $detil[$key]['sisa'] += $sisa; }


			// Level 2 - PROGRAM
			$key .= ".$kddept.$kdunit.$kdprogram"; $arr['recid'] = $key; $arr['kode'] = "$kddept.$kdunit.$kdprogram"; $arr['level'] = '2';
			if (! array_key_exists($key, $detil)) {
				$detil[$key] = $arr;
				$detil[$key]['uraian'] = array_key_exists($arr['kode'], $t_prg) ? $t_prg[$arr['kode']]['uraian'] : ''; 
			} else { $detil[$key]['pagu'] += $pagu; $detil[$key]['spm'] += $spm; $detil[$key]['sisa'] += $sisa; }

			// Level 3 - KEGIATAN
			$key .= ".$kdgiat"; $arr['recid'] = $key; $arr['kode'] = $kdgiat; $arr['level'] = '3';
			if (! array_key_exists($key, $detil)) {
				$detil[$key] = $arr;
				$detil[$key]['uraian'] = array_key_exists($arr['kode'], $t_keg) ? $t_keg[$arr['kode']]['uraian'] : ''; 
			} else { $detil[$key]['pagu'] += $pagu; $detil[$key]['spm'] += $spm; $detil[$key]['sisa'] += $sisa; }

			// Level 4 - OUTPUT
			$key .= ".$kdoutput"; $arr['recid'] = $key; $arr['kode'] = "$kdgiat.$kdoutput"; $arr['level'] = '4';
			if (! array_key_exists($key, $detil)) {
				$detil[$key] = $arr;
				$detil[$key]['uraian'] = array_key_exists($arr['kode'], $t_out) ? $t_out[$arr['kode']]['uraian'] : ''; 
			} else { $detil[$key]['pagu'] += $pagu; $detil[$key]['spm'] += $spm; $detil[$key]['sisa'] += $sisa; }

			// Level 5 - AKUN
			$key .= ".$kdakun"; $arr['recid'] = $key; $arr['kode'] = $kdakun; $arr['level'] = '5';
			if (! array_key_exists($key, $detil)) {
				$detil[$key] = $arr;
				$detil[$key]['uraian'] = array_key_exists($arr['kode'], $t_aku) ? $t_aku[$arr['kode']]['uraian'] : ''; 
			} else { $detil[$key]['pagu'] += $pagu; $detil[$key]['spm'] += $spm; $detil[$key]['sisa'] += $sisa; }

		}
		return $detil;
	}

	function digistamp_grid($kdsatker, $dbmula, $idmula, $dbjadi, $idjadi) {
		$sele1 = "thang,kdjendok,kdsatker,kddept,kdunit,kdprogram,kdgiat,kdoutput,kdlokasi,kdkabkota,kddekon";
		$sele2 = "kdkppn,kdbeban,kdjnsban,kdctarik,register,kdakun,jumlah,paguphln,pagurmp,pagurkp,blokirphln,blokirrmp,blokirrkp,rphblokir";

		$qry_o = $dbmula->query("SELECT $sele1, Floor(sum(vol)) vol FROM d_output WHERE kdsatker='$kdsatker' AND rev_id='$idmula' GROUP BY $sele1");
		$qry_i = $dbmula->query("SELECT $sele1, $sele2 FROM d_item WHERE kdsatker='$kdsatker' AND rev_id='$idmula'");
		$mula  = $this->ds->hashing($qry_o->result_array(), $qry_i->result_array(), '1');
		$ds_a  = $this->ds->hashing($qry_o->result_array(), $qry_i->result_array());

		$qry_o = $dbjadi->query("SELECT $sele1, Floor(sum(vol)) vol FROM d_output WHERE kdsatker='$kdsatker' AND rev_id='$idjadi' GROUP BY $sele1");
		$qry_i = $dbjadi->query("SELECT $sele1, $sele2 FROM d_item WHERE kdsatker='$kdsatker' AND rev_id='$idjadi'");
		$jadi  = $this->ds->hashing($qry_o->result_array(), $qry_i->result_array(), '1');
		$ds_b  = $this->ds->hashing($qry_o->result_array(), $qry_i->result_array());

		$outp = $jenb = array();
		foreach ($mula as $row) {
			$o = $row['kdgiat'].'.'.$row['kdoutput']; $l = $row['kdlokasi'].'.'.$row['kdkabkota']; $d = $row['kddekon']; $v = $row['vol']; $m = $row['md5']; 			
			if ($row['kdjnsbel'] == '**') 
				$outp[$o] = array('output'=>$o, 'lokasi'=>$l, 'dekon'=>$d, 'volume'=>$v, 'md5'=>$m, 'm_lokasi'=>'', 'm_dekon'=>'', 'm_volume'=>0, 'm_md5'=>'', 'status'=>0);

			$j = $row['kdjnsbel']; $k = $row['kdkppn']; $b = $row['kdbeban'].'.'.$row['kdjnsban'].'.'.$row['kdctarik']; $jm = $row['jumlah']; $ph = $row['paguphln'] + $row['pagurmp'] + $row['pagurkp']; $bl = $row['blokir'];		
			if ($row['register'] == '********') $r = ''; else $r = $row['register'];			
			if ($row['kdjnsbel'] != '**' and $row['kdakun'] == '000000') 
				$jenb["$o.$j"] = array('output'=>$o, 'kdjnsbel'=>$j, 'lokasi'=>$l, 'dekon'=>$d, 'kppn'=>$k, 'beban'=>$b, 'register'=>$r, 'jumlah'=>$jm, 'phln'=>$ph, 'blokir'=>$bl, 'md5'=>$m, 'm_lokasi'=>'', 'm_dekon'=>'', 'm_kppn'=>'', 'm_beban'=>'', 'm_register'=>'', 'm_jumlah'=>0, 'm_phln'=>0, 'm_blokir'=>0, 'm_md5'=>'', 'status'=>0);
		} 

		foreach ($jadi as $row) {
			$o = $row['kdgiat'].'.'.$row['kdoutput']; $l = $row['kdlokasi'].'.'.$row['kdkabkota']; $d = $row['kddekon']; $v = $row['vol']; $m = $row['md5'];
			if ($row['kdjnsbel'] == '**') {			
				if (! array_key_exists($o, $outp))
					$outp[$o] = array('output'=>$o, 'lokasi'=>'', 'dekon'=>'', 'volume'=>'', 'md5'=>'', 'm_lokasi'=>$l, 'm_dekon'=>$d, 'm_volume'=>$v, 'm_md5'=>$m, 'status'=>0);
				else 
					$outp[$o]['m_lokasi'] = $l; $outp[$o]['m_dekon'] = $d; $outp[$o]['m_volume'] = $v; $outp[$o]['m_md5'] = $m;
			}

			$j = $row['kdjnsbel']; $k = $row['kdkppn']; $b = $row['kdbeban'].'.'.$row['kdjnsban'].'.'.$row['kdctarik']; $jm = $row['jumlah']; $ph = $row['paguphln'] + $row['pagurmp'] + $row['pagurkp']; $bl = $row['blokir'];		
			if ($row['register'] == '********') $r = ''; else $r = $row['register'];			
			if ($row['kdjnsbel'] != '**' and $row['kdakun'] == '000000') {
				if (! array_key_exists("$o.$j", $jenb))
					$jenb["$o.$j"] = array('output'=>$o, 'kdjnsbel'=>$j, 'lokasi'=>'', 'dekon'=>'', 'kppn'=>'', 'beban'=>'', 'register'=>'', 'jumlah'=>0, 'phln'=>0, 'blokir'=>0, 'md5'=>'', 'm_lokasi'=>$l, 'm_dekon'=>$d,'m_kppn'=>$k, 'm_beban'=>$b, 'm_register'=>$r, 'm_jumlah'=>$jm, 'm_phln'=>$ph, 'm_blokir'=>$bl, 'm_md5'=>$m, 'status'=>0);
				else 
					$jenb["$o.$j"]['m_lokasi'] = $l; $jenb["$o.$j"]['m_dekon'] = $d; $jenb["$o.$j"]['m_kppn'] = $k; $jenb["$o.$j"]['m_beban'] = $b; $jenb["$o.$j"]['m_register'] = $r; $jenb["$o.$j"]['m_jumlah'] = $jm; $jenb["$o.$j"]['m_phln'] = $ph; $jenb["$o.$j"]['m_blokir'] = $bl; $jenb["$o.$j"]['m_md5'] = $m;
			}
		}
		// echo $this->ds->browse($outp); exit;

		foreach ($outp as $key=>$row) {
			if ($row['md5'] == $row['m_md5']) $outp[$key]['status'] = 1;
			unset($outp[$key]['md5']); unset($outp[$key]['m_md5']); 
		}

		foreach ($jenb as $key=>$row) {
			if ($row['md5'] == $row['m_md5']) $jenb[$key]['status'] = 1;
			unset($jenb[$key]['md5']); unset($jenb[$key]['m_md5']); 
		}

		$satk['ds'] = array('satker'=>$kdsatker, 'revisi_ke'=>$idmula, 'digistamp'=>$ds_a, 'no_tiket'=>$idjadi, 'm_digistamp'=>$ds_b, 'status'=> ($ds_a == $ds_b) ? 1 : 0);
		// echo $this->fc->browse($satk,'none','1-2-2-1'); exit;

		$arr = array(
			'satk' => $this->fc->browse($satk,'none','1-2-2-1'),
			'outp' => $this->fc->browse($outp, '-volume-m_volume', '1-3-3-1'),
			'jenb' => $this->fc->browse($jenb, '-jumlah-phln-blokir-m_jumlah-m_phln-m_blokir', '2-8-8-1')
		);
		return $arr;
	}

	function update_data_kpa($kdsatker, $db, $rev_id, $type=null) {
		$whr = "kdsatker='$kdsatker' AND rev_id='$rev_id'";
		$qry = $db->query("SELECT thang, kddept, kdunit, kddekon FROM d_output WHERE $whr LIMIT 1");
		$stk = $qry->row();
		$qry = $db->query("SELECT * FROM d_kpa WHERE $whr");
		$kpa = $qry->row();

		if (! $kpa) {
			if ($type == null) {
				$tag  = ($rev_id > '00') ? $rev_id : '';
				$adk  = 'd01_'. $stk->kddept.$stk->kdunit .'_00_'. $kdsatker .'_'. $stk->kddekon .'.s'. substr($stk->thang,2,2) . $tag;
				$dipa = "/backup154/adk_rkakl$this->thang/$stk->kddept/$stk->kdunit/ADK/$adk";
			} else {
				$qry  = $this->dbrvs->query("SELECT doc_adk FROM revisi WHERE rev_id='$rev_id' LIMIT 1");
				$rvs  = $qry->row();
				$adk  = $rvs->doc_adk;
				$dipa = "files/revisi/$rev_id/$adk";
			}

			if (file_exists($dipa)) {
				$dir = 'files/adkdipa';
				$this->fc->smartcopy($dipa, $dir);
				if ($type == null) $arr = $this->fc->extract_rar($dir, $adk, array('d_kpa','d_trktrm'), 'xml');
				else $arr = $this->fc->extract_rar($dir, $adk, array('d_kpa','d_trktrm'), 'XML');
				unlink("$dir/$adk");

				foreach ($arr as $row) {
					$tbl = $row['type'];
					$xml = "$dir/tmp/". $row['file'];
					$ext = str_replace('d_', 'c_', $tbl);

					file_put_contents($xml, str_replace('<thang>', "<rev_id>$rev_id</rev_id>\n\t\t<thang>", file_get_contents($xml)));

					$db->query("DELETE FROM $tbl WHERE $whr");
					$db->query("LOAD XML LOCAL INFILE '$xml' INTO TABLE $tbl ROWS IDENTIFIED BY '<$ext>'");
					unlink($xml);
				}
			}
		}
	}

	function recreate_rekap($kdsatker) {
		$rev = $this->dbrvs->query("SELECT kdsatker, kddept, kdunit, max(revisike) id FROM v_adk_dipa WHERE kdsatker='$kdsatker' GROUP BY 1,2,3")->row();
		$whr  = "WHERE kdsatker='$kdsatker' AND rev_id='$rev->id'";


		$col = "'RA' jenis, '$rev->id' rev_id, thang, kdjendok, kddept, kdunit, kdsatker, '00' kdfungsi, '00' kdsfung, kdprogram, kdgiat, kdoutput, kdlokasi, kdkabkota, kddekon, kdsoutput, kdkmpnen, trim(kdskmpnen) kdskmpnen, kdkppn";
		$sel = "kdctarik, register, substr(kdakun,1,2) kdjenbel, kdakun, '0' kdikat";
		$grp = "1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,22,23,24,25,26";

		$rkp = array();
		$sql = "SELECT $col, kdbeban, kdjnsban, $sel, '' kdblokir, 0 volume, '' volkmp, 0 jml_blokir, Sum(jumlah) jml_pagu FROM d_item $whr AND kdbeban NOT IN ('B','I') GROUP BY $grp";
		$qry = $this->dbrvs->query($sql);
		$rkp = array_merge_recursive($rkp, $qry->result_array());

		$sql = "SELECT $col, kdbeban, kdjnsban, $sel, '' kdblokir, 0 volume, '' volkmp, 0 jml_blokir, Sum(paguphln+pagurkp) jml_pagu FROM d_item $whr AND kdbeban IN ('B','I') GROUP BY $grp";
		$qry = $this->dbrvs->query($sql);
		$rkp = array_merge_recursive($rkp, $qry->result_array());

		$sql = "SELECT $col, 'C' kdbeban, '0' kdjnsban, $sel, '' kdblokir, 0 volume, '' volkmp, 0 jml_blokir, Sum(pagurmp) jml_pagu FROM d_item $whr AND kdbeban IN ('B','I') GROUP BY $grp";
		$qry = $this->dbrvs->query($sql);
		$rkp = array_merge_recursive($rkp, $qry->result_array());

		$sql = "SELECT $col, kdbeban, kdjnsban, $sel, '*' kdblokir, 0 volume, '' volkmp, Sum(blokirphln+blokirrmp+blokirrkp+rphblokir) jml_blokir, 0 jml_pagu FROM d_item $whr AND kdblokir='*' GROUP BY $grp";
		$qry = $this->dbrvs->query($sql);
		$rkp = array_merge_recursive($rkp, $qry->result_array());
		if (count($rkp) == 0) return 'Error Satker';


		$qry  = $this->dbref->query("SELECT kdgiat, kdfungsi, kdsfung FROM t_giat WHERE kddept='$dept' AND kdunit='$unit'");
		$keg  = $this->fc->ToArr($qry->result_array(), 'kdgiat');
		$qry  = $this->dbrvs->query("SELECT Concat(thang,kdsatker,kdgiat,kdoutput,kdlokasi,kdsoutput) kode, sum(volsout) volkeg FROM d_soutput $whr GROUP BY 1");
		$vkeg = $this->fc->ToArr($qry->result_array(), 'kode');
		$qry  = $this->dbrvs->query("SELECT Concat(thang,kdsatker,kdgiat,kdoutput,kdlokasi,kdsoutput,kdkmpnen) kode, kdtema volkmp FROM d_kmpnen $whr");
		$vkmp = $this->fc->ToArr($qry->result_array(), 'kode');

		$no = 0;
	    foreach ($rkp as $row) {
	         $kode = $row['thang'].$row['kdsatker'].$row['kdgiat'].$row['kdoutput'].$row['kdlokasi'].$row['kdsoutput'];
	         if (array_key_exists($kode, $vkeg)) { $rkp[$no]['volume'] += $vkeg[$kode]['volkeg']; unset($vkeg[$kode]); }
	         $kode = $row['thang'].$row['kdsatker'].$row['kdgiat'].$row['kdoutput'].$row['kdlokasi'].$row['kdsoutput'].$row['kdkmpnen'];
	         if (array_key_exists($kode, $vkmp)) { $rkp[$no]['volkmp'] += trim($vkmp[$kode]['volkmp']); unset($vkmp[$kode]); }
	         if (array_key_exists($row['kdgiat'], $keg)) {
	            $rkp[$no]['kdfungsi'] = $keg[$row['kdgiat']]['kdfungsi'];
	            $rkp[$no]['kdsfung']  = $keg[$row['kdgiat']]['kdsfung'];
	         }
	         if (strpos('kmp 001 002', $row['kdkmpnen'])) $rkp[$no]['kdikat'] = '1';
	         $no++;
	    }
		// echo $this->fc->browse($rkp); exit;
 
		$csv = "files/revisi/rekap.csv";
		$fop = fopen($csv, 'w');
		foreach ($rkp as $row)  fputcsv($fop, $row, '|', '#');
		fclose($fop);

		$this->dbrvs->query("DELETE FROM v_rekap WHERE jenis='RA' AND kddept='$rev->kddept' AND kdunit='$rev->kdunit' AND kdsatker='$kdsatker' AND rev_id='$rev->id'");
		$this->dbrvs->query("LOAD DATA LOCAL INFILE '$csv' INTO TABLE v_rekap FIELDS TERMINATED BY '|' OPTIONALLY ENCLOSED BY '#'");

		// unlink("files/revisi/rekap.csv");
		return 'Valid';
	}

}
