<?php 
class Rapat_ruang_model extends CI_Model {
	public function get_data() {
		$whr  = "";
		$cari = $this->session->userdata('cari');
			if ($cari) {
	    		$whr = " where " ;
	    		$ref = " where " ; 
				$arr = explode(' ', $cari);
				for ($i=0; $i<count($arr); $i++) {
					$str  = $arr[$i];
					$whr .= "nmrapat like '%$str%' or kapasitas like '%$str%' ";
					$ref .= "letak like '%$str%' ";
					if ($i<count($arr)-1) {
		      			$whr .= " or ";
		      			$ref .= " or ";
		      		}
				}
	    }

		$query = $this->db->query("select * from t_rapat_ruang $whr ORDER BY kdrapat ASC");
		$hasil = $this->fc->ToArr( $query->result_array(), 'kdrapat');
		return $hasil;
	}

	public function get_row( $kdrapat=NULL) {
		if ( is_null($kdrapat) ) $kdrapat='999999';
		$query = $this->db->query("SELECT * FROM t_rapat_ruang WHERE kdrapat=$kdrapat");
		$hasil = $query->row_array();

		if (! $hasil) {
			$hasil = $this->db->query("SELECT * FROM t_rapat_ruang LIMIT 1")->row_array();
			foreach ($hasil as $k=>$v) 
				$hasil[$k] = '';
		}
		return $hasil;
	}

	public function save2() {
		$action			  	= $_POST['ruhi'];
		$kdrapat     		= $_POST['kdrapat'];
		$nmrapat	  		= $_POST['nmrapat'];
		$kdaktif			= $_POST['kdaktif'];
		$letak				= $_POST['letak'];
		$luas				= $_POST['luas'];
		$kapasitas 			= $_POST['kapasitas'];
		$fasilitas			= $_POST['fasilitas'];
		$unit				= $_POST['unit'];
		$gambar		  		= $_FILES['gambar'];	

		if ($gambar) move_uploaded_file($gambar["tmp_name"], "files/rapat/".$gambar['name']);

		if ($action=='Rekam') {

			$sql = 	"insert into t_rapat_ruang (nmrapat,kdaktif,letak,luas,kapasitas,fasilitas,unit,gambar) values ('$nmrapat','$kdaktif','$letak','$luas','$kapasitas','$fasilitas','$unit','". $gambar['name']."')";
		}
		if ($action=='Ubah') {
			$sql = 	"update t_rapat_ruang set nmrapat='$nmrapat',kdaktif='$kdaktif',letak='$letak', luas='$luas', kapasitas='$kapasitas', fasilitas='$fasilitas', unit='$unit' where kdrapat=$kdrapat";
			if ( $gambar['name'] != '' )  	$this->db->query("update t_rapat_ruang set gambar='". $gambar['name'] ."' where kdrapat=$kdrapat" );
		}
		if ($action=='Hapus') {
			$sql = "delete from t_rapat_ruang where kdrapat=$kdrapat";
		}

		$query = $this->db->query($sql);
		return;
	}
}

