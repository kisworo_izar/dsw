<?php
class Panel_model extends CI_Model {

	function get_data() {
    $data['table'] = $this->get_rows( 'data' );
    $data['kolom'] = $this->get_rows( 'stru' );
    return $data;
	}

	function get_rows( $mode ) {
		switch( $mode ) {
    case "data":
      $query = $this->db->query("Select * From t_menu Limit 50");
      return json_encode( $query->result_array() );
      break;

    case "stru":
      $query = $this->db->query("Select * From t_panel_admin Where tabel='t_menu' Order By urutan");
      return $query->result_array();
      break;

    default:
      $query = $this->db->query("Select * From t_menu Where idmenu=$mode");
      return $query->row_array();
      break;
		}
	}

}
