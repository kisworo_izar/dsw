<?php 
class Stol_lampiran_model extends CI_Model {

	public function get_grid($idst, $tab, $es2) {
		// Query ST Informasi
		$query = $this->db->query("Select tglst, nost, jnsst, perihal, status, lokasi, tglawal, tglakhir From st Where idst=$idst "); 
		$data['st'] = $query->row_array();

		// Check Status, Jika '1' KONSEP -> ST_PEG dan '2' DISTRIBUSI -> ST_PEG_FINAL
		if ($data['st']['status']=='1') $table='st_peg'; else $table='st_peg_final';

		// Query Data Lampiran
		$sql = "Select a.nip,a.nama nmuser ,if(b.kdeselon!='',b.kdeselon,'aa') kdeselon, a.jabatan, pangkat, if(b.golongan!='',b.golongan,a.gol) golongan, if(d.nmso!='',d.nmso,a.direktorat) nmso, honor, lokasi, idstlain, tglawal, tglakhir, '' tooltip,a.direktorat 
				From $table a Left Join t_user b On a.nip=b.nip 
				Left Join t_so d On Concat(Left(b.kdso,2),'0000')=d.kdso
				Where idst=$idst Order By kdeselon, golongan DESC";
		$query = $this->db->query( $sql );
		$data['lamp']= $query->result_array();
		// echo '<pre>';print_r($data['lamp']);exit();
		$data['lamp']= $this->tooltip( $data['lamp'] );

		//$query = $this->db->query("Select Left(kdso,2) es2, nmso From t_so Where Left(kdso,2)!='00' And Right(kdso,4)='0000' Order By 1");
		$query = $this->db->query("Select Left(kdso,2) es2, nmso From t_so Where Right(kdso,4)='0000' Order By 1");
		$data['dja'] = $query->result_array();
		// $data['dja']['ex'] = array('es2'=>'ex', 'nmso'=>'EKSTERNAL DJA');


		// Condition CARI
		$whr  = "Left(a.kdso,2)='$es2'";
		$cari = $this->session->userdata('cari');
		if ($this->session->userdata('cari')) {
			$whr = ""; $arr = explode(' ', $this->session->userdata('cari'));
			for ($i=0; $i<count($arr); $i++) {
				$whr .= "nmuser Like '%". $arr[$i] ."%'";
				if ($i<count($arr)-1)  $whr .= " And ";
			}
		}

		// Query ESELON 3 - Sub Direktorat & Pegawai
		if ($es2!='ex') {
			$sql = "Select concat(a.kdso,kdeselon,nip) kdkey, concat(Left(a.kdso,4),'00') kdso, 
						nmuser, nip, jabatan, pangkat, golongan, kdeselon, nmso1 eselon2, '' idstlain, '' tooltip, '2' level
					From t_user a Left Join t_so b On concat(Left(a.kdso,2),'0000')=b.kdso 
					Where kdpeg='1' And nip Not In (Select Nip From $table Where idst=$idst) And $whr
					Order By 2,kdeselon,nmuser";
			$query = $this->db->query( $sql );
			$pegaw = $this->fc->ToArr($query->result_array(), 'kdkey');
			$pegaw = $this->check_st($pegaw, $data['st']['tglawal'], $data['st']['tglakhir']);

			if ($pegaw) $whrso = "Where kdso ". $this->fc->InVar($pegaw, 'kdso'); else $whrso = "";
			$query = $this->db->query("Select concat(kdso,'00','000000000000000000') kdkey, kdso, nmso nmuser, '' nip, '' jabatan, '' golongan, '' pangkat, '' kdeselon, '' idstlain, '' tooltip, '1' level From t_so $whrso");
			$esln3 = $query->result_array();
			$data['peg'] = $this->fc->array_index( array_merge_recursive($pegaw, $esln3), 'kdkey' );
		} else {
			$sql = "Select concat('ex-',nama) kdkey, 'ex' kdso, nama nmuser, nip, direktorat jabatan, '' pangkat, gol golongan, '99' kdeselon, direktorat eselon2, '' idstlain, '' tooltip, '2' level From st_peg_luar Where  nip Not In (Select Nip From st_peg Where idst=$idst) Order By 3";
			$query = $this->db->query( $sql );
			$pegaw = $query->result_array();
			$pegaw['ex'] = array('kdkey'=>'ex-', 'kdso'=>'ex', 'nmuser'=>' EKSTERNAL DJA', 'nip'=>'', 'jabatan'=>'', 'pangkat'=>'', 'golongan'=>'', 'kdeselon'=>'', 'idstlain'=>'', 'tooltip'=>'', 'level'=>'1');
			$data['peg'] = $this->fc->array_index($pegaw, 'nmuser');
		}

		$data['gol'] = $this->ref_gol();  
		$data['idst']= $idst;  
		$data['tab'] = $tab;  
		$data['es2'] = $es2;
		$data['cari']= $cari;
		return $data;
	}

	public function check_st( $arr, $tglawal, $tglakhir ) {
		$query = $this->db->query("Select nip, idst From st_peg Where '$tglawal' Between tglawal And tglakhir");
		$hasil = $this->fc->ToArr($query->result_array(), 'nip');
		foreach ($arr as $key=>$value) {
			$nip = $value['nip'];
			if ( array_key_exists($nip, $hasil) ) {
				$arr[$key]['idstlain'] = $hasil[$nip]['idst'];
			}
		}
		$arr = $this->tooltip( $arr );
		return $arr;
	}

	public function tooltip( $arr ) {
		if ($this->fc->InVar($arr, 'idstlain')=='in ()') return $arr;
		$query = $this->db->query("Select idst, tglst, nost, perihal From st Where idst ". $this->fc->InVar($arr, 'idstlain'));
		$hasil = $this->fc->ToArr($query->result_array(), 'idst');
		foreach ($arr as $key=>$value) {
			$idst = $value['idstlain'];
			if ( $idst>0 And array_key_exists($idst, $hasil) ) {
				$arr[$key]['tooltip'] = '<a href="#" data-toggle="tooltip" data-placement="bottom" title="Tgl. '.$this->fc->idtgl($hasil[$idst]['tglst'], 'tglfull') .' No.'. $hasil[$idst]['nost'] .' Hal.'. trim($hasil[$idst]['perihal'] .'">***</a>');
			}
		}
		return $arr;
	}

	public function ref_gol() {
		$db = $this->load->database('ref', TRUE);
		$query = $db->query("Select Upper(Concat(pangkat,' ',nmgol)) gol From t_gol Order By kdgol");

		$hasil = '<option value=""></option>';
		foreach ($query->result_array() as $row) $hasil .= '<option value="'. $row['gol'] .'">'. $row['gol'] .'</option>';
		return $hasil;
	}

	public function save( $id ) {
		$iduser  = $this->session->userdata('iduser');
		$status	 = $_POST['status']; 
		$lokasi  = $_POST['lokasi'];
		$tglawal = $_POST['tglawal'];
		$tglakhir= $_POST['tglakhir'];
		$idstlain= '';
		$honor   = '1';

		$peg = $_POST['peg'];
		foreach ($peg as $row) {
			$arr 		= explode('|', $row);
			$nip 		= $arr[0];  
			$nama 		= $arr[1];  
			$gol 		= $arr[2];  
			$jabatan	= $arr[3];
			$direktorat	= $arr[4];
			if ($arr[5]!='') { $honor = '0'; $idstlain = $arr[5]; } else { $honor = '1'; $idstlain = ''; }
			
			if ($status=='1') $table='st_peg'; else $table='st_peg_final'; 
			$sql = "Insert Into $table (idst,nip,nama,gol,jabatan,direktorat,lokasi,tglawal,tglakhir,honor,idstlain,iduser,tglupdate) Values ($id,'$nip','$nama','$gol','$jabatan','$direktorat',\"$lokasi\",'$tglawal','$tglakhir','$honor','$idstlain','$iduser',current_timestamp())";
			$query = $this->db->query( $sql );
		}
	}

	public function save_non_dja( $id ) {
		$iduser	 = $this->session->userdata('iduser');
		$status  = $_POST['status']; 
		$lokasi  = $_POST['lokasi'];
		$tglawal = $_POST['tglawal'];
		$tglakhir= $_POST['tglakhir'];
		$arrnip  = $_POST['nip'];
		$arrnama = $_POST['nama'];
		$arrgol  = $_POST['gol'];
		$arrinst = $_POST['inst'];
		$arrjabtn= $_POST['jabtn'];
		$kdeselon = 1;

		for ($i=0; $i<count($arrinst) ; $i++) { 
			if ($arrinst[$i]) {
				$inst  	 = $arrinst[$i];
				$jabtn   = $arrjabtn[$i];
				$gol   	 = $arrgol[$i];
				$nama  	 = $arrnama[$i];
				$nip   	 = $arrnip[$i];
				if (!$nip) $nip = 'r-'.substr(str_shuffle(md5($nama.$inst)),0,16); 
				
				if ($status=='1') $table='st_peg'; else $table='st_peg_final';
				$sql   = "Insert Into $table (idst,nip,nama,gol,direktorat,jabatan,lokasi,tglawal,tglakhir,honor,idstlain,iduser,tglupdate,kdeselon) Values ($id,'$nip','$nama','$gol','$inst','$jabtn',\"$lokasi\",'$tglawal','$tglakhir','1','','$iduser',current_timestamp(),'$kdeselon')";
				$query = $this->db->query( $sql );
			}
		}
	}

	public function save_edit_peg( $id, $status ) {
		$peg 		= $_POST['peg'];
		$lokasi		= $_POST['lokasi'];
		$tglawal	= $this->fc->ustgl($_POST['tglawal']);
		$tglakhir	= $this->fc->ustgl($_POST['tglakhir']);
		$arnip 		= explode(',', $peg);

		if ($status=='1') $table='st_peg'; else $table='st_peg_final';
		foreach ($arnip as $nip) $query = $this->db->query("Update $table Set lokasi=\"$lokasi\", tglawal=\"$tglawal\", tglakhir=\"$tglakhir\" Where idst=$id And nip='". $nip ."'");
	}
	
	public function delete( $id, $status ) {
		$peg = $_POST['peg'];
		
		if ($status=='1') $table='st_peg'; else $table='st_peg_final';
		foreach ($peg as $row) $query = $this->db->query("Delete From $table Where idst=$id And nip='$row'");
	}

}