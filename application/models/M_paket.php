<?php 
class M_paket extends CI_Model {
	public function get_data() {
		$whr  = "";
		$cari = $this->session->userdata('cari');

		if ($cari) {
			$whr = " where " ;
			$ref = " where " ;
			$arr = explode(' ', $cari);
			for ($i=0; $i<count($arr); $i++) {
				$str  = $arr[$i];
				$whr .= "keterangan like '%$str%' or nmambil like '%$str%' ";
				$ref .= "nmuser like '%$str%' ";
				if ($i<count($arr)-1) {
					$whr .= " or ";
					$ref .= " or ";
				}
			}
			$query = $this->db->query("select iduser from t_user $ref");
			$arr   = $query->result_array();
			if ($arr) {
				$whr .= " or ";
			}
			for ($i=0; $i<count($arr); $i++) {
				$str  = $arr[$i]['iduser'];
				$whr .= "idterima like '%$str%' ";
				if ($i<count($arr)-1) {
					$whr .= " or ";
				}
			}
		}

		// cari query data dari d_paket
		$query  = $this->db->query("Select *, concat(tahun,'.',idpaket) noid, ' ' nmterima From d_paket $whr Order By tahun Desc, idpaket Desc");

		// masukkan ke array dengan id (group by) 'idpaket'
		$paket  = $this->fc->ToArr( $query->result_array(), 'noid');

		// jika paket kosong
		if (! ($paket)) return $paket;

		// buat function kumpulan iduser dari array 'paket'	
		$invar1 = $this->fc->Invar( $paket, 'idterima');
		// $invar2 = $this->fc->Invar( $paket, 'idambil');

		// buat query iduser,nmuser dari t_user yang idusernya ada di array invar1
		$query  = $this->db->query("select iduser, nmuser, a.kdso, b.nmso1 from t_user a left join t_so b on concat(left(a.kdso,2),'0000')=b.kdso where iduser $invar1");

		// masukkan ke array dengan id (group by) 'iduser'
		$t_user = $this->fc->ToArr( $query->result_array(), 'iduser');

		// masukkan nama user di array
		foreach ($paket as $row) {
			$key = $row['idterima'];
			if ( array_key_exists($key, $t_user) ){
				$paket[ $row['noid'] ]['nmterima'] = $t_user[$key]['nmuser'];
				$paket[ $row['noid'] ]['nmso1'] = $t_user[$key]['nmso1'];
			}
		}
		return $paket;
	}

	// membuat function 'save' untuk penyimpanan 
	public function save() {
		$action     = $_POST['simpan'];
		$idpaket    = $_POST['idpaket'];
		$tahun      = $_POST['tahun'];
		$idterima   = $_POST['idterima'];
		$tglrekam   = $_POST['tglrekam'];
		$nmambil    = $_POST['nmambil'];
		$tglambil   = $_POST['tglambil'];
		$keterangan = $_POST['keterangan'];
		$iduser     = $this->session->userdata('iduser');

		// jika reakam maka melakukan ini.......
		if ($action=='Rekam') {
			$tahun = date("Y");
			$sql   = "insert into d_paket (idterima,tahun,tglrekam,nmambil,tglambil,keterangan,iduser) values ('$idterima','$tahun',current_timestamp(),'$nmambil','$tglambil','$keterangan','$iduser')";
			$query = $this->db->query( $sql );

			$query = $this->db->query("Select idpaket, tglrekam From d_paket Where idterima='$idterima' Order By tglrekam Desc Limit 1");
			$paket = $query->row_array();
			$query = $this->db->query("Insert Into d_notifikasi (iduser,judul,pesan,waktu,link,status) Values ('$idterima','Paket','Anda Mendapat paket dengan ID :". $paket['idpaket'] ."','". $paket['tglrekam'] ."','". site_url('paket') ."','0')");
		}
		// jika ubah paket melakukan ini.......
		if ($action=='Ubah') {
			$sql = 	"update d_paket set idterima='$idterima', tglrekam=current_timestamp(), nmambil='$nmambil', keterangan='$keterangan', iduser='$iduser' where idpaket='$idpaket' and tahun='$tahun'";
			$query = $this->db->query( $sql );
		}
		// jika ambil paket melakukan ini.......
		if ($action=='Ambil') {
			$sql = 	"update d_paket set idterima='$idterima', nmambil='$nmambil', tglambil=current_timestamp(), keterangan='$keterangan', iduser='$iduser' where idpaket='$idpaket' and tahun='$tahun'";
			$query = $this->db->query( $sql );
		}
		// jika hapus data melakukan ini........
		if ($action=='Hapus') {
			$sql = "delete from d_paket where idpaket='$idpaket' and tahun='$tahun'";
			$query = $this->db->query( $sql );
		}
		return;
	}

	// untuk tampilan autocomplete user
	function json_user(){
		$query = $this->db->query("select iduser, nmuser from t_user order by nmuser");
		$result = $query->result();
		if(count($result)>0){
			$json = "[";
			foreach ($result as $row) 
				$json .= '{ value: "'. trim($row->nmuser) .'", data: "'. trim($row->iduser) .'" },';
			$json .= "]";
		}
		return $json;
	}

	public function get_total() {
		$tahun = date("Y");
		$query  = $this->db->query("select $tahun tahun, count(*) total, sum(if(trim(nmambil)='', 1, 0)) baru from d_paket where tahun=$tahun");
		return $query->row_array();
	}

	public function get_data_dash( $array ) {
		foreach ($array as $key=>$value) {
			if ( trim($value['nmambil'])!='' ) unset( $array[$key] );
		}		
		return $array;
	}
}
