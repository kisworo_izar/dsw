<?php
class Revisi_model extends CI_Model {

	public function get_data( $depuni=null, $start, $end ) {
		$tgl = array('pus_tgl','t2_proses_tgl','t2_selesai_tgl','t3_proses_tgl','t3_selesai_tgl','t4_proses_tgl','t4_selesai_tgl','t5_proses_tgl','t5_selesai_tgl','t6_proses_tgl','t6_selesai_tgl','t7_proses_tgl','t7_selesai_tgl');
		$whr = "Where rev_tahun='".  date('Y') ."' And (";
		for ($i=0; $i<count($tgl); $i++) {
			$whr .= $tgl[$i] ." Between '$start' And '$end' ";
			if ($i<count($tgl)-1) $whr .= " Or "; else $whr .= ")";
		}
		$whr = "";
		//if ($depuni!=null) $whr .= " And  concat(kl_dept,'.',kl_unit)='$depuni' ";

		if (! (strrpos($this->session->userdata('idusergroup'), '001') or strrpos($this->session->userdata('idusergroup'), '002')) ) {
			$query = $this->db->query("Select kddept From revisi_wenang Where kdso='". $this->session->userdata('kdso') ."'");
			$hasil = $query->row_array();
			$whr = "Where kl_dept In ('". str_replace(",", "','", $hasil['kddept']) ."')";
		}
		// $whr = "";

		$query = $this->db->query("Select rev_tahun, rev_id, rev_tahap, rev_status, kl_dept, '' nmdept, concat(kl_dept,'.',kl_unit) kl_unit, '' nmunit, kl_surat_no, kl_surat_tgl, kl_surat_hal,
			pus_status, pus_tgl, kl_pjb_jab, kl_pjb_nama, kl_pjb_nip,
			t2_status, if(t2_status='1',t2_proses_tgl,t2_selesai_tgl) t2_tgl,
			t3_status, if(t3_status='1',t3_proses_tgl,t3_selesai_tgl) t3_tgl,
			t4_status, if(t4_status='1',t4_proses_tgl,t4_selesai_tgl) t4_tgl,
			t5_status, if(t5_status='1',t5_proses_tgl,t5_selesai_tgl) t5_tgl,
			t6_status, if(t6_status='1',t6_proses_tgl,t6_selesai_tgl) t6_tgl,
			t7_status, if(t7_status='1',t7_proses_tgl,t7_selesai_tgl) t7_tgl
			From revisi $whr  Order By rev_tahun Desc, rev_id Desc");
		$hasil = $this->fc->ToArr( $query->result_array(), 'rev_id');

		if ($hasil) {
			$dbref = $this->load->database('ref', TRUE);
			$query = $dbref->query("Select kddept kode, nmdept uraian, '1' level From t_dept Where kddept ". $this->fc->InVar($hasil, 'kl_dept') );
			$dept  = $this->fc->ToArr( $query->result_array(), 'kode');

			$query = $dbref->query("Select concat(kddept,'.',kdunit) kode, nmunit uraian, '2' level From t_unit Where concat(kddept,'.',kdunit) ". $this->fc->InVar($hasil, 'kl_unit') );
			$unit  = $this->fc->ToArr( $query->result_array(), 'kode');

			foreach ($hasil as $key=>$value) {
				if (array_key_exists($value['kl_dept'], $dept)) $hasil[$key]['nmdept'] = $dept[ $value['kl_dept'] ]['uraian'];
				if (array_key_exists($value['kl_unit'], $unit)) $hasil[$key]['nmunit'] = $unit[ $value['kl_unit'] ]['uraian'];
			}

			$data['depuni'] = $this->fc->array_index( array_merge_recursive($dept, $unit), 'kode');
			$data['kode']   = $depuni;
		} else {
			$data['depuni'] = array();
			$data['kode']   = array();
		}

		$data['revisi'] = $hasil;
		$data['start'] 	= $this->fc->idtgl( $start );
		$data['end']   	= $this->fc->idtgl( $end );
		return $data;
	}

	public function get_rev_id( $kddept, $kdunit ) {
		$thang = $this->session->userdata('thang');
		$query = $this->db->query("Select Right(rev_id,3) noid From revisi Where rev_tahun=$thang And kl_dept='$kddept' And kl_unit='$kdunit' Order By 1 Desc");
		$hasil = $query->row_array();

		$arr = array();
		if (!$hasil) $arr['rev_id'] = "$thang.$kddept.$kdunit.001";
		else $arr['rev_id'] = "$thang.$kddept.$kdunit.". sprintf('%03d', (int)$hasil['noid']+1);

		return $this->get_mitra( $arr, $kddept, $kdunit );
	}

	public function get_row( $rev_id ) {
		$query = $this->db->query("Select rev_id, rev_tahun, kl_dept, kl_unit, kl_surat_no, kl_surat_tgl, kl_surat_hal, kl_pjb_jab, kl_pjb_nama, kl_pjb_nip, kl_email, kl_telp, doc_usulan_revisi, doc_matrix, doc_rka, doc_adk, doc_dukung_sepakat, doc_dukung_hal4, doc_dukung_lainnya, pus_catatan, kl_email From revisi Where rev_id='$rev_id'");
		$hasil = $query->row_array();

		if ($hasil) {
			$dbref = $this->load->database('ref', TRUE);
			$query = $dbref->query("Select nmdept From t_dept Where kddept='". $hasil['kl_dept'] ."'");
			$dept  = $query->row_array();
			$query = $dbref->query("Select nmunit From t_unit Where kddept='". $hasil['kl_dept'] ."' And kdunit='". $hasil['kl_unit'] ."'");
			$unit  = $query->row_array();

			$hasil['nmdept'] = $dept['nmdept'];
			$hasil['nmunit'] = $unit['nmunit'];
			$hasil = $this->get_mitra( $hasil, $hasil['kl_dept'], $hasil['kl_unit'] );
		}
		return  $hasil;
	}

	public function get_mitra( $arr, $kddept, $kdunit ) {
		$query = $this->db->query("Select *, nmso From revisi_wenang a Left Join t_so b On a.kdso=b.kdso Where a.kddept like '%$kddept%'");
		$hasil = $query->row_array();
		if ($hasil) {
			$arr['rev_kdso'] = $hasil['kdso'];
			$arr['rev_nmso'] = $hasil['nmso'];
			$arr['rev_notlp']= 'ext: '. $hasil['intern'] .',  email: '. $hasil['email'];
			$arr['rev_nmcp'] = $hasil['cp1'] .' ['. $hasil['hp1'] .'],  '. $hasil['cp2'] .' ['. $hasil['hp2'] .']';
			if (strlen($arr['rev_nmcp']) < 10) $arr['rev_nmcp'] = '';
			if (strlen($arr['rev_notlp']) < 10) $arr['rev_notlp'] = '';
		} else {
			$arr['rev_kdso'] = ''; $arr['rev_nmso'] = ''; $arr['rev_nmcp'] = '';
		}

		$query = $this->db->query("Select email, phone From revisi_mitra Where kddept='$kddept' And kdunit='$kdunit'");
		$hasil = $query->row_array();
		if ($hasil) { $arr['kl_email'] = $hasil['email']; $arr['kl_telp']  = $hasil['phone']; }
		else { $arr['kl_email'] = ''; $arr['kl_telp']  = ''; }
		return $arr;
	}

	public function save_form1() {
		$simpan			= $_POST['simpan'];
		$rev_tahun		= date('Y');
		$rev_id			= $_POST['rev_id'];
		$rev_jns_upload	= $_POST['rev_jns_upload'];
		$rev_kdso		= $_POST['rev_kdso'];

		$kl_dept		= $_POST['kl_dept'];
		$kl_unit		= $_POST['kl_unit'];
		$kl_surat_no	= $_POST['kl_surat_no'];
		$kl_surat_tgl	= $this->fc->ustgl( $_POST['kl_surat_tgl'], 'hari');
		$kl_surat_hal	= $_POST['kl_surat_hal'];
		$kl_pjb_jab		= $_POST['kl_pjb_jab'];
		$kl_pjb_nama	= $_POST['kl_pjb_nama'];
		$kl_pjb_nip		= $_POST['kl_pjb_nip'];
		$kl_email		= $_POST['kl_email'];
		$kl_telp		= $_POST['kl_telp'];

		$doc_usulan_revisi	= $_POST['doc_usulan_revisi'];
		$doc_matrix			= $_POST['doc_matrix'];
		$doc_rka			= $_POST['doc_rka'];
		$doc_adk			= $_POST['doc_adk'];
		$doc_dukung_sepakat	= $_POST['doc_dukung_sepakat'];
		$doc_dukung_hal4	= $_POST['doc_dukung_hal4'];
		$doc_dukung_lainnya	= $_POST['doc_dukung_lainnya'];

		$pus_userid		= $this->session->userdata('iduser');
		$pus_catatan	= str_replace('"', "'", $_POST['pus_catatan']);
		$pus_ip			= 'xx.xx.xx';

		if ($rev_jns_upload=='1') {		// Data Awal Revisi
			if ($simpan=='terima') { $rev_tahap = '2'; $t2_status = '9'; $pus_status = '2'; } 	// Terima Dokumen
			if ($simpan=='kembali'){ $rev_tahap = '1'; $t2_status = '';  $pus_status = '0'; } 	// Pengembalian

			$this->db->query( "Insert Into revisi (rev_tahun, rev_id, rev_kdso, rev_tahap, kl_dept, kl_unit, kl_surat_no, kl_surat_tgl, kl_surat_hal, kl_pjb_jab, kl_pjb_nama, kl_pjb_nip, kl_email, kl_telp, doc_usulan_revisi, doc_matrix, doc_rka, doc_adk, doc_dukung_sepakat, doc_dukung_hal4, doc_dukung_lainnya, pus_userid, pus_catatan, pus_tgl, pus_status, pus_ip, t2_status) Values ('$rev_tahun', '$rev_id', '$rev_kdso', '$rev_tahap', '$kl_dept', '$kl_unit', '$kl_surat_no', '$kl_surat_tgl', \"$kl_surat_hal\", '$kl_pjb_jab', \"$kl_pjb_nama\", '$kl_pjb_nip', '$kl_email', '$kl_telp', '$doc_usulan_revisi', '$doc_matrix', '$doc_rka', '$doc_adk', '$doc_dukung_sepakat', '$doc_dukung_hal4', '$doc_dukung_lainnya', '$pus_userid', \"$pus_catatan\", current_timestamp(), '$pus_status', '$pus_ip', '$t2_status')" );
		}

		if ($rev_jns_upload=='2') {		// Data Dukung Tambahan
			$pus_status = '2'; 	// Sukses
			$this->db->query( "Insert Into revisi_perbaikan (rev_tahun, rev_id, kl_surat_no, kl_surat_tgl, kl_surat_hal, kl_pjb_jab, kl_pjb_nama, kl_pjb_nip, doc_usulan_revisi, doc_matrix, doc_rka, doc_adk, doc_dukung_sepakat, doc_dukung_hal4, doc_dukung_lainnya, pus_userid, pus_catatan, pus_status, pus_tgl, pus_ip) Values ('$rev_tahun', '$rev_id', '$kl_surat_no', '$kl_surat_tgl', \"$kl_surat_hal\", '$kl_pjb_jab', \"$kl_pjb_nama\", '$kl_pjb_nip', '$doc_usulan_revisi', '$doc_matrix', '$doc_rka', '$doc_adk', '$doc_dukung_sepakat', '$doc_dukung_hal4', '$doc_dukung_lainnya', '$pus_userid', \"$pus_catatan\", current_timestamp(), '$pus_status', '$pus_ip')" );

			$t6_status = '9';
			$this->db->query( "Update revisi Set rev_tahap='6', t6_status='$t6_status' Where rev_id='$rev_id'" );
		}
		return $rev_id;
	}

	public function get_tracking( $rev_id ) {
		$query = $this->db->query("Select rev_tahun, rev_id, rev_tahap, rev_status, kl_dept, '' nmdept, concat(kl_dept,'.',kl_unit) kl_unit, '' nmunit, kl_surat_no, kl_surat_tgl, kl_surat_hal, kl_pjb_jab,
			pus_status, pus_tgl,
			t2_status, t2_proses_tgl, t2_selesai_tgl,
			t3_status, t3_proses_tgl, t3_selesai_tgl,
			t4_status, t4_proses_tgl, t4_selesai_tgl,
			t5_status, t5_proses_tgl, t5_selesai_tgl,
			t6_status, t6_proses_tgl, t6_selesai_tgl,
			t7_status, t7_proses_tgl, t7_selesai_tgl
			From revisi Where rev_id='$rev_id'");
		$hasil = $query->row_array();

		$dbref = $this->load->database('ref', TRUE);
		$query = $dbref->query("Select kddept, nmdept From t_dept Where kddept='". $hasil['kl_dept'] ."'" );
		$dept  = $this->fc->ToArr( $query->result_array(), 'kddept');

		$query = $dbref->query("Select concat(kddept,'.',kdunit) kdunit, nmunit From t_unit Where concat(kddept,'.',kdunit)='". $hasil['kl_unit'] ."'");
		$unit  = $this->fc->ToArr( $query->result_array(), 'kdunit');

		if (array_key_exists($hasil['kl_dept'], $dept)) $hasil['nmdept'] = $dept[ $hasil['kl_dept'] ]['nmdept'];
		if (array_key_exists($hasil['kl_unit'], $unit)) $hasil['nmunit'] = $unit[ $hasil['kl_unit'] ]['nmunit'];
		return $hasil;
	}

	public function get_pdf($rev_id){
		$query=$this->db->query("Select a.*, '' nmdept, '' nmunit, nmso From revisi a Left Join t_so b On a.rev_kdso=b.kdso Left Join revisi_perbaikan c On a.rev_id=c.rev_id  Where a.rev_id='$rev_id'");
		$hasil = $query->row_array();

		$dbref = $this->load->database('ref', TRUE);
		$query = $dbref->query("Select nmdept From t_dept Where kddept='". $hasil['kl_dept'] ."'");
		$dept  = $query->row_array();

		$query = $dbref->query("Select nmunit From t_unit Where kddept='". $hasil['kl_dept'] ."' And kdunit='". $hasil['kl_unit'] ."'");
		$unit  = $query->row_array();

		$hasil['nmdept'] = $dept['nmdept'];
		$hasil['nmunit'] = $unit['nmunit'];
		return $hasil;
	}

	public function get_pdf_tambahan($rev_id){
		$query = $this->db->query("Select a.rev_tahun rev_tahun,b.rev_id_tambahan revisi_ke, a.rev_id rev_id, a.rev_tahap rev_tahap,a.rev_kdso rev_kdso, a.kl_dept kl_dept, a.kl_unit kl_unit,a.kl_telp kl_telp ,a.kl_surat_no kl_surat_no, a.kl_surat_tgl kl_surat_tgl, a.kl_surat_hal kl_surat_hal, a.kl_pjb_jab kl_pjb_jab, a.kl_pjb_nama kl_pjb_nama, a.kl_pjb_nip kl_pjb_nip, a.kl_email kl_email,b.doc_usulan_revisi doc_usulan_revisi,b.doc_matrix doc_matrix, b.doc_rka doc_rka, b.doc_adk doc_adk, b.doc_dukung_sepakat doc_dukung_sepakat, b.doc_dukung_hal4 doc_dukung_hal4, b.doc_dukung_lainnya doc_dukung_lainnya,b.pus_catatan pus_catatan, b.pus_tgl pus_tgl,'' nmdept, '' nmunit, nmso From revisi a Left Join revisi_perbaikan b On a.rev_id=b.rev_id Left Join t_so c On a.rev_kdso=c.kdso Where a.rev_id='$rev_id' Order By 2 DESC");
		$hasil = $query->row_array();

		$dbref = $this->load->database('ref', TRUE);
		$query = $dbref->query("Select nmdept From t_dept Where kddept='". $hasil['kl_dept'] ."'");
		$dept  = $query->row_array();

		$query = $dbref->query("Select nmunit From t_unit Where kddept='". $hasil['kl_dept'] ."' And kdunit='". $hasil['kl_unit'] ."'");
		$unit  = $query->row_array();

		$hasil['nmdept'] = $dept['nmdept'];
		$hasil['nmunit'] = $unit['nmunit'];
		return $hasil;
	}

	public function get_pdf_routing($rev_id){
		$query =$this->db->query("Select a.*, b.nmso From revisi a Left Join t_so b On a.rev_kdso=b.kdso Where rev_id='$rev_id'");
		$hasil = $query->row_array();

		$dbref = $this->load->database('ref', TRUE);
		$query = $dbref->query("Select nmdept From t_dept Where kddept='". $hasil['kl_dept'] ."'");
		$dept  = $query->row_array();

		$query = $dbref->query("Select nmunit From t_unit Where kddept='". $hasil['kl_dept'] ."' And kdunit='". $hasil['kl_unit'] ."'");
		$unit  = $query->row_array();

		$hasil['nmdept'] = $dept['nmdept'];
		$hasil['nmunit'] = $unit['nmunit'];
		return $hasil;
	}

	public function email_revisi( $rev_id, $file ) {
		//kirim tanda terima ke subdit masing2 kl begin
		$query 		= $this->db->query("Select kl_email email,kl_dept kddept From revisi Where rev_id = '$rev_id'");
	 	$email1		= $query->result_array();
	 	$query 		= $this->db->query("Select a.email,a.kddept kddept From revisi_wenang a Left Join revisi b On a.kdso=b.rev_kdso Where rev_id='$rev_id'  ");
	 	$email2 	= $query->result_array();
	 	$merge 		= array_merge($email1,$email2);

	 	$email 		= array();
		foreach ($merge as $row) if ( $row['email'] ) $email[ $row['email'] ] = $row['kddept'];
		//ending

		//query ambil data
		$query=$this->db->query("Select *, '' nmdept, '' nmunit, nmso, DATE(pus_tgl) From revisi a Left Join t_so b On a.rev_kdso=b.kdso Where rev_id='$rev_id'");
		$hasil = $query->row_array();

		$dbref = $this->load->database('ref', TRUE);
		$query = $dbref->query("Select nmdept From t_dept Where kddept='". $hasil['kl_dept'] ."'");
		$dept  = $query->row_array();

		$query = $dbref->query("Select nmunit From t_unit Where kddept='". $hasil['kl_dept'] ."' And kdunit='". $hasil['kl_unit'] ."'");
		$unit  = $query->row_array();

		$hasil['nmdept'] = $dept['nmdept'];
		$hasil['nmunit'] = $unit['nmunit'];

		//query php_mailer
		$query = $this->db->query("Select * From revisi_redaksi Where tahap=11");
		$hasilbody = $query->row_array();
		$subject = $hasilbody['email_subject'];
		$body    = $hasilbody['email_narasi'];
		$body    = str_replace('[kl_dept]', ucwords(strtolower($hasil['nmdept'])), $body);
		$body    = str_replace('[kl_unit]', ucwords(strtolower($hasil['nmunit'])), $body);
		$body    = str_replace('[kl_pjb_jab]', $hasil['kl_pjb_jab'], $body);
		$body    = str_replace('[kl_surat_tgl]', $this->fc->idtgl($hasil['kl_surat_tgl'],'tglfull'),$body);
		$body    = str_replace('[kl_surat_no]', $hasil['kl_surat_no'], $body);
		$body    = str_replace('[pus_tgl]', $this->fc->idtgl($hasil['pus_tgl'],'tglfull'), $body);
		$body    = str_replace('[rev_id]', $hasil['rev_id'], $body);
		$body    = str_replace('http://www.rkakldipaonline.kemenkeu.go.id','http://rkakldipa.anggaran.depkeu.go.id/index.php/tracking/revisi/'.$hasil['qrcode'], $body);
		$attach  = array( $file => "http://10.242.142.52/files/revisi/$rev_id/" .'/'. $file );
		$this->fc->send_mail( 'revisi', $email, $subject, $body, $attach);
	 	return $attach;
	}

	public function get_monit($start, $end) {
		// echo '<pre>'; print_r( $this->session );exit;
		// echo $this->group;exit;
		$cari = $this->security->xss_clean($this->session->userdata('cari'));
		$data['start'] = $this->fc->idtgl($start);
		$data['end']   = $this->fc->idtgl($end);
		$data['tabel'] = array(); 

		$whr = '';
		// if ($this->group == 'userkl')		$whr = "kddept='$this->dept' AND ";
		// if ($this->group == 'userunit') 	$whr = "kddept='$this->dept' AND kdunit='$this->unit' AND ";
		// if ($this->group == 'usersatker')	$whr = "kdsatker='". $this->session->userdata('kdsatker') ."' AND ";
		if ($this->group == '611' OR $this->group == '612')	$whr = "kdlokasi='". $this->session->userdata('kdlokasi') ."' AND ";
		if ($cari != '') {
			$whr.= "(rev_id LIKE '%$cari%' OR kddept LIKE '%$cari%' OR kdunit LIKE '%$cari%' OR kdsatker LIKE '%$cari%') AND";
			$qry = $this->dbrvs->query("SELECT *, concat(rev_id,'.',kdsatker) kdkey, '' uraian FROM revisi_satker WHERE $whr rev_id IN (SELECT rev_id FROM revisi)");
		} else {
			$qry = $this->dbrvs->query("SELECT *, concat(rev_id,'.',kdsatker) kdkey, '' uraian FROM revisi_satker WHERE $whr rev_id IN (SELECT rev_id FROM revisi WHERE rev_upload_level='3' AND pus_tgl >= '$start 00:00:00' AND pus_tgl <= '$end 23:59.59')");
		}
		$stk = $this->fc->ToArr($qry->result_array(), 'kdkey');	

		if (! $stk) return $data;

		$qry = $this->dbrvs->query("SELECT *,'' nmdept,'' nmunit FROM revisi WHERE rev_upload_level='3' AND rev_id ". $this->fc->InVar($stk, 'rev_id') ." ORDER BY pus_tgl DESC");
		$dat = $this->fc->ToArr($qry->result_array(), 'rev_id');	

		foreach ($dat as $key=>$row) { $dat[$key]['kanwil'] = ''; $dat[$key]['satker'] = array(); }
				
		$qry = $this->dbref->query("SELECT kdsatker kode, nmsatker uraian FROM t_satker WHERE kdsatker ". $this->fc->InVar($stk, 'kdsatker'));
		$tbl = $this->fc->ToArr($qry->result_array(), 'kode');
		$qry = $this->dbref->query("SELECT CONCAT(a.kddept,b.kdunit) kode, a.kddept,nmdept,kdunit,nmunit FROM t_dept a LEFT JOIN t_unit b ON a.kddept=b.kddept");
		$unt = $this->fc->ToArr($qry->result_array(),'kode');
		$qry = $this->dbref->query("SELECT kdlokasi kode, nmkanwil FROM t_kanwil");
		$kwl = $this->fc->ToArr($qry->result_array(),'kode');


		foreach ($dat as $key=>$row) {
			$link = 'Rev?q=bc007&revid='. $row['rev_id'] ;
			if ($row['bvalid1']) { $dat[$key]['icon-valid1'] = 'fa fa-check text-success'; $dat[$key]['link-valid1'] = site_url("$link&valid=1"); }
			else { $dat[$key]['icon-valid1'] = 'fa fa-times text-danger'; $dat[$key]['link-valid1'] = site_url("$link&valid=1"); }
			if ($row['bvalid2']) { $dat[$key]['icon-valid2'] = 'fa fa-check text-success'; $dat[$key]['link-valid2'] = site_url("$link&valid=2"); }
			else { $dat[$key]['icon-valid2'] = 'fa fa-times text-danger'; $dat[$key]['link-valid2'] = site_url("$link&valid=2"); }
			if ($row['bvalid3']) { $dat[$key]['icon-valid3'] = 'fa fa-check text-success'; $dat[$key]['link-valid3'] = site_url("$link&valid=3"); }
			else { $dat[$key]['icon-valid3'] = 'fa fa-times text-danger'; $dat[$key]['link-valid3'] = site_url("$link&valid=3"); }
			if(array_key_exists($row['kl_dept'].$row['kl_unit'], $unt)){
				$dat[$key]['nmdept'] = $unt[$row['kl_dept'].$row['kl_unit']]['nmdept'];
				$dat[$key]['nmunit'] = $unt[$row['kl_dept'].$row['kl_unit']]['nmunit'];
			} 
			if ($row['rev_upload_level'] == '3' AND array_key_exists($row['rev_id'].'.'.$row['kl_satker'], $stk)) {
				$loka = $stk[$row['rev_id'].'.'.$row['kl_satker']]['kdlokasi'];
				if (array_key_exists($loka, $kwl)) $dat[$key]['kanwil'] = $kwl[$loka]['nmkanwil'];
			}
		}

		foreach ($stk as $row) {
			if (array_key_exists($row['kdsatker'], $tbl)) $row['uraian'] = $tbl[ $row['kdsatker'] ]['uraian'];
			$ke = sprintf('%02d', ((int)$row['revisike'])-1);
			$link = 'unit='. $row['kddept'].$row['kdunit'] .'&satker='. $row['kdsatker'];
			$row['link_matrik'] = site_url("adk_dipa?q=rvs19&$link&id=". $row['rev_id']."&mula=$ke");
			$row['link_pok'] 	= site_url("adk_dipa?q=45007&$link&id=". $row['rev_id']);
			$row['link_ssb'] 	= site_url("revisi?q=e1d8c&$link&revid=". $row['rev_id']);
			$row['link_digist'] = site_url("adk_dipa?q=dgs19&$link&id=". $row['rev_id']."&mula=$ke");
			array_push($dat[ $row['rev_id'] ]['satker'], $row);
		}

		$data['tabel'] = $dat;
		return $data;
	}

	function get_monit_validasi_2020($rev_id, $valid, $view) { 
		if ($view == 'all') $query = $this->dbrvs->query("SELECT rev_upload_level lvl, valid$valid teks FROM revisi WHERE rev_id='$rev_id'");
		else $query = $this->dbrvs->query("SELECT rev_upload_level lvl, invalid$valid teks FROM revisi WHERE rev_id='$rev_id'");
		return $query->row_array();
	}

	public function validasi_adk_2021($rev_id, $level) {
		// Persiapan Validasi
			$html   = array('valid'=>'', 'invalid'=>'');
			$error = ''; $errev = $ermin = $erpgu = array();
			$bSta  = $bMin = 1;
			$query = $this->dbtmp->query("SELECT DISTINCT kdsatker Satker, '1' Tahap, '' Rev_sta, 1 status FROM v_rekap WHERE $this->index AND rev_id='$rev_id'");
			$satkr = $this->fc->ToArr($query->result_array(), 'Satker');;
			$wsatk = "kdsatker ". $this->fc->InVar($satkr, 'Satker');
			$mindx = "$this->index AND $wsatk";
			if (! $satkr) return 'Invalid ADK';

			$query = $this->dbrvs->query("SELECT kdsatker, concat(kdsatker,MAX(rev_id)) kode FROM v_rekap WHERE $mindx GROUP BY 1");
			$array = $query->result_array();
			if ($array) $indx  = "$this->index AND concat(kdsatker,rev_id) ". $this->fc->InVar($array, 'kode');
			else 		$indx  = $mindx;
			$mindx.= " AND rev_id='$rev_id'";
		// End - Persiapan Validasi 

		// Validasi SATKER sedang PROSES REVISI
			$finis = array(
				"!(rev_tahap = '1')",
				"!(rev_tahap = '2' AND rev_status = '0')",
				"!(rev_tahap = '6' AND rev_status = '3')",
				"!(rev_tahap = '7' AND rev_status = '2')",
				"!(rev_tahap = '7' AND rev_status = '0')"
			);
			$notin = implode(' AND ', $finis); 
			$query = $this->dbrvs->query("SELECT a.rev_id, b.rev_tahap, b.rev_status, a.kdsatker FROM revisi_satker a  LEFT JOIN revisi b ON a.rev_id=b.rev_id WHERE $wsatk AND $notin ORDER BY pus_tgl DESC");
			$revsi = $this->fc->ToArr($query->result_array(), 'kdsatker');
			$query = $this->dbref->query("SELECT kdsatker kode, nmsatker teks FROM t_satker WHERE kdsatker ". $this->fc->InVar($satkr, 'Satker'));
			$ref   = $this->fc->ToArr($query->result_array(), 'kode');
			foreach ($satkr as $key=>$row) {
				if (array_key_exists($row['Satker'], $ref))  $satkr[$key]['Satker'] .= ' '. $ref[$row['Satker']]['teks'];
				if (array_key_exists($key, $revsi)) 
					{ $satkr[$key]['status'] = 0; $satkr[$key]['Tahap'] = $revsi[$key]['rev_tahap']; $bSta = 0; $satkr[$key]['Rev_sta'] = $revsi[$key]['rev_status']; }
			}

			$html = $this->validasi_pmk_insert_tabel_2021($html, $rev_id, $satkr, 'Satker dalam Proses Revisi', '-pagu');
		// End - Validasi SATKER sedang PROSES REVISI

		// Validasi PAGU MINUS - JENIS BELANJA dan AKUN FUND BLOCK
			$kodej= "concat(kdsatker,'.',kdgiat,'.',kdoutput,'.',left(kdakun,2)) kode, kdsatker Satker, concat(kdgiat,'.',kdoutput) Output, left(kdakun,2) Jenis_Belanja_dan_Akun";
			$kodea= "concat(kdsatker,'.',kdgiat,'.',kdoutput,'.',kdakun) kode, kdsatker Satker, concat(kdgiat,'.',kdoutput) Output, kdakun Jenis_Belanja_dan_Akun";

			$query = $this->dbtmp->query("SELECT $kodej, sum(jml_pagu) Pagu, 0 Realisasi, 0 Blokir, 0 Sisa, 1 status FROM v_rekap WHERE $mindx GROUP BY 1");
			$mreal = $query->result_array();
			$query = $this->dbtmp->query("SELECT $kodea, sum(jml_pagu) Pagu, 0 Realisasi, 0 Blokir, 0 Sisa, 1 status FROM v_rekap WHERE $mindx GROUP BY 1");
			$mreal = array_merge_recursive($mreal, $query->result_array());
			$mreal = $this->fc->ToArr( $this->fc->array_index($mreal,'kode'), 'kode');

			$query = $this->dbtmp->query("SELECT $kodej, 0 Pagu, 0 Realisasi, sum(jml_blokir) Blokir, 0 Sisa, 1 status FROM v_rekap WHERE $mindx AND jml_blokir>0 GROUP BY 1,2,3,4");
			$blok  = $query->result_array();
			if ($blok) {
				$query = $this->dbtmp->query("SELECT $kodea, 0 Pagu, 0 Realisasi, sum(jml_blokir) Blokir, 0 Sisa, 1 status FROM v_rekap WHERE $mindx AND jml_blokir>0 GROUP BY 1,2,3,4");
				$blok  = array_merge_recursive($blok, $query->result_array());
				$blok  = $this->fc->ToArr( $this->fc->array_index($blok,'kode'), 'kode');
				foreach ($blok as $key=>$row) 
					if (array_key_exists($key, $mreal)) $mreal[$key]['Blokir'] = $blok[$key]['Blokir']; else $mreal[$key] = $row;
			}

			$kodej = "concat(kdsatker,'.',kegiatan,'.',output,'.',left(akun,2)) kode, kdsatker Satker, concat(kegiatan,'.',output) Output, left(akun,2) Jenis_Belanja_dan_Akun";
			$kodea = "concat(kdsatker,'.',kegiatan,'.',output,'.',akun) kode, kdsatker Satker, concat(kegiatan,'.',output) Output, akun Jenis_Belanja_dan_Akun";
			$query = $this->dbrvs->query("SELECT $kodej, 0 Pagu, sum(rphreal) Realisasi, 0 Blokir, 0 Sisa, 1 status FROM d_realisasi WHERE baes1='$this->dept$this->unit' AND $wsatk AND output!='ZZZ' GROUP BY 1");
			$sp2d  = $query->result_array();
			if ($sp2d) {
				$query = $this->dbrvs->query("SELECT $kodea, 0 Pagu, sum(rphreal) Realisasi, 0 Blokir, 0 Sisa, 1 status FROM d_realisasi WHERE baes1='$this->dept$this->unit' AND $wsatk AND output!='ZZZ' GROUP BY 1");
				$sp2d  = array_merge_recursive($sp2d, $query->result_array());
				$sp2d  = $this->fc->ToArr( $this->fc->array_index($sp2d,'kode'), 'kode');
				foreach ($sp2d as $key=>$row) 
					if (array_key_exists($key, $mreal)) $mreal[$key]['Realisasi'] = $sp2d[$key]['Realisasi']; else $mreal[$key] = $row;
			}

			$query = $this->dbref->query("SELECT kdgbkpk kode, concat('<b>',nmgbkpk,'</b>') teks FROM t_gbkpk");
			$jbel  = $this->fc->ToArr($query->result_array(), 'kode');
			$query = $this->dbref->query("SELECT kdakun kode, nmakun teks FROM t_akun WHERE kdakun ". $this->fc->InVar($mreal, 'Jenis_Belanja_dan_Akun'));
			$akun  = $this->fc->ToArr($query->result_array(), 'kode');

			foreach ($mreal as $key=>$row) {
				unset( $mreal[$key]['kode'] );
				$mreal[$key]['Sisa'] = $row['Sisa'] = $row['Pagu'] - $row['Realisasi'] - $row['Blokir']; 
				if (strlen($row['Jenis_Belanja_dan_Akun']) == 2) { 
					if ($row['Sisa'] < 0)
						if ($row['Jenis_Belanja_dan_Akun'] <> '51')  { $mreal[$key]['status'] = 0; $bMin = 0; }
				}

				if (array_key_exists($row['Jenis_Belanja_dan_Akun'], $akun)) 
					$mreal[$key]['Jenis_Belanja_dan_Akun'] .= ' '.  $akun[$row['Jenis_Belanja_dan_Akun']]['teks'];
				if (array_key_exists($row['Jenis_Belanja_dan_Akun'], $jbel)) 
					$mreal[$key]['Jenis_Belanja_dan_Akun']  = '<b>'. $mreal[$key]['Jenis_Belanja_dan_Akun'] .' '. $jbel[$row['Jenis_Belanja_dan_Akun']]['teks'].'</b>';
			}

			$html = $this->validasi_pmk_insert_tabel_2021($html, $rev_id, $mreal, 'Check Pagu Minus per Satker/K R O/Akun', '-Pagu-Realisasi-Blokir-Sisa');
		// End - Validasi PAGU MINUS - JENIS BELANJA dan AKUN FUND BLOCK

		// Rekap INFORMASI VALID
			if (! $bSta) $error .= "Kode Satker ". implode(', ', $errev) ." sedang Proses Revisi ! \r\n";
			if (! $bMin) $error .= "Kode Satker ". implode(', ', $errev) ." terdapat Pagu Minus ! \r\n";

			$error = ($error == '') ? 'Valid' : $error;
			if ($error == 'Valid') $this->dbrvs->query("UPDATE revisi SET rev_upload_step = IF(rev_upload_step LIKE '%A%', rev_upload_step, CONCAT(rev_upload_step,'A')) WHERE rev_id='$rev_id'");

			$bVal = ($error == 'Valid') ? 1 : 0;
			$this->dbrvs->query("UPDATE revisi SET bvalid1=$bVal WHERE rev_id='$rev_id'");

			file_put_contents("files/revisi_SatuDJA/$rev_id/valid1.html",   $html['valid']);
			file_put_contents("files/revisi_SatuDJA/$rev_id/invalid1.html", $html['invalid']);
		// End - Rekap INFORMASI VALID
		return $error;
	}

	function get_monit_validasi_2021($rev_id, $valid, $view) {
		$arr = array('lvl'=>'1', 'teks'=>'');
		$oldMessage 	= 'https://satudja.kemenkeu.go.id/revisi';
		$deletedFormat 	= 'http://intranet.anggaran.depkeu.go.id/Rev';
		$str = file_get_contents("files/revisi_SatuDJA/$rev_id/invalid$valid.html");
		$str = str_replace($oldMessage, $deletedFormat,$str);
		if ($view == 'all') $arr['teks'] = file_get_contents("files/revisi_SatuDJA/$rev_id/valid$valid.html");
		else 				$arr['teks'] = $str;

		return $arr;
	}

	function bypass_validasi($id){
		$userid = $this->session->userdata('iduser');
		$now    = date("Y-m-d h:i:s");
		$this->dbrvs->query("UPDATE revisi SET bvalid2='1', bypass_id='$userid', bypass_tgl='$now' WHERE rev_id='$id' ");
		return "success";
	}

}
