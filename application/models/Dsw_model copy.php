<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Dsw_model extends CI_Model {
	
	public function get_menu( $str ) {
		$idmenu = $this->fc->InVar( $this->menu_group(), 'menu');
		$query  = $this->db->query("Select *, if(right(idmenu,4)='0000','1', if(right(idmenu,2)='00','2', '3')) lvl, '' active From t_menu Where idmenu $idmenu Order By urutan, idmenu");
		$menu = $this->fc->ToArr( $query->result_array(), 'idmenu');

		foreach ($menu as $key => $value) {
			if ( $value['link']==$str ) $menu[ $key ]['active'] = 'active';
		}
		return $menu;
	}

	public function menu_group() {
		$whr = 'Public';
		$whr = "idusergroup in ('". $this->session->userdata('idusergroup') ."')";
		// $whr = " idusergroup = '001'";
		$query = $this->db->query("Select * From t_user_group Where $whr");
		$group  = $this->fc->ToArr( $query->result_array(), 'idusergroup');

		$arr = array();
		foreach ($group as $row) {
			$str = explode(';', $row['menu']);
			for ($i=0; $i<count($str); $i++) {
				$nil = explode('-', $str[$i]);
				if ( ! array_key_exists($nil[0], $arr) ) {
					$arr[ $nil[0] ] = array('menu'=>$nil[0], 'rwx'=>$nil[1]);
				}
			}
		}

		return $arr;
	}

	public function get_moncap() {
		$query = $this->db->query("Select id_bidang, Count(*) jumlah, Sum(progres) persen, 000 progres From d_moncap Where kd_jenis='1' Group By 1");
		$hasil = $this->fc->ToArr( $query->result_array(), 'id_bidang');

		foreach ($hasil as $key => $value) {
			$hasil[ $key ]['progres'] = round( $value['persen'] / $value['jumlah'] );
		}
		return $hasil;
	}

	public function get_pengumuman() {
		$query = $this->db->query("Select * From d_pengumuman Order By idpengumuman Desc Limit 4");
		$hasil = $this->fc->ToArr( $query->result_array(), 'idpengumuman');
		return $hasil;
	}

	public function get_forum() {
		$query = $this->db->query("Select concat(idparent,'.',idchild) idkey, idparent, idchild, max(tglpost) tglpost From d_forum Group By 1,2,3 Order By tglpost Desc Limit 3");

		$hasil = array();
		foreach ($query->result_array() as $row) {
			$idkey = $row['idkey'] ;
			$where = 'where idparent='. $row['idparent'] .' and idchild='. $row['idchild'];

			// Forum Room
			$query = $this->db->query("Select *, concat(idparent,'.',idchild) idkey From d_forum_room $where"); 
			$room  = $query->row_array();
			$hasil[ $idkey ] = $room;

			// Forum Home of Room
			$query = $this->db->query("Select nmroom From d_forum_room where idchild=".$row['idparent']); 
			$home  = $query->row_array();
			$hasil[ $idkey ]['nmhome'] = $home['nmroom'];


			// Prakata Room
			$query = $this->db->query("Select concat(idparent,'.',idchild) idkey, tglpost, post From d_forum $where and idforum=1"); 
			$post  = $query->row_array();
			$hasil[ $idkey ]['post'] = $post['post'];
		}
		$hasil  = $this->fc->ToArr( $hasil, 'idkey');
		$query  = $this->db->query("Select iduser, nmuser, nip From t_user where iduser " . $this->fc->InVar($hasil, 'iduser'));
		$t_user = $this->fc->ToArr( $query->result_array(), 'iduser');
		$hasil  = $this->fc->Left_Join( $hasil, $t_user, 'iduser,nmuser=nmuser,nip=nip' );

		// Last Thread
		foreach ($hasil as $row) {
			$where = 'where idparent='. $row['idparent'] .' and idchild='. $row['idchild'];
			$query = $this->db->query("Select concat(idparent,'.',idchild,'.',idforum) idkey, iduser, idforum, tglpost, post From d_forum $where and idforum!=1 Order By tglpost Desc Limit 3");
			$thread = $this->fc->ToArr( $query->result_array(), 'idkey');
			if ($thread) {
				$thread = $this->fc->array_index( $thread, 'idkey');

				$query  = $this->db->query("Select iduser, nmuser, nip From t_user where iduser " . $this->fc->InVar($thread, 'iduser'));
				$t_user = $this->fc->ToArr( $query->result_array(), 'iduser');
				$thread = $this->fc->Left_Join( $thread, $t_user, 'iduser,nmuser=nmuser,nip=nip' );

				$hasil[ $row['idkey'] ]['thread'] = $thread;
			} else { 
				$hasil[ $row['idkey'] ]['thread'] = ''; 
			}
		}
		return $hasil;
	}


	// == Milik PERSURATAN ==
	public function get_surat() {
		$dbsrt = $this->load->database('surat', TRUE);
		$sql   = "select distinct concat(golsurat,noagenda,trim(noagenda1)) idkey, tgagenda, golsurat, noagenda, noagenda1, jenis, perihal, kddari kode from d_induk order by tgagenda desc limit 5";
		$query = $dbsrt->query( $sql );
		$hasil = $this->fc->ToArr( $query->result_array(), 'idkey');
		$hasil = $this->fc->Ref_Join( $dbsrt, $hasil, 't_esl3', 'kode' );
		return $hasil;
	}

	public function param( $str=null ) {
		$dbsurat = $this->load->database('surat', TRUE);
		$query = $dbsurat->query("select '' nilai, '' tahun, '' bulan, '' bagian, '' cari, '0' status, 10 rowlimit, '' link, 0 rowstart");
		$query = $query->row();
		$query->tahun = date('Y');
		$query->bulan = sprintf('%02d', date("m"));

		if ($str<>null) {
			$arr = explode(',', $str);
			for ($i=0; $i<count($arr); $i++) {
				$arm = explode(':', $arr[$i]);
				$var = str_replace(' ','',$arm[0]);
				if ($var=='tahun') 	 { $query->tahun = $arm[1]; }
				if ($var=='bulan') 	 { $query->bulan = $arm[1]; }
				if ($var=='bagian')  { $query->bagian = $arm[1]; }
				if ($var=='status')  { $query->status = $arm[1]; }
				if ($var=='rowstart' and $arm[1]<>''){ $query->rowstart = $arm[1]; }
			}
		}

		if (!$query->status) $query->status = '0';
		$query->cari  = $this->session->userdata('cari');
		$query->link  = "$query->tahun/$query->bulan/$query->bagian/$query->status"; 
		$query->nilai = "tahun:$query->tahun, bulan:$query->bulan, bagian:$query->bagian, cari:$query->cari, status:$query->status, link:$query->link, rowstart:$query->rowstart";
		return $query;
	}

	public function bagian($string, $param, $url=null) {
		$dbsurat = $this->load->database('surat', TRUE);
		$bagian['01'] = array('kode'=>'01', 'uraian2'=>'Seluruh DJA');
		$query = $dbsurat->query("select kode, uraian2 from t_esl3 order by kode");
		foreach ($query->result_array() as $row) { $bagian[ $row['kode'] ] = array('kode'=>$row['kode'], 'uraian2'=>$row['uraian2']); }

		// // $query = $dbsurat->query("select kode, uraian from t_esl3 order by kode");
		// // $bagian = $this->fc->ToArr($query->result_array(), 'kode');

		// if ($string=='dropdown') {
		// 	$var = str_replace("'", "", $param->bagian);
		// 	$nil = trim($bagian["$var"]['uraian']);
		// 	$str = "<div class=\"dropdown\">
		// 			<button class=\"btn btn-default dropdown-toggle\" type=\"button\" id=\"bagian\" value=\"$param->bagian\" data-toggle=\"dropdown\">$nil
		// 			<span class=\"caret\"></span></button>
		// 			<ul class=\"dropdown-menu\" role=\"menu\" aria-labelledby=\"bagian\"> 
		// 		   ";
		// 	foreach($bagian as $row) {
		// 		$site = str_replace("/$param->bagian", "/".sprintf('%02d', $row['kode']), $url);
		// 		$str .= "<li role=\"presentation\"><a role=\"menuitem\" tabindex=\"-1\" href=\"$site\"> ". $row['uraian'] ."</a></li>";
		// 	}
		// 	$str .="</ul>
		// 			</div>
		// 		   ";
		// 	return $str;
		// }
		return $bagian;
	}
	
	public function tahun() {
		$dbsurat = $this->load->database('surat', TRUE);
		$query = $dbsurat->query("select distinct year(tgagenda) tahun, count(*) rec from d_induk group by 1");
		return $query->result();
	}

	public function bulan() {
		$bulan = array('Semua Bulan', 'Januari', 'Pebruari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'Nopember', 'Desember');
		return $bulan;
	}

	public function jml($str) {
		if ($str==0) {
			$nil = "zero\"> &nbsp; ";
		} elseif ($str>0) {
			$nil = "plus\">". number_format($str, 0, ',', '.') ;
		} else {
			$nil = "minus\">(". number_format(abs($str), 0, ',', '.') .")";
		}
		return $nil;
	}

}
