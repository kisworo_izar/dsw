<?php
class Mgm extends CI_Model{

	function __construct() {
		parent::__construct();
	}
 
	function param( $str=null ) {
		$dbsurat = $this->load->database('surat', TRUE);
		$query = $dbsurat->query("select '' nilai, '' tahun, '' bulan, '' bagian, '' cari, '0' status, 10 rowlimit, '' link, 0 rowstart");
		$query = $query->row();
		$query->tahun = date('Y');
		$query->bulan = sprintf('%02d', date("m"));

		if ($str<>null) {
			$arr = explode(',', $str);
			for ($i=0; $i<count($arr); $i++) {
				$arm = explode(':', $arr[$i]);
				$var = str_replace(' ','',$arm[0]);
				if ($var=='tahun') 	 { $query->tahun = $arm[1]; }
				if ($var=='bulan') 	 { $query->bulan = $arm[1]; }
				if ($var=='bagian')  { $query->bagian = $arm[1]; }
				if ($var=='status')  { $query->status = $arm[1]; }
				if ($var=='rowstart' and $arm[1]<>''){ $query->rowstart = $arm[1]; }
			}
		}

		if (!$query->status) $query->status = '0';
		$query->cari  = $this->session->userdata('cari');
		$query->link  = "$query->tahun/$query->bulan/$query->bagian/$query->status"; 
		$query->nilai = "tahun:$query->tahun, bulan:$query->bulan, bagian:$query->bagian, cari:$query->cari, status:$query->status, link:$query->link, rowstart:$query->rowstart";
		return $query;
	}

	public function bagian($string, $param, $url=null) {
		$dbsurat = $this->load->database('surat', TRUE);
		$bagian['01'] = array('kode'=>'01', 'uraian2'=>'Seluruh DJA');
		$query = $dbsurat->query("select kode, uraian2 from t_esl3 order by kode");
		foreach ($query->result_array() as $row) { $bagian[ $row['kode'] ] = array('kode'=>$row['kode'], 'uraian2'=>$row['uraian2']); }

		// // $query = $dbsurat->query("select kode, uraian from t_esl3 order by kode");
		// // $bagian = $this->fc->ToArr($query->result_array(), 'kode');

		// if ($string=='dropdown') {
		// 	$var = str_replace("'", "", $param->bagian);
		// 	$nil = trim($bagian["$var"]['uraian']);
		// 	$str = "<div class=\"dropdown\">
		// 			<button class=\"btn btn-default dropdown-toggle\" type=\"button\" id=\"bagian\" value=\"$param->bagian\" data-toggle=\"dropdown\">$nil
		// 			<span class=\"caret\"></span></button>
		// 			<ul class=\"dropdown-menu\" role=\"menu\" aria-labelledby=\"bagian\"> 
		// 		   ";
		// 	foreach($bagian as $row) {
		// 		$site = str_replace("/$param->bagian", "/".sprintf('%02d', $row['kode']), $url);
		// 		$str .= "<li role=\"presentation\"><a role=\"menuitem\" tabindex=\"-1\" href=\"$site\"> ". $row['uraian'] ."</a></li>";
		// 	}
		// 	$str .="</ul>
		// 			</div>
		// 		   ";
		// 	return $str;
		// }
		return $bagian;
	}
	
	public function tahun() {
		$dbsurat = $this->load->database('surat', TRUE);
		$query = $dbsurat->query("select distinct year(tgagenda) tahun, count(*) rec from d_induk group by 1");
		return $query->result();
	}

	public function bulan() {
		$bulan = array('Semua Bulan', 'Januari', 'Pebruari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'Nopember', 'Desember');
		return $bulan;
	}

	function jml($str) {
		if ($str==0) {
			$nil = "zero\"> &nbsp; ";
		} elseif ($str>0) {
			$nil = "plus\">". number_format($str, 0, ',', '.') ;
		} else {
			$nil = "minus\">(". number_format(abs($str), 0, ',', '.') .")";
		}
		return $nil;
	}
	
}