<?php
class Slideshow_model extends CI_Model {
	public function get_data() {
		$whr  = "";
		$cari = $this->session->userdata('cari');

		if ($cari) {
			$whr = " where " ;
			$arr = explode(' ', $cari);
			for ($i=0; $i<count($arr); $i++) {
				$str  = $arr[$i];
				$whr .= " keterangan like '%$str%' or gambar like '%$str%' ";
				if ($i<count($arr)-1) {
					$whr .= " or ";
				}
			}
		}

		// cari query data dari d_paket
		$query = $this->db->query("select *  from d_slideshow $whr order by idslideshow desc ");
		$hasil = $this->fc->ToArr( $query->result_array(), 'idslideshow');
		return $hasil;
	}

	public function get_row( $idslideshow=null ) {
		if ( is_null($idslideshow) ) $idslideshow='999999';
		$query = $this->db->query("select *  from d_slideshow where idslideshow=$idslideshow");
		$hasil = $query->row_array();
		return $hasil;
	}

	public function save(  ) {
		$action		  = $_POST['ruh'];
		$idslideshow  = $_POST['idslideshow'];
		$keterangan	  = $_POST['keterangan'];
		$link		  = $_POST['link'];
		$gambar		  = $_FILES['gambar'];
		$kdaktif	  = $_POST['aktif'];

		if ($gambar) move_uploaded_file($gambar["tmp_name"], "files/slideshow/".$gambar['name']);

		if ($action=='Rekam') {
			$this->db->query( "insert into d_slideshow (tglrekam,keterangan,link,kdaktif,gambar) values (current_timestamp(),'$keterangan','$link','$kdaktif','". $gambar['name']."')" );
		}
		if ($action=='Ubah') {
			$this->db->query( "update d_slideshow set keterangan='$keterangan', link='$link', kdaktif='$kdaktif' where idslideshow=$idslideshow" );
			if ( $gambar['name'] != '' )  	$this->db->query("update d_slideshow set gambar='". $gambar['name'] ."' where idslideshow=$idslideshow" );
		}
		if ($action=='Hapus') {
			$this->db->query( "delete from d_slideshow where idslideshow=$idslideshow" );
		}
		return;
	}
}
