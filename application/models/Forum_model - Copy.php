<?php 
class Forum_model extends CI_Model {

	public function sql_room() {
		$query = $this->db->query("Select a.*, nmuser, nip From d_forum_room a Left Join t_user b On a.iduser=b.iduser Where idparent=1 Order By 1,2");
		return $query->result_array();
	}

	public function sql_head($idparent, $idchild) {
		$query = $this->db->query("Select a.*, nmuser, nip From d_forum_room a Left Join t_user b On a.iduser=b.iduser Where idparent=$idparent and idchild=$idchild");
		return $query->row_array();
	}

	public function sql_sub($idparent, $idchild) {
		$query = $this->db->query("Select a.*, nmuser, nip From d_forum_room a Left Join t_user b On a.iduser=b.iduser Where idparent=$idchild Order By 1,2");
		return $query->result_array();
	}

	public function sql_thread($idparent, $idchild) {
		$query = $this->db->query("Select idparent, idchild From d_forum_room Where idparent=$idchild");
		$idkey = $this->fc->InVar( $query->result_array(), 'idchild' );

		$query = $this->db->query("Select concat(idparent,'.',idchild) idkey, idparent, idchild, thread, tglpost, a.iduser, nmuser, nip From d_forum a Left Join t_user b On a.iduser=b.iduser Where idparent $idkey and idforum=1 Order By 1,2");
		$hasil = $this->fc->ToArr( $query->result_array(), 'idkey');

		$query = $this->db->query("Select concat(idparent,'.',idchild) idkey, count(*)-1 replies From d_forum Where concat(idparent,'.',idchild) ". $this->fc->InVar($hasil, 'idkey') ." Group By 1");
		$repli = $this->fc->ToArr( $query->result_array(), 'idkey');

		$query = $this->db->query("Select idparent, idchild, concat(idparent, idchild, max(idforum)) idkey From d_forum Where concat(idparent,'.',idchild) ". $this->fc->InVar($hasil, 'idkey') ." Group By 1,2");
		$idkey = $this->fc->InVar( $query->result_array(), 'idkey' );
		$query = $this->db->query("Select concat(idparent,'.',idchild) idkey, a.iduser, nmuser, tglpost From d_forum a Left Join t_user b On a.iduser=b.iduser Where concat(idparent,idchild,idforum) $idkey");
		$post  = $this->fc->ToArr( $query->result_array(), 'idkey');

		foreach ($hasil as $key=>$value) {
			$hasil[ $key ]['replies'] = $repli[ $key ]['replies'];
			$hasil[ $key ]['nmreplies'] = $post[ $key ]['nmuser'];
			$hasil[ $key ]['tglreplies'] = $post[ $key ]['tglpost'];
		}

		$query  = $this->db->query("Select iduser, nmuser, nip, jabatan, a.kdso, nmso From t_user a left join t_so b on a.kdso=b.kdso where iduser " . $this->fc->InVar($hasil, 'iduser'));
		$t_user = $this->fc->ToArr( $query->result_array(), 'iduser');
		$hasil  = $this->fc->Left_Join( $hasil, $t_user, 'iduser,nmuser=nmuser,nip=nip,jabatan=jabatan,nmso=nmso' );
		return $hasil;
	}

	public function sql_top( $limit=null ) {
		if ($limit==null) $limit=5;
		$query = $this->db->query("Select concat(idparent,'.',idchild) idkey, idparent, idchild, count(*) reply From d_forum Group By 1,2,3 Order By reply Desc Limit $limit");
		$hasil = $this->fc->ToArr( $query->result_array(), 'idkey');

		foreach ($hasil as $key=>$value) {
			// Forum Room
			$query = $this->db->query("Select nmroom From d_forum_room where idparent=". $value['idparent'] ." and idchild=". $value['idchild']);
			$room  = $query->row_array();
			$hasil[ $key ]['nmroom'] = $room['nmroom'];

			// Forum Home of Room
			$query = $this->db->query("Select thread From d_forum where idparent=". $value['idparent'] ." and idchild=". $value['idchild'] ." and idforum=1");
			$home  = $query->row_array();
			$hasil[ $key ]['thread'] = $home['thread'];
		}
		return $hasil;
	}

	public function sql_last( $limit=null ) {
		if ($limit==null) $limit=5;
		$query = $this->db->query("Select concat(idparent,'.',idchild) idkey, idparent, idchild, max(tglpost) tglpost From d_forum Group By 1,2,3 Order By tglpost Desc Limit $limit");
		$hasil = $this->fc->ToArr( $query->result_array(), 'idkey');

		foreach ($hasil as $key=>$value) {
			// Forum Room
			$query = $this->db->query("Select nmroom From d_forum_room where idparent=". $value['idparent'] ." and idchild=". $value['idchild']);
			$room  = $query->row_array();
			$hasil[ $key ]['nmroom'] = $room['nmroom'];

			// Forum Home of Room
			$query = $this->db->query("Select thread From d_forum where idparent=". $value['idparent'] ." and idchild=". $value['idchild'] ." and idforum=1");
			$home  = $query->row_array();
			$hasil[ $key ]['thread'] = $home['thread'];
		}
		return $hasil;
	}

	public function post_room($idparent) {
		$query = $this->db->query("Select * From d_forum_room Where idchild=$idparent ");
		return $query->row_array();
	}

	public function post_head($idparent, $idchild) {
		$query = $this->db->query("Select a.*, nmuser, nip, jabatan, nmso From d_forum a Left Join t_user b On a.iduser=b.iduser Left Join t_so c On b.kdso=c.kdso Where idparent=$idparent and idchild=$idchild and idforum=1");
		return $query->row_array();
	}

	public function post_reply($idparent, $idchild) {
		$query = $this->db->query("Select a.*, nmuser, nip, jabatan, nmso From d_forum a Left Join t_user b On a.iduser=b.iduser Left Join t_so c On b.kdso=c.kdso Where idparent=$idparent and idchild=$idchild and idforum!=1 Order By idforum");
		return $query->result_array();
	}

	public function room_option($idparent=null, $idchild=null) {
		$query = $this->db->query("Select *, concat(idparent,'.',idchild) kdkey, '' selected From d_forum_room Where idparent>0 And kategori='1' Order By idparent,idchild");
		$hasil = $this->fc->ToArr( $query->result_array(), 'kdkey');
		
		foreach ($hasil as $key=>$value) {
			if ($value['idparent']==1) $hasil[$key]['kdkey'] = $value['idchild'] .'.0';
			if ($value['idparent']==$idparent and $value['idchild']==$idchild) $hasil[$key]['selected'] = 'selected';
		}

		$hasil = $this->fc->array_index($hasil, 'kdkey');
		return $hasil;
	}

}