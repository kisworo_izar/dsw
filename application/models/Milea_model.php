<?php
class Milea_model extends CI_Model {

	public function get_absen_manual($nip) {
		$query  = $this->dbmilea->query("SELECT a.*, b.nama namapenandatangan , '' isi,'' jml FROM absen_manual a left join pegawai_induk b on a.penandatangan=b.nip18 WHERE a.nip='$nip' order by tanggalsurat desc, id desc");
		$hsl  	= $this->fc->ToArr($query->result_array(),'id');
		$tgl  	= array();
		$thang 	= date('Y');

		$qry  = $this->dbmilea->query("SELECT thang, tgl, kerja FROM t_harikerja WHERE thang='$thang'");
		$krj  = $this->fc->ToArr($qry->result_array(),'tgl');

		foreach ($hsl as $row) $hsl[$row['id']]['isi'] = array();
		foreach($hsl as $row){
			if(array_key_exists($row['id'], $hsl)){
				for ($i=new DateTime($row['tanggalmulai']); $i<=new DateTime($row['tanggalselesai']) ; $i->modify('+1 day')) { 
					$date = $i->format("Y-m-d");
					array_push($hsl[$row['id']]['isi'], $date);
				}
			}
			$arr = $hsl[$row['id']]['isi'];
			$hsl[$row['id']]['isi'] = array_flip($arr);
			$hsl[$row['id']]['jml'] = (int)$hsl[$row['id']]['jml'];
			foreach($arr as $row2) {
				if(array_key_exists($row2, $krj)) $hsl[$row['id']]['isi'][$row2] = $krj[$row2]['kerja'];
				$hsl[$row['id']]['jml'] += (int)$hsl[$row['id']]['isi'][$row2];
			}

		}
		$hsl['dat'] = $hsl;
		return $hsl;
	}

	public function get_data_pegawai($nip) {
		$query  = $this->dbmilea->query("SELECT a.nip18, nama, gelardepan, gelarbelakang, jabatan, esl2, esl3, esl4,iduser FROM pegawai_induk a LEFT JOIN pegawai_iduser b ON a.nip18=b.nip18 where a.nip18 = '$nip' ");
		$hsl = $query->row_array();
		return $hsl;
	}

	public function get_absen_log( $tgl ) {
		$query  = $this->dbmilea->query("SELECT a.*, nmuser FROM absen_log a LEFT JOIN t_user b ON a.iduser=b.iduser WHERE Left(tgl,10)='$tgl' ORDER BY nmuser,tgl");
		$hsl = $query->result_array();
		return $hsl;
	}

	function save(){
		$nip = $this->session->userdata('nip');
		$iduser = $this->session->userdata('iduser');
		$tglsurat 		= date("Y-m-d");
		$sts 			= $_POST['sts'];  
		if($sts=='CAPB' or $sts=='CAPH' or $sts=='CAPI' or $sts=='CAPM' or $sts=='CAPP' or $sts=='CAPS') $sts = 'CP';
		$tglmulai 		= $this->fc->ustgl($_POST['tglmulai']);  
		$tglselesai		= $this->fc->ustgl($_POST['tglselesai']);
		if ($sts == 'TMH') {
			foreach ($_POST['datangpulang'] as $datangpulang) {
				if ($datangpulang == 'datang') {
					$tglmulai .= ' '.$_POST['datang_jam'].':00';
				} elseif ($datangpulang == 'pulang') {
					$tglselesai .= ' '.$_POST['pulang_jam'].':00';
				}
			}
		}

		$ket			= $_POST['ket'];
		$penandatangan	= $_POST['penandatangan'];
		$jab			= $_POST['jab'];
		$dok			= $_POST['nmfile'];
		// $dok			= $_FILES['dok']['name'];
        
        $this->dbmilea->trans_start();

		$this->dbmilea->query( "INSERT INTO absen_manual (nip,iduser,tanggalsurat,status,tanggalmulai,tanggalselesai,keterangan,penandatangan,jabatan,dokumen,approval ) VALUES ('$nip','$iduser','$tglsurat','$sts','$tglmulai','$tglselesai','$ket','$penandatangan','$jab','$dok','3')" );

        $insert_id = $this->dbmilea->insert_id();

        $this->dbmilea->trans_complete();

        return $insert_id;

	}

	function harian_manual_save(){
		$nip  = $_POST['nip'];
		$tgl  = $this->fc->ustgl($_POST['tgl']);
		$dtg  = $_POST['jamawal'];
		$plg  = $_POST['jamplg'];
		$sts  = $_POST['sts'];
		$ket  = $_POST['keterangan'];
		$this->dbmilea->query("UPDATE absen_net SET status='$sts', sumber='Manual', lembur='0', tgldatang='$tgl $dtg:00', tglpulang='$tgl $plg:00', pelanggaran='0', keterangan='$ket' WHERE nip='$nip' AND tgl='$tgl' ");

	}

	function del($id){
		$this->dbmilea->query( "Delete From absen_manual Where id='$id'" );
	}

	function hdr(){
		$qry        = $this->dbmilea->query("SELECT kode, uraian,sts,pelanggaran FROM t_kehadiran WHERE sts='1' ");
		// $arr 		= array('0'=>array('kode'=>'000', 'uraian'=>'Pilih Izin','sts'=>'','pelanggaran'=>''));
		$hsl['dat'] = $qry->result_array() ;
		// $hsl['dat'] = array_merge_recursive($arr,$qry->result_array() );

		$qry        = $this->dbmilea->query("SELECT nip18, nama, jabatan, esl1, esl2, esl3, esl4 FROM pegawai_induk");
		// $arr 		= array('0'=>array('nip18'=>'Pilih Penandatangan','nama'=>'', 'jabatan'=>'', 'esl1'=>'', 'esl2'=>'', 'esl3'=>'', 'esl4'=>''));
		// $hsl['peg'] = $this->fc->ToArr(array_merge_recursive($arr,$qry->result_array()),'nip18');
		$hsl['peg'] = $this->fc->ToArr($qry->result_array(),'nip18');

		return $hsl;
	}

	function krj(){
		$qry = $this->dbmilea->query("SELECT YEAR(CURDATE()) tahun");
        $thn = $qry->row_array();
        $tahun = $thn['tahun'];
		$now = date("Y-m-d");

		$qry = $this->dbmilea->query("SELECT thang,tgl,kerja FROM t_harikerja WHERE tgl BETWEEN DATE_SUB(CURDATE(), INTERVAL 20 DAY) AND CURDATE() AND kerja='1' ORDER BY tgl DESC ");
		$arr = $qry->result_array();
		$tgl = $arr[3]['tgl'];

		$diff = abs(strtotime($now)-strtotime($tgl));
		$hsl  = round($diff / 86400);

		return $hsl;
	}

	/**
     * Fungsi ini untuk mengambil data presensi pegawai berdasarkan nip, dan rentang waktu 
     */
	function getPresensi($nip='', $tanggalawal='', $tanggalakhir='')
	{
		$this->dbmilea->select('*');
		$this->dbmilea->from('absen_net');
		if (!empty($nip)) {
			$this->dbmilea->where('nip', $nip);
		}
		if (!empty($tanggalawal)) {
			$this->dbmilea->where('tgl >=', $tanggalawal);
		}
		if (!empty($tanggalakhir)) {
			$this->dbmilea->where('tgl <=', $tanggalakhir);
		}

		return $this->dbmilea->get()->result();
	}

	/* Fungsi ini untuk mengambil jumlah hari kerja */
	function jumlahHariKerja($tanggalawal, $tanggalakhir)
	{
		$this->dbmilea->select('*');
		$this->dbmilea->from('t_harikerja');
		$this->dbmilea->where('tgl >=', $tanggalawal);
		$this->dbmilea->where('tgl <=', $tanggalakhir);
		$this->dbmilea->where('kerja', '1');

		return count($this->dbmilea->get()->result());
	} 

	/* GET JUMLAH HARI PEGAWAI BERKERJA */
	function jumlahHariKerjaPegawai($nip, $tanggalawal, $tanggalakhir)
	{
		$this->dbmilea->select('*');
		$this->dbmilea->from('t_kehadiran');
		$this->dbmilea->where('sts', '1');
		$statuss = ['HN', 'TK'];
		foreach ($this->dbmilea->get()->result() as $row) {
			array_push($statuss, $row->kode);
		}

		$this->dbmilea->select('*');
		$this->dbmilea->from('absen_net');
		$this->dbmilea->where('nip', $nip);
		$this->dbmilea->where('tgl >=', $tanggalawal);
		$this->dbmilea->where('tgl <=', $tanggalakhir);
		$this->dbmilea->where_in('status', $statuss);

		return count($this->dbmilea->get()->result());
	} 

	/* CONVERT N1 HARI KERJA YANG LALU KE N2 HARI KALENDER YANG LALU */
	function harikerjaToHarikalender($nip, $n)
	{
		$this->dbmilea->select('*');
		$this->dbmilea->from('t_kehadiran');
		$this->dbmilea->where('sts', '1');
		$statuss = ['HN', 'TK'];
		foreach ($this->dbmilea->get()->result() as $row) {
			array_push($statuss, $row->kode);
		}

		$this->dbmilea->select('*');
		$this->dbmilea->from('absen_net');
		$this->dbmilea->where('nip', $nip);
		$this->dbmilea->where('tgl <', date('Y-m-d'));
		$this->dbmilea->where_in('status', $statuss);
		$this->dbmilea->order_by('tgl', 'desc');
		$this->dbmilea->limit($n);
		$result = $this->dbmilea->get()->result();

		if (empty($result)) {
			return 0;
		}

		return (strtotime(date('Y-m-d')) - strtotime($result[count($result)-1]->tgl)) / (24*60*60); 
	}
}
