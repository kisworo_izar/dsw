<?php
class Admin_satudja_model extends CI_Model {

	public function get_user() {
		$dbsatu = $this->load->database('dbsatu', TRUE); $no=0;
		$query  = $dbsatu->query("SELECT iduser, nmuser, fullname, email, nohp, kddept, kdunit, idusergroup FROM t_user_satu ORDER BY kddept, kdunit, nmuser, iduser");
		$hasil  = $query->result_array();

		foreach ($hasil as $key=>$value) { $hasil[$key]['recid'] = $no; $no++; }
		$data['table'] = $hasil;

		$qry    		= $dbsatu->query("SELECT idusergroup, nmusergroup FROM t_user_group_satu WHERE idusergroup NOT LIKE 'admin%' ORDER BY idusergroup");
		$data['subdit'] = $this->fc->array_index($qry->result_array(), 'idusergroup');

		return $data;
	}

	public function save_user(  ) {
		$dbsatu	 	= $this->load->database('dbsatu', TRUE);
		$aksi 	 	= $_POST['aksi'];
		$iduser  	= $_POST['iduser'];
		$nmuser  	= $_POST['nmuser'];
		$fullname	= $_POST['fullname'];
		$kddept	 	= $_POST['kddept'];
		$kdunit	 	= $_POST['kdunit'];
		$nohp 	 	= $_POST['nohp'];
		$email 	 	= $_POST['email'];
		$idusergroup= $_POST['idusergroup'];

		if ($aksi=='Rekam') 
			$nil = $dbsatu->query( "Insert Into t_user_satu (iduser,nmuser,fullname,kddept,kdunit,nohp,email,idusergroup) Values ('$iduser','$nmuser','$fullname','$kddept','$kdunit','$nohp','$email','$idusergroup')" );
		if ($aksi=='Ubah') 
			$nil = $dbsatu->query( "Update t_user_satu Set nmuser='$nmuser', fullname='$fullname', kddept='$kddept', kdunit='$kdunit', nohp='$nohp', email='$email', idusergroup='$idusergroup' Where iduser='$iduser'" );
		if ($aksi=='Hapus') 
			$nil = $dbsatu->query( "Delete From t_user_satu where iduser='$iduser'" );

		if ($nil > 0) $nil = "[{ iduser:'$iduser', nmuser:'$nmuser', fullname:'$fullname', kddept:'$kddept', kdunit:'$kdunit', nohp:'$nohp', email:'$email', idusergroup:'$idusergroup' }]"; else $nil = '';
		return $nil;
	}


	public function get_user_group() {
		$dbsatu = $this->load->database('dbsatu', TRUE); $no = 0;
		$query  = $dbsatu->query("Select * From t_user_group_satu Order By idusergroup");
		$hasil  = $query->result_array();


		foreach ($hasil as $key=>$value) { 
			$hasil[$key]['recid'] = $no; $no++; 
		}
		$data['table'] = $hasil;
		// echo '<pre>';print_r($data['table']);exit();
		return $data;
	}

	public function save_user_group(  ) {
		$dbsatu		 = $this->load->database('dbsatu', TRUE);
		$aksi		 = $_POST['aksi'];
		$recid 	 	 = $_POST['recid'];
		$idusergroup = $_POST['idusergroup'];
		$nmusergroup = $_POST['nmusergroup'];
		$menu		 = $_POST['menu'];
		$kwdept		 = $_POST['kwdept'];
		$kwunit		 = $_POST['kwunit'];
		$kwlokasi	 = $_POST['kwlokasi'];

		if ($aksi=='Rekam') 
			$nil = $dbsatu->query( "Insert Into t_user_group_satu (idusergroup,nmusergroup,menu,kwdept,kwunit,kwlokasi) Values ('$idusergroup','$nmusergroup','$menu','$kwdept','$kwunit','$kwlokasi')" );
		if ($aksi=='Ubah') 
			$nil = $dbsatu->query( "Update t_user_group_satu Set nmusergroup='$nmusergroup', menu='$menu', kwdept='$kwdept', kwunit='$kwunit', kwlokasi='$kwlokasi' Where idusergroup='$idusergroup'" );
		if ($aksi=='Hapus') 
			$nil = $dbsatu->query( "Delete From t_user_group_satu where idusergroup='$idusergroup'" );

		if ($nil > 0) $nil = "{ 'idusergroup':'$idusergroup', 'nmusergroup':'$nmusergroup', 'menu':'$menu', 'kwdept':'$kwdept', 'kwunit':'$kwunit', 'kwlokasi':'$kwlokasi', 'recid':$recid }"; else $nil = '';
		return str_replace("'", '"', $nil);
	}

	public function get_menu() {
		$dbsatu = $this->load->database('dbsatu', TRUE); $no = 0;
		$query  = $dbsatu->query("Select * From t_menu Where thang='2018' Order By idmenu");
		$hasil  = $query->result_array();

		foreach ($hasil as $key=>$value) { 
			$hasil[$key]['recid'] = $no; $no++; 
			if (substr($value['idmenu'],2,4) == '0000') 	$hasil[$key]['w2ui'] = array('style' => 'font-weight: bold');
			else if (substr($value['idmenu'],5,2) == '00') 	$hasil[$key]['menu'] = '<span style="padding-left: 20px">'. $value['menu'] .'</span>';
			else $hasil[$key]['menu'] = '<span style="padding-left: 40px">'. $value['menu'] .'</span>';
		}
		$data['table'] = $hasil;
		return $data;
	}

	public function save_menu(  ) {
		$dbsatu	= $this->load->database('dbsatu', TRUE);
		$aksi	= $_POST['aksi'];
		$idmenu = $_POST['idmenu'];
		$menu 	= $_POST['menu'];
		$link	= $_POST['link'];
		$icon	= $_POST['icon'];
		$urutan	= $_POST['urutan'];
		$aktif	= $_POST['aktif'];

		if ($aksi=='Rekam') 
			$nil = $dbsatu->query( "Insert Into t_menu (idmenu,menu,link,icon,urutan,aktif) Values ('$idlink','$menu','$menu','$icon','$urutan','$aktif')" );
		if ($aksi=='Ubah') 
			$nil = $dbsatu->query( "Update t_menu Set menu='$menu', link='$link', icon='$icon', urutan='$urutan', aktif='$aktif' Where idmenu='$idmenu'" );
		if ($aksi=='Hapus') 
			$nil = $dbsatu->query( "Delete From t_menu where idmenu='$idmenu'" );

		if ($nil > 0) $nil = "[{ idmenu:'$idmenu', menu:'$menu', link:'$link', icon:'$icon', urutan:'$urutan', aktif:'$aktif' }]"; else $nil = '';
		return $nil;
	}
}