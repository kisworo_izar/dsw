<?php
class Admin_panel_model extends CI_Model {

	public function get_user() {
		$query = $this->db->query("Select *, '' nmusergroup From t_user Where kdpeg='1' Order By kdso, kdeselon, nmuser");
		$peg   = $query->result_array();

		$query = $this->db->query("Select *, '' nmusergroup From t_user Where kdpeg='0' Order By iduser");
		$nonpeg= $this->fc->ToArr( $query->result_array(), 'iduser');
		foreach ($nonpeg as $key=>$value) $nonpeg[$key]['kdso'] = '990000';

		$hasil = $this->fc->ToArr( array_merge_recursive($peg, $nonpeg), 'iduser');

		$query = $this->db->query("Select * From t_user_group");
		$group = $this->fc->ToArr( $query->result_array(), 'idusergroup');
		foreach ($hasil as $key=>$value) {
			$arr = explode(';', $value['idusergroup']);
			foreach ($arr as $row) if (array_key_exists($row, $group)) $hasil[$key]['nmusergroup'] .= $group[$row]['nmusergroup'] .' ';
		}

		$query = $this->db->query("Select kdso, nmso From t_so Order By 1");
		$data['t_so']  = $query->result_array();
		$data['tabel'] = $this->fc->Array_Index($hasil, 'kdso');
		return $data;
	}

	public function save_user(  ) {
		$aksi  = $_POST['aksi'];
		$isian = $_POST['isian'];

		// Konversi ARRAY ISIAN ke Variabel
		$iduser 	= $isian[0];
		$nmuser 	= $isian[1];
		$fullname 	= $isian[2];
		$nip 		= $isian[4];
		$pangkat	= $isian[5];
		$golongan	= $isian[6];
		$kdso		= $isian[8];
		$email		= $isian[9];
		$nohp		= $isian[10];
		$idusergroup= $isian[12];
		$jabatan	= $isian[17];
		$kdeselon	= $isian[18];
		$passawal 	= '202cb962ac59075b964b07152d234b70';

		if ($aksi=='Rekam') {
			$this->db->query( "Insert Into t_user (iduser, nmuser, nip, pangkat, golongan, kdso, email, nohp, idusergroup, jabatan, kdeselon, password,kdpeg) Values ('$iduser', '$nmuser', '$nip', '$pangkat', '$golongan', '$kdso', '$email', '$nohp', '$idusergroup', '$jabatan', '$kdeselon', '$passawal','1')" );
		}
		if ($aksi=='Ubah') {
			$this->db->query( "Update t_user Set nmuser='$nmuser', fullname='$fullname', nip='$nip', pangkat='$pangkat', golongan='$golongan', kdso='$kdso', email='$email', nohp='$nohp', idusergroup='$idusergroup', jabatan='$jabatan', kdeselon='$kdeselon' Where iduser='$iduser'" );
		}
		if ($aksi=='Hapus') {
			$this->db->query( "Delete From t_user where iduser='$iduser'" );
		}
		return implode('#', $isian);
	}


	public function get_user_group() {
		$query = $this->db->query("Select *, '' daftar From t_user_group Order By idusergroup");
		$hasil = $this->fc->ToArr($query->result_array(), 'idusergroup');
		foreach ($hasil as $key=>$value) {
			$str = str_replace("-X", "", $value['menu']);
			$str = str_replace("-R", "", $str);
			$query = $this->db->query("Select idmenu, menu From t_menu Where idmenu in ('". str_replace(";", "','", $str) ."') Order By urutan, idmenu");
			$hasil[$key]['daftar'] = $query->result_array();
		}

		$data['tabel'] = $hasil;
		return $data;
	}

	public function save_user_group(  ) {
		$aksi  = $_POST['aksi'];
		$isian = $_POST['isian'];

		// Konversi ARRAY ISIAN ke Variabel
		$idusergroup = $isian[0];
		$nmusergroup = $isian[1];
		$menu  		 = $isian[2];

		if ($aksi=='Rekam') {
			$this->db->query( "Insert Into t_user_group (idusergroup, nmusergroup, menu) Values ('$idusergroup', '$nmusergroup', '$menu')" );
		}
		if ($aksi=='Ubah') {
			$this->db->query( "Update t_user_group Set nmusergroup='$nmusergroup', menu='$menu' Where idusergroup='$idusergroup'" );
		}
		if ($aksi=='Hapus') {
			$this->db->query( "Delete From t_user_group where idusergroup='$idusergroup'" );
		}
		return implode('#', $isian);
	}

	public function get_so() {
		$query = $this->db->query("Select * From t_so Order By kdso");
		$hasil = $query->result_array();
		$data['t_so'] = $this->fc->ToArr($hasil, 'kdso');
		return $data;
	}

	public function save_so(  ) {
		$aksi  = $_POST['aksi'];
		$isian = $_POST['isian'];

		// Konversi ARRAY ISIAN ke Variabel
		$kdso 			= $isian[0];
		$kodeorganisasi= $isian[1];
		$nmso 			= $isian[2];
		$kdsosurat  	= $isian[3];
		$intern			= $isian[4];
		$ekstern			= $isian[5];
		$nmso1			= $isian[6];

		if ($aksi=='Rekam') {
			$this->db->query( "Insert Into t_so (kdso, kodeorganisasi, nmso, kdsosurat, intern, ekstern, nmso1) Values ('$kdso', '$kodeorganisasi', '$nmso', '$kdsosurat', '$intern', '$ekstern', '$nmso1')" );
		}
		if ($aksi=='Ubah') {
			$this->db->query( "Update t_so Set kodeorganisasi='$kodeorganisasi', nmso='$nmso', kdsosurat='$kdsosurat', intern='$intern', ekstern='$ekstern', nmso1='$nmso1' Where kdso='$kdso'" );
		}
		if ($aksi=='Hapus') {
			$this->db->query( "Delete From t_so where kdso='$kdso'" );
		}
		return implode('#', $isian);
	}


	public function get_menu() {
		$query = $this->db->query("Select * From t_menu Order By urutan, idmenu");
		$hasil = $query->result_array();
		$data['tabel'] = $this->fc->ToArr($hasil, 'idmenu');
		return $data;
	}

	public function save_menu(  ) {
		$aksi  = $_POST['aksi'];
		$isian = $_POST['isian'];

		// Konversi ARRAY ISIAN ke Variabel
		$idmenu = $isian[0];
		$menu 	= $isian[1];
		$link  	= $isian[2];
		$icon		= $isian[3];
		$urutan	= $isian[4];
		$keterangan	= $isian[5];
		$onoff	= $isian[6];

		if ($aksi=='Rekam') {
			$this->db->query( "Insert Into t_menu (idmenu, menu, link, icon, urutan, keterangan, onoff) Values ('$idmenu', '$menu', '$link', '$icon', '$urutan', '$keterangan', '$onoff')" );
		}
		if ($aksi=='Ubah') {
			$this->db->query( "Update t_menu Set menu='$menu', link='$link', icon='$icon', urutan='$urutan', keterangan='$keterangan', onoff='$onoff' Where idmenu='$idmenu'" );
		}
		if ($aksi=='Hapus') {
			$this->db->query( "Delete From t_menu where idmenu='$idmenu'" );
		}
		return implode('#', $isian);
	}

	function respass($iduser){
		$id = str_replace('id_', '', $iduser);
		$this->db->query("UPDATE t_user SET password=md5('123') WHERE iduser='$id'");
	}
}
