<?php
class Milea_laporan_model extends CI_Model {

	function get_data($es, $subdit, $seksi, $start, $end) {
		/* Start Declare Variable Tahun dan Pencarian*/
		$year   = date("Y");
		$whrtgl = "WHERE tgl BETWEEN '$start' AND '$end' ";
		/* End Declare Variable Tahun dan Pencarian*/

		/* Start Query Ambil nama dari Referensi*/
		$qry 		= $this->dbmilea->query("SELECT namaorganisasi FROM t_eselon2 WHERE kodeorganisasi='$es' AND aktif='1'");
		$es2    = $qry->result_array();

		$qry 		= $this->dbmilea->query("SELECT namaorganisasi FROM t_eselon3 WHERE kodeorganisasi='$subdit' AND aktif='1'");
		$es3    = $qry->result_array();

		$qry 		= $this->dbmilea->query("SELECT namaorganisasi FROM t_eselon4 WHERE kodeorganisasi='$seksi' AND aktif='1'");
		$es4    = $qry->result_array();
		/* End Query Ambil nama dari Referensi*/

		/* Start Query hariKerja nama dari Referensi*/
		$qry    = $this->dbmilea->query("SELECT tgl, kerja FROM t_harikerja WHERE thang<='$year' ");
		$krj    = $this->fc->ToArr($qry->result_array(), 'tgl');
		// echo"<pre>";print_r($krj);exit();
		/* End Query hariKerja nama dari Referensi*/


		$query 	= $this->dbmilea->query("SELECT * FROM pegawai_induk");
		$dtl	= $this->fc->ToArr($query->result_array(),'nip18');

		/*Start Kondisi Pencarian Per Unit Eselon*/
      if($es !='000')     $whr  = " AND esl2 ". $this->fc->InVar($es2, 'namaorganisasi'); else $whr="";
      if($subdit !='000') $whr .= " AND esl3 ". $this->fc->InVar($es3, 'namaorganisasi');
      if($seksi !='000')  $whr .= " AND esl4 ". $this->fc->InVar($es4, 'namaorganisasi');
	  if($es =='1') 	  $whr  = " AND esl1='Direktorat Jenderal Anggaran' AND esl2=''";
		/*End Kondisi Pencarian Per Unit Eselon*/


		/* Ambil Nama dari Pegawai*/
    $query  	= $this->dbmilea->query("SELECT nip18,uraianstatus FROM pegawai_induk WHERE uraianstatus <> '' $whr ");
    $dat['nip'] = $query->result_array();
    $wherenip  	= " AND nip ". $this->fc->InVar($dat['nip'],'nip18');
		$query 			= $this->dbmilea->query("SELECT  iduser, nip, tgldatang, tglpulang, status, sumber, keterangan, tgl,'' nama, '' es1, '' es2, '' es3, '' isi, '' krj FROM absen_net $whrtgl $wherenip  ORDER BY nip ");
		$dat   			= $this->fc->ToArr($query->result_array(),'nip');
		$qry    = $this->dbmilea->query("SELECT '' iduser,'' nip, '' tgldatang, '' tglpulang, '' status, '' sumber, '' keterangan, tgl,'' nama, '' es1, '' es2, '' es3, '' isi, '' krj from absen_net $whrtgl ORDER BY tgl ");
		$tgl    = $this->fc->ToArr($qry->result_array(), 'tgl');

		foreach ($dat as $key=>$row)  $dat[$key]['isi'] = array();
		foreach ($query->result_array() as $row) {
			if ( array_key_exists($row['nip'], $dat)) $arr  = $dat[$row['nip']]['isi'];
			array_push($arr, $row);
			$array_1 = $tgl;
			$array_2 = $this->fc->ToArr($arr,'tgl');
			$hsl     = array_merge($array_1,$array_2);
			foreach($hsl as $row2){
				$hsl[$row2['tgl']]['krj'] = $krj[$row2['tgl']]['kerja'];
			}

			$dat[$row['nip']]['isi'] =  $hsl;
		}

		foreach ($dat as $key => $row) {
			if ( array_key_exists($row['nip'], $dtl) ) {
				$dat[$key]['es1']  = $dtl[$row['nip']]['esl1'];
				$dat[$key]['es2']  = $dtl[$row['nip']]['esl2'];
				$dat[$key]['es3']  = $dtl[$row['nip']]['esl3'];
				$dat[$key]['nama'] = $dtl[$row['nip']]['nama'];
			}
			$arr = $dat[$row['nip']]['isi'];
			$dat[$row['nip']]['isi'] = $this->fc->array_index($arr,'tgl');
		}
		// echo"<pre>";print_r($dat);exit();

		return $dat;
	}


	function get_menu($es,$subdit,$seksi,$start,$end){
		$usr 		= $this->session->userdata('iduser');
		$idusr  = $this->session->userdata('idusergroup');
		$grp 		= explode(';', $idusr);
		$kd 		= $this->db->query("SELECT kodeorganisasi FROM t_user a LEFT JOIN t_so b ON a.kdso=b.kdso WHERE iduser= '$usr'")->row_array();

		if($es !='000') 		$whr 	= " WHERE LEFT(kodeorganisasi,6)='$es' AND aktif='1' "; 		else $whr =" WHERE aktif='1'";
		if($subdit !='000') $whr2 = " WHERE LEFT(kodeorganisasi,8)='$subdit' AND aktif='1' "; else $whr2 = " WHERE aktif='1'";

		$query 			= $this->dbmilea->query("SELECT kodeorganisasi, namaorganisasi, aktif FROM t_eselon2 $whr ");
		if( in_array('002', $grp)  or substr($kd['kodeorganisasi'], 0,6)=='350401' ) {
			$arr  = array('0'=>array('kodeorganisasi'=>'000', 'namaorganisasi'=>'Seluruh Unit Eselon 2', 'aktif'=>'1'));
			$dir  = array('0'=>array('kodeorganisasi'=>'1', 'namaorganisasi'=>'Dirjen Anggaran', 'aktif'=>'1'));
			$hsl['es2'] = array_merge_recursive($arr,$query->result_array() );
			$hsl['es2'] = array_merge_recursive($dir, $hsl['es2']);
		} else $hsl['es2'] = $query->result_array();
		// if($es !='000') $hsl['es2'] = $query->result_array();

		// echo"<pre>";print_r($hsl['es2']);exit();


		$query 			= $this->dbmilea->query("SELECT kodeorganisasi, namaorganisasi, aktif FROM t_eselon3 $whr ");
		$arr 				= array('0'=>array('kodeorganisasi'=>'000', 'namaorganisasi'=>'Seluruh Unit Eselon 3', 'aktif'=>'1'));
		$hsl['es3'] = array_merge_recursive($arr,$query->result_array() );

		if($es=='000') $hsl['es3'] = array('0'=>array('kodeorganisasi'=>'000', 'namaorganisasi'=>'Seluruh Unit Eselon 3', 'aktif'=>'1'));


		$query 			= $this->dbmilea->query("SELECT kodeorganisasi, namaorganisasi, aktif FROM t_eselon4 $whr2 ");
		$arr 	    	= array('0'=>array('kodeorganisasi'=>'000', 'namaorganisasi'=>'Seluruh Unit Eselon 4', 'aktif'=>'1'));
		$hsl['es4'] = array_merge_recursive($arr,$query->result_array() );
		if($subdit=='000' AND $es='000') $hsl['es4'] = array('0'=>array('kodeorganisasi'=>'000', 'namaorganisasi'=>'Seluruh Unit Eselon 4', 'aktif'=>'1'));


		//Kondisi Pilihan es2
		$arr 		  		= $hsl['es2'][0];
		$hsl['pil2']  = $arr['kodeorganisasi'];

		// echo"<pre>";print_r($hsl['pil2']);exit();
		//Kondisi Pilihan es3
		$arr 	      	= $hsl['es3'][0];
		$hsl['pil3']  = $arr['kodeorganisasi'];
		//Kondisi Pilihan es4
		$arr 	      	= $hsl['es4'][0];
		$hsl['pil4']  = $arr['kodeorganisasi'];

		$hsl['start'] = $this->fc->idtgl($start);
		$hsl['end']   = $this->fc->idtgl($end);


		return $hsl;
	}

	function get_pil($es,$subdit,$seksi){

		$qry 				= $this->dbmilea->query("SELECT namaorganisasi FROM t_eselon2 WHERE kodeorganisasi='$es' AND aktif='1'");
		$pil['es2'] = $qry->row_array();

		$qry 	      = $this->dbmilea->query("SELECT namaorganisasi FROM t_eselon3 WHERE kodeorganisasi='$subdit' AND aktif='1'");
		$pil['es3'] = $qry->row_array();

		$qry 				= $this->dbmilea->query("SELECT namaorganisasi FROM t_eselon4 WHERE kodeorganisasi='$seksi' AND aktif='1'");
		$pil['es4'] = $qry->row_array();

		return $pil;
	}

	function get_ketertiban($es,$subdit,$seksi,$start,$end){
		/* Start Declare Variable Tahun dan Pencarian*/
		$year   = date("Y");
		$whrtgl = "WHERE tgl BETWEEN '$start' AND '$end' ";
		/* End Declare Variable Tahun dan Pencarian*/

		/* Start Query Ambil nama dari Referensi*/
		$qry 	= $this->dbmilea->query("SELECT namaorganisasi FROM t_eselon2 WHERE kodeorganisasi='$es' AND aktif='1'");
		$es2    = $qry->result_array();

		$qry 	= $this->dbmilea->query("SELECT namaorganisasi FROM t_eselon3 WHERE kodeorganisasi='$subdit' AND aktif='1'");
		$es3    = $qry->result_array();

		$qry 	= $this->dbmilea->query("SELECT namaorganisasi FROM t_eselon4 WHERE kodeorganisasi='$seksi' AND aktif='1'");
		$es4    = $qry->result_array();

		$query 	= $this->dbmilea->query("SELECT * FROM pegawai_induk");
		$dtl	= $this->fc->ToArr($query->result_array(),'nip18');
		/* End Query Ambil nama dari Referensi*/

		/*Start Kondisi Pencarian Per Unit Eselon*/
        if($es !='000')     $whr  = " AND esl2 ". $this->fc->InVar($es2, 'namaorganisasi'); else $whr="";
        if($subdit !='000') $whr .= " AND esl3 ". $this->fc->InVar($es3, 'namaorganisasi');
        if($seksi !='000')  $whr .= " AND esl4 ". $this->fc->InVar($es4, 'namaorganisasi');
		if($es =='1') 		$whr  = " AND esl1='Direktorat Jenderal Anggaran' AND esl2=''";
		/*End Kondisi Pencarian Per Unit Eselon*/

		/* Ambil Nama dari Pegawai*/
        $query  	= $this->dbmilea->query("SELECT nip18 FROM pegawai_induk WHERE uraianstatus <> '' $whr ");
        // $query     = $this->dbmilea->query("SELECT nip18 FROM pegawai_induk $whr");
        $peg       = $this->fc->ToArr($query->result_array(),'nip18');
        $wherenip  = " AND nip ". $this->fc->InVar($peg,'nip18');


		$qry  = $this->dbmilea->query("SELECT nip,'' nama, status,'-' HN,'-' TL1,'-' PSW1,'-' T1P1,'-' TK,'-' TL2,'-' TL3,'-' TL4,'-' PSW2,'-' PSW3,'-' PSW4,'-' T1P2,'-' T1P2,'-' T1P3,'-' T1P4,'-' T2P1,'-' T2P2,'-' T2P3, '-' T2P4, '-' T3P1,'-' T3P2,'-' T3P3,'-' T3P4,'-' T4P1,'-' T4P2,'-' T4P3,'-' T4P4,'-' CB,'-' CH,'-' CLTN,'-' CP,'-' CSRI,'-' CSRJ,'-' CT,'-' DL,'-' IP,'-' TB, '-' RDK, COUNT(status) jml FROM absen_net $whrtgl $wherenip GROUP BY nip, status");
		$dat  = $this->fc->ToArr( $qry->result_array(),'nip');

		foreach ($qry->result_array() as $row) {
			if(array_key_exists($row['nip'], $dat)){
				$dat[$row['nip']][$row['status']] = $row['jml'];
			}
		}

		foreach ($dat as $row) {
			unset($dat[$row['nip']]['status']);
			unset($dat[$row['nip']]['jml']);
			unset($dat[$row['nip']]['']);
			if (array_key_exists($row['nip'], $dtl)){
				$dat[$row['nip']]['nama'] = $dtl[$row['nip']]['nama'];
			}
		}

		return $dat;
	}

	function get_pelanggaran($es,$subdit,$seksi,$start,$end){
		/* Start Declare Variable Tahun dan Pencarian*/
		$year   = date("Y");
		$whrtgl = "WHERE tgl BETWEEN '$start' AND '$end' ";
		/* End Declare Variable Tahun dan Pencarian*/

		/* Start Query Ambil nama dari Referensi*/
		$qry 		= $this->dbmilea->query("SELECT namaorganisasi FROM t_eselon2 WHERE kodeorganisasi='$es' AND aktif='1'");
		$es2    = $qry->result_array();

		$qry 		= $this->dbmilea->query("SELECT namaorganisasi FROM t_eselon3 WHERE kodeorganisasi='$subdit' AND aktif='1'");
		$es3    = $qry->result_array();

		$qry 		= $this->dbmilea->query("SELECT namaorganisasi FROM t_eselon4 WHERE kodeorganisasi='$seksi' AND aktif='1'");
		$es4    = $qry->result_array();

		$query 	= $this->dbmilea->query("SELECT * FROM pegawai_induk");
		$dtl		= $this->fc->ToArr($query->result_array(),'nip18');
		/* End Query Ambil nama dari Referensi*/

		/*Start Kondisi Pencarian Per Unit Eselon*/
        if($es !='000')     $whr  = " AND esl2 ". $this->fc->InVar($es2, 'namaorganisasi'); else $whr="";
        if($subdit !='000') $whr .= " AND esl3 ". $this->fc->InVar($es3, 'namaorganisasi');
        if($seksi !='000')  $whr .= " AND esl4 ". $this->fc->InVar($es4, 'namaorganisasi');
		if($es =='1') 		$whr  = " AND esl1='Direktorat Jenderal Anggaran' AND esl2=''";
		/*End Kondisi Pencarian Per Unit Eselon*/

		/* Ambil Nama dari Pegawai*/
        $query  	= $this->dbmilea->query("SELECT nip18 FROM pegawai_induk WHERE uraianstatus <> '' $whr ");
        $peg       = $this->fc->ToArr($query->result_array(),'nip18');
        $wherenip  = " AND nip ". $this->fc->InVar($peg,'nip18');

        $qry  = $this->dbmilea->query("SELECT nip,'' nama,'' es2,'' es3,'' es4,'' jbt, SUM(pelanggaran) plg FROM absen_net $whrtgl $wherenip GROUP BY nip");
		$dat  = $this->fc->ToArr( $qry->result_array(),'nip');

		// echo "<pre>";print_r($qry->result_array());exit();

		foreach ($dat as $row) {
			if ( array_key_exists($row['nip'], $dtl) ) {
				$dat[$row['nip']]['es2']  = $dtl[$row['nip']]['esl2'];
				$dat[$row['nip']]['es3']  = $dtl[$row['nip']]['esl3'];
				$dat[$row['nip']]['es4']  = $dtl[$row['nip']]['esl4'];
				$dat[$row['nip']]['nama'] = $dtl[$row['nip']]['nama'];
				$dat[$row['nip']]['jbt']  = $dtl[$row['nip']]['jabatan'];
			}
		}

		return $dat;

	}

	function get_makan($es,$subdit,$seksi,$start,$end){
		/* Start Declare Variable Tahun dan Pencarian*/
		$year   = date("Y");
		$whrtgl = "WHERE tgl BETWEEN '$start' AND '$end' ";
		/* End Declare Variable Tahun dan Pencarian*/

		/* Start Query Ambil nama dari Referensi*/
		$qry 	= $this->dbmilea->query("SELECT namaorganisasi FROM t_eselon2 WHERE kodeorganisasi='$es' AND aktif='1'");
		$es2    = $qry->result_array();

		$qry 	= $this->dbmilea->query("SELECT namaorganisasi FROM t_eselon3 WHERE kodeorganisasi='$subdit' AND aktif='1'");
		$es3    = $qry->result_array();

		$qry 	= $this->dbmilea->query("SELECT namaorganisasi FROM t_eselon4 WHERE kodeorganisasi='$seksi' AND aktif='1'");
		$es4    = $qry->result_array();

		$query 	= $this->dbmilea->query("SELECT * FROM pegawai_induk");
		$dtl	= $this->fc->ToArr($query->result_array(),'nip18');

		/* Start Query hariKerja nama dari Referensi*/
		$qry    = $this->dbmilea->query("SELECT tgl, kerja FROM t_harikerja WHERE thang='$year' ");
		$krj    = $this->fc->ToArr($qry->result_array(), 'tgl');
		/* End Query hariKerja nama dari Referensi*/

		$query  = $this->dbmilea->query("SELECT kode,ket FROM t_kehadiran WHERE ket='1'");
		$hdr    = $this->fc->ToArr($query->result_array(),'kode');

		/*Start Kondisi Pencarian Per Unit Eselon*/
        if($es !='000')     $whr  = " AND esl2 ". $this->fc->InVar($es2, 'namaorganisasi'); else $whr="";
        if($subdit !='000') $whr .= " AND esl3 ". $this->fc->InVar($es3, 'namaorganisasi');
        if($seksi !='000')  $whr .= " AND esl4 ". $this->fc->InVar($es4, 'namaorganisasi');
		if($es =='1') 		$whr  = " AND esl1='Direktorat Jenderal Anggaran' AND esl2=''";
		/*End Kondisi Pencarian Per Unit Eselon*/

		/* Ambil Nama dari Pegawai*/
        $query  	= $this->dbmilea->query("SELECT nip18 FROM pegawai_induk WHERE uraianstatus <> '' $whr ");
        $peg       = $this->fc->ToArr($query->result_array(),'nip18');
        $wherenip  = " AND nip ". $this->fc->InVar($peg,'nip18');

        $qry  = $this->dbmilea->query("SELECT nip,'' nama, status, tgl, '' isi, '' total, '0' ket, '' krj FROM absen_net $whrtgl $wherenip ORDER BY nip");
		$dat  = $this->fc->ToArr( $qry->result_array(),'nip');


		foreach($dat as $row) $dat[$row['nip']]['isi'] = array();

		foreach ($qry->result_array() as $row) {
			if(array_key_exists($row['status'], $hdr)) $row['ket'] = $hdr[$row['status']]['ket'];
			$dat[$row['nip']]['ket'] +=$row['ket'];
			if(array_key_exists($row['nip'], $dat))  $arr = $dat[$row['nip']]['isi'];
			array_push($arr, $row);
			$dat[$row['nip']]['isi'] = $this->fc->ToArr($arr,'tgl');
			$dat[$row['nip']]['isi'][$row['tgl']]['krj'] = $krj[$row['tgl']]['kerja'];
		}

		foreach($dat as $key => $row){
			if(array_key_exists($row['nip'], $dtl)) $dat[$key]['nama'] = $dtl[$row['nip']]['nama'];
			$arr = $dat[$row['nip']]['isi'];
			$dat[$row['nip']]['isi'] = $this->fc->array_index($arr,'tgl');
		}

		return $dat;

	}

	function get_lembur($es,$subdit,$seksi,$start,$end){
		/* Start Declare Variable Tahun dan Pencarian*/
		$year   = date("Y");
		$whrtgl = "WHERE tgl BETWEEN '$start' AND '$end' ";
		/* End Declare Variable Tahun dan Pencarian*/

		/* Start Query Ambil nama dari Referensi*/
		$qry 	= $this->dbmilea->query("SELECT namaorganisasi FROM t_eselon2 WHERE kodeorganisasi='$es' AND aktif='1'");
		$es2    = $qry->result_array();

		$qry 	= $this->dbmilea->query("SELECT namaorganisasi FROM t_eselon3 WHERE kodeorganisasi='$subdit' AND aktif='1'");
		$es3    = $qry->result_array();

		$qry 	= $this->dbmilea->query("SELECT namaorganisasi FROM t_eselon4 WHERE kodeorganisasi='$seksi' AND aktif='1'");
		$es4    = $qry->result_array();

		$query 	= $this->dbmilea->query("SELECT * FROM pegawai_induk");
		$dtl	= $this->fc->ToArr($query->result_array(),'nip18');

		/* Start Query hariKerja nama dari Referensi*/
		$qry    = $this->dbmilea->query("SELECT tgl, kerja FROM t_harikerja WHERE thang='$year' ");
		$krj    = $this->fc->ToArr($qry->result_array(), 'tgl');
		/* End Query hariKerja nama dari Referensi*/

		$query  = $this->dbmilea->query("SELECT kode,ket FROM t_kehadiran WHERE ket='1'");
		$hdr    = $this->fc->ToArr($query->result_array(),'kode');

		/*Start Kondisi Pencarian Per Unit Eselon*/
        if($es !='000')     $whr  = " AND esl2 ". $this->fc->InVar($es2, 'namaorganisasi'); else $whr="";
        if($subdit !='000') $whr .= " AND esl3 ". $this->fc->InVar($es3, 'namaorganisasi');
        if($seksi !='000')  $whr .= " AND esl4 ". $this->fc->InVar($es4, 'namaorganisasi');
		if($es =='1') 		$whr  = " AND esl1='Direktorat Jenderal Anggaran' AND esl2=''";
		/*End Kondisi Pencarian Per Unit Eselon*/

		/* Ambil Nama dari Pegawai*/
        $query  	= $this->dbmilea->query("SELECT nip18 FROM pegawai_induk WHERE uraianstatus <> '' $whr ");
        $peg       = $this->fc->ToArr($query->result_array(),'nip18');
        $wherenip  = " AND nip ". $this->fc->InVar($peg,'nip18');

        $qry  = $this->dbmilea->query("SELECT nip,'' nama, status, tgl, lembur, '' isi, '-' total, '' krj, rdk FROM absen_net $whrtgl $wherenip ORDER BY nip");
		$dat  = $this->fc->ToArr( $qry->result_array(),'nip');

		foreach($dat as $row) $dat[$row['nip']]['isi'] = array();

		foreach($qry->result_array() as $row){
			$dat[$row['nip']]['total'] +=$row['lembur'];
			if(array_key_exists($row['nip'], $dat)) $arr = $dat[$row['nip']]['isi'];
			array_push($arr, $row);
			$dat[$row['nip']]['isi'] = $arr;
			$dat[$row['nip']]['isi'] = $this->fc->ToArr($arr,'tgl');
			$dat[$row['nip']]['isi'][$row['tgl']]['krj'] = $krj[$row['tgl']]['kerja'];
		}


		foreach($dat as $key => $row){
			if(array_key_exists($row['nip'], $dtl)) $dat[$key]['nama'] = $dtl[$row['nip']]['nama'];
			$arr = $dat[$row['nip']]['isi'];
			$dat[$row['nip']]['isi'] = $this->fc->array_index($arr,'tgl');
		}

		return $dat;
	}

}
