<?php
class Revisi_dja_model extends CI_Model {

	private $dbrevisi;

	public function __construct()
		{
		parent::__construct();

		        //$this->dbrevisi = $this->load->database('dbsatu', TRUE);
		        $this->dbrevisi = $this->load->database('revisi'. $this->session->userdata('thang'), TRUE);
		        //echo $this->dbrevisi;exit;
		}

	public function get_data( $depuni=null, $start, $end, $tahun, $cari ) {

		if (! (strrpos($this->session->userdata('idusergroup'), '001') or strrpos($this->session->userdata('idusergroup'), '002') or strrpos($this->session->userdata('idusergroup'), '503') ) ) {
			$query 	= $this->db->query("Select kddept From revisi_wenang Where kdso='". $this->session->userdata('kdso') ."' and tahun ='$tahun' ");
			// echo "Select kddept From revisi_wenang Where kdso='". $this->session->userdata('kdso') ."' and tahun ='$tahun' <br>";
			$hasil 	= $query->row_array();
			// echo "$hasil <br>";
			$whr 	= "AND kl_dept IN ('". str_replace(",", "','", $hasil['kddept']) ."')";
			// echo "$whr";exit();
			$range 	= "";
		} else { 
			$whr = "";
			$range = " AND pus_tgl >= '$start 00:00:00' AND pus_tgl <= '$end 23:59.59' ";
		}

		if ($cari != '') {
			$whr.= " AND (rev_id LIKE '%$cari%' OR kl_dept LIKE '%$cari%' ) ";
			$range = "";
		}

		$query = $this->dbrevisi->query("Select rev_tahun, rev_id, rev_tahap, rev_status, kl_dept, '' nmdept, concat(kl_dept,'.',kl_unit) kl_unit, '' nmunit, kl_surat_no, kl_surat_tgl, kl_surat_hal,
			pus_status, pus_tgl, kl_pjb_jab, kl_pjb_nama, kl_pjb_nip, kl_telp, t2_catatan, t7_catatan,
			t2_status, if(t2_status='1',t2_proses_tgl,t2_selesai_tgl) t2_tgl,
			t3_status, if(t3_status='1',t3_proses_tgl,t3_selesai_tgl) t3_tgl,
			t4_status, if(t4_status='1',t4_proses_tgl,t4_selesai_tgl) t4_tgl,
			t5_status, if(t5_status='1',t5_proses_tgl,t5_selesai_tgl) t5_tgl,
			t6_status, if(t6_status='1',t6_proses_tgl,t6_selesai_tgl) t6_tgl,
			t7_status, if(t7_status='1',t7_proses_tgl,t7_selesai_tgl) t7_tgl, appl
			From revisi where rev_level ='1'  $whr $range Order By rev_tahun Desc, pus_tgl Desc");


		$hasil = $this->fc->ToArr( $query->result_array(), 'rev_id');
		//print_r($hasil); exit;
		
		if ($hasil) {
			$dbref = $this->load->database('ref'. $this->session->userdata('thang'), TRUE);
			$query = $dbref->query("Select kddept kode, nmdept uraian, '1' level From t_dept Where kddept ". $this->fc->InVar($hasil, 'kl_dept') );
			$dept  = $this->fc->ToArr( $query->result_array(), 'kode');

			$query = $dbref->query("Select concat(kddept,'.',kdunit) kode, nmunit uraian, '2' level From t_unit Where concat(kddept,'.',kdunit) ". $this->fc->InVar($hasil, 'kl_unit') );
			$unit  = $this->fc->ToArr( $query->result_array(), 'kode');

			foreach ($hasil as $key=>$value) {
				if (array_key_exists($value['kl_dept'], $dept)) $hasil[$key]['nmdept'] = $dept[ $value['kl_dept'] ]['uraian'];
				if (array_key_exists($value['kl_unit'], $unit)) $hasil[$key]['nmunit'] = $unit[ $value['kl_unit'] ]['uraian'];
			}

			$data['depuni'] = $this->fc->array_index( array_merge_recursive($dept, $unit), 'kode');
			$data['kode']   = $depuni;
		} else {
			$data['depuni'] = array();
			$data['kode']   = array();
		}

		$data['revisi'] = $hasil;
		$data['start'] 	= $this->fc->idtgl( $start );
		$data['end']   	= $this->fc->idtgl( $end );
		
		return $data;
	}

	public function get_rev_id( $kddept, $kdunit ) {
		$thang = date('Y');
		$query = $this->dbrevisi("Select Right(rev_id,3) noid From revisi Where rev_tahun=$thang And kl_dept='$kddept' And kl_unit='$kdunit' Order By 1 Desc");
		$hasil = $query->row_array();

		$arr = array();
		if (!$hasil) $arr['rev_id'] = "$thang.$kddept.$kdunit.001";
		else $arr['rev_id'] = "$thang.$kddept.$kdunit.". sprintf('%03d', (int)$hasil['noid']+1);

		return $this->get_mitra( $arr, $kddept, $kdunit );
	}

	public function get_row( $rev_id ) {
		$query = $this->dbrevisi("Select rev_id, rev_tahun, kl_dept, kl_unit, kl_surat_no, kl_surat_tgl, kl_surat_hal, kl_pjb_jab, kl_pjb_nama, kl_pjb_nip, kl_email, kl_telp, doc_usulan_revisi, doc_matrix, doc_rka, doc_adk, doc_dukung_sepakat, doc_dukung_hal4, doc_dukung_lainnya, pus_catatan, kl_email From revisi Where rev_id='$rev_id'");
		$hasil = $query->row_array();

		if ($hasil) {
			$dbref = $this->load->database('ref'. $this->session->userdata('thang'), TRUE);
			$query = $dbref->query("Select nmdept From t_dept Where kddept='". $hasil['kl_dept'] ."'");
			$dept  = $query->row_array();
			$query = $dbref->query("Select nmunit From t_unit Where kddept='". $hasil['kl_dept'] ."' And kdunit='". $hasil['kl_unit'] ."'");
			$unit  = $query->row_array();

			$hasil['nmdept'] = $dept['nmdept'];
			$hasil['nmunit'] = $unit['nmunit'];
			$hasil = $this->get_mitra( $hasil, $hasil['kl_dept'], $hasil['kl_unit'] );
		}
		return  $hasil;
	}

	public function get_mitra( $arr, $kddept, $kdunit ) {
		$query = $this->dbrevisi("Select *, nmso From revisi_wenang a Left Join t_so b On a.kdso=b.kdso Where a.kddept like '%$kddept%'");
		$hasil = $query->row_array();
		if ($hasil) {
			$arr['rev_kdso'] = $hasil['kdso'];
			$arr['rev_nmso'] = $hasil['nmso'];
			$arr['rev_notlp']= 'ext: '. $hasil['intern'] .',  email: '. $hasil['email'];
			$arr['rev_nmcp'] = $hasil['cp1'] .' ['. $hasil['hp1'] .'],  '. $hasil['cp2'] .' ['. $hasil['hp2'] .']';
			if (strlen($arr['rev_nmcp']) < 10) $arr['rev_nmcp'] = '';
			if (strlen($arr['rev_notlp']) < 10) $arr['rev_notlp'] = '';
		} else {
			$arr['rev_kdso'] = ''; $arr['rev_nmso'] = ''; $arr['rev_nmcp'] = '';
		}

		$query = $this->dbrevisi("Select email, phone From revisi_mitra Where kddept='$kddept' And kdunit='$kdunit'");
		$hasil = $query->row_array();
		if ($hasil) { $arr['kl_email'] = $hasil['email']; $arr['kl_telp']  = $hasil['phone']; }
		else { $arr['kl_email'] = ''; $arr['kl_telp']  = ''; }
		return $arr;
	}

	
	public function get_row_id( $rev_id ) {
		$query = $this->dbrevisi->query("Select * From revisi Where rev_id='$rev_id'");
		$hasil = $query->row_array();
		return $hasil;
	}

	public function save_form2_proses( ) {
		$rev_id			= $_POST['rev_id'];
		$this->fc->logRevisi($rev_id, "Save_form2_proses - $rev_id");
		$ip	   			= $this->fc->get_client_ip();
		$t2_catatan		= str_replace(array("'", "\"", "&quot;"), "", htmlspecialchars($_POST['t2_catatan'] ) );
		$totim			= $_POST['radio-group'];
		$yesno			= $_POST['radio-group2'];
		//echo $totim.'-'.$yesno; exit();

		if ($totim=='1') 
		{ 

			if ($yesno=='1') {
				$t2='2'; $tahap='6'; $rev_status='9'; $next = ',t6_status=9';	
			} else
			if ($yesno=='2') {
				$t2='2'; $tahap='3'; $rev_status='9';  $next = ',t3_status=9';	
			} 
		}
		 else {$t2='0'; $tahap='2'; $rev_status='0'; $next = ''; }
		
		//if ($totim=='1') { $t2 = '2'; $tahap='3'; $next = ', t3_status=9'; } else {$t2='0'; $tahap='2'; $next = ''; } 


		//echo "t2=".$t2. "tahap".$tahap."next".$next; exit;
		$this->dbrevisi->query( "Update revisi Set t2_catatan=\"$t2_catatan\", t2_selesai_tgl=current_timestamp(),t2_selesai_ip='$ip',t2_status='$t2',
							rev_tahap=$tahap, rev_status=$rev_status $next	Where rev_id='$rev_id'" );

		if ($totim=='2') { $this->email_pengembalian_revisi( $rev_id );  } 

		return;
	}

	public function get_pdf_routing($rev_id){
		$query =$this->dbrevisi->query("Select a.*, b.nmso From revisi a Left Join t_so b On a.rev_kdso=b.kdso Where rev_id='$rev_id'");
		$hasil = $query->row_array();

		$dbref = $this->load->database('ref'. $this->session->userdata('thang'), TRUE);
		$query = $dbref->query("Select nmdept From t_dept Where kddept='". $hasil['kl_dept'] ."'");
		$dept  = $query->row_array();

		$query = $dbref->query("Select nmunit From t_unit Where kddept='". $hasil['kl_dept'] ."' And kdunit='". $hasil['kl_unit'] ."'");
		$unit  = $query->row_array();

		$hasil['nmdept'] = $dept['nmdept'];
		$hasil['nmunit'] = $unit['nmunit'];
		return $hasil;
	}

	public function email_pengembalian_revisi( $rev_id ) {
		//query_sender_email awal
	
		$query = $this->dbrevisi->query("Select *, kl_email email  From revisi Where rev_id = '$rev_id'");
	 	$hasilemail = $query->result_array();
	 	//print_r($hasilemail); exit;

		 $email = array(); $attach = array();
		 foreach ($hasilemail as $row) if ( $row['email'] ) $email[ $row['email'] ] = $row['kl_dept'];
		//print_r($email); exit;

		//ambil file lampiran
		$query = $this->dbrevisi->query("Select * From revisi Where rev_id='$rev_id'");
		$hasil = $query->row_array();
		$file  = array();

		//query ambil data email
		//$query = $this->dbrevisi->query("Select *, '' nmdept, '' nmunit, nmso, DATE(pus_tgl) From revisi a Left Join t_so b On a.rev_kdso=b.kdso Where rev_id='$rev_id'");
		$query = $this->dbrevisi->query("Select * From revisi Where rev_id='$rev_id'");
		$hasil = $query->row_array(); 

		$dbref = $this->load->database('ref'. $this->session->userdata('thang'), TRUE);
		$query = $dbref->query("Select nmdept From t_dept Where kddept='". $hasil['kl_dept'] ."'");
		$dept  = $query->row_array();

		$query = $dbref->query("Select nmunit From t_unit Where kddept='". $hasil['kl_dept'] ."' And kdunit='". $hasil['kl_unit'] ."'");
		$unit  = $query->row_array();

		$hasil['nmdept'] = $dept['nmdept'];
		$hasil['nmunit'] = $unit['nmunit'];
		//ending ambil data

		$query = $this->db->query("Select * From revisi_redaksi Where tahap = 13 ");
		$hasilbody = $query->row_array();
		$subject = $hasilbody['email_subject'];
		$body    = $hasilbody['email_narasi'];
		$body    = str_replace('[kl_dept]', ucwords(strtolower($hasil['nmdept'])), $body);
		$body    = str_replace('[kl_unit]', ucwords(strtolower($hasil['nmunit'])), $body);
		$body    = str_replace('[kl_pjb_jab]', $hasil['kl_pjb_jab'], $body);
		$body    = str_replace('[kl_surat_tgl]', $this->fc->idtgl($hasil['kl_surat_tgl'],'tglfull'),$body);
		$body    = str_replace('[kl_surat_no]', $hasil['kl_surat_no'], $body);
		$body    = str_replace('[pus_tgl]', $this->fc->idtgl($hasil['pus_tgl'],'tglfull'), $body);
		$body    = str_replace('[rev_id]', $hasil['rev_id'], $body);
		$body    = str_replace('[t2_catatan]', $hasil['t2_catatan'], $body);
		//$body    = str_replace('http://www.rkakldipaonline.kemenkeu.go.id','http://rkakldipa.anggaran.depkeu.go.id/index.php/tracking/'.$hasil['qrcode'], $body);
		
				$attach = array();
				
				$this->fc->send_mail( 'revisi', $email, $subject, $body, $attach ); 
		
	 	return $attach;
	}

	public function save_form3_proses( ) {
		$rev_id			= $_POST['rev_id'];
		$this->fc->logRevisi($rev_id, "save_form3_proses - $rev_id");

		$ip	   			= $this->fc->get_client_ip();
		$t3_catatan		= str_replace(array("'", "\"", "&quot;"), "", htmlspecialchars($_POST['t3_catatan'] ) );
		
		$no 			= $_POST['t3_und_no'];
		$file 			= $_POST['t3_und_file'];
		$tgl 			= $_POST['t3_und_tgl'];
		$tgltelaah		= $_POST['tglpenelaahan'];
		$pukul			= $_POST['pukul'];
		$penelaahan		= $tgltelaah." ".$pukul.":00";

		$tahap='4'; 
		$next = 't4_status=9';

			$this->dbrevisi->query( "Update revisi Set t3_catatan=\"$t3_catatan\", t3_selesai_tgl=current_timestamp(),t3_selesai_ip='$ip',t3_status=2,
			rev_tahap='$tahap', $next, t3_und_no='$no', t3_und_tgl='$tgl', t3_und_file='$file', t3_penelaahan_tgl='$penelaahan'
			Where rev_id='$rev_id'" );

		return;
	}

	public function email_und_revisi( $rev_id ) {
		//query_sender_email awal
		$query = $this->dbrevisi->query("Select *, kl_email email  From revisi Where rev_id = '$rev_id' ");
	 	$hasilemail = $query->result_array();

		$email = array();
		foreach ($hasilemail as $row) if ( $row['email'] ) $email[ $row['email'] ] = $row['kl_dept'];


		//ambil file lampiran
		$query = $this->dbrevisi->query("Select * From revisi Where rev_id='$rev_id'");
		$undfile = $query->row_array();
		$file  = $undfile['t3_und_file'];

		//query ambil data email
		//$query=$this->db->query("Select *, '' nmdept, '' nmunit, nmso, DATE(pus_tgl) From revisi a Left Join t_so b On a.rev_kdso=b.kdso Where rev_id='$rev_id'");
		$query = $this->dbrevisi->query("Select * From revisi Where rev_id='$rev_id'");
		$hasil = $query->row_array();

		$dbref = $this->load->database('ref'. $this->session->userdata('thang'), TRUE);
		$query = $dbref->query("Select nmdept From t_dept Where kddept='". $hasil['kl_dept'] ."'");
		$dept  = $query->row_array();

		$query = $dbref->query("Select nmunit From t_unit Where kddept='". $hasil['kl_dept'] ."' And kdunit='". $hasil['kl_unit'] ."'");
		$unit  = $query->row_array();

		$hasil['nmdept'] = $dept['nmdept'];
		$hasil['nmunit'] = $unit['nmunit'];
		//ending ambil data
		$query = $this->db->query("Select * From revisi_redaksi Where tahap = 20 ");
		$hasilbody = $query->row_array();
		$subject = $hasilbody['email_subject'];
		$body    = $hasilbody['email_narasi'];
		$body    = str_replace('[kl_dept]', ucwords(strtolower($hasil['nmdept'])), $body);
		$body    = str_replace('[kl_unit]', ucwords(strtolower($hasil['nmunit'])), $body);
		$body    = str_replace('[kl_pjb_jab]', $hasil['kl_pjb_jab'], $body);
		$body    = str_replace('[kl_surat_tgl]', $this->fc->idtgl($hasil['kl_surat_tgl'],'tglfull'),$body);
		$body    = str_replace('[kl_surat_no]', $hasil['kl_surat_no'], $body);
		$body    = str_replace('[pus_tgl]', $this->fc->idtgl($hasil['pus_tgl'],'tglfull'), $body);
		$body    = str_replace('[rev_id]', $hasil['rev_id'], $body);
		//$body    = str_replace('http://revisi_SatuDJA.kemenkeu.go.id','http://rkakldipa.anggaran.depkeu.go.id/index.php/tracking/revisi/'.$hasil['qrcode'], $body);

		// $subject = "Revisi - Undangan Penelaahan #$rev_id";
		// $body	 = "Telampir Undangan Penelaahan Terkait Revisi No. $rev_id";
		$attach  = array( $file => "http://10.242.142.52/files/revisi_SatuDJA/$rev_id/" .'/'. $file );
		//print_r($body); exit;
		$this->fc->send_mail( 'revisi', $email, $subject, $body, $attach);
	 	return $attach;
	}


	public function save_form4($rev_id){
		$rev_id= $_POST['rev_id'];
		$this->fc->logRevisi($rev_id, "save_form4 - $rev_id");
		
		$user_ip=$this->fc->get_client_ip();

		$query= $this->dbrevisi->query("Update revisi Set rev_tahap=4, t4_status=1, t4_proses_tgl=current_timestamp(), t4_proses_ip='$user_ip' where rev_id='$rev_id'");
		return;
	}

	public function save_form4_proses($rev_id) {
		$rev_id 				= $_POST['rev_id']; 
		$this->fc->logRevisi($rev_id, "save_form4_proses - $rev_id");
		
		$t4_catatan 			= str_replace(array("'", "\"", "&quot;"), "", htmlspecialchars($_POST['t4_catatan'] ) );
		$t4_ba_no 				= $_POST['t4_ba_no'];
		$t4_ba_tgl				= $_POST['t4_ba_tgl'];
		$t4_ba_file				= $_POST['t4_ba_file'];
		$t4_menteri_no			= $_POST['t4_menteri_no'];
		$t4_menteri_tgl			= $_POST['t4_menteri_tgl'];
		$t4_menteri_file		= $_POST['t4_menteri_file'];
		$rev_tahap				= $_POST['rev_tahap'];
		$t4_perbaikan_catatan 	= str_replace(array("'", "\"", "&quot;"), "", htmlspecialchars($_POST['t4_perbaikan_catatan'] ) );
		$t4_perbaikan_status	= $_POST['radio-group2'];

		if ($t4_perbaikan_status=='1') {
			$rev_tahap='6';
			$next_T =  't6_status=9';
		} else {
			$rev_tahap='5';
			$next_T = 't5_status=9';
		}
		
		$user_ip 				= $this->fc->get_client_ip();
		$query = $this->dbrevisi->query("Update revisi Set t4_catatan=\"$t4_catatan\", t4_ba_no='$t4_ba_no', t4_ba_tgl='$t4_ba_tgl', t4_menteri_no='$t4_menteri_no', t4_menteri_tgl='$t4_menteri_tgl', t4_perbaikan_catatan=\"$t4_perbaikan_catatan\", t4_selesai_ip='$user_ip',t4_status=2, t4_selesai_tgl=current_timestamp(), t4_perbaikan_status='$t4_perbaikan_status', t4_ba_file='$t4_ba_file', t4_menteri_file='$t4_menteri_file', rev_tahap='$rev_tahap', $next_T Where rev_id='$rev_id'");
		return;
	}

	public function save_form5($rev_id){
		$rev_id 		= $_POST['rev_id'];
		$this->fc->logRevisi($rev_id, "save_form5 - $rev_id");

		$user_ip		=$this->fc->get_client_ip();

		$query= $this->dbrevisi("Update revisi Set rev_tahap=5, t5_status=1, t5_proses_tgl=current_timestamp(), t5_proses_ip='$user_ip' where rev_id='$rev_id'");
		return;

	}

	public function save_form5_proses($rev_id){
		$rev_id 				= $_POST['rev_id'];
		$this->fc->logRevisi($rev_id, "save_form5_proses - $rev_id");

		$t5_hasil_catatan		= str_replace(array("'", "\"", "&quot;"), "", htmlspecialchars($_POST['t5_hasil_catatan'] ) );
		$t5_hasil_tgl			= $_POST['t5_hasil_tgl'];
		$t5_hasil_file			= $_POST['t5_hasil_file'];
		$t5_hasil_no 			= $_POST['t5_hasil_no'];
		$t5_hasil_status 		= $_POST['radio-group'];
		$user_ip 				= $this->fc->get_client_ip();

		$query= $this->dbrevisi->query("Update revisi Set rev_tahap=5, t5_status=2, t5_hasil_catatan=\"$t5_hasil_catatan\", t5_hasil_status='$t5_hasil_status', t5_hasil_no='$t5_hasil_no', t5_hasil_tgl='$t5_hasil_tgl', t5_proses_ip='$user_ip', t5_selesai_ip='$user_ip', t5_selesai_tgl=current_timestamp(), t5_hasil_file='$t5_hasil_file', rev_tahap=6, rev_status='3', t6_status=3, rev_upload_step = 1 Where rev_id='$rev_id'");
		return;
	}

	public function email_revisi_form4( $rev_id) {
		//query_sender_email awal
		$query = $this->dbrevisi->query("Select *, kl_email email  From revisi Where rev_id = '$rev_id'");
	 	$hasilemail = $query->result_array();

		$email = array();
		foreach ($hasilemail as $row) if ( $row['email'] ) $email[ $row['email'] ] = $row['kl_dept'];

		$query = $this->dbrevisi->query("Select * From revisi Where rev_id='$rev_id'");
		$hasil = $query->row_array();
		$file  = $hasil['t4_ba_file'];

		//query ambil data email
		//$query=$this->dbrevisi->query("Select *, '' nmdept, '' nmunit, nmso, DATE(pus_tgl) From revisi a Left Join t_so b On a.rev_kdso=b.kdso Where rev_id='$rev_id'");
		$query = $this->dbrevisi->query("Select * From revisi Where rev_id='$rev_id'");
		$hasil = $query->row_array();

		$dbref = $this->load->database('ref'. $this->session->userdata('thang'), TRUE);
		$query = $dbref->query("Select nmdept From t_dept Where kddept='". $hasil['kl_dept'] ."'");
		$dept  = $query->row_array();

		$query = $dbref->query("Select nmunit From t_unit Where kddept='". $hasil['kl_dept'] ."' And kdunit='". $hasil['kl_unit'] ."'");
		$unit  = $query->row_array();

		$hasil['nmdept'] = $dept['nmdept'];
		$hasil['nmunit'] = $unit['nmunit'];
		//ending ambil data

		$query = $this->db->query("Select * From revisi_redaksi Where tahap = 30 ");
		$hasilbody = $query->row_array();
		$subject = $hasilbody['email_subject'];
		$body    = $hasilbody['email_narasi'];
		$body    = str_replace('[kl_dept]', ucwords(strtolower($hasil['nmdept'])), $body);
		$body    = str_replace('[kl_unit]', ucwords(strtolower($hasil['nmunit'])), $body);
		$body    = str_replace('[kl_pjb_jab]', $hasil['kl_pjb_jab'], $body);
		$body    = str_replace('[kl_surat_tgl]', $this->fc->idtgl($hasil['kl_surat_tgl'],'tglfull'),$body);
		$body    = str_replace('[kl_surat_no]', $hasil['kl_surat_no'], $body);
		$body    = str_replace('[pus_tgl]', $this->fc->idtgl($hasil['pus_tgl'],'tglfull'), $body);
		$body    = str_replace('[rev_id]', $hasil['rev_id'], $body);
		$body    = str_replace('[t4_ba_tgl]',$this->fc->idtgl($hasil['t4_ba_tgl'],'tglfull'), $body);
		$body    = str_replace('[t4_catatan]', $hasil['t4_catatan'], $body);
		//$body    = str_replace('http://www.rkakldipaonline.kemenkeu.go.id','http://rkakldipa.anggaran.depkeu.go.id/index.php/tracking/revisi/'.$hasil['qrcode'], $body);

		$attach  = array( $file => "http://10.242.142.52/files/revisi_SatuDJA/$rev_id/" .'/'. $file );
		if ($file!='') { 
			$this->fc->send_mail( 'revisi', $email, $subject, $body, $attach); 
			} else {
					$attach = array();
					$this->fc->send_mail( 'revisi', $email, $subject, $body, $attach ); 
			}
		 return $attach;
	}

	public function email_revisi_form5($rev_id){
		//query_sender_email awal
		$query = $this->dbrevisi->query("Select *, kl_email email  From revisi Where rev_id = '$rev_id'");
	 	$hasilemail = $query->result_array();

		$email = array();
		foreach ($hasilemail as $row) if ( $row['email'] ) $email[ $row['email'] ] = $row['kl_dept'];

		$query = $this->dbrevisi->query("Select * From revisi Where rev_id='$rev_id'");
		$hasil = $query->row_array();
		$file  = $hasil['t5_hasil_file'];

		//query ambil data email
		//$query=$this->dbrevisi->query("Select *, '' nmdept, '' nmunit, nmso, DATE(pus_tgl) From revisi a Left Join t_so b On a.rev_kdso=b.kdso Where rev_id='$rev_id'");
		$query = $this->dbrevisi->query("Select * From revisi Where rev_id='$rev_id'");
		$hasil = $query->row_array();

		$dbref = $this->load->database('ref'. $this->session->userdata('thang'), TRUE);
		$query = $dbref->query("Select nmdept From t_dept Where kddept='". $hasil['kl_dept'] ."'");
		$dept  = $query->row_array();

		$query = $dbref->query("Select nmunit From t_unit Where kddept='". $hasil['kl_dept'] ."' And kdunit='". $hasil['kl_unit'] ."'");
		$unit  = $query->row_array();

		$hasil['nmdept'] = $dept['nmdept'];
		$hasil['nmunit'] = $unit['nmunit'];
		//ending ambil data

		$query = $this->db->query("Select * From revisi_redaksi Where tahap = 40 ");
		$hasilbody = $query->row_array();
		$subject = $hasilbody['email_subject'];
		$body    = $hasilbody['email_narasi'];
		$body    = str_replace('[kl_dept]', ucwords(strtolower($hasil['nmdept'])), $body);
		$body    = str_replace('[kl_unit]', ucwords(strtolower($hasil['nmunit'])), $body);
		$body    = str_replace('[kl_pjb_jab]', $hasil['kl_pjb_jab'], $body);
		$body    = str_replace('[kl_surat_tgl]', $this->fc->idtgl($hasil['kl_surat_tgl'],'tglfull'),$body);
		$body    = str_replace('[kl_surat_no]', $hasil['kl_surat_no'], $body);
		$body    = str_replace('[pus_tgl]', $this->fc->idtgl($hasil['pus_tgl'],'tglfull'), $body);
		$body    = str_replace('[rev_id]', $hasil['rev_id'], $body);
		$body    = str_replace('[t5_hasil_no]', $hasil['t5_hasil_no'], $body);
		$body    = str_replace('[t5_hasil_tgl]', $this->fc->idtgl($hasil['t5_hasil_tgl'],'tglfull'), $body);
		//$body    = str_replace('http://www.rkakldipaonline.kemenkeu.go.id','http://rkakldipa.anggaran.depkeu.go.id/index.php/tracking/revisi/'.$hasil['qrcode'], $body);

		$attach  = array( $file => "http://10.242.142.52/files/revisi_SatuDJA/$rev_id/" .'/'. $file );
		if ($file!='') { 
			$this->fc->send_mail( 'revisi', $email, $subject, $body, $attach); 
		} 
	 	return $attach;
	}


	public function save_form6_proses( ) {
		$rev_id			= $_POST['rev_id'];
		$this->fc->logRevisi($rev_id, "save_form6_proses - $rev_id");
		
		 
		$ip	   			= $this->fc->get_client_ip();
		$t6_catatan		= str_replace(array("'", "\"", "&quot;"), "", htmlspecialchars($_POST['t6_catatan'] ) );
		$totim			= $_POST['radio-group'];


		if ($totim=='1') {
		$this->dbrevisi->query( "Update revisi Set t6_catatan=\"$t6_catatan\", t6_selesai_tgl=current_timestamp(),t6_selesai_ip='$ip',t6_status=2,
						rev_tahap='7', rev_status='9', t7_status='9' Where rev_id='$rev_id'" );
		$this->email_lengkap( $rev_id );

		}

		if ($totim=='2') {
		$this->dbrevisi->query( "Update revisi Set t6_catatan=\"$t6_catatan\", t6_selesai_tgl=current_timestamp(),t6_selesai_ip='$ip',t6_status=0,
						 rev_tahap='7', rev_status=9, t7_status='9' Where rev_id='$rev_id'" );
		//$this->email_perbaikan_form6( $rev_id );
		}

		if ($totim=='3') {

		$no 			= $_POST['t6_surat_no'];
		$tgl 			= $_POST['t6_surat_tgl'];
		$file 			= $_POST['t6_surat_file'];

		$this->dbrevisi->query( "Update revisi Set t6_catatan=\"$t6_catatan\", t6_proses_tgl=current_timestamp(),t6_proses_ip='$ip',t6_status=3,rev_upload_step = 1,
						 rev_tahap='6', rev_status=3, t6_surat_no='$no', t6_surat_tgl='$tgl', t6_surat_file='$file'
						Where rev_id='$rev_id'" );
		}
		$this->email_perbaikan_form6( $rev_id );
		return;
	}

	

	public function email_perbaikan_form6(  $rev_id ){
		//query_sender_email awal
		$query = $this->dbrevisi->query("Select *, kl_email email  From revisi Where rev_id = '$rev_id'");
	 	$hasilemail = $query->result_array();

		$email = array();
		foreach ($hasilemail as $row) if ( $row['email'] ) $email[ $row['email'] ] = $row['kl_dept'];

		//ambil file lampiran
		$query = $this->dbrevisi->query("Select * From revisi Where rev_id='$rev_id'");
		$undfile = $query->row_array();
		$file  = $undfile['t6_surat_file'];

		//query ambil data email

		//$query=$this->dbrevisi("Select *, '' nmdept, '' nmunit, nmso, DATE(pus_tgl) From revisi a Left Join t_so b On a.rev_kdso=b.kdso Where rev_id='$rev_id'");
		$query = $this->dbrevisi->query("Select * From revisi Where rev_id='$rev_id'");
		$hasil = $query->row_array();

		$dbref = $this->load->database('ref'. $this->session->userdata('thang'), TRUE);
		$query = $dbref->query("Select nmdept From t_dept Where kddept='". $hasil['kl_dept'] ."'");
		$dept  = $query->row_array();

		$query = $dbref->query("Select nmunit From t_unit Where kddept='". $hasil['kl_dept'] ."' And kdunit='". $hasil['kl_unit'] ."'");
		$unit  = $query->row_array();

		$hasil['nmdept'] = $dept['nmdept'];
		$hasil['nmunit'] = $unit['nmunit'];
		//ending ambil data
		$query = $this->db->query("Select * From revisi_redaksi Where tahap = 40 ");
		$hasilbody = $query->row_array();
		$subject = $hasilbody['email_subject'];
		$body    = $hasilbody['email_narasi'];
		$body    = str_replace('[kl_dept]', ucwords(strtolower($hasil['nmdept'])), $body);
		$body    = str_replace('[kl_unit]', ucwords(strtolower($hasil['nmunit'])), $body);
		$body    = str_replace('[kl_pjb_jab]', $hasil['kl_pjb_jab'], $body);
		$body    = str_replace('[kl_surat_tgl]', $this->fc->idtgl($hasil['kl_surat_tgl'],'tglfull'),$body);
		$body    = str_replace('[kl_surat_no]', $hasil['kl_surat_no'], $body);
		$body    = str_replace('[pus_tgl]', $this->fc->idtgl($hasil['pus_tgl'],'tglfull'), $body);
		$body    = str_replace('[rev_id]', $hasil['rev_id'], $body);
		$body    = str_replace('[t5_hasil_no]', $hasil['t6_surat_no'], $body);
		$body    = str_replace('[t5_hasil_tgl]', $hasil['t6_surat_tgl'], $body);
		$body    = str_replace('[t6_catatan]', $hasil['t6_catatan'], $body);

		 

		// $subject = "Revisi - Undangan Penelaahan #$rev_id";
		// $body	 = "Telampir Undangan Penelaahan Terkait Revisi No. $rev_id";
		$attach  = array( $file => "http://10.242.142.52/files/revisi_SatuDJA/$rev_id/" .'/'. $file );

		if ($file!='') { 
			$this->fc->send_mail( 'revisi', $email, $subject, $body, $attach); 
			} else {
					$attach = array();
					$this->fc->send_mail( 'revisi', $email, $subject, $body, $attach ); 
			}
		 return $attach;
		//$this->fc->send_mail( 'revisi', $email, $subject, $body, $attach);
	 	//return $attach;
	}

	public function email_lengkap($rev_id){
		//query_sender_email awal
		$query = $this->dbrevisi->query("Select *, kl_email email  From revisi Where rev_id = '$rev_id'");
	 	$hasilemail = $query->result_array();
		$email = array(); $attach = array();
		foreach ($hasilemail as $row) if ( $row['email'] ) $email[ $row['email'] ] = $row['kl_dept'];

		//query ambil data email
		//$query=$this->dbrevisi->query("Select *, '' nmdept, '' nmunit, nmso, DATE(pus_tgl) From revisi a Left Join t_so b On a.rev_kdso=b.kdso Where rev_id='$rev_id'");
		$query = $this->dbrevisi->query("Select * From revisi Where rev_id='$rev_id'");
		$hasil = $query->row_array();

		$dbref = $this->load->database('ref'. $this->session->userdata('thang'), TRUE);
		$query = $dbref->query("Select nmdept From t_dept Where kddept='". $hasil['kl_dept'] ."'");
		$dept  = $query->row_array();

		$query = $dbref->query("Select nmunit From t_unit Where kddept='". $hasil['kl_dept'] ."' And kdunit='". $hasil['kl_unit'] ."'");
		$unit  = $query->row_array();

		$hasil['nmdept'] = $dept['nmdept'];
		$hasil['nmunit'] = $unit['nmunit'];
		//ending ambil data
		$query = $this->db->query("Select * From revisi_redaksi Where tahap = 50 ");
		$hasilbody = $query->row_array();
		$subject = $hasilbody['email_subject'];
		$body    = $hasilbody['email_narasi'];
		$body    = str_replace('[kl_dept]', ucwords(strtolower($hasil['nmdept'])), $body);
		$body    = str_replace('[kl_unit]', ucwords(strtolower($hasil['nmunit'])), $body);
		$body    = str_replace('[kl_surat_tgl]', $this->fc->idtgl($hasil['kl_surat_tgl'],'tglfull'),$body);
		$body    = str_replace('[kl_surat_no]', $hasil['kl_surat_no'], $body);
		$body    = str_replace('[kl_pjb_jab]', $hasil['kl_pjb_jab'], $body);
		$body    = str_replace('[pus_tgl]', $this->fc->idtgl($hasil['pus_tgl'],'tglfull'), $body);
		$body    = str_replace('[rev_id]', $hasil['rev_id'], $body);
		//$body    = str_replace('http://www.rkakldipaonline.kemenkeu.go.id','http://rkakldipa.anggaran.depkeu.go.id/index.php/tracking/revisi/'.$hasil['qrcode'], $body);
		$body    = str_replace('[t6_selesai_tgl]', $this->fc->idtgl($hasil['t6_selesai_tgl'],'tglfull'), $body);
		
		$attach  = array();
		$this->fc->send_mail( 'revisi', $email, $subject, $body, $attach);
	 	return $attach ;

	}

	public function email_revisi_form6($rev_id){
		//query_sender_email awal
		$query = $this->dbrevisi->query("Select *, kl_email email  From revisi Where rev_id = '$rev_id'");
	 	$hasilemail = $query->result_array();

		$email = array();
		foreach ($hasilemail as $row) if ( $row['email'] ) $email[ $row['email'] ] = $row['kl_dept'];

		$query = $this->dbrevisi->query("Select * From revisi Where rev_id='$rev_id'");
		$hasil = $query->row_array();
		$file  = $hasil['t6_surat_file'];

		//query ambil data email
		$query=$this->dbrevisi->query("Select *, '' nmdept, '' nmunit, nmso, DATE(pus_tgl) From revisi a Left Join t_so b On a.rev_kdso=b.kdso Where rev_id='$rev_id'");
		$hasil = $query->row_array();

		$dbref = $this->load->database('ref'. $this->session->userdata('thang'), TRUE);
		$query = $dbref->query("Select nmdept From t_dept Where kddept='". $hasil['kl_dept'] ."'");
		$dept  = $query->row_array();

		$query = $dbref->query("Select nmunit From t_unit Where kddept='". $hasil['kl_dept'] ."' And kdunit='". $hasil['kl_unit'] ."'");
		$unit  = $query->row_array();

		$hasil['nmdept'] = $dept['nmdept'];
		$hasil['nmunit'] = $unit['nmunit'];
		//ending ambil data

		$query = $this->db->query("Select * From revisi_redaksi Where tahap = 40 ");
		$hasilbody = $query->row_array();
		$subject = $hasilbody['email_subject'];
		$body    = $hasilbody['email_narasi'];
		$body    = str_replace('[kl_dept]', ucwords(strtolower($hasil['nmdept'])), $body);
		$body    = str_replace('[kl_unit]', ucwords(strtolower($hasil['nmunit'])), $body);
		$body    = str_replace('[kl_pjb_jab]', $hasil['kl_pjb_jab'], $body);
		$body    = str_replace('[kl_surat_tgl]', $this->fc->idtgl($hasil['kl_surat_tgl'],'tglfull'),$body);
		$body    = str_replace('[kl_surat_no]', $hasil['kl_surat_no'], $body);
		$body    = str_replace('[pus_tgl]', $this->fc->idtgl($hasil['pus_tgl'],'tglfull'), $body);
		$body    = str_replace('[rev_id]', $hasil['rev_id'], $body);
		$body    = str_replace('[t5_hasil_no]', $hasil['t6_surat_no'], $body);
		$body    = str_replace('[t6_catatan]', $hasil['t6_catatan'], $body);
		$body    = str_replace('[t5_hasil_tgl]', $this->fc->idtgl($hasil['t6_surat_tgl'],'tglfull'), $body);
		//$body    = str_replace('http://www.rkakldipaonline.kemenkeu.go.id','http://rkakldipa.anggaran.depkeu.go.id/index.php/tracking/revisi/'.$hasil['qrcode'], $body);

		$attach  = array( $file => "http://10.242.142.52/files/revisi_SatuDJA/$rev_id/" .'/'. $file );
		if ($file!='') { 
			$this->fc->send_mail( 'revisi', $email, $subject, $body, $attach); 
		} else {
				$this->fc->send_mail( 'revisi', $email, $subject, $body ); 
		}
	 	return $attach;
	}


	public function save_form7_proses( ) {
		$rev_id 	= $_POST['rev_id'];
		$this->fc->logRevisi($rev_id, "save_form7_proses - $rev_id");
		
		$ip	   = $this->fc->getUserIP();
		$t7_sp_no		= $_POST['t7_sp_no']; 
		$t7_sp_tgl		= $_POST['t7_sp_tgl']; 
		$t7_sp_file_u	= $_POST['t7_sp_file']; 
		$rev_ke			= $_POST['rev_ke']; 
		$t7_catatan		= str_replace(array("'", "\"", "&quot;"), "", htmlspecialchars($_POST['t7_catatan'] ) );
		$totim			= $_POST['radio-group'];

		//echo $totim; exit;
		
		if ($totim=='1') { $t7 = '2';  } else {$t7='0'; }
		if ($totim=='1') {$this->email_pengesahan_revisi( $rev_id );}
		if ($totim=='2') {$this->email_penolakan_revisi( $rev_id );}  

		
		$this->dbrevisi->query( "Update revisi set t7_status ='$t7', rev_tahap = 7, rev_status='2', t7_selesai_tgl=current_timestamp(), t7_selesai_ip='$ip', 
							t7_sp_tgl = '$t7_sp_tgl', t7_sp_no = '$t7_sp_no',t7_sp_file = '$t7_sp_file_u', t7_catatan = \"$t7_catatan\", rev_ke = '$rev_ke'  
							where rev_id='$rev_id' " );
		return;
	}

	public function email_pengesahan_revisi( $rev_id ) {
		//query_sender_email awal
		$query = $this->dbrevisi->query("Select *, kl_email email  From revisi Where rev_id = '$rev_id'");
	 	$hasilemail = $query->result_array();

		$email = array(); $attach = array();
		foreach ($hasilemail as $row) if ( $row['email'] ) $email[ $row['email'] ] = $row['kl_dept'];

		//ambil file lampiran
		$query = $this->dbrevisi->query("Select * From revisi Where rev_id='$rev_id'");
		$hasil = $query->row_array();
		$file  = $hasil['t7_sp_file'];

		//query ambil data email
		//$query=$this->dbrevisi->query("Select *, '' nmdept, '' nmunit, nmso, DATE(pus_tgl) From revisi a Left Join t_so b On a.rev_kdso=b.kdso Where rev_id='$rev_id'");
		$query = $this->dbrevisi->query("Select * From revisi Where rev_id='$rev_id'");
		$hasil = $query->row_array();
		$hasil = $query->row_array();

		$dbref = $this->load->database('ref'. $this->session->userdata('thang'), TRUE);
		$query = $dbref->query("Select nmdept From t_dept Where kddept='". $hasil['kl_dept'] ."'");
		$dept  = $query->row_array();

		$query = $dbref->query("Select nmunit From t_unit Where kddept='". $hasil['kl_dept'] ."' And kdunit='". $hasil['kl_unit'] ."'");
		$unit  = $query->row_array();

		$hasil['nmdept'] = $dept['nmdept'];
		$hasil['nmunit'] = $unit['nmunit'];
		//ending ambil data

		$query = $this->db->query("Select * From revisi_redaksi Where tahap = 71 ");
		$hasilbody = $query->row_array();
		$subject = $hasilbody['email_subject'];
		$body    = $hasilbody['email_narasi'];
		$body    = str_replace('[kl_dept]', ucwords(strtolower($hasil['nmdept'])), $body);
		$body    = str_replace('[kl_unit]', ucwords(strtolower($hasil['nmunit'])), $body);
		$body    = str_replace('[kl_pjb_jab]', $hasil['kl_pjb_jab'], $body);
		$body    = str_replace('[kl_surat_tgl]', $this->fc->idtgl($hasil['kl_surat_tgl'],'tglfull'),$body);
		$body    = str_replace('[kl_surat_no]', $hasil['kl_surat_no'], $body);
		$body    = str_replace('[pus_tgl]', $this->fc->idtgl($hasil['pus_tgl'],'tglfull'), $body);
		$body    = str_replace('[rev_id]', $hasil['rev_id'], $body);
		$body    = str_replace('[t7_sp_no]', $hasil['t7_sp_no'], $body);
		$body    = str_replace('[t7_sp_tgl]', $this->fc->idtgl($hasil['t7_sp_tgl'],'tglfull'), $body);
		//$body    = str_replace('http://www.rkakldipaonline.kemenkeu.go.id','http://rkakldipa.anggaran.depkeu.go.id/index.php/tracking/'.$hasil['qrcode'], $body);

		$attach  = array( $file => "http://10.242.142.52/files/revisi_SatuDJA/$rev_id/" .'/'. $file );
		if ($file!='') { 
			$this->fc->send_mail( 'revisi', $email, $subject, $body, $attach); 
		} else {
				$attach = array();
				$this->fc->send_mail( 'revisi', $email, $subject, $body, $attach ); 
		}
	 	return $attach;
	}

	public function email_penolakan_revisi( $rev_id ) {
		//query_sender_email awal
		$query = $this->dbrevisi->query("Select *, kl_email email  From revisi Where rev_id = '$rev_id'");
	 	$hasilemail = $query->result_array();

		$email = array(); $attach = array();
		foreach ($hasilemail as $row) if ( $row['email'] ) $email[ $row['email'] ] = $row['kl_dept'];

		//ambil file lampiran
		$query = $this->dbrevisi->query("Select * From revisi Where rev_id='$rev_id'");
		$hasil = $query->row_array();
		$file  = $hasil['t7_sp_file'];

		//query ambil data email
		//$query=$this->dbrevisi->query("Select *, '' nmdept, '' nmunit, nmso, DATE(pus_tgl) From revisi a Left Join t_so b On a.rev_kdso=b.kdso Where rev_id='$rev_id'");
		$query = $this->dbrevisi->query("Select * From revisi Where rev_id='$rev_id'");
		$hasil = $query->row_array();

		$dbref = $this->load->database('ref'. $this->session->userdata('thang'), TRUE);
		$query = $dbref->query("Select nmdept From t_dept Where kddept='". $hasil['kl_dept'] ."'");
		$dept  = $query->row_array();

		$query = $dbref->query("Select nmunit From t_unit Where kddept='". $hasil['kl_dept'] ."' And kdunit='". $hasil['kl_unit'] ."'");
		$unit  = $query->row_array();

		$hasil['nmdept'] = $dept['nmdept'];
		$hasil['nmunit'] = $unit['nmunit'];
		//ending ambil data

		$query = $this->db->query("Select * From revisi_redaksi Where tahap = 79 ");
		$hasilbody = $query->row_array();
		$subject = $hasilbody['email_subject'];
		$body    = $hasilbody['email_narasi'];
		$body    = str_replace('[kl_dept]', ucwords(strtolower($hasil['nmdept'])), $body);
		$body    = str_replace('[kl_unit]', ucwords(strtolower($hasil['nmunit'])), $body);
		$body    = str_replace('[kl_pjb_jab]', $hasil['kl_pjb_jab'], $body);
		$body    = str_replace('[kl_surat_tgl]', $this->fc->idtgl($hasil['kl_surat_tgl'],'tglfull'),$body);
		$body    = str_replace('[kl_surat_no]', $hasil['kl_surat_no'], $body);
		$body    = str_replace('[pus_tgl]', $this->fc->idtgl($hasil['pus_tgl'],'tglfull'), $body);
		$body    = str_replace('[rev_id]', $hasil['rev_id'], $body);
		$body    = str_replace('[t7_sp_no]', $hasil['t7_sp_no'], $body);
		$body    = str_replace('[t7_sp_tgl]', $hasil['t7_sp_tgl'], $body);
		//$body    = str_replace('http://www.rkakldipaonline.kemenkeu.go.id','http://rkakldipa.anggaran.depkeu.go.id/index.php/tracking/revisi/'.$hasil['qrcode'], $body);


		// $subject = "Revisi - Undangan Penelaahan #$rev_id";
		// $body	 = "Telampir Undangan Penelaahan Terkait Revisi No. $rev_id";
		$attach  = array( $file => "http://10.242.142.52/files/revisi_SatuDJA/$rev_id/" .'/'. $file );
		if ($file!='') { 
			$this->fc->send_mail( 'revisi', $email, $subject, $body, $attach); 
		} else {
				$attach = array();
				$this->fc->send_mail( 'revisi', $email, $subject, $body, $attach ); 
		}
	 	return $attach;
	}

	function mundur_status($lvl,$rev_id){
		$this->fc->logRevisi($rev_id, "mundur_status - $rev_id");

		$sql = " t6_selesai_tgl=null, t6_selesai_ip=null, t6_check_status=null, t6_surat_no=null, t6_surat_tgl=null, t6_surat_file=null, t6_catatan=null, t7_status=null, t7_proses_tgl=null, t7_proses_ip=null, t7_selesai_tgl=null, t7_selesai_ip=null, t7_nd_no=null, t7_nd_tgl=null, t7_nd_file=null, t7_nd_catatan=null, t7_sp_tgl=null, t7_sp_file=null, t7_penetapan_tgl=null, t7_catatan=null  WHERE rev_id='$rev_id'";
		// echo $sql;

		if($lvl == '2') $this->dbrevisi->query("UPDATE revisi SET rev_tahap='2', rev_status='9', t2_status='1', t2_selesai_tgl=null, t2_selesai_ip =null, t2_tolak_no=null,t2_tolak_tgl=null, t2_tolak_hal=null, t2_tolak_file=null, t2_catatan=null, t3_status=null,t3_proses_tgl=null, t3_proses_ip=null, t3_selesai_tgl=null, t3_selesai_ip=null, t3_und_no=null, t3_und_tgl=null, t3_und_file=null, t3_catatan=null, t3_penelaahan_tgl=null, t4_status=null,t4_proses_tgl=null,t4_proses_ip=null, t4_selesai_tgl=null, t4_selesai_ip=null, t4_catatan=null, t4_ba_no=null, t4_ba_tgl=null, t4_ba_file=null, t4_menteri_no=null, t4_menteri_tgl=null, t4_menteri_file=null, t4_perbaikan_status=null, t4_perbaikan_catatan=null, t5_status=null,t5_proses_tgl=null, t5_proses_ip=null, t5_selesai_tgl=null, t5_selesai_ip=null, t5_hasil_status=null, t5_hasil_no=null, t5_hasil_tgl=null, t5_hasil_file=null, t5_hasil_catatan=null, t6_status=null, t6_proses_tgl=null, t6_proses_ip=null,$sql ");

		if($lvl == '3') $this->dbrevisi->query("UPDATE revisi SET rev_tahap='3',rev_status='9', t3_status='1', t3_selesai_tgl=null, t3_selesai_ip=null, t3_und_no=null, t3_und_tgl=null, t3_und_file=null, t3_catatan=null, t3_penelaahan_tgl=null, t4_status=null,t4_proses_tgl=null,t4_proses_ip=null, t4_selesai_tgl=null, t4_selesai_ip=null, t4_catatan=null, t4_ba_no=null, t4_ba_tgl=null, t4_ba_file=null, t4_menteri_no=null, t4_menteri_tgl=null, t4_menteri_file=null, t4_perbaikan_status=null, t4_perbaikan_catatan=null, t5_status=null,t5_proses_tgl=null, t5_proses_ip=null, t5_selesai_tgl=null, t5_selesai_ip=null, t5_hasil_status=null, t5_hasil_no=null, t5_hasil_tgl=null, t5_hasil_file=null, t5_hasil_catatan=null, t6_status=null, t6_proses_tgl=null, t6_proses_ip=null, $sql ");

		if($lvl == '4') $this->dbrevisi->query("UPDATE revisi SET rev_tahap='4', rev_status='9', t4_status='1', t4_selesai_tgl=null, t4_selesai_ip=null, t4_catatan=null, t4_ba_no=null, t4_ba_tgl=null, t4_ba_file=null, t4_menteri_no=null, t4_menteri_tgl=null, t4_menteri_file=null, t4_perbaikan_status=null, t4_perbaikan_catatan=null, t5_status=null,t5_proses_tgl=null, t5_proses_ip=null, t5_selesai_tgl=null, t5_selesai_ip=null, t5_hasil_status=null, t5_hasil_no=null, t5_hasil_tgl=null, t5_hasil_file=null, t5_hasil_catatan=null, t6_status=null, t6_proses_tgl=null, t6_proses_ip=null, $sql");

		if($lvl == '5') $this->dbrevisi->query("UPDATE revisi SET rev_tahap='5', rev_status='9', t5_status='1', t5_selesai_tgl=null, t5_selesai_ip=null, t5_hasil_status=null, t5_hasil_no=null, t5_hasil_tgl=null, t5_hasil_file=null, t5_hasil_catatan=null, t6_status=null, t6_proses_tgl=null, t6_proses_ip=null, $sql ");

		if($lvl == '6') $this->dbrevisi->query("UPDATE revisi SET rev_tahap='6', rev_status='9', t6_status='1', $sql ");

	}





}
