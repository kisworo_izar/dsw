<?php


class Login_model extends CI_Model {

    var $details;

    function validate_user( $iduser, $password, $thang ) {
        $query = $this->db->query("SELECT * FROM t_user WHERE iduser='$iduser' AND password=md5('$password')");
		$login = $query->result();
        if ( is_array($login) && count($login) == 1 ) {
            $this->details = $login[0];
            $this->set_session($thang);
            $this->fc->log('Login');
            return true;
        }
        return false;

    }

    function set_session($thang) {
        $arlevel  = array('level','Admin','User','Non User','Manager');
        $arwenang = array('Admin','Read Write','Read','None','Manager');
        $ref = $this->load->database("ref$thang", TRUE);
        $qry = $ref->query("SELECT kddept FROM t_dept ORDER BY 1");
        $dep = "kddept ". $this->fc->InVar($this->fc->array_index($qry->result_array(), 'kddept'), 'kddept');

        $lok = '00';
        if (strpos('id:611,612', $this->details->idusergroup)) {
            $qry = $this->db->query("SELECT kdlokasi FROM revisi_wenang WHERE kdso='". $this->details->kdso ."'");
            $arr = $qry->row_array();
            $lok = $arr['kdlokasi'];
        }

        $this->session->set_userdata( array(
                'iduser'      => $this->details->iduser,
                'nmuser'      => $this->details->nmuser,
                'idusergroup' => $this->details->idusergroup,
                'nohp'        => $this->details->nohp,
                'jabatan'     => $this->details->jabatan,
                'kdso'        => $this->details->kdso,
                'kdlokasi'    => $lok,
                'nip'         => $this->details->nip,
                'nmalias'     => $this->details->nmalias,
                'kodelink'    => '1',
                'cari'        => '',
                'thang'       => $thang,
                'whrdept'     => $dep,
                'isLoggedIn'  => true
            )
        );
    }
                // $this->details->kdusergroup,
}
