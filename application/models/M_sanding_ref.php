<?php
class M_sanding_ref extends CI_Model {

	public function get_output1( $syn=null ) {
		// Setting Database Referensi
		$dbref = $this->load->database('ref', TRUE);
		$whr   = "where kdgiat in('2021','2052','2052','2162')";
		$whr   = "";

		// Query GIAT dari DB-REF
		$query = $dbref->query("select concat(kddept,'_',kdunit) depuni, kdgiat kode, kdgiat rka_kode, nmgiat rka_uraian, '' rka_sat, '' rka_sum, '' cw_kode, '' cw_uraian, '' cw_sat, '' cw_sum, '1' lvl, ' ' ruh from t_giat $whr");
		$giat  = $this->fc->ToArr( $query->result_array(), 'kode');

		// Query OUTPUT dari DB-REF dan Join dg GIAT
		$query = $dbref->query("Select '' depuni, concat(kdgiat,'.',kdoutput) kode, concat(kdgiat,'.',kdoutput) rka_kode, nmoutput rka_uraian, sat rka_sat, kdsum rka_sum,'' cw_kode, '' cw_uraian, '' cw_sat, '' cw_sum,'2' lvl, 'Syn' ruh From t_output $whr");
		$rkaout= $this->fc->ToArr( $query->result_array(), 'kode');

		foreach ($rkaout as $key=>$value) {
			if (array_key_exists( substr($key,0,4), $giat)) $rkaout[$key]['depuni'] = $giat[ substr($key,0,4) ]['depuni'];
		}

		// Merge GIAT dan OUTPUT menjadi GIATOUT
		$giatout = $this->fc->ToArr(array_merge_recursive($giat, $rkaout), 'kode');


		// Setting Database Oracle CUSTOM WEB
		$dbcw17 = $this->load->database('ora_cw2017', TRUE);

		// Query OUTPUT dari DB-REF dan Join dg GIAT
		$query  = $dbcw17->query("Select '' depuni, kdgiat||'.'||kdoutput kode, kdgiat||'.'||kdoutput cw_kode, nmoutput cw_uraian, sat cw_sat, kdsum cw_sum,'2' lvl From span_adk_t_output $whr Order By kdgiat");
		$cwout  = $this->fc->ToArr( $query->result_array(), 'KODE');

		// Join ke Array GIATOUT
		foreach ($cwout as $key=>$value) {
			if (array_key_exists($value['KODE'],$giatout)) {
				$giatout[$value['KODE']]['cw_kode']=$value['CW_KODE'];
				$giatout[$value['KODE']]['cw_uraian']=$value['CW_URAIAN'];
				$giatout[$value['KODE']]['cw_sat']=$value['CW_SAT'];
				$giatout[$value['KODE']]['cw_sum']=$value['CW_SUM'];
			} else {
				$giatout[$value['KODE']] = array('depuni'=>'', 'kode'=>$value['KODE'], 'rka_kode'=>'', 'rka_uraian'=>'', 'rka_sat'=>'', 'rka_sum'=>'','cw_kode'=>$value['CW_KODE'], 'cw_uraian'=>$value['CW_URAIAN'], 'cw_sat'=>$value['CW_SAT'], 'cw_sum'=>$value['CW_SUM'], 'lvl'=>'2');
			}
		}

		// Pilah Status ke INSERT, UPDATE dan DELETE
		foreach ($giatout as $key=>$value ) {
			if ($value['lvl']==2) {
				if ($value['cw_uraian']=='') {
					$giatout[$value['kode']]['ruh']='Ins';
				} elseif ($value['rka_uraian']!==$value['cw_uraian'] or $value['rka_sat']!==$value['cw_sat']) {
					$giatout[$value['kode']]['ruh']='Upd';
				} elseif ($value['rka_uraian']=='') {
					$giatout[$value['kode']]['ruh']='Del';
				}
			}
		}

		// if ($syn==null) {
		// 	$arr = array();
		// 	foreach ($giatout as $key=>$value) if ($value['lvl']=='2' and $value['ruh']!='Syn') array_push($arr, $value);
		//
		// 	$arr = $this->fc->array_index($arr, 'kode');
		// 	foreach ($arr as $key=>$value) {
		// 		$kode = substr($value['kode'],0,4);
		// 		if ( !array_key_exists($kode, $arr) and array_key_exists($kode, $giat) ) { $arr[$kode] = $giat[$kode]; $depuni = $giat[$kode]['depuni']; }
		// 		if ( $value['depuni']=='' ) $arr[$key]['depuni'] = $depuni;
		// 	}
		// 	$data['giatout'] = $this->fc->array_index($arr, 'kode');
		// } else {
		// 	$data['giatout'] = $this->fc->array_index($giatout, 'kode');
		// }

			$data['giatout'] = $this->fc->array_index($giatout, 'kode');

		// Query UNIT dan DEPT sesuai Data yg ada
		// $query = $dbref->query("Select kddept, concat(kddept,'.',kdunit) kode, nmunit uraian, '2' level From t_unit Where concat(kddept,'_',kdunit) ". $this->fc->InVar($data['giatout'], 'depuni'));
		// $unit  = $this->fc->ToArr( $query->result_array(), 'kode');
		// $query = $dbref->query("Select kddept kode, nmdept uraian, '1' level From t_dept Where kddept ". $this->fc->InVar($unit, 'kddept'));
		// $dept  = $this->fc->ToArr( $query->result_array(), 'kode');
		//
		// $data['depuni'] = $this->fc->array_index(array_merge_recursive($dept, $unit), 'kode');
		return $giatout;
	}


	public function get_giat( $syn=null ) {
		$dbref = $this->load->database('ref', TRUE);
		$whr   = "";

		$query = $dbref->query("select kdgiat kode, kdgiat rka_kode, nmgiat uraian, nmgiat rka_uraian, kddept rka_kddept, kdunit rka_kdunit, kdes2 rka_kdes2, kdprogram rka_kdprogram, kdfungsi rka_kdfungsi, kdsfung rka_kdsfung, '' cw_uraian, '' cw_kddept, '' cw_kdunit, '' cw_kdes2, '' cw_kdprogram, '' cw_kdfungsi, '' cw_kdsfung, '0' ruh from t_giat $whr Order By kdgiat");
		$rgiat  = $this->fc->ToArr( $query->result_array(), 'kode');

		$dbcw17 = $this->load->database('ora_cw2017', TRUE);
		$query  = $dbcw17->query("select kdgiat kode, kdgiat cw_kode, nmgiat cw_uraian, kddept cw_kddept, kdunit cw_kdunit, kdes2 cw_kdes2, kdprogram cw_kdprogram, kdfungsi cw_kdfungsi, kdsfung cw_kdsfung, '0' ruh From span_adk_t_giat Order By kdgiat");
		$cgiat  = $this->fc->ToArr( $query->result_array(), 'KODE');

		foreach ($cgiat as $key=>$value) {
			if (array_key_exists($value['KODE'],$rgiat)) {
				$rgiat[$value['KODE']]['cw_uraian']=$value['CW_URAIAN'];
				$rgiat[$value['KODE']]['cw_kddept']=$value['CW_KDDEPT'];
				$rgiat[$value['KODE']]['cw_kdunit']=$value['CW_KDUNIT'];
				$rgiat[$value['KODE']]['cw_kdes2']=$value['CW_KDES2'];
				$rgiat[$value['KODE']]['cw_kdprogram']=$value['CW_KDPROGRAM'];
				$rgiat[$value['KODE']]['cw_kdfungsi']=$value['CW_KDFUNGSI'];
				$rgiat[$value['KODE']]['cw_kdsfung']=$value['CW_KDSFUNG'];
			} else {
				$rgiat[$value['KODE']] = array('kode'=>$value['KODE'], 'rka_kode'=>'', 'uraian'=>$value['CW_URAIAN'], 'rka_kode'=>'', 'rka_uraian'=>'', 'rka_kddept'=>'', 'rka_kdunit'=>'', 'rka_kdes2'=>'', 'rka_kdprogram'=>'', 'rka_kdfungsi'=>'', 'rka_kdsfung'=>'', 'cw_uraian'=>$value['CW_URAIAN'], 'cw_kddept'=>$value['CW_KDDEPT'], 'cw_kdunit'=>$value['CW_KDUNIT'], 'cw_kdes2'=>$value['CW_KDES2'], 'cw_kdprogram'=>$value['CW_KDPROGRAM'], 'cw_kdfungsi'=>$value['CW_KDFUNGSI'], 'cw_kdsfung'=>$value['CW_KDSFUNG'], 'ruh'=>'',);
			}
		}

		foreach ($rgiat as $key=>$value ) {
			if ($value['rka_uraian']=='' or $value['cw_uraian']=='') {
				$rgiat[$value['kode']]['ruh']='D';
			} elseif ($value['rka_uraian']!==$value['cw_uraian']) {
				$rgiat[$value['kode']]['ruh']='U';
			} elseif ($value['rka_kdes2']!==$value['cw_kdes2']) {
				$rgiat[$value['kode']]['ruh']='K';
			} else {
				$rgiat[$value['kode']]['ruh']='S';
			}
		}
		$data['rgiat'] = $this->fc->array_index($rgiat, 'kode');
		return $data;
	}

	public function get_satker( $syn=null ) {
		$dbref = $this->load->database('ref', TRUE);
		$whr   = "";

		$query = $dbref->query("select kdsatker kode, kdsatker rka_kode, nmsatker uraian, nmsatker rka_uraian, kddept rka_kddept, kdunit rka_kdunit, kdlokasi rka_kdlokasi, kdkabkota rka_kdkabkota, kddekon rka_kddekon, kdpusda rka_kdpusda, kdkppn rka_kdkppn, '' cw_uraian, '' cw_kddept, '' cw_kdunit, '' cw_kdlokasi, '' cw_kdkabkota, '' cw_kddekon, '' cw_kdpusda, '' cw_kdkppn, '0' ruh from t_satker $whr");
		$rsatker  = $this->fc->ToArr( $query->result_array(), 'kode');

		$dbcw17 = $this->load->database('ora_cw2017', TRUE);
		$query  = $dbcw17->query("select kdsatker kode, kdsatker cw_kode, nmsatker cw_uraian, kddept cw_kddept, kdunit cw_kdunit, kdlokasi cw_kdlokasi, kdkabkota cw_kdkabkota, kddekon cw_kddekon, kdpusda cw_kdpusda, kdkppn cw_kdkppn, '0' ruh From span_adk_t_satker Order By kdsatker");
		$csatker= $this->fc->ToArr( $query->result_array(), 'KODE');

		foreach ($csatker as $key=>$value) {
			if (array_key_exists($value['KODE'],$rsatker)) {
				$rsatker[$value['KODE']]['cw_uraian']=$value['CW_URAIAN'];
				$rsatker[$value['KODE']]['cw_kddept']=$value['CW_KDDEPT'];
				$rsatker[$value['KODE']]['cw_kdunit']=$value['CW_KDUNIT'];
				$rsatker[$value['KODE']]['cw_kdlokasi']=$value['CW_KDLOKASI'];
				$rsatker[$value['KODE']]['cw_kdkabkota']=$value['CW_KDKABKOTA'];
				$rsatker[$value['KODE']]['cw_kddekon']=$value['CW_KDDEKON'];
				$rsatker[$value['KODE']]['cw_kdpusda']=$value['CW_KDPUSDA'];
				$rsatker[$value['KODE']]['cw_kdkppn']=$value['CW_KDKPPN'];
			} else {
				$rsatker[$value['KODE']] = array('kode'=>$value['KODE'], 'rka_kode'=>'', 'uraian'=>$value['CW_URAIAN'], 'rka_kode'=>'', 'rka_uraian'=>'', 'rka_kddept'=>'', 'rka_kdunit'=>'', 'rka_kdlokasi'=>'', 'rka_kdkabkota'=>'', 'rka_kddekon'=>'', 'rka_kdpusda'=>'', 'rka_kdkppn'=>'', 'cw_uraian'=>$value['CW_URAIAN'], 'cw_kddept'=>$value['CW_KDDEPT'], 'cw_kdunit'=>$value['CW_KDUNIT'], 'cw_kdlokasi'=>$value['CW_KDLOKASI'], 'cw_kdkabkota'=>$value['CW_KDKABKOTA'], 'cw_kddekon'=>$value['CW_KDDEKON'], 'cw_kdpusda'=>$value['CW_KDPUSDA'], 'cw_kdkppn'=>$value['CW_KDKPPN'], 'ruh'=>'',);
			}
		}

		foreach ($rsatker as $key=>$value ) {
			if ($value['rka_uraian']=='' or $value['cw_uraian']=='') {
				$rsatker[$value['kode']]['ruh']='D';
			} elseif ($value['rka_uraian']!==$value['cw_uraian']) {
				$rsatker[$value['kode']]['ruh']='U';
			} elseif ($value['rka_kddept'].$value['rka_kdunit'].$value['rka_kdlokasi'].$value['rka_kdkabkota'].$value['rka_kddekon'].$value['rka_kdpusda'].$value['rka_kdkppn']!==$value['cw_kddept'].$value['cw_kdunit'].$value['cw_kdlokasi'].$value['cw_kdkabkota'].$value['cw_kddekon'].$value['cw_kdpusda'].$value['cw_kdkppn']) {
				$rsatker[$value['kode']]['ruh']='K';
			} else {
				$rsatker[$value['kode']]['ruh']='S';
			}
		}
		$data['rsatker'] = $this->fc->array_index($rsatker, 'kode');
		return $data;
	}


	public function get_output( $syn=null ) {
		$dbref = $this->load->database('ref', TRUE);
		$whr   = "";

		// $query = $dbref->query("select concat(kdgiat,'.',kdoutput) kode, concat(kdgiat,'.',kdoutput) rka_kdoutput, nmoutput uraian, nmoutput rka_uraian, sat rka_sat, kdsum rka_kdsum, '' cw_kdoutput, '' cw_uraian, '' cw_sat, '' cw_kdsum, '0' ruh from t_output $whr Order By kdgiat, kdoutput");
		$query = $dbref->query("select concat(a.kdgiat,'.',a.kdoutput) kode, concat(a.kdgiat,'.',a.kdoutput) rka_kdoutput, concat(b.kddept,'.',b.kdunit) kl, a.nmoutput uraian, a.nmoutput rka_uraian, a.sat rka_sat, a.kdsum rka_kdsum, '' cw_kdoutput, '' cw_uraian, '' cw_sat, '' cw_kdsum, '0' ruh from t_output a left join t_giat b on a.kdgiat=b.kdgiat $whr Order By a.kdgiat, a.kdoutput");

		$routput  = $this->fc->ToArr( $query->result_array(), 'kode');

		$dbcw17 = $this->load->database('ora_cw2017', TRUE);
		$query  = $dbcw17->query("select kdgiat||'.'||kdoutput kode, kdgiat||'.'||kdoutput cw_kdoutput, nmoutput cw_uraian, sat cw_sat, kdsum cw_kdsum,'0' ruh From span_adk_t_output Order By kdgiat, kdoutput");
		$coutput  = $this->fc->ToArr( $query->result_array(), 'KODE');

		foreach ($coutput as $key=>$value) {
			if (array_key_exists($value['KODE'],$routput)) {
				$routput[$value['KODE']]['cw_kdoutput']=$value['CW_KDOUTPUT'];
				$routput[$value['KODE']]['cw_uraian']=$value['CW_URAIAN'];
				$routput[$value['KODE']]['cw_sat']=$value['CW_SAT'];
				$routput[$value['KODE']]['cw_kdsum']=$value['CW_KDSUM'];
			} else {
				$routput[$value['KODE']] = array('kode'=>$value['KODE'], 'kl'=>'','rka_kdoutput'=>'', 'uraian'=>$value['CW_URAIAN'], 'rka_kode'=>'', 'rka_uraian'=>'', 'rka_sat'=>'', 'rka_kdsum'=>'', 'cw_kdoutput'=>$value['CW_KDOUTPUT'], 'cw_uraian'=>$value['CW_URAIAN'], 'cw_sat'=>$value['CW_SAT'], 'cw_kdsum'=>$value['CW_KDSUM'], 'ruh'=>'0');
			}
		}

		foreach ($routput as $key=>$value ) {
			if ($value['rka_kdoutput']!=='' and $value['cw_kdoutput']=='') {
				$routput[$value['kode']]['ruh']='D';
			} elseif ($value['rka_kdoutput']==$value['cw_kdoutput'] and $value['rka_uraian']!==$value['cw_uraian']) {
				$routput[$value['kode']]['ruh']='U';
			} elseif ($value['rka_sat']!==$value['cw_sat']) {
				$routput[$value['kode']]['ruh']='K';
			} else {
				$routput[$value['kode']]['ruh']='S';
			}
		}

		$data['routput'] = $this->fc->array_index($routput, 'kode');
		return $data;
		// return $routput;
	}

	public function get_depuni() {
		$dbref = $this->load->database('ref', TRUE);
		$query = $dbref->query("Select kddept kode, nmdept uraian, '1' level From t_dept");
		$dept  = $this->fc->ToArr( $query->result_array(), 'kode');

		$query = $dbref->query("Select concat(kddept,'.',kdunit) kode, nmunit uraian, '2' level From t_unit");
		$unit  = $this->fc->ToArr( $query->result_array(), 'kode');
		return $this->fc->array_index( array_merge_recursive($dept, $unit), 'kode');
	}

}
