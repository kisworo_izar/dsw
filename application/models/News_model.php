<?php
class News_model extends CI_Model {
	public function get_data() {
		$whr  = "";
		$cari = $this->session->userdata('cari');

		if ($cari) {
			$whr = " where " ;
			$arr = explode(' ', $cari);
			for ($i=0; $i<count($arr); $i++) {
				$str  = $arr[$i];
				$whr .= "notif1 like '%$str%'";
				if ($i<count($arr)-1) {
					$whr .= " or ";
				}
			}
		}

		// cari query data dari d_paket
		$query = $this->db->query("select *  from d_news $whr order by idnews desc ");
		$hasil = $this->fc->ToArr( $query->result_array(), 'idnews');
		return $hasil;
	}

	public function get_row( $idnews=null ) {
		if ( is_null($idnews) ) $idnews='999999';
		$query = $this->db->query("select *  from d_news where idnews=$idnews");
		$hasil = $query->row_array();
		return $hasil;
	}

	public function save(  ) {
		$action		  = $_POST['ruh'];
		$idnews = $_POST['idnews'];
		$notif1		  = $_POST['notif1'];
		$link   	  = $_POST['link'];
		$toplist   	  = $_POST['toplist'];
		$file		  = $_FILES['file'];

		if ($file)   move_uploaded_file($file["tmp_name"], "files/news/".$file['name']);
		// if ($toplist==1) { $this->db->query( "update d_news set toplist='0'" ); } else { $toplist='0'; }

		if ($action=='Rekam') {
			$this->db->query( "insert into d_news (tglrekam,notif1,link,toplist,file) values (current_timestamp(),'$notif1','$link','$toplist','". $file['name'] ."')" );
		}
		if ($action=='Ubah') {
			$this->db->query( "update d_news set tglrekam=current_timestamp(), notif1='$notif1', link='$link', toplist='$toplist' where idnews=$idnews" );
			if ( $file['name'] != '' ) 	 	$this->db->query("update d_news set file='". $file['name'] ."' where idnews=$idnews" );
		}
		if ($action=='Hapus') {
			$this->db->query( "delete from d_news where idnews=$idnews" );
		}
		return;
	}
}
