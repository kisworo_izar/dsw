<?php
class Sb_survei_model extends CI_Model {
	public function get_data() {
		$query  = $this->db->query("SELECT CONVERT(a.kdlokasi, UNSIGNED INTEGER) AS ID, a.nmlokasi AS PROVINSI, IFNULL(b.KDSTATUS,0) AS KDSTATUS FROM t_lokasi a LEFT JOIN sb_survei_kirim b ON a.kdlokasi=b.kdlokasi WHERE a.lokasi_indo='1'");
		$data = '{"provinsi":'.json_encode($query->result_array()).'}';
		return $data;
	}

	public function get_jml() {
		$query  = $this->db->query("SELECT a.kdstatus AS kode,a.nmstatus, COUNT(b.kdstatus) AS jml,  (COUNT(b.kdstatus)/(SELECT COUNT(*) FROM sb_survei_kirim)) * 100 AS persen  FROM sb_survei_status a LEFT JOIN sb_survei_kirim b ON a.kdstatus=b.kdstatus GROUP BY 1");
		return $query->result_array();
	}

	public function get_notif() {
		$query = $this->db->query("Select * From sb_survei_notif Where toplist ='1'");
		return $query->result_array();
	}

	// notifikasi
	public function notif_get_data() {
		$whr  = "";
		$cari = $this->session->userdata('cari');

		if ($cari) {
			$whr = " where " ;
			$arr = explode(' ', $cari);
			for ($i=0; $i<count($arr); $i++) {
				$str  = $arr[$i];
				$whr .= "notif1 like '%$str%' or notif2 like '%$str%'";
				if ($i<count($arr)-1) {
					$whr .= " or ";
				}
			}
		}

		$query = $this->db->query("select *  from sb_survei_notif $whr order by idnotifikasi desc ");
		$hasil = $this->fc->ToArr( $query->result_array(), 'idnotifikasi');
		return $hasil;
	}

	public function notif_get_row( $idnotifikasi=null ) {
		if ( is_null($idnotifikasi) ) $idnotifikasi='999999';
		$query = $this->db->query("select *  from sb_survei_notif where idnotifikasi=$idnotifikasi");
		$hasil = $query->row_array();
		return $hasil;
	}

	public function notif_save(  ) {
		$action		  = $_POST['ruh'];
		$idnotifikasi = $_POST['idnotifikasi'];
		$notif1		  = $_POST['notif1'];
		$notif2	      = $_POST['notif2'];
		$link   	  = $_POST['link'];
		$toplist   	  = $_POST['toplist'];
		$file		  = $_FILES['file'];

		if ($file)   move_uploaded_file($file["tmp_name"], "files/notifikasi/".$file['name']);
		if ($toplist==1) { $this->db->query( "update sb_survei_notif set toplist='0'" ); } else { $toplist='0'; }

		if ($action=='Rekam') {
			$this->db->query( "insert into sb_survei_notif (tglrekam,notif1,notif2,link,toplist,file) values (current_timestamp(),'$notif1','$notif2','$link','$toplist','". $file['name'] ."')" );
		}
		if ($action=='Ubah') {
			$this->db->query( "update sb_survei_notif set tglrekam=current_timestamp(), notif1='$notif1', notif2='$notif2', link='$link', toplist='$toplist' where idnotifikasi=$idnotifikasi" );
			if ( $file['name'] != '' ) 	 	$this->db->query("update sb_survei_notif set file='". $file['name'] ."' where idnotifikasi=$idnotifikasi" );
		}
		if ($action=='Hapus') {
			$this->db->query( "delete from sb_survei_notif where idnotifikasi=$idnotifikasi" );
		}
		return;
	}



	// INPUT DATA dan REFERENSI ITEM
	function get_survei_item() {
		$thang = '2021';
		$query = $this->db->query("Select * From sb_survei_item Where thang='$thang' Order By kditem");
		$data['table'] = $this->fc->ToArr($query->result_array(), 'kditem');
		return $data;
	}

	function save_survei_item() {
		$aksi 		= $_POST['aksi'];
		$thang 		= $_POST['thang'];
		$kdlokasi	= $_POST['kdlokasi']; 
		$kditem 	= $_POST['kditem'];  
		$uraian 	= $_POST['uraian'];  
		$kdaktif	= $_POST['kdaktif'];
		$kdinput	= $_POST['kdinput'];

		if ($aksi=='Rekam') {
			$this->db->query( "Insert Into sb_survei_item (thang,kdlokasi ,kditem, uraian, kdaktif, kdinput) Values ('$thang','$kdlokasi' ,'$kditem', '$uraian', '$kdaktif', '$kdinput')" );
		}
		if ($aksi=='Ubah') {
			$this->db->query( "Update sb_survei_item Set uraian='$uraian',kdlokasi='$kdlokasi' ,kdaktif='$kdaktif', kdinput='$kdinput' Where thang='$thang' And kditem='$kditem'" );
		}
		if ($aksi=='Hapus') {
			$this->db->query( "Delete From sb_survei_item where thang='$thang' And kditem='$kditem'" );
		}
		//return "$thang#kdlokasi#$kditem#$uraian#$kdaktif#$kdinput";
		return;
	}

	function get_survei_data0($kdlokasi) {
		$thang = '2021';
		$query = $this->db->query("Select a.*, uraian,'' nil_input,'' nil_isi,concat(a.kdlokasi,a.kditem) kdkey, '0' kdcheck From sb_survei_data a Left Join sb_survei_item b On a.kditem=b.kditem Where a.thang='$thang' And a.kdlokasi='$kdlokasi' Order By 1,2,3");
		$hasil = $this->fc->ToArr($query->result_array(), 'kdkey');

		$query = $this->db->query("Select Concat(kdlokasi,Left(kditem,3),'000000000000') kdkey, Sum(If(kdisi='1',1,0)) kdisi, Count(*) recs From sb_survei_data Where kdinput='1' And kdlokasi='$kdlokasi' Group By 1");
		$final = $this->fc->ToArr($query->result_array(), 'kdkey');
		
		foreach ($final as $key=>$row) {
			if (array_key_exists($key, $hasil) And $row['kdisi'] == $row['recs']) $hasil[$key]['kdcheck'] ='1';
		}

		return $hasil;
	}

	function get_survei_data_kirim(){
		$thang = '2021';
		$query = $this->db->query("Select a.*,b.nmlokasi nmlokasi From sb_survei_kirim a Left Join t_lokasi b On a.kdlokasi=b.kdlokasi Order By a.kdlokasi");
		$data['table'] = $this->fc->ToArr($query->result_array(), 'kdlokasi');

		$query = $this->db->query("Select a.*, uraian,'' nil_input,'' nil_isi,concat(a.kdlokasi,a.kditem) kdkey, '0' kdcheck From sb_survei_data a Left Join sb_survei_item b On a.kditem=b.kditem Where a.thang='$thang' And a.kdlokasi='02' Order By 1,2,3");
		$data['table2'] = $this->fc->ToArr($query->result_array(), 'kdkey');

		$query = $this->db->query("Select Concat(kdlokasi,Left(kditem,3),'000000000000') kdkey, Sum(If(kdisi='1',1,0)) kdisi, Count(*) recs From sb_survei_data Where kdinput='1' And kdlokasi='02' Group By 1");
		$final = $this->fc->ToArr($query->result_array(), 'kdkey');
		
		foreach ($final as $key=>$row) {
			if (array_key_exists($key, $data['table2']) And $row['kdisi'] == $row['recs']) $data['table2'][$key]['kdcheck'] ='1';
		}

		return $data;
	}

	public function get_survei_data_kirim_rincian($kdlokasi){
		$thang = '2021';
		$query = $this->db->query("Select a.*, uraian,'' nil_input,'' nil_isi,concat(a.kdlokasi,a.kditem) kdkey, '0' kdcheck From sb_survei_data a Left Join sb_survei_item b On a.kditem=b.kditem Where a.thang='$thang' And a.kdlokasi='$kdlokasi' Order By 1,2,3");
		$data['table2'] = $this->fc->ToArr($query->result_array(), 'kdkey');

		$query = $this->db->query("Select Concat(kdlokasi,Left(kditem,3),'000000000000') kdkey, Sum(If(kdisi='1',1,0)) kdisi, Count(*) recs From sb_survei_data Where kdinput='1' And kdlokasi='$kdlokasi' Group By 1");
		$final = $this->fc->ToArr($query->result_array(), 'kdkey');
		
		foreach ($final as $key=>$row) {
			if (array_key_exists($key, $data['table2']) And $row['kdisi'] == $row['recs']) $data['table2'][$key]['kdcheck'] ='1';
		}
		return $data;
	}

	public function get_survei_data_dja(){
		$thang = '2021';

		$query = $this->db->query("Select a.*, uraian,'' nil_input,'' nil_isi,concat(a.kdlokasi,a.kditem) kdkey, '0' kdcheck From sb_survei_data a Left Join sb_survei_item b On a.kditem=b.kditem Where a.thang='$thang' And a.kdlokasi='02' Order By 1,2,3");
		$data['table'] = $this->fc->ToArr($query->result_array(), 'kdkey');

		$query = $this->db->query("Select a.*,concat(a.kdlokasi,a.kditem) kdkey,b.nmlokasi From sb_survei_data a Left Join t_lokasi b On a.kdlokasi=b.kdlokasi Where kdinput='1' And kditem='002001000000000' Order by kdkey  ");
		$data['table2'] = $this->fc->ToArr($query->result_array(), 'kdkey');

		return $data;
	}

	function get_survei_data_dja_rincian($kditem){
		$thang = '2021';
		$query = $this->db->query("Select a.*, concat(a.kdlokasi,a.kditem) kdkey, b.nmlokasi From sb_survei_data a Left Join t_lokasi b On a.kdlokasi=b.kdlokasi Where kdinput='1' And kditem='$kditem' Order by kdkey ");
		$data['table'] = $this->fc->ToArr($query->result_array(),'kdkey');
		return $data;
	}
	
	function save_survei_data( ) {
		$kditem 	   = $_POST['kditem'];
		$r1_nama 	   = $_POST['r1_nama'];
		$r1_harga 	   = $_POST['r1_harga'];
		$r2_nama 	   = $_POST['r2_nama'];
		$r2_harga 	   = $_POST['r2_harga'];
		$r3_nama 	   = $_POST['r3_nama'];
		$r3_harga 	   = $_POST['r3_harga'];
		$kdlokasi 	   = $_POST['kdlokasi'];
	
		$this->db->query("Update sb_survei_data Set r1_nama='$r1_nama', r1_harga='$r1_harga', r2_nama='$r2_nama',r2_harga='$r2_harga',r3_nama='$r3_nama', r3_harga='$r3_harga', kdisi=1 Where kditem='$kditem' And kdlokasi='$kdlokasi' ");
		return;
	}

	function save_survei_data_edit(){
		$kditem = $_POST['kditem'];
		$kdisi  = $_POST['kdisi'];
		//$kdlokasi= '01';

		if ($kdisi=='1'){
			$this->db->query("Update sb_survei_data Set kdisi='0' Where kditem='$kditem' And kdlokasi='02' ");
		}
		return;
	}

	function save_survei_data_flag(){
		$kditem   = $_POST['kditem'];
		$kdisi    = $_POST['kdisi'];
		$kdlokasi = $_POST['kdlokasi'];

		$this->db->query("Update sb_survei_data Set kdisi='1', r1_nama='-', r1_harga='0', r2_nama='-', r2_harga='0', r3_nama='-', r3_harga='0' Where kditem='$kditem' and kdlokasi='$kdlokasi' ");
		return;
	}

	function save_survei_data_keterangan(){
		$kditem 		= $_POST['kditem'];
		$r1_keterangan	= $_POST['r1_keterangan'];
		$file			= $_POST['file'];
		$kdlokasi 		= $_POST['kdlokasi'];

		$this->db->query("Update sb_survei_data Set r1_keterangan = '$r1_keterangan', file='$file' Where kditem='$kditem' And kdlokasi='$kdlokasi' ");
		return;
	}

	public function save_survei_perbenpusat(){
		$kdlokasi = $_POST['kdlokasi'];
		$file 	  = $_POST['file'];

		$this->db->query("Update sb_survei_kirim Set tglrekam=Current_TimeStamp(), kdstatus='1', file='$file' Where kdlokasi='$kdlokasi' ");
		return;
	}

	public function save_survei_dja(){
		$kdlokasi = $_POST['kdlokasi'];
		$value 	  = $_POST['value'];
		if($value=='simpan') $this->db->query("Update sb_survei_kirim Set tglkirim=Current_TimeStamp(), kdstatus='2' Where kdlokasi='$kdlokasi' ");
		if($value=='batal') $this->db->query("Update sb_survei_kirim Set kdstatus='0',tglrekam=null Where kdlokasi='$kdlokasi' ");
		return;
	}

	// notifikasi end

	public function get_test() {
		$query  = $this->db->query("SELECT a.kdstatus, COUNT(*) AS jml, (COUNT(*)/(SELECT COUNT(*) FROM sb_survei_kirim)) * 100 AS persen, b.nmstatus FROM sb_survei_kirim a LEFT JOIN sb_survei_status b ON a.kdstatus = b.kdstatus GROUP BY 1");
		$data = '{"provinsi":'.json_encode($query->result_array()).'}';

		return $data;
	}

	public function save_keterangan_subitem(){
		$kdlokasi = $_POST['kdlokasi'];
		$ket 	  = $_POST['ket'];
		$kditem   = $_POST['kditem'];

		$this->db->query("Update sb_survei_data set r2_keterangan='$ket' where kdlokasi='$kdlokasi' And kditem='$kditem' ");
		return;
	}

	// INFLASI

	public function get_survei_inflasi(){
		$thang = '2021';
		// $query = $this->db->query("Select a.*,b.* From sb_survei_inflasi a Left Join sb_survei_item b On a.kditem = b.kditem Order By a.kditem ");
		$query = $this->db->query("Select a.*, b.* From sb_survei_inflasi a Left Join t_lokasi b On a.kdlokasi= b.kdlokasi Where lokasi_indo > 0");
		$data['table'] = $this->fc->ToArr($query->result_array(), 'kdlokasi');

		$query = $this->db->query("Select * From sb_survei_item Where right(kditem, 12) = 000000000000");
		$data['item'] = $this->fc->ToArr($query->result_array(), 'kditem');
		// print_r($data['item']);exit();
		return $data;
	}

	function save_survei_inflasi() {
		$aksi 		= $_POST['aksi'];
		$thang 		= $_POST['thang'];  
		$kditem 	= $_POST['kditem'];  
		$inflasi_n 	= $_POST['inflasi_n'];
		$inflasi_n1 = $_POST['inflasi_n1'];

		// if ($aksi=='Rekam') {
		// 	$this->db->query( "Insert Into sb_survei_item (thang, kditem, uraian, kdaktif, kdinput) Values ('$thang', '$kditem', '$uraian', '$kdaktif', '$kdinput')" );
		// }
		if ($aksi=='Ubah') {
			$this->db->query( "Update sb_survei_inflasi Set inflasi_n='$inflasi_n', inflasi_n1='$inflasi_n1' Where thang='$thang' And kditem='$kditem'" );
		}
		// if ($aksi=='Hapus') {
		// 	$this->db->query( "Delete From sb_survei_item where thang='$thang' And kditem='$kditem'" );
		// }
		return "$thang#$kditem#$inflasi_n#inflasi_n1";
	}

}
