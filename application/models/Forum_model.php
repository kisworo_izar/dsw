<?php 
class Forum_model extends CI_Model {

	public function sql_room() {
		$query = $this->db->query("Select a.*, nmuser, nip From d_forum_room a Left Join t_user b On a.iduser=b.iduser Where idparent=1 Order By 1,2");
		return $query->result_array();
	}

	public function sql_head($idparent, $idchild) {
		$query = $this->db->query("Select a.*, nmuser, nip From d_forum_room a Left Join t_user b On a.iduser=b.iduser Where idparent=$idparent and idchild=$idchild");
		return $query->row_array();
	}

	public function sql_sub($idparent, $idchild) {
		$query = $this->db->query("Select a.*, concat(idparent,'.',idchild) idkey, '' nmpic, nmuser, nip From d_forum_room a Left Join t_user b On a.iduser=b.iduser Where idparent=$idchild Order By 1,2");
		$hasil = $this->fc->ToArr( $query->result_array(), 'idkey');
		foreach ($hasil as $key=>$value) {
            if ($value['pic']!='') {
                $arr = explode(',', trim($value['pic']));
                for ($i=0; $i<count($arr); $i++) {
                    $query = $this->db->query("Select fullname nama From t_user Where iduser='". $arr[$i] ."'");
                    $nilai = $query->row_array();
                    $hasil[$key]['nmpic'] .= str_replace('PIC ', '', $nilai['nama']);
                    if ( $i<count($arr)-1 ) $hasil[$key]['nmpic'] .= ",";
                }
            }
		}
		return $hasil;
	}

	public function sql_thread($idparent, $idchild) {
		$query = $this->db->query("Select idparent, idchild From d_forum_room Where idparent=$idchild");
		$idkey = $this->fc->InVar( $query->result_array(), 'idchild' );

		$query = $this->db->query("Select concat(idparent,'.',idchild) idkey, idparent, idchild, thread, tglpost, a.iduser, nmuser, nip From d_forum a Left Join t_user b On a.iduser=b.iduser Where idparent $idkey and idforum=1 Order By tglpost Desc");
		$hasil = $this->fc->ToArr( $query->result_array(), 'idkey');

		$query = $this->db->query("Select concat(idparent,'.',idchild) idkey, count(*)-1 replies From d_forum Where concat(idparent,'.',idchild) ". $this->fc->InVar($hasil, 'idkey') ." Group By 1");
		$repli = $this->fc->ToArr( $query->result_array(), 'idkey');

		$query = $this->db->query("Select idparent, idchild, concat(idparent, idchild, max(idforum)) idkey From d_forum Where concat(idparent,'.',idchild) ". $this->fc->InVar($hasil, 'idkey') ." Group By 1,2");
		$idkey = $this->fc->InVar( $query->result_array(), 'idkey' );
		$query = $this->db->query("Select concat(idparent,'.',idchild) idkey, a.iduser, nmuser, tglpost From d_forum a Left Join t_user b On a.iduser=b.iduser Where concat(idparent,idchild,idforum) $idkey");
		$post  = $this->fc->ToArr( $query->result_array(), 'idkey');

		foreach ($hasil as $key=>$value) {
			$hasil[ $key ]['replies'] = $repli[ $key ]['replies'];
			$hasil[ $key ]['nmreplies'] = $post[ $key ]['nmuser'];
			$hasil[ $key ]['tglreplies'] = $post[ $key ]['tglpost'];
		}

		$query  = $this->db->query("Select iduser, nmuser, nip, jabatan, a.kdso, nmso From t_user a left join t_so b on a.kdso=b.kdso where iduser " . $this->fc->InVar($hasil, 'iduser'));
		$t_user = $this->fc->ToArr( $query->result_array(), 'iduser');
		$hasil  = $this->fc->Left_Join( $hasil, $t_user, 'iduser,nmuser=nmuser,nip=nip,jabatan=jabatan,nmso=nmso' );
		return $hasil;
	}

	public function sql_top( $limit=null ) {	// NEED TO BE DELETED
		if ($limit==null) $limit=5;
		$query = $this->db->query("Select concat(idparent,'.',idchild) idkey, idparent, idchild, count(*) reply From d_forum Group By 1,2,3 Order By reply Desc Limit $limit");
		$hasil = $this->fc->ToArr( $query->result_array(), 'idkey');

		foreach ($hasil as $key=>$value) {
			// Forum Room
			$query = $this->db->query("Select nmroom From d_forum_room where idparent=". $value['idparent'] ." and idchild=". $value['idchild']);
			$room  = $query->row_array();
			$hasil[ $key ]['nmroom'] = $room['nmroom'];

			// Forum Home of Room
			$query = $this->db->query("Select thread From d_forum where idparent=". $value['idparent'] ." and idchild=". $value['idchild'] ." and idforum=1");
			$home  = $query->row_array();
			$hasil[ $key ]['thread'] = $home['thread'];
		}
		return $hasil;
	}

	public function sql_last( $limit=null ) {	// NEED TO BE DELETED
		if ($limit==null) $limit=5;
		$query = $this->db->query("Select concat(idparent,'.',idchild) idkey, idparent, idchild, max(tglpost) tglpost From d_forum Group By 1,2,3 Order By tglpost Desc Limit $limit");
		$hasil = $this->fc->ToArr( $query->result_array(), 'idkey');

		foreach ($hasil as $key=>$value) {
			// Forum Room
			$query = $this->db->query("Select nmroom From d_forum_room where idparent=". $value['idparent'] ." and idchild=". $value['idchild']);
			$room  = $query->row_array();
			$hasil[ $key ]['nmroom'] = $room['nmroom'];

			// Forum Home of Room
			$query = $this->db->query("Select thread From d_forum where idparent=". $value['idparent'] ." and idchild=". $value['idchild'] ." and idforum=1");
			$home  = $query->row_array();
			$hasil[ $key ]['thread'] = $home['thread'];
		}
		return $hasil;
	}

	public function post_room($idparent) {
		$query = $this->db->query("Select * From d_forum_room Where idchild=$idparent ");
		return $query->row_array();
	}

	public function post_head($idparent, $idchild) {
		$query = $this->db->query("Select a.*, nmuser, nip, jabatan, nmso, pic From d_forum a Left Join t_user b On a.iduser=b.iduser Left Join t_so c On b.kdso=c.kdso Left Join d_forum_room d On a.idparent=d.idparent and a.idchild=d.idchild Where a.idparent=$idparent and a.idchild=$idchild and idforum=1");
		return $query->row_array();
	}

	public function post_reply($idparent, $idchild) {
		$query = $this->db->query("Select a.*, nmuser, nip, jabatan, nmso From d_forum a Left Join t_user b On a.iduser=b.iduser Left Join t_so c On b.kdso=c.kdso Where idparent=$idparent and idchild=$idchild and idforum!=1 Order By idforum");
		return $query->result_array();
	}

	public function room_option($idparent=null, $idchild=null) {
		if ( $idparent!=null ) {
			$query = $this->db->query("Select *, concat(idparent,'.',idchild) kdkey, idparent*1000+idchild urut, 'selected' selected From d_forum_room Where idparent=$idparent And idchild=$idchild");
		} else {
			$query = $this->db->query("Select *, if(idparent=1, concat(idchild,'.0'), concat(idparent,'.',idchild)) kdkey, if(idparent=1, idchild*1000, idparent*1000+idchild) urut, '' selected From d_forum_room Where idparent>0 And kategori='1' Order By 1,2");
		}
		$hasil = $this->fc->ToArr( $query->result_array(), 'kdkey');
		return $this->fc->array_index($hasil, 'urut');
	}

	public function get_pic( $pic=null ) {
		$whr = '';  if ($pic!=null) $whr = "and iduser in ('". str_replace(',', "','", $pic) ."')" ;
		$query = $this->db->query("Select iduser, fullname, kdso, concat(kdso,'2',fullname,'Y') kdkey From t_user Where fullname Like 'PIC%' and kdso!='' $whr Order By 3");
		$subdit= $query->result_array();

		if ( count($subdit)==1 ) return $subdit;

		$query = $this->db->query("Select 'dit' iduser, nmso fullname, kdso, concat(kdso,'1','X') kdkey From t_so Where kdso ". $this->fc->InVar($subdit, 'kdso'));
		$dit   = $this->fc->ToArr( $query->result_array(), 'kdkey');
		$hasil = array_merge_recursive($dit, $subdit);
		return $this->fc->array_index($hasil, 'kdkey');
	}

	public function ajax_pic( $idkey) {
		$query = $this->db->query("Select pic From d_forum_room Where concat(idparent,'.',idchild)='$idkey'");
		$nilai = $query->row_array();
		$nilai = $this->get_pic( $nilai['pic'] );

        $msg = '';
        if ( count($nilai)>1 ) $msg = '<option hidden>** Pilih PIC untuk Thread **</option>';
        foreach ($nilai as $row) {
	        if ( substr($row['kdkey'],-1)=='1') {
	            if ( substr($row['kdkey'],-1)=='2') $msg .= '</optgroup>'; // Clossing Group
	            $msg .= '<optgroup label="'. $row['fullname'] .'">';
	        } else {
	            $msg .= '<option value="'. $row['iduser'] .'">&nbsp;&nbsp;&nbsp;'.  $row['fullname'] .'</option>';
	        }
	    }
        return $msg;
	}

	public function save_thread() {
		$action	  = $_POST['ruh'];
		$iduser	  = $this->session->userdata('iduser');
		$pic      = $_POST['pic'];
		$thread   = $_POST['thread'];
		$comment  = str_replace('"', "'", $_POST['content']);
		$file	  = $_FILES['file'];

		if (strpos($pic,'Pilih')) $pic=''; 
		if ($file) move_uploaded_file($file["tmp_name"], "files/forum/".$file['name']);

		if ($action=='Posting') {
			$idkey    = explode('.',  $_POST['group']);
			$idparent = $idkey[0];
			$idchild  = $idkey[1];

			// Add D_FORUM_ROOM
			$query = $this->db->query("insert into d_forum_room (idparent,nmroom,iduser,pic,tglcreate) values ($idchild,\"$thread\",'$iduser', '$pic', current_timestamp())");

			// Identify D_FORUM_ROOM
			$query = $this->db->query("Select idparent, idchild From d_forum_room Where idparent=$idchild And nmroom=\"$thread\"");
			$room  = $query->row();

			// Add D_FORUM
			$query = $this->db->query("insert into d_forum (idparent,idchild,iduser,tglpost,thread,post) values ($room->idparent,$room->idchild,'$iduser',current_timestamp(),\"$thread\",\"$comment\")");

			// Add D_FORUM Update Files
			if ( $file['name'] != '' ) 	$this->db->query("update d_forum set attach='". $file['name'] ."' where idparent=$room->idparent and idchild=$room->idchild" );
			return site_url("forum?q=JFuBN&idparent=$room->idparent&idchild=$room->idchild");
		}

		if ($action=='Ubah') {
			$idparent = $_POST['idparent'];
			$idchild  = $_POST['idchild'];
			$this->db->query( "update d_forum set thread=\"$thread\", post=\"$comment\" where idparent=$idparent and idchild=$idchild and idforum=1" );
			$this->db->query( "update d_forum_room set pic='$pic' where idparent=$idparent and idchild=$idchild" );
			if ( $file['name'] != '' ) $this->db->query("update d_forum set attach='". $file['name'] ."' where idparent=$idparent and idchild=$idchild and idforum=1" );
			return site_url("forum?q=JFuBN&idparent=$idparent&idchild=$idchild");

		}

		if ($action=='Catatan') {
			$idparent = $_POST['idparent'];
			$idchild  = $_POST['idchild'];
			$this->db->query( "update d_forum set catatan=\"$comment\" where idparent=$idparent and idchild=$idchild and idforum=1" );
			return site_url("forum?q=JFuBN&idparent=$idparent&idchild=$idchild");

		}
	}

}