<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Admin_model extends CI_Model {

	public function get_user_group() {
		$query = $this->db->query("Select * From t_user_group Order By idusergroup");
		$group = $this->fc->ToArr( $query->result_array(), 'idusergroup');

		$query = $this->db->query("Select * From t_menu");
		$menu  = $this->fc->ToArr( $query->result_array(), 'idmenu');

		foreach ($group as $key=>$value) {
			$arr = array();
			$str = explode(';', $value['menu']);
			asort( $str );
			for ($i=0; $i<count($str); $i++) {
				$nil = explode('-', $str[$i]);
				if ( array_key_exists($nil[0], $menu) ) {
					$arr[ $nil[0] ] = array('idmenu'=>$nil[0], 'menu'=>$menu[ $nil[0] ]['menu'], 'rwx'=>$nil[1]);
				}
			}
			$group[$key]['menu'] = $arr;
		}
		return $group;
	}

}
