<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Persuratan_model extends CI_Model {
	public function __construct() {
        parent::__construct();
    }

	function get_data( $param ) {
		$dbsurat = $this->load->database('surat', TRUE);

		// Where CONDITION
		$whr = "where year(tgagenda)=$param->tahun "; if ($param->bulan<>"00") $whr .= "And month(tgagenda)=$param->bulan "; 
		$sts = "And (kdstatus='1' or kdstatus='2' or kdstatus='6')";
		
		// FILTER BAGIAN
		$bag = "And kddari=$param->bagian ";  if ( $param->bagian=='01' ) $bag = "";

		// CARI
		if ( isset($param->cari) ) {
			$arr = explode(' ', $param->cari);
			for ($i=0; $i<count($arr); $i++) {
				$nilai = $arr[$i];
			 	$whr .= " And (Concat(noagenda,noagenda1) like '%$nilai%' or perihal like '%$nilai%')";
			}
		}

		// SQL - D_INDUK
		$sql = "Select Concat(golsurat,noagenda,trim(noagenda1)) idkey, tgagenda,noagenda,noagenda1,golsurat,jenis,tljnssurat,tltgsurat,tlnosurat,tlnosurat1,perihal,'' perihal2,kdstatus,kdbagian,tgdisposis,kddari,tgproses, 
				'' dispos, '' tgsurat,'' nosurat,'' pengirim, '' nmfile, '' selesai,'' lama,'' status, 0 nilai, '' dispos, catatan, catatan1, '' catatantup 
				From d_induk $whr $sts $bag Order By tgagenda Desc,kdstatus,idkey,kddari";
		$query = $dbsurat->query($sql);
		$induk = $query->result_array();

// return $induk;

		if (!$induk) return $induk;
		
		// SQL - D_SURAT
		$sql = "Select Concat(golsurat,noagenda,trim(noagenda1)) idkey, tgsurat,nosurat,pengirim,nmfile,catatantup From d_surat $whr";
		$query = $dbsurat->query($sql);
		$surat = $this->fc->ToArr( $query->result_array(), 'idkey' );

		// SQL - D_DISPOS
		$whr = "where year(tgdisposis)=$param->tahun "; if ($param->bulan<>"00") $whr .= "And month(tgdisposis)=$param->bulan"; 
		$sql = "Select Concat(golsurat,noagenda,trim(noagenda1)) idkey, selesai From d_dispos $whr And selesai<>'0000-00-00'";
		$query = $dbsurat->query($sql);
		$dispos = $this->fc->ToArr( $query->result_array(), 'idkey' );

		// SQL - ESELON 3
		$query = $dbsurat->query("Select kode, uraian From t_esl3");
		$bagian= $this->fc->ToArr($query->result_array(), 'kode');

		// SQL - T_JNSSURAT
		$query = $dbsurat->query("Select kode, huruf From t_jnssurat");
		$jnssurat = $this->fc->ToArr($query->result_array(), 'kode');

		
		// SQL - MERGE
		$str = array(); $no=0;
		$kddari = '';
		foreach ($induk as $row) {
			if ( !array_key_exists($row['idkey'], $str) ) $str[ $row['idkey'] ] = $row;
	
		    if ( $row['kdbagian']<>$param->bagian And array_key_exists($row['kdbagian'], $bagian) ) {
			  if ( $kddari != $row['kddari'] )  {
			  	$kddari = $row['kddari'];
			  	$str[ $row['idkey'] ]['dispos'] .= "<br><i>Dari : </i><b>" . $bagian[$kddari]['uraian'] ."</b>";
			  }
			  if ( strpos($str[ $row['idkey'] ]['dispos'], $bagian[ $row['kdbagian'] ]['uraian'])==0 )			  
			  	$str[ $row['idkey'] ]['dispos'] .= "<br>". $bagian[ $row['kdbagian'] ]['uraian'];
			}

		    if ( array_key_exists($row['idkey'], $surat) ) {
			  $str[ $row['idkey'] ]['tgsurat']  = $surat[ $row['idkey'] ]['tgsurat'];
			  $str[ $row['idkey'] ]['nosurat']  = $surat[ $row['idkey'] ]['nosurat'];
			  $str[ $row['idkey'] ]['pengirim'] = $surat[ $row['idkey'] ]['pengirim'];
			  $str[ $row['idkey'] ]['catatantup'] = $surat[ $row['idkey'] ]['catatantup'];
			  if ($surat[ $row['idkey'] ]['nmfile']) {
				$str[ $row['idkey'] ]['nmfile'] = $surat[ $row['idkey'] ]['nmfile'];
			  }
			}

			// Check Status sudah SELESAI (6) atau BELUM (2)
			if ($row['kdstatus']=='6') {
				  $str[ $row['idkey'] ]['tlnosurat'] = date('d-m-Y',strtotime($row['tltgsurat']));
				  $str[ $row['idkey'] ]['selesai'] = "";
				  $str[ $row['idkey'] ]['status'] = "";
				  $str[ $row['idkey'] ]['lama'] = 0;
				  $str[ $row['idkey'] ]['nilai']= 1;
				  if ( array_key_exists($row['tljnssurat'], $jnssurat) ) $str[ $row['idkey'] ]['tlnosurat'] .= '<br>'. $jnssurat[$row['tljnssurat']]['huruf'] . $row['tlnosurat'] . $row['tlnosurat1'];
				  if ( $str[ $row['idkey'] ]['perihal']<>$row['perihal'] ) $str[ $row['idkey'] ]['perihal2'] = '<br><br>'. $row['perihal'];
			} else {
				if (array_key_exists($row['idkey'], $dispos)) {
				  $diff = date_diff( date_create( $dispos[ $row['idkey'] ]['selesai'] ), date_create(date('Y-m-d')) );
				  $days = $diff->format("%a");
				  $str[ $row['idkey'] ]['selesai'] = '<br>Harus selesai selambat-lambatnya tanggal : '. date('d-m-Y',strtotime( $dispos[ $row['idkey'] ]['selesai'] ));
				  $str[ $row['idkey'] ]['lama'] = $days;
				
				  if ( $dispos[ $row['idkey'] ]['selesai'] >= date_create(date('Y-m-d')) ) {	// MASIH PROSES
					if ($days<-1) {
					  $str[ $row['idkey'] ]['status'] = "";
					  $str[ $row['idkey'] ]['nilai'] = 2;
					}
				    if ($days>=-1 And $days<=0) {
					  $str[ $row['idkey'] ]['status'] = abs($days);
					  $str[ $row['idkey'] ]['nilai'] = 3;
					}
				  }
				  if ( $dispos[ $row['idkey'] ]['selesai'] < date_create(date('Y-m-d')) ) {	// TERLAMBAT
					if ($days>0) {
					  $str[ $row['idkey'] ]['status'] = $days ;
					  $str[ $row['idkey'] ]['nilai'] = 4;
					}
				  }
				  
				}
			}
		}

		return $str;
		// return $this->fc->array_index($str, 'tgagenda', 'Desc');
	}

	function count_rec( $param ) {
		$whr = "where year(tgagenda)=$param->tahun And month(tgagenda)=$param->bulan And (kdstatus='1' or kdstatus='2' or kdstatus='6')"; 
		if ( strlen(trim($param->cari))>0 ) { $whr .= " And perihal like '%$param->cari%'"; }
		$query = $dbsurat->query("Select count(*) total From d_induk $whr And kddari=$param->bagian");
		$query = $query->row();
		return $query->total;
	}

	function get_badge( $array ) {
		$hasil = array(0,0,0,0,0);
		foreach ($array as $row) { $hasil[ $row['nilai'] ]++; }
		return $hasil;
	}
	
	function filter_status( $array, $status ) {
		$hasil = array();
		foreach ($array as $row) { 
			if ($row['nilai']==$status) $hasil[ $row['idkey'] ] = $row;
		}
		return $hasil;
	}

	function get_layanan( $param ) {
		$dbsurat = $this->load->database('surat', TRUE);

		// Where CONDITION
		$whr = "Where year(tgagenda)=$param->tahun"; if ($param->bulan<>"00") $whr .= " And month(tgagenda)=$param->bulan"; 
		$sts = "(kdstatus='1' or kdstatus='2' or kdstatus='6')";
		$bag = "And kddari='$param->bagian'";  if ( $param->bagian=='01' ) $bag = "";

		// SQL - D_SURAT untuk LAYANAN UNGGULAN
		$query = $dbsurat->query("Select Concat(golsurat,noagenda,trim(noagenda1)) idkey, tgsurat, nosurat, pengirim, nmfile, catatantup  From d_surat $whr And kdrevisi='$param->model' Order By tgagenda Desc");
		$surat = $this->fc->ToArr($query->result_array(), 'idkey');

		if (!$surat) return $surat;

		// SQL - D_INDUK
		$query = $dbsurat->query("Select Concat(golsurat,noagenda,trim(noagenda1)) idkey, tgagenda, noagenda, noagenda1, golsurat, jenis,tljnssurat, tltgsurat, tlnosurat, tlnosurat1, perihal, '' perihal2, kdstatus, kdbagian, tgdisposis, kddari, tgproses, '' dispos, '' tgsurat, '' nosurat,'' pengirim, '' nmfile, '' selesai, '' lama, '' status, 0 nilai, '' dispos, catatan, catatan1, '' catatantup From d_induk Where Concat(golsurat,noagenda,trim(noagenda1))". $this->fc->InVar($surat, 'idkey') ." Order By tgagenda Desc,kdstatus,idkey,kddari");
		$induk = $this->fc->ToArr($query->result_array(), 'idkey');
		
		// SQL - D_DISPOS
		$whr = "where year(tgdisposis)=".$param->tahun; if ($param->bulan<>"00") $whr .= " And month(tgdisposis)=".$param->bulan; 
		$query = $dbsurat->query("Select Concat(golsurat,noagenda,trim(noagenda1)) idkey, selesai From d_dispos $whr And selesai<>'0000-00-00'");
		$dispos = $this->fc->ToArr( $query->result_array(), 'idkey' );

		// SQL - ESELON 3
		$query = $dbsurat->query("Select kode, uraian From t_esl3");
		$bagian= $this->fc->ToArr($query->result_array(), 'kode');

		// SQL - T_JNSSURAT
		$query = $dbsurat->query("Select kode, huruf From t_jnssurat");
		$jnssurat = $this->fc->ToArr($query->result_array(), 'kode');

		
		// SQL - MERGE
		$str = array(); $no=0;
		$kddari = '';
		foreach ($induk as $row) {
			if ( !array_key_exists($row['idkey'], $str) ) $str[ $row['idkey'] ] = $row;
	
		    if ( $row['kdbagian']<>$param->bagian And array_key_exists($row['kdbagian'], $bagian) ) {
			  if ( $kddari != $row['kddari'] )  {
			  	$kddari = $row['kddari'];
			  	$str[ $row['idkey'] ]['dispos'] .= "<br><i>Dari : </i><b>" . $bagian[$kddari]['uraian'] ."</b>";
			  }
			  if ( strpos($str[ $row['idkey'] ]['dispos'], $bagian[ $row['kdbagian'] ]['uraian'])==0 )			  
			  	$str[ $row['idkey'] ]['dispos'] .= "<br>". $bagian[ $row['kdbagian'] ]['uraian'];
			}

		    if ( array_key_exists($row['idkey'], $surat) ) {
			  $str[ $row['idkey'] ]['tgsurat']  = $surat[ $row['idkey'] ]['tgsurat'];
			  $str[ $row['idkey'] ]['nosurat']  = $surat[ $row['idkey'] ]['nosurat'];
			  $str[ $row['idkey'] ]['pengirim'] = $surat[ $row['idkey'] ]['pengirim'];
			  $str[ $row['idkey'] ]['catatantup'] = $surat[ $row['idkey'] ]['catatantup'];
			  if ($surat[ $row['idkey'] ]['nmfile']) {
				$str[ $row['idkey'] ]['nmfile'] = $surat[ $row['idkey'] ]['nmfile'];
			  }
			}

			// Check Status sudah SELESAI (6) atau BELUM (2)
			if ($row['kdstatus']=='6') {
				  $str[ $row['idkey'] ]['tlnosurat'] = date('d-m-Y',strtotime($row['tltgsurat']));
				  $str[ $row['idkey'] ]['selesai'] = "";
				  $str[ $row['idkey'] ]['status'] = "";
				  $str[ $row['idkey'] ]['lama'] = 0;
				  $str[ $row['idkey'] ]['nilai']= 1;
				  if ( array_key_exists($row['tljnssurat'], $jnssurat) ) $str[ $row['idkey'] ]['tlnosurat'] .= '<br>'. $jnssurat[$row['tljnssurat']]['huruf'] . $row['tlnosurat'] . $row['tlnosurat1'];
				  if ( $str[ $row['idkey'] ]['perihal']<>$row['perihal'] ) $str[ $row['idkey'] ]['perihal2'] = '<br><br>'. $row['perihal'];
			} else {
				if (array_key_exists($row['idkey'], $dispos)) {
				  $diff = date_diff( date_create( $dispos[ $row['idkey'] ]['selesai'] ), date_create(date('Y-m-d')) );
				  $days = $diff->format("%a");
				  $str[ $row['idkey'] ]['selesai'] = '<br>Harus selesai selambat-lambatnya tanggal : '. date('d-m-Y',strtotime( $dispos[ $row['idkey'] ]['selesai'] ));
				  $str[ $row['idkey'] ]['lama'] = $days;
				
				  if ( $dispos[ $row['idkey'] ]['selesai'] >= date_create(date('Y-m-d')) ) {	// MASIH PROSES
					if ($days<-1) {
					  $str[ $row['idkey'] ]['status'] = "";
					  $str[ $row['idkey'] ]['nilai'] = 2;
					}
				    if ($days>=-1 And $days<=0) {
					  $str[ $row['idkey'] ]['status'] = abs($days);
					  $str[ $row['idkey'] ]['nilai'] = 3;
					}
				  }
				  if ( $dispos[ $row['idkey'] ]['selesai'] < date_create(date('Y-m-d')) ) {	// TERLAMBAT
					if ($days>0) {
					  $str[ $row['idkey'] ]['status'] = $days ;
					  $str[ $row['idkey'] ]['nilai'] = 4;
					}
				  }
				  
				}
			}
		}
		return $str;
	}

	function nama_model( $model ) {
		$dbsurat = $this->load->database('surat', TRUE);
		$query = $dbsurat->query("Select Upper(uraian) uraian From t_layanan Where kode='$model'");
		$model = $query->row_array();
		return $model['uraian'];
	}

}
?>