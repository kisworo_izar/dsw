<?php
class Revisi_pa_model extends CI_Model {

	private $dbrevisi;

	public function __construct()
		{
		parent::__construct();

		        //$this->dbrevisi = $this->load->database('dbsatu', TRUE);
		        $this->dbrevisi = $this->load->database('revisi'. $this->session->userdata('thang'), TRUE);

		}
	

	public function get_data( $depuni=null, $start, $end ) {
		
		// $tgl = array('pus_tgl','t2_proses_tgl','t2_selesai_tgl','t3_proses_tgl','t3_selesai_tgl','t4_proses_tgl','t4_selesai_tgl','t5_proses_tgl','t5_selesai_tgl','t6_proses_tgl','t6_selesai_tgl','t7_proses_tgl','t7_selesai_tgl');
		// $whr = "Where rev_tahun='".  date('Y') ."' And (";
		// for ($i=0; $i<count($tgl); $i++) {
		// 	$whr .= $tgl[$i] ." Between '$start' And '$end' ";
		// 	if ($i<count($tgl)-1) $whr .= " Or "; else $whr .= ")";
		// }
		// $whr = "";
		// //if ($depuni!=null) $whr .= " And  concat(kl_dept,'.',kl_unit)='$depuni' ";
		$range = "and date(pus_tgl) Between '$start' And '$end' ";
		if (! (strrpos($this->session->userdata('idusergroup'), '001') or strrpos($this->session->userdata('idusergroup'), '002') or strrpos($this->session->userdata('idusergroup'), '503')) ) {
			$query = $this->db->query("Select kddept From revisi_wenang Where kdso='". $this->session->userdata('kdso') ."'");
			$hasil = $query->row_array();
			$whr = "and kl_dept In ('". str_replace(",", "','", $hasil['kddept']) ."')";
		} else { $whr = ""; }

		//$whr='';
		$query = $this->dbrevisi->query("Select rev_tahun, rev_id, rev_tahap, rev_status, kl_dept, '' nmdept, concat(kl_dept,'.',kl_unit) kl_unit, '' nmunit, kl_surat_no, kl_surat_tgl, kl_surat_hal,
			pus_status, pus_tgl, kl_pjb_jab, kl_pjb_nama, kl_pjb_nip, kl_telp, t2_catatan, t7_catatan,
			t2_status, if(t2_status='1',t2_proses_tgl,t2_selesai_tgl) t2_tgl,
			t6_status, if(t6_status='1',t6_proses_tgl,t6_selesai_tgl) t6_tgl,
			t7_status, if(t7_status='1',t7_proses_tgl,t7_selesai_tgl) t7_tgl
			From revisi where rev_level ='2' $whr  Order By rev_tahun Desc, pus_tgl Desc");
		$hasil = $this->fc->ToArr( $query->result_array(), 'rev_id');

		if ($hasil) {
			$dbref = $this->load->database('ref'. $this->session->userdata('thang'), TRUE);
			$query = $dbref->query("Select kddept kode, nmdept uraian, '1' level From t_dept Where kddept ". $this->fc->InVar($hasil, 'kl_dept') );
			$dept  = $this->fc->ToArr( $query->result_array(), 'kode');

			$query = $dbref->query("Select concat(kddept,'.',kdunit) kode, nmunit uraian, '2' level From t_unit Where concat(kddept,'.',kdunit) ". $this->fc->InVar($hasil, 'kl_unit') );
			$unit  = $this->fc->ToArr( $query->result_array(), 'kode');

			foreach ($hasil as $key=>$value) {
				if (array_key_exists($value['kl_dept'], $dept)) $hasil[$key]['nmdept'] = $dept[ $value['kl_dept'] ]['uraian'];
				if (array_key_exists($value['kl_unit'], $unit)) $hasil[$key]['nmunit'] = $unit[ $value['kl_unit'] ]['uraian'];
			}

			$data['depuni'] = $this->fc->array_index( array_merge_recursive($dept, $unit), 'kode');
			$data['kode']   = $depuni;
		} else {
			$data['depuni'] = array();
			$data['kode']   = array();
		}

		$data['revisi'] = $hasil;
		$data['start'] 	= $this->fc->idtgl( $start );
		$data['end']   	= $this->fc->idtgl( $end );
		return $data;
	}

	public function get_form2( $rev_id ) {
		$query = $this->dbrevisi->query("Select * From revisi Where rev_id= '$rev_id' ");
		$hasil = $query->row_array();
		return $hasil;
	}

	public function save_form2_proses( ) {
		$rev_id			= $_POST['rev_id']; 
		$this->fc->logRevisi($rev_id, "save_form2_proses - $rev_id");

		$ip	   			= $this->fc->get_client_ip(); 
		$t2_catatan		= str_replace(array("'", "\"", "&quot;"), "", htmlspecialchars($_POST['t2_catatan'] ) );
		$totim			= $_POST['radio-group'];
	

		if ($totim=='1') { $t2 = '2'; $tahap='6'; $rev_status='9'; $next = ', t6_status=9'; } else {$t2='0'; $tahap='2'; $rev_status='0'; $next = ''; } 
				
		$this->dbrevisi->query( "Update revisi Set t2_catatan=\"$t2_catatan\", t2_selesai_tgl=current_timestamp(),t2_selesai_ip='$ip',t2_status='$t2',
							rev_tahap='$tahap', rev_status='$rev_status' $next	Where rev_id='$rev_id' " );
		
		if ($totim=='2') { $this->email_pengembalian_revisi( $rev_id );  }
		return;
	}

	public function email_pengembalian_revisi( $rev_id ) {
		//query_sender_email awal
	
		$query = $this->dbrevisi->query("Select *, kl_email email  From revisi Where rev_id = '$rev_id'");
	 	$hasilemail = $query->result_array();
	 	//print_r($hasilemail); exit;

		 $email = array(); $attach = array();
		 foreach ($hasilemail as $row) if ( $row['email'] ) $email[ $row['email'] ] = $row['kl_dept'];
		//print_r($email); exit;

		//ambil file lampiran
		$query = $this->dbrevisi->query("Select * From revisi Where rev_id='$rev_id'");
		$hasil = $query->row_array();
		$file  = array();

		//query ambil data email
		//$query = $this->dbrevisi->query("Select *, '' nmdept, '' nmunit, nmso, DATE(pus_tgl) From revisi a Left Join t_so b On a.rev_kdso=b.kdso Where rev_id='$rev_id'");
		$query = $this->dbrevisi->query("Select * From revisi Where rev_id='$rev_id'");
		$hasil = $query->row_array(); 

		$dbref = $this->load->database('ref'. $this->session->userdata('thang'), TRUE);
		$query = $dbref->query("Select nmdept From t_dept Where kddept='". $hasil['kl_dept'] ."'");
		$dept  = $query->row_array();

		$query = $dbref->query("Select nmunit From t_unit Where kddept='". $hasil['kl_dept'] ."' And kdunit='". $hasil['kl_unit'] ."'");
		$unit  = $query->row_array();

		$hasil['nmdept'] = $dept['nmdept'];
		$hasil['nmunit'] = $unit['nmunit'];
		//ending ambil data

		$query = $this->db->query("Select * From revisi_redaksi Where tahap = 13 ");
		$hasilbody = $query->row_array();
		$subject = $hasilbody['email_subject'];
		$body    = $hasilbody['email_narasi'];
		$body    = str_replace('[kl_dept]', ucwords(strtolower($hasil['nmdept'])), $body);
		$body    = str_replace('[kl_unit]', ucwords(strtolower($hasil['nmunit'])), $body);
		$body    = str_replace('[kl_pjb_jab]', $hasil['kl_pjb_jab'], $body);
		$body    = str_replace('[kl_surat_tgl]', $this->fc->idtgl($hasil['kl_surat_tgl'],'tglfull'),$body);
		$body    = str_replace('[kl_surat_no]', $hasil['kl_surat_no'], $body);
		$body    = str_replace('[pus_tgl]', $this->fc->idtgl($hasil['pus_tgl'],'tglfull'), $body);
		$body    = str_replace('[rev_id]', $hasil['rev_id'], $body);
		$body    = str_replace('[t2_catatan]', $hasil['t2_catatan'], $body);
		//$body    = str_replace('http://www.rkakldipaonline.kemenkeu.go.id','http://rkakldipa.anggaran.depkeu.go.id/index.php/tracking/'.$hasil['qrcode'], $body);
		
				$attach = array();
				
				$this->fc->send_mail( 'satudja', $email, $subject, $body, $attach ); 
		
	 	return $attach;
	}

	public function save_form6_proses( ) {
		$rev_id			= $_POST['rev_id'];
		$this->fc->logRevisi($rev_id, "save_form6_proses - $rev_id");
		 
		$ip	   			= $this->fc->get_client_ip();
		$t6_catatan		= str_replace(array("'", "\"", "&quot;"), "", htmlspecialchars($_POST['t6_catatan'] ) );
		$totim			= $_POST['radio-group'];


		if ($totim=='1') {
		$this->dbrevisi->query( "Update revisi Set t6_catatan=\"$t6_catatan\", t6_selesai_tgl=current_timestamp(),t6_selesai_ip='$ip',t6_status=2,
						rev_tahap='7', rev_status='9', t7_status='9' Where rev_id='$rev_id'" );
		

		}

		if ($totim=='2') {
		$this->dbrevisi->query( "Update revisi Set t6_catatan=\"$t6_catatan\", t6_selesai_tgl=current_timestamp(),t6_selesai_ip='$ip',t6_status=0,
						 rev_tahap='7', rev_status=9, t7_status='9' Where rev_id='$rev_id'" );
		
		//$this->email_perbaikan_form6( $rev_id );
		}

		if ($totim=='3') {

		$no 			= $_POST['t6_surat_no'];
		$tgl 			= $_POST['t6_surat_tgl'];
		$file 			= $_POST['t6_surat_file'];

		$this->dbrevisi->query( "Update revisi Set t6_catatan=\"$t6_catatan\", t6_proses_tgl=current_timestamp(),t6_proses_ip='$ip',t6_status=3,
						 rev_tahap='6', rev_status=1, t6_surat_no='$no', t6_surat_tgl='$tgl', t6_surat_file='$file'
						Where rev_id='$rev_id'" );
		
		}
		//$this->email_revisi_form6($rev_id);
		return;
	}

	

	public function save_form7_proses( ) {
		$rev_id 		= $_POST['rev_id'];
		$this->fc->logRevisi($rev_id, "save_form7_proses - $rev_id");

		$ip	   			= $this->fc->get_client_ip();
		$t7_sp_no		= $_POST['t7_sp_no']; 
		$t7_sp_tgl		= $_POST['t7_sp_tgl']; 
		$t7_sp_file_u	= $_POST['t7_sp_file']; 
		$rev_ke			= $_POST['rev_ke']; 
		$t7_catatan		= str_replace(array("'", "\"", "&quot;"), "", htmlspecialchars($_POST['t7_catatan'] ) );
		$totim			= $_POST['radio-group'];

		//echo $totim; exit;
		
		if ($totim=='1') { $t7 = '2'; $rev_status='2';  } else {$t7='0'; $rev_ke=''; $rev_status='0';}
		if ($totim=='1') {$this->email_pengesahan_revisi( $rev_id );}
		if ($totim=='2') {$this->email_penolakan_revisi( $rev_id );}  

		
		$this->dbrevisi->query( "Update revisi set t7_status ='$t7', rev_tahap = 7, t7_selesai_tgl=current_timestamp(), t7_selesai_ip='$ip', 
							t7_sp_tgl = '$t7_sp_tgl', t7_sp_no = '$t7_sp_no',t7_sp_file = '$t7_sp_file_u', t7_catatan = \"$t7_catatan\", rev_ke = '$rev_ke', rev_status='$rev_status'  
							where rev_id='$rev_id' " );
		
		return;
	}

	public function email_pengesahan_revisi( $rev_id ) {
		//query_sender_email awal
		$query = $this->dbrevisi->query("Select *, kl_email email  From revisi Where rev_id = '$rev_id'");
	 	$hasilemail = $query->result_array();

		$email = array(); $attach = array();
		foreach ($hasilemail as $row) if ( $row['email'] ) $email[ $row['email'] ] = $row['kl_dept'];

		//ambil file lampiran
		$query = $this->dbrevisi->query("Select * From revisi Where rev_id='$rev_id'");
		$hasil = $query->row_array();
		$file  = $hasil['t7_sp_file'];

		//query ambil data email
		//$query=$this->dbrevisi->query("Select *, '' nmdept, '' nmunit, nmso, DATE(pus_tgl) From revisi a Left Join t_so b On a.rev_kdso=b.kdso Where rev_id='$rev_id'");
		$query = $this->dbrevisi->query("Select * From revisi Where rev_id='$rev_id'");
		$hasil = $query->row_array();
		$hasil = $query->row_array();

		$dbref = $this->load->database('ref'. $this->session->userdata('thang'), TRUE);
		$query = $dbref->query("Select nmdept From t_dept Where kddept='". $hasil['kl_dept'] ."'");
		$dept  = $query->row_array();

		$query = $dbref->query("Select nmunit From t_unit Where kddept='". $hasil['kl_dept'] ."' And kdunit='". $hasil['kl_unit'] ."'");
		$unit  = $query->row_array();

		$hasil['nmdept'] = $dept['nmdept'];
		$hasil['nmunit'] = $unit['nmunit'];
		//ending ambil data

		$query = $this->db->query("Select * From revisi_redaksi Where tahap = 71 ");
		$hasilbody = $query->row_array();
		$subject = $hasilbody['email_subject'];
		$body    = $hasilbody['email_narasi'];
		$body    = str_replace('[kl_dept]', ucwords(strtolower($hasil['nmdept'])), $body);
		$body    = str_replace('[kl_unit]', ucwords(strtolower($hasil['nmunit'])), $body);
		$body    = str_replace('[kl_pjb_jab]', $hasil['kl_pjb_jab'], $body);
		$body    = str_replace('[kl_surat_tgl]', $this->fc->idtgl($hasil['kl_surat_tgl'],'tglfull'),$body);
		$body    = str_replace('[kl_surat_no]', $hasil['kl_surat_no'], $body);
		$body    = str_replace('[pus_tgl]', $this->fc->idtgl($hasil['pus_tgl'],'tglfull'), $body);
		$body    = str_replace('[rev_id]', $hasil['rev_id'], $body);
		$body    = str_replace('[t7_sp_no]', $hasil['t7_sp_no'], $body);
		$body    = str_replace('[t7_sp_tgl]', $this->fc->idtgl($hasil['t7_sp_tgl'],'tglfull'), $body);
		//$body    = str_replace('http://www.rkakldipaonline.kemenkeu.go.id','http://rkakldipa.anggaran.depkeu.go.id/index.php/tracking/'.$hasil['qrcode'], $body);

		$attach  = array( $file => "http://10.242.142.52/files/revisi_SatuDJA/$rev_id/" .'/'. $file );
		if ($file!='') { 
			$this->fc->send_mail( 'satudja', $email, $subject, $body, $attach); 
		} else {
				$attach = array();
				$this->fc->send_mail( 'satudja', $email, $subject, $body, $attach ); 
		}
	 	return $attach;
	}

	public function email_penolakan_revisi( $rev_id ) {

		//query_sender_email awal
		$query = $this->dbrevisi->query("Select *, kl_email email  From revisi Where rev_id = '$rev_id'");
	 	$hasilemail = $query->result_array();

		$email = array(); $attach = array();
		foreach ($hasilemail as $row) if ( $row['email'] ) $email[ $row['email'] ] = $row['kl_dept'];

		//ambil file lampiran
		$query = $this->dbrevisi->query("Select * From revisi Where rev_id='$rev_id'");
		$hasil = $query->row_array();
		$file  = $hasil['t7_sp_file'];

		//query ambil data email
		//$query=$this->dbrevisi->query("Select *, '' nmdept, '' nmunit, nmso, DATE(pus_tgl) From revisi a Left Join t_so b On a.rev_kdso=b.kdso Where rev_id='$rev_id'");
		$query = $this->dbrevisi->query("Select * From revisi Where rev_id='$rev_id'");
		$hasil = $query->row_array();

		$dbref = $this->load->database('ref'. $this->session->userdata('thang'), TRUE);
		$query = $dbref->query("Select nmdept From t_dept Where kddept='". $hasil['kl_dept'] ."'");
		$dept  = $query->row_array();

		$query = $dbref->query("Select nmunit From t_unit Where kddept='". $hasil['kl_dept'] ."' And kdunit='". $hasil['kl_unit'] ."'");
		$unit  = $query->row_array();

		$hasil['nmdept'] = $dept['nmdept'];
		$hasil['nmunit'] = $unit['nmunit'];
		//ending ambil data

		$query = $this->db->query("Select * From revisi_redaksi Where tahap = 79 ");
		$hasilbody = $query->row_array();
		$subject = $hasilbody['email_subject'];
		$body    = $hasilbody['email_narasi'];
		$body    = str_replace('[kl_dept]', ucwords(strtolower($hasil['nmdept'])), $body);
		$body    = str_replace('[kl_unit]', ucwords(strtolower($hasil['nmunit'])), $body);
		$body    = str_replace('[kl_pjb_jab]', $hasil['kl_pjb_jab'], $body);
		$body    = str_replace('[kl_surat_tgl]', $this->fc->idtgl($hasil['kl_surat_tgl'],'tglfull'),$body);
		$body    = str_replace('[kl_surat_no]', $hasil['kl_surat_no'], $body);
		$body    = str_replace('[pus_tgl]', $this->fc->idtgl($hasil['pus_tgl'],'tglfull'), $body);
		$body    = str_replace('[rev_id]', $hasil['rev_id'], $body);
		$body    = str_replace('[t7_sp_no]', $hasil['t7_sp_no'], $body);
		$body    = str_replace('[t7_sp_tgl]', $hasil['t7_sp_tgl'], $body);
		//$body    = str_replace('http://www.rkakldipaonline.kemenkeu.go.id','http://rkakldipa.anggaran.depkeu.go.id/index.php/tracking/revisi/'.$hasil['qrcode'], $body);


		// $subject = "Revisi - Undangan Penelaahan #$rev_id";
		// $body	 = "Telampir Undangan Penelaahan Terkait Revisi No. $rev_id";
		$attach  = array( $file => "http://10.242.142.52/files/revisi_SatuDJA/$rev_id/" .'/'. $file );
		if ($file!='') { 
			$this->fc->send_mail( 'satudja', $email, $subject, $body, $attach); 
		} else {
				$attach = array();
				$this->fc->send_mail( 'satudja', $email, $subject, $body, $attach ); 
		}
	 	return $attach;
	}

	public function email_lengkap($rev_id){
		//query_sender_email awal
		$query = $this->db->query("Select *, kl_email email  From revisi Where rev_id = '$rev_id'");
	 	$hasilemail = $query->result_array();
		$email = array(); $attach = array();
		foreach ($hasilemail as $row) if ( $row['email'] ) $email[ $row['email'] ] = $row['kl_dept'];

		//query ambil data email
		$query=$this->db->query("Select *, '' nmdept, '' nmunit, nmso, DATE(pus_tgl) From revisi a Left Join t_so b On a.rev_kdso=b.kdso Where rev_id='$rev_id'");
		$hasil = $query->row_array();

		$dbref = $this->load->database('ref'. $this->session->userdata('thang'), TRUE);
		$query = $dbref->query("Select nmdept From t_dept Where kddept='". $hasil['kl_dept'] ."'");
		$dept  = $query->row_array();

		$query = $dbref->query("Select nmunit From t_unit Where kddept='". $hasil['kl_dept'] ."' And kdunit='". $hasil['kl_unit'] ."'");
		$unit  = $query->row_array();

		$hasil['nmdept'] = $dept['nmdept'];
		$hasil['nmunit'] = $unit['nmunit'];
		//ending ambil data
		$query = $this->db->query("Select * From revisi_redaksi Where tahap = 50 ");
		$hasilbody = $query->row_array();
		$subject = $hasilbody['email_subject'];
		$body    = $hasilbody['email_narasi'];
		$body    = str_replace('[kl_dept]', ucwords(strtolower($hasil['nmdept'])), $body);
		$body    = str_replace('[kl_unit]', ucwords(strtolower($hasil['nmunit'])), $body);
		$body    = str_replace('[kl_surat_tgl]', $this->fc->idtgl($hasil['kl_surat_tgl'],'tglfull'),$body);
		$body    = str_replace('[kl_surat_no]', $hasil['kl_surat_no'], $body);
		$body    = str_replace('[kl_pjb_jab]', $hasil['kl_pjb_jab'], $body);
		$body    = str_replace('[pus_tgl]', $this->fc->idtgl($hasil['pus_tgl'],'tglfull'), $body);
		$body    = str_replace('[rev_id]', $hasil['rev_id'], $body);
		$body    = str_replace('http://www.rkakldipaonline.kemenkeu.go.id','http://rkakldipa.anggaran.depkeu.go.id/index.php/tracking/revisi/'.$hasil['qrcode'], $body);
		$body    = str_replace('[t5_hasil_tgl]', $this->fc->idtgl($hasil['t5_hasil_tgl'],'tglfull'), $body);
		$this->fc->send_mail( 'revisi', $email, $subject, $body);
	 	return $attach ;
	}

	public function email_perbaikan_form6(  $rev_id ){
		//query_sender_email awal
		$query = $this->db->query("Select *, kl_email email  From revisi Where rev_id = '$rev_id'");
	 	$hasilemail = $query->result_array();

		$email = array();
		foreach ($hasilemail as $row) if ( $row['email'] ) $email[ $row['email'] ] = $row['kl_dept'];

		//ambil file lampiran
		$query = $this->db->query("Select * From revisi Where rev_id='$rev_id'");
		$undfile = $query->row_array();
		$file  = $undfile['t6_surat_file'];

		//query ambil data email
		$query=$this->db->query("Select *, '' nmdept, '' nmunit, nmso, DATE(pus_tgl) From revisi a Left Join t_so b On a.rev_kdso=b.kdso Where rev_id='$rev_id'");
		$hasil = $query->row_array();

		$dbref = $this->load->database('ref'. $this->session->userdata('thang'), TRUE);
		$query = $dbref->query("Select nmdept From t_dept Where kddept='". $hasil['kl_dept'] ."'");
		$dept  = $query->row_array();

		$query = $dbref->query("Select nmunit From t_unit Where kddept='". $hasil['kl_dept'] ."' And kdunit='". $hasil['kl_unit'] ."'");
		$unit  = $query->row_array();

		$hasil['nmdept'] = $dept['nmdept'];
		$hasil['nmunit'] = $unit['nmunit'];
		//ending ambil data
		$query = $this->db->query("Select * From revisi_redaksi Where tahap = 40 ");
		$hasilbody = $query->row_array();
		$subject = $hasilbody['email_subject'];
		$body    = $hasilbody['email_narasi'];
		$body    = str_replace('[kl_dept]', ucwords(strtolower($hasil['nmdept'])), $body);
		$body    = str_replace('[kl_unit]', ucwords(strtolower($hasil['nmunit'])), $body);
		$body    = str_replace('[kl_pjb_jab]', $hasil['kl_pjb_jab'], $body);
		$body    = str_replace('[kl_surat_tgl]', $this->fc->idtgl($hasil['kl_surat_tgl'],'tglfull'),$body);
		$body    = str_replace('[kl_surat_no]', $hasil['kl_surat_no'], $body);
		$body    = str_replace('[pus_tgl]', $this->fc->idtgl($hasil['pus_tgl'],'tglfull'), $body);
		$body    = str_replace('[rev_id]', $hasil['rev_id'], $body);
		$body    = str_replace('[t4_ba_no]', $hasil['t4_ba_no'], $body);
		$body    = str_replace('[t4_ba_tgl]', $hasil['t4_ba_tgl'], $body);
		$body    = str_replace('http://www.rkakldipaonline.kemenkeu.go.id','http://rkakldipa.anggaran.depkeu.go.id/index.php/tracking/revisi/'.$hasil['qrcode'], $body);


		// $subject = "Revisi - Undangan Penelaahan #$rev_id";
		// $body	 = "Telampir Undangan Penelaahan Terkait Revisi No. $rev_id";
		$attach  = array( $file => "http://10.242.142.52/files/revisi/$rev_id/" .'/'. $file );
		$this->fc->send_mail( 'revisi', $email, $subject, $body, $attach);
	 	return $attach;
	}

	

	public function email_revisi_form6($rev_id){
		//query_sender_email awal
		$query = $this->db->query("Select *, kl_email email  From revisi Where rev_id = '$rev_id'");
	 	$hasilemail = $query->result_array();

		$email = array();
		foreach ($hasilemail as $row) if ( $row['email'] ) $email[ $row['email'] ] = $row['kl_dept'];

		$query = $this->db->query("Select * From revisi Where rev_id='$rev_id'");
		$hasil = $query->row_array();
		$file  = $hasil['t6_surat_file'];

		//query ambil data email
		$query=$this->db->query("Select *, '' nmdept, '' nmunit, nmso, DATE(pus_tgl) From revisi a Left Join t_so b On a.rev_kdso=b.kdso Where rev_id='$rev_id'");
		$hasil = $query->row_array();

		$dbref = $this->load->database('ref'. $this->session->userdata('thang'), TRUE);
		$query = $dbref->query("Select nmdept From t_dept Where kddept='". $hasil['kl_dept'] ."'");
		$dept  = $query->row_array();

		$query = $dbref->query("Select nmunit From t_unit Where kddept='". $hasil['kl_dept'] ."' And kdunit='". $hasil['kl_unit'] ."'");
		$unit  = $query->row_array();

		$hasil['nmdept'] = $dept['nmdept'];
		$hasil['nmunit'] = $unit['nmunit'];
		//ending ambil data

		$query = $this->db->query("Select * From revisi_redaksi Where tahap = 40 ");
		$hasilbody = $query->row_array();
		$subject = $hasilbody['email_subject'];
		$body    = $hasilbody['email_narasi'];
		$body    = str_replace('[kl_dept]', ucwords(strtolower($hasil['nmdept'])), $body);
		$body    = str_replace('[kl_unit]', ucwords(strtolower($hasil['nmunit'])), $body);
		$body    = str_replace('[kl_pjb_jab]', $hasil['kl_pjb_jab'], $body);
		$body    = str_replace('[kl_surat_tgl]', $this->fc->idtgl($hasil['kl_surat_tgl'],'tglfull'),$body);
		$body    = str_replace('[kl_surat_no]', $hasil['kl_surat_no'], $body);
		$body    = str_replace('[pus_tgl]', $this->fc->idtgl($hasil['pus_tgl'],'tglfull'), $body);
		$body    = str_replace('[rev_id]', $hasil['rev_id'], $body);
		$body    = str_replace('[t5_hasil_no]', $hasil['t6_surat_no'], $body);
		$body    = str_replace('[t6_catatan]', $hasil['t6_catatan'], $body);
		$body    = str_replace('[t5_hasil_tgl]', $this->fc->idtgl($hasil['t6_surat_tgl'],'tglfull'), $body);
		$body    = str_replace('http://www.rkakldipaonline.kemenkeu.go.id','http://rkakldipa.anggaran.depkeu.go.id/index.php/tracking/revisi/'.$hasil['qrcode'], $body);

		$attach  = array( $file => "http://10.100.159.45/files/revisi/$rev_id/" .'/'. $file );
		if ($file!='') { 
			$this->fc->send_mail( 'revisi', $email, $subject, $body, $attach); 
		} else {
				$this->fc->send_mail( 'revisi', $email, $subject, $body ); 
		}
	 	return $attach;
	}

	function mundur_status($lvl,$rev_id){
		$this->fc->logRevisi($rev_id, "mundur_status - $rev_id");
		
		$sql = " t6_selesai_tgl=null, t6_selesai_ip=null, t6_check_status=null, t6_surat_no=null, t6_surat_tgl=null, t6_surat_file=null, t6_catatan=null, t7_status=null, t7_proses_tgl=null, t7_proses_ip=null, t7_selesai_tgl=null, t7_selesai_ip=null, t7_nd_no=null, t7_nd_tgl=null, t7_nd_file=null, t7_nd_catatan=null, t7_sp_tgl=null, t7_sp_file=null, t7_penetapan_tgl=null, t7_catatan=null  WHERE rev_id='$rev_id'";

		if($lvl == '2') $this->dbrevisi->query("UPDATE revisi SET rev_tahap='2', rev_status='9', t2_status='1', t2_selesai_tgl=null, t2_selesai_ip =null, t2_tolak_no=null,t2_tolak_tgl=null, t2_tolak_hal=null, t2_tolak_file=null, t2_catatan=null, t3_status=null,t3_proses_tgl=null, t3_proses_ip=null, t3_selesai_tgl=null, t3_selesai_ip=null, t3_und_no=null, t3_und_tgl=null, t3_und_file=null, t3_catatan=null, t3_penelaahan_tgl=null, t4_status=null,t4_proses_tgl=null,t4_proses_ip=null, t4_selesai_tgl=null, t4_selesai_ip=null, t4_catatan=null, t4_ba_no=null, t4_ba_tgl=null, t4_ba_file=null, t4_menteri_no=null, t4_menteri_tgl=null, t4_menteri_file=null, t4_perbaikan_status=null, t4_perbaikan_catatan=null, t5_status=null,t5_proses_tgl=null, t5_proses_ip=null, t5_selesai_tgl=null, t5_selesai_ip=null, t5_hasil_status=null, t5_hasil_no=null, t5_hasil_tgl=null, t5_hasil_file=null, t5_hasil_catatan=null, t6_status=null, t6_proses_tgl=null, t6_proses_ip=null,$sql ");

		if($lvl == '3') $this->dbrevisi->query("UPDATE revisi SET rev_tahap='6',rev_status='9', t3_status=null,t3_proses_tgl=null,t3_proses_ip=null, t3_selesai_tgl=null, t3_selesai_ip=null, t3_und_no=null, t3_und_tgl=null, t3_und_file=null, t3_catatan=null, t3_penelaahan_tgl=null, t4_status=null,t4_proses_tgl=null,t4_proses_ip=null, t4_selesai_tgl=null, t4_selesai_ip=null, t4_catatan=null, t4_ba_no=null, t4_ba_tgl=null, t4_ba_file=null, t4_menteri_no=null, t4_menteri_tgl=null, t4_menteri_file=null, t4_perbaikan_status=null, t4_perbaikan_catatan=null, t5_status=null,t5_proses_tgl=null, t5_proses_ip=null, t5_selesai_tgl=null, t5_selesai_ip=null, t5_hasil_status=null, t5_hasil_no=null, t5_hasil_tgl=null, t5_hasil_file=null, t5_hasil_catatan=null, t6_status='1', $sql ");
	}
	
}
