<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// Class of Idub-Libraries
class Fc {

	function MemVar($jns=null, $unit=null ) { 
		$CI =& get_instance();

		if ($jns == null) {
			$CI->thang  = $CI->session->userdata('thang');
			$CI->group  = $CI->session->userdata('idusergroup');
			$CI->whrdept= $CI->session->userdata('whrdept');
			$CI->dept   = $CI->session->userdata('kddept');
			$CI->unit   = $CI->session->userdata('kdunit');

			$CI->dbone  = $CI->load->database("dbone", TRUE);
			$CI->dbdsw  = $CI->load->database("dbdsw", TRUE);
			$CI->dbref  = $CI->load->database("ref$CI->thang",TRUE);
			//$CI->dbnet  = $CI->load->database("telaah$CI->thang", TRUE);
			//$CI->dbfrm  = $CI->load->database("forum$CI->thang", TRUE);
			$CI->dbrvs  = $CI->load->database("revisi$CI->thang", TRUE);
			$CI->dbtmp  = $CI->load->database("temp$CI->thang", TRUE);
		} else {
			if ($unit == '00000') {
				if ($CI->dept == '') $CI->dept = '001';
				if ($CI->unit == '') $CI->unit = '01';
			} else { 
				$CI->dept = substr($unit,0,3);  
				$CI->unit = substr($unit,3,2); 
			}
			$CI->index = "jenis='$jns' AND kddept='$CI->dept' AND kdunit='$CI->unit'";			
		}
	}

// MANIPULASI STRING / ARRAY
	public function GetPosBy($string, $position) {
		$hasil = explode(',', $string);
		return trim($hasil[$position-1]);
	}

	public function ToArr($arr, $idkey) {
		$str = array();
		foreach($arr as $row) $str[ $row[$idkey] ] = $row;
		return $str;
	}

	public function InVar($arr, $kode) {
		$in = "in (";
		foreach ($arr as $row) {
			if (trim($row[$kode])!='') {
			  if ( !strpos($in, $row[$kode]) ) $in .= "'$row[$kode]'";
			}
		}
		$in = str_replace("''", "','", $in) .")";
		return $in;
	}

	function array_index($a, $subkey) {
		foreach($a as $k=>$v) $b[$k] = strtolower($v[$subkey]);
		asort($b);
		foreach($b as $key=>$val) $c[] = $a[$key];
		return $c;
	}



// MANIPULASI DATABASE
	public function Ref_Keys( $table, $string=null ) {
		$attr='keys';  if ($string)  $attr=$string;

		$primekeys = array(
			't_dept' => array('kode'=>'kddept', 'nama'=>'nmdept'),
			't_unit' => array('kode'=>'kddept,kdunit', 'nama'=>'nmunit'),
			't_program' => array('kode'=>'kddept,kdunit,kdprogram', 'nama'=>'nmprogram'),

			't_user' => array('kode'=>'iduser', 'nama'=>'nmuser'),
			't_rapat_ruang' => array('kode'=>'idruang', 'nama'=>'nmruang'),
			'd_paket' => array('kode'=>'idpaket', 'nama'=>''),
			'd_forum_room' => array('kode'=>'idparent,idchild', 'nama'=>'nmroom'),

			't_esl3' => array('kode'=>'kode', 'nama'=>'uraian')
		);

		switch ($attr) {
	    case "keys": $str = $primekeys[ $table ]['kode']; break;
	    case "kode": $str = "concat(". str_replace(",", ",'.',", $primekeys[ $table ]['kode']) .")"; break;
	    case "nama": $str = $primekeys[ $table ]['nama']; break;
	    default: $str = "";
		}

		return $str;
	}

	public function Download_Data( $db, $table, $string=null ) {
		$where ='';  if ($string)  $where="$string";
		$artbl = explode(',', $table);

		for ($i=0; $i<count($artbl); $i++) {
			$kdkey = $this->Ref_Keys( $artbl[$i], 'kode' );
			$query = $db->query("Select *, $kdkey kdkey From $artbl[$i] $where");

			if (count($artbl)==1) {
				$hasil = $this->ToArr( $query->result_array(), 'kdkey' );
			} else {
				$hasil[ $artbl[$i] ] = $this->ToArr( $query->result_array(), 'kdkey' );
			}
		}
		return $hasil;
	}

	public function Ref_Join( $db, $arr_main, $table, $key ) {
		$invar = $this->InVar( $arr_main, $key );
		$kode  = $this->Ref_Keys( $table, 'kode' );
		$nama  = $this->Ref_Keys( $table, 'nama' );

		$query  = $db->query("Select *, $kode kdkey From $table where $kode $invar");
		$t_ref  = $this->ToArr( $query->result_array(), 'kdkey');
		return $this->Left_Join( $arr_main, $t_ref, "$key, $nama=$nama" );
	}

	public function Left_Join( $arr_main, $arr_sub, $string ) {
		$arr_colomn = explode(',', $string);
		foreach($arr_main as $key=>$val) {
			$link = $val[ $arr_colomn[0] ];
			if ( count($arr_colomn)>1 ) {
				list($nm1, $nm2) = explode("=", trim($arr_colomn[1]));
				if ( array_key_exists($link, $arr_sub) )  $arr_main[ $key ][ $nm1 ] = $arr_sub[ $link ][ $nm2 ];
			}
			if ( count($arr_colomn)>2 ) {
				list($nm1, $nm2) = explode("=", trim($arr_colomn[2]));
				if ( array_key_exists($link, $arr_sub) )  $arr_main[ $key ][ $nm1 ] = $arr_sub[ $link ][ $nm2 ];
			}
			if ( count($arr_colomn)>3 ) {
				list($nm1, $nm2) = explode("=", trim($arr_colomn[3]));
				if ( array_key_exists($link, $arr_sub) )  $arr_main[ $key ][ $nm1 ] = $arr_sub[ $link ][ $nm2 ];
			}
			if ( count($arr_colomn)>4 ) {
				list($nm1, $nm2) = explode("=", trim($arr_colomn[4]));
				if ( array_key_exists($link, $arr_sub) )  $arr_main[ $key ][ $nm1 ] = $arr_sub[ $link ][ $nm2 ];
			}
			if ( count($arr_colomn)>5 ) {
				list($nm1, $nm2) = explode("=", trim($arr_colomn[5]));
				if ( array_key_exists($link, $arr_sub) )  $arr_main[ $key ][ $nm1 ] = $arr_sub[ $link ][ $nm2 ];
			}
		}
		return $arr_main;
	}



// MANIPULASI CLASS
	public function tahun($string, $param, $url=null) {
		$tahun = array('tahun','2013','2014','2015','2016','2017');

		if ($string=='array') {
			return $tahun;
		} else if ($string=='combo') {
			$str = '';
			for ($i=0; $i<=count($tahun); $i++) {
				$val  = $i;
				if ($tahun[$i]==$param->tahun) {$sele=" selected=\"selected\"";} else {$sele="";}
				$str .= "<option value=\"$tahun[$i]\" $sele> $tahun[$i] </option>";
			}
			return $str;
		} else if ($string=='dropdown') {
			$str = "<div class=\"dropdown\">
					<button class=\"btn btn-default dropdown-toggle\" type=\"button\" id=\"tahun\" value=\"$param->tahun\" data-toggle=\"dropdown\">$param->tahun
					<span class=\"caret\"></span></button>
					<ul class=\"dropdown-menu\" role=\"menu\" aria-labelledby=\"tahun\">
				   ";
			for ($i=0; $i<=count($tahun); $i++) {
				$site = str_replace("/$param->tahun/", "/$tahun[$i]/", $url);
				$str .= "<li role=\"presentation\"><a role=\"menuitem\" tabindex=\"-1\" href=\"$site\"> $tahun[$i] </a></li>";
			}
			$str .="</ul>
					</div>
				   ";
			return $str;
		} else if ($string=='readonly') {
			$nil = str_replace("'", "", $param->tahun);
			$str = "<button class=\"btn btn-default\" type=\"button\">$nil</button>";
			return $str;
		}
		return;
	}

	public function bulan($string, $param, $url=null) {
		$bulan = array('bulan', 'Januari', 'Pebruari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'Nopember', 'Desember');

		if ($string=='combo') {
			$str = '';
			for ($i=1; $i<=12; $i++) {
				$val = sprintf('%02d', $i);
				if ($i==$param->bulan) {$sele=" selected=\"selected\"";} else {$sele="";}
				$str .= "<option value=\"$val\"$sele> $bulan[$i] </option>";
			}
			return $str;
		} else if ($string=='dropdown') {
			$nil = $bulan[ (int)$param->bulan ];
			$str = "<div class=\"dropdown\">
					<button class=\"btn btn-default dropdown-toggle\" type=\"button\" id=\"bulan\" value=\"$param->bulan\" data-toggle=\"dropdown\">$nil
					<span class=\"caret\"></span></button>
					<ul class=\"dropdown-menu\" role=\"menu\" aria-labelledby=\"bulan\">
				   ";
			for ($i=1; $i<=12; $i++) {
				$site = str_replace("/$param->bulan", "/".sprintf('%02d', $i), $url);
				$str .= "<li role=\"presentation\"><a role=\"menuitem\" tabindex=\"-1\" href=\"$site\"> $bulan[$i] </a></li>";
			}
			$str .="</ul>
					</div>
				   ";
			return $str;
		} else if ($string=='readonly') {
			$nil = str_replace("'", "", $param->bulan);
			$nil = $bulan[ (int)$nil ];
			$str = "<button class=\"btn btn-default\" type=\"button\">$nil</button>";
			return $str;
		}
		return;
	}

	public function idtgl($tgl, $str=null) {
		$tgl = strtotime($tgl);
		if ( date('d-m-Y', $tgl)=='01-01-1970' ) return '&nbsp;';

		if ($str==null) {
				$var = date('d-m-Y', $tgl);
				if ($var=='01-01-1970') $var='&nbsp;';
		}

		if ($str=='tgljam') {
				$bulan = array('01'=>'Jan','02'=>'Feb','03'=>'Mar','04'=>'Apr','05'=>'Mei','06'=>'Jun','07'=>'Jul','08'=>'Agu','09'=>'Sep','10'=>'Okt','11'=>'Nov','12'=>'Des');
				$var   = date('d', $tgl) .' '. $bulan[date('m', $tgl)] .' '. date('Y', $tgl) .' '. date('H:i', $tgl) ;
		}

		if ($str=='hari' || $str=='full') {
				$hari  = array('Sunday'=>'Minggu','Monday'=>'Senin','Tuesday'=>'Selasa','Wednesday'=>'Rabu','Thursday'=>'Kamis','Friday'=>'Jum\'at','Saturday'=>'Sabtu');
				$bulan = array('01'=>'Januari','02'=>'Februari','03'=>'Maret','04'=>'April','05'=>'Mei','06'=>'Juni','07'=>'Juli','08'=>'Agustus','09'=>'September','10'=>'Oktober','11'=>'November','12'=>'Desember');
				$var   = $hari[date('l', $tgl)] .', '. date('d', $tgl) .' '. $bulan[date('m', $tgl)] .' '. date('Y', $tgl);
				if ($str=='full') $var .= ' '. date('H:i', $tgl) . ' WIB';
		}

		if ($str=='hr') {
				$hari  = array('Sunday'=>'Minggu','Monday'=>'Senin','Tuesday'=>'Selasa','Wednesday'=>'Rabu','Thursday'=>'Kamis','Friday'=>'Jum\'at','Saturday'=>'Sabtu');
				$bulan = array('01'=>'Jan','02'=>'Feb','03'=>'Mar','04'=>'Apr','05'=>'Mei','06'=>'Jun','07'=>'Jul','08'=>'Agu','09'=>'Sep','10'=>'Okt','11'=>'Nov','12'=>'Des');
				$var   = $hari[date('l', $tgl)] .', '. date('d', $tgl) .' '. $bulan[date('m', $tgl)] .' '. date('Y', $tgl);
		}

		if ($str=='hr_tgl_bln') {
				$hari  = array('Sunday'=>'Minggu','Monday'=>'Senin','Tuesday'=>'Selasa','Wednesday'=>'Rabu','Thursday'=>'Kamis','Friday'=>'Jum\'at','Saturday'=>'Sabtu');
				$bulan = array('01'=>'Jan','02'=>'Feb','03'=>'Mar','04'=>'Apr','05'=>'Mei','06'=>'Jun','07'=>'Jul','08'=>'Agu','09'=>'Sep','10'=>'Okt','11'=>'Nov','12'=>'Des');
				$var   = $hari[date('l', $tgl)] .', '. date('d', $tgl) .' '. $bulan[date('m', $tgl)];
		}

		if ($str=='mig') {
				$hari  = array('Sunday'=>'Minggu','Monday'=>'Senin','Tuesday'=>'Selasa','Wednesday'=>'Rabu','Thursday'=>'Kamis','Friday'=>'Jum\'at','Saturday'=>'Sabtu');
				$var   = $hari[date('l', $tgl)];
		}
		if ($str=='tgl') {
				$bulan = array('01'=>'Jan','02'=>'Feb','03'=>'Mar','04'=>'Apr','05'=>'Mei','06'=>'Jun','07'=>'Jul','08'=>'Agu','09'=>'Sep','10'=>'Okt','11'=>'Nov','12'=>'Des');
				$var   = date('d', $tgl) .' '. $bulan[date('m', $tgl)] .' '. date('Y', $tgl) ;
		}
		if ($str=='tglfull') {
				$bulan = array('01'=>'Januari','02'=>'Februari','03'=>'Maret','04'=>'April','05'=>'Mei','06'=>'Juni','07'=>'Juli','08'=>'Agustus','09'=>'September','10'=>'Oktober','11'=>'November','12'=>'Desember');
				$var   = date('d', $tgl) .' '. $bulan[date('m', $tgl)] .' '. date('Y', $tgl) ;
		}
		if ($str=='jam') {
				$var   = date('H:i', $tgl) ;
		}
		return $var;
	}

	function ustgl($tgl, $str=null) {
		if ( $tgl=='&nbsp;' ) return '&nbsp;';

		if ($str==null) {
				$arr = explode('-', $tgl);
				$var = $arr[2].'-'. $arr[1] .'-'.$arr[0] ;
		}
		if ($str=='hari' or $str=='full') {
				$bulan = array('Januari'=>'01','Februari'=>'02','Maret'=>'03','April'=>'04','Mei'=>'05','Juni'=>'06','Juli'=>'07','Agustus'=>'08','September'=>'09','Oktober'=>'10','November'=>'11','Desember'=>'12');
				$arr = explode(' ', $tgl);
				$var = $arr[3].'-'.$bulan[ $arr[2] ].'-'.$arr[1] ;
				if ($str=='full') $var .= ' '. $arr[6] .':00 ';
		}
		if ($str=='tgl') {
				$bulan = array('Jan'=>'01','Feb'=>'02','Mar'=>'03','Apr'=>'04','Mei'=>'05','Jun'=>'06','Jul'=>'07','Agu'=>'08','Sep'=>'09','Okt'=>'10','Nov'=>'11','Des'=>'12');
				$arr = explode(' ', $tgl);
				$var = $arr[2].'-'.$bulan[ $arr[1] ].'-'.$arr[0] ;
		}
		return $var;
	}

	function browse( $arr ) {
		if (count($arr) > 0) {
			$hasil  = "<table style='font-size: 80%; border: 1px solid grey;' cellpadding='0px' cellspacing='0px'>";
			$hasil .= "<thead><tr><th style='border: 1px solid grey'>". implode("</th><th style='border: 1px solid grey'>", array_keys(current($arr))) ."</th></tr></thead><tbody>";

			foreach ($arr as $row){
				array_map('htmlentities', $row);
				$hasil .= "<tr><td style='border: 1px solid grey'>". implode("</td><td style='border: 1px solid grey'>", $row) ."</td></tr>";
			}

			$hasil .= "</tbody></table>";
		}
		echo $hasil;
		return;
	}

	function read_more( $str ) {
		$arr = explode('<p>', $str);
		if ( count($arr)<3) {
			echo "$str";
		} else {
			$rand = chr(rand(97,122)) . chr(rand(97,122)) . rand();
			$satu = trim( $arr[1] );
			$dua  = str_replace($satu, '', $str);
			$dua  ="
				<div id='hidden_$rand' style='display: none;'> $dua </div>
				<button class='btn btn-sm' style='padding:2px;background-color:transparent;color:#3C8DBC' onclick=ReadMore('hidden_$rand'); return false;'> <em><span id='show_$rand' class='small'>Read More</span></em> </button>";

			echo "$satu $dua";
		}
		return;
	}

	function read_excel( $file, $sheet ) {
		//load Excel Library
		// $this->load->library('excel');

		//Baca file excel dari folder file
		$objPHPExcel = PHPExcel_IOFactory::load( $file );

		//Ambil nilai Cell secara keseluruhan dan nilai Sheet 0 (Default)
		$objWorksheet = $objPHPExcel->setActiveSheetIndex( $sheet );
		$cell_collection = $objPHPExcel->getActiveSheet()->getCellCollection();

		//Extract ke format array
		foreach ($cell_collection as $cell) {
		    $column = $objPHPExcel->getActiveSheet()->getCell($cell)->getColumn();
		    $row = $objPHPExcel->getActiveSheet()->getCell($cell)->getRow();
		    $data_value = $objPHPExcel->getActiveSheet()->getCell($cell)->getCalculatedValue();

		    // Kolom Header diberi space 2 baris
		    if ($row <= 2) {
		        $header[$row][$column] = $data_value;
		    } else {
		        $values[$row-2][$column] = $data_value;
		    }
		}

		//nilai balik dalam format array
		$header = $header;
		$values = $values;

		return $values;
	}

	function pagination( $base_url, $total_rows, $per_page, $uri_segment ) {
		$config['total_rows'] = $total_rows;
		$config['base_url'] = $base_url;
		$config['per_page'] = $per_page;
		$config['uri_segment'] = $uri_segment;

		$config['full_tag_open'] = '<ul class="pagination">';
		$config['full_tag_close'] = '</ul>';

		$config['first_link'] = 'First';
		$config['first_tag_open'] = '<li class="prev page">';
		$config['first_tag_close'] = '</li>';

		$config['last_link'] = 'Last';
		$config['last_tag_open'] = '<li class="next page">';
		$config['last_tag_close'] = '</li>';

		$config['next_link'] = 'Next';
		$config['next_tag_open'] = '<li class="next page">';
		$config['next_tag_close'] = '</li>';

		$config['prev_link'] = 'Previous';
		$config['prev_tag_open'] = '<li class="prev page">';
		$config['prev_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li><a href=""  class="active">';
		$config['cur_tag_close'] = '</a></li>';

		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="active"><a href="#">';
	    $config['cur_tag_close'] = '</a></li>';
		return $config;
	}

	public function getUserIP(){
	 //   	ob_start(); // Turn on output buffering
		// system(‘ipconfig /all’); //Execute external program to display output
		// $mycom=ob_get_contents(); // Capture the output into a variable
		// ob_clean(); // Clean (erase) the output buffer
		// $findme = “Physical”;
		// $pmac = strpos($mycom, $findme); // Find the position of Physical text
		// $mac=substr($mycom,($pmac+36),17); // Get Physical Address
		// return $mac;
   		$ip=$_SERVER['REMOTE_ADDR'];
		$mac_string = shell_exec("arp -a $ip");
		$mac_array = explode(" ",$mac_string);
		$mac = $mac_array[3];
		return $mac;
	}

	function send_mail( $type, $address, $subject, $body, $attach ) {
		require_once APPPATH.'third_party/phpmailer/PHPMailerAutoload.php';
		$mail = new PHPMailer();
		// $mail->SMTPDebug  = 2;
		// $mail->Debugoutput = 'html';

        // set smtp
        $sender = array();
        $sender['stol']	  = array('address'=>'stdja@kemenkeu.go.id', 'name'=>'Surat Tugas Online DJA', 'user'=>'kemenkeu/stdja', 'pass'=>'Semangat1%');
        $sender['revisi'] = array('address'=>'revisidja@kemenkeu.go.id', 'name'=>'Revisi DJA', 'user'=>'kemenkeu/revisidja', 'pass'=>'Integritas2000%');
        $sender['satudja'] = array('address'=>'satudja@kemenkeu.go.id', 'name'=>'Satu DJA', 'user'=>'kemenkeu/satudja', 'pass'=>'Halal1000%');

        $smtp = $sender[ $type ];

        $mail->isSMTP();
        $mail->Host = 'smtp.kemenkeu.go.id';
        $mail->Port = 25;
        $mail->SMTPAuth = true;
        $mail->Username = $smtp['user'];
        $mail->Password = $smtp['pass'];
        $mail->WordWrap = 50;

        // set email content
        $mail->setFrom($smtp['address'], $smtp['name']);
        foreach ($address as $email => $name) $mail->addAddress($email, $name);
        $mail->IsHTML(true);
        $mail->Subject = $subject;
        $mail->Body = $body;
        if (isset($attach)) {
        	foreach ($attach as $name => $tmp) $mail->AddStringAttachment(file_get_contents("$tmp"), "$name", 'base64', 'application/pdf');
        }
        $mail->send();
	}

	function check_akses( $cari=null ) {
		$CI =& get_instance();
        if (! $CI->session->userdata('isLoggedIn') ) redirect("login/show_login");

		$whr   = "idusergroup in ('". str_replace(';', "','", $CI->session->userdata('idusergroup')) ."')";
		$query = $CI->db->query("Select menu From t_user_group Where $whr");

		$arr = array();
		foreach ($query->result_array() as $row) {
			$str = explode(';', $row['menu']);
			for ($i=0; $i<count($str); $i++) $arr[ substr($str[$i],0,6) ] = substr($str[$i],0,6);
		}

		$whr = "('". implode("','", $arr) ."')";
		$query = $CI->db->query("Select trim(link) link From t_menu Where idmenu in $whr");
		$menu  = $this->ToArr($query->result_array(), 'link');

		if (array_key_exists($cari, $menu)) return TRUE;
		else return FALSE;
	}

	function ucAddress($str) {
	    $str = ucwords(strtolower($str));
	    $str = mb_eregi_replace('\b([0-9]{1,4}[a-z]{1,2})\b', "strtoupper('\\1')", $str, 'e');
	    $str = mb_eregi_replace('\bM{0,4}(CM|CD|D?C{0,3})(XC|XL|L?X{0,3})(IX|IV|V?I{0,3})\b', "strtoupper('\\0')", $str, 'e');

	    return $str;
	}

	function log( $aktifitas ) {
		$CI =& get_instance();
		  if (! $CI->session->userdata('isLoggedIn') ) redirect("login/show_login");

		  $iduser = $CI->session->userdata('iduser');
		  $nmuser = $CI->session->userdata('nmuser');
		  $lokasi = $this->get_client_ip();
		  $CI->db->query("Insert Into t_user_aktifitas (iduser, nmuser, waktu, lokasi, aktifitas) Values ('$iduser', '$nmuser', current_timestamp(), '$lokasi', '$aktifitas')");
	}

	function logRevisi($rev_id, $aktifitas){
		$CI =& get_instance();  
		$CI->thang  = $CI->session->userdata('thang');
		$CI->dbrvs  = $CI->load->database("revisi$CI->thang", TRUE);

		if (! $CI->session->userdata('isLoggedIn') ) redirect("login/show_login"); 

		$iduser = $CI->session->userdata('iduser');
		$nmuser = $CI->session->userdata('nmuser');
		$lokasi = $this->get_client_ip();
		$CI->dbrvs->query("Insert Into revisi_log (iduser, nmuser, waktu, lokasi, rev_id, aktifitas) Values ('$iduser', '$nmuser', current_timestamp(), '$lokasi','$rev_id' ,'$aktifitas')");
	}

	function get_client_ip() {
		 $ipaddress = '';
		 if (isset($_SERVER['HTTP_CLIENT_IP']))
			  $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
		 else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
			  $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
		 else if(isset($_SERVER['HTTP_X_FORWARDED']))
			  $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
		 else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
			  $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
		 else if(isset($_SERVER['HTTP_FORWARDED']))
			  $ipaddress = $_SERVER['HTTP_FORWARDED'];
		 else if(isset($_SERVER['REMOTE_ADDR']))
			  $ipaddress = $_SERVER['REMOTE_ADDR'];
		 else
			  $ipaddress = 'UNKNOWN';
		 return $ipaddress;
	 }

	 function milea_absen_net($nip, $start, $end, $status, $idst) {
		$CI =& get_instance();
		$milea = $CI->load->database("milea", TRUE);

		$qry = $milea->query("SELECT nip18, iduser FROM pegawai_iduser WHERE nip18 IN ('". implode("','", $nip) ."')");
		$usr = $this->ToArr($qry->result_array(), 'nip18');

		$qry = $CI->db->query("SELECT * FROM st_peg_final WHERE idst='$idst'");
		$arr = $this->ToArr($qry->result_array(),'nip');

		$qry = $milea->query("SELECT thang, tgl, kerja FROM t_harikerja WHERE kerja='1' AND thang>='2018' ");
		$krj = $this->ToArr($qry->result_array(), 'tgl');


		foreach ($nip as $row){
			if(array_key_exists($row, $usr)){
				$id = $usr[$row]['iduser'];

				if(array_key_exists($row, $arr)) {
					$sta = new DateTime ($arr[$row]['tglawal']);
					$end = new DateTime ($arr[$row]['tglakhir']);

					for ($i = $sta; $i <= $end; $i->modify('+1 day')) {
						$tgl = $i->format("Y-m-d");

						if(trim( $status )=='DL') { 
							$milea->query("REPLACE INTO absen_net (tgl,nip,iduser,tgldatang,tglpulang,status,sumber) VALUES ('$tgl','$row','$id','$tgl 00:00:00','$tgl 00:00:00','$status','Manual') "); 
						}

						if(array_key_exists($tgl, $krj) AND trim($status) !='DL' ) {
							if     ($status=='RDK'){ $milea->query(" UPDATE absen_net SET sumber='Manual', lembur='0' ,rdk='1' WHERE nip='$row' AND tgl='$tgl' "); } 
							elseif ($status=='HN') { }
							else   {$milea->query("REPLACE INTO absen_net (tgl,nip,iduser,tgldatang,tglpulang,status,sumber) VALUES ('$tgl','$row','$id','$tgl 00:00:00','$tgl 00:00:00','$status','Manual')");}
						}	
					}
				}
			}
		}
		// exit();
	}

	function milea_dsw($nip, $start, $end, $status) {
		$CI =& get_instance();
		$milea = $CI->load->database("milea", TRUE);
		$start = new DateTime( $start );
		$end   = new DateTime( $end );

		$qry 	 = $milea->query("SELECT nip18, iduser FROM pegawai_iduser WHERE nip18='$nip'");
		$usr 	 = $this->ToArr($qry->result_array(), 'nip18');
		$iduser  = $qry->row_array();
		$id 	 = $iduser['iduser'];

		for ($i = $start; $i <= $end; $i->modify('+1 day')) {
				if (array_key_exists($nip, $usr)) {
					$tgl = $i->format("Y-m-d");
					$milea->query("REPLACE INTO absen_net (tgl,iduser,nip,tgldatang,tglpulang,status,sumber) VALUES ('$tgl','$id','$nip',
						'$tgl 00:00:00','$tgl 00:00:00','$status','Manual')");
				}
		}
	}
}
