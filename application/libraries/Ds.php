<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ds { 
	function __construct() {
		$this->CI = &get_instance();
	}

	// Digital Stamp
		function create_ds($source, $string) {
			if ($source == 'mysql') {
				$d_outp = $this->read_mysql($string, 'd_output');
				$d_item = $this->read_mysql($string, 'd_item'); } 
			else if ($source == 'mysql_net') {
				$d_outp = $this->read_mysql($string, 'net_output');
				$d_item = $this->read_mysql($string, 'net_item'); } 
			else {
				$d_outp = $this->read_adk($string, 'd_output');
				$d_item = $this->read_adk($string, 'd_item'); } 

			$ds = $this->hashing($d_outp, $d_item);
			return $ds; }

		function read_mysql($kdsatker, $tbl) {
			$whr = "Where kdsatker='$kdsatker'";
			$key = "thang,kdjendok,kdsatker,kddept,kdunit,kdprogram,kdgiat,kdoutput,kdlokasi,kdkabkota,kddekon";
			$sql = "Select $key,kdkppn,kdbeban,kdjnsban,kdctarik,register,kdakun,jumlah,paguphln,pagurmp,pagurkp,blokirphln,blokirrmp,blokirrkp,rphblokir From $tbl $whr";
			
			if ($tbl == 'd_output' or $tbl == 'net_output') $sql = "Select $key, Round(vol,0) vol From $tbl $whr";
			$query = $this->CI->db->query($sql);
			return $query->result_array(); }

		function read_adk($adk, $tbl) {
			$arr  = explode('.', $adk);
			$tag  = str_replace('_', '', substr($arr[0],4,17));

			$file = "files/adk_tor/". $arr[0] ."/$tbl$tag.xml";
	 		$xml  = simplexml_load_file($file, "SimpleXMLElement", LIBXML_NOCDATA);
			$json = json_encode($xml);
	 		$arr  = json_decode($json, TRUE);

			if ( !$arr ) return array();
			foreach (current($arr) as $row) { $assoc = is_array($row); break; }
			if ( $assoc==1 ) $dat = current($arr); else return array();
			return $dat; }

		function hashing($c_outp, $c_item, $step=null) {
			$d_outp = array();
			foreach ($c_outp as $row) {
				$kdkey = $row['thang'].'.'.$row['kdjendok'].'.'.$row['kdsatker'].'.'.$row['kddept'].'.'.$row['kdunit'].'.'.$row['kdprogram'].'.'.$row['kdgiat'].'.'.$row['kdoutput'].'.'.$row['kdlokasi'].'.'.$row['kdkabkota'].'.'.$row['kddekon'];
				array_push($d_outp, array('kdkey'=>$kdkey, 'thang'=>$row['thang'], 'kdjendok'=>$row['kdjendok'], 'kdsatker'=>$row['kdsatker'], 'kddept'=>$row['kddept'], 'kdunit'=>$row['kdunit'], 'kdprogram'=>$row['kdprogram'], 'kdgiat'=>$row['kdgiat'], 'kdoutput'=>$row['kdoutput'], 'kdlokasi'=>$row['kdlokasi'], 'kdkabkota'=>$row['kdkabkota'], 'kddekon'=>$row['kddekon'], 'kdkppn'=>'***', 'kdbeban'=>'*', 'kdjnsban'=>'*', 'kdctarik'=>'*', 'register'=>str_repeat('*',8), 'kdjnsbel'=>'**', 'kdkbkpk'=>'***', 'kdakun'=>'******', 'jumlah'=>0, 'paguphln'=>0, 'pagurmp'=>0, 'pagurkp'=>0, 'blokir'=>0, 'rphpdpt'=>0, 'vol'=>$row['vol'])); }

			$d_item = array();
			foreach ($c_item as $row) {
				if (is_array($row['register']) or $row['register'] == '') $register = str_repeat('*',8);  else $register = $row['register'];
				$kdkey = $row['thang'].'.'.$row['kdjendok'].'.'.$row['kdsatker'].'.'.$row['kddept'].'.'.$row['kdunit'].'.'.$row['kdprogram'].'.'.$row['kdgiat'].'.'.$row['kdoutput'].'.'.$row['kdlokasi'].'.'.$row['kdkabkota'].'.'.$row['kddekon'];
				array_push($d_item, array('kdkey'=>$kdkey, 'thang'=>$row['thang'], 'kdjendok'=>$row['kdjendok'], 'kdsatker'=>$row['kdsatker'], 'kddept'=>$row['kddept'], 'kdunit'=>$row['kdunit'], 'kdprogram'=>$row['kdprogram'], 'kdgiat'=>$row['kdgiat'], 'kdoutput'=>$row['kdoutput'], 'kdlokasi'=>$row['kdlokasi'], 'kdkabkota'=>$row['kdkabkota'], 'kddekon'=>$row['kddekon'], 'kdkppn'=>$row['kdkppn'], 'kdbeban'=>$row['kdbeban'], 'kdjnsban'=>$row['kdjnsban'], 'kdctarik'=>$row['kdctarik'], 'register'=>$register, 'kdjnsbel'=>substr($row['kdakun'],0,2), 'kdkbkpk'=>'000', 'kdakun'=>'000000', 'jumlah'=>$row['jumlah'], 'paguphln'=>$row['paguphln'], 'pagurmp'=>$row['pagurmp'], 'pagurkp'=>$row['pagurkp'], 'blokir'=>$row['blokirphln']+$row['blokirrmp']+$row['blokirrkp']+$row['rphblokir'], 'rphpdpt'=>0, 'vol'=>0)); }

			$d_summ = $this->step1($d_outp, $d_item); if ($step == '1') return $d_summ;
			$d_summ = $this->step2($d_summ);
			$d_summ = $this->step3($d_summ);
			$d_summ = $this->step4($d_summ);
			$d_summ = substr($d_summ,0,4) .' '. substr($d_summ,4,4) .' '. substr($d_summ,8,4) .' '. substr($d_summ,12,4);
			return $d_summ; }	

		function step1($d_outp, $d_item) {
			$hasil = $this->CI->fc->ToArr($d_outp, 'kdkey');
			foreach ($d_item as $row) {
				$row['kdkey'] .= $row['kdkppn'].'.'. $row['kdbeban'].'.'. $row['kdjnsban'].'.'. $row['kdctarik'].'.'. $row['register'].'.'.$row['kdjnsbel'];
				$kdkey = $row['kdkey'];
				if (array_key_exists($kdkey, $hasil)) {
					$hasil[$kdkey]['jumlah']  += $row['jumlah'];
					$hasil[$kdkey]['paguphln']+= $row['paguphln'];
					$hasil[$kdkey]['pagurmp'] += $row['pagurmp'];
					$hasil[$kdkey]['pagurkp'] += $row['pagurkp'];
					$hasil[$kdkey]['blokir']  += $row['blokir']; } 
				else  $hasil[$kdkey] = $row; }

			$hasil = $this->CI->fc->array_index($hasil, 'kdkey');
			foreach ($hasil as $key=>$row) {
				$var = $row['thang'] . $row['kdjendok'] . $row['kdsatker'] . $row['kddept'] . $row['kdunit'] . $row['kdprogram'] . $row['kdgiat'] . $row['kdoutput'] . $row['kdlokasi'] . $row['kdkabkota'] . $row['kddekon'] . $row['kdkppn'] . $row['kdbeban'] . $row['kdjnsban'] . $row['kdctarik'] . $row['register'] . $row['kdjnsbel'] . $row['kdkbkpk'] . $row['kdakun'] . $row['jumlah'] . $row['paguphln'] . $row['pagurmp'] . $row['pagurkp'] . $row['blokir'] . $row['rphpdpt'] . $row['vol'];
				$var = str_replace('*', ' ', $var);
				$hasil[$key]['plaintext'] = $var;
				$hasil[$key]['md5'] = strtoupper( md5($var) ); }
			return $hasil; }

		function step2( $array ) {
			$hasil = array();
			foreach ($array as $row) $hasil[ $row['md5'] ]['md5'] = $row['md5'];
			return $this->CI->fc->array_index($hasil, 'md5'); }

		function step3_ok( $array ) {
			$hasil = '';  $hit = 0;  $var = '';  $str = ''; $fib = '';
			foreach ($array as $row) {
				$hit++;
				$var .= $row['md5'];
				if ($hit >= 50) {
					if ($str == '')  $str = strtoupper( md5($var) );
					else {
						$str = strtoupper(md5($str)) . strtoupper(md5($var));
						$str = strtoupper(md5($str)); }
					$hit = 0; } }

			if (count($array) > 50) {
				if (count($array) > 100) $str = $str . strtoupper( md5($var) );
				else  $str = strtoupper(md5($str)) . strtoupper( md5($var) );
				$str = strtoupper( md5($str) ); }
			else {
		        $str  = strtoupper( md5($var) );
		        $str  = strtoupper( md5($str) ); }
			$hasil = $str;
			return $hasil; }

		function step3( $array ) {
			// echo '<pre>'; print_r($array); exit;
			$dx = $stp = 0;
			$dsCode = $dsHash = '';
			for ($da=0; $da<count($array); $da+=50) { 
				$dx = 0;
				for ($ds=$da; $ds<count($array); $ds++) {
					if ($dx > 49) break;
					$dsCode .= $array[$ds]['md5'];
					$dx++;
				}
				$dsHash .= strtoupper( md5($dsCode) );
				// echo "<br>Step 3_$stp : $dsHash <br>"; 
				$dsHash  = strtoupper( md5($dsHash) );
				// echo "<br>Step 4_$stp : $dsHash <br>"; 
			}
			return $dsHash;
		}

		function step4( $var ) {
			$var = str_replace('a', '0', $var); $var = str_replace('b', '0', $var); $var = str_replace('c', '0', $var);
			$var = str_replace('d', '0', $var); $var = str_replace('e', '0', $var); $var = str_replace('f', '0', $var);

			$hasil = '';
			for ($i=0; $i<strlen($var); $i=$i+2) { 
				$nil = substr($var,$i,1) + substr($var,$i+1,1);
				if ($nil > 9) $nil = $nil - 9;
				$hasil .= $nil; }

			return $hasil; 
		}

	// Browse & SQL Assist
		function browse($array, $set=null) {
			// Declare Variabel
				$this->CI =& get_instance();
				if (! is_array($array) or ! $array) return;
				if (! isset($array[0])) { $i = 0; foreach ($array as $key=>$row) { $arr[$i] = $row; $i++; }}
				else $arr = $array;

				$num      = (isset($set['num']))    ? $set['num']     : '';
				$center   = (isset($set['center'])) ? $set['center']  : '';
				$right    = (isset($set['right']))  ? $set['right']   : '';
				$hide     = (isset($set['hide']))   ? $set['hide']    : '';

				$style    = (isset($set['style']))  ? $set['style']   : '';
				$class    = (isset($set['class']))  ? $set['class']   : '';

				$title    = (isset($set['title']))  ? $set['title']   : 'SatuDJA';
				$btn      = (isset($set['btn']))    ? $set['btn']     : '';
				$note     = (isset($set['note']))   ? $set['note']    : '';
				$show	  = (isset($set['show']))   ? $set['show']	  : false;

				$bold = (isset($set['bold'])) ? $set['bold'] : '';
					if ($bold != '') { $bold_row = explode(':', $bold)[0]; $bold_nil = explode(':', $bold)[1]; }
				$grey = (isset($set['grey'])) ? $set['grey'] : '';
					if ($grey != '') { $grey_row = explode(':', $grey)[0]; $grey_nil = explode(':', $grey)[1]; }
				$red  = (isset($set['red'])) ? $set['red'] : '';
					if ($red != '')  { $red_row = explode(':',  $red)[0];  $red_nil  = explode(':', $red)[1]; }
				$bgcolor = (isset($set['bgcolor'])) ? $set['bgcolor'] : '';
					if ($bgcolor != '') {
						$c = explode(':', $bgcolor); 
						$bgcolor_row = $c[0];  $bgcolor_nil = $c[1];  $bgcolor_warna = 'bg'.$c[2]; }

			// Create Table
				$width = ($show) ? "width='100%'" : '';
				$grid  = "<table id='iBrowse' class='table' $width cellpadding='0px' cellspacing='0px'>\n";
				$grid .= "<thead>\n<tr>\n"; 

				$text = "$('#<th>').click(function() { f_sl *= -1; var n = $(this).prevAll().length; sortTable(f_sl, n); })";
				$java = "";
				foreach (array_keys(current($arr)) as $k=>$v) {
					$th = str_replace('_', ' ', $v);
					if (! strpos("-$hide", $v)) { 
						$th = ($th == 'Kode') ? "$th <i class='fa fa-long-arrow-down' style='color: yellow'></i>" : $th;
						$grid .= "\t<th id='i$v'><b> $th </b></th>\n";
						$java .= str_replace('<th>', "i$v", $text) ."\n";
					}
				}

				$grid .= "</tr>\n</thead>\n<tbody>\n";
				foreach ($arr as $v) {
					$str = '';
					if ($bold != '' and $v[$bold_row] != '') if (strpos("-$bold_nil", $v[$bold_row])) $str .= 'font-weight: bold;';
					if ($grey != '' and $v[$grey_row] != '') if (strpos("-$grey_nil", $v[$grey_row])) $str .= 'color: #808080;';
					if ($red  != '' and $v[$red_row]  != '') if (strpos("-$red_nil",  $v[$red_row]))  $str .= 'color: #FF2121;';

					$var   = ($class == '') ? '' : 'o'.$v[$class];
					$grid .= "<tr class='$var' style='$str'>\n";
					foreach ($v as $ky=>$vl) {
						if (is_array($vl)) $grid .= "\t<td><a href=''>array</a></td>\n";
						elseif (! strpos("-$hide", $ky)) {
							$align = '';
							$txt   = strtolower($vl);
							$txt   = str_replace(' %', '', $txt);
							$txt   = str_replace(',',  '', $txt);

							if (strpos("-$center", $ky))  $align = 'text-align: center';
							if (strpos("-$right", $ky))   $align = 'text-align: right';
							if (strpos("-$num", $ky))     $align = 'text-align: right';

							if (strpos("-$num", $ky)) $vl = number_format($vl, 0, ',', '.');
							else 
								if (substr($vl,0,1) == '0') $vl = "&nbsp;$vl"; 
								else $vl = "$vl";

							$grid .= "\t<td style='$align' class='o-$ky' data-txt=\"$txt\"> $vl </td>\n";
						} 
					}
					$grid .= "</tr>\n"; }
				$grid .= "</tbody>\n</table>\n";
				$grid .= "
					<style> 
						#iBrowse { font-family: 'Trebuchet MS', Arial, Helvetica, sans-serif; border-collapse: collapse; font-size: 100%; }
						#iBrowse tr:nth-child(even) { background-color: #F2F2F2; }
						#iBrowse tr:hover { background-color: #ddd; }
						#iBrowse td, #iBrowse th { border: 1px solid #ddd; padding: 5px; }
						#iBrowse th { padding-top: 10px; padding-bottom: 10px; text-align: center; background-color: #2760A9; cursor: pointer; color: white; }
						$style
					</style>";


			if (! $show) return $grid; 
			else { 	$data = array('grid'=>$grid, 'title'=>$title, 'btn'=>$btn, 'note'=>$note, 'java'=>$java, 'count'=>count($arr));
					$this->CI->load->view('main/v_browse_table', $data);  } }

		function timer( $nil=null ) {
	    	if ($nil == null) {
				list($usec, $sec) = explode(" ", microtime());
				$this->CI->start = ((float)$usec + (float)$sec); }
	    	else {
				$end  = microtime(true);
				$time = number_format((float)$end - $this->CI->start, 2, '.', '');
				return $time; } }

		function summary_v_adk_dipa($sql, $whr, $tgl=null) {
			$rev = $this->CI->dbrvs->query("SELECT kdsatker, concat(kdsatker,'.',max(revisike)) kode FROM v_adk_dipa WHERE $whr $tgl GROUP BY 1")->result_array();
			$inv = "concat(kdsatker,'.',revisike) ". $this->CI->fc->Invar($rev, 'kode');		

			$arr = $this->CI->dbrvs->query("SELECT $sql FROM v_adk_dipa WHERE $inv GROUP BY 1")->result_array(); 
			// echo "V_ADK_DIPA : SELECT $sql FROM v_adk_dipa WHERE $inv GROUP BY 1 <br>";
			return $arr; }

		function summary_v_rekap($sql, $whr, $bts, $tgl=null) {
			$rev = $this->CI->dbrvs->query("SELECT kdsatker, concat(kdsatker,max(revisike)) kode FROM v_adk_dipa WHERE $whr $tgl GROUP BY 1")->result_array();
			$inv = "jenis='RA' AND $whr AND concat(kdsatker,rev_id) ". $this->CI->fc->Invar($rev, 'kode');

			$sql = str_replace('sum(pagu)', 'sum(jml_pagu)', $sql);
			$arr = $this->CI->dbrvs->query("SELECT $sql FROM v_rekap WHERE $inv $bts GROUP BY 1")->result_array(); 
			// echo "V_REKAP : SELECT $sql FROM v_rekap WHERE $inv GROUP BY 1 <br>";
			return $arr; }

		function summary_d_realisasi($key, $whr, $bts) {
			$a = array('kddept'=>'substr(baes1,1,3)', 'kdunit'=>'substr(baes1,4,2)', 'kdprogram'=>'program', 'kdgiat'=>'kegiatan', 'kdoutput'=>'output', 'kdjenbel'=>'substr(akun,1,2)', 'kdakun'=>'akun', 'kdbeban'=>'sdana', 'kdlokasi'=>'substr(lokasi,1,2)', 'kdkabkota'=>'substr(lokasi,3,2)');
			foreach ($a as $k=>$v) {
				$key = str_replace($k, $v, $key);  $whr = str_replace($k, $v, $whr);  $bts = str_replace($k, $v, $bts);}

			$arr = $this->CI->dbrvs->query("SELECT concat($key) kdkey, sum(rphreal) jml FROM d_realisasi WHERE $whr $bts GROUP BY 1")->result_array();
			// echo "Realisasi : SELECT concat($key) kdkey, sum(rphreal) jml FROM d_realisasi WHERE $whr $bts GROUP BY 1 <br><br>";
			return $arr; }

}
