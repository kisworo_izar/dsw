<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// Class of Idub-Libraries
class Ds {
	private $CI;
	function __construct() {
		$this->CI = &get_instance();
	}

	public function create_ds($source, $string) {
		if ($source == 'mysql') {
			$d_outp = $this->read_mysql($string, 'd_output');
			$d_item = $this->read_mysql($string, 'd_item');
		} else if ($source == 'mysql_net') {
			$d_outp = $this->read_mysql($string, 'net_output');
			$d_item = $this->read_mysql($string, 'net_item');
		} else {
			$d_outp = $this->read_adk($string, 'd_output');
			$d_item = $this->read_adk($string, 'd_item');
		}
		$ds = $this->hashing($d_outp, $d_item);
		return $ds;
	}

	function read_mysql($kdsatker, $tbl) {
		$whr = "Where kdsatker='$kdsatker'";
		$key = "thang,kdjendok,kdsatker,kddept,kdunit,kdprogram,kdgiat,kdoutput,kdlokasi,kdkabkota,kddekon";
		$sql = "Select $key,kdkppn,kdbeban,kdjnsban,kdctarik,register,kdakun,jumlah,paguphln,pagurmp,pagurkp,blokirphln,blokirrmp,blokirrkp,rphblokir From $tbl $whr";
		
		if ($tbl == 'd_output' or $tbl == 'net_output') $sql = "Select $key, Round(vol,0) vol From $tbl $whr";
		$query = $this->CI->db->query($sql);
		return $query->result_array();
	}

	function read_adk($adk, $tbl) {
		$arr  = explode('.', $adk);
		$tag  = str_replace('_', '', substr($arr[0],4,17));

		$file = "files/adk_tor/". $arr[0] ."/$tbl$tag.xml";
 		$xml  = simplexml_load_file($file, "SimpleXMLElement", LIBXML_NOCDATA);
		$json = json_encode($xml);
 		$arr  = json_decode($json, TRUE);

		if ( !$arr ) return array();
		foreach (current($arr) as $row) { $assoc = is_array($row); break; }
		if ( $assoc==1 ) $dat = current($arr); else return array();
		return $dat;
	}

	function hashing($c_outp, $c_item) {
		$d_outp = array();
		foreach ($c_outp as $row) {
			$kdkey = $row['thang'].'.'.$row['kdjendok'].'.'.$row['kdsatker'].'.'.$row['kddept'].'.'.$row['kdunit'].'.'.$row['kdprogram'].'.'.$row['kdgiat'].'.'.$row['kdoutput'].'.'.$row['kdlokasi'].'.'.$row['kdkabkota'].'.'.$row['kddekon'];
			array_push($d_outp, array('kdkey'=>$kdkey, 'thang'=>$row['thang'], 'kdjendok'=>$row['kdjendok'], 'kdsatker'=>$row['kdsatker'], 'kddept'=>$row['kddept'], 'kdunit'=>$row['kdunit'], 'kdprogram'=>$row['kdprogram'], 'kdgiat'=>$row['kdgiat'], 'kdoutput'=>$row['kdoutput'], 'kdlokasi'=>$row['kdlokasi'], 'kdkabkota'=>$row['kdkabkota'], 'kddekon'=>$row['kddekon'], 'kdkppn'=>'***', 'kdbeban'=>'*', 'kdjnsban'=>'*', 'kdctarik'=>'*', 'register'=>str_repeat('*',8), 'kdjnsbel'=>'**', 'kdkbkpk'=>'***', 'kdakun'=>'******', 'jumlah'=>0, 'paguphln'=>0, 'pagurmp'=>0, 'pagurkp'=>0, 'blokir'=>0, 'rphpdpt'=>0, 'vol'=>$row['vol']));
		}

		$d_item = array();
		foreach ($c_item as $row) {
			if (is_array($row['register']) or $row['register'] == '') $register = str_repeat('*',8);  else $register = $row['register'];
			$kdkey = $row['thang'].'.'.$row['kdjendok'].'.'.$row['kdsatker'].'.'.$row['kddept'].'.'.$row['kdunit'].'.'.$row['kdprogram'].'.'.$row['kdgiat'].'.'.$row['kdoutput'].'.'.$row['kdlokasi'].'.'.$row['kdkabkota'].'.'.$row['kddekon'];
			array_push($d_item, array('kdkey'=>$kdkey, 'thang'=>$row['thang'], 'kdjendok'=>$row['kdjendok'], 'kdsatker'=>$row['kdsatker'], 'kddept'=>$row['kddept'], 'kdunit'=>$row['kdunit'], 'kdprogram'=>$row['kdprogram'], 'kdgiat'=>$row['kdgiat'], 'kdoutput'=>$row['kdoutput'], 'kdlokasi'=>$row['kdlokasi'], 'kdkabkota'=>$row['kdkabkota'], 'kddekon'=>$row['kddekon'], 'kdkppn'=>$row['kdkppn'], 'kdbeban'=>$row['kdbeban'], 'kdjnsban'=>$row['kdjnsban'], 'kdctarik'=>$row['kdctarik'], 'register'=>$register, 'kdjnsbel'=>substr($row['kdakun'],0,2), 'kdkbkpk'=>'000', 'kdakun'=>'000000', 'jumlah'=>$row['jumlah'], 'paguphln'=>$row['paguphln'], 'pagurmp'=>$row['pagurmp'], 'pagurkp'=>$row['pagurkp'], 'blokir'=>$row['blokirphln']+$row['blokirrmp']+$row['blokirrkp']+$row['rphblokir'], 'rphpdpt'=>0, 'vol'=>0));
		}

		$d_summ = $this->step1($d_outp, $d_item);
		$d_summ = $this->step2($d_summ);
		$d_summ = $this->step3($d_summ);
		$d_summ = $this->step4($d_summ);
		$d_summ = substr($d_summ,0,4) .' '. substr($d_summ,4,4) .' '. substr($d_summ,8,4) .' '. substr($d_summ,12,4);
		return $d_summ;
	}	

	function step1($d_outp, $d_item) {
		$hasil = $this->CI->fc->ToArr($d_outp, 'kdkey');
		foreach ($d_item as $row) {
			$row['kdkey'] .= $row['kdkppn'].'.'. $row['kdbeban'].'.'. $row['kdjnsban'].'.'. $row['kdctarik'].'.'. $row['register'].'.'.$row['kdjnsbel'];
			$kdkey = $row['kdkey'];
			if (array_key_exists($kdkey, $hasil)) {
				$hasil[$kdkey]['jumlah']  += $row['jumlah'];
				$hasil[$kdkey]['paguphln']+= $row['paguphln'];
				$hasil[$kdkey]['pagurmp'] += $row['pagurmp'];
				$hasil[$kdkey]['pagurkp'] += $row['pagurkp'];
				$hasil[$kdkey]['blokir']  += $row['blokir'];
			} else {
				$hasil[$kdkey] = $row;
			}
		}
		$hasil = $this->CI->fc->array_index($hasil, 'kdkey');
		foreach ($hasil as $key=>$row) {
			$var = $row['thang'] . $row['kdjendok'] . $row['kdsatker'] . $row['kddept'] . $row['kdunit'] . $row['kdprogram'] . $row['kdgiat'] . $row['kdoutput'] . $row['kdlokasi'] . $row['kdkabkota'] . $row['kddekon'] . $row['kdkppn'] . $row['kdbeban'] . $row['kdjnsban'] . $row['kdctarik'] . $row['register'] . $row['kdjnsbel'] . $row['kdkbkpk'] . $row['kdakun'] . $row['jumlah'] . $row['paguphln'] . $row['pagurmp'] . $row['pagurkp'] . $row['blokir'] . $row['rphpdpt'] . $row['vol'];
			$var = str_replace('*', ' ', $var);
			$hasil[$key]['plaintext'] = $var;
			$hasil[$key]['md5'] = strtoupper( md5($var) );
		}
		return $hasil;
	}

	function step2( $array ) {
		$hasil = array();
		foreach ($array as $row) $hasil[ $row['md5'] ]['md5'] = $row['md5'];
		return $this->CI->fc->array_index($hasil, 'md5');
	}

	function step3( $array ) {
		$hasil = '';  $hit = 0;  $var = '';  $str = ''; $fib = '';
		foreach ($array as $row) {
			$hit++;
			$var .= $row['md5'];
			if ($hit >= 50) {
				if ($str == '') {
					$str = strtoupper( md5($var) );
					// echo "$str <br>";
				}
				else {
					$str = strtoupper(md5($str)) . strtoupper(md5($var));
					// echo "$str <br>";
					$str = strtoupper(md5($str));
				}
				$hit = 0;
			}
		}
		if (count($array) > 50) {
			$str  = $str . strtoupper( md5($var) );
			// echo "$str <br>";
			$str  = strtoupper( md5($str) );
		} else {
	        $str  = strtoupper( md5($var) );
	        $str  = strtoupper( md5($str) );
      	}
		$hasil = $str;
		return $hasil;
	}

	function step4( $var ) {
		$var = str_replace('a', '0', $var); $var = str_replace('b', '0', $var); $var = str_replace('c', '0', $var);
		$var = str_replace('d', '0', $var); $var = str_replace('e', '0', $var); $var = str_replace('f', '0', $var);

		$hasil = '';
		for ($i=0; $i<strlen($var); $i=$i+2) { 
			$nil = substr($var,$i,1) + substr($var,$i+1,1);
			if ($nil > 9) $nil = $nil - 9;
			$hasil .= $nil;
		}
		return $hasil;
	}

	function browse($array, $number=null) {
		if (! is_array($array) or ! $array) return;
		if (! isset($array[0])) { $i = 0; foreach ($array as $key=>$row) { $arr[$i] = $row; $i++; }}
		else $arr = $array;

		if (count($arr) > 0) {
			$hasil  = "<table style='font-size: 80%; border: 1px solid grey;' cellpadding='0px' cellspacing='0px'>";
			$hasil .= "<thead><tr><th style='border: 1px solid grey; text-align:center'>". implode("</th><th style='border: 1px solid grey; text-align:center'>", array_keys(current($arr))) ."</th></tr></thead><tbody>";

			foreach ($arr as $row){
				$i = 0;
				$colmn = array_keys($row);
				$hasil .= "<tr>";
				foreach ($row as $sub) {
					if (is_array($sub)) $hasil .= "<td style='border: 1px solid grey'><a href=''>array</a></td>";
					else 
						if ($number == null) $hasil .= "<td style='border: 1px solid grey'>$sub</td>"; 
						else {
							if (strpos($number, $colmn[$i])) 
								if (fmod($sub, 1) != 0) $hasil .= "<td style='border: 1px solid grey; text-align:right'>". number_format($sub, 4, ',', '.') ."</td>";
								else $hasil .= "<td style='border: 1px solid grey; text-align:right'>". number_format($sub, 0, ',', '.') ."</td>";
							elseif (strpos('-status', $colmn[$i])) 
								if ($sub) $hasil .= "<td style='border: 1px solid grey; color:green; text-align:center'> true</td>";
								else 	  $hasil .= "<td style='border: 1px solid grey; color:red; text-align:center'> false</td>";
							else 
								$hasil .= "<td style='border: 1px solid grey'>$sub</td>";
						}
					$i++;
				}
				$hasil .= "</tr>";
			}
			$hasil .= "</tbody></table>";
		}
		return $hasil;
	}

}
