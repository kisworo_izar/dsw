<div class="row">
    <div class="col-md-12" style="padding: 0px;">
        <div class="card">
            <div class="col-sm-6 col-md-4 grid-item">
                <div class="thumbnail card"> <img src="files/ekstra/hpp346.png">
                    <div class="caption">
                        <a href="files/ekstra/HPP-346.0.pdf">
                            <h4 style="word-wrap: break-word;">HPP-346.0 </h4>
                        </a>
                        <h5>Harmonisasi terhadap Kebijakan Norma Pedoman Peraturan Penganggaran di Bidang KementerianLembaga, Jaminan Sosial, PNBP, dan Remunerasi (Internal)</h5>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-4 grid-item">
                <div class="thumbnail card"> <img src="files/ekstra/hpp347.png">
                    <div class="caption">
                        <a href="files/ekstra/HPP-347.0.pdf">
                            <h4 style="word-wrap: break-word;">HPP-347.0 </h4>
                        </a>
                        <h5>Harmonisasi terhadap Kebijakan Norma Pedoman Peraturan Penganggaran di Bidang KementerianLembaga, Jaminan Sosial, PNBP, dan Remunerasi (Eksternal)</h5>
                    </div>
                </div>
            </div>

            <div class="col-sm-6 col-md-4 grid-item">
                <div class="thumbnail card"> <img src="files/ekstra/hpp348.png">
                    <div class="caption">
                        <a href="files/ekstra/HPP-348.0.pdf">
                            <h4 style="word-wrap: break-word;">HPP-348.0 </h4>
                        </a>
                        <h5>Kajian terhadap Kebijakan Norma Pedoman Peraturan Penganggaran di Bidang KementerianLembaga, Jaminan Sosial, PNBP, dan Remunerasi</h5>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-4 grid-item">
                <div class="thumbnail card"> <img src="files/ekstra/hpp349.png">
                    <div class="caption">
                        <a href="files/ekstra/HPP-349.0.pdf">
                            <h4 style="word-wrap: break-word;">HPP-349.0 </h4>
                        </a>
                        <h5>Evaluasi terhadap Kebijakan Norma Pedoman Peraturan Penganggaran di Bidang KementerianLembaga, Jaminan Sosial, PNBP, dan Remunerasi</h5>
                    </div>
                </div>
            </div>

            <div class="col-sm-6 col-md-4 grid-item">
                <div class="thumbnail card"> <img src="files/ekstra/hpp350.png">
                    <div class="caption">
                        <a href="files/ekstra/HPP-350.0.pdf">
                            <h4 style="word-wrap: break-word;">HPP-350.0 </h4>
                        </a>
                        <h5>Monitoring Program PenyusunanPerencanaan Peraturan Perundang-undangan di Lingkungan DJA</h5>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-4 grid-item">
                <div class="thumbnail card"> <img src="files/ekstra/hpp351.png">
                    <div class="caption">
                        <a href="files/ekstra/HPP-351.0.pdf">
                            <h4 style="word-wrap: break-word;">HPP-351.0 </h4>
                        </a>
                        <h5>Penyelesaian Ijin Prinsip terhadap Kebijakan Peraturan Penganggaran di Bidang Remunerasi (Tunjangan Kinerja)</h5>
                    </div>
                </div>
            </div>

            <div class="col-sm-6 col-md-4 grid-item">
                <div class="thumbnail card"> <img src="files/ekstra/hpp352.png">
                    <div class="caption">
                        <a href="files/ekstra/HPP-352.0.pdf">
                            <h4 style="word-wrap: break-word;">HPP-352.0 </h4>
                        </a>
                        <h5>Penyelesaian Ijin Prinsip terhadap Kebijakan Peraturan Penganggaran di Bidang Remunerasi (Non Tunjangan Kinerja)</h5>
                    </div>
                </div>
            </div>



        </div>
    </div>
</div>



<!-- <div class="col-md-12">

      <div class="grid capitalise" id="dosirList" style="position: relative;">
          <div class="col-sm-6 col-md-4 grid-item" style="position: absolute; left: 0%; top: 0px;">
              <div class="thumbnail card"> <img data-pdf-thumbnail-file="http://intranet.anggaran.depkeu.go.id/files/upload_milea/197611261996021002/dosir_1580267820315_didik_setiawan.pdf" src="http://intranet.anggaran.depkeu.go.id/files/upload_milea/197611261996021002/dosir_1580267820315_thumbnail.png">
                  <div class="caption">
                      <h4 style="word-wrap: break-word;">DP3 2019</h4>
                      <p> <span class="badge bg-cyan pull-right">Didik Setiawan</span> <span class="badge bg-teal">8 Jan 2020</span> </p>
                      <p> PENILAIAN KINERJA </p>
                      <p><small><span class="text-muted">Terakhir diubah oleh <a href="#">Didik Setiawan</a> - 29 Jan 2020 03.17.00</span></small></p>
                      <p> </p>
                      <div class="btn-group"> <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="material-icons">menu</i> </button>
                          <ul class="dropdown-menu">
                              <li><a href="#" class="waves-effect waves-block" data-toggle="modal" data-target="#dosirModal" data-action="edit" data-dosir="{&quot;id&quot;:400,&quot;title&quot;:&quot;DP3 2019&quot;,&quot;date&quot;:&quot;2020-01-08T00:00:00.000Z&quot;,&quot;group_id&quot;:11,&quot;filename&quot;:&quot;dosir_1580267820315_didik_setiawan.pdf&quot;,&quot;nip&quot;:&quot;197611261996021002&quot;,&quot;updatedby&quot;:&quot;197611261996021002&quot;,&quot;updatedat&quot;:&quot;2020-01-28T20:17:00.000Z&quot;,&quot;approval&quot;:0,&quot;approvedby&quot;:null,&quot;approvedat&quot;:null,&quot;thumbnailname&quot;:&quot;dosir_1580267820315_thumbnail.png&quot;,&quot;group_name&quot;:&quot;PENILAIAN KINERJA&quot;,&quot;updatedby_name&quot;:&quot;Didik Setiawan&quot;,&quot;pegawai_name&quot;:&quot;Didik Setiawan&quot;,&quot;approvedby_name&quot;:null}">Edit</a></li>
                              <li><a href="http://intranet.anggaran.depkeu.go.id/files/upload_milea/197611261996021002/dosir_1580267820315_didik_setiawan.pdf" target="_blank" class=" waves-effect waves-block">Download</a></li>
                              <li role="separator" class="divider"></li>
                              <li><a href="#" class="waves-effect waves-block deleteDosir" data-id="400">Hapus</a></li>
                          </ul>
                      </div> <button type="button" class="btn btn-warning btn-circle waves-effect waves-circle waves-float pull-right" data-toggle="tooltip" data-placement="top" title="" data-original-title="Menunggu penetapan oleh Bagian SDM" data-id="400"> <i class="material-icons">gavel</i> </button>
                      <p></p>
                  </div>
              </div>
          </div>
          <div class="col-sm-6 col-md-4 grid-item" style="position: absolute; left: 33.3329%; top: 0px;">
              <div class="thumbnail card"> <img data-pdf-thumbnail-file="http://intranet.anggaran.depkeu.go.id/files/upload_milea/197611261996021002/738f.pdf" src="">
                  <div class="caption">
                      <h4 style="word-wrap: break-word;">BUKTI PEMOTONGAN SETAHUN PAJAKPENGHASILAN PPH PASAL 21 (FINAL) TAHUN 2020</h4>
                      <p> <span class="badge bg-cyan pull-right">Didik Setiawan</span> <span class="badge bg-teal">9 Feb 2021</span> </p>
                      <p> PPH 21 </p>
                      <p><small><span class="text-muted">Terakhir diubah oleh <a href="#">Bagian SDM</a> - 9 Feb 2021 14.00.00</span></small></p>
                      <p> </p>
                      <div class="btn-group"> <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="material-icons">menu</i> </button>
                          <ul class="dropdown-menu">
                              <li><a href="#" class="waves-effect waves-block" data-toggle="modal" data-target="#dosirModal" data-action="edit" data-dosir="{&quot;id&quot;:1924,&quot;title&quot;:&quot;BUKTI PEMOTONGAN SETAHUN PAJAKPENGHASILAN PPH PASAL 21 (FINAL) TAHUN 2020&quot;,&quot;date&quot;:&quot;2021-02-09T00:00:00.000Z&quot;,&quot;group_id&quot;:44,&quot;filename&quot;:&quot;738f.pdf&quot;,&quot;nip&quot;:&quot;197611261996021002&quot;,&quot;updatedby&quot;:&quot;199111182019021008&quot;,&quot;updatedat&quot;:&quot;2021-02-09T07:00:00.000Z&quot;,&quot;approval&quot;:1,&quot;approvedby&quot;:&quot;199111182019021008&quot;,&quot;approvedat&quot;:&quot;2021-02-09T07:00:00.000Z&quot;,&quot;thumbnailname&quot;:&quot;&quot;,&quot;group_name&quot;:&quot;PPH 21&quot;,&quot;updatedby_name&quot;:&quot;Andre Novelando&quot;,&quot;pegawai_name&quot;:&quot;Didik Setiawan&quot;,&quot;approvedby_name&quot;:&quot;Andre Novelando&quot;}">Edit</a></li>
                              <li><a href="http://intranet.anggaran.depkeu.go.id/files/upload_milea/197611261996021002/738f.pdf" target="_blank" class=" waves-effect waves-block">Download</a></li>
                              <li role="separator" class="divider"></li>
                              <li><a href="#" class="waves-effect waves-block deleteDosir" data-id="1924">Hapus</a></li>
                          </ul>
                      </div> <button type="button" class="btn btn-success btn-circle waves-effect waves-circle waves-float pull-right" data-toggle="tooltip" data-placement="top" title="" data-original-title="Ditetapkan oleh Bagian SDM - 9 Feb 2021 14.00.00" data-id="1924"> <i class="material-icons">check</i> </button>
                      <p></p>
                  </div>
              </div>
          </div>
          <div class="col-sm-6 col-md-4 grid-item" style="position: absolute; left: 66.6657%; top: 0px;">
              <div class="thumbnail card"> <img data-pdf-thumbnail-file="http://intranet.anggaran.depkeu.go.id/files/upload_milea/197611261996021002/738.pdf" src="">
                  <div class="caption">
                      <h4 style="word-wrap: break-word;">BUKTI PEMOTONGAN PAJAK PENGHASILAN PASAL 21 BAGI PEGAWAI NEGERI SIPIL ATAU ANGGOTA TENTARA NASIONAL INDONESIA ATAU ANGGOTA POLISI REPUBLIK INDONESIA ATAU PEJABAT NEGARA ATAU PENSIUNANNYA</h4>
                      <p> <span class="badge bg-cyan pull-right">Didik Setiawan</span> <span class="badge bg-teal">9 Feb 2021</span> </p>
                      <p> PPH 21 </p>
                      <p><small><span class="text-muted">Terakhir diubah oleh <a href="#">Bagian SDM</a> - 9 Feb 2021 14.00.00</span></small></p>
                      <p> </p>
                      <div class="btn-group"> <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="material-icons">menu</i> </button>
                          <ul class="dropdown-menu">
                              <li><a href="#" class="waves-effect waves-block" data-toggle="modal" data-target="#dosirModal" data-action="edit" data-dosir="{&quot;id&quot;:2820,&quot;title&quot;:&quot;BUKTI PEMOTONGAN PAJAK PENGHASILAN PASAL 21 BAGI PEGAWAI NEGERI SIPIL ATAU ANGGOTA TENTARA NASIONAL INDONESIA ATAU ANGGOTA POLISI REPUBLIK INDONESIA ATAU PEJABAT NEGARA ATAU PENSIUNANNYA&quot;,&quot;date&quot;:&quot;2021-02-09T00:00:00.000Z&quot;,&quot;group_id&quot;:44,&quot;filename&quot;:&quot;738.pdf&quot;,&quot;nip&quot;:&quot;197611261996021002&quot;,&quot;updatedby&quot;:&quot;199111182019021008&quot;,&quot;updatedat&quot;:&quot;2021-02-09T07:00:00.000Z&quot;,&quot;approval&quot;:1,&quot;approvedby&quot;:&quot;199111182019021008&quot;,&quot;approvedat&quot;:&quot;2021-02-09T07:00:00.000Z&quot;,&quot;thumbnailname&quot;:&quot;&quot;,&quot;group_name&quot;:&quot;PPH 21&quot;,&quot;updatedby_name&quot;:&quot;Andre Novelando&quot;,&quot;pegawai_name&quot;:&quot;Didik Setiawan&quot;,&quot;approvedby_name&quot;:&quot;Andre Novelando&quot;}">Edit</a></li>
                              <li><a href="http://intranet.anggaran.depkeu.go.id/files/upload_milea/197611261996021002/738.pdf" target="_blank" class=" waves-effect waves-block">Download</a></li>
                              <li role="separator" class="divider"></li>
                              <li><a href="#" class="waves-effect waves-block deleteDosir" data-id="2820">Hapus</a></li>
                          </ul>
                      </div> <button type="button" class="btn btn-success btn-circle waves-effect waves-circle waves-float pull-right" data-toggle="tooltip" data-placement="top" title="" data-original-title="Ditetapkan oleh Bagian SDM - 9 Feb 2021 14.00.00" data-id="2820"> <i class="material-icons">check</i> </button>
                      <p></p>
                  </div>
              </div>
          </div>
          <div class="col-sm-6 col-md-4 grid-item" style="position: absolute; left: 66.6657%; top: 0px;">
              <div class="thumbnail card"> <img data-pdf-thumbnail-file="http://intranet.anggaran.depkeu.go.id/files/upload_milea/197611261996021002/738.pdf" src="">
                  <div class="caption">
                      <h4 style="word-wrap: break-word;">BUKTI PEMOTONGAN PAJAK PENGHASILAN PASAL 21 BAGI PEGAWAI NEGERI SIPIL ATAU ANGGOTA TENTARA NASIONAL INDONESIA ATAU ANGGOTA POLISI REPUBLIK INDONESIA ATAU PEJABAT NEGARA ATAU PENSIUNANNYA</h4>
                      <p> <span class="badge bg-cyan pull-right">Didik Setiawan</span> <span class="badge bg-teal">9 Feb 2021</span> </p>
                      <p> PPH 21 </p>
                      <p><small><span class="text-muted">Terakhir diubah oleh <a href="#">Bagian SDM</a> - 9 Feb 2021 14.00.00</span></small></p>
                      <p> </p>
                      <div class="btn-group"> <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="material-icons">menu</i> </button>
                          <ul class="dropdown-menu">
                              <li><a href="#" class="waves-effect waves-block" data-toggle="modal" data-target="#dosirModal" data-action="edit" data-dosir="{&quot;id&quot;:2820,&quot;title&quot;:&quot;BUKTI PEMOTONGAN PAJAK PENGHASILAN PASAL 21 BAGI PEGAWAI NEGERI SIPIL ATAU ANGGOTA TENTARA NASIONAL INDONESIA ATAU ANGGOTA POLISI REPUBLIK INDONESIA ATAU PEJABAT NEGARA ATAU PENSIUNANNYA&quot;,&quot;date&quot;:&quot;2021-02-09T00:00:00.000Z&quot;,&quot;group_id&quot;:44,&quot;filename&quot;:&quot;738.pdf&quot;,&quot;nip&quot;:&quot;197611261996021002&quot;,&quot;updatedby&quot;:&quot;199111182019021008&quot;,&quot;updatedat&quot;:&quot;2021-02-09T07:00:00.000Z&quot;,&quot;approval&quot;:1,&quot;approvedby&quot;:&quot;199111182019021008&quot;,&quot;approvedat&quot;:&quot;2021-02-09T07:00:00.000Z&quot;,&quot;thumbnailname&quot;:&quot;&quot;,&quot;group_name&quot;:&quot;PPH 21&quot;,&quot;updatedby_name&quot;:&quot;Andre Novelando&quot;,&quot;pegawai_name&quot;:&quot;Didik Setiawan&quot;,&quot;approvedby_name&quot;:&quot;Andre Novelando&quot;}">Edit</a></li>
                              <li><a href="http://intranet.anggaran.depkeu.go.id/files/upload_milea/197611261996021002/738.pdf" target="_blank" class=" waves-effect waves-block">Download</a></li>
                              <li role="separator" class="divider"></li>
                              <li><a href="#" class="waves-effect waves-block deleteDosir" data-id="2820">Hapus</a></li>
                          </ul>
                      </div> <button type="button" class="btn btn-success btn-circle waves-effect waves-circle waves-float pull-right" data-toggle="tooltip" data-placement="top" title="" data-original-title="Ditetapkan oleh Bagian SDM - 9 Feb 2021 14.00.00" data-id="2820"> <i class="material-icons">check</i> </button>
                      <p></p>
                  </div>
              </div>
          </div>
      </div>
</div> -->