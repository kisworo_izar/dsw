<div class="row">
    <div class="col-md-9">
        <div class="box box-widget" style="border-top: 1px solid #D2D6DE; margin-bottom:10px">

            <div class="box-header" style="background :#F7F7F7">
                <div class="user-block">
                    <?php
                        $foto_profile="files/profiles/_noprofile.png";
                        if (file_exists("files/profiles/".$d_head['nip'].".gif")) {$foto_profile =  "files/profiles/".$d_head['nip'].".gif";}
                    ?>
                    <img class="img-circle img-bordered-dsw" src="<?php echo site_url($foto_profile); ?>" alt="User Image">
                    <span class="username">
                        <span class="username" style="margin-left:0px">
                            <span class="profile-tooltip">
                                <span class="profile-tooltip-item"><?php echo '<a href="'. site_url('forum?q=y0xPz&idparent=1&idchild='.$d_head['idchild']) .'">'. $d_head['nmroom'] .'</a>' ?></span>
                            </span>
                        </span>
                    </span>
                    <div class="comment more shortened"><?php echo $d_head['deskripsi'] ?></div>
                </div>
            </div>

            <div class="box-footer" style="border-top: 1px solid #D2D6DE; padding: 5px 10px 5px 10px">
                <b>Subforum : <?php echo $d_head['nmroom'] ?></b>
            </div>

            <?php
            foreach ($d_sub as $row) {
                $foto_profile="files/profiles/_noprofile.png";
                if (file_exists("files/profiles/".$row['nip'].".gif")) {$foto_profile =  "files/profiles/".$row['nip'].".gif";}
            ?>
                <div class="box-footer box-comments" style="padding-bottom:0px; padding-top:5px; border-top:1px solid #D2D6DE">
                    <div class="box-comment">
                        <img class="img-circle img-bordered-dsw" src="<?php echo site_url($foto_profile); ?>" alt="User Image">
                        <div class="comment-text">
                            <span class="username">
                                <span class="profile-tooltip-item"><?php echo '<a href="'. site_url('forum?q=y0xPz&idparent=1&idchild='.$row['idparent'].'&idgrand='.$row['idchild']) .'">'. $row['nmroom'] .'</a>' ?>
                                </span>
                            </span>

                            <span class="hidden-xs text-muted pull-left">
                            <?php
                                if ($row['nmpic']!='') {
                                    echo "PIC : ";
                                    $arr = explode(',', trim($row['nmpic']));
                                    for ($i=0; $i<count($arr); $i++) {
                                        $nmpic   = $arr[$i];
                                        $urlfile = site_url("#");
                                        if ( $i<count($arr)-1 ) $nmpic .= ", ";
                                        echo $nmpic;
                                    }
                                } else {
                                    echo "PIC : - ";
                                }
                            ?>
                            </span>

                            <span class="hidden-xs text-muted pull-right">
                                <?php echo $this->fc->idtgl($row['tglcreate'],'full') ?>
                            </span>
                            <br>
                            <div class="comment more shortened"><?php echo $row['deskripsi'] ?></div>
                        </div>
                    </div>
                </div>
            <?php } ?>

        </div>
        <div class="box box-widget" style="margin-bottom:10px">
            <div class="box-footer" style="background :#F7F7F7; border:1px solid #D2D6DE; border-radius:0px">

                <form id="iForm" class="form-inline" role="form" action="#" method="post">
					<div class="form-group pull-right">
						<div class="input-group">
                			<input type="hidden" name="nmfunction" value="grid">
            				<input class="form-control input-sm" name="cari" type="text" value="" placeholder="Pencarian thread ...">
            			</div>
            		</div>
            		<div class="form-group">
            			<div class="input-group">
                			<span class="input-group-btn">
				  				<button type="button" class="btn btn-xm btn-primary" onclick="window.location.href='<?php echo site_url('forum?q=vryGh') ?>'" >Buat Thread</button>
                			</span>
            			</div>
            		</div>
                </form>
            </div>
        </div>



        <div class="box box-widget">
            <div class="box-footer" style="border-top: 1px solid #D2D6DE">
                <div class="btn-group">
                    <button type="button" class="btn btn-sm btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Thread</a></li>
                        <li><a href="#">Thread Starter</a></li>
                        <li><a href="#">Rating</a></li>
                        <li><a href="#">Last Post</a></li>
                        <li><a href="#">Replies</a></li>
                        <li><a href="#">View</a></li>
                    </ul>
                </div>
                <span class="pull-right" style="padding-top:5px">Hal : 1 dari 10  Last </span>
            </div>
            <div class="box-footer" style="border-top: 1px solid #D2D6DE; border-bottom: 1px solid #D2D6DE; padding: 5px 10px 5px 10px;background :#F7F7F7; border-radius:0px">
                <b>Thread</b>
                    <!-- - Urut View -->
            </div>

            <table class="table table-stripped" style="border-top: 1px solid #D2D6DE">
            <?php
            foreach ($thread as $row) {
                $foto_profile="files/profiles/_noprofile.png";
                if (file_exists("files/profiles/".$row['nip'].".gif")) {$foto_profile =  "files/profiles/".$row['nip'].".gif";}
            ?>
                <tr>
                    <td style="padding:0px">
                        <div class="box-footer box-comments" style="padding-bottom:0px;background:#fff;border:0px">
                            <div class="box-comment">

                                <span class="profile-tooltip">
                                    <img class="img-circle img-bordered-dsw" src="<?php echo site_url($foto_profile); ?>" alt="user image">
                                        <span class="profile-tooltip-content"><img src="<?php echo site_url($foto_profile); ?>">
                                        <span class="profile-tooltip-text" style="font-size:18px; padding-top:5px">
                                            <?php echo $row['nmuser'] ?>
                                        </span><br>
                                        <span class="profile-tooltip-text"><?php echo trim($row['jabatan']) ?> </span><br>
                                        <span class="profile-tooltip-text"><?php echo $row['nmso'] ?></span>
                                    </span>
                                </span>

                                <div class="comment-text">
                                    <span class="username">
                                        <span class="profile-tooltip-item">
                                            <a href="<?php echo site_url('forum?q=JFuBN').'&idparent='. $row['idparent'] .'&idchild='. $row['idchild'] ?>"><?php echo $row['thread'] ?></a>
                                        </span>
                                        <!-- <i class="fa fa-thumb-tack text-danger"></i> -->
                                    </span>
                                    <div class="comment more shortened">
                                    <span class="profile-tooltip">Oleh :
                                        <span class="profile-tooltip-item"><?php echo $row['nmuser'] ?> </span>
                                    </span>

                                    <span class="pull-right profile-tooltip">
                                        <span class="small text-muted">
                                            <i class="small fa fa-eye"></i> View : <?php echo $row['replies'] ?>
                                        </span>
                                    </span>
                                    <br>
                                    <span class="text-muted">
                                        <?php echo $this->fc->idtgl($row['tglpost'],'full') ?> &nbsp;
                                        <span class="pull-right profile-tooltip">
                                            <span class="small text-muted">
                                                <i class="small fa fa-reply"></i> Replies : <?php echo $row['replies'] ?>
                                            </span>
                                        </span>
                                    </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>

                    <!-- <td class="hidden-xs">
                        <div class="box-comment">
                             <div class="comment-text">
                                 <span class="username">
                                     <span class="profile-tooltip-item">&nbsp;</span>
                                 </span>
                                 <div class="comment more shortened">
                                     <a href="#"><?php echo $row['nmreplies'] ?></a></div>
                                 <span class="small text-muted"><?php echo $this->fc->idtgl($row['tglreplies'],'tgljam') ?></span>
                             </div>
                       </div>
                    </td> -->
                </tr>
            <?php } ?>

            </table>

            <div class="box-footer" style="border-top: 1px solid #D2D6DE; border-bottom: 1px solid #D2D6DE; padding: 5px 10px 5px 10px;background :#F7F7F7; border-radius:0px">
                <span class="pull-right">Hal : 1 dari 10 Last</span>
            </div>
        </div>
    </div>

    <div class="col-md-3 hidden-xs">
        <div class="box box-widget" style=";border-top: 1px solid #D2D6DE"  >
            <div style="background :#F7F7F7; border-radius:0px; border-left: 5px solid #3C8DBC;padding: 5px">
                <b style="color:#3C8DBC">Forum</b>
            </div>
            <?php foreach ($d_room as $row) { ?>
                <div class="box-footer" style="border-top: 1px solid #D2D6DE; padding: 5px 10px 5px 10px">
                    <a href="<?php echo site_url('forum/forum_grid').'/'. $row['idparent'] .'/'. $row['idchild'] ?>"><i class="fa fa-<?php echo $row['icon'] ?>"></i>&nbsp;&nbsp;<?php echo $row['nmroom'] ?></a>
                </div>
            <?php } ?>
        </div>
    </div>
    <div class="col-md-3 hidden-xs"><?php $this->load->view('forum/v_forum_top_thread'); ?></div>
    <div class="col-md-3 hidden-xs"><?php $this->load->view('forum/v_forum_last_thread'); ?></div>
    </div>
</div>
