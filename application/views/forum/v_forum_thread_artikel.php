
<?php
    $query = $this->db->query("Select * From d_forum_room Where idchild=$idparent");
    $d_room= $query->row_array();

    $query = $this->db->query("Select a.*, nmuser, nip, jabatan, nmso, pic From d_forum a Left Join t_user b On a.iduser=b.iduser Left Join t_so c On b.kdso=c.kdso Left Join d_forum_room d On a.idparent=d.idparent And a.idchild=d.idchild Where a.idparent=$idparent and a.idchild=$idchild and idforum=1");
    $d_head= $query->row_array();
?>

<div class="box box-widget" style="margin-bottom:0px">

    <div class="box-header with-border" style="border-radius:0px;border-bottom: 1px solid #D2D6DE;padding-top:3px; padding-bottom:3px; background-color:#FFF">
        <span class="pull-left">
            <?php
                $hom = site_url('forum?q=y0xPz');
                $url = site_url('forum?q=y0xPz&idparent=1&idchild='. $d_room['idparent'] .'&idgrand='.$d_room['idchild']);

                if ($this->session->userdata('idusergroup')=='999') { $hom = '#'; $url = '#'; }
            ?>
            <a href="<?php echo $hom ?>"><i class="fa fa-home"></i></a> -
            <a href="<?php echo $url ?>"><?php echo $d_room['nmroom'] ?></a>
        </span>
    </div>

    <div class="box-header" style="background :#f7f7f7;">
        <div class="user-block">
            <?php
                $foto_profile="files/profiles/_noprofile.png";
                if (file_exists("files/profiles/".$d_head['nip'].".gif")) {$foto_profile =  "files/profiles/".$d_head['nip'].".gif";}
            ?>

            <span class="profile-tooltip1">
                <img class="img-circle img-bordered-dsw" src="<?php echo site_url($foto_profile); ?>" alt="user image">
                    <span class="profile-tooltip-content"><img src="<?php echo site_url($foto_profile); ?>">
                    <span class="profile-tooltip-text" style="font-size:18px; padding-top:5px">
                        <?php echo $d_head['nmuser'] ?>
                    </span><br>
                    <span class="profile-tooltip-text"><?php echo trim($d_head['jabatan']) ?> </span><br>
                    <span class="profile-tooltip-text"><?php echo $d_head['nmso'] ?></span>
                </span>
            </span>


            <span class="username" tooltip="Click judul = Tampilkan Seluruh posting">
                <span class="username" style="margin-left:0px">
                    <span class="profile-tooltip">
                        <span class="profile-tooltip-item">
                            <?php
                                echo '<a href="'. site_url('forum?q=JFuBN&idparent='.$d_head['idparent'].'&idchild='.$d_head['idchild']) .'">'
                                .$d_head['thread'].'</a>';
                            ?>
                        </span>
                            <?php
                                if ( strpos($this->session->userdata('idusergroup'), '001') or $this->session->userdata('idusergroup')=='001' )
                                    echo '<a href="'. site_url('forum?q=vryGh&action=u&idparent='.$d_head['idparent'].'&idchild='.$d_head['idchild']) .'">
                                    <span class="small pull-right"><i class="small fa fa-edit"></i></span></a>';
                            ?>
                    </span>
                </span>
            </span>
            <div class="comment more shortened">
                <span class="profile-tooltip">
                    <span class="profile-tooltip-item"><?php echo $d_head['nmuser'] ?></span>
                    <span class="small text-muted"> - <?php echo $this->fc->idtgl($d_head['tglpost'],'full') ?></span>
                </span>
            </div>
        </div>
    </div>

    <div class="box-footer" style="border-top: 1px solid #D2D6DE; padding: 5px 10px 5px 10px">
        <?php
            $this->fc->read_more( $d_head['post'] );
            if ($d_head['attach']!='') {
                echo "<br><i class='small'>File : </i><br />";
                $arr = explode(';', trim($d_head['attach']));
                for ($i=0; $i<count($arr); $i++) {
                    $nmfile  = $arr[$i];
                    $urlfile = site_url("files/forum/$nmfile");
                    if ( $i<count($arr)-1 ) $nmfile .= "<br />";
                    echo "<i class='small'><a href='$urlfile' target='_blank'>$nmfile </a></i>";
                }
            }
        ?>
    </div>

    <div class="box-footer" style="border-top: 1px solid #D2D6DE; border-radius:0px; padding: 5px 10px 5px 10px;">
        <?php
            if ( strpos($this->session->userdata('idusergroup'), '001') or $this->session->userdata('iduser')==$d_head['pic'] )
                echo '
                    <b style="color:#7A7A7A">Catatan</b>
                    <span class="small pull-right">
                        <a href="'. site_url('forum?q=vryGh&action=c&idparent='.$d_head['idparent'].'&idchild='.$d_head['idchild']) .'">
                            <i class="fa fa-edit"></i> Edit</span>
                        </a>
                    </span><br>';
            if ($d_head['catatan']!='') $this->fc->read_more( $d_head['catatan'] );
        ?>
        <!-- <b style="color:#7A7A7A">Catatan</b>
        <span class="small pull-right">
            <a href="#"><i class="fa fa-edit"></i> Edit</a>
        </span><br>
        Catatan ada disini -->
    </div>
    <!-- <div class="box-footer" style="border-top: 1px solid #D2D6DE; border-radius:0px; padding: 5px 10px 5px 10px;">
        <b style="color:#7A7A7A">Sticky</b><br>
        Sticky posting
    </div> -->
<!-- 'forum/forum_post/'.$d_head['idparent'].'/'.$d_head['idchild'] -->
</div>

<script type="text/javascript">
    function ReadMore(obj) {
        var div  = document.getElementById(obj);
        var show = obj.replace('hidden','show');
        if (div.style.display == 'none') {
            div.style.display = '';
            document.getElementById(show).innerHTML = 'Hide';
        } else {
            div.style.display = 'none';
            document.getElementById(show).innerHTML = 'Read More';
        }
    }
</script>
