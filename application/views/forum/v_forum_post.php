<script src="<?=base_url('assets/plugins/slimScroll/jquery.slimscroll.min.js'); ?>" type="text/javascript"></script>
<script src="<?=base_url('assets/plugins/jQueryUI/jquery-ui.1.11.4.min.js'); ?>" type="text/javascript"></script>
<link rel="stylesheet" href="<?=base_url('assets/plugins/jQueryUI/jquery-ui.css'); ?>">


<div class="row">
    <div class="col-md-9">

        <?php $this->load->view('forum/v_forum_thread'); ?>

        <div class="box box-widget" style="margin-bottom:10px">

            <div class="box-footer" style="border-top: 1px solid #D2D6DE; padding: 5px 10px 5px 10px;background :#F7F7F7">
                <span class="pull-right"><?php echo $this->pagination->create_links(); ?></span>
            </div>

            <?php
            foreach ($d_post as $row) {
                $foto_profile="files/profiles/_noprofile.png";
                if (file_exists("files/profiles/".$row['nip'].".gif")) { $foto_profile =  "files/profiles/".$row['nip'].".gif"; } ?>

                <div class="box-footer box-comments" style="padding-bottom:0px; padding-top:0px; border-top:1px solid #D2D6DE;background :#fff">
                    <div class="box-comment">
                        <span class="profile-tooltip">
                            <img class="img-circle img-sm" src="<?php echo site_url($foto_profile); ?>" alt="user image">
                                <span class="profile-tooltip-content"><img src="<?php echo site_url($foto_profile); ?>">
                                <span class="profile-tooltip-text" style="font-size:18px; padding-top:5px">
                                    <?php echo $row['nmuser'] ?>
                                </span><br>
                                <span class="profile-tooltip-text"><?php echo trim($row['jabatan']) ?> </span><br>
                                <span class="profile-tooltip-text"><?php echo $row['nmso'] ?></span>
                            </span>
                        </span>
                        <div class="comment-text">
                            <span class="username">
                                <span class="profile-tooltip-item"><?php echo $row['nmuser'] ?> </span>
                                <span class="text-muted pull-right"><?php echo $this->fc->idtgl($row['tglpost'],'full') ?></span><br>
                            </span>
                            <?php
                                echo $row['post'];
                                if ($row['attach']!='') {
                                    echo "<br><i class='small'>File : </i>";
                                    $arr = explode(';', trim($row['attach']));
                                    for ($i=0; $i<count($arr); $i++) {
                                        $nmfile  = $arr[$i];
                                        $urlfile = site_url("files/forum/$nmfile");
                                        if ( $i<count($arr)-1 ) $nmfile .= ",";
                                        echo "<i class='small'><a href='$urlfile' target='_blank'>$nmfile </a></i>";
                                    }
                                }
                            ?>
                            <br>
                            <div class="small comment more shortened pull-right">
                                <a href="#"><i class="fa fa-thumb-tack"></i> Sticky</a>&nbsp;&nbsp;&nbsp;
                                <a href="#"><i class="fa fa-user-secret"></i> Lapor</a>&nbsp;&nbsp;&nbsp;
                                <a href="<?php echo site_url('forum?q=BP9VO&idparent='. $row['idparent'] .'&idchild='. $row['idchild'] ) ?>"><i class="fa fa-reply"></i> Reply</a>&nbsp;&nbsp;&nbsp;
                                <a href="<?php echo site_url('forum?q=BP9VO&idparent='. $row['idparent'] .'&idchild='. $row['idchild'] .'&idforum='. $row['idforum'] ) ?>"><i class="fa fa-commenting"></i> Quote</a>
                            </div>
                        </div>
                    </div>
                    <div id="<?php echo 'reply_'. $row['idforum'] ?>" class="box-footer div-reply" style="display: none"></div>
                </div>

            <?php } ?>

            <div class="box-footer" style="padding-top:0px;padding-bottom:0px "><?php $this->load->view('forum/v_forum_summernote'); ?></div>

            <div class="box-footer" style="border-top: 1px solid #D2D6DE; padding: 5px 10px 5px 10px;background :#F7F7F7">
                <span class="pull-right"><?php echo $this->pagination->create_links(); ?></span>
            </div>
        </div>
    </div>


    <div class="col-md-3 hidden-xs"><?php $this->load->view('forum/v_forum_top_thread'); ?></div>
    <div class="col-md-3 hidden-xs"><?php $this->load->view('forum/v_forum_last_thread'); ?></div>
    <div class="col-md-3 hidden-xs"><?php $this->load->view('forum/v_forum_carousel'); ?></div>

    <script type="text/javascript">
        $('.pagination').addClass('pagination-sm no-margin pull-right');
    </script>

</div>
