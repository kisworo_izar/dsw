<script src="<?=base_url('assets/plugins/slimScroll/jquery.slimscroll.min.js'); ?>" type="text/javascript"></script>
<script src="<?=base_url('assets/plugins/jQueryUI/jquery-ui.1.11.4.min.js'); ?>" type="text/javascript"></script>
<link rel="stylesheet" href="<?=base_url('assets/plugins/jQueryUI/jquery-ui.css'); ?>">

<div class="row">
    <div class="col-md-9">
        <?php $this->load->view('forum/v_forum_thread_artikel'); ?>
		<div class="box box-widget" style="margin-bottom:10px">
			<?php $this->load->view('forum/v_forum_summernote'); ?>
		</div>
    </div>

    <div class="col-md-3 hidden-xs"><?php $this->load->view('forum/v_forum_top_thread'); ?></div>
    <div class="col-md-3 hidden-xs"><?php $this->load->view('forum/v_forum_last_thread'); ?></div>
    <div class="col-md-3 hidden-xs"><?php $this->load->view('forum/v_forum_carousel'); ?></div>
</div>


