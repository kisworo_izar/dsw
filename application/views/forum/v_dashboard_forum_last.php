<?php
    // if ($limit==null)
    $limit=5;
    $query = $this->db->query("Select concat(idparent,'.',idchild) idkey, idparent, idchild, max(tglpost) tglpost, count(*) reply From d_forum Group By 1,2,3 Order By tglpost Desc Limit $limit");
    $d_last= $this->fc->ToArr( $query->result_array(), 'idkey');

    foreach ($d_last as $key=>$value) {
        // Forum Room
        $query = $this->db->query("Select nmroom From d_forum_room where idparent=". $value['idparent'] ." and idchild=". $value['idchild']);
        $room  = $query->row_array();
        // echo '<pre>'; print_r($room);
        if(isset($d_last[$key]['nmroom'])){
            $d_last[ $key ]['nmroom'] = $room['nmroom'];
        }else{
            $d_last[ $key ]['nmroom'] = '';
        }
        // $d_last[ $key ]['nmroom'] = $room['nmroom'];

        // Forum Home of Room
        $query = $this->db->query("Select thread From d_forum where idparent=". $value['idparent'] ." and idchild=". $value['idchild'] ." and idforum=1");
        $home  = $query->row_array();
        $d_last[ $key ]['thread'] = $home['thread'];
    }
?>

<div class="box box-widget" style="margin-bottom:0px">
    <div class="box-header with-border" style="border-radius:0px;border-left: 1px solid red; padding-top:3px; padding-bottom:3px; background-color:#F7F7F7">
        <h3 class="box-title" style="color:red">Posting Terkini</h3>
    </div>

    <div class="box-body" style="padding:0px 10px 0px 10px">
        <ul class="products-list product-list-in-box">
            <?php foreach ($d_last as $row) {
                $url = site_url('forum?q=JFuBN&idparent=').$row['idparent'].'&idchild='.$row['idchild'];
                if ($this->session->userdata('idusergroup')=='999') $url = '#'; ?>
                <li class="item" style="padding:3px">
                    <div class="product-info" style="margin-left:0px">
                        <span class="small product-description">
                            <a href="<?php echo $url ?>"><?php echo $row['thread'] ?></a>
                            <span class="small text text-danger pull-right"> <i class="text-info fa fa-reply"></i> Replies : <b><?php echo $row['reply']-1 ?></b></span>
                        </span>
                    </div>
                </li>
            <?php } ?>
        </ul>
    </div>
    <div class="box-header with-border" style="border-radius:0px;border-left: 1px solid orange; padding-top:3px; padding-bottom:3px; background-color:#F7F7F7">
        <h3 class="box-title" style="color:orange">Tanggapan Terkini</h3>
    </div>
</div>
