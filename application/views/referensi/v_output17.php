<script src="<?=base_url('assets/plugins/w2ui/w2ui-1.5.rc1.min.js') ?>" type="text/javascript"></script>
<link href="<?= base_url('assets/plugins/w2ui/w2ui-1.5.rc1.min.css') ?>" rel="stylesheet" type="text/css" />
<link href="<?= base_url('assets/plugins/select2/select2.css');?>" rel="stylesheet" type="text/css" />
<script src="<?=base_url('assets/plugins/select2/select2.full.min.js'); ?>" type="text/javascript"></script>


<div class="row">
   <div class="col-md-12">
      <div class="box box-widget" style="margin-bottom:0px">
         <div class="box-header with-border">

            <div class="col-sm-6" style="padding:0px 3px">
              <div class="form-group" style="margin-bottom:0px">
                <select class="org form-control" id="dept" onchange="changeDept( this.value )">
                <option value="" hidden> - Pilih Kementerian / Lembaga - </option>
                  <?php
                  $sel = 'selected'; $dep = '';
                  foreach ($refr['dept'] as $row) {
                    echo '<option value="'. $row['kode'] .'" '. $sel .'>'. $row['kode'] .' - '. $row['uraian'] .'</option>';
                    if ($sel == 'selected') { $dep = $row['kode']; $sel = ''; }
                  } ?>
                </select>
              </div>
            </div>

            <div class="col-sm-6" style="padding:0px 3px">
             <div class="form-group" style="margin-bottom:0px">
               <select class="org form-control" id="unit" onchange="changeRecords( this.value )">
               <option value="" hidden> - Pilih Unit Eselon I - </option>
                  <?php foreach ($refr['unit'] as $row) {
                    if (substr($row['kode'],0,3) == $dep) echo '<option value="'. $row['kode'] .'">'. $row['kode'] .' - '. $row['uraian'] .'</option>';
                  } ?>
               </select>
              </div>
            </div>
         </div>
      </div>
   </div>
</div>

<div id="main" style="width: 100%; height: 400px;"></div>

<script type="text/javascript">
  $(document).ready(function() {
    $(".org").select2({
      minimumResultsForSearch:5
    });
  });
</script>


<script type="text/javascript">
  var txtTop = $('#atas').html(); $('#atas').remove(),
      pstyle = 'border: 1px solid #dfdfdf style="padding-left: 10px"; padding: 5px;';

  var table = <?= json_encode($table) ?>, unit = <?= json_encode($refr['unit']) ?>, cExpid = '',
    config = {
    layout: { name: 'layout', padding: 0,
      panels: [
        { type: 'main', minSize: 550, overflow: 'hidden' },
        { type: 'right', size: $(window).width() * 0.5, resizable: true, hidden: true, style: pstyle, content: 'right' }
      ]
    },

    grdstk: {
      name: 'grdstk', show: { toolbar: true, footer: true },
      columns: [
        { field: 'kode', caption: '<center>Kode</center>', size: '120px' },
        { field: 'uraian', caption: '<center>Nomenklatur</center>', size: '100%' },
      ],
      records: table['list']
    },

    grdright: {
      name: 'grdright', header: 'Detail',
      show: { header: true, columnHeaders: false },
      columns: [
        { field: 'name', caption: 'Name', size: '125px', style: 'background-color: #efefef; border-bottom: 1px solid white; padding-right: 5px;', attr: "align=right" },
        { field: 'value', caption: 'Value', size: '100%' }
      ]
    }

  };

  $(function () {
    $('#main').css('height', $(window).height() * 0.84);
    $('#main').w2layout(config.layout);
    w2ui.layout.content('main', $().w2grid(config.grdstk));
    w2ui.layout.content('right', $().w2grid(config.grdright));

    // Grid Expand, Click & Right Click
    w2ui.grdstk.on('*', function (event) {
      if (event.type == 'click') {
        var id = event.recid, lvl = table.list[id].level;
        if (lvl == 't_output') {
          if (cExpid == '') { w2ui['layout'].toggle('right', window.instant); cExpid = id; }
          else if (cExpid == id) { w2ui['layout'].toggle('right', window.instant); cExpid = ''; }
          else cExpid = id;
          console.log('lvl :'+lvl);
          console.log('id :'+id);
          console.log('cExpid :'+cExpid);

          var arr = table.list[id].info;
          w2ui.grdright.clear();
          w2ui.grdright.add(arr);
        } else {
          if (cExpid != '') { w2ui['layout'].toggle('right', window.instant); cExpid = ''; }
        }
      }
    });
  });

  function changeDept( nil ) {
    var opt = '<option value="" hidden>*** Pilih Unit Eselon I ***</option>',
        arr = unit.filter( function(e) { return e.id.substr(0, 3) == nil});

    arr.forEach( function(v) { opt += '<option value="'+ v.kode +'">'+ v.kode +' - '+ v.uraian +'</option>'; });
    $('#unit').html(opt);
    ajaxDept( nil );
  };

  function changeRecords( nil ) {
    var arr = table['list'].filter( function(e) { return e.id.substr(0, 6) == nil });

    w2ui.grdstk.clear();
    w2ui.grdstk.records = arr;
    w2ui.grdstk.refresh();
  };

  function ajaxDept( kddept ) {
    $.ajax({ url : "<?php echo site_url('referensi17/ajaxDept/output') ?>", type: "POST", data: { 'kddept': kddept } })
    .done( function (arr) {
      table['list'] = JSON.parse(arr);
      w2ui.grdstk.clear();
      w2ui.grdstk.records = JSON.parse(arr);
      w2ui.grdstk.refresh();
    })
  }
</script>
