<script src="<?= base_url('assets/plugins/w2ui/w2ui-1.5.rc1.min.js') ?>" type="text/javascript"></script>
<link href="<?= base_url('assets/plugins/w2ui/w2ui-1.5.rc1.min.css') ?>" rel="stylesheet" type="text/css" />

<div class="box box-widget">
  <div class="box-body">
    <form role="form" action="<?php echo site_url("referensi?q=9l05r") ?>" method="post">
      <div class="input-group" style="margin-bottom:10px">
        <input type="hidden" name="nmfunction" value="dash">
        <input type="text" name="cari" class="form-control" placeholder="Pencarian..." value="<?php echo $this->session->userdata('cari') ?>" autocomplete="off">
        <span class="input-group-btn">
          <button type="submit" name="search" value="search" id="search-btn" class="btn btn-info btn-flat"><i class="fa fa-search" style="height:16px;margin-top:4px"></i></button>
        </span>
        <span class="input-group-btn">
          <button type="submit" name="search" value="clear" class="btn btn-info btn-flat"><i class="fa fa-times-circle" style="height:16px;margin-top:4px"></i></button>
        </span>
      </div>
      <div id="main" style="width: 100%; height: 400px; margin-bottom:10px"></div>
      <?php echo $this->pagination->create_links(); ?>
    </form>
  </div>
</div>

<script type="text/javascript">
  var table = <?= json_encode($table) ?>,
    config = {
      layout: {
        name: 'layout',
        padding: 0,
        attr: "align=center",
      },
      panels: [{
        type: 'main',
        minSize: 550,
        overflow: 'hidden'
      }],
      grdreg: {
        name: 'grdreg',
        show: {
          toolbar: false,
          footer: false,
          lineNumbers: false,
          selectColumn: false,
          expandColumn: false
        },
        columns: [{
            field: 'istilah',
            caption: '<center>Istilah</center>',
            size: '120px',
            sortable: true,
            attr: "align=left"
          },
          {
            field: 'tema',
            caption: '<center>Tema</center>',
            size: '160px',
            sortable: true,
            attr: "align=left"
          },
          {
            field: 'penjelasan',
            caption: '<center>Penjelasan</center>',
            size: '100%',
            sortable: true,
            attr: "align=left",
          },
        ],
        onExpand: function(event) {
          // console.log(table[0]['istilah']);
          // var html = w2ui['grdreg'].getRecordHTML(0, 1);
          // console.log(html);
          $rec_no = w2ui['grdreg'].getSelection() - 1;
          $('#' + event.box_id).html('<div style="padding: 10px; height: 100px"><b>' + table[$rec_no]['istilah'] + '</b><br><br><i>' + table[$rec_no]['penjelasan'] + '</i></div>');

        },
        records: table
      }
    };

  $(function() {
    $('#main').css('height', $(window).height() * 0.73);
    $('#main').w2layout(config.layout);
    w2ui.layout.content('main', $().w2grid(config.grdreg));
  });

  $('.pagination').addClass('pagination-sm no-margin pull-right');
</script>