<link href="<?= base_url('assets/plugins/select2/select2.css'); ?>" rel="stylesheet" type="text/css" />
<script src="<?= base_url('assets/plugins/select2/select2.full.min.js'); ?>" type="text/javascript"></script>
<style>
  td {
    padding-top: 4px !important;
    padding-bottom: 4px !important;
  }
</style>
<div class="box box-widget">
  <input type="hidden" id="aktif">
  <input type="hidden" id="strlen" value="0">

  <div class="box-header" style="padding-bottom:0px">
    <div class="row">

      <div class="col-sm-6" style="padding-bottom:3px">
        <div class="form-group" style="margin-bottom:0px">
          <select class="org form-control" id="dept" onchange="changeDept( this.value )">
            <option value="" hidden disabled> - Kementerian / Lembaga - </option>
            <option value="015" selected>015 - Kementerian Keuangan</option>';
          </select>
        </div>
      </div>
      <div class="col-sm-6" style="padding-bottom:10px">
        <div class="form-group" style="margin-bottom:0px">
          <select class="org form-control" id="unit" onchange="changeRecords( this.value )">
            <option value="" hidden disabled> - Pilih Unit - </option>

            <?php foreach ($refr['unit'] as $row) {
              $sel = '';
              if ($row['kode'] == substr($refr['pilih'], 0, 6)) $sel = 'selected';
              echo '<option value="' . $row['kode'] . '" ' . $sel . '>' . $row['kode'] . ' - ' . $row['uraian'] . '</option>';
            } ?>
          </select>
        </div>
      </div>

      <div class="col-sm-6" style="padding-bottom:3px ;<?php if ($this->session->userdata('idusergroup') == 'usersatker') echo "display: block;" ?>">
        <div class="form-group" style="margin-bottom:0px">
          <select class="org form-control" id="sat" onchange="changesatker(this.value )">
            <option value="" hidden disabled> - Pilih Satker - </option>
            <?php foreach ($refr['satker'] as $row) {
              $sel = '';
              if ($row['kode'] == substr($refr['pilih'],0,13)) $sel = 'selected';
              echo '<option value="' . $row['kode'] . '" ' . $sel . '>' . $row['kode'] . ' - ' . $row['uraian'] . '</option>';
            } ?>
          </select>
        </div>
      </div>
      <div class="col-sm-6" style="padding-bottom:3px">
        <form action="<?php echo site_url("referensi?q=55btk") ?>" method="post" style="margin-bottom: 0px;">
          <div class="input-group">
            <input type="text" name="cari" class="form-control" placeholder="Pencarian Nomenklatur Akun..." value="<?php echo $this->session->userdata('cari') ?>" autocomplete="off">
            <span class="input-group-btn">
              <button type="submit" name="search" value="search" class="btn btn-primary btn-flat"><i class="fa fa-search" style="height:16px;margin-top:4px"></i></button>
            </span>
            <span class="input-group-btn">
              <button type="submit" name="search" value="clear" class="btn btn-primary btn-flat"><i class="fa fa-times-circle" style="height:16px;margin-top:4px"></i></button>
            </span>
          </div>
        </form>
      </div>
    </div>
  </div>

  <div class="box-body">
    <table id="myTable" class="table table-hover table-bordered" style="border-spacing: 1; width: 100%;">

      <thead style="font-size:14px">
        <tr style="color: #fff; background-color: #3568B6;">
          <th class="text-center" width="150px"> Level </th>
          <th class="text-center" width="95px"> Kode </th>
          <th class="text-center"> Nomenklatur </th>
        </tr>
      </thead>

      <tbody style="font-size:14px;padding:0px" id="data_ssb">
        <?php foreach ($tabel as $key => $row) {
          $text            = "";
          $data_toggle     = "";
          $collapse        = "";
          $id       = "";
          $data_target = "";
          // $plus = "";
          $button = "";
          if ($row['type'] == 'Kelompok_SSB') {
            // $plus = '<i class="fa fa-plus" aria-hidden="true" href="#"></i>';
            $data_target = '.' . str_replace('.', '_', $row['idhide']);
            $button = ' <span type="button" style="color:grey!important" data-toggle="collapse" data-target="' . $data_target . '" aria-expanded="false" "><i class="fa fa-plus" aria-hidden="true"></i></span>';


            // $text = $row['nmakun'];
            // $class = 'akun_ssb '. str_replace('.', '_', substr($row['idhide']) );
          }
          if ($row['type'] == 'Akun' || $row['type'] == 'Kompnen' || $row['type'] == 'RO' || $row['type'] == 'KRO' || $row['type'] == 'Subkomponen') {
            $id       = str_replace('.', '_', $row['idhide']);
            $collapse        = "collapse";
            if ($row['type'] == 'Akun') $text = $row['nmakun'];
          }

        ?>
        <?php if($row['type'] !='kdgiat') { ?>
          <tr style="<?= $row['styl'] ?>" data-target="<?= $data_target ?>" data-toggle="<?= $collapse ?>" class="<?= $collapse . ' ' . $id ?>">
            <td class="<?= $row['class'] ?>" style="<?= $row['styl'] ?>"> &nbsp; <?= $button ?> &nbsp; <?= $row['type'] ?> </td>
            <td class="text-left" class="<?= $row['class'] ?>"> <?= $row['kode'] ?> </td>
            <td class="<?= $row['class'] ?>"><?= $text; ?></td>
          <?php } ?>
          </tr>
        <?php  } ?>
      </tbody>
    </table>
    <div class="text-orange" style="padding: 10px 5px 5px 0px">Click <i class=" fa fa-plus" aria-hidden="true"> </i> untuk menampilkan rincian SSB</div>

  </div>
</div>

<script>
  $(document).ready(function() {
    $(".org").select2({
      minimumResultsForSearch: 5
    });
  });
</script>

<script type="text/javascript">
  $('.skmpnen').click(function() {
    var id = $(this).attr('id');
    if ($('.' + id).style.display === "block") {
      $('.' + id).hide();
    } else {
      $('.' + id).show();

    }
  });
</script>

<script>
  function changeRecords(nil) {
    var depuni = $('#dept').val().substr(0, 3) + '.' + $('#unit').val().substr(4, 2);
    var sat = $('#sat').val();

    window.location.href = '<?= site_url('referensi?q=55btk&kddepuni=') ?>' + depuni;
  };

  function changesatker(sat) {
    var depuni = $('#dept').val().substr(0, 3) + '.' + $('#unit').val().substr(4, 2);
    var sat = $('#sat').val();

    window.location.href = '<?= site_url('referensi?q=55btk&kddepuni=') ?>' + depuni + '&sat=' + sat;
  }

  function ajaxDept(nil) {
    var sat = $('#sat').val();
    window.location.href = '<?= site_url('referensi?q=55btk&kddepuni=') ?>' + nil + '&sat=' + sat;
  }
</script>