<script src="<?= base_url('assets/plugins/w2ui/w2ui-1.5.rc1.js') ?>" type="text/javascript"></script>
<link href="<?= base_url('assets/plugins/w2ui/w2ui-1.5.rc1.min.css') ?>" rel="stylesheet" type="text/css" />
<link href="<?=base_url('assets/plugins/select2/select2.css');?>" rel="stylesheet" type="text/css" />
<script src="<?=base_url('assets/plugins/select2/select2.full.min.js'); ?>" type="text/javascript"></script>


<div class="row">
   <div class="col-md-12">
      <div class="box box-widget" style="margin-bottom:0px">
         <div class="box-header with-border">

            <div class="col-sm-6" style="padding:0px 3px">
              <div class="form-group" style="margin-bottom:0px">
                <select class="org form-control" id="dept" onchange="changeDept( this.value )">
                <option value="" hidden> - Pilih Kementerian / Lembaga - </option>
                  <?php
                  $sel = 'selected'; $dep = '';
                  foreach ($refr['dept'] as $row) {
                    echo '<option value="'. $row['kode'] .'" '. $sel .'>'. $row['kode'] .' - '. $row['uraian'] .'</option>';
                    if ($sel == 'selected') { $dep = $row['kode']; $sel = ''; }
                  } ?>
                </select>
              </div>
            </div>

            <div class="col-sm-6" style="padding:0px 3px">
             <div class="form-group" style="margin-bottom:0px">
               <select class="org form-control" id="unit" onchange="changeRecords( this.value )">
               <option value="" hidden> - Pilih Unit Eselon I - </option>
                  <?php foreach ($refr['unit'] as $row) {
                    if (substr($row['kode'],0,3) == $dep) echo '<option value="'. $row['kode'] .'">'. $row['kode'] .' - '. $row['uraian'] .'</option>';
                  } ?>
               </select>
              </div>
            </div>
         </div>
      </div>
   </div>
</div>


<div id="tabs-main" style="width: 100%"></div>
<div id="tabs-pilih" style="height: 300px"></div>

<script type="text/javascript">
  $(document).ready(function() {
    $(".org").select2({
      minimumResultsForSearch:5
    });
  });
</script>


<script type="text/javascript">
  $(document).ready(function() {
    $(".org").select2({
      minimumResultsForSearch:5
    });
  });
</script>

<script type="text/javascript">
  var table = <?= json_encode($table) ?>, TabsAktive = '', unit = <?= json_encode($refr['unit']) ?>,
  itabs = {
    tabref: { name: 'tabref', active: 't_nawacita',
      tabs: [
        { id: 'prioritas', text: 'Prioritas Nasional' },
        { id: 'nawacita', text: 'Nawacita' },
        { id: 'janpres', text: 'Janji Presiden' },
        { id: 'tema', text: 'Tematik' }],
      onClick: function (event) {
        var ev = event.target;
        w2ui.grdstk.records = table[ev];
        w2ui.grdstk.render();
      }
    },

    grdstk: {
      name: 'grdstk', show: { toolbar: true, footer: true },
      columns: [
        { field: 'kode', caption: '<center>Kode</center>', size: '100px', attr: 'align="left"' },
        { field: 'uraian', caption: '<center>Nomenklatur</center>', size: '100%' },
      ],
      records: table['prioritas']
    }
  };

  $(function () {
    $('#tabs-main').w2tabs(itabs.tabref);
    $('#tabs-pilih').css('height', $(window).height() * 0.76);
    $('#tabs-pilih').w2grid(itabs.grdstk);
  });

  function changeDept( nil ) {
    var opt = '<option value="" hidden>*** Pilih Unit Eselon I ***</option>',
        arr = unit.filter( function(e) { return e.id.substr(0, 3) == nil});
    arr.forEach( function(v) { opt += '<option value="'+ v.kode +'">'+ v.kode +' - '+ v.uraian +'</option>'; });
    $('#unit').html(opt);
    ajaxDept( nil );
  };

  function changeRecords( nil ) {
    var arr = table['list'].filter( function(e) { return e.id.substr(0, 6) == nil });

    w2ui.grdstk.clear();
    w2ui.grdstk.records = arr;
    w2ui.grdstk.refresh();
  };

  function ajaxDept( kddept ) {
    $.ajax({ url : "<?php echo site_url('referensi/ajaxDept') ?>", type: "POST", data: {'tabel': 'prinas', 'kddept': kddept} })
    .done( function (arr) {
      table = JSON.parse(arr);
      w2ui.grdstk.clear();
      w2ui.grdstk.records = table['prioritas'];
      w2ui.grdstk.refresh();
    })
  }
</script>
