<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Referensi extends CI_Controller {
	function __construct() {
		parent::__construct();
	    $this->load->model('referensi_model');
		$this->load->model('sbm_model');
		$this->session->set_userdata( array('cari' => '') );
		$this->fc->MemVar();
	}

	function index() {
		if (! $this->session->userdata('isLoggedIn')) redirect("login/show_login");
		if (isset($_GET['q'])) $menu = $_GET['q']; else show_404();
		if (! $this->fc->check_akses("referensi?q=4d74c")) show_404();

		if (isset($_GET['kode'])) 	  $kode = (int)$_GET['kode']; else $kode = '101';
		if (isset($_GET['per_page'])) $page = (int)$_GET['per_page']; else $page = 0;
		if (isset($_POST['search'])) {
			if ($_POST['search'] == 'search') $this->session->set_userdata('cari', strtolower($_POST['cari']));
			else $this->session->set_userdata('cari', '');
		}

		if 	  ($menu == '7efe4') $this->prinas();
		else if ($menu == '12377') $this->output();
		else if ($menu == '4d74c') $this->satker();
		else if ($menu == 'ab090') echo json_encode( $this->ajaxDept() );
		else if ($menu == 'd5bb7') $this->register($page);
		else if ($menu == 'aa746') $this->rekap_sbm();
		else if ($menu == '0558b') $this->detil_sbm($kode);
		else show_404();
	}

	private function prinas() {
		$this->fc->log('Referensi - Prioritas');
		$data['bread'] = array('header'=>'Prioritas Nasional'.'&nbsp'.$this->session->userdata('thang'), 'subheader'=>'Referensi');
		$data['view']  = "referensi/v_prinas";

		$depuni = $this->referensi_model->get_depuni();
		$kddept = $depuni['dept'][0];

		$data['refr']  = $depuni;
		$data['table'] = $this->referensi_model->get_prinas( $kddept['kode'] );
		$this->load->view('main/utama', $data);
	}

	private function output() {
		$this->fc->log('Referensi - Rencana Kinerja');
		$data['bread'] = array('header'=>'Rencana Kinerja'.'&nbsp'.$this->session->userdata('thang'), 'subheader'=>'Referensi');
		$data['view']  = "referensi/v_output";

		$depuni = $this->referensi_model->get_depuni();
		$kddept = $depuni['dept'][0];

		$data['refr']  = $depuni;
		$data['table'] = $this->referensi_model->get_output( $kddept['kode'] );
		$this->load->view('main/utama', $data);
	}

	private function satker() {
      $this->fc->log('Referensi - Satuan Kerja');
		$data['menu']  = $this->dsw_model->get_menu( $this->uri->segment(1) );
		$data['bread'] = array('header'=>'Satuan Kerja'.'&nbsp'.$this->session->userdata('thang'), 'subheader'=>'Referensi');
		$data['view']  = "referensi/v_satker";

		$depuni = $this->referensi_model->get_depuni();
		$kddept = $depuni['dept'][0];

		$data['refr']  = $depuni;
		$data['table'] = $this->referensi_model->get_satker( $kddept['kode'] );
		$this->load->view('main/utama', $data);
	}

	function ajaxDept() {
		$tbl = $_POST['tabel'];
		if (! $tbl ) redirect("home");
		if ($tbl == 'prinas') $table = $this->referensi_model->get_prinas( $_POST['kddept'] );
		if ($tbl == 'output') $table = $this->referensi_model->get_output( $_POST['kddept'] );
		if ($tbl == 'satker') $table = $this->referensi_model->get_satker( $_POST['kddept'] );
		echo json_encode( $table['list'] );
	}

	private function register($page) {
      $this->fc->log('Referensi - Register Pinjaman');

		$row = $this->referensi_model->get_register();
		$config = $this->fc->pagination( site_url("referensi?q=d5bb7"), count($row), 100, '3');
        $config['page_query_string'] = TRUE;
        $this->pagination->initialize($config);

		$data['table'] = array_slice($row, (int)$page, 100);
		$data['menu']  = $this->dsw_model->get_menu( $this->uri->segment(1) );
		$data['bread'] = array('header'=>'Register PHLN'.'&nbsp'.$this->session->userdata('thang'), 'subheader'=>'Referensi');
		$data['view']  = "referensi/v_register";
		$this->load->view('main/utama', $data);
	}

	private function rekap_sbm() {
		$this->fc->log('Referensi - SBM');
		$data['bread'] = array('header'=>'SBM'.'&nbsp'.$this->session->userdata('thang'), 'subheader'=>'SBM');
		$data['view']  = "sbm/v_sbm_rekap";

		$data['tabel'] = $this->sbm_model->get_rekap();
		$this->load->view('main/utama', $data);
	}

	private function detil_sbm( $kode ) {
		// echo $kode;exit();
		$data['tabel'] = $this->sbm_model->get_detil( $kode );
		// echo "<pre>";print_r($data['tabel']);exit();
		$data['bread'] = array('header'=>'Rincian SBM', 'subheader'=>'<a href="#" onclick="history.go(-1)">SBM</a>');
		$data['view']  = "sbm/v_sbm_detil";
		$this->load->view('main/utama', $data);
	}

}
