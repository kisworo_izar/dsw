<script src="<?= base_url('assets/plugins/w2ui/w2ui-1.5.rc1.js') ?>" type="text/javascript"></script>
<link href="<?= base_url('assets/plugins/w2ui/w2ui-1.5.rc1.min.css') ?>" rel="stylesheet" type="text/css" />

<div class="box box-widget">
   <div class="box-body">
      <form role="form" action="<?php echo site_url("referensi?q=d5bb7") ?>" method="post">
         <div class="input-group" style="margin-bottom:10px">
            <input type="hidden" name="nmfunction" value="dash">
            <input type="text" name="cari" class="form-control" placeholder="Pencarian..." value="<?php echo $this->session->userdata('cari') ?>" autocomplete="off">
            <span class="input-group-btn">
            <button type="submit" name="search" value="search" id="search-btn" class="btn btn-info btn-flat"><i class="fa fa-search" style="height:16px;margin-top:4px"></i></button>
            </span>
            <span class="input-group-btn">
            <button type="submit" name="search" value="clear" class="btn btn-info btn-flat"><i class="fa fa-times-circle" style="height:16px;margin-top:4px"></i></button>
            </span>
         </div>
      <div id="main" style="width: 100%; height: 400px; margin-bottom:10px"></div>
      <?php echo $this->pagination->create_links(); ?>
      </form>
   </div>
</div>

<script type="text/javascript">
  var table = <?= json_encode($table) ?>,
  config = {
    layout: { name: 'layout', padding: 0, attr: "align=center" },
      panels: [{ type: 'main', minSize: 550, overflow: 'hidden' }],
    grdreg: {
      name: 'grdreg', show: { toolbar: false, footer: false },
      columns: [
        { field: 'register',  caption: '<center>Register</center>',   size: '120px',  sortable: false, attr: "align=center" },
        { field: 'nonpln',    caption: '<center>No. NPLN</center>',   size: '160px',  sortable: false, attr: "align=center" },
        { field: 'tglnpln',   caption: '<center>Tgl. NPLN</center>',  size: '120px',  sortable: false, attr: "align=center" },
        { field: 'kdvalas',   caption: '<center>Valas</center>',      size: '80px',   sortable: false, attr: "align=center" },
        { field: 'pagu',      caption: '<center>Pagu</center>',       size: '120px',  sortable: false, attr: "align=right" },
        { field: 'nmdonor',   caption: '<center>Nama Donor</center>', size: '100%',   sortable: false, attr: "align=left" },
      ],
      records: table
    }
  };

  $(function () {
    $('#main').css('height', $(window).height() * 0.73);
    $('#main').w2layout(config.layout);
    w2ui.layout.content('main', $().w2grid(config.grdreg));
  });

  $('.pagination').addClass('pagination-sm no-margin pull-right');
</script>
