<style media="screen">
   .container {overflow-y: auto;padding: 0px;padding-top: 0px;width: 100%;
   }
</style>
<div class="box box-widget">
  <div class="box-header with-border">
    <div class="form-group text-center" style="padding: 0px">
      <input type="hidden" id="aktif">
      <input type="hidden" id="kdso">
      <input type="hidden" id="strlen" value="0">
      <button onclick="perdit(this.value)" value="00" id="kdso00" class="btn btn-flat btn-warning kdso">DJA</button>
      <button onclick="perdit(this.value)" value="01" id="kdso01" class="btn btn-flat btn-default kdso">Sekretariat</button>
      <button onclick="perdit(this.value)" value="02" id="kdso02" class="btn btn-flat btn-default kdso">P APBN</button>
      <button onclick="perdit(this.value)" value="03" id="kdso03" class="btn btn-flat btn-default kdso">Ekontim</button>
      <button onclick="perdit(this.value)" value="04" id="kdso04" class="btn btn-flat btn-default kdso">PMK</button>
      <button onclick="perdit(this.value)" value="05" id="kdso05" class="btn btn-flat btn-default kdso">Polhuk Hankam</button>
      <button onclick="perdit(this.value)" value="06" id="kdso06" class="btn btn-flat btn-default kdso">PNBP</button>
      <button onclick="perdit(this.value)" value="07" id="kdso07" class="btn btn-flat btn-default kdso">DSP</button>
      <button onclick="perdit(this.value)" value="08" id="kdso08" class="btn btn-flat btn-default kdso">HPP</button>
      <button onclick="perdit(this.value)" value="99" id="kdso99" class="btn btn-flat btn-default kdso">Lainnya</button>
    </div>
  </div>

   <div class="box-body">
      <div class="container">
        <table id="iGrid" class="table table-hover table-bordered" style="border-spacing: 1; width: 100%;">
          <thead>
            <tr style="color: #fff; background-color: #3c8dbc;">
              <th class="text-center">User ID</th>
              <th class="text-center">Nama User</th>
              <th class="text-center">NIP [Organisasi]</th>
              <th class="text-center">email</th>
              <th class="text-center">User Group</th>
            </tr>
            <tr id="tr_cari" style="margin: 0px" bgcolor="#fff">
              <td colspan="5">
                <div class="form-group pull-right">
                  <div class="input-group">
                    <span class="input-group-btn" style="padding-right: 5px">
                      <button id="btn_data" onclick="newadd()" class="btn btn-flat btn-default"><i class="fa fa-plus"></i></button>
                    </span>
                    <input id="cari" class="form-control" placeholder="Pencarian ..." value="<?php echo $this->session->userdata('cari') ?>">
                  </div>
                </div>
              </td>
            </tr>
          </thead>

          <tbody>
              <tr id="tr_crud" bgcolor="#eee">
                <td colspan="5"><div id="crud_form"><?php $this->load->view('admin/v_user_crud'); ?></div></td>
              </tr>

            <?php foreach ($tabel as $key=>$row) {              
              $class1 = implode('#', $row) .' 00 '. substr($row['kdso'],0,2);?>

              <tr id="id_<?= $row['iduser'] ?>" class="<?= $class1 ?>" title="00 <?= substr($row['kdso'],0,2) ?>" style="margin:0px;" onclick="move_row( this.id )">
                <td><?= $row['iduser'] ?></td>
                <td><?= $row['nmuser'] ?></td>
                <td><?= $row['nip'] ?></td>
                <td><?= $row['email'] ?></td>
                <td><?= $row['nmusergroup'] ?></td>
              </tr>

            <?php } ?>
          </tbody>
        </table>

      </div>
   </div>

</div>

<script type="text/javascript">
  // PENCARIAN
  $("#cari").keyup( function(event) {
    var kata = $('#cari').val();
    var kdso = $('#kdso').val();

    $('#tr_crud').insertAfter( $('#tr_cari') );
    $('#tr_crud').css('display', 'none');

    if (kata.length > $('#strlen').val()) {
      $('#strlen').val( kata.length );
      $('#iGrid > tbody  > tr:visible').each(function() {
        var id  = $(this).attr('id');
        var str = $('#'+id).text().toLowerCase();
        if (str.indexOf( kata.toLowerCase() ) < 0) $('#'+id).hide();
      });
    } else {
      $('#strlen').val( kata.length );
      $('#iGrid > tbody  > tr:hidden').each(function() {
        var id  = $(this).attr('id');
        var cls = $(this).attr('title');
        var str = $('#'+id).text().toLowerCase();
        if (cls.indexOf(kdso) >= 0) {
          if (str.indexOf( kata.toLowerCase() ) >= 0) $('#'+id).show();
        }
      });
    }
  });


  // Tombol DIREKTORAT
  function perdit( kdso ) {
    $('#kdso').val( kdso );
    $('.00').hide(); $('.'+kdso).show();
    $('.kdso').removeClass("btn-warning"); $('.kdso').addClass("btn-default");
    $('#kdso'+kdso).addClass("btn-warning"); $('#cari').val('');
    $('#tr_crud').css("display", "none");
  }


  // Click ROW untuk Buka Form CRUD
  function move_row( idkey ) {
    if ( $('#aktif').val()==idkey ) {
      $('#aktif').val('');
      $('#tr_crud').css('display', 'none');
    } else {
      var arr = $('#'+idkey).attr('class').split("#");
      $('#tr_crud').css('display', '');
      $('#tr_crud').insertAfter( $('#'+idkey) );
      $('input').each(function () {
        if ('aktif kdso cari'.indexOf( $(this).attr('id') ) < 0) $(this).val(arr[ $(this).attr('id') ]);
      })

      $("#8").val( arr[8] ).change();
      $("#0").prop('readonly', true);


      // var $myFoto = "<?= site_url('files/profiles/') ?>"+'/'+ arr[20];
      // console.log($myFoto);
      // $("#photo").attr('src', $myFoto);
      $("#photo").attr('src', "<?= site_url('files/profiles/') ?>"+'/'+ arr[4] +".gif");
      $('#Rekam').hide(); $('#Ubah').show(); $('#Hapus').show(); $('#aktif').val(idkey);
    }
  }


  // Click Tombol PLUS untuk Buka Form CRUD
  function newadd() {
    if ( $('#aktif').val()=='tr_crud' ) {
      $('#aktif').val('');
      $('#tr_crud').css('display', 'none');
    } else {
      $('#tr_crud').css('display', '');
      $('#tr_crud').insertAfter( $('#tr_cari') );
      $("input[ name='isian[]' ]").each(function () { $(this).val(''); })

      $("#8").val('').change();
      $("#0").prop('readonly', false);
      $("#photo").attr('src', "<?= site_url('files/profiles/_noprofile.png') ?>");
      $('#Rekam').show(); $('#Ubah').hide(); $('#Hapus').hide(); $('#aktif').val('tr_crud');
    }
  }
</script>
