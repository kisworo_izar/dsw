<style media="screen">
   .container {overflow-y: auto;padding: 0px;padding-top: 0px;width: 100%;
   }
</style>
<div class="box box-widget">
  <div class="box-header with-border">
     <div class="form-group text-center" style="padding: 0px">
      <input type="hidden" id="aktif">
      <input type="hidden" id="kdso" value="00">
      <button onclick="perdit(this.value)" value="00" id="kdso00" class="btn btn-flat btn-warning kdso">DJA</button>
      <button onclick="perdit(this.value)" value="01" id="kdso01" class="btn btn-flat btn-default kdso">Sekretariat</button>
      <button onclick="perdit(this.value)" value="02" id="kdso02" class="btn btn-flat btn-default kdso">P APBN</button>
      <button onclick="perdit(this.value)" value="03" id="kdso03" class="btn btn-flat btn-default kdso">Ekontim</button>
      <button onclick="perdit(this.value)" value="04" id="kdso04" class="btn btn-flat btn-default kdso">PMK</button>
      <button onclick="perdit(this.value)" value="05" id="kdso05" class="btn btn-flat btn-default kdso">Polhuk Hankam</button>
      <button onclick="perdit(this.value)" value="06" id="kdso06" class="btn btn-flat btn-default kdso">PNBP</button>
      <button onclick="perdit(this.value)" value="07" id="kdso07" class="btn btn-flat btn-default kdso">DSP</button>
      <button onclick="perdit(this.value)" value="08" id="kdso08" class="btn btn-flat btn-default kdso">HPP</button>
    </div>
  </div>

  <div class="box-body">
     <div class="container">
        <table id="iGrid" class="table table-hover table-bordered" style="border-spacing: 1; width: 100%;">
          <thead>
            <tr style="color: #fff; background-color: #3c8dbc;">
              <th class="text-center">Kode</th>
              <th class="text-center">Uraian Struktur Organisasi</th>
              <th class="text-center" width="85px">Kode Surat</th>
              <th class="text-center" width="85px">Intern</th>
              <th class="text-center" width="100px">Ekstern</th>
              <th class="text-center">Short</th>
            </tr>
            <tr id="tr_cari" style="margin: 0px" bgcolor="#fff">
              <td colspan="6">
                 <div class="form-group pull-right">
                  <div class="input-group">
                     <span class="input-group-btn" style="padding-right: 5px">
                      <button id="btn_data" onclick="newadd()" class="btn btn-flat btn-default"><i class="fa fa-plus"></i></button>
                    </span>
                    <input id="cari" class="form-control" placeholder="Pencarian ..." value="">
                  </div>
                </div>
              </td>
            </tr>
          </thead>

          <tbody>
              <tr id="tr_crud" style="display: none" bgcolor="#eee">
                <td colspan="6"><?php $this->load->view('admin/v_so_crud'); ?></td>
              </tr>

            <?php foreach ($t_so as $key=>$row) {
              $class1 = implode('#', $row) .'# 00 '. substr($row['kdso'],0,2);
              $bold = ''; if (substr($row['kdso'],4) == '0000') $bold = 'text-bold';
              $padding = '30px'; if (substr($row['kdso'],4,2) == '00') $padding = '15px'; if (substr($row['kdso'],2,4) == '0000') $padding = '0px'; ?>

              <tr id="id_<?= $row['kdso'] ?>" class="<?= $class1 .' '. $bold ?>" style="margin:0px;" onclick="move_row( this.id )">
                <td class="text-center"><?= $row['kdso'] ?></td>
                <td class="text-left" style="padding-left: <?= $padding ?>">  <?= $row['nmso'] ?></td>
                <td class="text-center"><?= $row['kdsosurat'] ?></td>
                <td class="text-center"><?= $row['intern'] ?></td>
                <td class="text-center"><?= $row['ekstern'] ?></td>
                <td class="text-left">  <?= $row['nmso1'] ?></td>
              </tr>

            <?php } ?>
          </tbody>
        </table>
     </div>

  </div>
</div>

<script type="text/javascript">
  // PENCARIAN
  $("#cari").keyup( function(event) {
    var kata = $('#cari').val();
    var kdso = $('#kdso').val();

    $('#tr_crud').insertAfter( $('#tr_cari') );
    $('#tr_crud').css('display', 'none');
    $('.'+kdso).show();

    $('#iGrid > tbody  > tr').each(function() {
      var id  = $(this).attr('id');
      var str = $('#'+id).text().toLowerCase();
      if (str.indexOf( kata.toLowerCase() ) < 0) $('#'+id).hide();
    });
  });


  // Tombol DIREKTORAT
  function perdit( kdso ) {
    $('#kdso').val( kdso );
    $('.00').hide(); $('.'+kdso).show();
    $('.kdso').removeClass("btn-warning"); $('.kdso').addClass("btn-default");
    $('#kdso'+kdso).addClass("btn-warning"); $('#cari').val('');
    $('#tr_crud').css("display", "none");
  }


  // Click ROW untuk Buka Form CRUD
  function move_row( idkey ) {
    if ( $('#aktif').val()==idkey ) {
      $('#aktif').val('');
      $('#tr_crud').css('display', 'none');
    } else {
      var arr = $('#'+idkey).attr('class').split("#");
      $('#tr_crud').css('display', '');
      $('#tr_crud').insertAfter( $('#'+idkey) );

      $("#0").prop('readonly', true);
      $("input[ name='isian[]' ]").each(function () { $(this).val(arr[ $(this).attr('id') ]); })
      $('#Rekam').hide(); $('#Ubah').show(); $('#Hapus').show(); $('#aktif').val(idkey);
    }
  }


  // Click Tombol PLUS untuk Buka Form CRUD
  function newadd() {
    if ( $('#aktif').val()=='tr_crud' ) {
      $('#aktif').val('');
      $('#tr_crud').css('display', 'none');
    } else {
      $("#0").prop('readonly', false);
      $('#tr_crud').css('display', '');
      $('#tr_crud').insertAfter( $('#tr_cari') );
      $("input[ name='isian[]' ]").each(function () { $(this).val(''); })
      $('#Rekam').show(); $('#Ubah').hide(); $('#Hapus').hide(); $('#aktif').val('tr_crud');
    }
  }

</script>
