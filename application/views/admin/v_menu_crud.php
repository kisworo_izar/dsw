<style media="screen">
  .form-group { margin: 0px;}
</style>


<div class="box box-widget" style="margin: 0px 0px 0px 0px;">
  <div class="box-body">
     <div class="row">
        <div class="col-sm-12">
           <div class="form-group">
               <label class="col-sm-2 control-label text-right" style="margin-top: 6px">Kode</label>
               <div class="col-sm-10" style="margin-bottom: 3px">
                <input id="0" name="isian[]" type="text" class="form-control" readonly value="">
               </div>
           </div>
           <div class="form-group">
               <label class="col-sm-2 control-label text-right" style="margin-top: 6px">Uraian</label>
               <div class="col-sm-10" style="margin-bottom: 3px">
                <input id="1" name="isian[]" type="text" class="form-control" value="">
               </div>
           </div>
           <div class="form-group">
               <label class="col-sm-2 control-label text-right" style="margin-top: 6px">Link</label>
               <div class="col-sm-10" style="margin-bottom: 3px">
                <input id="2" name="isian[]" type="text" class="form-control" value="">
               </div>
           </div>
           <div class="form-group">
               <label class="col-sm-2 control-label text-right" style="margin-top: 6px">Icon</label>
               <div class="col-sm-10" style="margin-bottom: 3px">
                <input id="3" name="isian[]" type="text" class="form-control" value="">
               </div>
           </div>
           <div class="form-group">
               <label class="col-sm-2 control-label text-right" style="margin-top: 6px">Urutan</label>
               <div class="col-sm-10" style="margin-bottom: 3px">
                <input id="4" name="isian[]" type="text" class="form-control" value="">
               </div>
           </div>
           <div class="form-group">
               <label class="col-sm-2 control-label text-right" style="margin-top: 6px">Keterangan</label>
               <div class="col-sm-10" style="margin-bottom: 3px">
                <input id="5" name="isian[]" type="text" class="form-control" value="">
               </div>
           </div>
           <div class="form-group">
               <label class="col-sm-2 control-label text-right" style="margin-top: 6px">On Off</label>
               <div class="col-sm-10" style="margin-bottom: 3px">
                <input id="6" name="isian[]" type="text" class="form-control" value="">
               </div>
           </div>
           <div class="form-group">
               <label class="col-sm-2 control-label text-right" style="margin-top: 6px"></label>
               <div class="col-sm-10" style="margin: 7px 0px">
                  <div class= "form-group text-right" style="padding-top: 10px">
                    <button id="Rekam" class="btn btn-flat btn-primary" onclick="crud( this.value )" value="Rekam">Rekam</button>
                    <button id="Ubah"  class="btn btn-flat btn-warning" onclick="crud( this.value )" value="Ubah">Ubah</button>
                    <button id="Hapus" class="btn btn-flat btn-danger" onclick="crud( this.value )" value="Hapus" >Hapus</button>
                  </div>

               </div>
           </div>
        </div>
     </div>
  </div>
</div>


<script type="text/javascript">
  function crud( aksi ) {
    var isian = new Array(),
        kdkey  = $('#aktif').val();
    $("input[ name='isian[]' ]").each(function() { isian.push( $(this).val() ); });

    $.ajax({
      url : "<?php echo site_url('admin_panel/crud_menu') ?>",
      type: "POST",
      data: { 'aksi': aksi, 'isian': isian, 'kdkey': kdkey },
      success: function(pesan) {
        if (aksi=='Rekam') { window.location.href = "<?php echo site_url('admin_panel/admin_menu') ?>" }
        if (aksi=='Ubah') {
          var arr = pesan.split("#");
          var cls = $('#'+kdkey).attr('class');
          $('#'+kdkey).attr('class', pesan);
          $('#'+kdkey).children( 'td:nth-child(1)' ).text( arr[0] );
          $('#'+kdkey).children( 'td:nth-child(2)' ).text( arr[1] );
          $('#'+kdkey).children( 'td:nth-child(3)' ).text( arr[2] );
          $('#'+kdkey).children( 'td:nth-child(4)' ).text( arr[3] );
          $('#'+kdkey).children( 'td:nth-child(5)' ).text( arr[4] );
          $('#'+kdkey).children( 'td:nth-child(6)' ).text( arr[5] );
          $('#'+kdkey).children( 'td:nth-child(7)' ).text( arr[6] );
          move_row( kdkey );
        }
        if (aksi=='Hapus') {
          move_row( kdkey );
          $('table#iGrid tr#'+kdkey).remove();
        }
      }
    })
  }
</script>
