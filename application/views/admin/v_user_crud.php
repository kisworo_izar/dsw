<link href= "<?=base_url('assets/plugins/select2/select2.css');?>" rel="stylesheet" type="text/css" />
<script src="<?=base_url('assets/plugins/select2/select2.full.min.js'); ?>" type="text/javascript"></script>

<script type="text/javascript">
  $(document).ready(function() {
    $(".seleksi").select2();
    $(".seleksi").select2({ minimumResultsForSearch: 5 });
    $('#tr_crud').css('display', 'none');
  });
</script>

<style media="screen">
  .form-group { margin: 0px;}
</style>


<div class="box box-widget" style="margin: 0px 0px 0px 0px;">
  <div class="box-body">

     <div class="row">
        <div class="col-sm-3">
           <?php
            $photo  = site_url('files/profiles/_noprofile.png');
           // if (file_exists('files/profiles/'. $kontak['nip'] .'.gif')) $photo  = site_url('files/profiles/'. $kontak['nip'] .'.gif');
           ?>

           <div class="thumbnail" style="border-radius: 50%; background:#C7C7C8;padding:5px">

            <img class="img-circle" id="photo" src="<?= $photo; ?>">

           </div>
        </div>
        <div class="col-sm-9">
           <div class="form-group">
               <label class="col-sm-3 control-label text-right" style="margin-top: 6px">ID User</label>
               <div class="col-sm-9" style="margin-bottom: 3px">
                <input id="0" name=" " type="text" class="form-control" readonly value="">
                <!-- <input id="0" name="isian[]" type="text" class="form-control" readonly value=""> -->
               </div>
           </div>
           <div class="form-group">
               <label class="col-sm-3 control-label text-right" style="margin-top: 6px">Nama</label>
               <div class="col-sm-9" style="margin-bottom: 3px">
                  <input id="1" name="isian[]" type="text" class="form-control" placeholder="" value="">
               </div>
           </div>
           <div class="form-group">
               <label class="col-sm-3 control-label text-right" style="margin-top: 6px">Nama Lengkap</label>
               <div class="col-sm-9" style="margin-bottom: 3px">
                  <input id="2" name="isian[]" type="text" class="form-control" placeholder="" value="">
               </div>
           </div>
           <input id="3" name="isian[]" type="hidden" cvalue="">
           <div class="form-group">
               <label class="col-sm-3 control-label text-right" style="margin-top: 6px">NIP [Organisasi]</label>
               <div class="col-sm-9" style="margin-bottom: 3px">
                  <input id="4" name="isian[]" type="text" class="form-control" placeholder="" value="">
               </div>
           </div>
           <div class="form-group">
               <label class="col-sm-3 control-label text-right" style="margin-top: 6px">Pangkat</label>
               <div class="col-sm-9" style="margin-bottom: 3px">
                  <input id="5" name="isian[]" type="text" class="form-control" placeholder="" value="">
               </div>
           </div>
           <div class="form-group">
               <label class="col-sm-3 control-label text-right" style="margin-top: 6px">Golongan</label>
               <div class="col-sm-9" style="margin-bottom: 3px">
                  <input id="6" name="isian[]" type="text" class="form-control" placeholder="" value="">
               </div>
           </div>
           <input id="7" name="isian[]" type="hidden" cvalue="">
           <div class="form-group">
               <label class="col-sm-3 control-label text-right" style="margin-top: 6px">Kode SO</label>
               <div class="col-sm-9" style="margin-bottom: 3px">
                  <select id="8" class="form-control seleksi">
                    <?php foreach ($t_so as $row) { ?>
                      <option value="<?= $row['kdso'] ?>"> <?= $row['kdso'] .' '. $row['nmso'] ?> </option>
                    <?php } ?>
                  </select>
               </div>
           </div>
           <div class="form-group">
               <label class="col-sm-3 control-label text-right" style="margin-top: 6px">Email</label>
               <div class="col-sm-9" style="margin-bottom: 3px">
                  <input id="9" name="isian[]" type="text" class="form-control" placeholder="" value="">
               </div>
           </div>
           <div class="form-group">
               <label class="col-sm-3 control-label text-right" style="margin-top: 6px">No Selular (HP)</label>
               <div class="col-sm-9" style="margin-bottom: 3px">
                  <input id="10" name="isian[]" type="text" class="form-control" placeholder="" value="">
               </div>
           </div>
           <input id="11" name="isian[]" type="hidden" cvalue="">
           <div class="form-group">
               <label class="col-sm-3 control-label text-right" style="margin-top: 6px">User Group</label>
               <div class="col-sm-9" style="margin-bottom: 3px">
                  <input id="12" name="isian[]" type="text" class="form-control" placeholder="" value="">
               </div>
           </div>
           <input id="13" name="isian[]" type="hidden" cvalue="">
           <input id="14" name="isian[]" type="hidden" cvalue="">
           <input id="15" name="isian[]" type="hidden" cvalue="">
           <input id="16" name="isian[]" type="hidden" cvalue="">
           <div class="form-group">
               <label class="col-sm-3 control-label text-right" style="margin-top: 6px">Jabatan</label>
               <div class="col-sm-9" style="margin-bottom: 3px">
                  <input id="17" name="isian[]" type="text" class="form-control" placeholder="" value="">
               </div>
           </div>
           <div class="form-group">
               <label class="col-sm-3 control-label text-right" style="margin-top: 6px">Eselon</label>
               <div class="col-sm-9" style="margin-bottom: 3px">
                  <input id="18" name="isian[]" type="text" class="form-control" placeholder="" value="">
               </div>
           </div>
           <div class="form-group">
               <label class="col-sm-3 control-label text-right" style="margin-top: 6px"></label>
               <div class="col-sm-9" style="margin: 7px 0px">
                  <div class="text-right">
                    <?php $grp = explode(";", $this->session->userdata('idusergroup')); ?>
                    <?php if(in_array('004', $grp)){ ?>
                      <span class="pull-left text-danger" >
                        <i class="text-danger fa fa-key" onclick=" reset( this.value )" value="reset"> </i> 
                      </span>                     
                     <?php } ?>
                     


                     <button id="Rekam" class="btn btn-flat btn-primary" onclick="crud( this.value )" value="Rekam">Rekam</button>
                     <button id="Ubah"  class="btn btn-flat btn-warning" onclick="crud( this.value )" value="Ubah">Ubah</button>
                     <button id="Hapus" class="btn btn-flat btn-danger" onclick="crud( this.value )" value="Hapus" >Hapus</button>
                  </div>
               </div>
           </div>
        </div>
     </div>
    <div class="col-sm-9 pull-right">
    </div>

  </div>
</div>


<script type="text/javascript">
  function crud( aksi ) {
    var isian = new Array(),
        kdkey  = $('#aktif').val();

    $("#crud_form input, select").each(function() { isian.push( $(this).val() ); });
    $.ajax({
      url : "<?php echo site_url('admin_panel/crud_user') ?>",
      type: "POST",
      data: { 'aksi': aksi, 'isian': isian, 'kdkey': kdkey },
      success: function(pesan) {
        if (aksi=='Rekam') { window.location.href = "<?php echo site_url('admin_panel/admin_user') ?>" }
        if (aksi=='Ubah') {
          var arr = pesan.split("#");
          $('#'+kdkey).attr('class', pesan);
          $('#'+kdkey).children( 'td:nth-child(1)' ).text( arr[0] );
          $('#'+kdkey).children( 'td:nth-child(2)' ).text( arr[1] );
          $('#'+kdkey).children( 'td:nth-child(3)' ).text( arr[4] );
          $('#'+kdkey).children( 'td:nth-child(4)' ).text( arr[6] );
          $('#'+kdkey).children( 'td:nth-child(5)' ).text( arr[17] );
          move_row( kdkey );
        }
        if (aksi=='Hapus') {
          move_row( kdkey );
          $('table#iGrid tr#'+kdkey).remove();
        }
      }
    })

    
  }
</script>

<script type="text/javascript">
  function reset(){
    id = $('#aktif').val();
    window.location.href = "<?php echo site_url('admin_panel/lps') ?>" + '/' + id;  
    // alert(kdkey);
  }

  // if (aksi =='reset') {
  //         alert(reset);
  //         var arr = pesan.split("#");
  //         alert(arr);
  //         // window.location.href = "<?php echo site_url('admin_panel/lps') ?>" 
  //       }
</script>
