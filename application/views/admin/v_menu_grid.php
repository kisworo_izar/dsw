<style media="screen">
   .container {overflow-y: auto;padding: 0px;padding-top: 0px;width: 100%;
   }
</style>
<div class="box box-widget">
  <input type="hidden" id="aktif">

  <div class="box-body">
     <div class="container">
        <table id="iGrid" class="table table-hover table-bordered" style="border-spacing: 1; width: 100%;">
          <thead>
            <tr style="color: #fff; background-color: #3c8dbc;">
              <th class="text-center">Kode</th>
              <th class="text-center">Uraian Menu</th>
              <th class="text-center">Link</th>
              <th class="text-center">Icon</th>
              <th class="text-center">Order</th>
              <th class="text-center">On Off</th>
            </tr>
            <tr id="tr_cari" style="margin: 0px" bgcolor="#fff">
               <td colspan="5">
                 <div class="form-group pull-right">
                  <div class="input-group">
                     <span class="input-group-btn" style="padding-right: 5px">
                      <button id="btn_data" onclick="newadd()" class="btn btn-default"><i class="fa fa-plus"></i></button>
                    </span>
                    <input id="cari" class="form-control" placeholder="Pencarian ..." value="">
                  </div>
                </div>
              </td>
            </tr>
          </thead>

          <tbody>
              <tr id="tr_crud" style="display: none" bgcolor="#eee">
                <td colspan="5"><?php $this->load->view('admin/v_menu_crud'); ?></td>
              </tr>

            <?php foreach ($tabel as $key=>$row) {
              $bold = ''; if (substr($row['idmenu'],2,4) == '0000') $bold = 'text-bold';
              $padding = '45px'; if (substr($row['idmenu'],4,2) == '0000') $padding = '25px'; if (substr($row['idmenu'],2,4) == '0000') $padding = '5px';
              ?>

              <tr id="id_<?= $row['idmenu'] ?>" class="<?= implode('#', $row) .'# All '. $bold ?>" style="margin:0px;" onclick="move_row( this.id )">
                <td class="text-center"><?= $row['idmenu'] ?></td>
                <td class="text-left" style="padding-left: <?= $padding ?>"> <?= $row['menu'] ?></td>
                <td class="text-left">  <?= $row['link'] ?></td>
                <td><?= $row['icon'] ?></td>
                <td class="text-center"><?= $row['urutan'] ?></td>
                <td class="text-center"><?= $row['onoff'] ?></td>
              </tr>

            <?php } ?>
          </tbody>
        </table>
     </div>
  </div>
</div>

<script type="text/javascript">
  // PENCARIAN
  $("#cari").keyup( function(event) {
    var kata = $('#cari').val();

    $('#tr_crud').insertAfter( $('#tr_cari') );
    $('#tr_crud').css('display', 'none');

    $('.All').show();
    $('#iGrid > tbody  > tr').each(function() {
      var id  = $(this).attr('id');
      var str = $('#'+id).text().toLowerCase();
      if (str.indexOf( kata.toLowerCase() ) < 0) $('#'+id).hide();
    });
  });


  // Click ROW untuk Buka Form CRUD
  function move_row( idkey ) {
    if ( $('#aktif').val()==idkey ) {
      $('#aktif').val('');
      $('#tr_crud').css('display', 'none');
    } else {
      var arr = $('#'+idkey).attr('class').replace(' text-bold','').split("#");
      $('#tr_crud').css('display', '');
      $('#tr_crud').insertAfter( $('#'+idkey) );

      $("#0").prop('readonly', true);
      $("input[ name='isian[]' ]").each(function () { $(this).val(arr[ $(this).attr('id') ]); })
      $('#Rekam').hide(); $('#Ubah').show(); $('#Hapus').show(); $('#aktif').val(idkey);
    }
  }


  // Click Tombol PLUS untuk Buka Form CRUD
  function newadd() {
    if ( $('#aktif').val()=='tr_crud' ) {
      $('#aktif').val('');
      $('#tr_crud').css('display', 'none');
    } else {
      $("#0").prop('readonly', false);
      $('#tr_crud').css('display', '');
      $('#tr_crud').insertAfter( $('#tr_cari') );
      $("input[ name='isian[]' ]").each(function () { $(this).val(''); })
      $('#Rekam').show(); $('#Ubah').hide(); $('#Hapus').hide(); $('#aktif').val('tr_crud');
    }
  }

</script>
