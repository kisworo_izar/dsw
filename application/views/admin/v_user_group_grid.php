<style media="screen">
   .container {overflow-y: auto;padding: 0px;padding-top: 0px;width: 100%;
   }
</style>
<div class="box box-widget">
  <input type="hidden" id="aktif">

  <div class="box-body">
     <div class="container">
        <table id="iGrid" class="table table-hover table-bordered" style="border-spacing: 1; width: 100%;">
          <thead>
            <tr style="color: #fff; background-color: #3c8dbc;">
              <th class="text-center">Kode</th>
              <th class="text-center" width="200px">User Group</th>
              <th class="text-center">Menu</th>
            </tr>
            <tr id="tr_cari" style="margin: 0px" bgcolor="#fff">
              <td colspan="5">
                 <div class="form-group pull-right">
                  <div class="input-group">
                     <span class="input-group-btn" style="padding-right: 5px">
                      <button id="btn_data" onclick="newadd()" class="btn btn-default"><i class="fa fa-plus"></i></button>
                    </span>
                    <input id="cari" class="form-control" placeholder="Pencarian ..." value="">
                  </div>
                </div>
              </td>
            </tr>
          </thead>

          <tbody>
              <tr id="tr_crud" style="display: none" bgcolor="#eee">
                <td colspan="5"><div id="crud_form"><?php $this->load->view('admin/v_user_group_crud'); ?></td>
              </tr>

            <?php foreach ($tabel as $row) {
              $class1 = $row['idusergroup'] .'#'. $row['nmusergroup'] .'#'. $row['menu'];
              $class2 = '';
              if ($row['daftar']) {
                foreach ($row['daftar'] as $value) {
                  $space = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
                  if (substr($value['idmenu'],4,2)=='00') $space = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
                  if (substr($value['idmenu'],2,4)=='0000') $space = '';
                  $class2 .= $value['idmenu'] ." - $space". $value['menu'] .'</span><br>';
                }
              }
              ?>

              <tr id="id_<?= $row['idusergroup'] ?>" class="<?= $class1 ?># All" style="margin:0px;" onclick="move_row( this.id )">
                <td class="text-center"><?= $row['idusergroup'] ?></td>
                <td class="<?= $class2 ?>">  <?= $row['nmusergroup'] ?></td>
                <td class="text-left">  <?= $row['menu'] ?></td>
              </tr>

            <?php } ?>
          </tbody>
        </table>
     </div>
  </div>
</div>

<script type="text/javascript">
  // PENCARIAN
  $("#cari").keyup( function(event) {
    var kata = $('#cari').val();

    $('#tr_crud').insertAfter( $('#tr_cari') );
    $('#tr_crud').css('display', 'none');

    $('.All').show();
    $('#iGrid > tbody  > tr').each(function() {
      var id  = $(this).attr('id');
      var str = $('#'+id).text().toLowerCase();
      if (str.indexOf( kata.toLowerCase() ) < 0) $('#'+id).hide();
    });
  });


  // Click ROW untuk Buka Form CRUD
  function move_row( idkey ) {
    if ( $('#aktif').val()==idkey ) {
      $('#aktif').val('');
      $('#tr_crud').css('display', 'none');
    } else {
      var arr = $('#'+idkey).attr('class').replace(' text-bold','').split("#");
      $('#tr_crud').css('display', '');
      $('#tr_crud').insertAfter( $('#'+idkey) );

      $("#0").prop('readonly', true);
      $("input[ name='isian[]' ]").each(function () { $(this).val(arr[ $(this).attr('id') ]); })

      $('#2').html( arr[2] );
      $('#daftar').html( $('#'+idkey).children( 'td:nth-child(2)' ).prop('class') );
      $('#Rekam').hide(); $('#Ubah').show(); $('#Hapus').show(); $('#aktif').val(idkey);
    }
  }


  // Click Tombol PLUS untuk Buka Form CRUD
  function newadd() {
    if ( $('#aktif').val()=='tr_crud' ) {
      $('#aktif').val('');
      $('#tr_crud').css('display', 'none');
    } else {
      $("#0").prop('readonly', false);
      $('#tr_crud').css('display', '');
      $('#tr_crud').insertAfter( $('#tr_cari') );
      $("input[ name='isian[]' ]").each(function () { $(this).val(''); })
      $('#Rekam').show(); $('#Ubah').hide(); $('#Hapus').hide(); $('#aktif').val('tr_crud');
    }
  }

</script>
