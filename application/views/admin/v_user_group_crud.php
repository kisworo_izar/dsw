<style media="screen">
  .form-group { margin: 0px;}
</style>


<div class="box box-widget" style="margin: 0px 0px 0px 0px;">
  <div class="box-body">

     <div class="row">
        <div class="col-sm-4 pull-right">
          <h5><b>Daftar Menu</b></h5>
          <div id="daftar" class="small"></div>
        </div>

        <div class="col-sm-8">
           <div class="form-group">
               <label class="col-sm-2 control-label text-right" style="margin-top: 6px">Kode</label>
               <div class="col-sm-10" style="margin-bottom: 3px">
                <input id="0" name="isian[]" type="text" class="form-control" readonly value="">
               </div>
           </div>
           <div class="form-group">
               <label class="col-sm-2 control-label text-right" style="margin-top: 6px">Group</label>
               <div class="col-sm-10" style="margin-bottom: 3px">
                <input id="1" name="isian[]" type="text" class="form-control" value="">
               </div>
           </div>
           <div class="form-group">
               <label class="col-sm-2 control-label text-right" style="margin-top: 6px">Menu</label>
               <div class="col-sm-10" style="margin-bottom: 3px">
                  <textarea id="2" name="isian[]" type="text" class="form-control" rows="10" placeholder=""></textarea>
               </div>
           </div>
           <div class="form-group">
               <label class="col-sm-2 control-label text-right" style="margin-top: 6px"></label>
               <div class="col-sm-10" style="margin: 7px 0px">
                  <div class= "form-group text-right" style="padding-top: 10px">
                    <button id="Rekam" class="btn btn-flat btn-primary" onclick="crud( this.value )" value="Rekam">Rekam</button>
                    <button id="Ubah"  class="btn btn-flat btn-warning" onclick="crud( this.value )" value="Ubah">Ubah</button>
                    <button id="Hapus" class="btn btn-flat btn-danger" onclick="crud( this.value )" value="Hapus" >Hapus</button>
                  </div>
               </div>
           </div>
        </div>
     </div>
    </div>
  </div>
</div>


<script type="text/javascript">
  function crud( aksi ) {
    var isian = new Array(),  kdkey = $('#aktif').val();
    $("#crud_form input, textarea").each(function() { isian.push( $(this).val() ); });

    $.ajax({
      url : "<?php echo site_url('admin_panel/crud_user_group') ?>",
      type: "POST",
      data: { 'aksi': aksi, 'isian': isian, 'kdkey': kdkey },
      success: function(pesan) {
        if (aksi=='Rekam') { window.location.href = "<?php echo site_url('admin_panel/admin_user_group') ?>" }
        if (aksi=='Ubah') { window.location.href = "<?php echo site_url('admin_panel/admin_user_group') ?>" }
        if (aksi=='Hapus') {
          move_row( kdkey );
          $('table#iGrid tr#'+kdkey).remove();
        }
      }
    })
  }
</script>
