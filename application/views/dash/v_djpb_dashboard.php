<link rel="stylesheet" href="<?=base_url('assets/plugins/Leaflet/leaflet.css'); ?>" />
<link rel="stylesheet" href="<?=base_url('assets/plugins/Leaflet-MiniMap-master/src/Control.MiniMap.css'); ?>" />

<script src="<?=base_url('assets/plugins/Leaflet/leaflet.js'); ?>"></script>
<script src="<?=base_url('assets/plugins/Leaflet-MiniMap-master/src/Control.MiniMap.js'); ?>"></script>

<style>
  #mapindonesia { height: 460px; }
  .info {padding: 6px 8px;font: 12px/14px Arial, Helvetica, sans-serif;background: white;background: rgba(255,255,255,0.8);box-shadow: 0 0 15px rgba(0,0,0,0.2);border-radius: 0px;}
  .info h4 {margin: 0 0 5px;color: #777;}
  .legend {text-align: left;line-height: 15px;color: #555;}
  .legend i {width: 15px;height: 15px;display: inline-block;margin-right: 3px;opacity: 0.7;}
</style>

<div class="row">

   <div class="col-md-6">
       <div class="dsw-info-box" style="padding-right:45px">
       <span class="dsw-info-box-icon bg-white"><i class="fa fa-street-view text-orange"></i></span>
           <div id="text-carousel" class="carousel slide" data-ride="carousel">
               <div class="carousel-inner" style="position:absolute; left:45px">
                   <?php
                     $i=1;
                     foreach ($d_rss as $row) {
                   ?>
                       <div class="item <?php if ($i==1) echo 'active'?>">
                           <div class="carousel-content dsw-info-box-content" style="margin-left:-10px">
                              <span class="dsw-info-box-text">
                                   <b><?php echo $row['site'] ?></b>  <?php echo $this->fc->idtgl($row['pubdate'],'full') ?>
                              </span>
                              <span class="dsw-info-box-text">
                                   <a href="<?php echo $row['link'] ?>" target="_blank"><?php echo $row['berita'] ?></a>
                              </span>
                           </div>
                       </div>
                   <?php
                       $i++;
                   }
                   ?>
               </div>
           </div>
       </div>
   </div>

   <div class="col-md-6">
      <div class="dsw-info-box">
         <span class="dsw-info-box-icon bg-white"><i class="fa fa-heartbeat text-red"></i></span>
            <div id="text-carousel" class="carousel slide" data-ride="carousel">
               <div class="carousel-inner" style="position:absolute; left:45px">
                   <?php
                     $i=1;
                     foreach ($notif as $row) {
                   ?>
                       <div class="item <?php if ($i==1) echo 'active'?>">
                           <div class="carousel-content dsw-info-box-content" style="margin-left:-10px">
                              <span class="dsw-info-box-text">
                                   <b><?php echo $row['notif1'] ?></b> 
                              </span>
                              <span class="dsw-info-box-text">
                                   <?php echo $row['notif2'] ?>
                              </span>
                           </div>
                       </div>
                   <?php
                       $i++;
                   }
                   ?>
               </div>
           </div>

      </div>
   </div>
</div>

<div class="row">
   <div class="col-md-12">
      <div id="mapindonesia">

         <?php
            if ($this->session->userdata('idusergroup') == '611' || $this->session->userdata('idusergroup') == '612') {
            ?>
            <?php echo  $this->session->userdata('nmalias'); ?>
         <div class="">
            <a href="Revisi_Kanwil">
               <div class="pull-right" style="padding-right:10px">
                  <div class="pull-right" style="font-size:13px;"><b>
                     <?php echo $this->session->userdata('jabatan') ?></b>
                     <div class="" style="font-size: 9px">



                        <?php if ($revisi) {
                           foreach ($revisi as $row) { ?>
                            <?php if($row['kdlokasi'] == $this->session->userdata('kdlokasi')) { ?>
                            <?php foreach($row['sub'] as $sub) {?>
                              <div class="clearfix" style="padding-top:2px">
                               <span class="pull-left"><?php echo($sub['nmstatus']) ?></span>
                               <small class="pull-right"><?php echo("<b>".$sub['jumlah_revisi']."</b> (".round($sub['persen'],2)." %)") ?></small>
                              </div>
                              <div class="progress xs" style="margin-bottom:0px">
                               <div class="progress-bar <?php echo($sub['warna']) ?>" style="width: <?php echo ($sub['persen']) ?>%;"></div>
                              </div>
                            <?php } ?>
                          <?php }?>
                          
                        <?php
                           }}
                        ?>
                     </div>
                  </div>

               </div>
            </a>

         </div>
         <?php
            }
         ?>

      </div>
   </div>
</div>

<div class="row">
   <div class="col-md-12">
      <div class="box box-widget">
         <div class="box-header with-border">
            <!-- <span class="box-title">Diskusi Revisi</span>
            <div class="small box-tools" style="padding-top:5px">
                <a href="<?php echo site_url('forum_revisi'); ?>"><i class="fa fa-link"></i></a>
            </div> -->
            <div class="box-title"><i class="fa fa-bullhorn"></i> Informasi</div>
            <br>
            <div class="box-body">
               <b>Panduan penggunaan DSW user Kanwil</b>: <a href="http://intranet.anggaran.depkeu.go.id/pandukanwil/">http://intranet.anggaran.depkeu.go.id/pandukanwil</a>
               <br>
               <br>
               Laporan jumlah revisi dalam format XLS dapat diperoleh melalui menu <b>Revisi Anggaran</b> 
               <br>
               Pilih Range tanggal untuk membatasi pencarian & click icon XLS (Laporan Revisi - Excel) untuk unduh data dalam format Excel.
            </div>
         </div>
      </div>
   </div>
</div>
<!-- <div class="box box-widget">
  <div class="box-body" style="padding:0px 10px">
    <span class="text-blue">Jumlah Revisi hari ini : <b>455</b></span>
    <span class="text-blue pull-right">Jumlah Revisi s.d. <?php echo date('d-m-Y '); ?> : <b>455</b></span>
  </div>
</div> -->



<script>
    var style = {
      "fillColor": "#ff7800",
      "fillOpacity": 0.8,
      "stroke": true,
      "color": "#03f",
      "weight": 1,
  };

  var geojson;
  // var map = L.map('mapindonesia').setView([-2.721933, 116.593895], 5);
  var map = L.map('mapindonesia').setView([-2, 116.5], 5);

  $.ajax({
  dataType: "json",
  url: "<?php echo site_url('djpb_dash_revisi/dash_map'); ?>",
  success: function(datasql) {
     indonesialayer(datasql);
     }
  });
  legenda();
  function indonesialayer(datasql)
  {
     $.ajax({
     dataType: "json",
     url: "<?=base_url('assets/plugins/Leaflet/provinsi.json'); ?>",
     success: function(data) {
         geojson=L.geoJSON(data,{
          onEachFeature:onEachFeature,
          style:style
         }).addTo(map);

         function onEachFeature(feature, layer) {
          var provinsi,persentase,jml,ket;

          layer.on({
             mouseover: highlightFeature,
             mouseout: resetHighlight,
          });
          for(var i=0;i<datasql.provinsi.length;i++)
          {
             if(datasql.provinsi[i].ID==feature.properties.ID2013)
             {
               jml=parseInt(datasql.provinsi[i].JML);
               if(jml<=0)
               { layer.setStyle({fillColor:"#9AD1CB"}); ket=""}
               else if(jml>0&&jml<=10)
               { layer.setStyle({fillColor:"#AFD35D"}); ket=""}
               else if(jml>10&&jml<=20)
               { layer.setStyle({fillColor:"#AFB4D7"}); ket=""}
               else if(jml>20&&jml<=30)
               { layer.setStyle({fillColor:"#F3B55C"}); ket=""}
               else if(jml>30)
               { layer.setStyle({fillColor:"#F3AD99"}); ket=""}
             provinsi=datasql.provinsi[i].PROVINSI;
            //  persentase=datasql.provinsi[i].PERSENTASE;
             }
          }

          var html="<b>"+provinsi+"</b><br>";
          html+="Jumlah Revisi : <b>"+jml+"</b><br>";
          // html+="Status : "+ket;
          layer.bindPopup(html);
         }
      }
     });
  }
  //fungsi menampilkan legenda
  function legenda()
  {
     var legend = L.control({position: 'bottomleft'});
     legend.onAdd = function (map) {
         var div = L.DomUtil.create('div', 'info legend'),
             grades = [0, 1, 2, 3, 4],
             labels = ['0', '1 - 10', '11 - 20', '21 - 30', ' > 30' ];
         for (var i = 0; i < grades.length; i++) {
             div.innerHTML +=
                 '<i style="background:' + getColor(grades[i] + 1) + '">&nbsp</i> ' +
                 labels[i]+'<b>&nbsp;'+'</b>'+'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
         }
         return div;
     };

     legend.addTo(map);
  }


  //fungsi menampilkan warna sesuai nilai
  function getColor(d) {
      return d > 4 ? '#F3AD99' :
             d > 3 ? '#F3B55C' :
             d > 2 ? '#AFB4D7' :
             d > 1 ? '#AFD35D' :
                     '#9AD1CB';
  }

  function highlightFeature(e) {
     var layer = e.target;

     layer.setStyle({
      weight: 2,
      color: '#ff0000',
      dashArray: '',
      fillOpacity: 0.7
     });

     if (!L.Browser.ie && !L.Browser.opera && !L.Browser.edge) {
      layer.bringToFront();
     }
  }

  function resetHighlight(e) {
     var layer=e.target;

     layer.setStyle({
      stroke: true,
      color: "#03f",
      weight: 1,
     })
  }

</script>
