<div class="row">
	<div class="col-md-12">
		<div class="box box-widget">
			<div class="box-header with border">
                <div id="main" style="height:500px"></div>
            </div>
        </div>
    </div>
</div>
<!-- <?php echo($chart['datachart']['data']); ?> -->

<script src="<?=base_url('assets/plugins/echarts3/echarts.min.js'); ?>" type="text/javascript"></script>
<script type="text/javascript">
    var myChart = echarts.init(document.getElementById('main'));
    var option = {
        title : {
                text: '<?echo $chart['title'] ?>',
                subtext: '<?echo $chart['subtitle'] ?>',
                x:'center'
            },
            tooltip : {
                trigger: 'item',
                formatter: "{a} <br/>{b} : {c} ({d}%)"
            },
            legend: {
                x : 'center',
                y : 'bottom',
                data:[<?php echo($chart['datachart']['legend']); ?>] // legend
            },
            calculable : true,
            series : [
                {
                    name:'<?echo $chart['name'] ?>',
                    type:'pie',
                    radius : [30, 150],
                    center : ['50%', 250],
                    roseType : 'pie',
                    label: {
                        normal: {
                            show: true
                        },
                        emphasis: {
                            show: true
                        }
                    },
                    lableLine: {
                        normal: {
                            show: true
                        },
                        emphasis: {
                            show: true
                        }
                    },
                    data:[
                        <?php echo($chart['datachart']['data']); ?> // datacart 1
                    ]
                }
            ]
    };

    myChart.setOption(option);
</script>
