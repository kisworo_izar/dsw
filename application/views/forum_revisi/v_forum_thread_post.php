<link rel="stylesheet" href="<?=base_url('assets/plugins/emojione/emojione.css'); ?>">
<script src="<?=base_url('/assets/plugins/emojione/emojione.js');?>" type="text/javascript"></script>

<div class="box box-widget" style="margin-bottom:10px">
    <div class="box-footer" style="border-top: 0px solid #D2D6DE; padding: 5px 10px 5px 10px;background :#FFF">
        <span class="pull-right"><?php echo $this->pagination->create_links(); ?></span>
    </div>

    <?php
    foreach ($d_post as $row) {
        $foto_profile="files/profiles/_noprofile.png";
        if (file_exists("files/profiles/".$row['nip'].".gif")) { $foto_profile =  "files/profiles/".$row['nip'].".gif"; } ?>

        <div class="box-footer box-comments" style="padding-bottom:0px; padding-top:5px; border-top:1px solid #D2D6DE;background :#f7f7f7">
            <div class="box-comment"  style="border-bottom: 0px;">
                <span class="profile-tooltip1">
                    <img class="img-circle img-sm" src="<?php echo site_url($foto_profile); ?>" alt="user image">
                        <span class="profile-tooltip-content"><img src="<?php echo site_url($foto_profile); ?>">
                        <span class="profile-tooltip-text" style="font-size:18px; padding-top:5px">
                            <?php echo $row['nmuser'] ?>
                        </span><br>
                        <span class="profile-tooltip-text"><?php echo trim($row['jabatan']) ?> </span><br>
                        <span class="profile-tooltip-text"><?php echo $row['nmso'] ?></span>
                    </span>
                </span>
                <div class="comment-text">
                    <span class="username">
                        <span class="profile-tooltip-item"><?php echo $row['nmuser'] ?> </span>
                        <span class="text-muted pull-right"><?php echo $this->fc->idtgl($row['tglpost'],'full') ?></span><br>
                    </span>
                    <div class="convert-emoji">
                    <?php
                        $this->fc->read_more( $row['post'] );
                        if ($row['attach']!='') {
                            echo "<i class='small'>File : </i>";
                            $arr = explode(';', trim($row['attach']));
                            for ($i=0; $i<count($arr); $i++) {
                                $nmfile  = $arr[$i];
                                $urlfile = site_url("files/forum_revisi/$nmfile");
                                if ( $i<count($arr)-1 ) $nmfile .= ",";
                                echo "<i class='small'><a href='$urlfile' target='_blank'>$nmfile </a></i>";
                            }
                        }
                    ?>
                    </div>
                    <div class="small comment more shortened pull-right">
                        <?php if ($this->session->userdata('idusergroup') != '999') { ?>
                            <!--
                            <a href="#"><i class="fa fa-thumb-tack"></i> Sticky</a>&nbsp;&nbsp;&nbsp;
                            <a href="#"><i class="fa fa-user-secret"></i> Lapor</a>&nbsp;&nbsp;&nbsp;
                            -->
                            <a href="<?php echo site_url('forum_revisi/reply/'. $row['idparent'] .'/'. $row['idchild'] ) ?>"><i class="fa fa-reply"></i> Reply</a>&nbsp;&nbsp;&nbsp;
                            <a href="<?php echo site_url('forum_revisi/reply/'. $row['idparent'] .'/'. $row['idchild'] .'/'. $row['idforum'] ) ?>"><i class="fa fa-commenting"></i> Quote</a>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <div id="<?php echo 'reply_'. $row['idforum'] ?>" class="box-footer div-reply" style="display: none"></div>
        </div>

    <?php } ?>

    <?php if ($this->session->userdata('idusergroup') != '999') { ?>
        <div class="box-footer"><?php $this->load->view('forum_revisi/v_forum_summernote'); ?></div>
    <?php } ?>

    <div class="box-footer" style="border-top: 1px solid #D2D6DE; padding: 5px 10px 5px 10px;background :#F7F7F7">
        <span class="pull-right"><?php echo $this->pagination->create_links(); ?></span>
    </div>
    <script type="text/javascript"> $('.pagination').addClass('pagination-sm no-margin pull-right'); </script>
</div>

<script type="text/javascript">
   $(document).ready(function() {
   $(".convert-emoji").each(function() {
       var original = $(this).html();
       var converted = emojione.shortnameToImage(original);
       console.log(emojione.shortnameToImage(original));
       $(this).html(converted);
   });
});
</script>
