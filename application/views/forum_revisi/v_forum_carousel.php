<div class="box box-widget">
    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
            <?php
            $min = 1; $max = 30;
            $ran = range($min, $max);
            shuffle($ran);

            for ($i=$min; $i < $max; $i++) {
            echo '<div class="item';
                if ($i==1) echo ' active'; echo '">';
            ?>
            <img src="<?php echo base_url("files/poster/poster$ran[$i]"); ?>.jpg">
            </div>
            <?php } ?>
        </div>
    </div>
</div>
