
<?php
     $dbrevisi = $this->load->database('revisi'. $this->session->userdata('thang'), TRUE);
    $query = $dbrevisi->query("Select doc_usulan_revisi, doc_matrix, doc_rka, doc_adk, doc_dukung_sepakat, doc_dukung_hal4, doc_dukung_lainnya From revisi Where rev_id='$rev_id'");
    $docs  = $query->row_array();
    //print_r($docs); exit;

    $link_F01 = '';  $link_FE1 = ''; $doc_F01 = '';  
    if ( $docs['doc_usulan_revisi']!='' And $docs['doc_usulan_revisi']!='' And file_exists("files/revisi_SatuDJA/$rev_id/". $docs['doc_usulan_revisi']) ) {
        $link_F01 = '<a href="'. site_url("files/revisi_SatuDJA/$rev_id/". $docs['doc_usulan_revisi']) .'">';  
        $link_FE1 = '</a>'; $doc_F01 = 'checked'; 
    }
    $link_F02 = '';  $link_FE2 = ''; $doc_F02 = '';  
    if ( $docs['doc_matrix']!='' And file_exists("files/revisi_SatuDJA/$rev_id/". $docs['doc_matrix']) ) {
        $link_F02 = '<a href="'. site_url("files/revisi_SatuDJA/$rev_id/". $docs['doc_matrix']) .'">';  
        $link_FE2 = '</a>'; $doc_F02 = 'checked'; 
    }
    $link_F03 = '';  $link_FE3 = ''; $doc_F03 = '';  
    if ( $docs['doc_rka']!='' And file_exists("files/revisi_SatuDJA/$rev_id/". $docs['doc_rka']) ) {
        $link_F03 = '<a href="'. site_url("files/revisi_SatuDJA/$rev_id/". $docs['doc_rka']) .'">';  
        $link_FE3 = '</a>'; $doc_F03 = 'checked'; 
    }
    $link_F04 = '';  $link_FE4 = ''; $doc_F04 = '';  
    if ( $docs['doc_adk']!='' And file_exists("files/revisi_SatuDJA/$rev_id/". $docs['doc_adk']) ) {
        $link_F04 = '<a href="'. site_url("files/revisi_SatuDJA/$rev_id/". $docs['doc_adk']) .'">';  
        $link_FE4 = '</a>'; $doc_F04 = 'checked'; 
    }
    $link_F05 = '';  $link_FE5 = ''; $doc_F05 = '';  
    if ( $docs['doc_dukung_sepakat']!='' And file_exists("files/revisi_SatuDJA/$rev_id/". $docs['doc_dukung_sepakat']) ) {
        $link_F05 = '<a href="'. site_url("files/revisi_SatuDJA/$rev_id/". $docs['doc_dukung_sepakat']) .'">';  
        $link_FE5 = '</a>'; $doc_F05 = 'checked'; 
    }
    $link_F06 = '';  $link_FE6 = ''; $doc_F06 = '';  
    if ( $docs['doc_dukung_hal4']!='' And file_exists("files/revisi_SatuDJA/$rev_id/". $docs['doc_dukung_es1']) ) {
        $link_F06 = '<a href="'. site_url("files/revisi_SatuDJA/$rev_id/". $docs['doc_dukung_hal4']) .'">';  
        $link_FE6 = '</a>'; $doc_F06 = 'checked'; 
    }
    $link_F07 = '';  $link_FE7 = ''; $doc_F07 = '';  
    if ( $docs['doc_dukung_lainnya']!='' And file_exists("files/revisi_SatuDJA/$rev_id/". $docs['doc_dukung_lainnya']) ) {
        $link_F07 = '<a href="'. site_url("files/revisi_SatuDJA/$rev_id/". $docs['doc_dukung_lainnya']) .'">';  
        $link_FE7 = '</a>'; $doc_F07 = 'checked'; 
    }
?>


<div class="col-sm-12 text-center" style="padding:0px;background:#3C8DBC;margin:5px 0px 15px 0px">
  <label class="checkbox-custom-label" style="color:#FFF;padding:5px 15px">KELENGKAPAN DOKUMEN USULAN REVISI</label>
</div>

<div style="margin-top:20px">
  <div style="padding-top:20px">

    <div class="form-group jarak">
        <label class="col-sm-2 rev text-right"></label>
        <div class="col-sm-8" style="padding:0px">
            <input id="chk-F01" class="checkbox-custom" name="chk-F01" type="checkbox" <?= $doc_F01 ?> disabled>
            <label for="chk-F01" class="checkbox-custom-label fdok infotext">
                <?= $link_F01 ?> Surat usulan revisi yang ditandatangani pejabat eselon I <?= $link_FE1 ?>
            </label>
        </div>
    </div>
    <div class="form-group jarak">
        <label class="col-sm-2 rev text-right"></label>
        <div class="col-sm-8" style="padding:0px">
            <input id="chk-F04" class="checkbox-custom" name="chk-F04" type="checkbox" <?= $doc_F04 ?> disabled>
            <label for="chk-F04" class="checkbox-custom-label fdok infotext">
                <?= $link_F04 ?> Arsip Data Komputer (ADK) RKA-K/L DIPA revisi Satker <?= $link_FE4 ?>
            </label>
        </div>
    </div>
     <div class="form-group jarak">
        <label class="col-sm-2 rev text-right"></label>
        <div class="col-sm-8" style="padding:0px">
            <input id="chk-F04" class="checkbox-custom" name="chk-F04" type="checkbox" checked="">
            <label for="chk-F04" class="checkbox-custom-label fdok infotext">
                <a href="<?=site_url('Monit_satker/adk_per_tiket/') .'/'. $rev_id; ?>"> Informasi ADK </a>
            </label>
        </div>
    </div>

    <div class="form-group jarak">
        <label class="col-sm-2 rev text-right"></label>
        <div class="col-sm-8" style="padding:0px">
            <input id="chk-F06" class="checkbox-custom" name="chk-F06" type="checkbox" <?= $doc_F06 ?> disabled>
            <label for="chk-F06" class="checkbox-custom-label fdok infotext">
                <?= $link_F06 ?> Surat Persetujuan Eselon I <?= $link_FE6 ?>
            </label>
        </div>
    </div>
    <div class="form-group jarak">
        <label class="col-sm-2 rev text-right"></label>
        <div class="col-sm-8" style="padding:0px">
            <input id="chk-F07" class="checkbox-custom" name="chk-F07" type="checkbox" <?= $doc_F07 ?> disabled>
            <label for="chk-F07" class="checkbox-custom-label fdok infotext">
                <?= $link_F07 ?> Dokumen pendukung terkait lainnya <?= $link_FE7 ?>
            </label>
        </div>
    </div>

  </div>
</div>

