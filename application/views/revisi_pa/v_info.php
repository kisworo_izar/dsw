<?php
  if ( ! isset($rev_id) ) $rev_id=17081945;
  $dbrevisi = $this->load->database('revisi'. $this->session->userdata('thang'), TRUE);
  $query = $dbrevisi->query("Select rev_tahun, kl_dept, kl_unit, kl_pjb_jab, kl_surat_no, kl_surat_tgl, kl_surat_hal, kl_catatan, kl_email From revisi Where rev_id='$rev_id'");
  $info  = $query->row_array();

  $dbref = $this->load->database('ref', TRUE);
  $query = $dbref->query("Select nmdept From t_dept Where kddept='". $info['kl_dept'] ."'"); 
  $dept  = $query->row_array();

  $query = $dbref->query("Select nmunit From t_unit Where kddept='". $info['kl_dept'] ."' And kdunit='". $info['kl_unit'] ."'"); 
  $unit  = $query->row_array();
?>

<div class="col-sm-12 text-center" style="padding:0px;background:#3C8DBC;margin:5px 0px 15px 0px">
  <label class="checkbox-custom-label" style="color:#FFF;padding:5px 15px">INFORMASI USULAN REVISI ANGGARAN</label>
</div>

<div style="margin-top:20px">
    <div class="form-group infojarak">
      <label class="col-sm-2 inforev text-right">ID Revisi : </label>
      <label class="col-sm-8 inforev">
        <span class="text-primary" id="tam_rev_id"><?= $rev_id .' / '. $info['rev_tahun'] ?></span>
        <span class="pull-right small infotext" id="tam_kl_email">Email : <?= $info['kl_email'] ?></span>
      </label>
    </div>
    <div class="form-group infojarak">
      <label class="col-sm-2 inforev text-right">K/L : </label>
      <label class="col-sm-8 inforev infotext" id="tam_kl_dept"><?= $info['kl_dept'] .' ' ?></label>
    </div>
    <div class="form-group infojarak">
      <label class="col-sm-2 inforev text-right">Unit Eselon I : </label>
      <label class="col-sm-8 inforev infotext" id="tam_kl_unit"><?= $info['kl_dept'] .'.'. $info['kl_unit'] .' ' ?></label>
    </div>
    <div class="form-group infojarak">
      <label class="col-sm-2 inforev text-right">Pejabat : </label>
      <label class="col-sm-8 inforev infotext" id="tam_kl_pjb_jab"><?= $info['kl_pjb_jab'] ?></label>
    </div>
    <div class="form-group infojarak">
      <label class="col-sm-2 inforev text-right">No. Surat : </label>
      <label class="col-sm-3 inforev infotext" id="tam_kl_surat_no"><?= $info['kl_surat_no'] ?></label>
    </div>
    <div class="form-group infojarak">
      <label class="col-sm-2 inforev text-right">Tanggal Surat : </label>
      <label class="col-sm-3 inforev infotext" id="tam_kl_surat_tgl"><?= $info['kl_surat_tgl'] ?></label>
    </div>
    <div class="form-group infojarak">
      <label class="col-sm-2 inforev text-right">Hal : </label>
      <label class="col-sm-3 inforev infotext" id="tam_kl_surat_hal"><?= $info['kl_surat_hal'] ?></label>
    </div>

    <div class="form-group infojarak">
      <label class="col-sm-2 inforev text-right">Keterangan : </label>
      <label class="col-sm-3 inforev infotext" id="tam_pus_catatan"><?= $info['kl_catatan'] ?></label>
    </div>
</div>
