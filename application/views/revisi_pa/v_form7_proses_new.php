<link href="<?=base_url('assets/bootstrap/css/bootstrap-datetimepicker.min.css');?>" rel="stylesheet" type="text/css" />
<script src="<?=base_url('assets/plugins/datepicker/moment.min.js');?>" type="text/javascript"></script>
<script src="<?=base_url('assets/plugins/datepicker/id.js');?>" type="text/javascript"></script>
<script src="<?=base_url('assets/bootstrap/js/bootstrap-datetimepicker.min.js');?>" type="text/javascript"></script>
<link href="<?=base_url(); ?>assets/plugins/uploadfile/fileinput.css" rel="stylesheet">
<script src="<?=base_url();?>assets/plugins/uploadfile/fileinput.min.js" type="text/javascript"></script>

<style media="screen">
.checkbox-custom, .radio-custom{opacity: 0;position: absolute;}
.checkbox-custom, .checkbox-custom-label, .radio-custom, .radio-custom-label{
    display: inline-block;vertical-align: middle;margin: 0px;}
.checkbox-custom-label, .radio-custom-label {position: relative;}
.checkbox-custom + .checkbox-custom-label:before, .radio-custom + .radio-custom-label:before{
    content: '';background: #fff;border: 1px solid #DFDFDF;display: inline-block;vertical-align: middle;
    width: 20px;height: 20px;padding: 2px;margin-right: 5px;text-align: center;}
.checkbox-custom:checked + .checkbox-custom-label:before{background: #3C8DBC;}
.radio-custom + .radio-custom-label:before{border-radius: 50%;}
.radio-custom:checked + .radio-custom-label:before {background: #3C8DBC;}
.checkbox-custom:focus + .checkbox-custom-label, .radio-custom:focus + .radio-custom-label {outline: 0px solid #ddd;}
.inforev{padding-top:0px;padding-left:0px;}
.infotext{font-weight: normal}
.infojarak{margin-bottom:0px;}
.jarak{margin-bottom:8px;}
.fdok{padding-left:30px;text-indent:-30px}
</style>

<!-- <script type="text/javascript">
    $(function () {
        $('.datetimepicker1,.datetimepicker2').datetimepicker({
            format : 'dddd, DD MMMM YYYY',
            locale : 'id'
        });
    });
</script> -->

<script type="text/javascript">
    $(function () {
        $('.datetimepicker1').datetimepicker({
            format : 'YYYY-MM-DD',
            locale : 'id'
        });
    });
    $(function () {
        $('.datetimepicker2').datetimepicker({
            format : 'YYYY-MM-DD',
            locale : 'id'
        });
    });
</script>

<script>
function toggle_opt($opt) {
 if( $opt == 'sah' ){
     document.getElementById("proses").innerHTML = 'Proses Pengesahan Revisi';
     document.getElementById("revisike").style.display = '';
     document.getElementById("pengesahan").style.display = '';
     document.getElementById("penolakan").style.display = 'none';
     $('#proses').removeClass('btn-danger');
     $('#proses').addClass('btn-primary');
     $('#proses').attr('disabled',false);
 }else{
     document.getElementById("proses").innerHTML = 'Proses Surat Penolakan';
     document.getElementById("penolakan").style.display = '';
     document.getElementById("revisike").style.display = 'none';
     document.getElementById("pengesahan").style.display = 'none';
     $('#proses').removeClass('btn-primary');
     $('#proses').addClass('btn-danger');
     $('#proses').attr('disabled',false);
    }
}
</script>

<?php
    $disb = ''; $terima = ''; $tolak = ''; $sesi=$this->session->userdata('idusergroup');$tidak = ''; $ya = '';
    if ( ($proses['t7_status']=='2') And ($sesi <> '001') ) { $disb = 'disabled'; $terima = 'checked'; $tidak = 'checked'; $revisike = $proses['rev_ke']; $no = $proses['t7_sp_no']; $tgl= $proses['t7_sp_tgl']; $file= $proses['t7_sp_file']; $catatan = $proses['t7_catatan'];  }
    if ( ($proses['t7_status']=='0') And ($sesi <> '001') ) { $disb = 'disabled'; $tolak = 'checked'; $ya = 'checked'; $revisike = $proses['rev_ke']; $no = $proses['t7_sp_no']; $tgl= $proses['t7_sp_tgl']; $file= $proses['t7_sp_file']; $catatan = $proses['t7_catatan']; }
    if ( ($proses['t7_status']=='1') And ($sesi <> '001') ) { $disb = ''; $tolak = ''; $ya = ''; $revisike = ' '; $no = ''; $tgl= ''; $file= ''; $catatan = ''; }
    if ( ($proses['t7_status']=='2') And ($sesi == '001') ) {  $terima = 'checked'; $revisike = $proses['rev_ke']; $no = $proses['t7_sp_no']; $tgl= $proses['t7_sp_tgl']; $file= $proses['t7_sp_file']; $catatan = $proses['t7_catatan']; }
    if ( ($proses['t7_status']=='0') And ($sesi == '001') ) {  $tolak = 'checked'; $revisike = $proses['rev_ke']; $no = $proses['t7_sp_no']; $tgl= $proses['t7_sp_tgl']; $file= $proses['t7_sp_file']; $catatan = $proses['t7_catatan']; }
    if ( ($proses['t7_status']=='1') And ($sesi == '001') ) { $disb = ''; $tolak = ''; $ya = ''; $revisike = ' '; $no = ''; $tgl= ''; $file= ''; $catatan = ''; }
    if ( ($proses['t6_status']=='0') And ($sesi <> '001') ) { $aktif = 'disabled'; }
    if ( ($proses['t6_status']!='0') And ($sesi <> '001') ) { $aktif = ''; }
?>


<div class="row" ng-app="app" >
    <div class="col-md-12">
        <div class="box box-widget">
            <div class="box-header with-border">
              <h3 style="margin:0px">F7. Penetapan Revisi</h3>
        </div>
                                  
            
            <div class="box-body">
                

                    <form name="myForm" class="form-horizontal" action="<?=site_url('Revisi_PA/crud_form7_proses')?>" onsubmit="" method="post" role="form">
                      <?php $this->load->view('revisi_pa/v_info'); ?>     

                    <div class="col-sm-12 text-center" style="padding:0px;background:#3C8DBC;margin:5px 0px 15px 0px">
                        <label class="checkbox-custom-label" style="color:#FFF;padding:5px 15px">
                            PENETAPAN DAN PENGESAHAN REVISI ANGGARAN
                        </label>
                    </div>

                    <div style="margin-top:20px">
                        <div style="padding-top:20px">
                            <div class="form-group jarak">
                                <label class="col-sm-2 rev text-right">Penetapan : </label>
                                <div class="col-sm-8" style="padding:0px">
                                    <label class="col-sm-12" style="padding-left:0px">
                                        <input id="radio-1" class="radio-custom" name="radio-group" type="radio" value='1' <?php echo "$terima $disb $aktif" ?> onclick="toggle_opt('sah')" >
                                        <label for="radio-1" class="radio-custom-label">Pengesahan Revisi</label>
                                    </label>
                                    <label class="col-sm-12" style="padding-left:0px">
                                        <input id="radio-2" class="radio-custom" name="radio-group" type="radio" value='2' <?php echo "$tolak $disb" ?> onclick="toggle_opt('tolak')">
                                        <label for="radio-2" class="radio-custom-label">Surat Penolakan</label>
                                    </label>
                                </div>
                            </div>

                            <div class="form-group jarak" id="revisike" style="display:">
                                <label class="col-sm-2 rev text-right">Revisi Ke :</label>
                                <div class="col-sm-8" style="padding:0px">
                                    <input name="rev_ke" id="file5" class="form-control" placeholder="Revisi Ke" <?php echo "$disb" ?> value="<?php echo $revisike; ?>" >
                                </div>
                            </div>

                            <div class="form-group jarak">
                                <label class="col-sm-2 rev text-right">No. Surat : </label>
                                <div class="col-sm-3" style="padding:0px">
                                    <input name="t7_sp_no" class="form-control" placeholder="Nomor Surat Penetapan" <?php echo "$disb" ?> id="" value="<?php echo $no; ?>" >
                                </div>
                                <label class="col-sm-2 rev text-right"  style="padding-right:0px">Tanggal Surat : </label>
                                <div class="col-sm-3" style="padding-right:0px">
                                    <div class="input-group date datetimepicker1">
                                        <input name="t7_sp_tgl" id="t7_sp_tgl" type="text" class="form-control" <?php echo "$disb" ?> placeholder="Tanggal Surat Penetapan" value="<?php echo $tgl; ?>">
                                        <span class="input-group-addon">
                                            <i class="fa fa-calendar text-primary"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group jarak" id="pengesahan" style="display:none">
                                  
                            </div>

                             <div class="form-group jarak" id="penolakan" style="display:">
                                  
                            </div>

                             <div class="form-group jarak">
                                        <label class="col-sm-2 rev text-right">File Surat :</label>
                                        <div class="col-sm-8" style="padding:0px">
                                            <?php if ($proses['t7_status']=='1') { ?>
                                            <input type="hidden" name="t7_sp_file" id="t7_sp_file" >
                                            <input type="file" name="form7" id="form7" <?php echo "$disb" ?> class="file" data-show-preview="false" >
                                            <?php } else { ?>
                                            <input name="t7_catatan" value="<?php echo $file ?>" <?php echo "$disb" ?> class="form-control" >   
                                            <?php } ?>                                    
                                        </div>
                            </div>

                            <div class="form-group jarak">
                                <label class="col-sm-2 rev text-right">Catatan : </label>
                                <div class="col-sm-8" style="padding:0px">
                                    <textarea class="form-control" placeholder="Catatan Penetapan" <?php echo "$disb" ?> name="t7_catatan" rows="5"><?php echo $catatan; ?></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <div class="col-sm-12 pull-right">
                        <div class="pull-right">
                        <input type="hidden" name="rev_id"  id="rev_id" value="<?php echo $rev_id ?>" >
                              <button  type="submit" id="proses" disabled="disabled"  > </button>
                        </div>
                    </div>
                </div>
            </form>
          
        </div>
    </div>
</div>

<!-- <script type="text/javascript">
    $("#form7").fileinput({
        uploadUrl: "<?php echo site_url("revisi_23/fileupload_form3/form7/$rev_id") ?>",
        allowedFileExtensions : ['17','jpg','xlsx','doc','docx','rtf','pdf'],
        overwriteInitial: true,
        maxFileSize: 1000,
        slugCallback: function(filename) {
            return filename.replace('(', '_').replace(']', '_');
        }
    });
    $("#form7").on('filebatchuploadcomplete', function(event, files, extra) {
        var id = event.target.id;
        var name = '';
        if (id=='form7') name = 't7_sp_file';
        $("#"+name).val( $("#"+id).val() );
        alert($("#"+id).val());
    });
</script> -->

<!-- New -->
<script type="text/javascript">
    $('#form7').fileinput({
        uploadExtraData: function(previewID,index){
            var data = {rev_id : $("#rev_id").val(), name: 'form7'};
            return data;
        },
        uploadUrl: "<?php echo site_url('Revisi_PA/fileupload_form7')  ?>",
        allowedFileExtensions : ['pdf'],
        msgInvalidFileExtension: "Invalid extension file !",
        overwriteInitial: true,
        maxFileCount: 1,
        maxFileSize: 5000
    });
    $('#form7').on('filebatchuploadsuccess', function(event, data, extra) {
      var extra = data.extra, response = data.response;
      $('#t7_sp_file').val( response.uploaded );
      $('#t7_sp_file').attr('type','text'); $('#form7').css('display', 'none');
    });
    $('#form7').on('filebatchuploaderror', function(event, data) {
      var response = data.response;
      alert('Gagal upload !');
    });

  function inputund(){
    window.location.href = "<?php echo site_url('Revisi_PA/inputund') ?>";
  }

  function inputund_tolak(){
    window.location.href = "<?php echo site_url('Revisi_PA/inputund_tolak') ?>";
  }

</script>