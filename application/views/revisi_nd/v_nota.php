<div class="form-group jarak">
    <label class="col-sm-2 rev text-right">Tujuan ND : </label>
    <div class="col-sm-8" style="padding:0px">
        <input type="hidden" name="rev_id" value="<?= $nd['revisi']['rev_id'] ?>">
        <input class="form-control" readonly value="<?= $nd['revisi']['direktur'] ?>">
    </div>
    <div class="col-sm-2 pull-right" style="padding-top:5px">
        <input type="hidden" name="nd_dit_pgs"> 
        <input name="radio_dit_pgs" id="nd_dit_plh" type="radio" <?php if ($nd['nota']['nd_dit_pgs'] == 'Plh.') echo 'checked'; ?> class="checkbox-custom">
        <label for="nd_dit_plh" class="checkbox-custom-label">Plh.</label> &nbsp;&nbsp;&nbsp;
        <input name="radio_dit_pgs" id="nd_dit_plt" type="radio" <?php if ($nd['nota']['nd_dit_pgs'] == 'Plt.') echo 'checked'; ?> class="checkbox-custom">
        <label for="nd_dit_plt" class="checkbox-custom-label">Plt.</label>
    </div>
</div>

<div class="form-group jarak">
    <label class="col-sm-2 rev text-right">Dari :</label>
    <div class="col-sm-8" style="padding:0px">
        <input class="form-control" readonly value="<?= $nd['revisi']['subdit'] ?>">
    </div>
    <div class="col-sm-2 pull-right" style="padding-top:5px">
        <input type="hidden" name="nd_subdit_pgs"> 
        <input name="radio_subdit_pgs" id="nd_subdit_plh" type="radio" <?php if ($nd['nota']['nd_subdit_pgs'] == 'Plh.') echo 'checked'; ?> class="checkbox-custom">
        <label for="nd_subdit_plh" class="checkbox-custom-label">Plh.</label> &nbsp;&nbsp;&nbsp;
        <input name="radio_subdit_pgs" id="nd_subdit_plt" type="radio" <?php if ($nd['nota']['nd_subdit_pgs'] == 'Plt.') echo 'checked'; ?> class="checkbox-custom">
        <label for="nd_subdit_plt" class="checkbox-custom-label">Plt.</label>
    </div>
</div>

<div class="form-group jarak">
    <label class="col-sm-2 rev text-right">Lampiran : </label>
    <div class="col-sm-1" style="padding:0px">
        <input name="nd_lamp_jml" class="form-control" placeholder="Jumlah" value="<?= $nd['nota']['nd_lamp_jml'] ?>">
    </div>
    <label class="col-sm-9 rev"> Dokumen</label>
</div>

<div class="form-group jarak">
    <label class="col-sm-2 rev text-right">Hal : </label>
    <div class="col-sm-8" style="padding:0px">
        <input name="nd_hal" type="text" class="form-control" placeholder="Hal Nota Dinas" value="<?= $nd['nota']['nd_hal'] ?>">
    </div>
    <div class="col-sm-2">&nbsp;</div>
</div>

<div class="form-group jarak">
    <label class="col-sm-2 rev text-right">Tanggal ND : </label>
    <div class="col-sm-3" style="padding:0px">
        <div class="input-group date datetimepicker3">
            <input name="nd_tgl" type="text" class="form-control" placeholder="Tanggal ND" style="height:40px" value="<?= $nd['nota']['nd_tgl'] ?>">
            <span class="input-group-addon"><i class="fa fa-calendar text-primary"></i></span>
        </div>
    </div>
    <div class="col-sm-7">&nbsp;</div>
</div>

<div class="form-group jarak">
    <label class="col-sm-2 rev text-right">Penandatangan : </label>
    <div class="col-sm-3" style="padding:0px">
        <select class="h-penandatangan form-control" name="nd_subdit">
        <?php foreach ($nd['nmsubdit'] as $row) { 
            $selc = ''; if ($row['nmuser'] == $nd['nota']['nd_subdit_nama']) $selc = 'selected="selected"';?>
            <option value="<?= $row['nip'] .' '. $row['nmuser'] ?>" <?= $selc ?>><?= $row['nmuser'] ?></option>
        <?php } ?>
        </select>
    </div>
    <div class="col-sm-7">&nbsp;</div>
</div>

<div class="form-group jarak">
    <div class="col-sm-2">&nbsp;</div>
    <div class="col-sm-5 text-bold" style="padding:10px 0px 0px 0px;color:#3C8DBC">
        Menindaklanjuti surat dari :
    </div>
</div>

<script type="text/javascript">
    $('input[name="radio_dit_pgs"]').change(function(){
        var id = $(this).attr('id');
        var label = $('#'+id).next("label").text();
        $('input[name="nd_dit_pgs"]').val(label);
    });

    $('input[name="radio_subdit_pgs"]').change(function(){
        var id = $(this).attr('id');
        var label = $('#'+id).next("label").text();
        $('input[name="nd_subdit_pgs"]').val(label);
    });
</script>


