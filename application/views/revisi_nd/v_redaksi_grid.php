<div class="box box-widget">
  <input type="hidden" id="aktif">
  
  <div class="box-body">
    <table id="iGrid" class="table table-hover table-bordered" style="border-spacing: 1; width: 100%;">
      <thead>
        <tr style="color: #fff; background-color: #3c8dbc;">
          <th class="text-center" width=" 5%">Tahun</th>
          <th class="text-center" width=" 5%">Tahap</th>
          <th class="text-center" width="10%">Email Subject</th>
          <th class="text-center" width="40%">Email Narasi</th>
          <th class="text-center" width="40%">SMS Narasi</th>
        </tr>
        <tr id="tr_cari" style="margin: 0px" bgcolor="#fff">
          <td colspan="5">
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-btn">
                  <button id="btn_data" onclick="newadd()" class="btn btn-default"><i class="fa fa-plus"></i></button>
                </span>
                <input id="cari" class="form-control" placeholder="ketik kata cari" value="">
              </div>      
            </div>
          </td>
        </tr>
      </thead>

      <tbody>
          <tr id="tr_crud" style="display: none" bgcolor="#eee">
            <td colspan="5"><div id="crud_form"><?php $this->load->view('revisi_nd/v_redaksi_crud'); ?></div></td>
          </tr>

        <?php foreach ($tabel as $row) {
          $class = $row['tahun'] .'#'. $row['tahap'] .'#'. $row['email_subject'] .'#email_narasi#sms_narasi#' ; ?>

          
          <tr id="id_<?= $row['tahap'] ?>" class="<?= "$class# All" ?>" style="margin:0px;" onclick="move_row( this.id )">
            <td class="text-left"><?= $row['tahun'] ?></td>
            <td class="text-left"><?= $row['tahap'] ?></td>
            <td class="text-left"><?= $row['email_subject'] ?></td>
            <td class="text-left"><div id="email_<?= $row['tahap'] ?>"><html><?= $row['email_narasi'] ?></html></div></td>
            <td class="text-left"><div id="sms_<?= $row['tahap'] ?>"><html><?= $row['sms_narasi'] ?></html></div></td>
          </tr>

        <?php } ?>
      </tbody>
    </table>

  </div>
</div>

<script type="text/javascript">
  // PENCARIAN
  $("#cari").keyup( function(event) {
    var kata = $('#cari').val();

    $('#tr_crud').insertAfter( $('#tr_cari') );
    $('#tr_crud').css('display', 'none');

    $('.All').show();
    $('#iGrid > tbody  > tr').each(function() {
      var id  = $(this).attr('id');
      var str = $('#'+id).text().toLowerCase();
      if (str.indexOf( kata.toLowerCase() ) < 0) $('#'+id).hide();
    });
  });


  // Click ROW untuk Buka Form CRUD
  function move_row( idkey ) {
    var id  = idkey.replace('id_', 'email_'),  email_narasi = $('#'+id).html();
    var id  = idkey.replace('id_', 'sms_'),  sms_narasi = $('#'+id).html();
    if ( $('#aktif').val()==idkey ) {
      $('#aktif').val('');
      $('#tr_crud').css('display', 'none');
    } else { 
      var arr = $('#'+idkey).attr('class').replace(' text-bold','').split("#");
      $('#tr_crud').css('display', '');
      $('#tr_crud').insertAfter( $('#'+idkey) );

      $("#0, #1").prop('readonly', true);
      $("#3").val( email_narasi ).change();
      $("#4").val( sms_narasi ).change();
      $("input[ name='isian[]' ]").each(function () { $(this).val(arr[ $(this).attr('id') ]); })
      $('#Rekam').hide(); $('#Ubah').show(); $('#Hapus').show(); $('#aktif').val(idkey);
    }
  }


  // Click Tombol PLUS untuk Buka Form CRUD
  function newadd() {
    if ( $('#aktif').val()=='tr_crud' ) {
      $('#aktif').val('');
      $('#tr_crud').css('display', 'none');
    } else {
      $("#0, #1").prop('readonly', false);
      $('#tr_crud').css('display', ''); 
      $('#tr_crud').insertAfter( $('#tr_cari') );
      $("input[ name='isian[]' ]").each(function () { $(this).val(''); })
      $('#Rekam').show(); $('#Ubah').hide(); $('#Hapus').hide(); $('#aktif').val('tr_crud');
    }
  }

</script>


