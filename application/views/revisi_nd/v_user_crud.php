<style media="screen">
  .form-group { margin: 0px;}
</style>


<div class="box box-widget" style="margin: 0px 0px 0px 0px;">
  <div class="box-body">

    <div class="col-sm-12 pull-left">
      <div class="col-sm-12 form-group">
        <label class="col-sm-2 text-right">Tahun</label>
        <div class="col-sm-10">
          <input id="0" name="isian[]" type="text" class="form-control" readonly value="">
        </div>
      </div>
      <div class="col-sm-12 form-group">
        <label class="col-sm-2 text-right">Kode SO</label>
        <div class="col-sm-10">
          <input id="1" name="isian[]" type="text" class="form-control" readonly value="">
        </div>
      </div>
      <div class="col-sm-12 form-group">
        <label class="col-sm-2 text-right">Mitra K/L</label>
        <div class="col-sm-10">
          <input id="2" name="isian[]" type="text" class="form-control" value="">
        </div>
      </div>
      <div class="col-sm-12 form-group">
        <label class="col-sm-2 text-right">CP #1</label>
        <div class="col-sm-10">
          <input id="3" name="isian[]" type="text" class="form-control" value="">
        </div>
      </div>
      <div class="col-sm-12 form-group">
        <label class="col-sm-2 text-right">HP #1</label>
        <div class="col-sm-10">
          <input id="4" name="isian[]" type="text" class="form-control" value="">
        </div>
      </div>
      <div class="col-sm-12 form-group">
        <label class="col-sm-2 text-right">CP #2</label>
        <div class="col-sm-10">
          <input id="5" name="isian[]" type="text" class="form-control" value="">
        </div>
      </div>
      <div class="col-sm-12 form-group">
        <label class="col-sm-2 text-right">HP #2</label>
        <div class="col-sm-10">
          <input id="6" name="isian[]" type="text" class="form-control" value="">
        </div>
      </div>
      <div class="col-sm-12 form-group">
        <label class="col-sm-2 text-right">Intern</label>
        <div class="col-sm-10">
          <input id="7" name="isian[]" type="text" class="form-control" value="">
        </div>
      </div>
      <div class="col-sm-12 form-group">
        <label class="col-sm-2 text-right">Email</label>
        <div class="col-sm-10">
          <input id="8" name="isian[]" type="text" class="form-control" value="">
        </div>
      </div>
      <div class="col-sm-12 form-group">
        <label class="col-sm-2 text-right">Phone</label>
        <div class="col-sm-10">
          <input id="9" name="isian[]" type="text" class="form-control" value="">
        </div>
      </div>

      <div class= "col-sm-12 form-group text-center" style="padding-top: 10px">
        <button id="Rekam" class="btn btn-warning" onclick="crud( this.value )" value="Rekam">Rekam</button>
        <button id="Ubah"  class="btn btn-warning" onclick="crud( this.value )" value="Ubah">Ubah</button>
        <button id="Hapus" class="btn btn-warning" onclick="crud( this.value )" value="Hapus" >Hapus</button>
      </div>
    </div>

  </div>
</div>


<script type="text/javascript">
  function crud( aksi ) {
    var isian = new Array(),  kdkey = $('#aktif').val();
    $("#crud_form input, textarea").each(function() { isian.push( $(this).val() ); });

    $.ajax({
      url : "<?php echo site_url('revisi_nd/crud_user') ?>",
      type: "POST",
      data: { 'aksi': aksi, 'isian': isian, 'kdkey': kdkey },
      success: function(pesan) {
        window.location.href = "<?php echo site_url('revisi_nd/admin_user') ?>";
      }  
    })
  }
</script>
