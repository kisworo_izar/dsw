<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Revisi_nd_dja extends CI_Controller {

	function __construct() {
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
		$this->dbrevisi = $this->load->database('revisi'. $this->session->userdata('thang'), TRUE);
        $this->load->model('dsw_model');
        $this->load->model('revisi_nd_dja_model');
	}

	function index( $rev_id=null ) {
		$this->fc->log('Revisi ND');
        if ($rev_id == null) show_404();
        if (! $this->session->userdata('isLoggedIn') ) redirect("login/show_login");
		$this->rekam( $rev_id );
	}

	function rekam( $rev_id=null ) {
        if ($rev_id == null) show_404();
		$data['nd']	  = $this->revisi_nd_dja_model->get_data( $rev_id );
		$data['bread']= array('view'=>'revisi_nd_dja/v_rekam', 'header'=>'&nbsp;', 'subheader'=>'Revisi Anggaran');
		$this->load->view('main/utama', $data);
	}

	function crud_form1() {
		$msg = $this->revisi_nd_dja_model->save();
		echo $msg;
	}

	// MAINTENANCE TABEL REFERENSI
	function admin_jenis() {
        if ( !$this->fc->check_akses('revisi_nd_dja/admin_jenis') ) show_404();
		$data = $this->revisi_nd_dja_model->get_jenis();
		$data['bread'] = array('view'=>'revisi_nd_dja/v_jenis_grid', 'header'=>'Referensi Jenis Revisi', 'subheader'=>'Admin Panel');
		$this->load->view('main/utama', $data);
	}

	function crud_jenis() {
		echo $this->revisi_nd_dja_model->save_jenis();
	}

	function admin_user() {
        if ( !$this->fc->check_akses('revisi_nd_dja/admin_user') ) show_404();
		$data = $this->revisi_nd_dja_model->get_user();
		$data['bread'] = array('view'=>'revisi_nd_dja/v_user_grid', 'header'=>'Referensi User Revisi', 'subheader'=>'Admin Panel');
		$this->load->view('main/utama', $data);
	}

	function crud_user() {
		echo $this->revisi_nd_dja_model->save_user();
	}

	function admin_mitra() {
        if ( !$this->fc->check_akses('revisi_nd_dja/admin_mitra') ) show_404();
		$data = $this->revisi_nd_dja_model->get_mitra();
		$data['bread'] = array('view'=>'revisi_nd_dja/v_mitra_grid', 'header'=>'Referensi Mitra K/L', 'subheader'=>'Admin Panel');
		$this->load->view('main/utama', $data);
	}

	function crud_mitra() {
		echo $this->revisi_nd_dja_model->save_mitra();
	}

	function admin_redaksi() {
        if ( !$this->fc->check_akses('revisi_nd_dja/admin_redaksi') ) show_404();
		$data = $this->revisi_nd_dja_model->get_redaksi();
		$data['bread'] = array('view'=>'revisi_nd_dja/v_redaksi_grid', 'header'=>'Referensi Redaksi Email / SMS', 'subheader'=>'Admin Panel');
		$this->load->view('main/utama', $data);
	}

	function crud_redaksi() {
		echo $this->revisi_nd_dja_model->save_redaksi();
	}

	function editor_nd($rev_id) {
		$data['status_rev']= $this->revisi_nd_dja_model->get_status($rev_id);
		$data['teks']  = $this->revisi_nd_dja_model->nd_to_text_simpan($rev_id);
		if($data['teks']==null) $data['teks']  = $this->revisi_nd_dja_model->nd_to_text($rev_id);
		$data['bread'] = array('view'=>'revisi_nd_dja/v_tinymce_nd', 'header'=>'Nota Dinas Revisi', 'subheader'=>'Revisi Anggaran');
		$this->load->view('main/utama', $data);
	}

	function editor_save() {
		echo $this->revisi_nd_dja_model->save_nd();
	}

	function editor_penolakan($rev_id) {
		$data['teks']  = $this->revisi_nd_dja_model->spra_to_text_simpan($rev_id);
		if($data['teks']==null) $data['teks'] = $this->revisi_nd_dja_model->tolak_to_text($rev_id);
		$data['bread'] = array('view'=>'revisi_nd_dja/v_tinymce_tolak', 'header'=>'Surat Penolakan', 'subheader'=>'Revisi Anggaran');
		$this->load->view('main/utama', $data);
	}

	function editor_penolakan_save(){
		echo $this->revisi_nd_dja_model->save_penolakan();
	}

	function editor_spra($rev_id) {
		$data['teks']  = $this->revisi_nd_dja_model->spra_to_text_simpan($rev_id);
		if($data['teks']==null) $data['teks']  = $this->revisi_nd_dja_model->spra_to_text($rev_id);
		$data['bread'] = array('view'=>'revisi_nd_dja/v_tinymce_spra', 'header'=>'SPRA', 'subheader'=>'Revisi Anggaran');
		$this->load->view('main/utama', $data);
	}

	function editor_spra_save(){
		echo $this->revisi_nd_dja_model->save_spra();
	}

	function generate_data($rev_id){
		$this->revisi_nd_dja_model->generate_data($rev_id);
		$data['teks']  = $this->revisi_nd_dja_model->nd_to_text($rev_id);
		redirect("revisi_nd_dja/editor_nd/". $rev_id);
	}


}
