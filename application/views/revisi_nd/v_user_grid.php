<div class="box box-widget">
  <input type="hidden" id="aktif">
  
  <div class="box-body">
    <table id="iGrid" class="table table-hover table-bordered" style="border-spacing: 1; width: 100%;">
      <thead>
        <tr style="color: #fff; background-color: #3c8dbc;">
          <th class="text-center" width="25%">Sub Direktorat</th>
          <th class="text-center" width=""   >Kementerian / Lembaga</th>
          <th class="text-center" width="15%">Contact Persona</th>
        </tr>
        <tr id="tr_cari" style="margin: 0px" bgcolor="#fff">
          <td colspan="3">
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-btn">
                  <button id="btn_data" onclick="newadd()" class="btn btn-default"><i class="fa fa-plus"></i></button>
                </span>
                <input id="cari" class="form-control" placeholder="ketik kata cari" value="">
              </div>      
            </div>
          </td>
        </tr>
      </thead>

      <tbody>
          <tr id="tr_crud" style="display: none" bgcolor="#eee">
            <td colspan="3"><div id="crud_form"><?php $this->load->view('revisi_nd/v_user_crud'); ?></div></td>
          </tr>

        <?php foreach ($tabel as $row) { ?>
          
          <tr id="id_<?= $row['kdso'] ?>" class="<?= implode('#', $row) .'# All ' ?>" style="margin:0px;" onclick="move_row( this.id )">
            <td class="text-left">
              <?php
                $kdso = $row['kdso'];  
                if( array_key_exists($kdso, $so) )   $nama = $so[$kdso]['nmso']; else $nama = '...';
                if( array_key_exists($kdso, $user) ) $opr  = $user[$kdso]['iduser']; else $opr = '...';
                echo "<b> $kdso </b><br> $nama <br><br> User :<b> $opr </b><br> Email :<b> ".$row['email']." </b><br> Phone :<b> ".$row['phone']." </b>";
              ?>
            </td>
            <td class="text-left">
              <?php
                if (substr($kdso,0,2) == 'KW') {
                  $arr = explode(',', $row['kdlokasi']);
                  foreach ($arr as $kode) {
                    if( array_key_exists($kode, $lokasi) ) $nama = $lokasi[$kode]['nmlokasi']; else $nama = '...';
                    echo "[$kode] $nama <br>";
                  } 
                } else {
                  $arr = explode(',', $row['kddept']);
                  foreach ($arr as $kode) {
                    if( array_key_exists($kode, $dept) ) $nama = $dept[$kode]['nmdept']; else $nama = '...';
                    echo "[$kode] $nama <br>";
                  } 
                }
              ?>
            </td>
            <td class="text-left">
              <?php echo
                '<b>'. $row['cp1'] .'</b><br><small>HP. </small>'. $row['hp1'] .'<br>'. 
                '<b>'. $row['cp2'] .'</b><br><small>HP </small>'. $row['hp2'] .'<br>'.
                '<b>Intern : </b><br>'. $row['intern'] 
              ?>
            </td>
          </tr>

        <?php } ?>
      </tbody>
    </table>

  </div>
</div>

<script type="text/javascript">
  // PENCARIAN
  $("#cari").keyup( function(event) {
    var kata = $('#cari').val();

    $('#tr_crud').insertAfter( $('#tr_cari') );
    $('#tr_crud').css('display', 'none');

    $('.All').show();
    $('#iGrid > tbody  > tr').each(function() {
      var id  = $(this).attr('id');
      var str = $('#'+id).text().toLowerCase();
      if (str.indexOf( kata.toLowerCase() ) < 0) $('#'+id).hide();
    });
  });


  // Click ROW untuk Buka Form CRUD
  function move_row( idkey ) {
    if ( $('#aktif').val()==idkey ) {
      $('#aktif').val('');
      $('#tr_crud').css('display', 'none');
    } else { 
      var arr = $('#'+idkey).attr('class').replace(' text-bold','').split("#");
      $('#tr_crud').css('display', '');
      $('#tr_crud').insertAfter( $('#'+idkey) );

      $("#0").prop('readonly', true);
      $("input[ name='isian[]' ]").each(function () { $(this).val(arr[ $(this).attr('id') ]); })
      $('#2').html( arr[2] );
      $('#Rekam').hide(); $('#Ubah').show(); $('#Hapus').show(); $('#aktif').val(idkey);
    }
  }


  // Click Tombol PLUS untuk Buka Form CRUD
  function newadd() {
    if ( $('#aktif').val()=='tr_crud' ) {
      $('#aktif').val('');
      $('#tr_crud').css('display', 'none');
    } else {
      $("#0").prop('readonly', false);
      $('#tr_crud').css('display', ''); 
      $('#tr_crud').insertAfter( $('#tr_cari') );
      $("input[ name='isian[]' ]").each(function () { $(this).val(''); })
      $('#Rekam').show(); $('#Ubah').hide(); $('#Hapus').hide(); $('#aktif').val('tr_crud');
    }
  }

</script>


