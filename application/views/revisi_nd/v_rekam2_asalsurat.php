<div  ng-controller="ctrlrefr">
    <div class="form-group jarak">
        <label class="col-sm-2 rev text-right">K/L : </label>
        <div class="col-sm-8" style="padding:0px">
            <input class="form-control" readonly value="<?= $nd['revisi']['kl_dept'] .' '.str_replace("Dan","dan",ucwords(strtolower($nd['revisi']['nmdept'])))  ?>">
        </div>
    </div>
    <div class="form-group jarak">
        <label class="col-sm-2 rev text-right">Unit Eselon I : </label>
        <div class="col-sm-8" style="padding:0px">
            <input class="form-control" readonly value="<?= $nd['revisi']['kl_dept'] .'.'. $nd['revisi']['kl_unit'] .' '. $nd['revisi']['nmunit'] ?>">
        </div>
    </div>

    <?php foreach ($nd['surat'] as $row) { ?>
        <div class="form-group jarak">
            <label class="col-sm-2 rev text-right">Pejabat : </label>
            <div class="col-sm-8" style="padding:0px">
                <input class="form-control" readonly value="<?= $row['kl_pjb_jab'] ?>">
            </div>
        </div>
        <div class="form-group jarak">
            <label class="col-sm-2 rev text-right">No. Surat : </label>
            <div class="col-sm-3" style="padding:0px">
                <input class="form-control" readonly value="<?= $row['kl_surat_no'] ?>">
            </div>
            <label class="col-sm-2 rev text-right">Tanggal Surat : </label>
            <div class="col-sm-3" style="padding-right:0px">
                <div class="input-group date datetimepicker1">
                    <input class="form-control" readonly value="
                        <?php
                           if (($row['kl_surat_tgl'])<>'0000-00-00') echo $this->fc->idtgl($row['kl_surat_tgl'], 'hari')
                        ?>">
                      <span class="input-group-addon">
                        <i class="fa fa-calendar text-primary"></i>
                    </span>
                </div>
            </div>
        </div>
        <div class="form-group jarak">
            <label class="col-sm-2 rev text-right">Hal : </label>
            <div class="col-sm-8" style="padding:0px">
                <input class="form-control" readonly value="<?= $row['kl_surat_hal'] ?>">
            </div>
        </div>
    <?php } ?>

</div>

<!-- Tujuan  -->
<div class="box-header with-border revHead">
    <h4 class="box-title" style="color:#FFF;padding-left:5px">1. TUJUAN</h4>
</div>

<div class="form-group jarak">
    <div class="col-sm-10" style="padding:5px 0px 5px 30px">
        <label for="">Tujuan : </label>
        <textarea name="f1_tujuan" class="form-control" placeholder="Uraian Tujuan Nota Dinas" rows="5">
        <?php if (empty($nd['nota']['f1_tujuan']))
            {echo "Menindaklanjuti surat ".$row['kl_pjb_jab']." nomor ".$row['kl_surat_no']." tanggal ".$this->fc->idtgl($row['kl_surat_tgl'], 'tgl')." tentang ".$row['kl_surat_hal'].".";}
           else {echo $nd['nota']['f1_tujuan'];}
        ?>
        </textarea>
    </div>
</div>

<div style="padding-bottom:10px"></div>
