<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-type" content="text/html;charset=UTF-8">
<style type="text/css">

  .vertical_line { margin-top:1px;height:2px; width:600px;background:#000; }
  .vertical_line2 { height:1px; width:600px;background:#000; }
  .tes {vertical-align: middle; }
  table { border-collapse: collapse; font-family: Arial,Helvetica,sans-serif; font-style: normal;font-variant:normal; border-spacing: 10px;}
  p {
    text-indent: 20px; margin-top:0px; margin-bottom: 0px;
  }
  @page { margin: 0.3in 1in 0.3in 1in; }
  .firstpage { page-break-after: always; }
  .otherpages{ margin: 0.3in 0.5in 0.5in -0.2in; }
  .isisurat {font-size: 11pt}
</style>
</head>
<body>

 <table>
    <tbody>
      <tr>
        <td width="50"><img src="<?='files/cuti/header.jpg' ?>" width="96" height="100%" /></td>
          <td width="320">
            <table style="margin-left: 0px;">
              <tbody>
                <tr>
                  <td style="font-size: 13pt;text-align:center"><strong>KEMENTERIAN KEUANGAN REPUBLIK INDONESIA</strong></td>
                </tr>
                <tr>
                  <td style="font-size: 12pt;text-align:center"><strong>DIREKTORAT JENDERAL ANGGARAN</strong></td>
                </tr>
                <tr>
                  <td style="font-size: 11pt;text-align:center; text-transform: uppercase; "><strong><?php  echo substr ($nd['revisi']['direktur'], 9); ?></strong></td>
                </tr>
                <tr style="margin-top:100px;">
                  <td></td>
                </tr>
                <tr>
                  <td style="font-size: 7pt;text-align:center">GEDUNG SUTIKNO SLAMET, JL. DR. WAHIDIN NO. 1 JAKARTA PUSAT 10710 KOTAK POS 2435 JKP 10024</td>
                </tr>
                <tr>
                  <td style="font-size: 7pt;text-align:center;">TELEPON (021) 3866117: FAKSIMILE (021) 3505118; SITUS: WWW.ANGGARAN.DEPKEU.GO.ID</td>
                </tr>
              </tbody>
            </table>
          </td>
      </tr>
    </tbody>
  </table>

<div class="vertical_line"></div>

<div class="isisurat">

  <table style="margin-top: 10px; margin-left:150px;text-align:center;line-height:16px;">
    <tr>
      <td>NOTA DINAS</td>
    </tr>
    <tr>
      <td>Nomor&nbsp;ND-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; /AG.<?php echo substr($this->session->userdata('kdso'),1,1); ?><?php echo substr($this->session->userdata('kdso'),3,1); ?>/<?php echo date('Y') ?></td>
    </tr>
  </table>

  <table style="width:600px;font-size:11pt;text-align:left;line-height:17px;margin-top: 14px;margin-bottom: 5px;">
    <tbody style=" vertical-align: text-top;">
      <tr>
       <td>Yth.</td>
       <td>:</td>
       <td><?php
       echo str_replace('Direktorat','',$nd['revisi']['direktur']);
       ?></td>
      </tr>
      <tr>
        <td>Dari</td>
        <td>:</td>
        <td><?php echo $nd['revisi']['subdit']; ?></td>
      </tr>
      <tr>
        <td>Sifat</td>
        <td>:</td>
        <td>Segera</td>
      </tr>
      <tr>
        <td>Lampiran</td>
        <td>:</td>
        <td><?php echo $nd['nota']['nd_lamp_jml']!=0?$nd['nota']['nd_lamp_jml'].' Dokumen':'-' ?> </td>
      </tr>
      <tr>
        <td>Hal</td>
        <td>:</td>
        <td><?php echo $nd['nota']['nd_hal']; ?></td>
      </tr>
      <tr>
        <td>Tanggal</td>
        <td>:</td>
        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $nd['nota']['nd_tgl'] ?></td>
      </tr>
    </tbody>
   </table>
<div class="vertical_line2"></div>

<table style="justify-content:width:700px;font-size:11pt;text-align:justify;margin-left:0px;margin-top: 10px">
    <tr>
      <td><p>Menindaklanjuti surat dari <?php echo $nd2['kl_pjb_jab']; ?> nomor
      <?php echo $nd['revisi']['kl_surat_no']; ?> tanggal <?php echo $this->fc->idtgl($nd['revisi']['kl_surat_tgl'],'tglfull'); ?> perihal <?php echo $nd['revisi']['kl_surat_hal']; ?>
      bersama ini kami sampaikan hal-hal sebagai berikut:</p></td>
    </tr>
</table>

<table style="font-size: 11pt;margin-top: 5px;line-height: 16px;text-align: justify;">
  <tr>
    <td style="margin-top: 5px!important"><b>1.</b></td>
    <td></td>
    <td style="margin-top: 5px!important"><b>Tujuan</b></td>
  </tr>
  <tr>
    <td></td>
    <td></td>
    <td style="margin-top: 5px!important;margin-bottom: 5px!important; margin-left: 0px!important;"><p><?php echo $nd['nota']['f1_tujuan']; ?></p></td>
  </tr>
  <tr>
    <td style="margin-top: 5px!important"><b>2.</b></td>
    <td></td>
    <td style="margin-top: 5px!important;"><b>Pembahasan</b></td>
  </tr>
  <tr>
    <td style="margin-top: 10px!important;"></td>
    <td></td>
    <td style="margin-top: 10px!important;"><b>A.&nbsp;Latar Belakang Usulan Revisi Anggaran</b></td>
  </tr>
  <tr>
    <td></td>
    <td></td>
    <td style="padding-left: 0px!important; margin-left: 0px!important;"><p><?php echo $nd['nota']['f2_latar_blkg']; ?></p></td>
  </tr>
  <tr>
    <td style="margin-top: 10px!important;"></td>
    <td></td>
    <td style="margin-top: 10px!important;"><b>B.&nbsp;Jenis Revisi</b></td>
  </tr>
  <?php $i=0; ?>
  <tr>
    <td></td>
    <td></td>
    <td style="padding-left: 20px!important; margin-left: 0px!important;margin-top: 5px!important">
      <?php if($nd['nota']['f2_jnsrev1_a'] !=='' OR $nd['nota']['f2_jnsrev1_b'] !=='' OR $nd['nota']['f2_jnsrev1_c'] !=='') {
        echo ++$i.') '. 'Perubahan Anggaran Termasuk Perubahan Rinciannya' ;
          }
      ?>
    </td>
  <tr>
    <td></td>
    <td></td>
    <td style="padding-left: 18px!important; margin-left: 0px!important;">
    <?php 
     $j = 0;
          if ($nd['nota']['f2_jnsrev1_a']) {
            $j++;
          }
          if ($nd['nota']['f2_jnsrev1_b']) {
            $j++;
          }
          if ($nd['nota']['f2_jnsrev1_c']) {
            $j++;
          }
     ?>
      <?php if ($j > 1): ?>
      <ol type="a" style="vertical-align: top; text-align: justify; line-height: 17px;margin-top: 0px!important; margin-bottom: 0px!important;">
      <?php else: ?>
      <ol type="a" style="vertical-align: top; text-align: justify; line-height: 17px;margin-top: 0px!important; margin-bottom: 0px!important;margin-left:-21px">
      <?php endif ?>
        <?php
          if ($j > 1) {
            if ($nd['nota']['f2_jnsrev1_a']) echo '<li>'. substr($nd['nota']['f2_jnsrev1_a'],3) .'</li>';
            if ($nd['nota']['f2_jnsrev1_b']) echo '<li>'. substr($nd['nota']['f2_jnsrev1_b'],3) .'</li>';
            if ($nd['nota']['f2_jnsrev1_c']) echo '<li>'. substr($nd['nota']['f2_jnsrev1_c'],3) .'</li>';
          } else {
            if ($nd['nota']['f2_jnsrev1_a']) echo substr($nd['nota']['f2_jnsrev1_a'],3);
            if ($nd['nota']['f2_jnsrev1_b']) echo substr($nd['nota']['f2_jnsrev1_b'],3);
            if ($nd['nota']['f2_jnsrev1_c']) echo substr($nd['nota']['f2_jnsrev1_c'],3);
          }
          
         ?>
      </ol>
    </td>
  </tr>
  <tr>
    <td></td>
    <td></td>
    <td style="line-height: 17px;margin-top: 0px!important;padding-left: 37px!important; margin-top: 0px!important;">
      <?php if ($nd['nota']['f2_jnsrev1_ket']) {echo 'Keterangan :'.'<br>'.'<p>' .$nd['nota']['f2_jnsrev1_ket'].'</p>'; }?>
    </td>
  </tr>
  <tr>
    <td></td>
    <td></td>
    <td style="padding-left: 20px!important; margin-left: 0px!important;margin-top: 5px!important
    <?php if ($nd['nota']['f2_jnsrev1_ket']=='') echo "margin-top: 0px!important"; ?>">
      <?php if($nd['nota']['f2_jnsrev2_a']!='' OR $nd['nota']['f2_jnsrev2_b']!='' OR $nd['nota']['f2_jnsrev2_c']!='') {
        echo ++$i.') '. 'Pergeseran Anggaran Termasuk Perubahan Rinciannya Dalam Hal Pagu Tetap';
        } ?>
    </td>
  </tr>
  <tr>
    <td></td>
    <td></td>
    <td style="padding-left: 18px!important; margin-left: 0px!important;">
    <?php 
     $k = 0;
          if ($nd['nota']['f2_jnsrev2_a']) {
            $k++;
          }
          if ($nd['nota']['f2_jnsrev2_b']) {
            $k++;
          }
          if ($nd['nota']['f2_jnsrev2_c']) {
            $k++;
          }
     ?>
      <?php  if ($k > 1): ?>
      <ol type="a" style="vertical-align: top; text-align: justify; line-height: 17px;margin-top: 0px!important;margin-bottom: 0px!important;">
      <?php else: ?>
      <ol type="a" style="vertical-align: top; text-align: justify; line-height: 17px;margin-top: 0px!important;margin-bottom: 0px!important;margin-left:-21px">
      <?php endif ?>  
        <?php
            if ($k > 1) {
            if ($nd['nota']['f2_jnsrev2_a']) echo '<li>'. substr($nd['nota']['f2_jnsrev2_a'],3) .'</li>';
            if ($nd['nota']['f2_jnsrev2_b']) echo '<li>'. substr($nd['nota']['f2_jnsrev2_b'],3) .'</li>';
            if ($nd['nota']['f2_jnsrev2_c']) echo '<li>'. substr($nd['nota']['f2_jnsrev2_c'],3) .'</li>';
            } else {
            if ($nd['nota']['f2_jnsrev2_a']) echo substr($nd['nota']['f2_jnsrev2_a'],3);
            if ($nd['nota']['f2_jnsrev2_b']) echo substr($nd['nota']['f2_jnsrev2_b'],3);
            if ($nd['nota']['f2_jnsrev2_c']) echo substr($nd['nota']['f2_jnsrev2_c'],3);
            }
         ?>
      </ol>
    </td>
  </tr>
  <tr>
    <td style="padding-left: 20px!important;"></td>
    <td></td>
    <td style="line-height: 17px;margin-top: 0px!important;padding-left: 37px!important; margin-top: 0px!important;">
      <?php if ($nd['nota']['f2_jnsrev2_ket']) { echo 'Keterangan :'.'<br>'.'<p>'.$nd['nota']['f2_jnsrev2_ket'].'</p>'; } ?>
    </td>
  </tr>
  <tr>
    <td></td>
    <td></td>
    <td style="padding-left: 20px!important;margin-left: 0px!important;margin-top: 5px!important">
      <?php if ($nd['nota']['f2_jnsrev3_a'] !='' OR $nd['nota']['f2_jnsrev3_b'] !='' OR $nd['nota']['f2_jnsrev3_c'] !=''){
        echo ++$i.') '. 'Revisi Administrasi';
        } else {
          echo '';
          } ?>
    </td>
  </tr>
  <tr>
    <td></td>
    <td></td>
    <td style="padding-left: 18px!important; margin-left: 0px!important;">
    <?php 
     $l = 0;
          if ($nd['nota']['f2_jnsrev3_a']) {
            $l++;
          }
          if ($nd['nota']['f2_jnsrev3_b']) {
            $l++;
          }
          if ($nd['nota']['f2_jnsrev3_c']) {
            $l++;
          }
     ?>
      <?php  if ($l > 1): ?>
      <ol type="a" style="vertical-align: top; text-align: justify; line-height: 17px;margin-top: 0px!important;margin-bottom: 0px!important;">
      <?php else: ?>
      <ol type="a" style="vertical-align: top; text-align: justify; line-height: 17px;margin-top: 0px!important;margin-bottom: 0px!important;margin-left:-21px">
      <?php endif ?>  
        <?php
            if ($l > 1) {
            if ($nd['nota']['f2_jnsrev3_a']) echo '<li>'. substr($nd['nota']['f2_jnsrev3_a'],3) .'</li>';
            if ($nd['nota']['f2_jnsrev3_b']) echo '<li>'. substr($nd['nota']['f2_jnsrev3_b'],3) .'</li>';
            if ($nd['nota']['f2_jnsrev3_c']) echo '<li>'. substr($nd['nota']['f2_jnsrev3_c'],3) .'</li>';
            } else {
            if ($nd['nota']['f2_jnsrev3_a']) echo substr($nd['nota']['f2_jnsrev3_a'],3);
            if ($nd['nota']['f2_jnsrev3_b']) echo substr($nd['nota']['f2_jnsrev3_b'],3);
            if ($nd['nota']['f2_jnsrev3_c']) echo substr($nd['nota']['f2_jnsrev3_c'],3);
            }
         ?>
      </ol>
    </td>
  </tr>
  <tr>
    <td style="padding-left: 20px!important;"></td>
    <td></td>
    <td style="line-height: 17px;margin-top: 0px!important;padding-left: 37px!important; margin-top: 0px!important;">
      <?php if ($nd['nota']['f2_jnsrev3_ket'] !='') echo 'Keterangan :'.'<br>'.'<p>' .$nd['nota']['f2_jnsrev3_ket'].'</p>'; ?>
    </td>
  </tr>
  <tr>
    <td></td>
    <td></td>
    <td style="padding-left: 20px!important;margin-left: 0px!important;margin-top: 5px!important">
      <?php if ($nd['nota']['f2_jnsrev4_a'] !='' OR $nd['nota']['f2_jnsrev4_b'] !='' OR $nd['nota']['f2_jnsrev4_c'] !='' ) {
        echo ++$i.') '.'Revisi Kewenangan Kanwil Ditjen Perbendaharaan';}
      ?>
    </td>
  </tr>
  <tr>
    <td></td>
    <td></td>
    <td style="padding-left: 18px!important; margin-left: 0px!important;">
    <?php 
     $m = 0;
          if ($nd['nota']['f2_jnsrev4_a']) {
            $m++;
          }
          if ($nd['nota']['f2_jnsrev4_b']) {
            $m++;
          }
          if ($nd['nota']['f2_jnsrev4_c']) {
            $m++;
          }
     ?>
      <?php  if ($m > 1): ?>
      <ol type="a" style="vertical-align: top; text-align: justify; line-height: 17px;margin-top: 0px!important;margin-bottom: 0px!important;">
      <?php else: ?>
      <ol type="a" style="vertical-align: top; text-align: justify; line-height: 17px;margin-top: 0px!important;margin-bottom: 0px!important;margin-left:-21px">
      <?php endif ?>  
        <?php
            if ($m > 1) {
            if ($nd['nota']['f2_jnsrev4_a']) echo '<li>'. substr($nd['nota']['f2_jnsrev4_a'],3) .'</li>';
            if ($nd['nota']['f2_jnsrev4_b']) echo '<li>'. substr($nd['nota']['f2_jnsrev4_b'],3) .'</li>';
            if ($nd['nota']['f2_jnsrev4_c']) echo '<li>'. substr($nd['nota']['f2_jnsrev4_c'],3) .'</li>';
            } else {
            if ($nd['nota']['f2_jnsrev4_a']) echo substr($nd['nota']['f2_jnsrev4_a'],3);
            if ($nd['nota']['f2_jnsrev4_b']) echo substr($nd['nota']['f2_jnsrev4_b'],3);
            if ($nd['nota']['f2_jnsrev4_c']) echo substr($nd['nota']['f2_jnsrev4_c'],3);
            }
         ?>
      </ol>
    </td>
  </tr>
  <tr>
    <td style="padding-left: 20px!important;"></td>
    <td></td>
    <td style="line-height: 17px;margin-top: 0px!important;padding-left: 37px!important; margin-top: 0px!important;">
      <?php if ($nd['nota']['f2_jnsrev4_ket'] !='') echo 'Keterangan :'.'<br>'.'<p>' .$nd['nota']['f2_jnsrev4_ket'].'</p>'; ?>
    </td>
  </tr>
  <tr>
    <td style="margin-top: 5px!important;"></td>
    <td></td>
    <td style="margin-top: 5px!important;"><b>C.&nbsp;Penelaahan Usulan Revisi Anggaran</b></td>
  </tr>
  <tr>
    <td></td>
    <td></td>
    <td><?php
        if($nd2['t3_penelaahan_tgl'] =='') {
          echo '<p>'."Terhadap usulan revisi, tidak dilakukan Penelaahan".'</p>';
        }
        else {
          echo "<p>Terhadap usulan revisi, telah dilakukan penelaahan pada tanggal ".$this->fc->idtgl($nd2['t3_penelaahan_tgl'],'tglfull')."." ."</p>";
        } ?>
    </td>
  </tr>

  <tr>
    <td style="margin-top: 10px!important;"></td>
    <td></td>
    <td style="margin-top: 10px!important;"><b>D. Dokumen Yang Dipersyaratkan</b></td>
  </tr>
  <?php $a=0; ?>
  <tr>
    <td></td>
    <td></td>
    <td>
      <p>Dokumen usulan revisi anggaran yang disampaikan sebagai berikut : <br>
        <p><?php if ($nd2['doc_usulan_revisi']) echo ++$a.'.'.' Surat usulan revisi yang ditandatangani pejabat eselon I '; ?></p><br>
        <p><?php if ($nd2['doc_matrix']) echo ++$a.'.'.' Matriks Perubahan (semula menjadi)'; ?></p> <br>
        <p><?php if ($nd2['doc_rka']) echo ++$a.'.'.' Rencana Kerja dan Anggaran (RKA) Satker'; ?></p> <br>
        <p><?php if ($nd2['doc_adk']) echo ++$a.'.'.' Arsip Data Komputer (ADK) RKA-K/L DIPA revisi Satker'; ?></p><br>
       <!--  <p><?php if ($nd2['doc_dukung_sepakat']) echo ++$a.'.'.'Dokumen pendukung terkait sesuai hasil kesepakatan antara K/L dengan DJA dalam pembahasan usulan revisi anggaran'; ?></p> <br>
        <p><?php if ($nd2['doc_dukung_hal4']) echo ++$a.'.'.'Dokumen pendukung terkait perubahan/penghapusan catatan dalam halaman IV DIPA'; ?></p><br>
        <p><?php if ($nd2['doc_dukung_lainnya']) echo ++$a.'.'.'Dokumen pendukung terkait lainnya'; ?></p> -->
      </p>
    </td>
  </tr>

  <tr>
    <td style="margin-top: 5px!important;"><b>3.</b></td>
    <td></td>
    <td style="margin-top: 5px!important;"><b>Pendapat</b></td>
  </tr>
  <tr>
    <td style="margin-top: 5px!important; padding-left: 5px!important;"></td>
    <td></td>
    <td style="line-height: 17px; padding-left:0px;margin-top: 5px!important;padding-left: 5px!important;"><p><?php echo $nd['pendapat']['pendapat']; ?></p></td>
  </tr>
</table>

<table style="width: 600px;font-size:11pt;padding-left:5px;text-align:left;margin-left: 20px;margin-bottom: 0px!important;">
    <tbody style=" vertical-align: text-top;">
      <tr>
        <td style="width: 25px">3.1</td>
        <td style="width: 400px;"><?php echo $nd['pendapat']['hal1'] ?></td>
        <td>:</td>
        <td style="width: 165px"><?php echo $nd['nota']['f3_batasan_rev'] ?></td>
      </tr>
      <tr>
        <td style="width: 25px">3.2</td>
        <td style="width: 400px"><?php echo $nd['pendapat']['hal2']; ?></td>
        <td>:</td>
        <td style="width: 165px"><?php echo $nd['nota']['f3_jns_wenang']; ?></td>
      </tr>
      <tr>
        <td style="width: 25px">3.3</td>
        <td style="width: 400px"><?php echo $nd['pendapat']['hal3']; ?></td>
        <td>:</td>
        <td style="width: 165px"><?php echo $nd['nota']['f3_dok_usulan']; ?></td>
      </tr>
      <tr>
        <td style="width: 25px">3.4</td>
        <td style="width: 400px"><?php echo $nd['pendapat']['hal4']; ?></td>
        <td>:</td>
        <td style="width: 165px"><?php echo $nd['nota']['f3_dok_pendukung']; ?></td>
      </tr>
    </tbody>
</table>

<table>
  <tbody>
    <tr>
      <td style="margin-left:25px!important"><?php if ($nd['nota']['f3_ket_catatan'] !='') echo 'Keterangan:' ?></td>
    </tr>
  </tbody>
</table>

<table style="width: 600px;font-size:11pt;text-align:justify;margin-left: 20px;margin-bottom: 0px!important;">
  <tbody style=" vertical-align: text-top;">
    <?php
      foreach ($pot as $row){ ?>
        <tr><td><?php echo $row;?></td></tr>
      <?php } ?>
  </tbody>
</table>

<table style="font-size: 11pt;margin-top: 0px;line-height: 16px;text-align: justify; ">
  <tr>
    <td style="margin-top: 5px!important;"><b>4.</b>&nbsp;&nbsp;</td>
    <td></td>
    <td style="margin-top: 5px!important;"><b>Rekomendasi</b></td>
  </tr>
</table>
<table style="margin-left: 388px; margin-bottom: -10px!important;">
  <tr>
    <td>Ribu</td>
    <td></td>
    <td>Rupiah</td>
  </tr>
</table>

<table border="1px" style="width:600px;font-size:11pt;text-align:left;margin-bottom: 12px;margin-top: 0px;padding-left:15px">
  <thead >
    <tr style="text-align: center; font-weight: bold;line-height:17px;"><b></b>
      <td>Uraian</td>
      <td>Semula</td>
      <td>Menjadi</td>
      <td>Selisih</td></b>
    </tr>
  </thead>
  <tbody style="font-variant-numeric: all;line-height:15px;">
    <?php
    $psml = $nd['nota']['pagu_rm_mula']+$nd['nota']['pagu_pnbp_mula']+$nd['nota']['pagu_phln_mula']+$nd['nota']['pagu_sbsn_mula'];
    $psjd = $nd['nota']['pagu_rm_jadi']+$nd['nota']['pagu_pnbp_jadi']+$nd['nota']['pagu_phln_jadi']+$nd['nota']['pagu_sbsn_jadi'];
    $tarsml = $nd['nota']['pagu_pdpt_pajak_mula']+$nd['nota']['pagu_pdpt_pnbp_mula'];
    $tarsjd = $nd['nota']['pagu_pdpt_pajak_jadi']+$nd['nota']['pagu_pdpt_pnbp_jadi'];
     ?>
    <tr><b>
      <td style="text-align: left;"><b>Pagu</b></td>
      <td style="text-align: right;"><?php echo number_format($psml) ?></td>
      <td style="text-align: right;"><?php echo number_format($psjd) ?></td>
      <td style="text-align: right;"><?php echo number_format($psjd-$psml) ?></b>
    </tr>
    <tr>
      <td colspan="4" style="text-align: left;">Rincian Sumber Dana :</td>
    </tr>
    <tr>
      <td style="text-align: left;">RM</td>
      <td style="text-align: right;"><?php echo number_format($nd['nota']['pagu_rm_mula']); ?></td>
      <td style="text-align: right;"><?php echo number_format($nd['nota']['pagu_rm_jadi']); ?></td>
      <td style="text-align: right;"><?php echo number_format($nd['nota']['pagu_rm_jadi']-$nd['nota']['pagu_rm_mula']); ?></td>
    </tr>
    <tr>
      <td style="text-align: left;">PNBP/BLU</td>
      <td style="text-align: right;"><?php echo number_format($nd['nota']['pagu_pnbp_mula']); ?></td>
      <td style="text-align: right;"><?php echo number_format($nd['nota']['pagu_pnbp_jadi']); ?></td>
      <td style="text-align: right;"><?php echo number_format($nd['nota']['pagu_pnbp_jadi']-$nd['nota']['pagu_pnbp_mula']); ?></td>
    </tr>
    <tr>
      <td style="text-align: left;">PHLN/PHDN</td>
      <td style="text-align: right;"><?php echo number_format($nd['nota']['pagu_phln_mula']); ?></td>
      <td style="text-align: right;"><?php echo number_format($nd['nota']['pagu_phln_jadi']); ?></td>
      <td style="text-align: right;"><?php echo number_format($nd['nota']['pagu_phln_jadi']-$nd['nota']['pagu_phln_mula']); ?></td>
    </tr>
    <tr>
      <td style="text-align: left;">SBSN-PBS</td>
      <td style="text-align: right;"><?php echo number_format($nd['nota']['pagu_sbsn_mula']); ?></td>
      <td style="text-align: right;"><?php echo number_format($nd['nota']['pagu_sbsn_jadi']); ?></td>
      <td style="text-align: right;"><?php echo number_format($nd['nota']['pagu_sbsn_jadi']-$nd['nota']['pagu_sbsn_mula']); ?></td>
    </tr>
    <tr>
      <td style="text-align: left;"><b>Target Pendapatan</b></td>
      <td style="text-align: right;"><?php echo number_format($tarsml); ?></td>
      <td style="text-align: right;"><?php echo number_format($tarsjd) ?></td>
      <td style="text-align: right;"><?php echo number_format($tarsjd-$tarsml) ?></td>
    </tr>
    <tr>
      <td style="text-align: left;">Perpajakan</td>
      <td style="text-align: right;"><?php echo number_format($nd['nota']['pagu_pdpt_pajak_mula']); ?></td>
      <td style="text-align: right;"><?php echo number_format($nd['nota']['pagu_pdpt_pajak_jadi']); ?></td>
      <td style="text-align: right;"><?php echo number_format($nd['nota']['pagu_pdpt_pajak_jadi']-$nd['nota']['pagu_pdpt_pajak_mula']); ?></td>
    </tr>
    <tr>
      <td style="text-align: left;">PNBP</td>
      <td style="text-align: right;"><?php echo number_format($nd['nota']['pagu_pdpt_pnbp_mula']); ?></td>
      <td style="text-align: right;"><?php echo number_format($nd['nota']['pagu_pdpt_pnbp_jadi']); ?></td>
      <td style="text-align: right;"><?php echo number_format($nd['nota']['pagu_pdpt_pnbp_jadi']-$nd['nota']['pagu_pdpt_pnbp_mula']); ?></td>
    </tr>
    <tr>
      <td style="text-align: left;"><b>Catatan Halaman IV DIPA</b></td>
      <td style="text-align: right;"><?php echo number_format($nd['nota']['pagu_hal_4_mula']); ?></td>
      <td style="text-align: right;"><?php echo number_format($nd['nota']['pagu_hal_4_jadi']); ?></td>
      <td style="text-align: right;"><?php echo number_format($nd['nota']['pagu_hal_4_jadi']-$nd['nota']['pagu_hal_4_mula']); ?></td>
    </tr>
  </tbody>
</table>

<table style="justify-content:width:700px;font-size:11pt;margin-bottom:0px;text-align: justify;text-justify: inter-word;padding-left:15px">
  <tr>
    <td><?php echo $nd['pendapat']['rekomendasi']; ?>&nbsp;<?php echo $nd1['f4_usulan_rev'] ?>.</td>
  </tr>
</table>

<table>
  <tr>
    <td></td>
    <td></td>
    <td style="line-height: 17px;text-align: justify;">
      <?php if ($nd['nota']['f4_catatan'] !='') echo 'Catatan :'.'<br>'.'<p>' .$nd['nota']['f4_catatan'].'</p>'; ?>
    </td>
  </tr>
</table>

<table style="justify-content:width:700px;font-size:11pt;text-align: justify;text-justify: inter-word;margin-top: 0px;">
  <tr>
    <td ><p>Apabila Bapak/Ibu tidak berpendapat lain, terlampir disampaikan konsep/net surat <?php if( $nd1['f4_usulan_rev']=='usulan revisi tidak dapat ditetapkan' ) {echo 'penolakan';} elseif($nd1['f4_usulan_rev']=='usulan revisi dapat ditetapkan') {echo 'pengesahan';} elseif($nd1['f4_usulan_rev']=='usulan revisi dapat ditetapkan sebagian, dan sebagian ditolak') {echo 'pengesahan dan penolakan';} ?> revisi anggaran kepada Direktur Jenderal Perbendaharaan c.q. Direktur Sistem Perbendaharaan dan <?php echo $nd2['kl_pjb_jab']; ?>, untuk ditetapkan.</p></td>
  </tr>
</table>

<br>
<br>

 <table style="margin-left: 310px;width: 600px ;text-transform:capitalize ; font-size: 11pt; height: 63px;margin-top:20px;line-height:1.5;">
       <tbody>
        <tr>
          <td>&nbsp;</td>
       <td><?php if ( ($nd['nota']['nd_dit_pgs'])=='Plt.') echo $nd['revisi']['subdit']; else echo "" ?></td>
        </tr>
        </tbody>
     </table>
     <table>
         <tbody>
             <tr>
                 <td>&nbsp;</td>
             </tr>
         </tbody>
     </table>
     <p>&nbsp;</p>

    <table style="margin-left: 314px;height: 20px; font-size: 11pt;margin-top:10px;" border="0">
         <tbody>
             <tr>
                 <td><?php echo $nd1['nd_subdit_nama']; ?></td>
             </tr>
             <tr>
                 <td>NIP <?php echo substr($nd1['nd_subdit_nip'], 0,8); ?>&nbsp;<?php  echo substr ($nd1['nd_subdit_nip'], 8,6); ?>&nbsp;<?php echo substr($nd1['nd_subdit_nip'], -4,1); ?>&nbsp;<?php echo substr($nd1['nd_subdit_nip'], -3); ?></td>
             </tr>
         </tbody>
    </table>
</div>
</body>
</html>
