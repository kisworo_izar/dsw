<script src="<?= base_url('assets/plugins/jQuery/jquery.maskMoney.min.js') ?>" type="text/javascript"></script>
<link href= "<?= base_url('assets/plugins/select2/select2.css') ?>" rel="stylesheet" type="text/css" />
<script src="<?= base_url('assets/plugins/select2/select2.full.min.js') ?>" type="text/javascript"></script>
<link href= "<?= base_url('assets/bootstrap/css/bootstrap-datetimepicker.min.css') ?>" rel="stylesheet" type="text/css" />
<script src="<?= base_url('assets/plugins/datepicker/moment.min.js') ?>" type="text/javascript"></script>
<script src="<?= base_url('assets/plugins/datepicker/id.js') ?>" type="text/javascript"></script>
<script src="<?= base_url('assets/bootstrap/js/bootstrap-datetimepicker.min.js') ?>" type="text/javascript"></script>
<script src="<?= base_url('assets/AngularJS/angular.min.js') ?>"></script>
<script src="<?= base_url('assets/AngularJS/angular-locale_id-id.js') ?>"></script>
<script src="<?= base_url('assets/AngularJS/angular-input-masks-dependencies.js') ?>"></script>
<script src="<?= base_url('assets/AngularJS/angular-input-masks.js') ?>"></script>

<style media="screen">
  .checkbox-custom, .radio-custom{opacity: 0;position: absolute;}
  .checkbox-custom, .checkbox-custom-label, .radio-custom, .radio-custom-label{
    display: inline-block;vertical-align: middle;margin: 0px;cursor: pointer;}
  .checkbox-custom-label, .radio-custom-label {position: relative;}
  .checkbox-custom + .checkbox-custom-label:before, .radio-custom + .radio-custom-label:before{
    content: '';background: #fff;border: 1px solid #DFDFDF;display: inline-block;vertical-align: middle;
    width: 20px;height: 20px;padding: 2px;margin-right: 5px;text-align: center;}
  .checkbox-custom:checked + .checkbox-custom-label:before{background: #3C8DBC;}
  .radio-custom + .radio-custom-label:before{border-radius: 50%;}
  .radio-custom:checked + .radio-custom-label:before {background: #3C8DBC;}
  .checkbox-custom:focus + .checkbox-custom-label, .radio-custom:focus + .radio-custom-label {outline: 0px solid #ddd;}

  .rev{padding-top:8px;padding-left:35px;text-indent:-20px}
  .revHead{border-radius:0px;border-top: 1px solid #DFDFDF;border-bottom: 1px solid #DFDFDF; padding-top:5px; padding-bottom:5px; background-color:#3C8DBC;}
  .jarak{margin-bottom:8px;}
  .rp_off{background: #ECF0F5;}
  .rp_on{color:#367FA9;}
  .garis{border-right:1px solid #ECF0F5;}
  th, td{padding:5px;}
  .tjudul1{height:35px;border:0px solid red;background:#AFD0E3;text-align:center;font-weight:bold;padding:7px 0px;}
  .tjudul2{height:35px;border:0px solid red;background:#D9E9F2;padding:7px;font-weight:bold;text-align:right;}
  .tjudul3{height:35px;border:0px solid red;background:#FFF;padding:7px;}
  .tdata{height:35px;border:0px solid red;background:#DFDFDF;padding-left:0px;padding-right:0px;}
  .rp_norm{height:35px;text-align: right;color:#367FA9;border:0px;font-weight:normal;background:#FFFFFF}
  .line_r{border-right:solid 1px #88BAD6}
  .line_b{border-bottom:solid 1px #88BAD6}
  .line_b2{border-bottom:solid 1px #e4eff5}
  .fdok{padding-left:30px;text-indent:-30px}
  .selisih{text-align:right;padding-right:12px;padding-top:7px;font-weight:bold;background:#D9E9F2}
</style>

<script>
  var app = angular.module('app', ['ui.utils.masks'])
  app.controller('ctrltabel', function ctrl($scope) {
      $scope.numberWith2Decimals = -1234.178;
      $scope.defaultMoney = 153.12;
      $scope.negativeMoney = -153.12;
  });
  app.controller('ctrlrefr', function ($scope, $http) {
      $http.get('<?= site_url('files/json/t_dept.json') ?>').success( function( data ) {
          $scope.refrdept = data;
      });
      $http.get('<?= site_url('files/json/t_unit.json') ?>').success( function( data ) {
          $scope.refrunit = data;
      });
      $http.get('<?= site_url('files/json/t_jnsrev.json') ?>').success( function( data ) {
          $scope.refrjnsrev = data;
      });
  });
  app.controller("ctrldok" , function($scope){
  $scope.data ={
     namesdok:[{ namedok:""}]
  };
  $scope.addRow = function(index){
  var namedok = {namedok:""};
     if($scope.data.namesdok.length <= index+1){
          $scope.data.namesdok.splice(index+1,0,namedok);
      }
  };
  $scope.deleteRow = function($event,index){
  if($event.which == 1)
     $scope.data.namesdok.splice(index,1);
  }
  });
</script>

<script>
  function toggle($rev) {
   if( document.getElementById($rev).style.display=='none' ){
       document.getElementById($rev).style.display = '';
   }else{
       document.getElementById($rev).style.display = 'none';}
  }
  function togglesrt($srt) {
   if( document.getElementById($srt).style.display=='none' ){
       document.getElementById($srt).style.display = '';
       document.getElementById('btn'+$srt).style.display = 'none';
       document.getElementById('ket'+$srt).style.display = '';
   }else{
       document.getElementById($srt).style.display = 'none';}
  }
  function toggle_opt1($opt) {
   if( $opt == 'on' ){
       document.getElementById("telaah").style.display = '';
   }else{
       document.getElementById("telaah").style.display = 'none';}
  }
  function toggle_opt2($opt) {
   if( $opt == 'on' ){
       document.getElementById("dokrev").style.display = '';
       document.getElementById("dokrevada").innerHTML = " Ada, yaitu : ";
   }else{
       document.getElementById("dokrev").style.display = 'none';
       document.getElementById("dokrevada").innerHTML = " Ada";}
  }
  $(function() {
    $('#rms,#rmm,#pns,#pnm,#phs,#phm,#pdpts,#pdptm,#hal4s,#hal4m').maskMoney({allowNegative: true, thousands:'.', decimal:',', affixesStay: false, precision:0});
  })
</script>

<div class="row" ng-app="app" >
  <div class="col-md-12">
    <div class="box box-widget">
      <div class="box-header with-border">
        <h3 class="text-center" style="margin:0px">Kertas Kerja Nota Dinas Revisi Anggaran</h3>
      </div>

      <form class="form-horizontal" id="form1" role="form">
        <div class="box-body">
            <?php $this->load->view('revisi_nd/v_rekam1_nota'); ?>
            <?php $this->load->view('revisi_nd/v_rekam2_asalsurat'); ?>
            <?php $this->load->view('revisi_nd/v_rekam3_pembahasan'); ?>
            <?php $this->load->view('revisi_nd/v_rekam4_pendapat'); ?>
            <?php $this->load->view('revisi_nd/v_rekam5_rekomendasi'); ?>
        </div>

        <div class="box-footer" id="btn-awal">
          <div class="col-sm-10 pull-right">
            <div class="pull-right">
              <input name="simpan" type="hidden" value="simpan">
              <button type="submit" value="Simpan" id="Simpan" class="btn btn-warning" onclick="crud_form1()">Simpan</button>
              <button type="submit" value="Batal" id="Batal" class="btn btn-default" onclick="window.history.back()">Batal</button>
            </div>
          </div>
        </div>

        <div class="box-footer" id="btn-cetak" style="display: none">
          <div class="col-sm-10 pull-right">
            <div class="pull-right">
              <button type="button" id="cetak" class="btn btn-success" onclick="cetak_nd('<?= $nd['revisi']['rev_id'] ?>')">Nota Dinas Revisi</button>
              <button onclick="window.history.back();" class="btn btn-warning">Kembali</button>
            </div>
          </div>
        </div>
      </form>

    </div>
  </div>
</div>


<script type="text/javascript">
  $(document).ready(function() {
    $('.datetimepicker1').datetimepicker({ format : 'dddd, DD MMMM YYYY', locale : 'id' });
    $('.datetimepicker2').datetimepicker({ format: 'LT' });
    $('.datetimepicker3').datetimepicker({ format : 'MMMM YYYY', locale : 'id' });

    $(".h-tujuan,.h-dari,.h-penandatangan,.1-kl,.1-unit").select2();
    $(".jr-11,.jr-12,.jr-13").select2({ minimumResultsForSearch: 7 });
    $(".3-31,.3-32,.3-33,.3-34").select2({ minimumResultsForSearch: 5 });
  });

  function cetak_nd(rev_id) {
    window.location.href = '<?= site_url('revisi_nd/editor_nd') ?>' +'/'+ rev_id;
  }

  function crud_form1() {
    $.ajax({
      url: '<?= site_url('revisi_nd/crud_form1') ?>', type: 'POST', data: $('#form1').serializeArray(),
      success: function( msg ) {
        alert('Data tersimpan ...');
        $('#btn-awal').hide(); $('#btn-cetak').show();
      }
    });
  }
</script>
