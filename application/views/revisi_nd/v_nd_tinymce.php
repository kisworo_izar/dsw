<script src="<?=base_url('assets/plugins/tinymce/tinymce.min.js'); ?>"></script>
<script type="text/javascript">
   tinymce.init({
      selector: "textarea",
      content_css: "<?=base_url('assets/plugins/tinymce/myStyle.css'); ?>, <?=base_url('assets/bootstrap/css/bootstrap.min.css'); ?>",
      fontsize_formats: '7pt 8pt 9pt 10pt 11pt 12pt 13pt 14pt',
      toolbar: "styleselect",
      style_formats: [
        // {title: 'Line height 20px', selector: 'p div', styles: {lineHeight: '20px'}},
        {title: 'Paragraph', selector: 'p', styles: {'text-indent': '2.5em'}},
        {title: 'Penomoran Level 1', selector: 'ol', styles: {'padding-left':'20px'}},
        {title: 'Penomoran Level 2', selector: 'ol', styles: {'padding-left':'20px','padding-top':'5px'}},
        {title: 'Penomoran Level 3', selector: 'ol', styles: {'padding-left':'20px','padding-top':'5px'}},
        {title: 'Line Spacing Penomoran 5px', selector: 'li', styles: {'padding-top':'5px'}},
     ],
     format: {
      removeformat: [
        {selector: 'b,strong,em,i,font,u,strike', remove : 'all', split : true, expand : false, block_expand: true, deep : true},
        {selector: 'span', attributes : ['style', 'class'], remove : 'empty', split : true, expand : false, deep : true},
        {selector: '*', attributes : ['style', 'class'], split : false, expand : false, deep : true}
      ]
  },
      content_style: ".mce-content-body {font-size:11pt;font-family:Arial,sans-serif;}",
      theme: "modern",
      height: "450",
      width: "210mm",
      plugins: [
         "advlist autolink lists link image charmap print preview hr anchor pagebreak",
         "searchreplace wordcount visualblocks visualchars code fullscreen",
         "insertdatetime nonbreaking save table contextmenu directionality",
         "template paste textcolor textpattern"
      ],
      toolbar1: "insertfile undo redo | styleselect | fontsizeselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | print",
   });
</script>
<style>
   #mceu_2. { height: 240px; }
</style>

<body>
<form method="post">
    <textarea><?= $teks ?></textarea>
</form>
</body>
