
<div class="box-header with-border revHead">
    <h4 class="box-title" style="color:#FFF;padding-left:5px">2. PEMBAHASAN</h4>
</div>
<div class="form-group" style="padding-left:10px;margin-bottom:0px">
    <h5 class="col-sm-12 text-bold" style="color:#3C8DBC;padding-left:20px">A. DHP SEBELUM REVISI</h5>
</div>
<div class="form-group" style="margin-left:15px;margin-bottom:3px">
    <div class="col-sm-10" style="padding:0px">
        <textarea name="f2_latar_blkg" class="form-control" placeholder="Uraian narasi DHP awal" rows="5"><?= $nd['nota']['f2_latar_blkg'] ?></textarea>
    </div>
    <div class="form-group" style="padding-left:5px;margin-bottom:0px">
        <h5 class="col-sm-12 text-bold" style="color:#3C8DBC;padding-left:10px">B. REVISI YANG DIUSULKAN</h5>
    </div>

    <div ng-controller="ctrlrefr">
      <!-- REV #1 -->
         <div class="col-sm-10" style="padding:0px">
             <div class="" style="border-top:solid 1px #9DC6DD;margin-bottom:5px;"></div>
             <input id="checkbox-5" class="checkbox-custom" name="checkbox-5" type="checkbox">
             <label for="checkbox-5" class="checkbox-custom-label fdok" onclick="toggle('rev1')">
                 1) PERUBAHAN ANGGARAN TERMASUK PERUBAHAN RINCIANNYA
             </label>
         </div>

         <div style="display:none" id="rev1">
            <!-- Start jenis revisi 1 -->
            <div  class="col-sm-10" style="padding:5px 0px 5px 40px">
                <select name="f2_jnsrev1_a" class="jr-11 form-control" style="width:100%">
                    <option value="" hidden><em>---  Pilih Jenis Revisi  ---</em></option>;
                    <?php foreach ($nd['jnsrev'] as $row) {
                        if (substr($row['kdjnsrev'],0,5) == date('Y').'A') {
                            if ($nd['nota']['f2_jnsrev1_a'] == $row['nmjnsrev']) $sel = 'selected="selected"'; else $sel = '';
                            echo "<option value='". $row['nmjnsrev'] ."' $sel >". $row['nmjnsrev'] ."</option>";
                        }
                    } ?>
                </select>
            </div>
            <div class="col-sm-10" style="padding:5px 0px 5px 40px">
                <select name="f2_jnsrev1_b" class="jr-11 form-control" style="width:100%">
                    <option value="" hidden><em>---  Pilih Jenis Revisi  ---</em></option>;
                    <?php foreach ($nd['jnsrev'] as $row) {
                        if (substr($row['kdjnsrev'],0,5) == date('Y').'A') {
                            if ($nd['nota']['f2_jnsrev1_b'] == $row['nmjnsrev']) $sel = 'selected="selected"'; else $sel = '';
                            echo "<option value='". $row['nmjnsrev'] ."' $sel >". $row['nmjnsrev'] ."</option>";
                        }
                    } ?>
                </select>
            </div>
            <div class="col-sm-10" style="padding:5px 0px 5px 40px">
                <select name="f2_jnsrev1_c" class="jr-11 form-control" style="width:100%">
                    <option value="" hidden><em>---  Pilih Jenis Revisi  ---</em></option>;
                    <?php foreach ($nd['jnsrev'] as $row) {
                        if (substr($row['kdjnsrev'],0,5) == date('Y').'A') {
                            if ($nd['nota']['f2_jnsrev1_c'] == $row['nmjnsrev']) $sel = 'selected="selected"'; else $sel = '';
                            echo "<option value='". $row['nmjnsrev'] ."' $sel >". $row['nmjnsrev'] ."</option>";
                        }
                    } ?>
                </select>
            </div>
            <!-- End jenis revisi 1 -->
            <div class="col-sm-10" style="padding:5px 0px 15px 40px">
                <label for="">Keterangan : </label>
                <textarea name="f2_jnsrev1_ket" class="form-control" placeholder="Keterangan" rows="5"><?= $nd['nota']['f2_jnsrev1_ket'] ?></textarea>
            </div>
         </div>
         <!-- REV #1 END -->

         <!-- REV #2 -->
         <div class="col-sm-10" style="padding:0px">
            <div class="" style="border-top:solid 1px #9DC6DD;margin-bottom:5px;margin-top:5px"></div>
            <input id="checkbox-6" class="checkbox-custom" name="checkbox-6" type="checkbox">
            <label for="checkbox-6" class="checkbox-custom-label fdok" onclick="toggle('rev2')">
                2) PERGESERAN ANGGARAN TERMASUK PERUBAHAN RINCIANNYA DALAM HAL PAGU TETAP
            </label>
         </div>
         <div style="display:none" id="rev2">
            <!-- Start jenis revisi 2 -->
            <div class="col-sm-10" style="padding:5px 0px 5px 40px">
                <select name="f2_jnsrev2_a" class="jr-11 form-control" style="width:100%">
                    <option value="" hidden><em>---  Pilih Jenis Revisi  ---</em></option>;
                    <?php foreach ($nd['jnsrev'] as $row) {
                        if (substr($row['kdjnsrev'],0,5) == date('Y').'B') {
                            if ($nd['nota']['f2_jnsrev2_a'] == $row['nmjnsrev']) $sel = 'selected="selected"'; else $sel = '';
                            echo "<option value='". $row['nmjnsrev'] ."' $sel >". $row['nmjnsrev'] ."</option>";
                        }
                    } ?>
                </select>
            </div>
            <div class="col-sm-10" style="padding:5px 0px 5px 40px">
                <select name="f2_jnsrev2_b" class="jr-11 form-control" style="width:100%">
                    <option value="" hidden><em>---  Pilih Jenis Revisi  ---</em></option>;
                    <?php foreach ($nd['jnsrev'] as $row) {
                        if (substr($row['kdjnsrev'],0,5) == date('Y').'B') {
                            if ($nd['nota']['f2_jnsrev2_b'] == $row['nmjnsrev']) $sel = 'selected="selected"'; else $sel = '';
                            echo "<option value='". $row['nmjnsrev'] ."' $sel >". $row['nmjnsrev'] ."</option>";
                        }
                    } ?>
                </select>
            </div>
            <div class="col-sm-10" style="padding:5px 0px 5px 40px">
                <select name="f2_jnsrev2_c" class="jr-11 form-control" style="width:100%">
                    <option value="" hidden><em>---  Pilih Jenis Revisi  ---</em></option>;
                    <?php foreach ($nd['jnsrev'] as $row) {
                        if (substr($row['kdjnsrev'],0,5) == date('Y').'B') {
                            if ($nd['nota']['f2_jnsrev2_c'] == $row['nmjnsrev']) $sel = 'selected="selected"'; else $sel = '';
                            echo "<option value='". $row['nmjnsrev'] ."' $sel >". $row['nmjnsrev'] ."</option>";
                        }
                    } ?>
                </select>
            </div>
            <!-- end jenis revisi 2 -->

            <div class="col-sm-10" style="padding:5px 0px 15px 40px">
                <label for="">Keterangan : </label>
                <textarea name="f2_jnsrev2_ket" class="form-control" placeholder="Keterangan" rows="5"><?= $nd['nota']['f2_jnsrev2_ket'] ?></textarea>
            </div>
         </div>
         <!-- REV #2 END -->

         <!-- REV #3 -->
         <div class="col-sm-10" style="padding:0px">
            <div class="" style="border-top:solid 1px #9DC6DD;margin-bottom:5px;margin-top:5px"></div>
            <input id="checkbox-7" class="checkbox-custom" name="checkbox-7" type="checkbox">
            <label for="checkbox-7" class="checkbox-custom-label fdok" onclick="toggle('rev3')">
                3) REVISI ADMINISTRASI
            </label>
         </div>
         <div style="display:none" id="rev3">

            <!-- pilihan untuk jenis revisi 3 -->
            <div class="col-sm-10" style="padding:5px 0px 5px 40px">
                <select name="f2_jnsrev3_a" class="jr-11 form-control" style="width:100%">
                    <option value="" hidden><em>---  Pilih Jenis Revisi  ---</em></option>;
                    <?php foreach ($nd['jnsrev'] as $row) {
                        if (substr($row['kdjnsrev'],0,5) == date('Y').'C') {
                            if ($nd['nota']['f2_jnsrev3_a'] == $row['nmjnsrev']) $sel = 'selected="selected"'; else $sel = '';
                            echo "<option value='". $row['nmjnsrev'] ."' $sel >". $row['nmjnsrev'] ."</option>";
                        }
                    } ?>
                </select>
            </div>
            <div class="col-sm-10" style="padding:5px 0px 5px 40px">
                <select name="f2_jnsrev3_b" class="jr-11 form-control" style="width:100%">
                    <option value="" hidden><em>---  Pilih Jenis Revisi  ---</em></option>;
                    <?php foreach ($nd['jnsrev'] as $row) {
                        if (substr($row['kdjnsrev'],0,5) == date('Y').'C') {
                            if ($nd['nota']['f2_jnsrev3_b'] == $row['nmjnsrev']) $sel = 'selected="selected"'; else $sel = '';
                            echo "<option value='". $row['nmjnsrev'] ."' $sel >". $row['nmjnsrev'] ."</option>";
                        }
                    } ?>
                </select>
            </div>
            <div class="col-sm-10" style="padding:5px 0px 5px 40px">
                <select name="f2_jnsrev3_c" class="jr-11 form-control" style="width:100%">
                    <option value="" hidden><em>---  Pilih Jenis Revisi  ---</em></option>;
                    <?php foreach ($nd['jnsrev'] as $row) {
                        if (substr($row['kdjnsrev'],0,5) == date('Y').'C') {
                            if ($nd['nota']['f2_jnsrev3_c'] == $row['nmjnsrev']) $sel = 'selected="selected"'; else $sel = '';
                            echo "<option value='". $row['nmjnsrev'] ."' $sel >". $row['nmjnsrev'] ."</option>";
                        }
                    } ?>
                </select>
            </div>
            <!-- end jenis revisi 3 -->

            <div class="col-sm-10" style="padding:5px 0px 15px 40px">
                <label for="">Keterangan : </label>
                <textarea name="f2_jnsrev3_ket" class="form-control" placeholder="Keterangan" rows="5"><?= $nd['nota']['f2_jnsrev3_ket'] ?></textarea>
            </div>
         </div>
         <!-- REV #3 END -->

         <!-- REV #4 -->
         <div class="col-sm-10" style="padding:0px">
            <div style="border-top:solid 1px #9DC6DD;margin-bottom:5px;margin-top:5px"></div>
            <input id="checkbox-8" class="checkbox-custom" name="checkbox-8" type="checkbox">
            <label for="checkbox-8" class="checkbox-custom-label fdok" onclick="toggle('rev4')">
                4) USULAN REVISI ANGGARAN YANG MEMUAT SUBSTANSI KEWENANGAN DIREKTORAT PA DITJEN PERBENDAHARAAN
            </label>
         </div>
         <div style="display:none" id="rev4">

            <!-- pilihan untuk jenis revisi 4 -->
            <div class="col-sm-10" style="padding:5px 0px 5px 40px">
                <select name="f2_jnsrev4_a" class="jr-11 form-control" style="width:100%">
                    <option value="" hidden><em>---  Pilih Jenis Revisi  ---</em></option>;
                    <?php foreach ($nd['jnsrev'] as $row) {
                        if (substr($row['kdjnsrev'],0,5) == date('Y').'D') {
                            if ($nd['nota']['f2_jnsrev4_a'] == $row['nmjnsrev']) $sel = 'selected="selected"'; else $sel = '';
                            echo "<option value='". $row['nmjnsrev'] ."' $sel >". $row['nmjnsrev'] ."</option>";
                        }
                    } ?>
                </select>
            </div>
            <div class="col-sm-10" style="padding:5px 0px 5px 40px">
                <select name="f2_jnsrev4_b" class="jr-11 form-control" style="width:100%">
                    <option value="" hidden><em>---  Pilih Jenis Revisi  ---</em></option>;
                    <?php foreach ($nd['jnsrev'] as $row) {
                        if (substr($row['kdjnsrev'],0,5) == date('Y').'D') {
                            if ($nd['nota']['f2_jnsrev4_b'] == $row['nmjnsrev']) $sel = 'selected="selected"'; else $sel = '';
                            echo "<option value='". $row['nmjnsrev'] ."' $sel >". $row['nmjnsrev'] ."</option>";
                        }
                    } ?>
                </select>
            </div>
            <div class="col-sm-10" style="padding:5px 0px 5px 40px">
                <select name="f2_jnsrev4_c" class="jr-11 form-control" style="width:100%">
                    <option value="" hidden><em>---  Pilih Jenis Revisi  ---</em></option>;
                    <?php foreach ($nd['jnsrev'] as $row) {
                        if (substr($row['kdjnsrev'],0,5) == date('Y').'D') {
                            if ($nd['nota']['f2_jnsrev4_c'] == $row['nmjnsrev']) $sel = 'selected="selected"'; else $sel = '';
                            echo "<option value='". $row['nmjnsrev'] ."' $sel >". $row['nmjnsrev'] ."</option>";
                        }
                    } ?>
                </select>
            </div>
            <!-- end jenis revisi 4 -->

            <div class="col-sm-10" style="padding:5px 0px 5px 40px">
                <label for="">Keterangan : </label>
                <textarea name="f2_jnsrev4_ket" class="form-control" placeholder="Keterangan" rows="5"><?= $nd['nota']['f2_jnsrev4_ket'] ?></textarea>
            </div>
         </div>
         <!-- REV #4 END -->

         <!-- REV #5 -->
         <div class="col-sm-10" style="padding:0px">
            <div style="border-top:solid 1px #9DC6DD;margin-bottom:5px;margin-top:5px"></div>
            <input id="checkbox-9" class="checkbox-custom" name="checkbox-9" type="checkbox">
            <label for="checkbox-9" class="checkbox-custom-label fdok" onclick="toggle('rev5')">
                5) USULAN REVISI ANGGARAN YANG MEMUAT SUBSTANSI KEWENANGAN KANTOR WILAYAH DITJEN PERBENDAHARAAN
            </label>
         </div>
         <div style="display:none" id="rev5">

            <!-- pilihan untuk jenis revisi 4 -->
            <div class="col-sm-10" style="padding:5px 0px 5px 40px">
                <select name="f2_jnsrev5_a" class="jr-11 form-control" style="width:100%">
                    <option value="" hidden><em>---  Pilih Jenis Revisi  ---</em></option>;
                    <?php foreach ($nd['jnsrev'] as $row) {
                        if (substr($row['kdjnsrev'],0,5) == date('Y').'E') {
                            if ($nd['nota']['f2_jnsrev5_a'] == $row['nmjnsrev']) $sel = 'selected="selected"'; else $sel = '';
                            echo "<option value='". $row['nmjnsrev'] ."' $sel >". $row['nmjnsrev'] ."</option>";
                        }
                    } ?>
                </select>
            </div>
            <div class="col-sm-10" style="padding:5px 0px 5px 40px">
                <select name="f2_jnsrev5_b" class="jr-11 form-control" style="width:100%">
                    <option value="" hidden><em>---  Pilih Jenis Revisi  ---</em></option>;
                    <?php foreach ($nd['jnsrev'] as $row) {
                        if (substr($row['kdjnsrev'],0,5) == date('Y').'E') {
                            if ($nd['nota']['f2_jnsrev5_b'] == $row['nmjnsrev']) $sel = 'selected="selected"'; else $sel = '';
                            echo "<option value='". $row['nmjnsrev'] ."' $sel >". $row['nmjnsrev'] ."</option>";
                        }
                    } ?>
                </select>
            </div>
            <div class="col-sm-10" style="padding:5px 0px 5px 40px">
                <select name="f2_jnsrev5_c" class="jr-11 form-control" style="width:100%">
                    <option value="" hidden><em>---  Pilih Jenis Revisi  ---</em></option>;
                    <?php foreach ($nd['jnsrev'] as $row) {
                        if (substr($row['kdjnsrev'],0,5) == date('Y').'E') {
                            if ($nd['nota']['f2_jnsrev5_c'] == $row['nmjnsrev']) $sel = 'selected="selected"'; else $sel = '';
                            echo "<option value='". $row['nmjnsrev'] ."' $sel >". $row['nmjnsrev'] ."</option>";
                        }
                    } ?>
                </select>
            </div>
            <!-- end jenis revisi 4 -->

            <div class="col-sm-10" style="padding:5px 0px 5px 40px">
                <label for="">Keterangan : </label>
                <textarea name="f2_jnsrev5_ket" class="form-control" placeholder="Keterangan" rows="5"><?= $nd['nota']['f2_jnsrev5_ket'] ?></textarea>
            </div>
         </div>
         <!-- REV #5 END -->

         <div class="col-sm-10" style="padding:0px">
            <div class="" style="border-top:solid 1px #9DC6DD;margin-bottom:5px;margin-top:5px"></div>
         </div>
    </div>
</div>


<div class="form-group jarak" style="padding-left:10px">
    <h5 class="col-sm-12 text-bold" style="color:#3C8DBC;padding-left:20px">C. DOKUMEN PENDUKUNG YANG DISAMPAIKAN</h5>
    <div class="" style="padding:10px 0px 5px 5px;margin-bottom:0px">
        <div class="col-sm-10"><label>Dokumen pendukung yang telah disampaikan meliputi:</label></div>
    </div>
    <div class="col-sm-10" style="padding:10px 10px 0px 35px">
        <input id="chk-F01" class="checkbox-custom" type="checkbox" disabled <?php if ($nd['revisi']['doc_usulan_revisi'] != null) echo 'checked'; ?> />
        <label for="chk-F01" class="checkbox-custom-label fdok">
            Surat usulan revisi yang ditandatangani pejabat eselon I
        </label>
    </div>
    <div class="col-sm-10" style="padding:10px 10px 0px 35px">
        <input id="chk-F02" class="checkbox-custom" type="checkbox" disabled <?php if ($nd['revisi']['doc_matrix'] != null) echo 'checked'; ?> />
        <label for="chk-F02" class="checkbox-custom-label fdok">
            Matriks perubahan (semula menjadi)
        </label>
    </div>
    <!-- <div class="col-sm-10" style="padding:10px 10px 0px 35px">
        <input id="chk-F04" class="checkbox-custom" type="checkbox" disabled <?php if ($nd['revisi']['doc_rka'] != null) echo 'checked'; ?> />
        <label for="chk-F04" class="checkbox-custom-label fdok">
            Rencana Kerja dan Anggaran (RKA) Satker
        </label>
    </div> -->
    <div class="col-sm-10" style="padding:10px 10px 0px 35px">
        <input id="chk-F03" class="checkbox-custom" type="checkbox" disabled <?php if ($nd['revisi']['doc_adk'] != null) echo 'checked'; ?> />
        <label for="chk-F03" class="checkbox-custom-label fdok">
            Arsip Data Komputer (ADK) RKA-K/L DIPA revisi Satker
        </label>
    </div>
    <!-- <div class="col-sm-10" style="padding:10px 10px 0px 35px">
        <input id="chk-F06" class="checkbox-custom" type="checkbox" disabled <?php if ($nd['revisi']['doc_dukung_sepakat'] != null) echo 'checked'; ?> />
        <label for="chk-F06" class="checkbox-custom-label fdok">
            Dokumen pendukung terkait sesuai hasil kesepakatan antara K/L dengan DJA dalam pembahasan usulan revisi anggaran
        </label>
    </div>
    <div class="col-sm-10" style="padding:10px 10px 5px 35px">
        <input id="chk-F07" class="checkbox-custom" type="checkbox" disabled <?php if ($nd['revisi']['doc_dukung_hal4'] != null) echo 'checked'; ?> />
        <label for="chk-F07" class="checkbox-custom-label fdok">
            Dokumen pendukung terkait perubahan/penghapusan catatan dalam halaman IV DIPA
        </label>
    </div> -->
    <div class="col-sm-10" style="padding:10px 10px 5px 35px">
        <input id="chk-F07" class="checkbox-custom" type="checkbox" disabled <?php if ($nd['revisi']['doc_dukung_lainnya'] != null) echo 'checked'; ?> />
        <label for="chk-F07" class="checkbox-custom-label fdok">
            Dokumen pendukung usul revisi
        </label>
    </div>
    <div class="col-sm-10" style="padding:10px 10px 0px 5px">
        <label class="col-sm-7" style="margin-top:7px">Seluruh dokumen telah diterima lengkap pada tanggal :</label>
        <div class="col-sm-4" style="padding-right:0px">
            <div class="input-group date datetimepicker1">
                <input id="tglterimadok" type="text" class="form-control" disabled value="<?php echo $this->fc->idtgl($nd['revisi']['t6_selesai_tgl'],'hari') ?>" style="height:40px">
                <span class="input-group-addon">
                    <i class="fa fa-calendar text-primary"></i>
                </span>
            </div>
        </div>
    </div>
    <div class="col-sm-10" style="padding:10px 10px 0px 5px">
        <label class="col-sm-7" style="margin-top:7px">Melalui tiket nomor :</label>
        <div class="col-sm-4" style="padding-right:0px">
            <div class="col-sm-12" style="padding:0px">
                <input class="form-control" disabled value="<?= $nd['revisi']['rev_id'] ?>" />
            </div>
        </div>
    </div>
</div>



<div class="form-group jarak" style="padding-left:10px;margin-bottom:0px">
    <h5 class="col-sm-12 text-bold" style="color:#3C8DBC;padding-left:20px">D. PENELAAHAN USULAN REVISI ANGGARAN</h5>
</div>

<div class="form-group jarak" style="padding-left:15px;margin-bottom:0px">
   <div class="col-sm-10" style="padding:0px 10px 0px 0px;">
      <label class="col-sm-7" style="margin-top:7px">
           Penelaahan usulan revisi anggaran telah dilaksanakan pada tanggal :
      </label>
      <div class="col-sm-4" style="padding-right:0px">
           <div class="input-group date datetimepicker1">
               <input id="tgltelaah" type="text" class="form-control" disabled value="<?php echo $this->fc->idtgl($nd['revisi']['t3_penelaahan_tgl'],'hari')?>" style="height:40px">
               <span class="input-group-addon"><i class="fa fa-calendar text-primary"></i></span>
           </div>
      </div>
   </div>
</div>

<div class="form-group" style="margin-left:15px;margin-bottom:3px">
    <div class="col-sm-10" style="padding:0px">
        <label>dengan kesepakatan sebagai berikut : </label>
        <textarea name="f2_telaah_catatan" class="form-control" placeholder="Uraian kesepakatan penelaahan" rows="5"><?= $nd['nota']['f2_telaah_catatan'] ?></textarea>
    </div>
</div>

<br>
