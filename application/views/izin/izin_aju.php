<!-- Bootstrap time Picker -->
<link rel="stylesheet" href="<?=base_url();?>assets/plugins/timepicker/bootstrap-timepicker.min.css">
<!-- iCheck for checkboxes and radio inputs -->
<link rel="stylesheet" href="<?=base_url();?>assets/plugins/iCheck/all.css">
<style type="text/css">
  #ajuForm .form-control-feedback {
    top: 0;
    right: -15px;
  }
</style>

<!-- Modal -->
<div class="modal fade" id="ajuModal" tabindex="-1" role="dialog" aria-labelledby="input" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
    <div class="modal-content">
      <!-- header -->
      <div class="modal-header">
        <h4 class="modal-title">Pengajuan Izin Pemberitahuan </h4>
      </div>
      <form id="ajuForm" action="<?php echo site_url('izin?q=xi9Ug')?>" method="POST" class="" enctype="multipart/form-data">
        <div class="modal-body">
          <div class="container-fluid">
            <!-- Form Pengajuan -->
            <div class="row" hidden id="izin_sebelumnya">
              <div class="col-md-12">
                <div class="form-group">
                  <label class="col-md-3 control-label text-right">ID Izin Sebelumnya</label>
                  <div class="col-md-9" style="margin-bottom:5px">
                    <input class="form-control" name="prevIzin_id" id="prevIzin_id" readonly>
                  </div>
                </div>
              </div>
            </div>
            <div class="row" hidden id="izin_id">
              <div class="col-md-12">
                <div class="form-group">
                  <label class="col-md-3 control-label text-right">ID</label>
                  <div class="col-md-9" style="margin-bottom:5px">
                    <input class="form-control" name="id" id="id" readonly>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label class="col-md-3 control-label text-right">Izin</label>
                  <div class="col-md-9" style="margin-bottom:5px">
                    <select class="select_status org form-control" data-placeholder="Pilih Izin" name="sts" data-bv-remote-name="isMatchStatusTanggal" id="sts" style="width: 100%" size="5">
                      <option></option>
                      <?php foreach ($hdr['dat'] as $row) {
                        echo '<option value="'. $row['kode'] .'">'. $row['uraian']." ( ".$row['pelanggaran']." )".'</option>';
                      } ?>
                    </select>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group col-md-6">
                  <label class="col-md-6 control-label text-right">Tanggal Mulai</label>
                  <div class="col-md-6" style="margin-bottom:5px">
                    <div class="input-group date date_tgl3">
                      <input type="text"  name="tglmulai" id="tglmulai" class="form-control tglmulai" placeholder="Tanggal Mulai" autocomplete="off" />
                      <span class="input-group-addon"><i class="fa fa-calendar text-primary"></i></span>
                    </div>
                  </div>
                </div>
                <div class="form-group col-md-6">
                  <label class="col-md-6 control-label text-right">Tanggal Selesai</label>
                  <div class="col-md-6" style="margin-bottom:5px">
                    <div class="input-group date date_tgl4">
                      <input type="text"  name="tglselesai" id="tglselesai" class="form-control tglselesai" placeholder="Tanggal Selesai" autocomplete="off" />
                      <span class="input-group-addon"><i class="fa fa-calendar text-primary"></i></span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="row" id="jam" hidden>
              <div class="col-md-12">
                <div class="col-md-3">
                </div>
                <div class="col-xs-4">
                  <div class="form-group">
                    <div class="col-md-12">
                      <div class="minimal">
                        <label class="control-label">
                          <input type="checkbox" class="" id="datang" value="datang" name="datangpulang[]">
                          TMH Datang
                        </label>
                      </div>
                      <div class="minimal">
                        <label class="control-label">
                          <input type="checkbox" class="" id="pulang" value="pulang" name="datangpulang[]">
                          TMH Pulang
                        </label>
                      </div>
                      <div class="help-block with-errors"></div>
                    </div>
                  </div>
                </div>
                <div class="col-xs-4">
                  <div class="form-group ">
                    <div class="col-xs-12">
                      <div class="input-group" style="margin-bottom:5px">
                        <input type="text" class="form-control timepicker" id="datang_jam" name="datang_jam" disabled>
                        <span class="input-group-addon">
                          <i class="fa fa-clock-o text-primary"></i>
                        </span>
                      </div>
                    </div>
                  </div>
                  <div class="form-group ">
                    <div class="col-xs-12">
                      <div class="input-group" style="margin-bottom:5px">
                        <input type="text" class="form-control timepicker" id="pulang_jam" name="pulang_jam" disabled>
                        <span class="input-group-addon">
                          <i class="fa fa-clock-o text-primary"></i>
                        </span>
                      </div>
                    </div>
                  </div>
                </div>

              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label class="col-md-3 control-label text-right">Keterangan</label>
                  <div class="col-md-9" style="margin-bottom:5px">
                    <textarea class="form-control"  rows="2" name="ket" id="ket" width="100%" autocomplete="off"></textarea>
                  </div>
                </div>
              </div>
            </div>
            <div class="row" id="dokumen" hidden>
              <div class="col-md-12">
                <div class="form-group">
                  <label class="col-md-3 control-label text-right">Dokumen (jpg/jpeg/png)</label>
                  <div class="col-md-9" style="margin-bottom:5px">
                    <input  name="dok" id="dok" autocomplete="off" type="file" class="form-control" style="height:34px" data-show-preview="false" />
                    <a href="" id="dok_name"></a>
                    <input type="hidden" name="nmfile" id="nmfile">
                    <div id="kv-success-2" class="alert alert-success" style="margin-top:10px;display:none">File Terupload !</div>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label class="col-md-3 control-label"> </label>
                  <h4 class="col-md-9 control-label"> Persetujuan (Atasan / PLH) </h4>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label class="col-md-3 control-label text-right">NIP</label>
                  <div class="col-md-9" style="margin-bottom:5px;">

                    <select class="select_tandatangan org form-control" placeholder="Pilih Penandatangan" name="penandatangan" id="penandatangan" style="width: 100%" onchange="referTtd(this.value)">
                      <option></option>
                      <?php foreach ($hdr['peg'] as $row) {
                        echo '<option value="'. $row['nip18'] .'">'. $row['nip18'].'  '.$row['nama'].'</option>';
                      } ?>
                    </select>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label class="col-md-3 control-label text-right">Nama</label>
                  <div class="col-md-9" style="margin-bottom:5px">
                    <input class="form-control" name="nama" id="nama" autocomplete="off" type="text" style="height:34px" readonly>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label class="col-md-3 control-label text-right">Jabatan</label>
                  <div class="col-md-9" style="margin-bottom:5px">
                    <input class="form-control" name="jab" id="jab" autocomplete="off" type="text" style="height:34px" >
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">TUTUP</button>
          <button type="submit" class="btn btn-primary btn-flat">SIMPAN</button>
        </div>
      </form>
      <!-- Form Pengajuan End -->
    </div>
  </div>
</div>
<!-- Modal End -->

<!-- ajuGagalModal -->
<div class="modal modal-danger fade" id="ajuGagalModal">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <p></p>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<!-- bootstrap time picker -->
<script src="<?=base_url();?>assets/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="<?=base_url();?>assets/plugins/iCheck/icheck.min.js"></script>

<script type="text/javascript">
  $('#ajuModal').on('show.bs.modal', function (event) {
    if (event.target.id == 'ajuModal') {
      var izin = $(event.relatedTarget).data('izin');
      var type = $(event.relatedTarget).data('type');

      $('#izin_sebelumnya').hide();
      $('#prevIzin_id').val('');
      $('#izin_id').hide();
      $('#id').val('');
      $('#tglmulai').datepicker('setStartDate', '-<?php echo $krj ?>d');
      $('#tglselesai').datepicker('setEndDate', '+1y');
      $('#ajuForm').bootstrapValidator('enableFieldValidators', 'dok', true);

      if (type == 'aju') {
        return true;
      }

      $('#ajuForm').bootstrapValidator('resetForm', true);
      validateForm();
      
      if (type == 'ulang') {
        $('#izin_sebelumnya').show();
        $('#prevIzin_id').val(izin.id);
        $('#sts').attr('readonly', true);
        $('#tglmulai').datepicker('setStartDate', formatDate(izin.tanggalmulai));
      }

      if (type == 'edit') {
        $('#izin_id').show();
        $('#id').val(izin.id);
      }

      $('#sts').select2('val', izin.status, true);
      $('#sts').trigger({
        type: 'select2:select',
        params: {
          data: {
            id: izin.status
          }
        }
      });

      if (type == 'ulang') {
        $('#tglmulai').datepicker('setEndDate', formatDate(izin.tanggalselesai));
        $('#tglselesai').datepicker('setEndDate', formatDate(izin.tanggalselesai));
      }

      $('#tglmulai').datepicker('setDate', formatDate(izin.tanggalmulai));
      $('#tglselesai').datepicker('setDate', formatDate(izin.tanggalselesai));
      $('#ket').val(izin.keterangan);

      var jamdatang, jampulang;
      jamdatang = izin.tanggalmulai.split(' ')[1];
      jampulang = izin.tanggalselesai.split(' ')[1];
      if (jamdatang != '00:00:00') {
        $('#datang').iCheck('check');
        $('#datang_jam').timepicker('setTime', jamdatang)
      } else {
        $('#datang').removeAttr('checked').iCheck('update');
        $('#datang').trigger({
          type: 'ifUnchecked',
        });
      }
      if (jampulang != '00:00:00') {
        $('#pulang').iCheck('check');
        $('#pulang_jam').timepicker('setTime', jampulang)
      } else {
        $('#pulang').removeAttr('checked').iCheck('update');
        $('#pulang').trigger({
          type: 'ifUnchecked',
        });
      }

      var url = '<?= base_url() ?>files/upload_milea/'+izin.bawahan_nip+'/'+formatDate(izin.tanggalsurat)+'_'+izin.dokumen;
      $('#dok_name').attr('href', '<?= base_url() ?>files/upload_milea/'+izin.bawahan_nip+'/'+formatDate(izin.tanggalsurat)+'_'+izin.dokumen);
      $('#dok_name').text(izin.dokumen);
      $('#nmfile').val(izin.dokumen);

      $('#ajuForm').bootstrapValidator('enableFieldValidators', 'dok', false);

      validateForm();
    } 
  });

  $(function() {
    $('#ajuForm').bootstrapValidator({
      excluded: [':disabled', ':hidden'],
      feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
        sts: {
          validators: {
            notEmpty: {
            },
            remote: {
              type: 'POST',
              url: "<?= site_url('izin?q=hDPMa') ?>",
              data: function(validator){
                return {
                  status: validator.getFieldElements('sts').val(),
                  tanggalmulai: validator.getFieldElements('tglmulai').val(),
                  tanggalselesai: validator.getFieldElements("tglselesai").val()
                };
              }
            }
          }
        },
        tglmulai: {
          validators: {
            notEmpty: {
            },
            date: {
              format: 'DD-MM-YYYY'
            },
            remote: {
              type: 'POST',
              url: "<?= site_url('izin?q=hDPMa') ?>",
              data: function(validator){
                return {
                  status: validator.getFieldElements('sts').val(),
                  tanggalmulai: validator.getFieldElements('tglmulai').val(),
                  tanggalselesai: validator.getFieldElements("tglselesai").val()
                };
              }
            }
          }
        },
        tglselesai: {
          validators: {
            notEmpty: {
            },
            date: {
              format: 'DD-MM-YYYY'
            },
            remote: {
              type: 'POST',
              url: "<?= site_url('izin?q=hDPMa') ?>",
              data: function(validator){
                return {
                  status: validator.getFieldElements('sts').val(),
                  tanggalmulai: validator.getFieldElements('tglmulai').val(),
                  tanggalselesai: validator.getFieldElements("tglselesai").val()
                };
              }
            }
          }
        },
        ket: {
          validators: {
            notEmpty: {
            }
          }
        },
        nmfile: {
          validators: {
            notEmpty: {
            }
          }
        },
        dok: {
          validators: {
            notEmpty: {
            },
            file: {
              extension: 'jpeg,jpg,png',
              type: 'image/jpeg,image/png',
              maxSize: 2097152
            }
          }
        },
        penandatangan: {
          validators: {
            notEmpty: {
            }
          }
        },
        jab: {
          validators: {
            notEmpty: {
            }
          }
        },
        'datangpulang[]': {
          validators: {
            notEmpty: {
            }
          }
        },
        datang_jam: {
          validators: {
            notEmpty: {
            }
          }
        },
        pulang_jam: {
          validators: {
            notEmpty: {
            }
          }
        },
      }
    })      
    .on('success.form.bv', function(e) {
      e.preventDefault();
      var $form = $(e.target);
      var bv = $form.data('bootstrapValidator');

      if ($('#sts').val() == 'TMH') {
        $('#dok').fileinput('upload');
      } else {
        $.ajax({
          type: "POST",
          url: '<?= base_url('izin/aju');?>',
          data: $('#ajuForm').serialize(),
          dataType: 'json',
          success: function(result) {
            $('#ajuModal').modal('toggle');
            $('[data-status='+option+']').click();
            if (!result['valid']) {
              swal({
                text: result['message'],
                icon: 'error'
              })
            } else {
              swal({
                text: 'Pengajuan Izin Berhasil',
                icon: 'success'
              })
            }
          },
          error: function(result) {
            swal({
              text: 'ERROR! Pengajuan Gagal',
              icon: 'error'
            })
          }
        });
      }
    })
    .find('input[name="datangpulang[]"]').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass   : 'iradio_minimal-blue'
    }).on('ifChanged', function(e) {
      var field = $(this).attr('name');
      $('#ajuForm').bootstrapValidator('revalidateField', field);
    });

    $('.timepicker').timepicker({
      minuteStep: 1,
      showMeridian: false,
    }).on('changeTime.timepicker', function(e) {    
      /* Validasi Jam >= 06:00 */ 
      if(e.time.hours*60 + e.time.minutes < 360)
        $('.timepicker').timepicker('setTime', '06:00');
    });
    $('[name="datang_jam"]').timepicker('setTime', '07:30');
    $('[name="pulang_jam"]').timepicker('setTime', '17:00');
  });
</script>

<script type="text/javascript">
  $(document).ready(function() {
    $(".org").select2({
      minimumResultsForSearch:5,
    });

    $('#penandatangan').select2('val', JSON.parse('<?php echo json_encode(empty($atasan) ? "" : $atasan->nip) ?>'));

    $('#datang').on('ifChecked', function(event){
      $('#datang_jam').attr('disabled', false);
      $('#ajuForm').bootstrapValidator('revalidateField', 'datang_jam');
    });
    $('#datang').on('ifUnchecked', function(event){
      $('#datang_jam').attr('disabled', true);
      $('#ajuForm').bootstrapValidator('revalidateField', 'datang_jam');
    });

    $('#pulang').on('ifChecked', function(event){
      $('#pulang_jam').attr('disabled', false);
      $('#ajuForm').bootstrapValidator('revalidateField', 'pulang_jam');
    });
    $('#pulang').on('ifUnchecked', function(event){
      $('#pulang_jam').attr('disabled', true);
      $('#ajuForm').bootstrapValidator('revalidateField', 'pulang_jam');
    });
  });


</script>

<script type="text/javascript">
  $('.tglmulai').datepicker({
    startDate: "-<?php echo $krj ?>d",
    daysOfWeekDisabled: [0,6],
    format: "dd-mm-yyyy",
    language: "id",
    orientation: "bottom auto",
    autoclose: true,
    todayHighlight: true,
  })
  .on('changeDate', function(ev) {
    var endDate = $('#tglselesai').datepicker('getDate');
    $('#tglselesai').datepicker('setStartDate', formatDate(ev.date));

    if (endDate == null || ev.date.valueOf() > endDate.valueOf() || $('#sts').val() == 'TMH' && !('<?= $isTUPim ?>')) {
      $('#tglselesai').datepicker('setDate', ev.date);
    }

    $('#ajuForm').bootstrapValidator('revalidateField', 'tglmulai');
    $('#ajuForm').bootstrapValidator('revalidateField', 'tglselesai');
    $('#ajuForm').bootstrapValidator('revalidateField', 'sts');
  });


  $('.tglsurat').datepicker({
    format: "dd-mm-yyyy",
    language: "id",
    orientation: "bottom auto",
    autoclose: true,
    todayHighlight: true
  })
  .on('changeDate', function() {
  });

  $('.tglselesai').datepicker({
    startDate: "-<?php echo $krj ?>d",
    daysOfWeekDisabled: [0,6],
    format: "dd-mm-yyyy",
    language: "id",
    orientation: "bottom auto",
    autoclose: true,
    todayHighlight: true,
  })
  .on('changeDate', function() {
    $('#ajuForm').bootstrapValidator('revalidateField', 'tglmulai');
    $('#ajuForm').bootstrapValidator('revalidateField', 'tglselesai');
    $('#ajuForm').bootstrapValidator('revalidateField', 'sts');
  });

  $('#sts').on('select2:select', function (e) {
    if (e.params.data.id == "TMH") {
      $('#dokumen').show();
      $('#tglmulai').datepicker('setEndDate', '0d');
      $('#tglselesai').datepicker('setEndDate', '0d');
      $('#tglmulai').datepicker('setStartDate', '-2m');
      if (!('<?= $isTUPim ?>')) {
        var today = new Date();
        var tglmulai = $('#tglmulai').datepicker('getDate');
        if (tglmulai > today) {
          $('#tglmulai').datepicker('setDate', 'today');
        }
        $('#tglselesai').attr("readonly", true);
      }
      $('#jam').show()
    } else {
      $('#dokumen').hide();
      $('#tglmulai').datepicker('setEndDate', '+1y');
      $('#tglselesai').attr("readonly", false);
      if ($('#tglmulai').attr("readonly")) {
        $('#tglselesai').attr("readonly", true);
      }
      $('#jam').hide()
    }
    $('#ajuForm').bootstrapValidator('revalidateField', 'nmfile');
    $('#ajuForm').bootstrapValidator('revalidateField', 'dok');

    if (e.params.data.id == "IP") {
      $('#penandatangan').select2('val', JSON.parse('<?php echo json_encode(empty($atasan2) ? "" : $atasan2->nip) ?>'));
    } else {
      $('#penandatangan').select2('val', JSON.parse('<?php echo json_encode(empty($atasan) ? "" : $atasan->nip) ?>'));
    }
    $('#penandatangan').select2().trigger('change');
    $('#ajuForm').bootstrapValidator('revalidateField', 'jab');

    $('#ajuForm').bootstrapValidator('revalidateField', 'tglmulai');
    $('#ajuForm').bootstrapValidator('revalidateField', 'tglselesai');
  });

  $('#penandatangan').on('select2:select', function (e) {
    $('#ajuForm').bootstrapValidator('revalidateField', 'jab');
  });



// Return string date with format dd-mm-yyyy
function formatDate(date){
  var d = new Date(date),
  month = '' + (d.getMonth() + 1),
  day = '' + d.getDate(),
  year = d.getFullYear();

  if (month.length < 2) month = '0' + month;
  if (day.length < 2) day = '0' + day;

  return [day, month, year].join('-');
}

function referTtd( nil ) {
  if (nil == "") {
    return true;
  }
  
  var dat = <?= json_encode($hdr['peg']) ?>;
  var str = "Kepala ";
  var dir = "Direktur ";

  $("#nama").val( dat[nil].nama.toUpperCase() );
  if(dat[nil].jabatan.indexOf("Kepala Subdirektorat") >= 0){
    $("#jab").val( str.concat(dat[nil].esl3).toUpperCase() );
  } else if(dat[nil].jabatan.indexOf("Kepala Bagian") >= 0) {
    $("#jab").val( str.concat(dat[nil].esl3).toUpperCase() );
  } else if(dat[nil].jabatan.indexOf("Kepala Seksi") >= 0){
    $("#jab").val( str.concat(dat[nil].esl4).toUpperCase() );
  } else if(dat[nil].jabatan.indexOf("Kepala Subbagian") >=0){
    $("#jab").val( str.concat(dat[nil].esl4).toUpperCase() );
  } else if(dat[nil].jabatan.indexOf("Direktur") >=0){
    $("#jab").val( dir.concat(dat[nil].esl2).toUpperCase() );
  } else if(dat[nil].jabatan.indexOf("Sekretaris Direktorat Jenderal") >=0){
    $("#jab").val( dat[nil].jabatan.toUpperCase());
  } else {
    $("#jab").val( dat[nil].jabatan.toUpperCase() );
  }
}


</script>

<script type="text/javascript">
  $('#dok').fileinput({
    uploadExtraData:  function(previewId, index) {
      var data = { nip : $('#nip').val(), tgl : $('#tglsurat').val(), name : 'dok' };
      return data;
    },
    uploadUrl: "<?php echo site_url('izin?q=X5bIe') ?>",
    autoReplace: true,
    showUpload: false,
    showRemove: false,
    overwriteInitial: true,
    maxFileCount: 1,
    showPreview: true,
  });

  $('#dok').on('filebatchuploadsuccess', function(event, data, extra) {
    var extra = data.extra, response = data.response;
    var out = '';
    $.each(data.files, function(key, file) {
      var fname = file.name;
      out = out + '<li>' + 'Uploaded file # ' + (key + 1) + ' - '  +  fname + ' successfully.' + '</li>';
    });
    $('#kv-success-2 ul').append(out);
    $('#kv-success-2').fadeIn('slow');
    $('#nmfile').val(response.file);

    /* Submit AjuForm */
    $.ajax({
      type: "POST",
      url: '<?= base_url('izin?q=xi9Ug');?>',
      data: $('#ajuForm').serialize(),
      dataType: 'json',
      success: function(result) {
        $('#ajuModal').modal('toggle');
        $('[data-status='+option+']').click();
        if (!result['valid']) {
          swal({
            text: result['message'],
            icon: 'error'
          })
        } else {
          swal({
            text: 'Pengajuan Izin Berhasil',
            icon: 'success'
          })
        }
      },
      error: function(result) {
        swal({
          text: 'ERROR! Pengajuan Gagal',
          icon: 'error'
        })
      }
    });
  });

  $('#dok').on('filebatchuploaderror', function(event, data) {
    var response = data.response;
    if ($('#nmfile').val()) {
      $.ajax({
        type: "POST",
        url: '<?= base_url('izin?q=xi9Ug');?>',
        data: $('#ajuForm').serialize(),
        dataType: 'json',
        success: function(result) {
          $('#ajuModal').modal('toggle');
          $('[data-status='+option+']').click();
          if (!result['valid']) {
            swal({
              text: result['message'],
              icon: 'error'
            })
          } else {
            swal({
              text: 'Pengajuan Izin Berhasil',
              icon: 'success'
            })
          }
        },
        error: function(result) {
          swal({
            text: 'ERROR! Pengajuan Gagal',
            icon: 'error'
          })
        }
      });
      return true;
    }
    swal({
      text: 'ERROR! Upload File Gagal',
      icon: 'error'
    })
  });

  $('#dok').on('fileclear', function(event, data) {
    $('#ajuForm').bootstrapValidator('revalidateField', 'dok');
  });

  $('#dok').on('filebrowse', function(event) {
    $('#dok').fileinput('clear');
  });

  function postAjuForm(){
    $.ajax({
      type: "POST",
      url: '<?= base_url('izin?q=xi9Ug');?>',
      data: $('#ajuForm').serialize(),
      dataType: 'json',
      success: function(result) {
        $('#ajuModal').modal('toggle');
        $('[data-status='+option+']').click();
        if (!result['valid']) {
          swal({
            text: result['message'],
            icon: 'error'
          })
        } else {
          swal({
            text: 'Pengajuan Izin Berhasil',
            icon: 'success'
          })
        }
      },
      error: function(result) {
        swal({
          text: 'ERROR! Pengajuan Gagal',
          icon: 'error'
        })
      }
    });
  }

  function validateForm(){
    $('#ajuForm').bootstrapValidator('revalidateField', 'sts');
    $('#ajuForm').bootstrapValidator('revalidateField', 'tglmulai');
    $('#ajuForm').bootstrapValidator('revalidateField', 'tglselesai');
    $('#ajuForm').bootstrapValidator('revalidateField', 'ket');
    $('#ajuForm').bootstrapValidator('revalidateField', 'nmfile');
    $('#ajuForm').bootstrapValidator('revalidateField', 'dok');
    $('#ajuForm').bootstrapValidator('revalidateField', 'penandatangan');
    $('#ajuForm').bootstrapValidator('revalidateField', 'jab');
    $('#ajuForm').bootstrapValidator('revalidateField', 'datangpulang[]');
    $('#ajuForm').bootstrapValidator('revalidateField', 'datang_jam');
    $('#ajuForm').bootstrapValidator('revalidateField', 'pulang_jam');
  }
</script>