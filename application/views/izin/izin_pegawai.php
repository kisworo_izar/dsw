
<link href="<?= base_url('assets/plugins/select2/select2.css');?>" rel="stylesheet" type="text/css" />
<link href="<?=base_url('assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css');?>" rel="stylesheet" type="text/css" />
<link href="<?=base_url(); ?>assets/plugins/uploadfile/fileinput.css" rel="stylesheet">
<link href="<?= base_url('assets/dist/css/renja.css') ?>" type="text/css" rel="stylesheet" />

<script src='<?= base_url('assets/plugins/select2/select2.full.min.js');?>'></script>
<script src="<?=base_url('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js');?>" type="text/javascript"></script>
<script src="<?=base_url();?>assets/plugins/uploadfile/fileinput.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<?=base_url();?>assets/plugins/sweetalert/sweetalert.min.js"></script>

<style media="screen">
   .tableFixHead    { overflow-y: auto; height: 76%; }
   .tableFixHead th { position: sticky; top: -1px; }
   .ketInfo {color: white; font-weight: bold;background: #74BEDA; padding: 3px 5px}
   .ketDanger {color: white; font-weight: bold;background: red; padding: 3px 5px}
   .ketSuccess {color: white; font-weight: bold;background: green; padding: 3px 5px}
   .ketWarning {color: white; font-weight: bold;background: yellow; padding: 3px 5px}
   table  { border-collapse: collapse; width: 100%; }
   th     { background:#3C6DB0; z-index: 9; color: #FFFFFF}
   td     { padding: 3px 7px!important; }
   .selected {
      background-color:rgb(230, 141, 40)!important;
      color: #FFF!important;
   }

   label {padding-top:7px}
   .select2-container .select2-selection { height: 34px; border-color: #D2D6DE}
   .select2-container--default .select2-selection--single .select2-selection__arrow { margin-top: 3px;}
   .datepicker table tr th { background: #F4F4F4; color: #3C6DB0}
   th.ui-datepicker-week-end, td.ui-datepicker-week-end {display: none;}
</style>

<style type="text/css">
   .swal-wide {width: 90%; padding: 5px}   
</style>

<div class="row">
   <div class="col-md-12">
      <div class="nav-tabs-custom">
         <ul class="nav nav-tabs">
            <li class="dropdown">
               <a class="dropdown-toggle" data-toggle="dropdown" href="#"><i class="fa fa-user"></i>
                  Izin Saya <span class="caret"></span>
               </a>
               <ul class="dropdown-menu">
                  <li role="presentation"><a id="izin-default" role="menuitem" tabindex="-1" href="#" data-toggle="tab" data-status="semua"><i class="fa fa-list"></i>Semua</a></li>
                  <li role="presentation"><a role="menuitem" tabindex="-1" href="#" data-toggle="tab" data-status="proses"><i class="fa fa-hourglass-start"></i>Proses</a></li>
               </ul>
            </li>
            <?php if ($isAtasan) { ?>
               <li class="dropdown">
                  <a class="dropdown-toggle" data-toggle="dropdown" href="#"><i class="fa fa-users"></i>
                     Izin Bawahan <span class="caret"></span>
                  </a>
                  <ul class="dropdown-menu">
                     <li role="presentation"><a role="menuitem" tabindex="-1" href="#" data-toggle="tab" data-status="bawahan_semua"><i class="fa fa-list"></i>Semua</a></li>
                     <li role="presentation"><a role="menuitem" tabindex="-1" href="#" data-toggle="tab" data-status="bawahan_setuju"><i class="fa fa-check-square-o"></i>Persetujuan</a></li>
                  </ul>
               </li>
            <?php } ?>
            <li class="pull-right">
               <button class="btn btn-primary btn-flat pull-right" style="margin-left: 5px; margin-right:0px" data-toggle="modal" data-target="#ajuModal" data-type="aju">
                  <i class="fa fa-plus-circle text-white" style="padding:3px 0px"></i>&nbsp; Pengajuan
               </button>
            </li>
         </ul>
         <div class="tab-content">
            <div class="tab-pane active" id="tab_1">
               <div class="box box-solid tableFixHead">
                  <div class="overlay" hidden>
                     <i class="fa fa-refresh fa-spin"></i>
                  </div>
                  <table id="iGrid" class="table table-hover table-bordered">
                     <thead>
                        <tr>
                           <th class="text-center">ID</th>
                           <th class="text-center" data-header="pegawai"  hidden>Pegawai</th>
                           <th class="text-center">Izin</th>
                           <th class="text-center">Mulai</th>
                           <th class="text-center">Selesai</th>
                           <th class="text-center">Lama</th>
                           <th class="text-center">Status</th>
                        </tr>
                     </thead>
                     <tr id="template-row" data-toggle="collapse" data-target="" class="text-right" hidden>
                        <td data-row="id"></td>
                        <td data-row="pegawai" class="text-left" hidden></td>
                        <td data-row="izin" class="text-left"></td>
                        <td data-row="tanggalmulai"></td>
                        <td data-row="tanggalselesai"></td>
                        <td data-row="lama"></td>
                        <td data-row="status" class="text-left"></td>
                     </tr>

                     <tr id="template-row-detail" hidden>
                        <td class="hiddenRow" colspan="10" style="margin:0px!important; padding:0px!important">
                           <div class="small info collapse" aria-expanded="false" style="height: 0px;">
                              <div class="row" style="margin:10px;" >
                                 <div class="box box-widget" style="margin-bottom:0px">
                                    <div class="box-body">
                                       <div class="col-sm-12" style="padding:0px">
                                          <div class="col-xs-12 col-sm-4">
                                             <div class="callout callout-success"><p>PENGAJUAN</p></div>
                                             <table style="font-size: 13px" class="pull-left">
                                                <tr><td class="fa fa-user pull-right"></td><td data-row="bawahan_nama"></td></tr>
                                                <tr><td></td><td data-row="bawahan_nip"></td></tr>
                                                <tr><td></td><td data-row="bawahan_jabatan"></td></tr>
                                                <tr><td class="fa fa-calendar-check-o pull-right"></td><td data-row="tanggal"></td></tr>
                                                <tr><td class="fa fa-pencil-square-o pull-right"></td><td data-row="keterangan" style="word-wrap: break-word; white-space: normal;" class="text-justify"></td></tr>
                                                <tr><td class="fa fa-paperclip pull-right"></td><td data-row="dokumen"></td></tr>
                                             </table>
                                          </div>
                                          <div class="col-xs-12 col-sm-4">
                                             <div class="callout bg-gray" data-row="setuju_status"><p>PERSETUJUAN</p></div>
                                             <table style="font-size: 13px" class="pull-left">
                                                <tbody class="pull-left">
                                                   <tr><td class="fa fa-user pull-right"></td><td data-row="atasan_nama"></td></tr>
                                                   <tr><td></td><td data-row="atasan_nip"></td></tr>
                                                   <tr><td></td><td data-row="atasan_jabatan"></td></tr>
                                                   <tr><td class="fa fa-calendar-check-o pull-right"></td><td data-row="setuju_waktu"></td></tr>
                                                   <tr><td class="fa fa-pencil-square-o pull-right"></td><td data-row="setuju_catatan" style="word-wrap: break-word; white-space: normal;" class="text-justify"></td></tr>
                                                </tbody>
                                             </table>
                                          </div>
                                          <div class="col-xs-12 col-sm-4">
                                             <div class="callout bg-gray" data-row="tetap_status"><p>PENETAPAN</p></div>
                                             <table style="font-size: 13px" class="pull-left">
                                                <tbody class="pull-left">
                                                   <tr><td class="fa fa-user pull-right"></td><td data-row="sdm_nama">SDM</td></tr>
                                                   <tr><td class="fa fa-calendar-check-o pull-right"></td><td data-row="tetap_waktu"></td></tr>
                                                   <tr><td class="fa fa-pencil-square-o pull-right"></td><td data-row="tetap_catatan" style="word-wrap: break-word; white-space: normal;" class="text-justify"></td></tr>
                                                </tbody>
                                             </table>
                                          </div>
                                       </div>
                                    </div>
                                    <div id="atasan_aksi" class="box-footer" hidden>
                                       <div class="col-lg-2 pull-right">
                                          <button type="button" class="btn btn-block btn-success btn-flat btn-setuju" data-izin="">Setuju</button>
                                       </div>
                                       <div class="col-lg-2 pull-right">
                                          <button type="button" class="btn btn-block btn-danger btn-flat" data-toggle="modal" data-target="#tolakModal" data-izin="">Tolak</button>
                                       </div>
                                    </div>
                                    <div id="ajuUlang" class="box-footer" hidden>
                                       <div class="col-lg-2 pull-right">
                                          <button type="button" class="btn btn-block btn-primary btn-flat" data-toggle="modal" data-target="#ajuModal" data-izin="">PENGAJUAN ULANG</button>
                                       </div>
                                    </div>
                                    <div id="aksi" class="box-footer" hidden>
                                       <div class="col-lg-2 pull-right">
                                          <button type="button" class="btn btn-block btn-info btn-flat" data-toggle="modal" data-target="#ajuModal" data-izin="">UBAH</button>
                                       </div>
                                       <div class="col-lg-2 pull-right">
                                          <button type="button" class="btn btn-block btn-danger btn-flat btn-delete-izin" data-izin="">HAPUS</button>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </td>
                     </tr>

                     <tbody id="data_"></tbody>
                  </table>

                  <?php $this->load->view('izin/izin_aju'); ?>

               </div>
            </div>
            <!-- /.tab-pane -->
         </div>
         <!-- /.tab-content -->
      </div>
      <!-- nav-tabs-custom -->
   </div>
</div>

<!-- tolakModal -->
<div class="modal fade" id="tolakModal" tabindex="-1" role="dialog">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <form class="form-horizontal" method="POST" enctype="multipart/form-data" id="tolakForm">
            <div class="modal-body">
               <div class="form-group">
                  <label class="col-sm-2 control-label">Izin ID</label>
                  <div class="col-sm-10">
                     <input class="form-control" name="id" readonly>
                  </div>
               </div>
               <div class="form-group">
                  <label class="col-sm-2 control-label">Catatan</label>
                  <div class="col-sm-10">
                     <textarea class="form-control" name="catatan"></textarea>
                  </div>
               </div>
            </div>
            <div class="modal-footer">
               <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Batal</button>
               <button id="tolak" type="submit" class="btn btn-danger btn-flat">Tolak</button>
            </div>
         </form>
      </div>
   </div>
</div>
<!-- tolakModal End -->

<script type="text/javascript" src="<?=base_url();?>assets/plugins/bootstrapvalidator/js/bootstrapValidator.js"></script>
<script type="text/javascript" src="<?=base_url();?>assets/plugins/bootstrapvalidator/js/language/id_ID.js"></script>

<script type="text/javascript">
   var option;
   var currentRequest;
   $('[data-toggle="tab"]').click(function(e){
      if (currentRequest) {
         currentRequest.abort();
      }
      $('.overlay').show();
      option = $(this).attr('data-status');
      currentRequest = $.get("<?php echo base_url(); ?>izin?q=tNGPu&opt=/"+option, function(data) {
         var rows = JSON.parse(data);
         $('#data_').empty();

         if (rows.length == 0) {
            $('#data_').append('<tr><td colspan="10" class="text-danger"> Data tidak ada ...</td></tr>');
         }

         if (~option.indexOf('bawahan')) {
            $('[data-header="pegawai"]').show();
         } else {
            $('[data-header="pegawai"]').hide();
         }

         for (var i = 0; i < rows.length; i++) {
            var row = $('#template-row').clone();
            row.show();

            row.attr('data-target', '.'+rows[i]['id']);
            row.find('[data-row="id"]').each(function(){$(this).text(rows[i]['id'])});
            if (~option.indexOf('bawahan')) {
               row.find('[data-row="pegawai"]').each(function(){$(this).text(rows[i]['bawahan_nama']); $(this).show()});
            }
            row.find('[data-row="izin"]').each(function(){$(this).text(rows[i]['status'])});
            row.find('[data-row="alasan"]').each(function(){$(this).text(rows[i]['keterangan'])});
            var datang_time = '', pulang_time = '';
            if (rows[i]['status'] == 'TMH') {
               datang_time = rows[i]['tanggalmulai'].split(" ")[1];
               datang_time = datang_time != '00:00:00' ? datang_time : '';
               pulang_time = rows[i]['tanggalselesai'].split(" ")[1];
               pulang_time = pulang_time != '00:00:00' ? pulang_time : '';
            }
            row.find('[data-row="tanggalmulai"]').each(function(){$(this).text(formatDate(rows[i]['tanggalmulai'])+' '+datang_time)});
            row.find('[data-row="tanggalselesai"]').each(function(){$(this).text(formatDate(rows[i]['tanggalselesai'])+' '+pulang_time)});
            row.find('[data-row="lama"]').each(function(){$(this).text(rows[i]['jml'] + ' Hari')});
            var status_label = "<span class='label label-warning'>Proses</span>";
            var setuju_status_class = 'bg-yellow', setuju_status='callout-warning';
            var setuju_catatan = '';
            if (rows[i]['setuju_status'] == '1') {
               setuju_status_class = 'bg-green';
               setuju_status = 'callout-success';
            } else if (rows[i]['setuju_status'] == '2'){
               setuju_status_class = 'bg-red';
               status_label = "<span class='label label-danger'>Ditolak</span>";
               setuju_catatan = rows[i]['catatan'];
               setuju_status = 'callout-danger';
            } 
            row.find('[data-row="setuju_status"]').each(function(){$(this).addClass(setuju_status_class)});
            var tetap_status_class = '', tetap_status='';
            var tetap_catatan = '';
            if (rows[i]['approval'] == '1') {
               tetap_status_class = 'bg-green';
               status_label = "<span class='label label-success'>Diterima</span>";
               tetap_status = 'callout-success';
            } else if (rows[i]['approval'] == '2'){
               tetap_status_class = 'bg-red';
               status_label = "<span class='label label-danger'>Ditolak</span>";
               tetap_catatan = rows[i]['catatan'];
               tetap_status = 'callout-danger';
            } else if (rows[i]['setuju_status'] == '1'){
               tetap_status_class = 'bg-yellow';
               tetap_status = 'callout-warning';
            }
            row.find('[data-row="tetap_status"]').each(function(){$(this).addClass(tetap_status_class)});
            row.find('[data-row="status"]').each(function(){$(this).append(status_label)});

            var rowDetail = $('#template-row-detail').clone();
            rowDetail.show();
            rowDetail.find('.small').each(function(){$(this).addClass(rows[i]['id'])});
            rowDetail.find('[data-row="bawahan_nama"]').each(function(){$(this).text(rows[i]['bawahan_nama'])});
            rowDetail.find('[data-row="bawahan_nip"]').each(function(){$(this).text(rows[i]['bawahan_nip'])});
            rowDetail.find('[data-row="bawahan_jabatan"]').each(function(){$(this).text(rows[i]['bawahan_jabatan'])});
            rowDetail.find('[data-row="tanggal"]').each(function(){$(this).text(formatDate(rows[i]['tanggalsurat']))});
            rowDetail.find('[data-row="keterangan"]').each(function(){$(this).text(rows[i]['keterangan'])});
            rowDetail.find('[data-row="dokumen"]').each(function(){$(this).append('<a href="#" data-img="<?= base_url() ?>files/upload_milea/'+rows[i]['bawahan_nip']+'/'+formatDate(rows[i]['tanggalsurat'])+'_'+rows[i]['dokumen']+'">'+rows[i]['dokumen']+'</a>')});
            rowDetail.find('[data-row="setuju_status"]').each(function(){$(this).addClass(setuju_status)});
            rowDetail.find('[data-row="atasan_nama"]').each(function(){$(this).text(rows[i]['atasan_nama'])});
            rowDetail.find('[data-row="atasan_nip"]').each(function(){$(this).text(rows[i]['atasan_nip'])});
            rowDetail.find('[data-row="atasan_jabatan"]').each(function(){$(this).text(rows[i]['atasan_jabatan'])});
            rowDetail.find('[data-row="setuju_waktu"]').each(function(){$(this).text(formatDate(rows[i]['setuju_waktu']))});
            rowDetail.find('[data-row="setuju_catatan"]').each(function(){$(this).text(setuju_catatan)});
            rowDetail.find('[data-row="tetap_status"]').each(function(){$(this).addClass(tetap_status)});
            rowDetail.find('[data-row="tetap_waktu"]').each(function(){$(this).text(formatDate(rows[i]['tanggalapproval']))});
            rowDetail.find('[data-row="tetap_catatan"]').each(function(){$(this).text(tetap_catatan)});
            if (~option.indexOf('bawahan') && rows[i]['setuju_status'] == null) {
               rowDetail.find('#atasan_aksi').each(function(){$(this).show()});
               rowDetail.find('button').each(function(){$(this).attr('data-izin', rows[i]['id'])});
            }
            if (option.indexOf('bawahan') == -1) {
               if (rows[i]['setuju_status'] == '2' || rows[i]['approval'] == '2') {
                  rowDetail.find('#ajuUlang').show();
                  rowDetail.find('#ajuUlang').find('button').each(function(){
                     $(this).attr('data-izin', JSON.stringify(rows[i]));
                     $(this).attr('data-type', 'ulang');
                  });
               }
               if (rows[i]['setuju_status'] == null) {
                  rowDetail.find('#aksi').show();
                  rowDetail.find('#aksi').find('button').each(function(){
                     $(this).attr('data-izin', JSON.stringify(rows[i]));
                     $(this).attr('data-type', 'edit');
                  });
               }
            }

            row.appendTo('#data_');
            rowDetail.appendTo('#data_');
         }
         $('.overlay').hide();
         return false;
      });
});

$('#izin-default').click();

$('#data_').on('click', '.btn-setuju', function(){
   $.ajax({
      type: "POST",
      url: '<?= base_url('izin?q=vR3ys');?>',
      data: { 
         id: $(this).attr('data-izin'), 
         value: '1' 
      },
      dataType: 'json',
      success: function(result) {
         $('[data-status='+option+']').click();
         swal({
            text: result['message'],
            icon: result['state'] ? 'success' : 'error'
         })
      },
      error: function(result) {
         swal({
            text: 'ERROR! Persetujuan gagal.',
            icon:'error'
         })
      }
   });
});

$('#tolakModal').on('show.bs.modal', function (event) {
   $('#tolakModal [name="id"]').val($(event.relatedTarget).data('izin') );
})

$('#data_').on('show.bs.collapse', function () {
   $('.collapse.in').collapse('hide');
});

var $rows = $('#data_ tr');
$('#search').keyup(function() {
   var val = $.trim($(this).val()).replace(/ +/g, ' ').toLowerCase();

   $rows.show().filter(function() {
      var text = $(this).text().replace(/\s+/g, ' ').toLowerCase();
      return !~text.indexOf(val);
   }).hide();
});

$('.tglStart,.tglEnd').datepicker({
   format: "dd-mm-yyyy",
   language: "id",
   orientation: "bottom auto",
   autoclose: true,
   todayHighlight: true
}).on('changeDate', function() {
   var idnip = $('#idnip').val();
   sta   = $('#start').val();
   end   = $('#end').val();
   window.location.href = "<?php echo site_url('milea/manual') ?>" +'/'+ idnip +'/'+ sta +'/'+ end;
});


$(function() {
   $.fn.modal.Constructor.prototype.enforceFocus = function() {};
   $('.select_tandatangan').select2({
      placeholder: "Pilih Penandatangan"
   });
   $('.select_status').select2({
      placeholder: "Pilih Status"
   });
});

/*
This function is used to reformat date string to 'd-sm-yyyy'
*/
function formatDate(dateString){
   if (!dateString) {
      return '';
   }
   date = new Date(dateString);
   var month = date.getMonth()+1;
   var day = date.getDate();
   return (day.toString().length == 1 ? '0'+day : day)  + "-" + (month.toString().length == 1 ? '0'+month : month) + "-" + date.getFullYear();
}

</script>
<script type="text/javascript">
   $(function() {
      $('#tolakForm').bootstrapValidator({
         excluded: [':disabled', ':hidden'],
         feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
         },
         fields: {
            id: {
               validators: {
                  notEmpty: {
                  },
               }
            },
            catatan: {
               validators: {
                  notEmpty: {
                  },
               }
            },
         }
      })      
      .on('success.form.bv', function(e) {
         e.preventDefault();
         var $form = $(e.target);
         var bv = $form.data('bootstrapValidator');
         $.ajax({
            type: "POST",
            url: '<?= base_url('izin?q=vR3ys');?>',
            data: $form.serialize()+'&value=2',
            dataType: 'json',
            success: function(result) {
               $('#tolakModal').modal('toggle');
               $('[data-status='+option+']').click();
               swal({
                  text: result['message'],
                  icon: result['state'] ? 'success' : 'error'
               })
            },
            error: function(result) {
               swal({
                  text: 'ERROR! Persetujuan gagal.',
                  icon:'error'
               })
            }
         });
      });
   });
</script>

<script type="text/javascript">
   $(function(){
      $('#data_').on('click', '.btn-delete-izin', function(){
         var izin_id = $(this).data('izin').id;
         swal({
            title: "Anda yakin?",
            text: "Hapus Izin #" + izin_id,
            icon: "warning",
            buttons: {
               cancel: {
                  text: "TIDAK",
                  visible: true
               },
               confirm: {
                  text: "YA",
                  closeModal: false
               }
            },
            dangerMode: true,
         })
         .then((willDelete) => {
            if (willDelete) {
               $.post("<?= site_url('izin?q=Z7EbR') ?>", {'id': izin_id}, function(result) {
                  if (result['state']) {
                     swal(result['message'], {
                        icon: "success",
                     }); 
                     $('[data-status='+option+']').click();
                  } else {
                     swal(result['message'], {
                        icon: "error",
                     });
                  }
               }, 'json');
            } 
         });
      });

      $('#data_').on('click', 'a', function(){
         swal({
            icon: $(this).attr('data-img'),
            className: "swal-wide",
         });
         return false;
      });
   });
</script>

