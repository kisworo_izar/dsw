
<div class="row">
  <div class="col-md-7">

    <?php foreach ($info as $row) { 
      $artikel = explode('<hr />', $row['artikel']) ?>
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title"><a href="<?php echo site_url('home/reading') ?>"><?php echo $row['judul'] ?></a></h3><br>
          <small><i>by :</i><a href="#">Admin</a></small> &nbsp;&nbsp; 
          <small><i>tanggal: </i>15 Juni 2015</small>                   
         <div class="box-tools pull-right">
            <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
            <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
          <img src="<?php echo base_url() .'files/images/'.$row['gambar'] ?>" class="img-responsive"><br />
          <p style="text-align: justify;"><?php echo $artikel[0] ?><a href="<?php echo site_url('home/reading') ?>">continue reading...</a></p>
        </div><!-- /.box-body -->
        <div class="box-footer">
          <small><em>sumber:</em> <?php echo $row['sumber'] ?></small>
        </div>
      </div><!-- /.box -->
    <?php } ?>

  </div><!-- /.col -->
  <div class="col-md-5">
    <div class="box box-primary">
      <div class="box-body no-padding">
        <!-- THE CALENDAR -->
        <div id="calendar"></div>
      </div><!-- /.box-body -->
    </div><!-- /. box -->

    <div class="box box-default">
      <div class="box-header with-border">
        <h3 class="box-title">Komposisi per Jenis Belanja</h3>
        <div class="box-tools pull-right">
          <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
        </div>
      </div><!-- /.box-header -->
      <div class="box-body">
        <div class="row">
          <div class="col-md-8">
            <div class="chart-responsive">
              <canvas id="pieChart" height="150"></canvas>
            </div><!-- ./chart-responsive -->
          </div><!-- /.col -->
          <div class="col-md-4">
            <ul class="chart-legend clearfix">
              <li><i class="fa fa-circle-o text-red"></i> Belanja Pegawai</li>
              <li><i class="fa fa-circle-o text-green"></i> Belanja Barang</li>
              <li><i class="fa fa-circle-o text-yellow"></i> Belanja Modal</li>
            </ul>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.box-body -->
      <div class="box-footer no-padding">
        <ul class="nav nav-pills nav-stacked">
          <li><a href="#">Belanja Pegawai <span class="pull-right text-red"><i class="fa fa-angle-down"></i> 12%</span></a></li>
          <li><a href="#">Belanja Barang <span class="pull-right text-green"><i class="fa fa-angle-up"></i> 4%</span></a></li>
          <li><a href="#">Belanja Modal <span class="pull-right text-yellow"><i class="fa fa-angle-left"></i> 0%</span></a></li>
        </ul>
      </div><!-- /.footer -->
    </div><!-- /.box -->
    
  </div><!-- /.col -->
</div><!-- /.row -->

<link href="<?php echo base_url(); ?>assets/plugins/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url(); ?>assets/plugins/fullcalendar/fullcalendar.print.css" rel="stylesheet" type="text/css" media='print' />
<script src="<?php echo base_url();?>assets/plugins/fullcalendar/moment.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/plugins/fullcalendar/fullcalendar.min.js" type="text/javascript"></script>
<script type="text/javascript">
  $(function () {

    /* initialize the calendar
     -----------------------------------------------------------------*/
    //Date for the calendar events (dummy data)
    $('#calendar').fullCalendar({
      header: {
        left: 'prev,next',
        center: 'title',
        right: 'today'
      },
      buttonText: {
        today: 'today',
        month: 'month',
        week: 'week',
        day: 'day'
      },
      //Random default events
      events: [
        {
          title: 'Cuti Bersama',
          start: '2015-07-16',
          backgroundColor: "#f39c12", //yellow
          borderColor: "#f39c12" //yellow
        },
        {
          title: 'Idhul Fitri 1436H',
          start: '2015-07-17 01:00:00',
          end: '2015-07-18 24:00:00',
          backgroundColor: "#f56954", //red
          borderColor: "#f56954" //red
        },
        {
          title: 'Cuti Bersama',
          start: '2015-07-20 02:00',
          end: '2015-07-21 24:00',
          backgroundColor: "#f39c12", //yellow
          borderColor: "#f39c12" //yellow
        },
        {
          title: 'Hari Kemerdekaan RI',
          start: '2015-08-17',
          backgroundColor: "#f56954", //yellow
          borderColor: "#f56954" //yellow
        },
      ],
      editable: false,
      droppable: false, // this allows things to be dropped onto the calendar !!!
      drop: function (date, allDay) { // this function is called when something is dropped

        // retrieve the dropped element's stored Event Object
        var originalEventObject = $(this).data('eventObject');

        // we need to copy it, so that multiple events don't have a reference to the same object
        var copiedEventObject = $.extend({}, originalEventObject);

        // assign it the date that was reported
        copiedEventObject.start = date;
        copiedEventObject.allDay = allDay;
        copiedEventObject.backgroundColor = $(this).css("background-color");
        copiedEventObject.borderColor = $(this).css("border-color");

        // render the event on the calendar
        // the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
        $('#calendar').fullCalendar('renderEvent', copiedEventObject, true);

        // is the "remove after drop" checkbox checked?
        if ($('#drop-remove').is(':checked')) {
          // if so, remove the element from the "Draggable Events" list
          $(this).remove();
        }

      }
    });

  });
</script>

<script src="<?php echo base_url(); ?>assets/plugins/chartjs/Chart.min.js" type="text/javascript"></script>
<script type="text/javascript">
  //-------------
  //- PIE CHART -
  //-------------
  // Get context with jQuery - using jQuery's .get() method.
  var pieChartCanvas = $("#pieChart").get(0).getContext("2d");
  var pieChart = new Chart(pieChartCanvas);
  var PieData = [
    {
      value: 500,
      color: "#f56954",
      highlight: "#f56954",
      label: "Belanja Pegawai"
    },
    {
      value: 1000,
      color: "#00a65a",
      highlight: "#00a65a",
      label: "Belanja Barang"
    },
    {
      value: 400,
      color: "#f39c12",
      highlight: "#f39c12",
      label: "Belanja Modal"
    },
  ];
  var pieOptions = {
    //Boolean - Whether we should show a stroke on each segment
    segmentShowStroke: true,
    //String - The colour of each segment stroke
    segmentStrokeColor: "#fff",
    //Number - The width of each segment stroke
    segmentStrokeWidth: 2,
    //Number - The percentage of the chart that we cut out of the middle
    percentageInnerCutout: 50, // This is 0 for Pie charts
    //Number - Amount of animation steps
    animationSteps: 100,
    //String - Animation easing effect
    animationEasing: "easeOutBounce",
    //Boolean - Whether we animate the rotation of the Doughnut
    animateRotate: true,
    //Boolean - Whether we animate scaling the Doughnut from the centre
    animateScale: false,
    //Boolean - whether to make the chart responsive to window resizing
    responsive: true,
    // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
    maintainAspectRatio: false,
    //String - A legend template
    legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>"
  };
  //Create pie or douhnut chart
  // You can switch between pie and douhnut using the method below.
  pieChart.Doughnut(PieData, pieOptions);
</script>            
