<link href="<?= base_url('assets/plugins/bxslider/jquery.bxslider.css'); ?>" rel="stylesheet" type="text/css" />
<link href="<?= base_url('assets/dist/css/marquee.css'); ?>" rel="stylesheet" type="text/css" />
<link href="<?= base_url('assets/dist/css/renja.css') ?>" type="text/css" rel="stylesheet" />

<script src="<?= base_url('assets/plugins/bxslider/jquery.bxslider.min.js'); ?>" type="text/javascript"></script>
<script src="<?= base_url('assets/plugins/jQuery/jQuery.Shorten.1.0.js'); ?>" type="text/javascript"></script>
<script src="<?= base_url('assets/plugins/justgage/raphael-2.1.4.min.js'); ?>" type="text/javascript"></script>
<script src="<?= base_url('assets/plugins/justgage/justgage.js'); ?>" type="text/javascript"></script>

<style>
    #g1 {
        width: 110px;
        height: 110px;
        float: left;
    }

    .txt {
        padding-top: 25px;
    }

    .txt1,
    .txt2,
    .txt3 {
        text-align: left;
        color: #2D7DAD;
        right: 0px;
        padding: 0px;
    }

    .txt1 {
        padding-bottom: 5px;
        font-size: 17px;
    }

    .txt2,
    .txt3 {
        color: #262B30;
        font-size: 12px
    }

    .img_li {
        display: block;
        width: 100%;
        position: relative;
        height: 0;
        padding: 56.25% 0 0 0;
        overflow: hidden;
    }

    .img_img {
        position: absolute;
        display: block;
        max-width: 100%;
        max-height: 100%;
        left: 0;
        right: 0;
        top: 0;
        bottom: 0;
        margin: auto;
    }
</style>

<div class="row">
    <div class="col-md-6">
        <div class="dsw-info-box" tooltip="Presensi Pegawai">
            <?php
            if ($absensi) {
                $i = 0;
                foreach ($absensi as $row) {
                    if ($i == 0) { ?>
                        <?php if (strtotime($row['tgldatang']) < strtotime(substr($row['tgldatang'], 0, 11) . '07:31:00')) {
                            $bgabsen = 'text-green';
                        } else {
                            $bgabsen = 'text-red';
                        } ?>
                        <span class="dsw-info-box-icon2" style="background:#ffffff"> <a href="<?php echo site_url('absensi') ?>" style="color:white">
                                <i class="fa fa-clock-o <?php echo $bgabsen ?>"></i></a></span>
                        <div class="dsw-info-box-content">
                            <span class="dsw-info-box-text"><b><?php echo $this->fc->idtgl($row['tgl'], 'hari'); ?></b></span>
                            <span class="dsw-info-box-number text-yellow"><?php echo substr($row['tgldatang'], 11, 5) ?> |
                            <?php } else { ?>
                                <span class="small text-muted"><?php echo substr($row['tgldatang'], 11, 5) ?> - <?php echo substr($row['tglpulang'], 11, 5) ?></span>
                                <span class="pull-right small <?php echo $bgabsen ?>"> <a href="<?php echo site_url('absensi') ?>">Presensi</a> </span>
                            </span>
                    <?php }
                        $i++;
                    } ?>
                        </div>
                    <?php } else {
                    echo '&nbsp;';
                } ?>
        </div>
    </div>

    <div class="col-md-6 hidden-xs">
        <div class="dsw-info-box" style="padding-right:45px" tooltip="RSS berita nasional">
            <span class="dsw-info-box-icon bg-white"><i class="fa fa-rss text-orange"></i></span>
            <div id="text-carousel" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner" style="position:absolute; left:45px">

                    <?php
                    $i = 1;
                    foreach ($d_rss as $row) {
                    ?>
                        <div class="item <?php if ($i == 1) echo 'active' ?>">
                            <div class="carousel-content dsw-info-box-content" style="margin-left:-10px">
                                <span class="dsw-info-box-text">
                                    <b><?php echo $row['site'] ?></b> <?php echo $this->fc->idtgl($row['pubdate'], 'full') ?>
                                </span>
                                <span class="dsw-info-box-text">
                                    <a href="<?php echo $row['link'] ?>" target="_blank"><?php echo $row['berita'] ?></a>
                                </span>
                            </div>
                        </div>
                    <?php
                        $i++;
                    }
                    ?>

                </div>
            </div>
        </div>
    </div>
</div>



<div class="row">
    <div class="col-md-6">
        <?php $this->load->view('main/dashboard_forum') ?>
    </div>

    <div class="col-md-6">
        <div class="box box-widget" style="margin-bottom: 10px">
            <div class="bxslider">
                <?php foreach ($slideshow as $row) { ?>
                    <li class="img_li">
                        <a target='_blank' href="<?php echo $row['link'] ?>">
                            <img class="img_img" src="<?php echo site_url('files/slideshow') . '/' . $row['gambar'] ?>" title="<?php echo $row['keterangan'] ?>" />
                        </a>
                    </li>
                <?php } ?>
            </div>
        </div>
        <div class="box-header with-border text-center" style="border-radius:0px;padding:3px 7px; background-color:#1661AB">
            <span class="small" style="color:#FFFFFF"><a href="https://www.youtube.com/channel/UCLWS6ooefElyfa6TfZVDDnA" style="color:white"><i class="fa fa-youtube-play"></i>Anggaran Kemenkeu</a></span>&nbsp;&nbsp;&nbsp;
            <span class="small" style="color:#FFFFFF"><a href="https://twitter.com/ditjenanggaran" style="color:white"><i class="fa fa-twitter"></i>DitjenAnggaran</a></span>&nbsp;&nbsp;&nbsp;
            <span class="small" style="color:#FFFFFF"><a href="https://www.instagram.com/ditjenanggaran/" style="color:white"><i class="fa fa-instagram"></i>ditjenanggaran</a></span><br>
            <span class="small" style="color:#FFFFFF"><i class="fa fa-phone"></i>021-3868085</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <span class="small"> <a href="mailto:pusatlayanan.dja@kemenkeu.go.id" style="color:#FFFFFF"><i class="fa fa-envelope"></i>pusatlayanan.dja@kemenkeu.go.id</a></span>
        </div>
        <div class="box box-widget hidden-xs" style="margin-bottom:10px">
            <div class="box box-widget text-center" style="padding: 5px 10px; margin:0px; background-color:#fff;">
                <!-- <span tooltip="Perpustakaan DJA"><i class="fa fa-book"></i></span> -->
                <span class="pull-left" tooltip="Perpustakaan DJA">
                    <a href="<?php echo site_url('perpustakaan') ?>">
                        <img class="attachment-img " width="32px" src="<?php echo base_url('files/images/head.png'); ?>">
                        <span style="font-size:15px; margin-top:20px">8054 - 9715</span>

                        <!-- <i class="fa fa-2x fa-book text-white"></i> -->
                    </a>
                </span>
                <span tooltip="Standar Operaional Prosedur DJA" style="padding: 0px 15px">
                    <a href="http://bit.ly/SOP_DJA" target="_blank">
                        <img class="attachment-img" width="100px" src="<?php echo base_url('files/images/sop_dja.png'); ?>">
                    </a>
                </span>
                <span tooltip="Kinerja DJA" style="padding: 0px 15px">
                    <a href="http://bit.ly/kinerjadja" target="_blank">
                        <img class="attachment-img" width="100px" src="<?php echo base_url('files/images/KinerjDJA.png'); ?>">
                    </a>
                </span>
                <span tooltip="Glosarium Anggaran Negara" style="padding: 0px 15px">
                    <a href="http://intranet.anggaran.depkeu.go.id/referensi?q=9l05r">
                        <img class="attachment-img " width="90px" src="<?php echo base_url('files/images/glo.jpeg'); ?>">
                    </a>
                </span>
                <span class="pull-right" tooltip="KM DJA">
                    <a href="https://10.242.142.200/">
                        <img class="attachment-img " width="50px" src="<?php echo base_url('files/images/km.png'); ?>">
                    </a>
                </span>
            </div>
        </div>

        <div class="box box-widget">
            <div class="box-header with-border">
                <h3 class="box-title text"> Pengumuman</h3>
                <div class="small box-tools" style="padding-top:5px">
                    <a href="<?php echo site_url('home/pengumuman'); ?>"><i class="fa fa-link"></i></a>
                </div>
            </div>

            <?php
            $first = 1;
            foreach ($pengumuman as $row) {
                $arr = explode('<p>', $row['pengumuman']);
                $pengumuman = trim($arr[1]);
                $lengkapnya = str_replace($pengumuman, '', $row['pengumuman']);

                if ($first == 1) {
                    $first++; ?>

                    <!-- pengumuman terbaru -->
                    <div class="box-body" style="background-color:#F7F7F7">
                        <div class="attachment-block clearfix" style="padding:0px; margin-bottom:0px">
                            <?php
                            if ($row['gambar']) {
                                echo '<img class="attachment-img" src="' . base_url("files/pengumuman") . '/' . $row['gambar'] . '">';
                            } else {
                                echo '<img class="attachment-img" src="' . base_url("files/images/depkeu_news.png") . '">';
                            }
                            ?>
                            <div class="attachment-pushed">
                                <span class="username">
                                    <b>Sekretariat</b>
                                    <span class="small text-muted pull-right"><?php echo $this->fc->idtgl($row['tglrekam'], 'hari') ?></span>
                                </span>
                                <h4 class="attachment-heading"><a href="#" onclick="showHide('<?php echo 'hidden_div' . $row['idpengumuman'] ?>'); return false;"><?php echo $row['judul'] ?></a>
                                </h4>

                                <div class="attachment-text">
                                    <?php
                                    echo $pengumuman;
                                    echo '<div id="' . 'hidden_div' . $row['idpengumuman'] . '" style="display: none;">' . $lengkapnya . '</div>';
                                    if ($row['file']) {
                                        echo '<i class="small">File : </i>
                                        <a href="' . base_url("files/pengumuman") . '/' . $row['file'] . '"  target=_blank> &nbsp;<i class=" small fa fa-file-pdf-o"></i> <i class="small">' . $row['file'] . '</a></i>';
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- <div class="<?php if ($first == 2) echo 'box-footer'; ?> box-comments"> -->
                    <div class="box-footer box-comments" style="background:white">
                    <?php } else { ?>

                        <div class="box-comment">
                            <img class="img-circle img-sm img-bordered-dsw" src="<?php echo base_url("files/images/depkeu_round.png") ?>">
                            <div class="comment-text">
                                <span class="username">
                                    Sekretariat
                                    <span class="text-muted pull-right"><?php echo $this->fc->idtgl($row['tglrekam'], 'hari') ?></span>
                                </span>
                                <a href="#" onclick="showHide('<?php echo 'hidden_div' . $row['idpengumuman'] ?>'); return false;"><?php echo $row['judul'] ?></a><br>
                                <?php
                                if ($row['gambar']) {
                                    echo '<img style="width:95px !important; height:75px !important; margin: 5px 10px 5px 0px" src="' . base_url("files/pengumuman") . '/' . $row['gambar'] . '">';
                                }
                                echo $pengumuman;
                                echo '<div id="' . 'hidden_div' . $row['idpengumuman'] . '" style="display: none;">' . $lengkapnya . '</div>';
                                if ($row['file']) {
                                    echo '<i class="small">File : </i>
                                    <a href="' . base_url("files/pengumuman") . '/' . $row['file'] . '" target=_blank><i class=" small fa fa-file-pdf-o"></i> <i class="small">' . $row['file'] . '</a></i>';
                                }
                                ?>
                            </div>
                        </div>

                <?php $first++;
                }
            } ?>
                    </div>

                    <div class="box-footer">
                        <div class="small text-center">
                            <a href="<?php echo site_url('home?q=Gag2c') ?>">Tampilkan Seluruh Pengumumam </a>
                        </div>
                    </div>
        </div>

    </div>
</div>



<script type="text/javascript">
    $(document).ready(function() {
        $(".more").shorten({
            "showChars": 150,
            "moreText": "<i class='small'> Read More</i>",
            "lessText": " <i class='small'> Less</i>"
        });
    });

    $(document).ready(function() {
        $('.bxslider').bxSlider({
            mode: 'fade',
            pause: 10000,
            infiniteLoop: true,
            controls: false,
            pager: false,
            auto: true,
            randomStart: true,
            captions: true
        });
    });

    function showHide(obj) {
        var div = document.getElementById(obj);
        if (div.style.display == 'none') {
            div.style.display = '';
        } else {
            div.style.display = 'none';
        }
    }
</script>