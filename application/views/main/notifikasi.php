
<style media="screen">
    .badge {
        margin-top:0px;background: radial-gradient( 5px -9px, circle, white 8%, red 26px );
        background-color: red;border: 1px solid white;border-radius: 12px;color: white;text-align: center;
        font: bold 10px/10px Helvetica, Verdana, Tahoma;height: 14px;min-width: 2px;padding: 1px 3px 0px 3px;}
</style>

<?php
    $clear = ''; $hit=0;
    if (!$notif) {
        echo "<a href='#' class='dropdown-toggle' data-toggle='dropdown'>
                <i class='fa fa-bell-o'></i>
              </a>
            <ul class='dropdown-menu'>
                <li class='header'>Notifikasi :</li>
                <li style='border-radius:0px'>
                    <ul class='menu'>
                        <li style='padding-left: 10px;'>Tidak ada notifikasi baru</li>
                    </ul>
                <li class='footer'>
                    <a href='". site_url('home/notif_history') ."'>Lihat Semua</a>
                </li>
            </ul>
            ";
    } else {
        echo "
            <a href='#' class='dropdown-toggle' data-toggle='dropdown'>
                <i class='fa fa-bell-o'></i>
                <sup class='badge'>".sizeof($notif)."</sup>
            </a>
            <ul class='dropdown-menu'>
                <li class='header'>Notifikasi :</li>

                <li style='border-radius:0px'>
                    <ul class='menu'>";
                    foreach ($notif as $row) {
                        $clear .= $row['idnotif']; $hit++;
                        if ($hit<sizeof($notif)) $clear .= ','; 
                        echo "
                            <li>
                            <a href='". $row['link'] ."' onclick=\"notif_read('". $row['idnotif'] ."')\" style='padding:5px 10px;cursor:default'>
                                <h4 style='margin:0px;'>
                                    <i class=''></i> ". $row['judul'] ."
                                    <small>". $this->fc->idtgl($row['waktu'],'tgljam') ."
                                    <i class='fa fa-calendar'></i> </small>
                                </h4>
                                <p style='margin:0px;color:#5D5D5D'>".mb_strimwidth($row['pesan'],0,49,'...')."</p>
                            </a>
                            </li>
                            ";
                    }
        echo "
                    </ul>
                </li>
                <li class='footer'>
                    <a href='". site_url('home') ."' onclick=\"notif_clear('". $clear ."')\">Clear All</a>
                </li>
                <li class='footer'>
                    <a href='". site_url('home/notif_history') ."'>Lihat Semua</a>
                </li>
            </ul>
        ";
    }
?>


<script>
    function notif_read( nilai ) {
        $.ajax({
            url : "<?php echo site_url('home/notif_read') ?>",
            type: "POST",
            data: { 'nilai': nilai },
        })
    }

    function notif_clear( nilai ) {
        $.ajax({
            url : "<?php echo site_url('home/notif_clear') ?>",
            type: "POST",
            data: { 'nilai': nilai },
        })
    }
</script>