<aside class="control-sidebar control-sidebar-dark">
    <!-- create the tabs -->
    <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
        <!-- <li class="active"><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li> -->
        <li><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
        <!-- <li><a href="#control-sidebar-setting-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li> -->
    </ul>
    <!-- tab panes -->
    <div class="tab-content">
        <div class="tab-pane" id="control-sidebar-home-tab">
            <!-- control sidebar menu 1 -->
            <h3 class="control-sidebar-heading">Aktifitas lalu</h3>
            <ul class='control-sidebar-menu'>
                <li>
                    <a href='javascript::;'>
                        <i class="menu-icon fa fa-birthday-cake bg-red"></i>
                        <div class="menu-info">
                            <h4 class="control-sidebar-subheading">Ulang tahun Albert</h4>
                            <p>
                                Tanggal 21 Desember 2015
                            </p>
                        </div>
                    </a>
                </li>
            </ul><!-- control sidebar menu 1 end -->

            <!-- control sidebar menu 2 -->
            <h3 class="control-sidebar-heading">Task Progress</h3>
            <ul class='control-sidebar-menu'>
                <li>
                    <a href='javascript::;'>
                        <h4 class="control-sidebar-subheading">
                        Desain UI Aplikasi
                        <span class="label label-danger pull-right">70%</span>
                        </h4>
                        <div class="progress progress-xss">
                            <div class="progress-bar progress-bar-danger" style="width: 70%"></div>
                        </div>
                    </a>
                </li>

                <li>
                    <a href='javascript::;'>
                        <h4 class="control-sidebar-subheading">
                        Update Aplikasi ADIK
                        <span class="label label-danger pull-right">90%</span>
                        </h4>
                        <div class="progress progress-xss">
                            <div class="progress-bar progress-bar-danger" style="width: 90%"></div>
                        </div>
                    </a>
                </li>
            </ul><!-- control sidebar menu 2 end -->
        </div>
<!--
        seting tab content
        <div class="tab-pane" id="control-sidebar-setting-tab">
            <form method="post">
                <h3 class="control-sidebar-heading">General Setting</h3>
                <div class="form-group">
                    <label class="control-sidebar-subheading">
                    Report panel usage
                    <input type="checkbox" class="pull-right" checked>
                    </label>
                    <p>
                        Pengaturan pilihan
                    </p>
                </div>form group end
            </form>
        </div>tab-pane end
-->                </div>
</aside><!-- control-sidebar end -->
