<link href="<?=base_url('assets/plugins/bxslider/jquery.bxslider.css');?>" rel="stylesheet" type="text/css" />
<link href="<?=base_url('assets/dist/css/marquee.css');?>" rel="stylesheet" type="text/css" />

<script src="<?=base_url('assets/plugins/bxslider/jquery.bxslider.min.js'); ?>" type="text/javascript"></script>
<script src="<?=base_url('assets/plugins/jQuery/jQuery.Shorten.1.0.js'); ?>" type="text/javascript"></script>
<script src="<?=base_url('assets/plugins/justgage/raphael-2.1.4.min.js'); ?>" type="text/javascript"></script>
<script src="<?=base_url('assets/plugins/justgage/justgage.js'); ?>" type="text/javascript"></script>

<style>
    #g1 {width: 110px;height: 110px;float: left;}
    .txt {padding-top: 25px;}
    .txt1, .txt2, .txt3 {text-align: left;color:#2D7DAD;right: 0px;padding: 0px;}
    .txt1 {padding-bottom: 5px;font-size:17px;}
    .txt2, .txt3 {color:#262B30;font-size:12px}
</style>

<script type="text/javascript">
    $(document).ready(function() {
        $(".more").shorten({
            "showChars" : 150,
            "moreText"  : "<i class='small'> Read More</i>",
            "lessText"  : " <i class='small'> Less</i>"
        });
    });
</script>

<script type="text/javascript">
$(document).ready(function(){
    $('.bxslider').bxSlider({
     mode: 'fade',
     pause: 10000,
     infiniteLoop: true,
     controls: true,
     pager: false,
     auto: true,
     randomStart: true,
     captions: true
   });
});
</script>

<script type="text/javascript">
var g1;
document.addEventListener("DOMContentLoaded", function(event) {
    g1 = new JustGage({
        id: "g1",
        donut: true,
        value: 0,
        valueFontColor: '#327EAB',
        min: 0,
        max: 100,
        title: "",
        titlePosition: "above",
        gaugeColor: '#CDE2EF',
        gaugeWidthScale: 0.15,
        label: "Capaian [%]",
        labelFontColor: '#85B9D7',
        levelColors: [
          "#FF0000",
          "#FFFF00",
          "#008932"
        ]
    });

    function text(){
        document.getElementById("t1").innerHTML = vNilai[ i ][ 0 ];
    }
    var vNilai = [<?php foreach ($d_moncap as $row) { echo '['.$row['id_bidang'].','.$row['jumlah'].','.$row['progres'].'],' ;} ?>];
    i = -1;
    (function f(){
        i = (i + 1) % vNilai.length;
        var vLink = '<?php echo base_url("fokus/bidang/") ?>'+'/'+vNilai[ i ][ 0 ];
        document.getElementById("txt1").innerHTML = '<a href="'+vLink+'">Bidang <b>'+vNilai[ i ][ 0 ]+'</b></a>';
        document.getElementById("txt2").innerHTML = 'Capaian : <b>'+vNilai[ i ][ 2 ] +'</b> %';
        document.getElementById("txt3").innerHTML = 'Jumlah Output : <b>'+vNilai[ i ][ 1 ]+'</b>';
        g1.refresh(vNilai[ i ][ 2 ]);
        setTimeout(f, 3000);
     })();
});
</script>

<div class="row">
    <div class="col-md-6">
        <div class="dsw-info-box">
          <span class="dsw-info-box-icon bg-white"><i class="fa fa-flash text-red"></i></span>
          <div class="marquee up">
          <?php
            $i=1;
            foreach ($d_news as $row) {
          ?>
          	<p style="color:red">
               <?php
               if ($row['link']) {echo '<a href="'.$row['link'] .'">'. $row['notif1'] .'</a>';}
               if ($row['file']) {
                   echo '<a href="'.base_url("files/news") .'/'. $row['file'] .'"  target=_blank>'. $row['notif1'] .'</a>';}
               ?>
            </p>
          <?php
              $i++;
          }
          ?>
         </div>
        </div>
    </div>

    <div class="col-md-6">
        <div class="hidden-xs dsw-info-box" style="padding-right:45px">
        <span class="dsw-info-box-icon bg-white"><i class="fa fa-rss text-orange"></i></span>
            <div id="text-carousel" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner" style="position:absolute; left:45px">

                    <?php
                    $i=1;
                    foreach ($d_rss as $row) {
                    ?>
                        <div class="item <?php if ($i==1) echo 'active'?>">
                            <div class="carousel-content dsw-info-box-content" style="margin-left:-10px">
                                <span class="dsw-info-box-text">
                                    <b><?php echo $row['site'] ?></b>  <?php echo $this->fc->idtgl($row['pubdate'],'full') ?>
                                </span>
                                <span class="dsw-info-box-text">
                                    <a href="<?php echo $row['link'] ?>" target="_blank"><?php echo $row['berita'] ?></a>
                                </span>
                            </div>
                        </div>
                    <?php
                        $i++;
                    }
                    ?>

                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-4">
        <div class="hidden-xs row" style="margin-bottom:0px;margin-top:-10px">
                <div class="col-sm-12">
                    <div style="padding:0px" id="g1"></div>
                    <div class="txt">
                        <a href=""><div class="txt1" id="txt1"> </div></a>
                        <div class="txt2" id="txt2"> </div>
                        <div class="txt3" id="txt3"> </div>
                    </div>
                </div>
        </div>
    </div>
    <div class="col-md-5">
        <div class="hidden-md hidden-sm hidden-xs" style="margin-bottom:5px;margin-top:5px">
            <a href="<?php echo site_url('quiz') ?>">
                <img class="attachment-img" height="80px" src="<?php echo base_url('files/images/head.png');?>">
            </a>
        </div>
    </div>
    <div class="col-md-3">
        <!-- <div class="dsw-info-box" style="background-color:#ECF0F5; box-shadow: 0 3px 3px -3px rgba(0, 0, 0, 0.2);"> -->
        <div class="dsw-info-box" style="background-color:#ECF0F5; box-shadow: 0 0px 0px rgba(0, 0, 0, 0.2);">
            <?php
            if ($absensi) {
              $i=1;
              foreach ($absensi as $row) {
                if ($i==1) { ?>
                    <?php if (strtotime($row['masuk']) < strtotime( substr($row['masuk'],0,11).'07:31:00' )) { $bgabsen='text-green'; } else { $bgabsen='text-red'; } ?>
                    <span class="dsw-info-box-icon2" style="background-color:#ECF0F5"> <a href="<?php echo site_url('absensi') ?>" style="color:white">
                        <i class="fa fa-clock-o <?php echo $bgabsen ?>"></i></a></span>
                    <div class="dsw-info-box-content">
                        <span class="dsw-info-box-text"><b><?php echo $this->fc->idtgl($row['tanggal'],'hari'); ?></b></span>
                        <span class="dsw-info-box-number text-yellow"><?php echo substr($row['masuk'],11,5) ?> |
                <?php } else { ?>
                            <span class="small text-muted"><?php echo substr($row['masuk'],11,5) ?> - <?php echo substr($row['keluar'],11,2) + 12; echo substr($row['keluar'],13,3) ?></span>
                        </span>
                <?php }
                $i++;
              } ?>
            </div>
            <?php } else { echo '&nbsp;'; } ?>
        </div>
    </div>
    <div class="col-md-3">
        <div class="dsw-info-box-content">
            <!-- <i class="fa fa-gift text-blue" style="font-size:25px"></i>&nbsp;&nbsp; -->
            <!-- <i class="fa fa-apple text-gray" style="font-size:25px"></i>&nbsp;&nbsp;
            <i class="fa fa-street-view text-gray" style="font-size:25px"></i>&nbsp;&nbsp; -->
        </div>
    </div>

</div>

<!-- <div class="box box-widget" style="margin-top:10px">
    <div class="box-header with-border">
        <h3 class="box-title"> Monitoring Capaian</h3>
        <div class="box-tools">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
        </div>
    </div>
    <div class="box-body" style="padding-top:5px">
        <div class="row">
            <?php foreach ($d_moncap as $row) { ?>
            <div class="col-sm-4 col-md-2">
                <?php
                    if ( $row['progres'] < 50 ) { $warna='red'; }
                    elseif ( $row['progres'] < 75 ) { $warna='yellow'; }
                    else { $warna='green'; }
                ?>
                <h4 class="control-sidebar-subheading" style="margin-bottom:3px; margin-top:3px"><a href="<?php echo site_url('fokus/bidang/'.$row['id_bidang']) ?>">Bidang <b><?php echo $row['id_bidang'] ?></b></a>
                    <span class="text-muted pull-right"><b><?php echo $row['jumlah'] ?></b> <span class="hidden-md">Out.</span>
                        <span class="small"> &nbsp;<?php echo $row['progres'] ?>%</span>
                    </span>
                </h4>
               <div class="progress xxs" style="margin-bottom:5px">
                  <div class="progress-bar progress-bar-<?php echo $warna ?>" role="progressbar" aria-valuemin="<?php echo $row['progres'] ?>" aria-valuemax="100" style="width:<?php echo $row['progres'] ?>%"></div>
               </div>


            </div>
            <?php } ?>
        </div>
    </div>
</div> -->

<div class="row">
    <div class="col-md-6">
      <?php
      if (stripos($this->session->userdata('idusergroup'), "010") !== false) { ?>
        <div class="box box-widget" style="margin-bottom:5px">
          <div class="" style="padding:5px">
              <a href="<?php echo site_url('stol/grid') ?>">
                  <img class="attachment-img" width="100%" src="<?php echo base_url('files/images/icon_stol.jpg');?>">
              </a>
          </div>
        </div>
        <div class="box box-widget" style="margin-bottom:5px">
          <div class="" style="padding:5px;">
              <a href="<?php echo site_url('rapat') ?>">
                  <img class="attachment-img" width="100%" src="<?php echo base_url('files/images/icon_RR.jpg');?>">
              </a>
          </div>
        </div>
        <div class="box box-widget" style="margin-bottom:5px">
          <div class="" style="padding:5px">
              <a href="<?php echo site_url('quiz') ?>">
                  <img class="attachment-img" width="100%" src="<?php echo base_url('files/images/icon_quiz.jpg');?>">
              </a>
          </div>
        </div>
      <?php } else { ?>
        <div class="box box-widget">
          <div class="" style="padding:5px">
              <a href="<?php echo site_url('perpustakaan') ?>">
                  <img class="attachment-img" width="100%" src="<?php echo base_url('files/images/head_perpus.png');?>">
              </a>
          </div>
        </div>

      <?php } ?>

        <!-- sementara dsw
        <div class="box box-widget" style="padding:5px">
            <video width="100%" src="http://intranet.anggaran.depkeu.go.id/files/film/Pengaduan.mov" controls loop></video>
        </div>
        -->
        <?php $this->load->view('main/dashboard_forum') ?>
    </div>

    <div class="col-md-6">
      <div class="box box-widget">
          <div class="bxslider">
              <?php foreach ($slideshow as $row) { ?>
              <li>
                  <a target='_blank' href="<?php echo $row['link'] ?>">
                      <img src="<?php echo site_url('files/slideshow').'/'. $row['gambar'] ?>"
                      title="<?php echo $row['keterangan'] ?>"/>
                  </a>
              </li>
              <?php } ?>
          </div>
      </div>
    </div>

    <div class="col-md-6">
        <div class="box box-widget">
            <div class="box-header with-border">
                <h3 class="box-title text"> Pengumuman</h3>
                <div class="small box-tools" style="padding-top:5px">
                    <a href="<?php echo site_url('home/pengumuman'); ?>"><i class="fa fa-link"></i></a>
                </div>
            </div>

            <?php
            $first = 1;
            foreach ($pengumuman as $row) {
                $arr = explode('<p>', $row['pengumuman']);
                $pengumuman = trim( $arr[1] );
                $lengkapnya = str_replace($pengumuman, '', $row['pengumuman']);

                if ($first==1) {
                    $first++; ?>

                    <!-- pengumuman terbaru -->
                    <div class="box-body" style="background-color:#F7F7F7" >
                        <div class="attachment-block clearfix" style="padding:0px; margin-bottom:0px">
                            <?php
                            if ($row['gambar']) {
                                echo '<img class="attachment-img" src="'. base_url("files/pengumuman") .'/'. $row['gambar'] .'">';
                            } else {
                                echo '<img class="attachment-img" src="'. base_url("files/images/depkeu_news.png") .'">';
                            }
                            ?>
                            <div class="attachment-pushed">
                                <span class="username">
                                    <b>Sekretariat</b>
                                    <span class="small text-muted pull-right"><?php echo $this->fc->idtgl($row['tglrekam'], 'hari') ?></span>
                                </span>
                                <h4 class="attachment-heading"><a href="#" onclick="showHide('<?php echo 'hidden_div'.$row['idpengumuman'] ?>'); return false;"><?php echo $row['judul'] ?></a>
                                </h4>

                                <div class="attachment-text">
                                    <?php
                                    echo $pengumuman ;
                                    echo '<div id="'. 'hidden_div'.$row['idpengumuman'] .'" style="display: none;">'. $lengkapnya .'</div>';
                                    if ($row['file']) {
                                        echo '<i class="small">File : </i>
                                        <a href="'.base_url("files/pengumuman") .'/'. $row['file'] .'"  target=_blank> &nbsp;<i class=" small fa fa-file-pdf-o"></i> <i class="small">'. $row['file'] .'</a></i>';
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- <div class="<?php if ($first==2) echo 'box-footer'; ?> box-comments"> -->
                    <div class="box-footer box-comments" style="background:white">
                <?php } else { ?>

                        <div class="box-comment">
                            <img class="img-circle img-sm img-bordered-dsw" src="<?php echo base_url("files/images/depkeu_round.png") ?>">
                            <div class="comment-text">
                                <span class="username">
                                    Sekretariat
                                    <span class="text-muted pull-right"><?php echo $this->fc->idtgl($row['tglrekam'], 'hari') ?></span>
                                </span>
                                <a href="#" onclick="showHide('<?php echo 'hidden_div'.$row['idpengumuman'] ?>'); return false;"><?php echo $row['judul'] ?></a><br>
                                <?php
                                if ($row['gambar']) {
                                    echo '<img style="width:95px !important; height:75px !important; margin: 5px 10px 5px 0px" src="'. base_url("files/pengumuman") .'/'. $row['gambar'] .'">';
                                }
                                echo $pengumuman ;
                                echo '<div id="'. 'hidden_div'.$row['idpengumuman'] .'" style="display: none;">'. $lengkapnya .'</div>';
                                if ($row['file']) {
                                    echo '<i class="small">File : </i>
                                    <a href="'.base_url("files/pengumuman") .'/'. $row['file'] .'" target=_blank><i class=" small fa fa-file-pdf-o"></i> <i class="small">'. $row['file'] .'</a></i>';
                                }
                                ?>
                            </div>
                        </div>

                <?php $first++;
                }
           } ?>
                    </div>

            <div class="box-footer">
                <div class="small text-center">
                    <a href="<?php echo site_url('home/pengumuman') ?>">Tampilkan Seluruh Pengumumam </a>
                </div>
            </div>
        </div>

        <!-- Tutup Surat -->
        <!-- <div class="box box-widget">
            <div class="box-header">
                <h3 class="box-title"> Surat Terkini</h3>
                <div class="box-tools">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div>
            </div>

            <?php
            foreach ($d_surat as $row) {
                $golsurat = 'ND-';
                if ($row['golsurat']=='1' and $row['jenis']=='10') $golsurat = 'RSM-';
                if ($row['golsurat']=='2') $golsurat = 'UND-';
            ?>
                <div class="box-footer box-comments" style="background:white; padding-bottom:0px">
                    <div class="box-comment">
                        <img class="img-circle img-sm img-bordered-dsw" src="<?php echo base_url("files/images/depkeu_round.png") ?>">
                        <div class="comment-text">
                            <span class="username">
                                No Agd : <?php echo $golsurat . $row['noagenda'] . $row['noagenda1'] ?>
                                <?php
                                if ( date('d-m-Y', strtotime($row['tgagenda']))== date('d-m-Y') ) {
                                    echo '<span class="text-muted pull-right">'. substr($row['tgagenda'],11,5) .' WIB</span>';
                                } else {
                                    echo '<span class="text-muted pull-right">'. $this->fc->idtgl( $row['tgagenda'], 'hr' ) .'</span>';
                                } ?>
                            </span>
                            <span class="small text-info"><?php echo $row['uraian'] ?></span><br>
                            <?php echo $row['perihal'] ?>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div> -->
    </div>
</div>




<script type="text/javascript">
    function showHide(obj) {
        var div = document.getElementById(obj);
            if (div.style.display == 'none') {
            div.style.display = '';
        } else {
            div.style.display = 'none';
        }
    }
</script>
