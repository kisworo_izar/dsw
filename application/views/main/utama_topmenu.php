<header class="main-header">
    <!-- Logo -->
    <a href="<?=base_url('home');?>" class="logo">
        <span class="logo-mini"><b>DSW</b></span>
        <span class="logo-lg"><b>DSW</b> Kemenkeu</span>
    </a>

    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top" role="navigation">
    <!-- <div class="container-fluid" style="padding-left:0px"> -->
        <!-- Sidebar toggle button-->
        <a href=" " class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Navigasi</span>
        </a>

        <div class="navbar-custom-menu"><ul class="nav navbar-nav">
            <li style="margin-top: 4px">
                <div class="pull-right">
                    <h3 style="padding: 10px; margin:0px; color: white">T.A. <b> <?php echo $this->session->userdata('thang')?></b></h3>
                </div>
            </li>

             <li id="dsw_notif" class="dropdown messages-menu"></li>

            <!-- user account menu -->
            <li class="dropdown user user-menu">
                <!-- menu toggle button -->
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <!-- user image in the navbar -->
                    <?php
                        $foto_profile="files/profiles/_noprofile.png";
                        if (file_exists("files/profiles/".$this->session->userdata('nip').".gif")) {$foto_profile =  "files/profiles/".$this->session->userdata('nip').".gif";}
                    ?>
                    <img src="<?php echo site_url($foto_profile); ?>" class="user-image img-bordered-dsw" />
                    <!-- hides the user name on small device -->
                    <span class="hidden-xs"><?php echo $this->session->userdata('nmuser') ?></span>
                </a>
                <ul class="dropdown-menu">
                    <!-- user image in the menu -->
                    <li class="user-header">
                        <img src="<?php echo site_url($foto_profile); ?>" class="img-circle" />
                        <p>
                            <?php echo $this->session->userdata('nmuser') ?>
                            <small><?php echo $this->session->userdata('jabatan') ?> </small>
                            <small><?php echo $this->session->userdata('nmso') ?> </small>
                        </p>
                    </li>

                    <!-- menu body -->
                    <?php  if (substr($this->session->userdata('idusergroup'),0,1) <> '6') { ?>

                       <li class="user-body">
                           <div class="col-xs-12 text-center">
                               <a href="<?php echo base_url("Contact") ?>">Pegawai DJA</a>
                           </div>

                       </li>
                    <?php } ?>

                    <!-- menu footer -->
                    <li class="user-footer">
                        <div class="pull-left">
                            <a href="<?php echo base_url("Profile") ?>" class="btn btn-default btn-flat">Profile</a>
                        </div>
                        <div class="pull-right">
                            <a href="<?php echo site_url("login/logout_user") ?>" class="btn btn-default btn-flat">Sign Out</a>
                        </div>
                    </li>
                </ul>
            </li>

            <!-- control sidebar toggle button -->
            <li>
                <a href="<?php echo site_url("login/logout_user") ?>" data-toggle="control-sidebar"><i class="fa fa-sign-out"></i></a>
            </li>
        </ul>
     </div><!-- /.navbar-collapse -->
    <!-- </div> -->
    </nav>
</header>

<?php  if (substr($this->session->userdata('idusergroup'),0,1) <> '6') { ?>

   <script>
       function auto_load(){
           $.ajax({
               url: "<?php echo site_url('home/notif') ?>",
               cache: false,
               success: function(data){
                   $("#dsw_notif").html(data);
               }
           });
       }

       $(document).ready(function(){
       auto_load();
       });
       setInterval(auto_load,"<?php echo rand(180000,240000) ?>");
   </script>

<?php } ?>
