<meta charset="UTF-8">
<title>DSW Kemenkeu</title>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<link rel="icon" href="<?=base_url('files/favicon.ico');?>" />
<link href="<?=base_url('assets/bootstrap/css/bootstrap.css');?>" rel="stylesheet" type="text/css" />
<link href="<?=base_url('assets/plugins/fontawesome/css/font-awesome.min.css');?>" rel="stylesheet" type="text/css" />
<link href="<?=base_url('assets/plugins/ionicons/css/ionicons.min.css');?>" rel="stylesheet" type="text/css" />
<link href="<?=base_url('assets/dist/css/AdminLTE.css');?>" rel="stylesheet" type="text/css" />
<link href="<?=base_url('assets/dist/css/skins/skin-blue.css');?>" rel="stylesheet" type="text/css" />
<link href="<?=base_url('assets/dist/css/skins/skin-danwa.css');?>" rel="stylesheet" type="text/css" />
<link href="<?=base_url('assets/dist/css/spinner.css'); ?>" rel="stylesheet" type="text/css" >

<script src="<?=base_url('assets/plugins/jQueryUI/jQuery-2.1.3.min.js');?>" type="text/javascript"></script>
<script src="<?=base_url('assets/bootstrap/js/bootstrap.min.js');?>" type="text/javascript"></script>
<script src='<?=base_url('assets/plugins/fastclick/fastclick.min.js');?>'></script>
<!-- <script src="<?=base_url('assets/dist/js/app.min.js');?>" type="text/javascript"></script> -->
<script src="<?=base_url('assets/dist/js/app.d.js');?>" type="text/javascript"></script>
