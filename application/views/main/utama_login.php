<!DOCTYPE html>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<!-- <link rel="icon" type="image/png" href="favicon.ico"> -->
   <style media="screen">
      select {-webkit-appearance: none; -moz-appearance: none; text-indent: 20px; text-overflow: ''; }
      .putih{color:#FFFFFF} .merah{color:yellow}
   </style>
<title>DSW Kemenkeu</title>

<head>
   <link href="<?=base_url('assets/bootstrap/css/bootstrap.css');?>" rel="stylesheet" type="text/css" />
   <link href="<?=base_url('assets/dist/css/AdminLTE.min.css');?>" rel="stylesheet" type="text/css" />
   <link href="<?=base_url('assets/dist/css/now-ui-kit.css?v=1.2.0');?>" rel="stylesheet" type="text/css" />
   <script src="<?=base_url('assets/dist/js/jquery.min.js');?>" type="text/javascript"></script>
</head>

<body style="background:#1E5595">
     <div class="login-box" style="vertical-align: middle;margin-top:115px; background:transparent">
        <div class="text-center">
           <div class="logo-mini" style="padding-bottom: 5px;" ><img src="<?php echo site_url('/files/images/logo_trans.png'); ?>"   width= "70px"/>
           </div>
          <div style="margin-bottom:0px; font-family:Arial; font-size:23px">
             <a style="color:#FFF" href="<?php echo base_url() ?>"><b> DSW</b></a></div>
          <!-- <div class="small" style="color:white; padding-top: 0px;">Sistem Aplikasi Terpadu</div> -->
          <div class="small" style="color:white; padding-top: 0px;">Kementerian Keuangan RI</div>
        </div>

         <div class="" style="padding:40px;";>
             <div class="form-group has-feedback no-border">
                 <input type="text" class="form-control" id="user" placeholder="User ID" autocomplete="off" style="color:white" />
                 <span class="fa fa-male form-control-feedback"></span>
             </div>

             <div class="form-group has-feedback no-border">
                 <input type="password" class="form-control" id="pass" placeholder="Password" autocomplete="off" style="color:white"/>
                 <span class="fa fa-key form-control-feedback"></span>
             </div>

             <div class="has-feedback no-border">
               <select class="form-control" id="thang" style="color:orange; border: 0px;text-align: center;text-align-last: center; cursor: pointer">
                 <option value="2021" selected>&nbsp;Tahun Anggaran 2021</option>

               </select>
             </div>

             <div class="row">
                 <div class="col-xs-12">
                     <button type="submit" class="btn btn-block btn-info btn-round btn-flat"> <b> Login</b></button>
                 </div>
                 <div class="col-xs-12" style="padding:7px">
                    <div class="text-center small">
                       <!-- <a href="<?php echo base_url() ?>login?q=605bf" style="color:white">Lupa Password</a> -->
                       <p class="infoLogin"></p>
                    </div>
                 </div>

             </div>
         </div>
     </div>

 </body>

<script type="text/javascript"> $(document).ready(function(){$(document).keypress(function (e) {if (e.which == 13) {$("button").click();}}); $("button").click(function(event){var user = $("#user").val(); var pass = $("#pass").val(); var thn  = $("#thang").val(); $("#user").click(function(event){$(".infoLogin").hide();}); $("#pass").click(function(event){$(".infoLogin").hide();}); if (user == "") {$(".infoLogin").hide().html('<span class="putih"> Silahkan masukan User ID Anda</span>').fadeIn(300);} else if (pass == "") {$(".infoLogin").hide().html('<span class="putih"> Silahkan masukan Password Anda</span>').fadeIn(300);} else {$.ajax({url: '<?php echo base_url() ?>login/login_user', type: 'POST', data: {iduser: user, password: pass, thang: thn},}) .done(function(str) {if (str == 1) {$(".infoLogin").hide().html('<span style="color:#D2D6DE"> Login ... </span>').fadeIn(200).fadeOut(300); setTimeout(function(){window.location = "<?php echo base_url() ?>home?q=RrPW9";}, 300); } else if (str == 0) {$(".infoLogin").hide().html('<span class="merah"> User ID dan Password tidak terdaftar</span>').fadeIn(300);}}) .fail(function() {}) .always(function() {});}})});</script>
