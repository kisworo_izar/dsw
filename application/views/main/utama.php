<!DOCTYPE html>
<html>


<head><?php $this->load->view('main/utama_link'); ?></head>

<body class="skin-danwa fixed sidebar-mini">
   <script>
    (function () {
       if (Boolean(sessionStorage.getItem('sidebar-toggle-collapsed'))) {
        var body = document.getElementsByTagName('body')[0];
        body.className = body.className + ' sidebar-collapse';
       }
    })();
   </script>

    <div class="wrapper">
        <?php if (! isset($view)) $view = $bread['view']; ?>
        <?php $this->load->view('main/utama_topmenu'); ?>
        <?php $this->load->view('main/utama_sidebar'); ?>
        <div class="content-wrapper">
            <section class="content-header">
            <h1><?php echo $bread['header'] ?></h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
                <li class="active"><?php echo $bread['subheader'] ?></li>
            </ol>
            </section>

            <section class="content">
               <?php
                   if (! isset($view)) $view = $bread['view'];
                   $this->load->view( $view );
               ?>
            </section>
        </div>

        <footer class="main-footer">
            <div class="pull-right hidden-xs small">
                Load in : {elapsed_time} Sec, Memory : {memory_usage}
            </div>
            <span><b>DSW</b> &copy; Kementerian Keuangan RI</span>
        </footer>
    </div>

</body>

</html>

<script type="text/javascript">
	$(document).ready(function() {
		$(".lds-ripple").addClass('hide');
	});

   $('.sidebar-toggle').click(function(event) {
     event.preventDefault();
     if (Boolean(sessionStorage.getItem('sidebar-toggle-collapsed'))) {
      sessionStorage.setItem('sidebar-toggle-collapsed', '');
     } else {
      sessionStorage.setItem('sidebar-toggle-collapsed', '1');
     }
   });
</script>
