<!DOCTYPE html>
<html>
    <head>
        <?php $this->load->view('main/utama_link'); ?>
        <link href="<?=base_url('assets/plugins/iCheck/square/blue.css');?>" rel="stylesheet" type="text/css" />
        <script src="<?=base_url('assets/plugins/iCheck/icheck.min.js');?>" type="text/javascript"></script>
    </head>
    <body class="login-page bg-dsw">
        <div class="login-box">
            <div class="login-logo" style="margin-bottom:15px">
                <a class="text-blue" href="<?php echo base_url() ?>">
                    <img src="<?php echo base_url() ?>files/images/depkeu_round.png" width="45px"/>
                    <b>DJA</b> <smal class="small">single window</smal></a>
            </div>
            <div class="login-box-body dsw-border" style="padding-bottom:15px">
                <p class="login-box-msg">Silahkan masukkan User ID dan password</p>
                <div class="form-group has-feedback">
                    <input type="text" class="form-control" id="user" placeholder="User ID" />
                    <span class="glyphicon glyphicon-user form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback">
                    <input type="password" class="form-control" id="pass" placeholder="Password" />
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                </div>
                <div class="row">
                    <div class="col-xs-8">
                        <span class="infoLogin"></span>
                    </div>
                    <div class="col-xs-4">
                        <button type="submit" class="btn btn-primary btn-block btn-flat">Login</button>
                    </div>
                </div>
                <a href="#">Lupa password</a>
                <div class="small" style="margin-top:5px">
                    DSP <strong> <a href="http://www.anggaran.depkeu.go.id">DJA</a></strong> © 2015
                </div>
            </div>
        </div>
    </body>
    <script type="text/javascript">
        $(document).ready(function(){
            $(document).keypress(function (e) {
                if (e.which == 13) {
                    $("button").click();
                }
            });
            $("button").click(function(event){
                var user = $("#user").val();
                var pass = $("#pass").val();

                $("#user").click(function(event){
                    $(".infoLogin").hide();
                });
                $("#pass").click(function(event){
                    $(".infoLogin").hide();
                });

                if (user == "") {
                    $(".infoLogin").hide().html('<small class="text-danger"> * Silahkan isikan User ID Anda </small>').fadeIn(1000);
                } else if (pass == "") {
                    $(".infoLogin").hide().html('<small class="text-danger"> * Silahkan isikan Password Anda </small>').fadeIn(1000);
                } else {
                    $.ajax({
                        url: '<?php echo base_url() ?>login/login_user',
                        type: 'POST',
                        data: {iduser: user, password: pass},
                    })
                    .done(function(str) {
                        if (str == 1) {
                            $(".infoLogin").hide().html('<small class="text-success"> Redirecting ... </small>').fadeIn(1000).fadeOut(2000);

                            setTimeout(function(){
                                window.location = "<?php echo base_url() ?>home";
                            }, 2000);
                        } else if (str == 0) {
                            $(".infoLogin").hide().html('<small class="text-danger"> * User ID tidak terdaftar</small>').fadeIn(1000);
                        }
                        // console.log("success");
                    })
                    .fail(function() {
                        // console.log("error");
                    })
                    .always(function() {
                        // console.log("complete");
                    });
                }
            })
        });
    </script>
</html>
