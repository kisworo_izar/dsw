<script src="<?=base_url('assets/plugins/jQuery/jQuery.Shorten.1.0.js'); ?>" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $(".more").shorten({
            "showChars" : 150,
            "moreText"  : "<i class='small'> Read More</i>",
            "lessText"  : " <i class='small'> Less</i>"
        });

        $(".lengkap").shorten({
            "showChars" : 100,
            "moreText"  : "<i class='small'> Read More</i>",
            "lessText"  : " <i class='small'> Less</i>"
        });
    });
</script>


<div class="box box-widget" style="margin-bottom:0px;">
    <div class="box-header with-border">
        <!-- <h3 class="box-title" style="color:#DD4B39">Top Thread</h3> -->
        <span class="box-title">Diskusi DJA</span>
        <div class="small box-tools" style="padding-top:5px">
            <a href="<?php echo site_url('forum'); ?>"><i class="fa fa-link"></i></a>
        </div>
    </div>
</div>

<div class="box box-widget" style="margin-bottom:0px;">
    <div class="box-header with-border" style="border-radius:0px;border-left: 1px solid #DD4B39; padding-top:3px; padding-bottom:3px; background-color:#F7F7F7">
        <span class="box-title" style="color:#DD4B39">Top Thread</span>
    </div>
    <div class="box-body" style="padding:0px 10px 0px 10px">
        <ul class="products-list product-list-in-box">

            <?php foreach ($d_top as $row) { ?>
            <li class="item" style="padding:3px">
                <div class="product-info" style="margin-left:0px">
                    <span class="small product-description">
                        <a href="<?php if ($this->session->userdata('idusergroup')!='999') echo site_url('forum/forum_post').'/'. $row['idparent'] .'/'. $row['idchild'] ?>"><?php echo $row['thread'] ?></a>
                        <span class="small text text-danger pull-right">Replies : <b><?php echo $row['reply']-1 ?></b></span>
                    </span>
                </div>
            </li>
            <?php } ?>

        </ul>
    </div>
</div>

<div class="box box-widget">
    <div class="box-header with-border" style="border-radius:0px;border-left: 1px solid orange; padding-top:3px; padding-bottom:3px; background-color:#F7F7F7">
        <h3 class="box-title" style="color:orange">Last Post</h3>
    </div>

    <?php foreach ($d_forum as $row) { ?>
        <div class="box box-widget">
            <div class="box-header with-border">
                <div class="user-block">
                    <?php
                        $foto_profile="files/profiles/_noprofile.png";
                        if (file_exists("files/profiles/".$row['nip'].".gif")) {$foto_profile =  "files/profiles/".$row['nip'].".gif";}
                    ?>
                    <!-- <img class="img-circle img-bordered-dsw" src="<?php echo site_url($foto_profile); ?>"> -->

                    <span class="profile-tooltip">
                        <img class="img-circle img-bordered-dsw" src="<?php echo site_url($foto_profile); ?>" alt="user image">
                            <span class="profile-tooltip-content"><img src="<?php echo site_url($foto_profile); ?>">
                            <span class="profile-tooltip-text" style="font-size:18px; padding-top:5px">
                                <?php echo $row['nmuser'] ?>
                            </span><br>
                            <span class="profile-tooltip-text"><?php echo trim($row['jabatan']) ?> </span><br>
                            <span class="profile-tooltip-text"><?php echo $row['nmso'] ?></span>
                        </span>
                    </span>



                    <span class="username">

                        <span class="username" style="margin-left:0px">
                            <span class="profile-tooltip">
                                <span class="profile-tooltip-item"><?php echo $row['nmuser'] ?> </span>
                            </span>
                        </span>

                    </span>
                    <span class="description"><?php echo '<b>'. $row['nmhome'] .'</b> - '. $this->fc->idtgl( $row['tglcreate'], 'full') ?></span>
                </div>
                <div class="box-tools">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <!-- <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button> -->
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <h4 class="attachment-heading" style="margin-top:0px; ">
                    <a href="<?php echo site_url('forum/forum_post').'/'. $row['idparent'] .'/'. $row['idchild'] ?>"><?php echo $row['judul'] ?></a>
                </h4>
                <?php if ($row['gambar']) { ?>
                    <img width="180px" class="img-responsive pad" src="files/forum/<?php echo $row['gambar'] ?>" style="float:left;padding-left:0px; padding-top:0px">
                <?php } ?>
                <span class="comment">
                    <?php
                        echo '<span class="">'. trim($row['post']) .'</span>';
                        if ($row['attach']) {
                            echo "<br><i class='small'>File : </i><br>";
                            $arr = explode(';', trim($row['attach']));
                            for ($i=0; $i<count($arr); $i++) {
                                $nmfile  = $arr[$i];
                                $urlfile = site_url("files/forum/$nmfile");
                                if ( $i<count($arr)-1 ) $nmfile .= " ";
                                echo "<i class='small'><a href='$urlfile' target='_blank'>- $nmfile </a></i><br>";
                            }

                            // echo '<br><i class="small">File : </i><br>
                            // <a href="'.base_url("files/forum") .'/'. $row['attach'] .'"  target=_blank> &nbsp;<i class=" small fa fa-file-pdf-o"></i> <i class="small">- '. $row['attach'] .'</a></i><br>';
                        }
                    ?>
                </span>
            </div>
            <?php if ($row['jumlah']>4 and $this->session->userdata('idusergroup') != '999') { ?>
            <div class="box-footer">
                <div class="small text-center">
                    <a href="<?php echo site_url('forum/forum_post').'/'. $row['idparent'] .'/'. $row['idchild'] ?>">Tampilkan Seluruh Tanggapan </a>
                </div>
            </div>
            <?php } ?>
            <div class="box-footer box-comments">

            <?php if ($row['thread']) {
                foreach ($row['thread'] as $col) { ?>
                    <div class="box-comment">
                        <!-- User image -->
                        <?php
                            $foto_profile="files/profiles/_noprofile.png";
                            if (file_exists("files/profiles/".$col['nip'].".gif")) {$foto_profile =  "files/profiles/".$col['nip'].".gif";}
                        ?>

                        <span class="profile-tooltip">
                            <img class="img-circle img-bordered-dsw" src="<?php echo site_url($foto_profile); ?>">
                                <span class="profile-tooltip-content"><img src="<?php echo site_url($foto_profile); ?>">
                                <span class="profile-tooltip-text" style="font-size:18px; padding-top:5px">
                                    <?php echo $col['nmuser'] ?>
                                </span><br>
                                <span class="profile-tooltip-text"><?php echo trim($col['jabatan']) ?> </span><br>
                                <span class="profile-tooltip-text"><?php echo $col['nmso'] ?></span>
                            </span>
                        </span>


                        <div class="comment-text">
                            <span class="username">
                                <span class="profile-tooltip">
                                    <span class="profile-tooltip-item"><?php echo $col['nmuser'] ?> </span>
                                </span>

                                <span class="text-muted pull-right"><?php echo $this->fc->idtgl( $col['tglpost'], 'full') ?></span>
                            </span>
                            <div>
                                <?php
                                    echo '<span class="comment more">'. trim($col['post']) .'</span>';
                                    if ($col['attach']!='') {
                                        echo "<br><i class='small'>File : </i>";
                                        $arr = explode(';', trim($col['attach']));
                                        for ($i=0; $i<count($arr); $i++) {
                                            $nmfile  = $arr[$i];
                                            $urlfile = site_url("files/forum/$nmfile");
                                            if ( $i<count($arr)-1 ) $nmfile .= ",";
                                            echo "<i class='small'><a href='$urlfile' target='_blank'>$nmfile </a></i>";
                                        }
                                    }
                                ?>
                            </div>
                        </div>
                    </div>
            <?php
                }
            } ?>
            </div>

            <?php if ($this->session->userdata('idusergroup') != '999') { ?>
            <div class="box-footer">
                <form action="<?php echo site_url('home/tanggapan/'. $row['idparent'] .'/'. $row['idchild']) ?>" method="post">
                    <img class="img-responsive img-circle img-sm img-bordered-dsw" src=
                    <?php
                        $foto_profile="files/profiles/_noprofile.png";
                        if (file_exists("files/profiles/".$this->session->userdata('nip').".gif")) {$foto_profile =  "files/profiles/".$this->session->userdata('nip').".gif";}
                        echo $foto_profile;
                    ?> alt=<?php $this->session->userdata('nmuser')?>>
                    <div class="form-group img-push">
                        <div class="input-group">
                            <input type="text" name="comment" class="form-control input-sm" value="" placeholder="Tanggapan ...">
                            <div class="input-group-addon"><a href="#" onclick="showHide('forum-upload'); return false;"><i class="fa fa-paperclip"></i></a></div>
                        </div>
                        <div id="forum-upload" style="display: none;">
                            <input type="hidden" id="nmfile" name="nmfile" value="" />
                            <input type="file" id="files" name="files[]" multiple class="file" data-overwrite-initial="true" data-min-file-count="1" />
                        </div>
                    </div>
                </form>
            </div>
            <?php } ?>
        </div>
    <?php } ?>

</div>

<script type="text/javascript">
    function showHide(obj) {
        var div = document.getElementById(obj);
            if (div.style.display == 'none') {
            div.style.display = '';
        } else {
            div.style.display = 'none';
        }
    }
</script>

<link href="<?php echo base_url(); ?>assets/plugins/uploadfile/fileinput.css" rel="stylesheet">
<script src="<?php echo base_url();?>assets/plugins/uploadfile/fileinput.min.js" type="text/javascript"></script>

<script type="text/javascript">
    $("#files").fileinput({
        uploadUrl: "<?php echo site_url('home/fileupload') ?>",
        allowedFileExtensions : ['jpg','jpeg','png','gif','xls','xlsx','doc','docx','ppt','pptx','rtf','pdf'],
        overwriteInitial: true,
        maxFileSize: 9000000,
        maxFilesNum: 10,
        minFileCount: 1,
        maxFileCount: 5,
        dropZoneTitle: 'Drag & Drop file Image / Pdf / Xls / Doc disini ...',
        msgInvalidFileExtension: "Invalid extension file {name}. Hanya {extensions} files yang bisa diproses ...",
        slugCallback: function(filename) {
            return filename.replace('(', '_').replace(']', '_');
        }
    });

    $('#files').on('fileuploaded', function(event, data, previewId, index) {
        var files = document.getElementById("files").files;
        var names = "";
        for (var i = 0; i < files.length; i++)
            if (i < files.length-1) { names += files[i].name + ';'; }
            else { names += files[i].name; }

        document.getElementById("nmfile").value = names;
    });
</script>
