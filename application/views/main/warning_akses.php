<!DOCTYPE html>
<html lang="en">
<head>
    <!--?php $this->load->view('main/f_headlink'); ?-->
</head>
<body>

    <div id="WarnModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title"><span class="glyphicon glyphicon-minus-sign"></span> Peringatan !</h4>
                </div>
                <div class="modal-body">
                    <p>
                      <?php 
                        if ($warn=='Admin-Only') echo "Mohon maaf ! Demi keamanan Database sistem<br>Kewenangan ini hanya bisa dilakukan oleh <b>Admininstrator</b>."; 
                        if ($warn=='Read-Only')  echo "Anda tidak mempunyai akses untuk <b>Rekam</b> / <b>Ubah</b> / <b>Hapus</b> Data ini<br>Mohon hubungi Admininstrator."; 
                      ?>
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Tutup</button>
                </div>
            </div>
        </div>
    </div>
    
</body>
<script>
    $(document).ready(function(){
        $("#WarnModal").modal('show');
    });
</script>
</html>
