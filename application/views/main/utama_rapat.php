<!DOCTYPE html>
<html>
<head><?php $this->load->view('main/utama_link'); ?></head>
<body>
    <section class="">
        <?php $this->load->view( $view ); ?>
    </section>

    <footer class="main-footer" style="margin-left:0px">
        <div class="pull-right hidden-xs small">
            Load in : {elapsed_time} Sec, Memory : {memory_usage}
        </div>
        DSP <a href="http://www.anggaran.depkeu.go.id">DJA</a> &copy; 2015
    </footer>
</body>
</html>
