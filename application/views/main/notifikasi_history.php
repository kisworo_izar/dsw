<div class="row">
  <div class="col-md-12">
    <!-- The time line -->
    <ul class="timeline">
      <!-- timeline time label -->
      <?php foreach ($notifs as $notif) { ?>
        <li>
          <i class="fa fa-envelope bg-blue"></i>

          <div class="timeline-item">
            <span class="time"><i class="fa fa-clock-o"></i><?= $notif['waktu'] ?></span>

            <h3 class="timeline-header"><a href="<?= $notif['link'] ?>"><?= $notif['judul'] ?></a></h3>

            <div class="timeline-body">
              <?= $notif['pesan'] ?>
            </div>
          </div>
        </li>  
      <?php } ?>
      
    </ul>
  </div>
  <!-- /.col -->
</div>