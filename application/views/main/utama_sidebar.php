<aside class="main-sidebar">
  <section class="sidebar">

    <ul class="sidebar-menu">
      <li class="header text-center" style="color:#FFFFFF; font-size: 13px">Kementerian Keuangan RI</li>

    <?php if (! isset($menu)) $menu = $this->dsw_model->get_menu( $this->uri->segment(1) ); ?>

    <?php
    $arr = array();
    if ($menu) {
      foreach ($menu as $row) {
        if ( $row['lvl']==3) {
          $key = substr($row['idmenu'],0,4);
          if ( ! array_key_exists($key, $arr) ) $arr[ $key ] = '';
          $arr[ $key ] .=
              '<li class="'. $row['active'] .'"><a href="'. site_url($row['link']) .'">
                 <i class="fa fa-'. $row['icon'] .'"></i>'. $row['menu'] .
                '</a>
               </li>';
        }
      }

      $sub = array();
      foreach ($menu as $row) {
        if ( $row['lvl']==2) {
          $key = substr($row['idmenu'],0,2);
          if ( ! array_key_exists($key, $sub) ) $sub[ $key ] = '';

          $alm = substr($row['idmenu'],0,4);
          if ( array_key_exists($alm, $arr) ) {
            $sub[ $key ] .=
              '<li class="treeview '. $row['active'] .'">
                 <a href="'. site_url($row['link']) .'">
                   <i class="fa fa-'. $row['icon'] .'"></i>
                   <span>'. $row['menu'] .'</span>
                   <i class="fa fa-angle-left pull-right"></i>
                 </a>
                 <ul class="treeview-menu">'. $arr[ $alm ] .'</ul>
               </li>';
          } else {
            $sub[ $key ] .=
              '<li><a href="'. site_url($row['link']) .'">
                 <i class="fa fa-'. $row['icon'] .'"></i>'. $row['menu'] .
                '</a>
               </li>';
          }
        }
      }

      foreach ($menu as $row) {
        if ( $row['lvl']==1) {
          $key = substr($row['idmenu'],0,2);
          if ( array_key_exists($key, $sub) ) {
            echo
              '<li class="treeview '. $row['active'] .'">
                 <a href="'. site_url($row['link']) .'">
                   <i class="fa fa-'. $row['icon'] .'"></i>
                   <span>'. $row['menu'] .'</span>
                   <i class="fa fa-angle-left pull-right"></i>
                 </a>
                 <ul class="treeview-menu">'. $sub[ $key ] .'</ul>
               </li>';
          } else {
            echo
              '<li class="'. $row['active'] .'">
                 <a href="'. site_url($row['link']) .'">
                   <i class="fa fa-'. $row['icon'] .'"></i>
                   <span>'. $row['menu'] .'</span>
                 </a>
               </li>';
          }
        }
      }
    }
    echo '</ul>';
    ?>

  </section>
</aside>
