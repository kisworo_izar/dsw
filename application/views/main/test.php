        <?php foreach ($d_forum as $row) { ?>
            <div class="box box-widget">
                <div class="box-header with-border">
                    <div class="user-block">
                        <?php
                            $foto_profile="files/profiles/_noprofile.png";
                            if (file_exists("files/profiles/".$row['nip'].".gif")) {$foto_profile =  "files/profiles/".$row['nip'].".gif";}
                        ?>
                        <img class="img-circle img-bordered-dsw" src="<?php echo site_url($foto_profile); ?>">
                        <span class="username">

                            <span class="username" style="margin-left:0px">
                                <span class="profile-tooltip">
                                    <span class="profile-tooltip-item"><?php echo $row['nmuser'] ?> </span>
                                        <span class="profile-tooltip-content"><img src="<?php echo site_url($foto_profile); ?>">
                                        <span class="profile-tooltip-text" style="font-size:18px; padding-top:5px">
                                            <?php echo $row['nmuser'] ?>
                                        </span><br>
                                        <span class="profile-tooltip-text"><?php echo trim($row['jabatan']) ?> </span><br>
                                        <span class="profile-tooltip-text"><?php echo $row['nmso'] ?></span>
                                    </span>
                                </span>
                            </span>

                        </span>
                        <span class="description"><?php echo '<b>'. $row['nmhome'] .'</b> - '. $this->fc->idtgl( $row['tglcreate'], 'full') ?></span>
                    </div>
                    <div class="box-tools">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        <!-- <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button> -->
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <h4 class="attachment-heading" style="margin-top:0px; ">
                    <!-- <i class="fa fa-comments-o"></i> -->
                    <a href="<?php echo site_url('forum/forum_post').'/'. $row['idparent'] .'/'. $row['idchild'] ?>"><?php echo $row['judul'] ?></h4></a>
                    <?php if ($row['gambar']) { ?>
                        <img width="180px" class="img-responsive pad" src="files/forum/<?php echo $row['gambar'] ?>" style="float:left;padding-left:0px; padding-top:0px">
                    <?php } ?>
                    <span class="comment">
                        <?php 
                            echo $row['post'];
                            if ($row['attach']) {
                                echo '<i class="small">File : </i>
                                <a href="'.base_url("files/forum") .'/'. $row['attach'] .'"  target=_blank> &nbsp;<i class=" small fa fa-file-pdf-o"></i> <i class="small">'. $row['attach'] .'</a></i>';
                            }
                        ?>
                    </span>
                </div>
                <?php if ($row['jumlah']>4 and $this->session->userdata('idusergroup') != '999') { ?>
                <div class="box-footer">
                    <div class="small text-center">
                        <a href="<?php echo site_url('forum/forum_post').'/'. $row['idparent'] .'/'. $row['idchild'] ?>">Tampilkan Seluruh Tanggapan </a>
                    </div>
                </div>
                <?php } ?>
                <div class="box-footer box-comments">

                <?php if ($row['thread']) {
                    foreach ($row['thread'] as $col) { ?>
                        <div class="box-comment">
                            <!-- User image -->
                            <?php
                                $foto_profile="files/profiles/_noprofile.png";
                                if (file_exists("files/profiles/".$col['nip'].".gif")) {$foto_profile =  "files/profiles/".$col['nip'].".gif";}
                            ?>
                            <img class="img-circle img-bordered-dsw" src="<?php echo site_url($foto_profile); ?>">
                            <div class="comment-text">
                                <span class="username">
                                    <span class="profile-tooltip">
                                        <span class="profile-tooltip-item"><?php echo $col['nmuser'] ?> </span>
                                            <span class="profile-tooltip-content"><img src="<?php echo site_url($foto_profile); ?>">
                                            <span class="profile-tooltip-text" style="font-size:18px; padding-top:5px">
                                                <?php echo $col['nmuser'] ?>
                                            </span><br>
                                            <span class="profile-tooltip-text"><?php echo trim($col['jabatan']) ?> </span><br>
                                            <span class="profile-tooltip-text"><?php echo $col['nmso'] ?></span>
                                        </span>
                                    </span>

                                    <span class="text-muted pull-right"><?php echo $this->fc->idtgl( $col['tglpost'], 'full') ?></span>
                                </span>
                                <div class="comment more">
                                    <?php 
                                        echo trim($col['post']); 
                                    ?>
                                </div>
                            </div>
                        </div>
                <?php
                    }
                } ?>
                </div>

                <?php if ($this->session->userdata('idusergroup') != '999') { ?>
                <div class="box-footer">
                    <form action="<?php echo site_url('home/tanggapan/'. $row['idparent'] .'/'. $row['idchild']) ?>" method="post">
                        <img class="img-responsive img-circle img-sm img-bordered-dsw" src=
                        <?php
                            $foto_profile="files/profiles/_noprofile.png";
                            if (file_exists("files/profiles/".$this->session->userdata('nip').".gif")) {$foto_profile =  "files/profiles/".$this->session->userdata('nip').".gif";}
                            echo $foto_profile;
                        ?> alt=<?php $this->session->userdata('nmuser')?>>
                        <div class="form-group img-push">
                            <div class="input-group">
                                <input type="text" name="comment" class="form-control input-sm" value="" placeholder="Judul thread Anda, maksimal 200 Karakter">
                                <div class="input-group-addon"><a href="#" onclick="showHide('forum-upload'); return false;"><i class="fa fa-external-link"></i></a></div>
                            </div>
                            <div id="forum-upload" style="display: none;">
                                <input type="file" id="files" name="files[]" multiple class="file" data-overwrite-initial="true" data-min-file-count="5" />
                            </div>
                        </div>
                    </div>
                        </div>
                    </form>
                </div>
                <?php } ?>
            </div>
        <?php } ?>


<script type="text/javascript">
    function showHide(obj) {
        var div = document.getElementById(obj);
            if (div.style.display == 'none') {
            div.style.display = '';
        } else {
            div.style.display = 'none';
        }
    }
</script>

<link href="<?php echo base_url(); ?>assets/plugins/uploadfile/fileinput.css" rel="stylesheet">
<script src="<?php echo base_url();?>assets/plugins/uploadfile/fileinput.min.js" type="text/javascript"></script>

<script type="text/javascript">
    $("#files").fileinput({
        uploadUrl: "<?php echo site_url('upload/fileupload') ?>", 
        allowedFileExtensions : ['jpg','png','xlsx','doc','docx','rtf','pdf'],
        overwriteInitial: true,
        maxFileSize: 100000,
        maxFilesNum: 10,
        minFileCount: 1,
        maxFileCount: 5,
        dropZoneTitle: 'Drag & Drop file Image / Pdf / Xls / Doc disini ...',
        msgInvalidFileExtension: "Invalid extension file {name}. Hanya {extensions} files yang bisa diproses ...",
        slugCallback: function(filename) {
            return filename.replace('(', '_').replace(']', '_');
        }
    });
</script>