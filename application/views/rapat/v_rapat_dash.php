<meta http-equiv="refresh" content="600; url=http://intranet.anggaran.depkeu.go.id/rapat">
<link href="<?=base_url('assets/plugins/NewsTicker/css/site.css'); ?>" rel="stylesheet" >
<script src="<?=base_url('assets/plugins/NewsTicker/scripts/jquery.bootstrap.newsbox.min.js'); ?>" type="text/javascript"></script>
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default" style="border-radius:0px;margin-bottom:0px">
			<div class="wrapper" style="background:#2A5F99;">
			    <header class="main-header">
					<img style="float:left;margin-left:23px;margin-top:7px" src="<?php echo site_url('files/images/logo_trans.png'); ?>" width= "35px" style="margin-top:7px"/>
					<div style="float:left;margin-left:-15px">
						<a href="" class="logo" style="color:#fff">
					        <span class="logo-lg" style="width:200px"><b>DSW</b> | Agenda Rapat</span>
					    </a>
					</div>
					<div style="float:right">
						<a href="" class="logo" style="color:#fff; padding-right:-10px">
					        <span id="tanggal1" class="logo-lg "><?php echo $this->fc->idtgl($prm['tanggal'], 'hari') ?></span>
					    </a>
					</div>
				</header>

			</div>
			<div class="row" style="background:#447AB2; border-bottom:1px solid #a8c7db">
				<div class="col-md-1 pull-right">
						<div class="input-group date" id="datetimepicker1" style="margin-right:12px">
							<input style="background:transparent;border:0px;color:transparent" type="" id="tanggal" class="form-control" value="<?php echo $this->fc->idtgl($prm['tanggal'], 'hari') ?>" >
							<span class="input-group-addon" style="background:transparent;border:transparent">
								<i class="fa fa-calendar" style="color:#fff"></i>
							</span>
						</div>
				</div>
			</div>

			<div class="panel-body" style="padding:0px">
				<div class="row">
					<div class="col-md-12">
						<ul class="rafat" style="border-right:solid 1px #BCDBED">

							<?php if ($rapat) {
	                          	foreach ($rapat as $row) { ?>

								<li class="news-item">
								    <table>
								        <tr>
											<th rowspan="2" style="font-family:Arial; font-weight:bold; font-size:50px; text-align:center; vertical-align:top; color:#085382" width="165px">
											  	<?php echo $this->fc->idtgl($row['tglawal'], 'jam') ?>
											</th>
											<th style="color:#105480;padding:7px 10px 0px 0px;vertical-align:top;" width="500px">
											  	<?php echo $row['agenda'] ?>
											</th>
											<th rowspan="2" width="200px">
												<?php
								                    if ($row['gambar']) {
								                        echo '<img width="160" style="border:solid 3px #E3E3E3" src="'. base_url("files/rapat") .'/'. $row['gambar'] .'">';
								                    }else {
						                                echo '<img width="160" style="border:solid 3px #E3E3E3" src="'. base_url("files/rapat/Rapat.JPG") .'">';
						                            }
												?>
											</th>
											<th width="200px" style="vertical-align:bottom;">
												<span style="color:#318BC0;"><?php echo $row['nmrapat']  ?></span>
											</th>
								        </tr>
								        <tr>
											<td style="color:#4e4e4e;vertical-align:bottom;" width="450px">
												<span style="color:#318BC0;"><?php echo $row['pic']  ?></span>

											</td>
											<td style="color:#4e4e4e;vertical-align:bottom;">
												<span style="color:#318BC0;"><?php echo $row['letak']  ?></span>
											</td>
								        </tr>
								    </table>
								</li>
							<?php
	                          }
	                        } else { ?>
	                            <tr><td colspan="4" class="text-danger"> Tidak ada agenda rapat hari ini</td></tr>
	                        <?php } ?>


						</ul>
					</div>


				</div>
			</div>
			<div class="panel-footer" style="padding:7px 7px 7px;margin-top:0px; background: #2A5F99"></span>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
    $(function () {
        $(".rafat").bootstrapNews({
            newsPerPage: 4,
            autoplay: true,
			pauseOnHover:true,
            direction: 'up',
            newsTickerInterval: 7000,
            onToDo: function () {
                //console.log(this);
            }
        });
    });
</script>

<link href="<?=base_url('assets/bootstrap/css/bootstrap-datetimepicker.min.css');?>" rel="stylesheet" type="text/css" />
<script src="<?=base_url('assets/plugins/datepicker/moment.min.js');?>" type="text/javascript"></script>
<script src="<?=base_url('assets/plugins/datepicker/id.js');?>" type="text/javascript"></script>
<script src="<?=base_url('assets/bootstrap/js/bootstrap-datetimepicker.min.js');?>" type="text/javascript"></script>


<script type="text/javascript">
    $('#datetimepicker1').datetimepicker({
        format : 'dddd, DD MMMM YYYY',
        locale : 'id'
    }).on('dp.change', function() {
		  var tgl;
		  tgl = document.getElementById("tanggal").value;
		  document.getElementById("tanggal1").innerHTML = tgl;
          window.location.href = "<?php echo site_url('rapat/cari?mod=dash&tgl=') ?>"+ tgl;
        });
</script>
