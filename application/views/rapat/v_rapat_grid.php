<script src="<?=base_url('assets/plugins/jQueryUI/jquery-ui.1.11.4.min.js'); ?>" type="text/javascript"></script>

<style media="screen">
  th {
      text-align: center;
    }
    thead tr {
      color: #fff;
      background: #00acd6;
    }

    #iGrid .ui-selecting { background: #FECA40; }
    #iGrid .ui-selected { background: #F39814; color: white; }
</style>

<div class="row">
    <div class="col-md-12">
        <div class="box box-widget">
            <div class="box-body with-border">
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group" style="margin-bottom:0px">
                            <span class="input-group-btn">
                                <input type="hidden" id="idrapat" value="">
                                <input type="hidden" id="kdedit" value="">
                                <input type="hidden" id="idusergroup" value="<?= $this->session->userdata('idusergroup') ?>">
                                <button type="button" id="addrow"  class="btn btn-warning" onclick="window.location.href='<?php echo site_url('rapat/index/Gsm5e/r') ?>'">Rekam</button>
                                <button type="button" id="editrow" class="btn btn-warning">Ubah</button>
                                <button type="button" id="delrow"  class="btn btn-warning">Hapus</button>
                            </span>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group" style="margin-bottom:0px">
                            <div class="input-group date" id="datetimepicker1" style="display:none">
                                <input type="text" id="tanggal" class="form-control" value="<?php echo $this->fc->idtgl($prm['tanggal'], 'hari') ?>">
                                <span class="input-group-addon">
                                    <i class="fa fa-calendar text-primary"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <form role="form" action="<?php echo site_url("rapat?q=pqYHV&mod=grid") ?>" method="post">
                          <div class="input-group">
                              <input type="hidden" name="nmfunction" value="dash">
                              <input type="text" name="cari" class="form-control" placeholder="Pencarian..." value="<?php echo $this->session->userdata('cari') ?>">
                                  <span class="input-group-btn">
                                    <button type="submit" name="search" value="search" id="search-btn" class="btn btn-default btn-flat"><i class="fa fa-search"></i>
                                    </button>
                                  </span>
                                  <span class="input-group-btn">
                                      <button type="submit" name="search" value="clear" class="btn btn-default btn-flat"><i class="fa fa-times-circle"></i></button>
                                  </span>
                          </div>
                        </form>
                    </div>
                </div>
            </div>

            <div class="box box-widget">
                <div class="box-body with-border" style="padding-top:0px">
                      <table id="iGrid" class="table table-hover table-bordered">
                        <thead>
                          <tr style="background: #00acd6; color: #fff;">
                            <th class="text-center">Unit</th>
                            <th class="text-center">Agenda Rapat</th>
                            <th class="text-center hidden-xs">PIC</th>
                            <th class="text-center">Hari</th>
                            <th class="text-center">Tanggal</th>
                            <th class="text-center">Waktu</th>
                            <th class="text-center">Ruang Rapat</th>
                            <th class="text-center">Peserta</th>
                            <th class="text-center" colspan="2">Catatan</th>
                          </tr>
                        </thead>

                        <tbody>
                          <?php if ($rapat) {
                            foreach ($rapat as $row) {
                              $text = 'text-muted'; if ($this->session->userdata('kdso')==$row['kdso']) $text = ''; ?>
                              <tr class="<?php echo $text ?>">
                                <td id="<?php echo $row['idrapat'] ?>"><?php echo $row['nmso1'] ?></td>
                                <td id="<?php echo $text ?>"><?php echo $row['agenda'] ?>
                                  <br>
                                  <div class="small">
                                    <?php if ($row['tglrekam']>0) echo '* Tanggal Rekam : '.$this->fc->idtgl($row['tglrekam'], 'tgl').' '. $this->fc->idtgl($row['tglrekam'], 'jam'); ?>                                    
                                  </div>
                                </td>
                                <td class="hidden-xs"><?php echo $row['pic'] ?></td>
                                <td><?php echo $this->fc->idtgl($row['tglawal'], 'mig') ?></td>
                                <td><?php echo $this->fc->idtgl($row['tglawal'], 'tgl') ?></td>
                                <td><?php echo $this->fc->idtgl($row['tglawal'], 'jam') ?></td>
                                <td><?php echo $row['nmrapat'] ?></td>
                                <td class="text-right"><?php echo $row['jmlpeserta'] ?></td>
                                <td><?php echo $row['keterangan'] ?></td>
                                <td><?php if ($row['makan']>0) echo '&nbsp; <i class="fa fa-cutlery"></i>'; ?>
                                  <?php if ($row['snack_p']>0 or $row['snack_s']>0) echo '<i class="fa fa-coffee"></i>'; ?>
                                </td> <!-- moon-o coffee cutlery -->
                              </tr>
                          <?php
                            }
                          } else { ?>
                              <tr><td colspan="9" class="text-danger"> Data tersebut tidak ditemukan ...</td></tr>
                          <?php } ?>
                        </tbody>
                      </table>
                </div>

                <div class="box-footer clearfix">
                  <?php echo $this->pagination->create_links(); ?>
                </div>
            </div>

        </div>
    </div>
</div>



<link href=" <?=base_url('assets/bootstrap/css/bootstrap-datetimepicker.min.css');?>" rel="stylesheet" type="text/css" />
<script src="<?=base_url('assets/plugins/datepicker/moment.min.js');?>" type="text/javascript"></script>
<script src="<?=base_url('assets/plugins/datepicker/id.js');?>" type="text/javascript"></script>
<script src="<?=base_url('assets/bootstrap/js/bootstrap-datetimepicker.min.js');?>" type="text/javascript"></script>
<link href=" <?=base_url('assets/plugins/bxslider/jquery.bxslider.css');?>" rel="stylesheet" type="text/css" />
<script src="<?=base_url('assets/plugins/bxslider/jquery.bxslider.min.js'); ?>" type="text/javascript"></script>


<script type="text/javascript">
    $(function () {
        $('#datetimepicker1').datetimepicker({
            format : 'dddd, DD MMMM YYYY',
            locale : 'id'
        });
    }).on('dp.change', function() {
          var tgl = document.getElementById("tanggal").value;
          window.location.href = "<?php echo site_url('rapat?q=pqYHV&mod=grid&tgl=') ?>"+ tgl;
        });
</script>

<script type="text/javascript">
$(document).ready(function(){
    $('.bxslider').bxSlider({
     mode: 'fade',
     infiniteLoop: true,
     controls: false,
     pager: false,
     auto: true,
     randomStart: true,
     captions: true
   });
});
</script>

<script type="text/javascript">
  $('.pagination').addClass('pagination-sm no-margin pull-right');
</script>

<script type="text/javascript">
  $(function() {

    // Tandai ROW yang dipilih dalam iGRID
    $( "#iGrid tbody" ).selectable({
      filter: ":not(td)",
        create: function( e, ui ) {
        iGridLink( $() );
      },
      selected: function( e, ui ) {
        var widget = $(this).find('.ui-selected');
        $(ui.unselected).addClass("info");
        iGridLink( widget );
      },
      unselected: function( e, ui ) {
        $(ui.unselected).removeClass("info");
        var widget = $(this).find('.ui-selected');
        iGridLink( widget );
      }
    });

    // ONCLICK untuk Rekam / Ubah / Hapus
    $("#addrow").on("click", function() {
      window.location.href = "<?php echo site_url('rapat/index/Gsm5e/r') ?>" ;
    });
    $("#editrow").on("click", function() {
      var kdedit = document.getElementById("kdedit").value;
      var idusergroup = document.getElementById("idusergroup").value;
      if (idusergroup='001') { window.location.href = "<?php echo site_url('rapat/index/Gsm5e/u') ?>" +"/"+ document.getElementById("idrapat").value;
      } else {
          if (kdedit=='text-muted') { alert('Anda tidak berhak melakukan Ubah Data'); }
          else { window.location.href = "<?php echo site_url('rapat/index/Gsm5e/u') ?>" +"/"+ document.getElementById("idrapat").value; }
      }
    });
    $("#delrow").on("click", function() {
      var kdedit = document.getElementById("kdedit").value;
      var idusergroup = document.getElementById("idusergroup").value;
      if (idusergroup='001') { window.location.href = "<?php echo site_url('rapat/index/Gsm5e/h') ?>" +"/"+ document.getElementById("idrapat").value;
      } else {
          if (kdedit=='text-muted') { alert('Anda tidak berhak melakukan Hapus Data'); }
          else { window.location.href = "<?php echo site_url('rapat/index/Gsm5e/h') ?>" +"/"+ document.getElementById("idrapat").value; }
      }
    });

    // Ambil Nilai ROW pada iGRID yang dipilih per-ID sesuai kolom CHILD
    function iGridLink( $selectees ) {
      selected = $.makeArray( $selectees.filter( ".ui-selected" ) );
        selected.reduce( function( a, b ) {
          document.getElementById("idrapat").value= $(b).children( "td:nth-child(1)" ).attr('id');
          document.getElementById("kdedit").value= $(b).children( "td:nth-child(2)" ).attr('id');
        }, 0
      );
     }

  });
</script>

<script type="text/javascript">
    $(function () {
        $('#datetimepicker1').datetimepicker({
            format : 'dddd, DD MMMM YYYY',
            locale : 'id'
        }).on('changeDate', function() {
              var tgl = document.getElementById("tanggal").value;
              window.location.href = "<?php echo site_url('rapat?q=W06or') ?>/"+ tgl;
            });
    });
</script>
