<script src="<?=base_url('assets/plugins/slimScroll/jquery.slimscroll.min.js'); ?>" type="text/javascript"></script>    
<script src="<?=base_url('assets/plugins/jQueryUI/jquery-ui.1.11.4.min.js'); ?>" type="text/javascript"></script>
<link rel="stylesheet" href="<?=base_url('assets/plugins/jQueryUI/jquery-ui.css'); ?>">

<div class="modal fade" id="myModal">
  <div class="modal-dialog">
    <div class="modal-content">

      <form onsubmit="setting_rapat('')" method="post" role="form">
        <div class="modal-header">
          <h4 id="judul-form" class="modal-title">Edit Penandatangan dan Jam</h4>
        </div>
        <div class="modal-body">
          Nip:
          <input name="nip" id="nip" type="text" placeholder="197208141993011001" class="form-control" value="">
          Jabatan:
          <input name="jabatan" id="jabatan" type="text" placeholder="Kepala Subbagian Rumah Tangga" class="form-control" value="">
          Jam:
          <input name="jam" id="jam" type="text" placeholder="3" class="form-control" value="">
        </div>
        <div class="modal-footer">
          <input type="submit" class="btn btn-primary" value="Simpan">
        </div>
      </form>

    </div>
  </div>
</div>

