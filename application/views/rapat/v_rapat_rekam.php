<link href=" <?=base_url('assets/plugins/select2/select2.min.css');?>" rel="stylesheet" type="text/css" />
<script src="<?=base_url('assets/plugins/select2/select2.full.min.js'); ?>" type="text/javascript"></script>

<link href=" <?=base_url('assets/bootstrap/css/bootstrap-datetimepicker.min.css');?>" rel="stylesheet" type="text/css" />
<script src="<?=base_url('assets/plugins/datepicker/moment.min.js');?>" type="text/javascript"></script>
<script src="<?=base_url('assets/plugins/datepicker/id.js');?>" type="text/javascript"></script>
<script src="<?=base_url('assets/bootstrap/js/bootstrap-datetimepicker.min.js');?>" type="text/javascript"></script>

<style>
    .select2-container--default.select2-container--focus,
    .select2-selection.select2-container--focus,
    .select2-container--default:focus,
    .select2-selection:active { outline: none; }
    .select2-container--default .select2-selection--single,
    .select2-selection .select2-selection--single { border: 1px solid #d2d6de; border-radius: 0; padding: 6px 12px; height: 34px; }
    .select2-dropdown { border: 1px solid #d2d6de; border-radius: 0; }
    .select2-container .select2-selection--single .select2-selection__rendered { padding-left: 0; padding-right: 0; height: auto; margin-top: -4px; }
    #tab { display: inline-block; margin-left: 40px; }
</style>

<script>
    function validateForm() {
        var nound = document.forms["myForm"]["nound"].value;
        if (nound == null || nound == "") { alert("Nomor Undangan harus diisi ...!"); return false; }

        var kdrapat = document.forms["myForm"]["kdrapat"].value;
        if (kdrapat == null || kdrapat == "") { alert("Ruang Rapat belum dipilih ...!"); return false; }

        var agenda = document.forms["myForm"]["agenda"].value;
        if (agenda == null || agenda == "") { alert("Agenda Rapat harus diisi ...!"); return false; }

        var pic = document.forms["myForm"]["pic"].value;
        if (pic == null || pic == "") { alert("PIC belum dipilih ...!"); return false; }
    }

    function handleClick( nil ) {
        document.getElementById("jnsrapat").value = nil;
        if (nil==1) { 
            document.getElementById("jamawal").value = '09.00';
            document.getElementById("jamawal").readOnly = false;
            $('#tglakhirdiv').hide();
        }
        if (nil==2) { 
            document.getElementById("jamawal").value = '17.00';
            document.getElementById("jamawal").readOnly = true;
            $('#tglakhirdiv').hide();
        }
        if (nil==3) { 
            document.getElementById("jamawal").value = '08.00';
            document.getElementById("jamawal").readOnly = true;
            $('#tglakhirdiv').show();
        }
    }

    function check_tanggal() {
        var tglawal  = $('#tglawal').val();
        var tglakhir = $('#tglakhir').val();
        if (tglawal>tglakhir) { 
            document.getElementById("tglakhir").value = tglawal;
        }
    }
</script>

<?php
    $kdso = $this->session->userdata('kdso');
    $admin_rt = 'admin'; if ($kdso=='010402') $admin_rt = 'admin';
    $udisabled = '';
    if ($ruh=='Ubah' or $ruh=='Hapus') { 
        $udisabled = 'readonly';
        $tglA = $this->fc->idtgl($rapat['tglawal'], 'hari');
        $jam  = $this->fc->idtgl($rapat['tglawal'], 'jam');
        $ruang= $rapat['nmrapat'];
    } else {
        $add  = 86400; if (date("l")=='Friday') $add  = 86400*3;
        $tglA = $this->fc->idtgl( date('Y-m-d', time()+$add), 'hari' );
        $jam  = '09:00';
        $ruang= ''; 
    }
?>

<div class="row">
    <div class="col-md-9">
        <div class="box box-widget">
            <div class="box-header with-border">
              <h3 class="box-title"><?php echo $ruh ?> Data Pemesanan Ruang Rapat</h3>
            </div>
            <form name="myForm" class="form-horizontal" action="<?php echo site_url('rapat/index/jpWHi') ?>" onsubmit="return validateForm()" method="post" role="form">
                <div class="box-body">
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Jenis Rapat</label>
                        <div class="col-sm-3">
                            <label style="margin: 7px 0px 0px 0px">
                                <input type="hidden" id="jnsrapat" value="1" />
                                <input type="radio" name="jnsrapat" id="optRadio1" value="1" checked onchange="handleClick(this.value);" > &nbsp;&nbsp;Rapat Biasa
                            </label>
                        </div>
                        <div class="col-sm-3">
                            <label style="margin: 7px 0px 0px 0px">
                                <input type="radio" name="jnsrapat" id="optRadio2" value="2" onchange="handleClick(this.value);" > &nbsp;&nbsp;RDK
                            </label>
                        </div>
                        <?php if ($admin_rt=='admin') { ?>
                        <div class="col-sm-3">
                            <label style="margin: 7px 0px 0px 0px">
                                <input type="radio" name="jnsrapat" id="optRadio3" value="3" onchange="handleClick(this.value);" > &nbsp;&nbsp;Rapat Khusus
                            </label>
                        </div>
                        <?php } ?>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Nomor Surat</label>
                        <div class="col-sm-7">
                            <input type="text" name="nound" class="form-control" placeholder="Nomor Surat" <?php echo $udisabled ?> value="<?php echo $rapat['nound'] ?>" onchange="check_nound(this.value)">
                            <input name="ruh" type="hidden" value="<?php echo $ruh ?>" />
                            <input name="kdedit" type="hidden" value="<?php echo $rapat['kdso'] ?>" />
                            <input name="idrapat" type="hidden" value="<?php echo $rapat['idrapat'] ?>" />
                        </div>
                        <div id="okund" class="col-sm-1"></div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Tanggal</label>
                        <div class="col-sm-4">
                            <div class="input-group date datetimepicker1">
                                <input name="tglawal" id="tglawal" type="text" class="form-control" placeholder="Tanggal" <?php echo $udisabled ?> value="<?php echo $tglA ?>" onchange="check_tanggal();" >
                                <span class="input-group-addon">
                                    <i class="fa fa-calendar text-primary"></i>
                                </span>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="input-group date datetimepicker2">
                                <input name="jamawal" id="jamawal" type="text" class="form-control" placeholder="Jam" <?php echo $udisabled ?> value="<?php echo $jam ?>" >
                                <span class="input-group-addon">
                                    <i class="fa fa-clock-o text-primary"></i>
                                </span>
                            </div>
                        </div>
                        <div class="col-sm-1">
                            <button type="button" id="check_availability" <?php echo $udisabled ?> class="btn btn-block btn-default"><i class="fa fa-refresh"></i></button>
                        </div>
                    </div>

                    <div class="form-group" id="tglakhirdiv" style="display:none">
                        <label class="col-sm-3 control-label"></label>
                        <div class="col-sm-4">
                            <div class="input-group date datetimepicker1">
                                <input name="tglakhir" id="tglakhir" type="text" class="form-control" value="<?php echo $tglA ?>" onchange="check_tanggal();" >
                                <span class="input-group-addon">
                                    <i class="fa fa-calendar text-primary"></i>
                                </span>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">Ruang Rapat</label>
                        <div id="ruang_availability" class="col-sm-8">
                              <select name="kdrapat" class="form-control select2" disabled="disabled" style="width: 100%;">
                                <option><?php echo $ruang ?></option>
                              </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-3 control-label">Agenda</label>
                        <div class="col-sm-8">
                        <input name="agenda" class="form-control" id="agenda" placeholder="Agenda Rapat" value="<?php echo $rapat['agenda'] ?>" >
                        </div>

                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">PIC</label>
                        <div class="col-sm-8">
                        <input name="pic" class="form-control" id="pic" placeholder="Pelaksana Kegiatan" value="<?php echo $rapat['pic'] ?>" >
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Peserta</label>
                        <div class="col-sm-8">
                        <input name="jmlpeserta" class="form-control" id="peserta" placeholder="Jumlah Peserta" value="<?php echo $rapat['jmlpeserta'] ?>" >
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Keterangan</label>
                        <div class="col-sm-8">
                        <input name="keterangan" class="form-control" id="keterangan" placeholder="Keterangan" value="<?php echo $rapat['keterangan'] ?>" >
                        </div>
                    </div>
<!--                     <div class="form-group">
                        <label class="col-sm-3 control-label"></label>  
                        <div class="col-sm-8">
                        <input type="hidden" name="kdaktif" id="kdaktif" value="0" />
                        <input type="checkbox" name="kdaktif" id="kdaktif" value="0">
                        Snack / Makan
                        </div>
                    </div>  -->
                    <div class="form-group" style="">
                        <label class="col-sm-3 control-label"></label>
                        <div class="small col-sm-8 text-info">Click &nbsp;&nbsp;<i class="fa fa-refresh">&nbsp;&nbsp;</i>
                            untuk update ketersediaan Ruang Rapat.
                        </div>
                    </div>

                </div>
                <div class="box-footer">
                    <!-- <button class="btn btn-default" onclick="window.location.href = '<?php echo site_url('rapat/index/wIxda') ?>'">Batal </button> -->
                    <button type="submit" id="simpan" class="btn btn-info pull-right"><?php echo $ruh ?></button>
                </div>
            </form>
        </div>
    </div>

    <div class="col-md-3">
        <div class="box box-widget" id="box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Ruang Rapat</h3>
            </div>
            <div class="box-body">
                <p class="">
                    <b>Lokasi</b><br> &nbsp;
                </p>
                <p class="">
                    <b>Kapasitas</b><br> &nbsp;
                </p>
                <p class="">
                    <b>Luas</b><br> &nbsp;
                </p>
                <p class="">
                    <b>Pengelola</b><br> &nbsp;
                </p>
                <p class="">
                    <b>Fasilitas</b><br> &nbsp;
                </p>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(function () {
        $('.datetimepicker1').datetimepicker({
            format : 'dddd, DD MMMM YYYY',
            locale : 'id'
        });
        $('.datetimepicker2').datetimepicker({
            format: 'LT'
        });
    });
</script>
<script type="text/javascript">
    $(function () {
        $(".select2").select2({
            placeholder: "Pilih Ruang Rapat"
        });
    });
</script>
<script type="text/javascript">
    $(document).ready(function() {
        var checking_html = '<img src="<?php echo base_url(); ?>files/images/loading.gif" /> Checking...';
        $('#check_availability').click(function(){
            $('#ruang_availability').html(checking_html);
            check_availability();
        });
    });

    function check_availability(){
        var jnsrapat = $('#jnsrapat').val();
        var tglawal  = $('#tglawal').val();
        var tglakhir = $('#tglakhir').val();
        var jamawal  = $('#jamawal').val();
        $.ajax({  
            type: "POST",  
            url: "<?php echo site_url('rapat/index/hIFru') ?>",  
            data: { 'jnsrapat': jnsrapat, 'tglawal': tglawal, 'tglakhir': tglakhir, 'jamawal': jamawal },  
            success: function(msg) { 
                $('#ruang_availability').html( msg );
                document.getElementById("tglawal").readOnly  = true;
                document.getElementById("jamawal").readOnly  = true;
                document.getElementById("tglakhir").readOnly = true;
            }
        });

    }  

    function check_nound( nound ) {
      $.ajax({
        url : "<?php echo site_url('rapat/index/eBV1c') ?>",
        type: "POST",
        data: { 'nound': nound },
      })
      .done( function (msg) {
        var element = document.getElementById('okund');
        if (msg=='ok') {
            element.innerHTML = '<span style="color: green"> &radic; </span>';
            document.getElementById("simpan").disabled = false;
        } else {
            element.innerHTML = '<span style="color: red"> X </span>';
            document.getElementById("simpan").disabled = true;
        }
      })
    }

    function info_ruang( kdrapat){
        $.ajax({  
            type: "POST",  
            url: "<?php echo site_url('rapat/index/2UD9h') ?>/"+ kdrapat,  
            data: { pesan: 'ok' },  
            success: function(msg) { 
                $('#box-info').html( msg );
            }
        });
    }  

</script>
