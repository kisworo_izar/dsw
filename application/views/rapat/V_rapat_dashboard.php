<link href="<?=base_url('assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css');?>" rel="stylesheet" type="text/css" />
<script src="<?=base_url('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js');?>" type="text/javascript"></script>
<link href="<?=base_url('assets/plugins/select2/select2.css');?>" rel="stylesheet" type="text/css" />
<script src="<?=base_url('assets/plugins/select2/select2.full.min.js'); ?>" type="text/javascript"></script>

<style media="screen">
    td a {display:block;width:100%; color: red}
    A:link,A:visited,A:active,A:hover {text-decoration: none; color: white;}
    .table-bordered > thead > tr > th,
    .table-bordered > thead > tr > td {border-bottom: 2px solid #E7E9EE;}
    .table-bordered > tbody > tr > td {padding:3px}
    .dt {border:solid 1px #B1B9C1;padding:0px 5px;}
    .hiddenRow {padding: 0 !important;}
    .info{border-top:solid 1px #E7E9EE;padding:0px;margin:0px}
    .info1{border-top:0px;padding:0px;margin:0px}
    .x { display: inline-block; width: 110px; }
    .p3 {padding:3px}
	 .thd {width:65px;border-right:solid 1px #B1B9C1;border-bottom:solid 1px #B1B9C1; border-top:solid 1px #B1B9C1;text-align: center; background: #E7E9EE}
    section {position: relative;}
    section.positioned {position: absolute;top:10px;left:10px;}
    .container {overflow-y: auto;padding: 0px;padding-top: 0px;width: 100%}
    table {border-spacing: 1;width:100%;}
    th div{position: absolute;background #E7E9EE;color:black;border-bottom: solid #E7E9EE 1px;border-top: solid 1px #E7E9EE;
        padding: 6px 0px;top: 0;line-height: normal;}
    .calculated-width {width: -moz-calc(100% - 1px);width: -webkit-calc(100% - 1px);width: calc(100% -1px);}​
</style>


<div class="row" ng-app="app" >
    <div class="col-md-12">
        <div class="box box-widget">

           <div class="box-header with-border" style="padding:7px 10px">
               <div class="col-md-2 pull-left" style="padding-top:7px;padding-left:5px">
                     <div class="text-primary" type="button" data-toggle="modal" data-target="#myModal"><i class="fa fa-wrench text-primary btn-flat"></i>&nbsp;&nbsp;  Setting</div>
               </div>
               <div class="span5 col-md-4 pull-right" id="sandbox-container" style="border:0px; padding:0px; height:34px">
                   <div class="input-daterange input-group" id="datepicker">

                          <input type="text" class="form-control" value="<?php echo $start ?>" name="start" id="start" style="padding-top:0px; padding-bottom:0px;  height:34px">

                          <span class="input-group-addon" style="border:0px;"> s.d. </span>

                          <input type="text" class="form-control" value="<?php echo $end ?>" name="end" id="end" style="padding-top:0px; padding-bottom:0px; height:34px">

                   </div>
               </div>
           </div>

            <div class="box-body">

            <section class="">
            <div class="container" style="height:650px">
				<table id="iGrid" class="table table-hover table-bordered">
					<thead>
					<tr>
                  <th style="padding-left:0px;padding-bottom:1px">&nbsp;
							<div class="thd calculated-width" style="text-align:left;padding-left:3px;border-left:solid 1px #B1B9C1;"> Hari, Tanggal</div></th>
						<th style="width:65px;padding:0px"><div class="thd" style="border-left:solid 1px #B1B9C1">R. Biasa</div></th>
						<th style="width:65px;padding:0px"><div class="thd">RDK</div></th>
						<th style="width:65px;padding:0px"><div class="thd">R. Khusus</div></th>
						<th style="width:65px;padding:0px"><div class="thd">Peserta</div></th>
<!--
						<th style="width:65px;padding:0px">
							<div class="thd"><i class="small text-muted fa fa-coffee"></i> Pagi</div></th>
						<th style="width:65px;padding:0px">
							<div class="thd"><i class="small text-muted fa fa-coffee"></i> Sore</div></th>
						<th style="width:65px;padding:0px">
							<div class="thd">Makan</div></th>
-->
					</tr>
					</thead>
				    <tbody>
            <?php
            foreach ($rpt as $key=>$row) { ?>
						<tr style="margin:0px;">
              <td class="text-primary" data-toggle="collapse" data-target=".<?= $row['tgl'];?>"><?php echo $this->fc->idtgl($row['tgl'],'hari');?></td>
              <td style="text-align:right" data-toggle="collapse" data-target=".<?= $row['tgl'];?>"><?= $row['rapat'];?></td>
              <td style="text-align:right" data-toggle="collapse" data-target=".<?= $row['tgl'];?>"><?= $row['rdk'];?></td>
              <td style="text-align:right" data-toggle="collapse" data-target=".<?= $row['tgl'];?>"><?= $row['khusus'];?></td>
              <td style="text-align:right" data-toggle="collapse" data-target=".<?= $row['tgl'];?>"><?php echo number_format($row['jumlah'],0,0,'.');?></td>
  <!--
              <td style="text-align:right" data-toggle="collapse" data-target=".<?= $row['tgl'];?>"><?php echo number_format($row['snack_p'],0,0,'.') ;?></td>
              <td style="text-align:right" data-toggle="collapse" data-target=".<?= $row['tgl'];?>"><?php echo number_format($row['snack_s'],0,0,'.') ;?></td>
              <td style="text-align:right" data-toggle="collapse" data-target=".<?= $row['tgl'];?>"><?php echo number_format($row['makan'],0,0,'.') ;?></td>
-->
            </tr>
                  <tr style="background:#FFF;color:#34495E">
                      <td class="hiddenRow" colspan="8">
                          <div class="small collapse info <?= $row['tgl'];?> " style="padding:7px">
                              <?php if (!$row['detail'] ) {
                               echo "<h5 class='text-center'>Tidak Ada Rapat</h5>";
                             } else { ?>

                              <table>
                                 <tr>
                                    <td style="padding:3px" colspan="4">Rekapitulasi Rapat : <b><?= $this->fc->idtgl($row['tgl'],'hari');?></b></td>
                                    <td colspan="4" class="pull-left">
                                       <button type="button" class="fa fa-print" value="<?= $key ?>" onclick="cetak_pdf( this.value )" style="background:#FFF;border:solid 0px #FFF" data-toggle="tooltip" title="Cetak Laporan"></button>
                                    </td>
                                 </tr>
                                 <tr style="background:#E7E9EE">
                                    <td class="dt" style="padding:3px; text-align:center;width:70px"><b>Waktu</b></td>
                                    <td class="dt" style="padding:3px"><b> Jenis</b></td>
                                    <td class="dt" style="padding:3px"><b>Ruang Rapat</b></td>
                                    <td class="dt" style="padding:3px"><b>Nomor Surat - Agenda Rapat</b></td>
                                    <td class="dt" style="padding:3px"><b>Unit Eselon 2 - PIC</b></td>
                                    <td class="dt text-center" style="padding:3px;width:50px"><b>Peserta</b></td>
  <!--
                                    <td class="dt text-center" style="padding:3px;width:50px">
                                       <i class="text-muted fa fa-coffee"></i><b> Pagi</b></td>
                                    <td class="dt text-center" style="padding:3px;width:50px">
                                       <i class="text-muted fa fa-coffee"></i><b> Sore</b></td>
                                    <td class="dt text-center" style="padding:3px;width:50px"><b> Makan</b></td>
-->
                                 </tr>

                                   <?php foreach ($row['detail'] as $detail) { ?>
                                   <tr>
                                      <td class="dt p3 text-center" style="vertical-align: text-top"><?= $detail['tgl']?> WIB</td>
                                      <td class="dt p3" style="vertical-align: text-top"><?php if($detail['jnsrapat']=='1')echo "R.Biasa"; elseif ($detail['jnsrapat']=='2'){echo "RDK";} else {echo "R.Khusus";}?></td>
                                      <td class="dt p3" style="vertical-align: text-top"><?= $detail['nmrapat']?></td>
                                      <td class=" dt p3" style="vertical-align: text-top">
                                      <b><?=$detail['nound']?></b>
                                      <br>
                                      <?= $detail['agenda']?>
                                      </td>
                                      <td class="dt p3" style="vertical-align: text-top"><?=ucwords(strtolower($detail['nmso']))?> <br>
                                         <span class="small"><b>PIC :</b> <?= $detail['pic']?></span>
                                         <?php if ($detail['tglrekam']>0)
                                         echo '<br>
                                         <span class="small"> <i class="fa fa-clock-o"> : </i>'. $detail['tglrekam']. ' WIB</span>'
                                         ?>
                                      </td>
                                      <td class="dt p3 text-right" style="vertical-align: text-top"><?php echo number_format($detail['jmlpeserta'],0,0,'.') ?></td>
<!--
                                      <td class="dt p3 text-right" style="vertical-align: text-top"><?php echo number_format($detail['snack_p'],0,0,'.') ?></td>
                                      <td class="dt p3 text-right" style="vertical-align: text-top"><?php echo number_format($detail['snack_s'],0,0,'.') ?></td>
                                      <td class="dt p3 text-right" style="vertical-align: text-top"><?php echo number_format($detail['makan'],0,0,'.') ?></td>
-->                                      
                                   </tr>
                                  <?php } ?>
                              </table>
                            <?php }?>
                          </div>
                      </td>
                  </tr>
            <?php } ?>
					</tbody>
				</table>
                </div>
                </section>

			</div> <!-- box body -->
        </div>
    </div>
</div>

<script type="text/javascript">
    $('.collapse').on('show.bs.collapse', function () {
    $('.collapse.in').collapse('hide');
    });
</script>
<script type="text/javascript">
   $('#sandbox-container .input-daterange').datepicker({
      format: "dd-mm-yyyy",
      language: "id",
      orientation: "bottom auto",
      autoclose: true,
      todayHighlight: true
   }).on('changeDate', function() {
         var srt = document.getElementById("start").value;
         var end = document.getElementById("end").value;
         window.location.href = "<?php echo site_url('rapat/caridash?start=') ?>"+ srt +"&end="+ end;
      });
</script>
<script type="text/javascript">
    function cetak_pdf( tgl ) {
        window.location.href = "<?php echo site_url('rapat/pdf') ?>" +'/'+ tgl;
    }
</script>

<script type="text/javascript">

  function setting_rapat(url) {
    var nip      = $('#nip').val();
    var jabatan  = $('#jabatan').val();
    var jam      = $('#jam').val();
    link         = "<?= site_url('rapat/setting_rapat') ?>" + url +'/'+ nip +'/'+ jabatan +'/' + jam;
    window.location.href = link;
  }

</script>

<script type="text/javascript">
$(document).ready(function() {
  $(".filterunit").select2({
      minimumResultsForSearch:5
  });
});
</script>

<?php $this->load->view('rapat/V_rapat_setting') ?>
