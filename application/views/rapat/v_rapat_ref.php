<!--datetimepicker-->
<script src="<?=base_url('assets/plugins/jQuery/jquery.validate.min.js');?>" type="text/javascript"></script>
<script src="<?=base_url('assets/plugins/datepicker/bootstrap-datepicker.js');?>" type="text/javascript"></script>
<!--datetimepicker-->
<script src="<?=base_url('assets/plugins/jQueryUI/jquery-ui.1.11.4.min.js'); ?>" type="text/javascript"></script>
<style media="screen">
	th {
    	text-align: center;
  	}
  	thead tr {
    	color: #fff;
    	background: #00acd6;
  	}

  	#iGrid .ui-selecting { background: #FECA40; }
  	#iGrid .ui-selected { background: #F39814; color: white; }
</style>
<div class="row">
	<div class="col-md-12">
		<div class="box box-widget">
			<div class="box-header with border">
				<div class="wel wel-sm">
					<form id="iForm" class="form-inline" role="form" action="<?php echo site_url("rapat/index/bjnvo") ?>" method="post">
						<div class="form-group pull-right">
							<div class="input-group">
                				<input class="form-control" name="cari" type="text" placeholder="Pencarian..." value="<?php echo $this->session->userdata('cari') ?>">
            					<span class="input-group-btn">
            						<button type="submit" name="search" value="search" class="btn btn-info btn-flat"><i class="fa fa-search"></i></button>
            					</span>
            					<span class="input-group-btn">
            						<button type="submit" name="search" value="clear" class="btn btn-info btn-flat"><i class="fa fa-times-circle"></i></button>
                				</span>
                			</div>
                		</div>
                		<div class="form-group pull-left">
                			<div class="input-group">
	                			<span class="input-group-btn">
                    				<input type="hidden" id="kdrapat" value=" ">
									<button type="button" id="addrow"  class="btn btn-warning" onclick="window.location.href=<?= site_url('rapat/index/7HmSU/1') ?>">Rekam</button>
					  				<button type="button" id="editrow" class="btn btn-warning" value="2">Ubah</button>
					  				<button type="button" id="delrow"  class="btn btn-warning">Hapus</button>
	                			</span>
                			</div>
                   		</div>
                	</form>
                </div>
            </div>   <!-- box header  -->
			<div class="box-body">
				<table id="iGrid" class="table table-hover table-bordered">
					<thead>
					<tr>
						<th>Nama Ruangan Rapat</th>
						<th>Letak Ruangan</th>
						<th>Luas Ruangan Rapat</th>
						<th>Kapasitas Kursi</th>
						<th>Fasilitas Tersedia</th>
						<th>Unit Pengelola</th>
						<th>Gambar</th>
						<th>Status</th>
					</tr>
					</thead>
				<tbody>
					<?php
					if ($table) {
						foreach ($table as $row) { ?>
							<tr>
								<!-- <td><?php echo $row['kd_rapat'] ?></td> -->
								<td id="<?php echo $row['kdrapat'] ?>"><?php echo $row['nmrapat'] ?></td>
								<td><?php echo $row['letak'] ?></td> 
								<td><?php echo $row['luas'] ?> m<sup>2</sup></td>
								<td><?php echo $row['kapasitas']?> </td>
								<td><?php echo $row['fasilitas']?> </td>
								<td><?php echo $row['unit']?></td>
								<td><?php echo $row['gambar']?></td>
								<td><?php if($row['kdaktif']=='1') {echo "Aktif";} else {echo "Nonaktif";} ?></td>	
							</tr>
						<?php
						}
					} else { ?>
							<tr><td colspan="4" class="text-danger"> Data ruang rapat tersebut tidak ditemukan ...</td></tr>
					<?php } ?>
				</tbody>
				</table>
			</div> <!-- box body -->

			<div class="box-footer clearfix">
				<?php echo $this->pagination->create_links(); ?>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$('.pagination').addClass('pagination-sm no-margin pull-right');
</script>

<script type="text/javascript">
	$(function() {

		// Tandai ROW yang dipilih dalam iGRID
		$( "#iGrid tbody" ).selectable({
			filter: ":not(td)",
				create: function( e, ui ) {
				iGridLink( $() );
			},
			selected: function( e, ui ) {
				var widget = $(this).find('.ui-selected');
				$(ui.unselected).addClass("info");
				iGridLink( widget );
			},
			unselected: function( e, ui ) {
				$(ui.unselected).removeClass("info");
				var widget = $(this).find('.ui-selected');
				iGridLink( widget );
			}
		});

		// ONCLICK untuk Rekam / Ubah / Hapus
		$("#addrow").on("click", function() {
			window.location.href = "<?php echo site_url('rapat/index/7HmSU/r') ?>" ;
		});
		$("#editrow").on("click", function() {
			window.location.href = "<?php echo site_url('rapat/index/7HmSU/u') ?>" +"/"+ document.getElementById("kdrapat").value;
		});
		$("#delrow").on("click", function() {
			window.location.href = "<?php echo site_url('rapat/index/7HmSU/h') ?>" +"/"+ document.getElementById("kdrapat").value;
		});

		// Ambil Nilai ROW pada iGRID yang dipilih per-ID sesuai kolom CHILD
		function iGridLink( $selectees ) {
			selected = $.makeArray( $selectees.filter( ".ui-selected" ) );
				selected.reduce( function( a, b ) {
					document.getElementById("kdrapat").value= $(b).children( "td:nth-child(1)" ).attr('id');
				}, 0 
			);
		 }

	}); 
</script>