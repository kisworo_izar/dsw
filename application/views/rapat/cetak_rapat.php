<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-type" content="text/html;charset=UTF-8">
<style type="text/css">
.vertical_line{margin-top:3px;height:1.5px; width:600px;background:#000;}
.vertical_line2{margin-top:2px;height:1px; width:600px;background:#000;}
table {
    border-collapse: collapse;
    font-family: Arial,Helvetica,sans-serif;
    font-style: normal;font-variant:normal;
  }

p {
    text-indent: 50px;
    margin-top:0px;
    margin-bottom: 0px;
}
.inlineTable {
    display: inline-block;
}
@page { margin: 0.3in 0.3in 0.3in 0.3in; }
</style>
</head>
<body>

  <table style="font-size:12pt;width:100%;margin-top:20px">
    <tbody>
      <tr>
        <td><center><b>DAFTAR PERMINTAAN KONSUMSI RAPAT <br>DIREKTORAT JENDERAL ANGGARAN</b></center></td>
      </tr>
    </tbody>
  </table>

  <table style="font-size:12pt;margin-top:20px">
    <tbody>
      <tr>
      <td>Hari/Tanggal: <?php echo $this->fc->idtgl($sum['tgl'],'hari') ?></td>
      </tr>
    </tbody>
  </table>

  <table border="1px" style="font-size:11pt;width:100%;margin-top:5px;">
      <thead style="background-color: #ddd;">
        <tr>
          <th rowspan="3">NO</th>
          <th rowspan="3">UNIT PEMESAN</th>
          <th colspan="4">JUMLAH</th>
        </tr>
        <tr>
          <th colspan="2">Snack</th>
          <th colspan="2">Makan</th>
        </tr>
        <tr>
          <th style="padding-left:  5px;padding-right: 5px;">Pagi</th>
          <th style="padding-left:  5px;padding-right: 5px;">Siang</th>
          <th style="padding-left:  5px;padding-right: 5px;">Siang</th>
          <th style="padding-left:  5px;padding-right: 5px;">Ket</th>
        </tr>
      </thead>
      <tbody>
      <?php
      $no=0;
      foreach ($rpt as $key=>$row) {
      $no++;
      ?>
        <tr>
          <td style="width:3%;text-align:right;padding-right: 5px;vertical-align: text-top"><?php echo $no;?></td>
          <td style="width:45%; font-size: 10pt;text-align: left;padding-left: 5px;">
            <div><strong><?php echo ucwords(strtolower($row['nmso'])); ?></strong></div>
            <div style="margin-top:5px;">Pic : <?php echo $row['pic'];?></div>
            <div style="font-size: 8pt"><?php if ($row['tglrekam']!=NULL) echo 'Tgl Rekam :'.' '.$this->fc->idtgl($row['tglrekam'],'tgl').', Jam: '.$this->fc->idtgl($row['tglrekam'],'jam'); ?></div>
          </td>
          <td style="width:3%;text-align:right;padding-right: 3px;vertical-align: text-top"><?php echo $row['snack_p'];?></td>
          <td style="width:3%;text-align:right;padding-right: 3px;vertical-align: text-top"><?php echo $row['snack_s'];?></td>
          <td style="width:3%;text-align:right;padding-right: 3px;vertical-align: text-top"><?php echo $row['makan'];?></td>
          <td style="width:12%;"></td>
        </tr>
      <?php } ?>
      </tbody>
  </table>


  <div style="border: solid 1px white;font-family:Arial,Helvetica,sans-serif;font-size:11pt;">
    <br>
    Total Permintaan konsumsi : <?php echo $this->fc->idtgl($sum['tgl'],'hari') ?>
    <table style="font-size:11pt;margin-top:5px;">
      <tr>
        <td colspan="3"></td>
      </tr>
      <tr>
        <td>Total Makan</td>
        <td>:&nbsp;</td>
        <td style="text-align: right;"><?php echo $sum['makan'] ?></td>
      </tr>
      <tr>
        <td>Total Snack Pagi</td>
        <td>:&nbsp;</td>
        <td style="text-align: right;"><?php echo $sum['snack_p'] ?></td>
      </tr>
      <tr>
        <td>Total Snack Siang</td>
        <td>:&nbsp;</td>
        <td style="text-align: right;"><?php echo $sum['snack_s'] ?></td>
      </tr>
    </table>

    <div class="inlineTable">
      <table style="font-size:11pt;margin-top: 30px;float: right;margin-left: 150px">
        <tr>
          <td><?php echo $jbtan['jabatan']; ?></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td><?php echo $jbtan['nmuser']?></td>
        </tr>
        <tr>
          <td>NIP <?php echo $jbtan['nip'] ?></td>
        </tr>
      </table>
    </div>

  </div>
</div>
</body>
</html>
