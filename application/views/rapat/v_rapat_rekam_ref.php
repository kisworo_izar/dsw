<link href="<?php echo base_url(); ?>assets/plugins/uploadfile/fileinput.css" rel="stylesheet">
<script src="<?php echo base_url();?>assets/plugins/uploadfile/fileinput.min.js" type="text/javascript"></script>

<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-widget">
                <div class="box-body with-border">

          <form action="<?php echo site_url('rapat/index/of8AP') ?>" method="post" role="form" enctype="multipart/form-data">
            <div class='box-body pad'>
              <div class="form-group">
                <label>
                  <input type="hidden" name="kdaktif" value="0" />
                  <input type="checkbox" name="kdaktif" value="1" class="flat-red" checked>
                  Aktif
                </label>
               </div>
                <label>Nama Ruang Rapat</label>
                   <div class="input-group">
                      <div class="input-group-addon"><i class="fa fa-newspaper-o"></i></div>
                        <input name="ruhi" type="hidden" value="<?php echo $ruhi?>" />
                          <input name="kdrapat" type="hidden" value="<?php echo $table['kdrapat'] ?>" />
                            <input name="nmrapat" type="text" id="" class="form-control" value="<?php echo $table['nmrapat'] ?>" />
                            </div>
                <label>Letak Ruang Rapat</label>
                        <div class="input-group">
                            <div class="input-group-addon"><i class="fa fa-newspaper-o"></i></div>
                            <input name="letak" type="text" id="" class="form-control" value="<?php echo $table['letak'] ?>" />
                            </div>
                          <label>Luas</label>
                        <div class="input-group">
                            <div class="input-group-addon"><i class="fa fa-newspaper-o"></i></div>
                                <input name="luas" type="text" id="" class="form-control" value="<?php echo $table['luas'] ?>" />
                        </div>
                    <label>Kapasitas</label>
                        <div class="input-group">
                            <div class="input-group-addon"><i class="fa fa-newspaper-o"></i></div>
                                <input name="kapasitas" type="text" id="" class="form-control" value="<?php echo $table['kapasitas'] ?>" />
                        </div>

                    <label>Fasilitas</label>
                        <div class="input-group">
                            <div class="input-group-addon"><i class="fa fa-newspaper-o"></i></div>
                                <input name="fasilitas" type="text" id="" class="form-control" value="<?php echo $table['fasilitas'] ?>" />
                            </div>

                    <label>Unit Pengelola</label>
                        <div class="input-group">
                            <div class="input-group-addon"><i class="fa fa-newspaper-o"></i></div>
                                <input name="unit" type="text" id="" class="form-control" value="<?php echo $table['unit'] ?>" />
                            </div>
                        </div>

                <label>Gambar</label>&nbsp; <span class="text-muted"><?php echo $table['gambar'] ?></span>
                  <div class="input-group">
                      <div class="input-group-addon"><i class="fa fa-image"></i></div>
                      <input name="gambar" type="file" class="file" />
                  </div>

                <div class="box-footer">
                    <div class=" pull-right">
                        <input name="simpan" type="submit" id="simpan" class="btn btn-primary" value="<?php echo $ruhi ?>">
                    </div>
                </div>  
            </div>
          </div>
        </div>
      </div>
</section>