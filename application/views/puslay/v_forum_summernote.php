<script src="<?=base_url('assets/plugins/summernote/summernote.min.js'); ?>"></script>
<link rel="stylesheet" href="<?=base_url('assets/plugins/summernote/summernote.css'); ?>">
<script src="<?php echo base_url();?>assets/plugins/uploadfile/fileinput.min.js" type="text/javascript"></script>
<link href="<?php echo base_url(); ?>assets/plugins/uploadfile/fileinput.css" rel="stylesheet">

<style media="screen">
    .note-editor {-webkit-border-radius: 0px;-moz-border-radius: 0px;border-radius: 0px;border: 1px solid #c0c8cf;background: #fff;margin: 10px;}
</style>
<form action="<?php echo site_url("puslay?q=5Untc&kd=1&idparent=$idparent&idchild=$idchild&idforum=$idforum") ?>" id="postForm" method="post" role="form" enctype="multipart/form-data" onsubmit="return postForm()">
    <div class="div-reply">
        <div class="form-group" style="margin-bottom:0px">
            <input name="url" type="hidden" value="<?php echo $url ?>" />
            <textarea id="summernote" name="reply" required>
            <?php if ($quote) { ?>
                    <?php if($jns != '3') { ?>
                        <blockquote class="small" style="padding-top: 5px;padding-bottom: 15px;padding-left: 5px;padding-right: 10px">
                    <?php } ?>
                    
                    <?php if ($jns != '3') { ?>
                        <?php
                            // $foto_profile="files/profiles/_noprofile.png";
                            // if (file_exists("files/profiles/".$quote['profilepic'])) { $foto_profile =  "https://satudja.kemenkeu.go.id/files/profiles/".$quote['profilepic']; }
                            $foto_profile =  "https://satudja.kemenkeu.go.id/files/profiles/".$quote['profilepic'];
                        ?>
                    <?php } ?>

                        <div class="box-comment">
                            <input type="hidden" id='jns' name='jns' value='<?php echo $jns ?>' >
                            <input type="hidden" id='sta' name='sta' value='<?php echo $sta ?>' >
                            <input type="hidden" id='end' name='end' value='<?php echo $end ?>' >
                            <input type="hidden" id='pil' name='pil' value='<?php echo $pil ?>' >
                    <?php if ($jns != '3') { ?>
                            <span class="profile-tooltip"><img class="img-circle img-sm" src="<?php echo $foto_profile; ?>" /></span>
                    <?php } ?>

                    <?php if($jns != '3') { ?>
                        <div class="comment-text">
                            <span class="username">
                                    <span class="profile-tooltip-item"><?php echo $quote['iduser'] ?> </span>
                            </span>
                    <?php } ?>

                                <?php echo $this->fc->read_more(htmlspecialchars($quote['post'] )); ?>
                    <?php if($jns != '3') { ?>
                        </div>        
                        </div>
                        </blockquote><br>
                    <?php } ?>
                <?php } ?>
            </textarea>
        </div>
        <div class="input-group-btn" style="padding:0px 10px 10px 0px">
            <button name="simpan" type="submit" class="btn btn-sm btn-primary pull-right" style="border-radius:0px">Post</button>
        </div>
    </div>
</form>

<script type="text/javascript">
    $(document).ready(function() {
        var nil = 350;
        var str = window.location.pathname;
        if (str.indexOf("home")>0) nil = 50;
        $('#summernote').summernote({
            height: nil,
            toolbar: [
                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['Insert', ['picture', 'link', 'table']]
            ],
            placeholder: 'Isikan posting Anda disini ...',
            onImageUpload: function(files, editor, welEditable) {
                sendFile(files[0], editor, welEditable);
            }
        });

        function sendFile(file,editor,welEditable) {
            data = new FormData();
            data.append("file", file);
            $.ajax({
                url: "<?php echo site_url('forum/upload_image') ?>",
                data: data,
                cache: false,
                contentType: false,
                processData: false,
                success: function(data){
                    // alert(data);
                    $('.summernote').summernote("insertImage", data, 'filename');
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    console.log(textStatus+" "+errorThrown);
                }
            });
        }
    });

    var postForm = function() {
        var content = $('textarea[name="content"]').html($('#summernote').code());
    }
</script>
