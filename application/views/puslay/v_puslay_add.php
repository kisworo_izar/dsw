<link href="<?=base_url('assets/plugins/select2/select2.css');?>" rel="stylesheet" type="text/css" />
<script src="<?=base_url('assets/plugins/select2/select2.full.min.js'); ?>" type="text/javascript"></script>

<div class="row pull-center" >
    <div class="col-md-9">
        <div class="box box-widget">
            <div class="box-header with-border">
              <h3 class="box-title"> Add User </h3>
            </div>

            <div class="form-group">
                <form name="myForm" class="form-horizontal" action="<?php echo site_url('puslay?q=cr4ut') ?>" method="post" role="form">
                    <input type="hidden" name="jenis" value="add">
                    <div class="form-group">
                        <label class="col-md-3 control-label">ID User</label>
                        <div   class="col-md-6">
                            <input type="text" id="iduser" name="iduser" class="form-control " placeholder="ID User" value="<?= $table['data']['kdsatker'] ?> ">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label">Nama</label>
                        <div   class="col-md-6">
                            <input type="text" id="nmuser" name="nmuser" class="form-control " placeholder="Nama User" value="<?= $table['data']['nmsatker'] ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label">NIP</label>
                        <div   class="col-md-6">
                            <input type="text" id="nip" name="nip" class="form-control " placeholder="NIP User" value="">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label">Jabatan</label>
                        <div   class="col-md-6">
                            <input type="text" id="jab" name="jab" class="form-control " placeholder="Jabatan" value="">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label">KL/Unit/Satker/Lokasi</label>
                        <div   class="col-md-2">
                            <input type="text" id="kddept" name="kddept" class="form-control " placeholder="Kode K/L" value="<?= $table['data']['kddept'] ?>">
                        </div>
                        <div   class="col-md-1">
                            <input type="text" id="kdunit" name="kdunit" class="form-control " placeholder="Kode Unit" value="<?= $table['data']['kdunit'] ?>">
                        </div>
                        <div   class="col-md-2">
                            <input type="text" id="kdsatker" name="kdsatker" class="form-control " placeholder="Kode Satker" value="<?= $table['data']['kdsatker'] ?>">
                        </div>
                        <div   class="col-md-1">
                            <input type="text" id="kdlokasi" name="kdlokasi" class="form-control " placeholder="Kode Lokasi" value="<?= $table['data']['kdlokasi'] ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label">NO HP</label>
                        <div   class="col-md-6">
                            <input type="text" id="nohp" name="nohp" class="form-control " placeholder="Nomer HP" value="<?= $table['dat']['nohp'] ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label">E-Mail</label>
                        <div   class="col-md-6">
                            <input type="text" id="email" name="email" class="form-control " placeholder="Email User" value="<?= $table['dat']['email'] ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label">Idusergroup</label>
                        <div   class="col-md-6" >
                            <select class="org form-control" id="idusergroup" name="idusergroup">
                                <option value="" hidden disabled >- Pilih Idusergroup - </option>
                                <?php
                                    foreach($table['sel'] as $row)  {
                                       $sel = ''; if ($row['idusergroup'] == 'usersatker') $sel = 'selected'; 
                                        echo '<option value="'. $row['idusergroup'] .'" '. $sel .'>'. $row['idusergroup'] .' - '. $row['nmusergroup'] .'</option>';
                                    }
                                 ?>
                            </select>
                            <span class="text-small">*Pilih <b>userkl</b> Untuk KL, *Pilih <b>userunit</b> Untuk Unit, *Pilih <b>usersatker</b> Untuk Satker</span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label">Password</label>
                        <div   class="col-md-6">
                            <input type="text" id="pswd" name="pswd" class="form-control " placeholder="Password" value="<?= $table['data']['hash'] ?>">
                        </div>
                    </div>
                    <div class="box-footer">
                        <button type="submit" id="simpan" class="btn btn-info pull-right">Simpan</button>
                    </div>

                </form>
            </div>
        </div>
    </div>

</div>

<script type="text/javascript">
  $(document).ready(function() {
    $(".org").select2({
      minimumResultsForSearch:5
    });
  });
</script>