<table border="1">
  <thead>
    <tr style="color: #000;">
      <th style="width: 40px" class="text-left">No</th>
      <th hidden-xs hidden-sm>No. Surat Satker</th>
      <th class="text-left">Tgl Surat</th>
      <th class="text-left">Kode Satker</th>
      <th class="text-left">Nama Satker</th>
      <th class="text-left">Revis Ke</th>
      <th class="text-left">Jenis Revisi</th>
      <th class="text-left">Tgl. Sp</th>
      <th class="text-left">No. Sp</th>
      <th class="text-left">KPPN</th>
      <th class="text-left">KL</th>
      <th class="text-left">Unit Es. I</th>





      <!-- <th class="text-left">C. W</th> -->
    </tr>
  </thead>
  <tbody style="margin:10px">
    <?php $no =0; foreach($dat as $key=> $row) {
      $no++
      ?>
    <tr>
      <td><?= $no ?></td>
      <td><?= $row['kl_surat_no'] ?></td>
      <td><?= date("d-m-Y", strtotime($row['kl_surat_tgl']));  ?></td>
      <td><?= "'".$row['kl_satker'] ?></td>
      <td><?= $row['nmsatker'] ?></td>
      <td><?= $row['rev_ke'] ?></td>
      <td><?= $row['rev_jns_revisi'] ?></td>
      <td><?= date("d-m-Y", strtotime($row['t7_sp_tgl'])); ?></td>
      <td><?= $row['t7_sp_no'] ?></td>
      <td><?= $row['kppn'] ?></td>
      <td><?= "'".$row['kl_dept'].' '.$row['nmdept'] ?></td>
      <td><?= "'".$row['kl_dept'].'.'.$row['kl_unit'].' '. $row['nmunit'] ?></td>
    </tr>
    <?php } ?>
  </tbody>
</table>