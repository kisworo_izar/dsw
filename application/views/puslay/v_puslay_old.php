<link href="<?=base_url('assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css');?>" rel="stylesheet" type="text/css" />
<link href="<?=base_url('assets/dist/css/revisi.css');?>" rel="stylesheet" type="text/css" />
<link href="<?=base_url('assets/dist/css/tooltip.css');?>" rel="stylesheet" type="text/css" />
<script src="<?=base_url('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js');?>" type="text/javascript"></script>
<style media="screen">
   A:link,A:visited,A:active,A:hover {text-decoration: none; color: #7290B6;}
   .btn-flat{border-radius: 0px!important}
</style>
<div class="row">
   <div class="col-md-8">
      <div class="box box-widget">

         <div class="box-header with-border">
            <div class="row">
               <div class="col-md-5" id="sandbox-container" style="height:30px">
                  <div class="input-daterange input-group" id="datepicker">
                     <input type="text" class="input-sm form-control" value="" name="start" id="start" style="padding-top:0px; padding-bottom:0px;" autocomplete="off">
                     <span class="input-group-addon" style="border:0px;"> s.d. </span>
                     <input type="text" class="input-sm form-control" value="" name="end" id="end" style="padding-top:0px; padding-bottom:0px;" autocomplete="off">
                  </div>
               </div>
               <div class="col-md-6 pull-right">
                  <div class="input-group input-group-sm">
                     <input type="text" class="form-control" placeholder="Pencarian..." value="" autocomplete="off">
                     <span class="input-group-btn">
                        <button type="button" class="btn btn-primary btn-flat"> <i class="fa fa-refresh"></i> </button>
                     </span>
                  </div>
               </div>
            </div>
         </div>

         <div class="box-body" style="padding-top: 0px">
            <section>
               <div class="container" style="height: 650px; padding: 0px">
                  <h3 class="text-center text-bold text-danger">
                     --- Development ---
                  </h3>
                  <table id="iGrid" class="table table-hover table-bordered">
                     <tbody>

                        <?php
                        for ($i=0; $i < 50; $i++) {
                           ?>

                           <tr>
                              <!-- <td> -->


                                 <div class="box-footer box-comments" style="padding-bottom:0px; padding-top:5px;background :#FFFFFF; border: solid 1px white; border-bottom: solid 1px white">
                                    <div class="box-comment" style="border-bottom: solid 1px #EDF0F4">
                                       <span class="profile-tooltip1">
                                           <img class="img-circle img-sm" src="files/profiles/tip.gif" alt="user image">
                                       </span>
                                       <div class="comment-text">
                                          <span class="username">
                                             <span class="profile-tooltip-item">Development</span> <span class="small">[ xxx ]</span>
                                             <span class="text-muted pull-right">Selasa, 05 Maret 2019 08:47 WIB &nbsp;
                                                <a href="#"> <i class="fa fa-thumb-tack text-gray"></i> </a>
                                             </span>
                                              <br>
                                          </span>
                                          <div class="convert-emoji">
                                             solusi sementara: download berupa XLS, lalu buka paka aplikasi pengolah angka (spreadsheet) aplikasi pengolah angka (spreadsheet) aplikasi pengolah angka (spreadsheet)
                                          </div>
                                          <div class="small text-left text-bold"> <a href="#">Nama Thread</a></div>

                                          <div class="small comment more shortened pull-right">
                                             <a href="#" class="text-red"><i class="fa fa-trash"></i> Delete</a>&nbsp;&nbsp;&nbsp;
                                             <a href="#" class="text-yellow"><i class="fa fa-edit"></i> Edit</a>&nbsp;&nbsp;&nbsp;
                                             &nbsp;&nbsp;&nbsp;
                                             <a href="http://intranet.anggaran.depkeu.go.id/puslay/reply/15/156181" class="text-green"><i class="fa fa-reply"></i> Reply</a>&nbsp;&nbsp;&nbsp;
                                             <a href="http://intranet.anggaran.depkeu.go.id/puslay/reply/15/156181/2" class="text-primary"><i class="fa fa-commenting"></i> Quote</a>
                                          </div>
                                       </div>
                                    </div>
                                    <div id="reply_2" class="box-footer div-reply" style="display: none"></div>
                                 </div>

                              <!-- </td> -->
                           </tr>





                           <?php
                        } ?>


                     </tbody>
                  </table>
               </div>
            </section>
         </div>
      </div>
   </div>

   <div class="col-md-4">

      <!-- OTP -->
      <div class="box box-widget" style="margin-bottom: 10px">
         <div class="box-header with-border" style="background:#CA5C54; color: #FFFFFF; padding: 5px 10px; border-bottom: solid 2px orange">
          <span class="small"> <i class="fa fa-key"></i>&nbsp;&nbsp;<b>OTP</b></span>

         </div>
         <div class="box-body with-border">
            <div class="input-group input-group-sm">
             <input type="text" class="form-control" id="otp" value="" placeholder="ID Revisi, Kode Satker ">
                 <span class="input-group-btn">
                   <button type="button" class="btn btn-flat btn-primary text-bold" onclick="otp()">Check OTP</button>
                 </span>
            </div>
            <div class="" style="margin-top: 10px">

               <!-- ====== -->
               <table id="iGrid" class="table table-hover table-bordered small" style="border-spacing: 1; width: 100%; margin-bottom:0px">
            <?php if($otp){ ?>
                  <thead>
                     <tr style="color: #fff; background-color:Blue;">
                        <th style="padding: 3px; background:#2B5590; color:#FFFFFF">ID Revisi</th>
                        <th style="padding: 3px; background:#2B5590; color:#FFFFFF">Kode OTP</th>
                        <th style="padding: 3px; background:#2B5590; color:#FFFFFF">Tanggal Create</th>
                        <th style="padding: 3px; background:#2B5590; color:#FFFFFF">Sts</th>
                     </tr>
                  </thead>
            <?php } ?>

                  <!-- data loop -->
            <?php if($otp) foreach($otp as $row) { ?>
                  <tr>
                     <td class="text-center"><?= $row['rev_id'] ?></td>
                     <td class="text-center"><b><?= $row['otp'] ?></b></td>
                     <td class="text-center"><?= $row['tglcreate'] ?></td>
                     <td class="text-center"><?= $row['status'] ?></td>
                  </tr>
            <?php } else {
               ?>
               <!-- <tr>
                  <td colspan="4" class="text-center text-primary text-bold">Kode OTP tidak ditemukan atau kadaluarsa <br>Silahkan ajukan permintaan OTP kembali</td>
               </tr> -->
            <?php
            } ?>

               </table>
            <!-- end loop -->

            </div>
         </div>
      </div>

      <!-- Check Satker -->
      <div class="box box-widget" style="margin-bottom: 10px">
         <div class="box-header with-border" style="background:#429258; color: #FFFFFF; padding: 5px 10px; border-bottom: solid 2px orange">
          <span class="small"> <i class="fa fa-eye"></i>&nbsp;&nbsp;Status <b>Revisi Satker</b></span>

         </div>
         <div class="box-body with-border">
            <div class="input-group input-group-sm">
             <input type="text" class="form-control" id="stsRev" value="" placeholder="Kode Satker ">
                 <span class="input-group-btn">
                   <button type="button" class="btn btn-flat btn-primary text-bold" onclick="stsRev()">Check Status</button>
                 </span>
            </div>
            <div class="" style="margin-top: 10px">

               <!-- ====== -->
               <table id="iGrid" class="table table-hover table-bordered small" style="border-spacing: 1; width: 100%; margin-bottom:0px">

                  <?php if($stsRev){ ?>
                        <thead>
                           <tr style="color: #fff; background-color:Blue;">
                              <th style="padding: 3px; background:#2B5590; color:#FFFFFF">Satker</th>
                              <th style="padding: 3px; background:#2B5590; color:#FFFFFF">Kewenangan</th>
                              <th style="padding: 3px; background:#2B5590; color:#FFFFFF">ID Revisi</th>
                              <th style="padding: 3px; background:#2B5590; color:#FFFFFF">Tahap</th>
                           </tr>
                        </thead>

                              <!-- data loop -->
                        <?php if($stsRev) foreach($stsRev as $row) { ?>
                              <tr>
                                 <td class="text-center"><?= $row['kdsatker'] ?></td>
                                 <td class="text-center"><?php
                                       if ($row['rev_level']=='1') echo "Revisi <b>DJA</b>";
                                       elseif ($row['rev_level']=='2' ) echo "Revisi <b>PA</b>";
                                       else echo "Revisi <b>Kanwil</b>";
                                       ?>
                                 </td>
                                 <td class="text-center"><?= $row['rev_id'] ?></td>
                                 <td class="text-center"><?php
                                       if ($row['rev_tahap'] == '2') echo "Penelitian";
                                       elseif ($row['rev_tahap'] == '6') echo "Penelaahan";
                                       else echo "Pengajuan";
                                       ?>
                                 </td>
                              </tr>
                     <?php }
                  }  else { ?>
                     <tr>
                        <td colspan="4" class="text-center text-primary">Satker tidak sedang mengajukan Revisi</td>
                     </tr>
                  <?php } ?>
               </table>

            </div>
         </div>
      </div>

      <!-- Check User  -->
      <div class="box box-widget">
         <div class="box-header with-border" style="background:#2A5F99; color: #FFFFFF; padding: 5px 10px; border-bottom: solid 2px orange">
          <span class="small"> <i class="fa fa-user"></i>&nbsp;&nbsp;User <b>SatuDJA</b>
             <a href="#"> <i class="fa fa-users pull-right" style="padding-top:3px; color:#FFF"> </i></a>
          </span>

         </div>
         <div class="box-body with-border">
            <!-- Reset Pass -->
            <div class="input-group input-group-sm">
             <input type="text" class="form-control" placeholder="User ID" value="<?= $userid['userid'] ?>" id="userid">
                 <span class="input-group-btn">
                   <button type="button" class="btn btn-flat btn-primary text-bold" value="" onclick="surel('chekuser')" >Check User</button>
                 </span>
            </div>
            <div class="" style="margin-top: 5px">
               <textarea class="form-control" style="font-size: 90%" rows="5" id="txt1" placeholder="Data User"><?= $userid['txt1'] ?></textarea>
            </div>
            <div class="row" style="margin-bottom: 10px">
               <div class="col-md-12">
                  <div class="pull-left" style="margin-top: 10px">
                     <button type="button" class="btn bg-orange btn-flat margin" style="margin:0px; padding: 7px 10px" tooltip="Isi Formulir" onclick="surel('form')"> <i class="fa fa-file-text-o"></i> </button> &nbsp;
                     <button type="button" class="btn bg-orange btn-flat margin" style="margin:0px; padding: 7px 10px" tooltip="Cek Login" onclick="surel('ceklogin')"> <i class="fa fa-search-plus"></i> </button> &nbsp;
                     <button type="button" class="btn bg-orange btn-flat margin" style="margin:0px; padding: 7px 10px" tooltip="User Baru" onclick="userbaru()"> <i class="fa fa-user-plus"></i> </button> &nbsp;
                     <button type="button" class="btn bg-orange btn-flat margin" style="margin:0px; padding: 7px 10px" tooltip="Ubah Kewenangan" onclick="editwenang()"> <i class="fa fa-edit"></i> </button> &nbsp;
                  </div>
                  <div class="pull-right" style="margin-top: 10px">
                     <button type="button" class="btn bg-orange btn-flat margin" style="margin:0px; padding: 7px 10px" tooltip="Pass Baru" onclick="surel('passbaru')"> <i class="fa fa-key"></i> </button> &nbsp;
                     <button type="button" class="btn bg-orange btn-flat margin" style="margin:0px; padding: 7px 10px" tooltip="Reset Pass" onclick="surel('respas')"> <i class="fa fa-recycle"></i> </button>
                  </div>
               </div>
            </div>

            <div class="" style="margin-top: 5px">
               <span class="small"><i>Sugest</i> jawaban:</span>
               <button class="small pull-right text-danger" onclick="copyTxt()">Copy Text</button>
               <textarea class="form-control" style="font-size: 90%; margin-top: 5px" rows="13" id="txt2" placeholder="Result"><?= $userid['txt2'] ?></textarea>
            </div>

         </div>
      </div>

   </div>
</div>


<script type="text/javascript">
  $('#sandbox-container .input-daterange').datepicker({
    format: "dd-mm-yyyy",
    language: "id",
    orientation: "bottom auto",
    autoclose: true,
    todayHighlight: true
  }).on('changeDate', function() {
    var sta = $('#start').val();
    var end = $('#end').val();
    window.location.href = "#" +'&start='+ sta +"&end="+ end });

   function copyTxt() {
    // alert(nil)
     var copyText = document.getElementById("txt2");
     copyText.select();
     document.execCommand("copy");
     // alert("Copied the text: " + copyText.value);
   }

   function otp() {
      var otp = $('#otp').val();
      if(otp !=''){
        window.location.href = '<?= site_url('puslay?q=p4sly') ?>' +'&otp='+ otp;
      }
   }

   function stsRev() {
      var stsRev = $('#stsRev').val();
      if(stsRev !=''){
        window.location.href = '<?= site_url('puslay?q=p4sly') ?>' +'&stsRev='+ stsRev;
      }
   }

  function surel(typ){
    var userid = $('#userid').val();

    if(userid !='' || typ=='form'){
        window.location.href = '<?= site_url('puslay?q=p4sly') ?>' +'&userid='+ userid +'&typ='+typ;
      }
  }

  function userbaru(){
    var userid = $('#userid').val();
    if(userid ==''){
      window.location.href ='<?= site_url('puslay?q=ad47s') ?>';
    } else {
      window.location.href ='<?= site_url('puslay?q=ad47s') ?>' + '&userid='+userid;
    }
  }

  function editwenang(){
    var userid = $('#userid').val();
    if (userid == '') return alert('User ID Kosong !, Masukan terlebih dahulu User ID');
    if (userid != '') {
      window.location.href = '<?= site_url('puslay?q=3d1ts') ?>' + '&userid=' + userid;
    }
  }
</script>
