<?php
    // if ($limit==null) 
    $limit=5;
    $query = $this->db->query("Select concat(idparent,'.',idchild) idkey, idparent, idchild, count(*) reply From d_forum Group By 1,2,3 Order By reply Desc Limit $limit");
    $d_top = $this->fc->ToArr( $query->result_array(), 'idkey');

    foreach ($d_top as $key=>$value) {
        // Forum Room
        $query = $this->db->query("Select nmroom From d_forum_room where idparent=". $value['idparent'] ." and idchild=". $value['idchild']);
        $room  = $query->row_array();
        $d_top[ $key ]['nmroom'] = $room['nmroom'];

        // Forum Home of Room
        $query = $this->db->query("Select thread From d_forum where idparent=". $value['idparent'] ." and idchild=". $value['idchild'] ." and idforum=1");
        $home  = $query->row_array();
        $d_top[ $key ]['thread'] = $home['thread'];
    }
?>

<div class="box box-widget" style="border-bottom: 1px solid #D2D6DE">
    <div style="background :#fff; border-radius:0px; border-left: 5px solid #DD4B39;padding: 5px">
        <b style="color:#DD4B39">Top Thread</b>
    </div>
    <?php foreach ($d_top as $row) { 
        $url = site_url('forum/forum_post').'/'. $row['idparent'] .'/'. $row['idchild'];
        if ($this->session->userdata('idusergroup')=='999') $url = '#'; ?>

        <div class="box-footer" style="border-top: 1px solid #D2D6DE; padding: 5px 10px 5px 10px;background :#ECF0F5; border-radius:0px">
            <a href="<?php echo $url ?>"><?php echo $row['thread'] ?></a>
        </div>
    <?php } ?>
</div>
