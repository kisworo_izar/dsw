<link href="<?=base_url('assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css');?>" rel="stylesheet" type="text/css" />
<link href="<?=base_url('assets/plugins/select2/select2.css');?>" rel="stylesheet" type="text/css" />
<link href="<?=base_url('assets/dist/css/revisi.css');?>" rel="stylesheet" type="text/css" />
<link href="<?=base_url('assets/dist/css/tooltip.css');?>" rel="stylesheet" type="text/css" />
<script src="<?=base_url('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js');?>" type="text/javascript"></script>
<script src="<?=base_url('assets/plugins/select2/select2.full.min.js'); ?>" type="text/javascript"></script>
<style media="screen">
   A:link,A:visited,A:active,A:hover {text-decoration: none; color: #7290B6;}
   .btn-flat{border-radius: 0px!important}
.select2-selection {font-size: 12px;}
.select2-selection__rendered {line-height: 25px !important;padding-left:7px !important}
.select2-container .select2-selection--single {height: 30px !important;padding-left:0px !important}
.loader { position: fixed; left: 45%; top: 45%; z-index: 9999; border: 16px solid #f3f3f3; border-radius: 50%; border-top: 16px solid #3498db; width: 120px;
 height: 120px;  -webkit-animation: spin 2s linear infinite; /* Safari */ animation: spin 2s linear infinite;}
@keyframes spin { 0% { transform: rotate(0deg); }100% { transform: rotate(360deg); }}
</style>
<div class="row">
<div class="loader" id="loader" style="display:none;"></div>

<!-- --- -->
<?php
   $grp = explode(";", $this->session->userdata('idusergroup')); 
   if(in_array('600',$grp) or in_array('601',$grp) or in_array('611',$grp) or in_array('612',$grp)) { ?> 


      <div class="col-md-8">
         <div class="box box-widget">

               <div class="box-footer box-comments" style="background:white">
                  <img src="files/slideshow/dsw-wfh-04.jpg" width=100%>           
               </div>
               <div class="box-footer box-comments" style="background:white">
                  <img class="img-circle img-sm img-bordered-dsw" src="files/profiles/angga1.png">
                  <div class="comment-text" style="bg-color:white !important">
                  <span >Mas <b>Angga</b>
                     <!-- <span class="text-muted pull-right">Kamis, 25 April 2019</span> -->
                  </span>
                  <p>
                     <br>
                     Gunakan <b style="color:#BD6259">OTP</b> untuk memperoleh informasi kode OTP<br>
                     Isi kolom isian dengan <b>ID Revisi</b> atau <b>kode Satker</b> lalu click <b style="color: #508CB8">Check OTP</b>.
                  </p>
                  <p>
                     Gunakan <b style="color:#57905E">Status Revisi Satker</b> untuk mengetahui status Revisi Satker<br>
                     Isi kolom isian dengan <b>kode Satker</b> lalu click <b style="color: #508CB8">Check Status</b>.
                  </p>
                  <p>
                     Gunakan <b style="color:#365F95">User SatuDJA</b> untuk mengetahui informasi User<br>
                     Isi kolom isian dengan <b>ID User</b> lalu click <b style="color: #508CB8">Check User</b>.<br>
                     Click <b style="color:#AB6360"> Copy Text </b> untuk menyalin informasi ke <i>clipboard</i>, informasi yg telah tersalin dapat ditempel (<i>Paste</i>) ke email atau sarana komuniksi elektronik lain.
                  </p>
                     <!-- <div id="hidden_div475" style="display: none;"><p></p></div>                 -->
                  </div>
               </div>

               <div class="box-footer box-comments" style="background:white">
                  <img class="img-circle img-sm img-bordered-dsw" src="files/profiles/angga1.png">
                  <div class="comment-text" style="bg-color:white !important">
                  <span >Mas <b>Angga</b>
                  </span>
                  <p>
                     <br>
                     <img src="files/images/inter.png"  width="100px" alt=""> <br><br>
                     Tanda diatas pada <b>Monitoring Revisi Anggaran</b> menunjukkan bahwa pengajuan usulan Revisi dilakukan secara langsung menggunakan Aplikasi <b>SAKTI</b> melalui mekanisme pertukaran data.  
                  </p>
                  </div>
               </div>


         </div>
      </div>

<?php   
   } else { 
?>
<!-- --- -->
   <div class="col-md-8">
      <div class="box box-widget">
         <div class="box-header with-border" style="background:#2A5F99; color: #FFFFFF; padding: 5px 10px; border-bottom: solid 2px orange">
          <span class="small"> <i class="fa fa-wechat"></i>&nbsp;&nbsp;Forum <b>SatuDJA</b></span>

         </div>

         <div class="box-header with-border">
            <div class="row">
               <div class="col-md-5" id="sandbox-container" style="height:30px">
                  <div class="input-daterange input-group" id="datepicker">
                     <input type="text" class="input-sm form-control" name="start" id="start" style="padding-top:0px; padding-bottom:0px;" value="<?= $forum['sta']?>" autocomplete="off">
                     <span class="input-group-addon" style="border:0px;">s.d.</span>
                     <input type="text" class="input-sm form-control" name="end" id="end" style="padding-top:0px; padding-bottom:0px;" value="<?= $forum['end'] ?>" autocomplete="off">
                  </div>
               </div>
               <div class="col-sm-2" >
                     <select class="opt form-control" id="opt" onchange="pil( this.value )">
                     <?php $a=''; $b=''; if($forum['pil']) $a = 'selected'; else $b = 'selected'; ?>
                        <option value="0" <?= $b ?> > Belum    </option>
                        <option value="1" <?= $a ?> > Terjawab </option>
                     </select>
               </div>

               <div class="col-md-5 pull-right">
               <!-- <label for="cars">Choose a car:</label> -->

                  <form role="form" action="<?php echo site_url("puslay?q=p4sly") ?>" method="post" style="margin-bottom:0px">
                     <div class="input-group input-group-sm">
                        <input type="text" name="cari" class="form-control" placeholder="Pencarian..." value="<?= $forum['cari']?>" autocomplete="off">
                        <span class="input-group-btn">
                           <button type="submit" name="search" value="search" class="btn btn-primary btn-flat"><i class="fa fa-search" style="height:16px;margin-top:4px"></i></button>
                        </span>
                        <span class="input-group-btn">
                           <button type="submit" name="search" value="clear" class="btn btn-primary btn-flat"><i class="fa fa-times-circle" style="height:16px;margin-top:4px"></i></button>
                        </span>
                     </div>
                  </form>

               </div>
            </div>
         </div>

         <div class="box-body" style="padding: 2px">


          <section>
                  <div class="container" style="height: 810px; padding: 0px">
                  <input type="hidden" id='sta' name="sta" value="<?php echo $sta ?>" >
                  <input type="hidden" id='sta' name="sta" value="<?php echo $end ?>" >
                  <input type="hidden" id='sta' name="sta" value="<?php echo $pil ?>" >
                  <table id="iGrid" class="table table-hover table-bordered">
                     <tbody>

                        <?php if($forum['d_post']){ ?>
                           <?php foreach($forum['d_post'] as $row) {
                              $foto_profile="https://satudja.kemenkeu.go.id/files/profiles/puslay.png";   
                              // $foto_profile="files/profiles/".$row['profilepic'];
                              if ($row['profilepic']) { $foto_profile =  "https://satudja.kemenkeu.go.id/files/profiles/".$row['profilepic']; }
                           ?>
                           <tr>

                              <div class="box-footer box-comments" style="padding:0px; background :#F7F7F7; border: solid 1px white; border-bottom: solid 1px white">
                              <!-- <input type="text" name="sta" id="sta" value="" > -->
                                 <div class="box-comment" style="padding:10px; border-bottom: solid 1px #EDF0F4">
                                    <span class="profile-tooltip1">
                                          <img class="img-circle img-sm" src="<?php echo $foto_profile; ?>" alt="user image">
                                    </span>
                                    <div class="comment-text">
                                      <span class="">
                                          <span class="profile-tooltip-item text-bold"><?php echo $row['iduser'] ?></span>&nbsp;
                                          <span class="profile-tooltip-item small"><?php echo $row['nmuser'] ?></span>
                                          <span class="text-muted pull-right"><?php echo $this->fc->idtgl($row['tglpost'],'full') ?> &nbsp;
                                          <?php $tanda = ($row['terjawab'] == '0' ? 'text-gray':'text-green'); ?>
                                          <i class="fa  fa-check-circle <?php echo $tanda; ?>"></i>
                                          </span>
                                             <br>
                                       </span>
                                       <?php
                                             // $this->fc->read_more( $row['post'] );
                                             echo $row['post'];
                                             if ($row['attach']!='') {
                                                echo "<i class='small'>File : </i>";
                                                $arr = explode(';', trim($row['attach']));
                                                for ($i=0; $i<count($arr); $i++) {
                                                   $nmfile  = $arr[$i];
                                                   $urlfile = site_url("files/forum/$nmfile");
                                                   if ( $i<count($arr)-1 ) $nmfile .= ",";
                                                   echo "<i class='small'><a href='$urlfile' target='_blank'>$nmfile </a></i>";
                                                }
                                             }
                                       ?>

                                       <div class="small comment more shortened pull-right">
                                          <!-- <a href="#" class="text-red"><i class="fa fa-trash"></i> Delete</a>&nbsp;&nbsp;&nbsp; -->
                                          <a href=<?php echo site_url().'puslay/?q=r3dits&idparent='.$row['idparent'].'&idchild='.$row['idchild'].'&idforum='.$row['idforum'].'&sta='.$sta.'&end='.$end.'&pil='.$pil ?> <?php echo $row['idforum']?> class="text-yellow"><i class="fa fa-edit"></i> Edit</a>&nbsp;&nbsp;&nbsp;

                                          <span class="text-red" style="cursor: pointer;" onclick="del('<?=$row['idparent'] ?>','<?= $row['idchild'] ?>','<?= $row['idforum']?>')"> Hapus&nbsp;
                                          <i class="fa fa-trash" > </i>
                                          </span>
                                          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                          <a href=<?php echo site_url().'puslay/?q=rx3lz&idparent='.$row['idparent'].'&idchild='.$row['idchild'].'&idforum='.$row['idforum'].'&sta='.$sta.'&end='.$end.'&pil='.$pil ?> <?php echo $row['idforum']?> class="text-yellow"><i class="fa fa-commenting"></i> Jawab</a>&nbsp;&nbsp;&nbsp;
                                          
                                       </div>
                                    </div>
                                 </div>
                                 <div id="reply_2" class="box-footer div-reply" style="display: none"></div>
                              </div>
                           </tr>
                        <?php } ?>
                        <?php } else { echo '<tr><td colspan="9" class="text-center text-muted" style="font-size:13px; font-style: italic">Tidak ada data ... </td></tr>';} ?> 

                        


                     </tbody>
                  </table>
               </div>
            </section>
          

            
         </div>
      </div>
   </div>


<?php } ?>

<!-- ---    -->

   <div class="col-md-4">

      <!-- OTP -->
      <div class="box box-widget" style="margin-bottom: 10px">
         <div class="box-header with-border" style="background:#ff851b; color: #FFFFFF; padding: 5px 10px; border-bottom: solid 2px orange">
          <span class="small"> <i class="fa fa-key"></i>&nbsp;&nbsp;<b>OTP</b></span>

         </div>
         <div class="box-body with-border">
            <div class="input-group input-group-sm">
             <input type="text" class="form-control" id="otp" value="" placeholder="ID Revisi, Kode Satker ">
                 <span class="input-group-btn">
                   <button type="button" class="btn btn-flat btn-primary text-bold" onclick="otp()">Check OTP</button>
                 </span>
            </div>
            <div class="" style="margin-top: 10px">

               <!-- ====== -->
               <table id="iGrid" class="table table-hover table-bordered small" style="border-spacing: 1; width: 100%; margin-bottom:0px">
            <?php if($otp){ ?>
                  <thead>
                     <tr style="color: #fff; background-color:Blue;">
                        <th style="padding: 3px; background:#2B5590; color:#FFFFFF">ID Revisi</th>
                        <th style="padding: 3px; background:#2B5590; color:#FFFFFF">Kode OTP</th>
                        <th style="padding: 3px; background:#2B5590; color:#FFFFFF">Tanggal Create</th>
                        <th style="padding: 3px; background:#2B5590; color:#FFFFFF">Sts</th>
                     </tr>
                  </thead>
            <?php } ?>

                  <!-- data loop -->
            <?php if($otp) foreach($otp as $row) { ?>
                  <tr>
                     <td class="text-center"><?= $row['rev_id'] ?></td>
                     <td class="text-center"><b><?= $row['otp'] ?></b></td>
                     <td class="text-center"><?= $row['tglcreate'] ?></td>
                     <td class="text-center"><?= $row['status'] ?></td>
                  </tr>
            <?php } else {
               ?>
               <!-- <tr>
                  <td colspan="4" class="text-center text-primary text-bold">Kode OTP tidak ditemukan atau kadaluarsa <br>Silahkan ajukan permintaan OTP kembali</td>
               </tr> -->
            <?php
            } ?>

               </table>
            <!-- end loop -->

            </div>
         </div>
      </div>

      <!-- Check Satker -->
      <div class="box box-widget" style="margin-bottom: 10px">
         <div class="box-header with-border" style="background:#429258; color: #FFFFFF; padding: 5px 10px; border-bottom: solid 2px orange">
          <span class="small"> <i class="fa fa-eye"></i>&nbsp;&nbsp;Status <b>Revisi Satker</b></span>

         </div>
         <div class="box-body with-border">
            <div class="input-group input-group-sm">
             <input type="text" class="form-control" id="stsRev" value="" placeholder="Kode Satker ">
                 <span class="input-group-btn">
                   <button type="button" class="btn btn-flat btn-primary text-bold" onclick="stsRev()">Check Status</button>
                 </span>
            </div>
            <div class="" style="margin-top: 10px">

               <!-- ====== -->
               <table id="iGrid" class="table table-hover table-bordered small" style="border-spacing: 1; width: 100%; margin-bottom:0px">

                  <?php if($stsRev){ ?>
                        <thead>
                           <tr style="color: #fff; background-color:Blue;">
                              <th style="padding: 3px; background:#2B5590; color:#FFFFFF">Satker</th>
                              <th style="padding: 3px; background:#2B5590; color:#FFFFFF">Kewenangan</th>
                              <th style="padding: 3px; background:#2B5590; color:#FFFFFF">ID Revisi</th>
                              <th style="padding: 3px; background:#2B5590; color:#FFFFFF">Tahap</th>
                           </tr>
                        </thead>

                              <!-- data loop -->
                        <?php if($stsRev) foreach($stsRev as $row) { ?>
                              <tr>
                                 <td class="text-center"><?= $row['kdsatker'] ?></td>
                                 <td class="text-center"><?php
                                       if ($row['rev_level']=='1') echo "Revisi <b>DJA</b>";
                                       elseif ($row['rev_level']=='2' ) echo "Revisi <b>PA</b>";
                                       else echo "Revisi <b>Kanwil</b>";
                                       ?>
                                 </td>
                                 <td class="text-center"><?= $row['rev_id'] ?></td>
                                 <td class="text-center"><?php
                                       if ($row['rev_tahap'] == '2') echo "Penelitian";
                                       elseif ($row['rev_tahap'] == '6') echo "Penelaahan";
                                       else echo "Pengajuan";
                                       ?>
                                 </td>
                              </tr>
                     <?php }
                  }  else { ?>
                     <tr>
                        <td colspan="4" class="text-center text-primary">Satker tidak sedang mengajukan Revisi</td>
                     </tr>
                  <?php } ?>
               </table>

            </div>
         </div>
      </div>

      <!-- Check User  -->
      <div class="box box-widget" style="margin-bottom: 10px">
         <div class="box-header with-border" style="background:#605ca8; color: #FFFFFF; padding: 5px 10px; border-bottom: solid 2px orange">
          <span class="small"> <i class="fa fa-user"></i>&nbsp;&nbsp;User <b>SatuDJA</b>
             <a href="#"> <i class="fa fa-users pull-right" style="padding-top:3px; color:#FFF"> </i></a>
          </span>

         </div>
         <div class="box-body with-border">
            <!-- Reset Pass -->
            <div class="input-group input-group-sm">
             <input type="text" class="form-control" placeholder="User ID" value="<?= $userid['userid'] ?>" id="userid">
                 <span class="input-group-btn">
                   <button type="button" class="btn btn-flat btn-primary text-bold" value="" onclick="surel('chekuser')" >Check User</button>
                 </span>
            </div>
            <div class="" style="margin-top: 5px">
               <textarea class="form-control" style="font-size: 90%" rows="7" id="txt1" placeholder="Data User"><?= $userid['txt1'] ?></textarea>
            </div>
            
            <div class="row" style="margin-bottom: 10px">

<?php
   $grp = explode(";", $this->session->userdata('idusergroup')); 
   if(in_array('600',$grp) or in_array('601',$grp) or in_array('611',$grp) or in_array('612',$grp)) {} else { 
?>
      <div class="col-md-12">
         <div class="pull-left" style="margin-top: 10px">
            <button type="button" class="btn bg-orange btn-flat margin" style="margin:0px; padding: 7px 10px" tooltip="Isi Formulir" onclick="surel('form')"> <i class="fa fa-file-text-o"></i> </button> &nbsp;
            <button type="button" class="btn bg-orange btn-flat margin" style="margin:0px; padding: 7px 10px" tooltip="Cek Login" onclick="surel('ceklogin')"> <i class="fa fa-search-plus"></i> </button> &nbsp;
            <button type="button" class="btn bg-orange btn-flat margin" style="margin:0px; padding: 7px 10px" tooltip="User Baru" onclick="userbaru()"> <i class="fa fa-user-plus"></i> </button> &nbsp;
            <button type="button" class="btn bg-orange btn-flat margin" style="margin:0px; padding: 7px 10px" tooltip="Ubah Kewenangan" onclick="editwenang()"> <i class="fa fa-edit"></i> </button> &nbsp;
         </div>
         <div class="pull-right" style="margin-top: 10px">
            <button type="button" class="btn bg-orange btn-flat margin" style="margin:0px; padding: 7px 10px" tooltip="Pass Baru" onclick="surel('passbaru')"> <i class="fa fa-key"></i> </button> &nbsp;
            <button type="button" class="btn bg-orange btn-flat margin" style="margin:0px; padding: 7px 10px" tooltip="Reset Pass" onclick="surel('respas')"> <i class="fa fa-recycle"></i> </button>
         </div>
      </div>
<?php } ?>

            </div>

            <div class="" style="margin-top: 5px">
               <span class="small"><i>Sugest</i> jawaban:</span>
               <button class="small pull-right text-danger" onclick="copyTxt()">Copy Text</button>
               <textarea class="form-control" style="font-size: 90%; margin-top: 5px" rows="13" id="txt2" placeholder="Result"><?= $userid['txt2'] ?></textarea>
            </div>

         </div>
      </div>

      <!-- Reload & Revalid -->
      <div class="box box-widget" style="margin-bottom: 10px">
         <div class="box-header with-border" style="background:#CA5C54; color: #FFFFFF; padding: 5px 10px; border-bottom: solid 2px orange">
          <span class="small"> <i class="fa fa-recycle"></i>&nbsp;&nbsp;<b>Reload</b> & <b>Revalid</b></span>

         </div>
         <div class="box-body with-border">
            <div class="input-group">
               <div class="row">
                  <div class="col-sm-8">
                     <input type="text" class="form-control input-sm" id="reload_satker" value="" placeholder="Reload : Kode Satker">
                  </div>
                  <div class="col-sm-4">
                     <input type="text" class="form-control input-sm" id="reload_revke" value="" placeholder="Revisi Ke XX">
                  </div>
               </div>
                 <span class="input-group-btn align-bottom">
                   <button type="button" class="btn btn-flat btn-danger text-bold btn-sm" onclick="r3load()">&nbsp;Reload </button>
                 </span>
            </div>
         </div>
         <div class="box-body with-border" style="padding-top:0px">
            <div class="input-group input-group-sm">
             <input type="text" class="form-control" id="revalid_id" value="" placeholder="Revalid : ID Revisi ">
                 <span class="input-group-btn">
                   <button type="button" class="btn btn-flat btn-danger text-bold" onclick="revalid( this.value )">Revalid</button>
                 </span>
            </div>
         </div>
         <div class="box-body text-danger" style="padding-top:0px" id="pesan_kirim"></div>
      </div>
   </div>

<div id="pesan_kirim"></div>

   <!-- Modal -->
   <div id="myModal" class="modal fade" role="dialog">
      <div class="modal-dialog">

         <!-- Modal content-->
         <div class="modal-content">
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Modal Header</h4>
            </div>
            <div class="modal-body">
            <div id="summernote" name="bekicot">
               <span id="ModalItem" type="text">
                  <!-- ini isidsddsd -->
               </span>
            </div>
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
         </div>

      </div>
   </div>
   <!-- End Modal -->

</div>
<script type="text/javascript">
  $(document).ready(function() {
    $(".org").select2({
      minimumResultsForSearch:5
    });
  });
</script>

<script type="text/javascript">
  $('#sandbox-container .input-daterange').datepicker({
    format: "dd-mm-yyyy",
    language: "id",
    orientation: "bottom auto",
    autoclose: true,
    todayHighlight: true
  }).on('changeDate', function() {
    var sta = $('#start').val();
    var end = $('#end').val();
    var pil = <?= $forum['pil']; ?>

    window.location.href = "<?= site_url('puslay?q=p4sly') ?>" +'&sta='+ sta +"&end="+ end + "&pil=" + pil
   });

   function copyTxt() {
     var copyText = document.getElementById("txt2");
     copyText.select();
     document.execCommand("copy");
   }

   function otp() {
      var otp = $('#otp').val();
      if(otp !=''){
        window.location.href = '<?= site_url('puslay?q=p4sly') ?>' +'&otp='+ otp;
      }
   }

   function stsRev() {
      var stsRev = $('#stsRev').val();
      if(stsRev !=''){
        window.location.href = '<?= site_url('puslay?q=p4sly') ?>' +'&stsRev='+ stsRev;
      }
   }

  function surel(typ){
    var userid = $('#userid').val();

    if(userid !='' || typ=='form'){
        window.location.href = '<?= site_url('puslay?q=p4sly') ?>' +'&userid='+ userid +'&typ='+typ;
      }
  }

  function userbaru(){
    var userid = $('#userid').val();
    if(userid ==''){
      window.location.href ='<?= site_url('puslay?q=ad47s') ?>';
    } else {
      window.location.href ='<?= site_url('puslay?q=ad47s') ?>' + '&userid='+userid;
    }
  }

  function editwenang(){
    var userid = $('#userid').val();
    if (userid == '') return alert('User ID Kosong !, Masukan terlebih dahulu User ID');
    if (userid != '') {
      window.location.href = '<?= site_url('puslay?q=3d1ts') ?>' + '&userid=' + userid;
    }
  }

  function del(idparent,idchild,idforum){
   //   alert(idparent);
   // console.log(idparent);
   var result = confirm("Apakah Anda Yakin Menghapus komentar ini ??");
    if(result){
     window.location.href = '<?= site_url('puslay/?q=xz3ed') ?>' +'&idparent='+ idparent + '&idchild='+ idchild+ '&idforum='+ idforum;
    }
  }

  function pil(pil){
   var sta = $('#start').val();
   var end = $('#end').val();
   window.location.href = "<?= site_url('puslay?q=p4sly') ?>" +'&sta='+ sta +"&end="+ end + "&pil=" + pil
  }

  function revalid(){
      var data = $("#revalid_id").val();
      if(data.length != 15) return $('#pesan_kirim').html("Invalid Rev id !");
      $.ajax({
         url: "https://10.242.142.71/rest_server",
         type: "get", //send it through get method
         data: { 
            q: '362acf4daf650c7cfc355cddfa07972a', 
            r: data 
         },
         beforeSend: function(){
            $("#loader").show();
         },
         success: function(respon) {
            $("#loader").hide();
            // alert(respon);
            $('#pesan_kirim').html(respon);
         },
         error: function(xhr) {
         }
      });
  }

  function r3load(){
   var satker  = $("#reload_satker").val();
   var rev     = $("#reload_revke").val();
   if(satker.length!= 6 || rev.length !=2) return $('#pesan_kirim').html("Invalid Kdsatker dan Rev-ke !");
      $.ajax({
         url: "https://satudja.kemenkeu.go.id/r3cronjob/adk_dipa_2020_csv.php",
         type: "get", //send it through get method
         data: { 
            satker: satker, 
            rev: rev 
         },
         beforeSend: function(){
            $("#loader").show();
         },
         success: function(respon) {
            $("#loader").hide();
            $('#pesan_kirim').html(respon);
            // alert(respon);
         },
         error: function(xhr) {
         }
      });
  }

  $(document).ready(function() {
    $(".opt").select2({
      minimumResultsForSearch:5
    });
  });

</script>

