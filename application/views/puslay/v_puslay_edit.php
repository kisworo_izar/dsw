<link href="<?=base_url('assets/plugins/select2/select2.css');?>" rel="stylesheet" type="text/css" />
<script src="<?=base_url('assets/plugins/select2/select2.full.min.js'); ?>" type="text/javascript"></script>

<div class="row pull-center" >
    <div class="col-md-9">
        <div class="box box-widget">
            <div class="box-header with-border">
              <h3 class="box-title"> <?= $table['dat']['jenis'] ?> User </h3>
            </div>

            <div class="form-group">
                <form name="myForm" class="form-horizontal" action="<?php echo site_url('puslay?q=cr4ut') ?>" method="post" role="form">
                    <input type="hidden" name="jenis" value="edit">
                    <div class="form-group">
                        <label class="col-md-3 control-label">ID User</label>
                        <div   class="col-md-6">
                            <input type="text" id="iduser" name="iduser" class="form-control " placeholder="ID User" value="<?= $table['dat']['iduser'] ?> <?= $table['data']['kdsatker'] ?> ">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label">Nama</label>
                        <div   class="col-md-6">
                            <input type="text" id="nmuser" name="nmuser" class="form-control " placeholder="Nama User" value="<?= $table['dat']['nmuser'] ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label">NIP</label>
                        <div   class="col-md-6">
                            <input type="text" id="nip" name="nip" class="form-control " placeholder="NIP User" value="<?= $table['dat']['nip'] ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label">Jabatan</label>
                        <div   class="col-md-6">
                            <input type="text" id="jab" name="jab" class="form-control " placeholder="Jabatan" value="<?= $table['dat']['jabatan'] ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label">KL/Unit/Satker/Lokasi</label>
                        <div   class="col-md-1">
                            <input type="text" id="kddept" name="kddept" class="form-control " placeholder="Kode K/L" value="<?= $table['dat']['kddept'] ?>">
                        </div>
                        <div   class="col-md-1">
                            <input type="text" id="kdunit" name="kdunit" class="form-control " placeholder="Kode Unit" value="<?= $table['dat']['kdunit'] ?>">
                        </div>
                        <div   class="col-md-2">
                            <input type="text" id="kdsatker" name="kdsatker" class="form-control " placeholder="Kode Satker" value="<?= $table['dat']['kdsatker'] ?>">
                        </div>
                        <div   class="col-md-2">
                            <input type="text" id="kdlokasi" name="kdlokasi" class="form-control " placeholder="Kode Lokasi" value="<?= $table['dat']['kdlokasi'] ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label">NO HP</label>
                        <div   class="col-md-6">
                            <input type="text" id="nohp" name="nohp" class="form-control " placeholder="Nomer HP" value="<?= $table['dat']['nohp'] ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label">E-Mail</label>
                        <div   class="col-md-6">
                            <input type="text" id="email" name="email" class="form-control " placeholder="Email User" value="<?= $table['dat']['email'] ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label">Idusergroup</label>
                        <div   class="col-md-6" >
                            <select class="org form-control" id="idusergroup" name="idusergroup">
                                <option value="" hidden disabled >- Pilih Idusergroup - </option>
                                <?php
                                    foreach($table['sel'] as $row)  {
                                       $sel = ''; if ($table['dat']['idusergroup'] == $row['idusergroup']) $sel = 'selected'; 
                                        echo '<option value="'. $row['idusergroup'] .'" '. $sel .'>'. $row['idusergroup'] .' - '. $row['nmusergroup'] .'</option>';
                                    }
                                 ?>
                            </select>
                            <span class="text-small">*Pilih <b>userkl</b> jika yang ditambahkan adalah KL, *Pilih <b>userunit</b> jika yang ditambahkan adalah Unit, *Pilih <b>usersatker</b> jika yang ditambahkan adalah Satker</span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label">Password</label>
                        <div   class="col-md-6">
                            <input type="text" id="pswd" name="pswd" class="form-control " placeholder="Password" value="<?= $table['dat']['passasli'] ?>">
                        </div>
                    </div>
                    <div class="box-footer">
                        <button type="submit" id="simpan" class="btn btn-info pull-right">Simpan</button>
                    </div>

                </form>
            </div>
        </div>
    </div>

</div>

<script type="text/javascript">
  $(document).ready(function() {
    $(".org").select2({
      minimumResultsForSearch:5
    });
  });
</script>
