<!-- Google Fonts -->
<link href="https://fonts.googleapis.com/css?family=Roboto:400,700&amp;subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
<link href="<?=base_url();?>assets/plugins/materialicons/materialicons.css" rel="stylesheet" type="text/css">
<!-- bootstrapValidator -->
<link rel="stylesheet" href="<?=base_url();?>assets/plugins/bootstrapvalidator/css/bootstrapValidator.min.css"/>
<!-- SweetAlert2 -->
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
<!-- Waves Effect Css -->
<link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/plugins/node-waves/waves.css">
<!-- Animation Css -->
<link href="<?=base_url();?>assets/plugins/animate-css/animate.css" rel="stylesheet" />
<!-- Bootstrap DatePicker Css -->
<link href="<?=base_url();?>assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.css" rel="stylesheet" />
<!-- Bootstrap Select Css -->
<link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/plugins/bootstrap-select/css/bootstrap-select.css">
<!-- Dropify -->
<link href="<?=base_url();?>assets/plugins/dropify/css/dropify.min.css" rel="stylesheet">
<!-- AdminBSB Custom Css -->
<link href="<?=base_url();?>assets/plugins/adminbsb/style.css" rel="stylesheet">

<style type="text/css">
  form .dropify-wrapper p {
    text-align: center;
  }

  .swal2-radio [type="radio"]:not(:checked),
  .swal2-radio [type="radio"]:checked {
    position: unset;
    opacity: 1;
  }

  .form-group .form-line.focused .form-label {
    top: -15px;
  }

  .bs-searchbox .form-control {
    width: 88%;
  }

  .capitalise {
    text-transform:capitalize;
  }

  .dropdown-header {
    padding: 3px 20px !important;
  }
</style>

<div class="row">
  <div class="col-md-12">

    <div class="card">
      <div class="body">
        <div id="st_wizard">
          <h3>Informasi Surat Tugas</h3>
          <fieldset>
            <form id="st_form">
              <br>
              <input name="id" class="id" type="text" hidden>
              <input name="filename" class="filename" type="text" hidden>
              <div class="row clearfix">
                <div class="col-sm-6">
                  <div class="form-group form-float">
                    <div class="form-line form-line-selectpicker">
                      <select name="st_type_id" class="form-control show-tick capitalise selectpicker st_type_id" data-live-search="true" data-size="5" title="Pilih Kategori">
                      </select>
                      <label class="form-label">Kategori</label>
                    </div>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group form-float">
                    <div class="form-line">
                      <input name="no" type="text" class="form-control no">
                      <label class="form-label">Nomor Surat</label>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row clearfix">
                <div class="col-sm-6">
                  <div class="form-group form-float">
                    <div class="form-line">
                      <input name="startdate" type="text" class="form-control datepicker startdate">
                      <label class="form-label">Dari Tanggal</label>
                    </div>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group form-float">
                    <div class="form-line">
                      <input name="enddate" type="text" class="form-control datepicker enddate">
                      <label class="form-label">Sampai Tanggal</label>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row clearfix">
                <div class="col-sm-12">
                  <div class="form-group form-float">
                    <div class="form-line">
                      <input name="dokumen" type="file" class="dropify" data-max-file-size-preview="2M" id="dokumen" />
                    </div>
                  </div>
                </div>
              </div>
            </form>
          </fieldset>

          <h3>Daftar Pegawai</h3>
          <fieldset class="row">
            <div class="icon-button-demo">
              <button type="button" class="btn btn-primary waves-effect" data-toggle="modal" data-target="#st_pegawai_modal" data-action="add" id="st_pegawai_add">
                <i class="material-icons">person_add</i>
              </button>
            </div>
            <div id="st_pegawai"></div>

          </fieldset>

        </div>
      </div>
    </div>

  </div>
</div>

<!-- ST PEGAWAI MODAL -->
<div class="modal fade" id="st_pegawai_modal" tabindex="-1" role="dialog" style="display: none;">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="card">
        <div class="body">
          <form id="st_pegawai_form">
            <br>
            <input name="id" class="id" type="text" hidden>
            <input name="st_id" class="st_id" type="text" hidden>
            <div class="row clearfix">
              <div class="col-sm-12">
                <div class="form-group form-float">
                  <div class="form-line">
                    <select name="nip" class="form-control show-tick capitalise select-pegawai nip" data-live-search="true" data-size="5" title="Pilih pegawai">
                    </select>
                    <label class="form-label">Pegawai</label>
                  </div>
                </div>
              </div>
            </div>
            
            <button type="submit" class="btn btn-primary m-t-15 waves-effect" id="st_pegawai_save">SIMPAN</button>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- ST DATE MODAL -->
<div class="modal fade" id="st_date_modal" tabindex="-1" role="dialog" style="display: none;">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="card">
        <div class="body">
          <form id="st_date_form">
            <br>
            <input name="id" class="id" type="text" hidden>
            <input name="st_pegawai_id" class="st_pegawai_id" type="text" hidden>
            <div class="col-sm-6">
              <div class="form-group form-float">
                <div class="form-line">
                  <input name="startdate" type="text" class="form-control datepicker startdate">
                  <label class="form-label">Dari Tanggal</label>
                </div>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="form-group form-float">
                <div class="form-line">
                  <input name="enddate" type="text" class="form-control datepicker enddate">
                  <label class="form-label">Sampai Tanggal</label>
                </div>
              </div>
            </div>
            
            <button type="submit" class="btn btn-primary m-t-15 waves-effect" id="st_date_save">SIMPAN</button>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- imagesloaded -->
<script src="<?=base_url();?>assets/plugins/imagesloaded/imagesloaded.pkgd.min.js"></script>
<!-- Masonry -->
<script src="<?=base_url();?>assets/plugins/masonry/masonry.pkgd.min.js"></script>
<!-- bootstrapValidator -->
<script type="text/javascript" src="<?=base_url();?>assets/plugins/bootstrapvalidator/js/bootstrapValidator.js"></script>
<script type="text/javascript" src="<?=base_url();?>assets/plugins/bootstrapvalidator/js/language/id_ID.js"></script>
<!-- jquery.serializeObject -->
<script type="text/javascript" src="<?=base_url();?>assets/plugins/jQuery.serializeObject/jquery.serializeObject.min.js"></script>
<!-- Bootstrap Select Css -->
<script type="text/javascript" src="<?=base_url();?>assets/plugins/bootstrap-select/js/bootstrap-select.min.js"></script>
<!-- Slimscroll Plugin Js -->
<script src="<?=base_url();?>assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- JQuery Steps Plugin Js -->
<script src="<?=base_url();?>assets/plugins/jquery-steps/jquery.steps.js"></script>
<!-- Bootstrap Notify Plugin Js -->
<script src="<?=base_url();?>assets/plugins/bootstrap-notify/bootstrap-notify.js"></script>
<!-- Waves Effect Plugin Js -->
<script type="text/javascript" src="<?=base_url();?>assets/plugins/node-waves/waves.js"></script>
<!-- Bootstrap Datepicker Plugin Js -->
<script src="<?=base_url();?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<!-- Dropify -->
<script src="<?=base_url();?>assets/plugins/dropify/js/dropify.min.js"></script>
<!-- AdminBSB Custom Js -->
<script src="<?=base_url();?>assets/plugins/adminbsb/admin.js"></script>

<script type="text/javascript">
  $(function(){

    /* ST WIZARD */

    var st_wizard = $('#st_wizard').show();

    st_wizard.steps({
      headerTag: 'h3',
      bodyTag: 'fieldset',
      transitionEffect: 'slideLeft',
      labels: {
        next: 'Simpan & Lanjut',
        previous: 'Kembali',
        finish: 'Ajukan!',
      },
      onInit: function (event, currentIndex) {
        $.AdminBSB.input.activate();

        /*Set tab width*/
        var $tab = $(event.currentTarget).find('ul[role="tablist"] li');
        var tabCount = $tab.length;
        $tab.css('width', (100 / tabCount) + '%');

        /*set button waves effect*/
        setButtonWavesEffect(event);
      },
      onStepChanging: function (event, currentIndex, newIndex) {
        if (currentIndex > newIndex) { return true; }

        if (currentIndex < newIndex) {
          st_wizard.find('.body:eq(' + newIndex + ') label.error').remove();
          st_wizard.find('.body:eq(' + newIndex + ') .error').removeClass('error');
        }

        var form = $('#st_form').data('bootstrapValidator');

        form.validate();

        return form.isValid();
      },
      onStepChanged: function (event, currentIndex, priorIndex) {
        setButtonWavesEffect(event);
      },
      onFinishing: function (event, currentIndex) {
        /*form.validate().settings.ignore = ':disabled';*/
        /*return form.valid();*/
        return true;
      },
      onFinished: function (event, currentIndex) {
        /* UPDATE STATE: DIAJUKAN */
        approval({id: $('#st_form .id').val(), approval: 1, approvalnote: ''})
      }
    });

    /* ST FORM */
    $('#st_form')
    /* DATEPICKER */
    .find('.datepicker')
    .datepicker({
      autoclose: true,
      format: "yyyy-mm-dd",
      language: "id",
    }).on('changeDate', function(ev) {
      $('#st_form').bootstrapValidator('revalidateField', $(this).attr('name'));
      $(this).closest('.form-line').addClass('focused')
    })
    .end()
    /* VALIDATOR */
    .bootstrapValidator({
      excluded: [':disabled', ':hidden'],
      feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
        st_type_id: {
          validators: {
            notEmpty: {}
          }
        },
        no: {
          validators: {
            notEmpty: {}
          }
        },
        startdate: {
          validators: {
            notEmpty: {},
            date: { format: 'YYYY-MM-DD' }
          }
        },
        enddate: {
          validators: {
            notEmpty: {},
            date: { format: 'YYYY-MM-DD' }
          }
        },
        dokumen: {
          validators: {
            notEmpty: {},
            file: {
              extension: 'pdf',
              type: 'application/pdf',
              maxSize: 2097152
            }
          }
        },
      }
    })
    /* SUBMIT */      
    .on('success.form.bv', function(e) {
      e.preventDefault();

      /*Upload file*/
      var formData = new FormData();
      formData.append('file', $('#dokumen')[0].files[0]);

      $.ajax({
        url: "<?= $this->config->item('api_server') ?>st/upload",
        data: formData,
        type: 'POST',
        processData: false,
        contentType: false,
        success: function(result) {
          if (result.status == 200) {

            /*CREATE / UPDATE ST*/
            if (result.values) {
              $('#st_form .filename').val(result.values);
            }

            var data = $('#st_form').serializeObject(),
            type = (data.id) ? 'PUT' : 'POST';

            data.updatedby = "<?= $this->session->userdata('nip') ?>";
            if (!(data.nip)) {
              data.createdby = "<?= $this->session->userdata('nip') ?>";
            }

            $.ajax({
              url: "<?= $this->config->item('api_server') ?>st",
              data: data,
              type: type,
              dataType: 'json',
              success: function(result) {
                if (result.status == 200) {
                  showNotification('bg-green', 'Surat Tugas Tersimpan')
                  $('#st_form .id').val(result.values);
                  $('#st_pegawai_form .st_id').val(result.values);
                  loadSTPegawai();
                } else {
                  showNotification('bg-red', 'Surat Tugas Gagal Tersimpan ' + '(' + result.values + ')')
                  st_wizard.steps('previous');
                }
              },
              error: function(result) {
                showNotification('bg-red', 'Surat Tugas Error')
              }
            });
          } else {
            showNotification('bg-red', 'Surat Tugas Gagal Upload ' + '(' + result.values + ')')
          }
        },
        error: function(result) {
          showNotification('bg-red', 'Surat Tugas Error')
        }
      });
    })

    /* ST PEGAWAI FORM */
    $('#st_pegawai_form')
    /* SELECTPICKER */
    .find('select')
    .selectpicker()
    .change(function(e) {
      $(this).closest('.form-line').addClass('focused')
    })
    .end()
    /* VALIDATOR */
    .bootstrapValidator({
      excluded: [':disabled', ':hidden'],
      feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
        nip: {
          validators: {
            notEmpty: {}
          }
        }
      }
    })
    /* SUBMIT */      
    .on('success.form.bv', function(e) {
      e.preventDefault();

      $('#st_pegawai_modal').modal('toggle');

      /*CREATE & UPDATE ST PEGAWAI*/
      var data = $('#st_pegawai_form').serializeObject(),
      type = ($('#st_pegawai_save').attr('data-action') == 'add') ? 'POST' : 'PUT';

      $.ajax({
        url: "<?= $this->config->item('api_server') ?>st/pegawai",
        data: data,
        type: type,
        dataType: 'json',
        success: function(result) {
          if (result.status == 200) {
            loadSTPegawai();
            showNotification('bg-green', 'Data Pegawai Tersimpan')
          } else {
            showNotification('bg-red', 'Data Pegawai Gagal Tersimpan ' + '(' + result.values + ')')
          }
        },
        error: function(result) {
          showNotification('bg-red', 'Data Pegawai Error')
        }
      });
    })

    /* ST PEGAWAI DELETE */
    $('#st_pegawai').on('click', 'a.delete-st_pegawai', function(){
      $.ajax({
        url: "<?= $this->config->item('api_server') ?>st/pegawai",
        data: {id: $(this).attr('data-id')},
        type: 'DELETE',
        dataType: 'json',
        success: function(result) {
          if (result.status == 200) {
            showNotification('bg-green', 'Data Pegawai Terhapus')
            loadSTPegawai();
          } else {
            showNotification('bg-red', 'Data Pegawai Gagal Terhapus ' + '(' + result.values + ')')
          }
        },
        error: function(result) { 
          showNotification('bg-red', 'Data Pegawai Error')
        }
      });
    })

    /* ST PEGAWAI MODAL */
    $('#st_pegawai_modal').on('shown.bs.modal', function (event) {
      var action = $(event.relatedTarget).data('action');

      if (action == 'edit') {
        var st_pegawai = $(event.relatedTarget).data('st_pegawai');
        $('#st_pegawai_form').bootstrapValidator('resetForm', true);
        $('#st_pegawai_form .id').val(st_pegawai.id);
        $('#st_pegawai_form .nip').selectpicker('val', st_pegawai.nip);
      }

      $('#st_pegawai_save').attr('data-action', action);
    })

    /* ST DATE FORM */
    $('#st_date_form')
    /* DATEPICKER */
    .find('.datepicker')
    .datepicker({
      autoclose: true,
      format: "yyyy-mm-dd",
      language: "id",
    }).on('changeDate', function(ev) {
      $('#st_date_form').bootstrapValidator('revalidateField', $(this).attr('name'));
      $(this).closest('.form-line').addClass('focused')
    })
    .end()
    /* VALIDATOR */
    .bootstrapValidator({
      excluded: [':disabled', ':hidden'],
      feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
        startdate: {
          validators: {
            notEmpty: {},
            date: { format: 'YYYY-MM-DD' }
          }
        },
        enddate: {
          validators: {
            notEmpty: {},
            date: { format: 'YYYY-MM-DD' }
          }
        }
      }
    })
    /* SUBMIT */      
    .on('success.form.bv', function(e) {
      e.preventDefault();

      $('#st_date_modal').modal('toggle');

      /*CREATE & UPDATE ST DATE*/
      var data = $('#st_date_form').serializeObject(),
      type = ($('#st_date_save').attr('data-action') == 'add') ? 'POST' : 'PUT';

      $.ajax({
        url: "<?= $this->config->item('api_server') ?>st/date",
        data: data,
        type: type,
        dataType: 'json',
        success: function(result) {
          if (result.status == 200) {
            loadSTPegawai();
            showNotification('bg-green', 'Data Tanggal Tersimpan')
          } else {
            showNotification('bg-red', 'Data Tanggal Gagal Tersimpan ' + '(' + result.values + ')')
          }
        },
        error: function(result) {
          showNotification('bg-red', 'Data Tanggal Error')
        }
      });
    })

    /* ST DATE DELETE */
    $('#st_pegawai').on('click', 'a.delete-st_date', function(){
      $.ajax({
        url: "<?= $this->config->item('api_server') ?>st/date",
        data: {id: $(this).attr('data-id')},
        type: 'DELETE',
        dataType: 'json',
        success: function(result) {
          if (result.status == 200) {
            showNotification('bg-green', 'Data Tanggal Terhapus')
            loadSTPegawai();
          } else {
            showNotification('bg-red', 'Data Tanggal Gagal Terhapus ' + '(' + result.values + ')')
          }
        },
        error: function(result) { 
          showNotification('bg-red', 'Data Tanggal Error')
        }
      });
    })

    /* ST DATE MODAL */
    $('#st_date_modal').on('shown.bs.modal', function (event) {
      var action = $(event.relatedTarget).data('action');
      var st_date = $(event.relatedTarget).data('st_date');

      if (action == 'edit') {
        $('#st_date_form').bootstrapValidator('resetForm', true);
        $('#st_date_form .id').val(st_date.id);
        $('#st_date_form .startdate').datepicker('setDate', new Date(st_date.startdate));
        $('#st_date_form .enddate').datepicker('setDate', new Date(st_date.enddate));
      }

      $('#st_date_form .st_pegawai_id').val(st_date.st_pegawai_id);

      $('#st_date_save').attr('data-action', action);
    })

    /*Dropify*/
    $('.dropify').dropify({
      messages: {
        'default': 'Drag dan drop file/dokumen di sini atau klik',
        'replace': 'Drag dan drop atau klik untuk mengganti',
        'remove':  'Hapus',
        'error':   'Ooops, terjadi kesalahan.'
      }
    });

    /*Pegawai Reference*/
    $.ajax({
      url: '<?= $this->config->item('api_server') ?>pegawai',
      type: 'GET',
      dataType: 'json',
      success: function(result) {
        if (result.status == 200) {
          var html = '';
          $.each(result.values, function(index, item){
            html += '<option value="'+item.nip18+'">'+item.nama+' - '+item.nip18+'</option>'
          })
          $('select.select-pegawai').append(html).selectpicker('refresh');
        } else {
          console.log('Gagal load data pegawai');
        }
      }
    })

    /* ST GROUP REFERENCE */
    $.ajax({
      url: '<?= $this->config->item('api_server') ?>stgroup',
      type: 'GET',
      dataType: 'json',
      success: function(result) {
        if (result.status == 200) {
          $('select.st_type_id').html(buildGroup(result.values)).selectpicker('refresh');
        } else {
          console.log('Gagal load data Kategori Surat Tugas');
        }
      }
    })

    /* NEW/EDIT HANDLER */
    loadST();

  });



function buildGroup(items) {
  if (items.length == 0) { return ''; }

  var html = "";

  $.each(items, function(index, item){
    if (item.state == '1') {
      if (item.children) {
        html += '<optgroup label="'+item.name+'">';
        html += buildGroup(item.children);
        html += '</optgroup>';
      } else {
        html += '<option value='+item.id+'>'+item.name+' (' + item.code + ')</option>';
      }
    }
  })

  return html;
}

function loadST(){
  var st_id = '<?= $st_id ?>';

  /* ST NEW */
  if (!(st_id)) {
    var drEvent = $('#dokumen').dropify();

    drEvent.on('dropify.afterClear', function(event, element){
      $('#st_form').bootstrapValidator('revalidateField', 'dokumen');
      $('#st_form .filename').val('');
    });
    
    return true;
  } 

  /* ST EDIT */
  $.ajax({
    url: "<?= $this->config->item('api_server') ?>st/"+st_id,
    type: 'GET',
    dataType: 'json',
    success: function(result) {
      if (result.status == 200) {
        if (result.values.length < 1) {
          /* REDIRECT */
          window.location.replace("<?=base_url();?>st" );
        }

        var st = result.values[0];

        $('#st_form .id').val(st.id);
        $('#st_form .st_type_id').selectpicker('val', st.st_type_id);
        $('#st_form .no').val(st.no).closest('.form-line').addClass('focused');
        $('#st_form .startdate').datepicker('setDate', new Date(st.startdate));
        $('#st_form .enddate').datepicker('setDate', new Date(st.enddate));

        var defaultFile = (st.filename) ? '<?=base_url();?>DSW/files/upload_milea/dosir/' + st.filename : '';
        var drEvent = $('#dokumen').dropify({ defaultFile: defaultFile });

        drEvent.on('dropify.afterClear', function(event, element){
          $('#st_form').bootstrapValidator('revalidateField', 'dokumen');
          $('#st_form .filename').val('');
        });

        drEvent = drEvent.data('dropify');
        drEvent.resetPreview();
        drEvent.clearElement();
        drEvent.settings.defaultFile = defaultFile;
        drEvent.destroy();
        drEvent.init();

        if (st.filename) $('#st_form').bootstrapValidator('updateStatus', 'dokumen', 'VALID');
        
        $('#st_form .filename').val(st.filename);
      } else {
        /* REDIRECT */
        window.location.replace("<?=base_url();?>st" );
      }
    },
    error: function(result) {
      /* REDIRECT */
      window.location.replace("<?=base_url();?>st" );
    }
  });
}

function loadSTPegawai(){
  var st_id = $('#st_pegawai_form .st_id').val();

  $.ajax({
    url: "<?= $this->config->item('api_server') ?>st/pegawai/"+st_id,
    type: 'GET',
    dataType: 'json',
    success: function(result) {
      if (result.status == 200) {
        var html = '<div class="list-group">';

        if (result.values.length < 1) {
          html += '<a href="javascript:void(0);" class="list-group-item col-orange font-italic" data-toggle="modal" data-target="#st_pegawai_modal" data-action="add">Daftar Pegawai Belum Diisi</a>';
        }

        for (var i = 0; i < result.values.length; i++) {
          var st_pegawai = result.values[i];
          html += '<div id="pegawai-'+st_pegawai.id+'">\
          <div id="pegawai-'+st_pegawai.id+'-info">\
          <div class="dropdown">\
          <a href="javascript:void(0);" type="button" class="list-group-item dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">\
          <i class="material-icons pull-right">more_vert</i>\
          <span>'+st_pegawai.nama.toUpperCase()+' <small>'+st_pegawai.nip+'</small></span>\
          </a>\
          <ul class="dropdown-menu pull-right">\
          <li><a href="javascript:void(0);" class=" waves-effect waves-block" data-toggle="modal" data-target="#st_pegawai_modal" data-action="edit" data-st_pegawai=\''+JSON.stringify(st_pegawai)+'\'>Ubah</a></li>\
          <li><a href="javascript:void(0);" class=" waves-effect waves-block delete-st_pegawai" data-id="'+st_pegawai.id+'">Hapus</a></li>\
          <li role="separator" class="divider"></li>\
          <li><a href="javascript:void(0);" class=" waves-effect waves-block" data-toggle="modal" data-target="#st_date_modal" data-action="add" data-st_date=\''+JSON.stringify({st_pegawai_id: st_pegawai.id})+'\'>Tambah Tanggal Hadir</a></li>\
          </ul>\
          </div>\
          </div>';

          if (st_pegawai.dates.length < 1) {
            html += '<a href="javascript:void(0);" class="list-group-item col-orange font-italic" data-toggle="modal" data-target="#st_date_modal" data-action="add" data-st_date=\''+JSON.stringify({st_pegawai_id: st_pegawai.id})+'\'><small>Tanggal Hadir Belum Diisi</small></a>';
          }

          for (var j = 0; j < st_pegawai.dates.length; j++) {
            var st_date = st_pegawai.dates[j];
            html += '<div id="date-'+st_date.id+'">\
            <div class="dropdown">\
            <a href="javascript:void(0);" type="button" class="list-group-item dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">\
            <i class="material-icons pull-right">more_vert</i>\
            <i class="material-icons">date_range</i><small style="vertical-align: super;">'+formatDate(st_date.startdate)+' - '+formatDate(st_date.enddate)+'</small>\
            </a>\
            <ul class="dropdown-menu pull-right">\
            <li><a href="javascript:void(0);" class=" waves-effect waves-block" data-toggle="modal" data-target="#st_date_modal" data-action="edit" data-st_date=\''+JSON.stringify(st_date)+'\'>Ubah</a></li>\
            <li><a href="javascript:void(0);" class=" waves-effect waves-block delete-st_date" data-id="'+st_date.id+'">Hapus</a></li>\
            </ul>\
            </div>\
            </div>';
          }

          html += '</div>';
        }

        html += '</div>';

        $('#st_pegawai').html(html)
      } else {
        showNotification('bg-red', 'Gagal Mengambil Daftar Pegawai ' + '(' + result.values + ')')
      }
    },
    error: function(result) {
      showNotification('bg-red', 'Error Mengambil Daftar Pegawai')
    }
  });
}

function approval(data){
  data.approvedby = "<?= $this->session->userdata('nip') ?>";

  $.ajax({
    url: "<?= $this->config->item('api_server') ?>st/approval",
    data: data,
    type: 'PUT',
    dataType: 'json',
    success: function(result) {
      if (result.status == 200) {
        Swal.fire({ title: 'Status Surat Tugas Diperbaharui', type: 'success' })
        /* REDIRECT */
        window.location.replace("<?=base_url();?>st" );
      } else {
        Swal.fire({ title: 'Status Surat Tugas Gagal Diperbaharui (' + result.values + ')', type: 'error' })
      }
    },
    error: function(result) { Swal.fire({ title: 'Status Surat Tugas Error', type: 'error' }) }
  });
}

function setButtonWavesEffect(event) {
  $(event.currentTarget).find('[role="menu"] li a').removeClass('waves-effect');
  $(event.currentTarget).find('[role="menu"] li:not(.disabled) a').addClass('waves-effect');
}

function showNotification(colorName, text) {
  var 
  placementFrom   = 'top', 
  placementAlign  =  'center', 
  animateEnter    = '', 
  animateExit     = '';

  if (colorName === null || colorName === '') { colorName = 'bg-black'; }
  if (text === null || text === '') { text = 'Turning standard Bootstrap alerts'; }
  if (animateEnter === null || animateEnter === '') { animateEnter = 'animated fadeInDown'; }
  if (animateExit === null || animateExit === '') { animateExit = 'animated fadeOutUp'; }
  var allowDismiss = true;

  $.notify({
    message: text
  },
  {
    type: colorName,
    allow_dismiss: allowDismiss,
    newest_on_top: true,
    timer: 1000,
    placement: {
      from: placementFrom,
      align: placementAlign
    },
    animate: {
      enter: animateEnter,
      exit: animateExit
    },
    template: '<div data-notify="container" class="bootstrap-notify-container alert alert-dismissible {0} ' + (allowDismiss ? "p-r-35" : "") + '" role="alert">' +
    '<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>' +
    '<span data-notify="icon"></span> ' +
    '<span data-notify="title">{1}</span> ' +
    '<span data-notify="message">{2}</span>' +
    '<div class="progress" data-notify="progressbar">' +
    '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
    '</div>' +
    '<a href="{3}" target="{4}" data-notify="url"></a>' +
    '</div>'
  });
}

function formatDate(date, time){
  var options;
  if (time) {
    options = {dateStyle: "medium", timeStyle: "medium"};
  } else {
    options = {dateStyle: "medium"};
  }
  return new Date(date).toLocaleDateString('id-ID', options);
}
</script>
