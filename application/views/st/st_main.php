<!-- Google Fonts -->
<link href="https://fonts.googleapis.com/css?family=Roboto:400,700&amp;subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
<link href="<?=base_url();?>assets/plugins/materialicons/materialicons.css" rel="stylesheet" type="text/css">
<!-- bootstrapValidator -->
<link rel="stylesheet" href="<?=base_url();?>assets/plugins/bootstrapvalidator/css/bootstrapValidator.min.css"/>
<!-- SweetAlert2 -->
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
<!-- Waves Effect Css -->
<link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/plugins/node-waves/waves.css">
<!-- Bootstrap DatePicker Css -->
<link href="<?=base_url();?>assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.css" rel="stylesheet" />
<!-- Bootstrap Select Css -->
<link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/plugins/bootstrap-select/css/bootstrap-select.css">
<!-- Dropify -->
<link href="<?=base_url();?>assets/plugins/dropify/css/dropify.min.css" rel="stylesheet">
<!-- AdminBSB Custom Css -->
<link href="<?=base_url();?>assets/plugins/adminbsb/style.css" rel="stylesheet">

<style type="text/css">
  form .dropify-wrapper p {
    text-align: center;
  }

  .swal2-radio [type="radio"]:not(:checked),
  .swal2-radio [type="radio"]:checked {
    position: unset;
    opacity: 1;
  }

  .form-group .form-line.focused .form-label {
    top: -15px;
  }

  .bs-searchbox .form-control {
    width: 88%;
  }

  .capitalise {
    text-transform:capitalize;
  }
</style>

<div class="row">
  <div class="col-md-12">
    <div class="card">
      <div class="header">
        <button type="button" onclick="window.location.href = '<?=base_url();?>st?q=2FZTv';" class="btn btn-primary btn-lg waves-effect">
          <i class="material-icons">add</i>
        </button>
        <button type="button" class="btn btn-primary btn-lg waves-effect" data-toggle="modal" data-target="#stFilterModal" data-action="add">
          <i class="material-icons">filter_list</i>
        </button>
        <div class="col-xs-6 col-sm-4 pull-right">
          <div class="input-group pull-right">
            <div class="form-line">
              <input type="text" class="form-control" placeholder="Cari..." id="searchST">
            </div>
            <span class="input-group-addon">
              <i class="material-icons">search</i>
            </span>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="grid capitalise" id="stList">

  </div>
</div>

<div class="row" id="st-load" hidden>
  <div class="col-xs-12 align-center">
    <div class="preloader">
      <div class="spinner-layer pl-teal">
        <div class="circle-clipper left">
          <div class="circle"></div>
        </div>
        <div class="circle-clipper right">
          <div class="circle"></div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="row" id="st-empty" hidden>
  <div class="col-xs-12">
    <div class="alert alert-warning">Tidak ada Surat Tugas yang ditampilkan</div>
  </div>
</div>

<div class="modal fade" id="stFilterModal" tabindex="-1" role="dialog" style="display: none;">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="card">
        <div class="header">
          <div class="row clearfix">
            <div class="col-xs-12 col-sm-6">
              <h2 id="modalTitle">Filter Daftar Surat Tugas</h2>
            </div>
          </div>
        </div>
        <div class="body">
          <form id="stFilterForm">
            <br>
            <div class="row clearfix">
              <div class="col-sm-6">
                <div class="form-group form-float">
                  <div class="form-line">
                    <select name="approval" class="form-control show-tick capitalise" title="Pilih Status" >
                      <option value=" ">Semua</option>
                      <option value="0">Konsep</option>
                      <option value="1">Menunggu Penetapan</option>
                      <option value="2">Ditetapkan</option>
                      <option value="3">Ditolak</option>
                      <option value="4">Menunggu Persetujuan Pembatalan</option>
                    </select>
                    <label class="form-label">Status</label>
                  </div>
                </div>
              </div>
              <div class="col-sm-6">
                <div class="form-group form-float">
                  <div class="form-line">
                    <select name="group_id" class="form-control show-tick selectpicker capitalise" data-live-search="true" data-size="5" title="Pilih Kategori" >
                      <option value=" ">Semua</option>

                    </select>
                    <label class="form-label">Kategori</label>
                  </div>
                </div>
              </div>
            </div>
            <div class="row clearfix">
              <div class="col-sm-6">
                <div class="form-group form-float">
                  <div class="form-line">
                    <input name="startdate" type="text" class="form-control datepicker">
                    <label class="form-label">Dari Tanggal</label>
                  </div>
                </div>
              </div>
              <div class="col-sm-6">
                <div class="form-group form-float">
                  <div class="form-line">
                    <input name="enddate" type="text" class="form-control datepicker">
                    <label class="form-label">Sampai Tanggal</label>
                  </div>
                </div>
              </div>
            </div>
            <button type="button" class="btn btn-primary m-t-15 waves-effect" id="filterST">FILTER</button>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- imagesloaded -->
<script src="<?=base_url();?>assets/plugins/imagesloaded/imagesloaded.pkgd.min.js"></script>
<!-- Masonry -->
<script src="<?=base_url();?>assets/plugins/masonry/masonry.pkgd.min.js"></script>
<!-- bootstrapValidator -->
<script type="text/javascript" src="<?=base_url();?>assets/plugins/bootstrapvalidator/js/bootstrapValidator.js"></script>
<script type="text/javascript" src="<?=base_url();?>assets/plugins/bootstrapvalidator/js/language/id_ID.js"></script>
<!-- jquery.serializeObject -->
<script type="text/javascript" src="<?=base_url();?>assets/plugins/jQuery.serializeObject/jquery.serializeObject.min.js"></script>
<!-- Bootstrap Select Css -->
<script type="text/javascript" src="<?=base_url();?>assets/plugins/bootstrap-select/js/bootstrap-select.min.js"></script>
<!-- Slimscroll Plugin Js -->
<script src="<?=base_url();?>assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- JQuery Steps Plugin Js -->
<script src="<?=base_url();?>assets/plugins/jquery-steps/jquery.steps.js"></script>
<!-- Waves Effect Plugin Js -->
<script type="text/javascript" src="<?=base_url();?>assets/plugins/node-waves/waves.js"></script>
<!-- Bootstrap Datepicker Plugin Js -->
<script src="<?=base_url();?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<!-- Dropify -->
<script src="<?=base_url();?>assets/plugins/dropify/js/dropify.min.js"></script>
<!-- AdminBSB Custom Js -->
<script src="<?=base_url();?>assets/plugins/adminbsb/admin.js"></script>
<!-- <script src="<?=base_url();?>assets/plugins/adminbsb/demo.js"></script> -->


<script type="text/javascript">

  var 
  st_page = 1,
  st_req;
  
  $(function(){

    /* INFINITE SCROLL */
    $(window).bind('scroll', function() {
      // console.log($(window).innerHeight(), $(window).scrollTop(), $("body").height())
      if($(window).scrollTop() >= $('#stList').offset().top + $('#stList').outerHeight() - window.innerHeight) {

        st();

      }
    });

    /*FILTER ST*/
    $('#stFilterForm')
    .find('select')
    .selectpicker()
    .change(function(e) {
      $(this).closest('.form-line').addClass('focused')
    })
    .end()
    .find('.datepicker')
    .datepicker({
      autoclose: true,
      format: "yyyy-mm-dd",
      language: "id",
    }).on('changeDate', function(ev) {
      $(this).closest('.form-line').addClass('focused')
    })
    .end()

    $('#filterST').click(function(){
      $('#stFilterModal').modal('hide');
      $('#stList').html('')
      st_page = 1;
      st_req = false;
      st();
    })

    /*SEARCH ST*/
    $('#searchST').keyup(function(e){
      $('#stList .grid-item').each(function() {
        if ($(this).text().toLowerCase().indexOf( $('#searchST').val().toLowerCase() ) < 0){
          $(this).hide()
        } else {
          $(this).show()
        }
      });
      var $grid = $('.grid').masonry();
      $grid.masonry('reloadItems');
      $grid.masonry('layout');
    })

    /*DELETE ST*/
    $('#stList').on('click', 'a.deleteST', function() {
      Swal.fire({
        title: 'Hapus Surat Tugas?',
        type: 'warning',
        confirmButtonText: 'Ya',
        showCancelButton: true,
        cancelButtonText: 'Tidak',
        focusCancel:true,
        reverseButtons: true,
        showLoaderOnConfirm: true,
        preConfirm: () => {}
      }).then((result) => {
        if (result.value) {
          $.ajax({
            url: "<?= $this->config->item('api_server') ?>st",
            data: {id: $(this).attr('data-id')},
            type: 'DELETE',
            dataType: 'json',
            success: function(result) {
              if (result.status == 200) {
               $('#filterST').click()
               Swal.fire({ title: 'Surat Tugas Dihapus', type: 'success' })
             } else {
              Swal.fire({ title: 'Surat Tugas Gagal Dihapus (' + result.values + ')', type: 'error' })
            }
          },
          error: function(result) { Swal.fire({ title: 'Surat Tugas Error', type: 'error' }) }
        });
        }
      })
    });

    /*APPROVING ST*/
    $('#stList').on('click', '.btn-approval', function() {
      let st = JSON.parse(decodeURIComponent($(this).closest('.grid-item').attr('data-st')));
      let approvalnote = st.approvalnote;
      let action, input, inputOptions, next_state;

      switch(st.approval) {
        /* KONSEP */
        case 0:
        action = 'Ajukan';
        next_state = 1;
        approvalnote = '';
        break;

        /* DIAJUKAN */
        case 1:
        action = 'Tetapkan';
        input = 'radio';
        inputOptions = {2: 'Ya, Tetapkan!', 3: 'Tolak'};
        break;

        /* DITETAPKAN */
        case 2:
        action = 'Alasan Mengajukan Pembatalan';
        next_state = 4;
        input = 'text';
        break;

        /* REQUEST PEMBATALAN */
        case 4:
        action = 'Setujui Permintaan Pembatalan';
        input = 'radio';
        inputOptions = {0: 'Ya, Setuju!', 2: 'Tolak'};
        break;
      }

      Swal.fire({
        title: action + ' Surat Tugas?',
        type: 'question',
        input: input,
        inputOptions: inputOptions,
        inputValidator: (value) => {
          if (!value) {
            return 'Harus Diisi!'
          }
        },
        confirmButtonText: 'Ok',
        showCancelButton: true,
        cancelButtonText: 'Batal',
        focusCancel:true,
        reverseButtons: true,
        showLoaderOnConfirm: true,
        preConfirm: () => {}
      }).then((result) => {


        if (result.value) {

          if (st.approval == 2) {
            approvalnote = result.value;
          }

          if (st.approval == 1 || st.approval == 4) {
            next_state = result.value;
          }

          if ((st.approval == 1 && result.value == 3) || (st.approval == 4 && result.value == 2)) {
            Swal.fire({
              title: ' Alasan Surat Tugas Ditolak?',
              type: 'question',
              input: 'text',
              inputValidator: (value) => {
                if (!value) {
                  return 'Pilih salah satu!'
                }
              },
              confirmButtonText: 'Ok',
              showCancelButton: true,
              cancelButtonText: 'Batal',
              focusCancel:true,
              reverseButtons: true,
              showLoaderOnConfirm: true,
              preConfirm: () => {}
            }).then(result => {
              approvalnote = result.value;
              approval({id: st.id, approval: next_state, approvalnote: approvalnote})
            })
          } else {
            approval({id: st.id, approval: next_state, approvalnote: approvalnote})
          }

        }
      })
    });

    /*Pegawai Reference*/
    $.ajax({
      url: '<?= $this->config->item('api_server') ?>pegawai',
      type: 'GET',
      dataType: 'json',
      success: function(result) {
        if (result.status == 200) {
          var html = '';
          $.each(result.values, function(index, item){
            html += '<option value="'+item.nip18+'">'+item.nama+' - '+item.nip18+'</option>'
          })
          $('select[name="nip"]').append(html).selectpicker('refresh');
        } else {
          console.log('Gagal load data pegawai');
        }
      }
    })

    /*ST Group Reference*/
    $.ajax({
      url: '<?= $this->config->item('api_server') ?>stgroup',
      type: 'GET',
      dataType: 'json',
      success: function(result) {
        if (result.status == 200) {
          $('select[name="group_id"]').append(buildGroup(result.values)).selectpicker('refresh');
        } else {
          console.log('Gagal load data st group');
        }
      }
    })

    /*Masonry*/
    $('.grid').masonry({percentPosition: true});

    $('#stList').on('shown.bs.collapse', 'li', function(){
      var $grid = $('.grid').masonry();
      $grid.masonry('reloadItems');
      $grid.masonry('layout');
    })

    $('#stList').on('hidden.bs.collapse', 'li', function(){
      var $grid = $('.grid').masonry();
      $grid.masonry('reloadItems');
      $grid.masonry('layout');
    })

    /*Tooltip*/
    $('body').tooltip({selector: '[data-toggle="tooltip"]'});

    /*READ ST*/
    $('#filterST').click()

  })

function approval(data){
  data.approvedby = "<?= $this->session->userdata('nip') ?>";

  $.ajax({
    url: "<?= $this->config->item('api_server') ?>st/approval",
    data: data,
    type: 'PUT',
    dataType: 'json',
    success: function(result) {
      if (result.status == 200) {
       $('#filterST').click()
       Swal.fire({ title: 'Status Surat Tugas Diperbaharui', type: 'success' })
     } else {
      Swal.fire({ title: 'Status Surat Tugas Gagal Diperbaharui (' + result.values + ')', type: 'error' })
    }
  },
  error: function(result) { Swal.fire({ title: 'Status Surat Tugas Error', type: 'error' }) }
});
}

function st(){

  if (st_req) {
    return true;
  }

  st_req = $.ajax({
    url: '<?= $this->config->item('api_server') ?>st?nip=<?= $this->session->userdata('nip') ?>&' + $('#stFilterForm').serialize() + '&page=' + st_page,
    type: 'GET',
    dataType: 'json',
    beforeSend: function(xhr) {
      st_req = true;
      $('#st-load').show()
    },
    success: function(result) {

      st_req = false;
      $('#st-load').hide()

      if (result.status == 200) {

        st_page++;

        var html = '';

        if (result.values.length < 1) {
          $('#st-empty').show()
          st_req = true;
        } else {
          $('#st-empty').hide()
        }

        $.each(result.values, function(index, item){

          var approval_icon, approval_tooltip = '', approval_class, action, approvalnote, download, created, updated;

          if (item.approvedby_name || item.approvedat) {
            approval_tooltip = '(';

            if (item.approvedby_name) approval_tooltip += ( ( item.approval == 2 || item.approval == 3 ) ? 'Bagian SDM' : item.approvedby_name ) + ' - ';
            if (item.approvedat) approval_tooltip += formatDate(item.approvedat, true);

            approval_tooltip += ')';
          }

          created = (item.createdby_name && item.createdat) ? '<small><span class="text-muted">Dibuat oleh <a href="#">' + item.createdby_name + '</a> - ' + formatDate(item.createdat, true) + '</span></small>' : '';

          updated = (item.updatedby_name && item.updatedat) ? '<small><span class="text-muted">Terakhir Diubah oleh <a href="#">' + item.updatedby_name + '</a> - ' + formatDate(item.updatedat, true) + '</span></small>' : '';

          approvalnote = (item.approvalnote) ? '<div class="body" style="padding: 0px"><div class="alert alert-info"><strong>CATATAN:</strong> ' + item.approvalnote + '</div></div>' : '';

          if (item.filename) {
            download = '<li><a href="<?=base_url();?>files/upload_milea/st/' + item.filename + '" target="_blank" class=" waves-effect waves-block">Download</a></li>';
          } else {
            download = '<li class="disabled"><a href="javascript:void(0);" class=" waves-effect waves-block">Download (Dokumen tidak tersedia)</a></li>';
          }

          switch(item.approval){
            case 0:
            approval_icon     = 'mode_edit';
            approval_tooltip  = 'Status: Konsep';
            approval_class    = 'bg-blue-grey btn-approval';
            action            = '\
            <li><a href="javascript:void(0);" class="waves-effect waves-block btn-approval">Ajukan!</a></li>\
            <li role="separator" class="divider"></li>\
            <li><a href="<?=base_url();?>st?q=CxtYc&st_id=' + item.id + '" class="waves-effect waves-block">Edit</a></li>\
            ' + download + '\
            <li role="separator" class="divider"></li>\
            <li><a href="#" class="waves-effect waves-block deleteST" data-id=' + item.id + '>Hapus</a></li>';
            break;

            case 1:
            approval_icon     = 'publish';
            approval_tooltip  = 'Status: Diajukan ' + approval_tooltip;
            approval_class    = 'btn-warning';
            action            = '\
            <li class="disabled"><a href="javascript:void(0);" class="waves-effect waves-block">Menunggu Penetapan</a></li>\
            <li role="separator" class="divider"></li>\
            <li class="disabled"><a href="javascript:void(0);" class="waves-effect waves-block">Edit</a></li>\
            ' + download + '\
            <li role="separator" class="divider"></li>\
            <li class="disabled"><a href="javascript:void(0);" class="waves-effect waves-block" data-id=' + item.id + '>Hapus</a></li>';
            break;

            case 2:
            approval_icon     = 'check';
            approval_tooltip  = 'Status: Ditetapkan ' + approval_tooltip;
            approval_class    = 'btn-success btn-approval';
            action            = '\
            <li><a href="javascript:void(0);" class="waves-effect waves-block btn-approval">Batalkan!</a></li>\
            <li role="separator" class="divider"></li>\
            <li class="disabled"><a href="javascript:void(0);" class="waves-effect waves-block">Edit</a></li>\
            ' + download + '\
            <li role="separator" class="divider"></li>\
            <li class="disabled"><a href="javascript:void(0);" class="waves-effect waves-block" data-id=' + item.id + '>Hapus</a></li>';
            break;

            case 3:
            approval_icon     = 'close';
            approval_tooltip  = 'Status: Ditolak ' + approval_tooltip;
            approval_class    = 'btn-danger';
            action            = '\
            <li><a href="<?=base_url();?>st?q=CxtYc&st_id=' + item.id + '" class="waves-effect waves-block">Edit</a></li>\
            ' + download + '\
            <li role="separator" class="divider"></li>\
            <li><a href="#" class="waves-effect waves-block deleteST" data-id=' + item.id + '>Hapus</a></li>';
            break;

            case 4:
            approval_icon     = 'live_help';
            approval_tooltip  = 'Status: Mengajukan Pembatalan ' + approval_tooltip;
            approval_class    = 'bg-yellow';
            action            = '\
            <li class="disabled"><a href="javascript:void(0);" class="waves-effect waves-block">Menunggu Persetujuan Pembatalan</a></li>\
            <li role="separator" class="divider"></li>\
            <li class="disabled"><a href="javascript:void(0);" class="waves-effect waves-block">Edit</a></li>\
            ' + download + '\
            <li role="separator" class="divider"></li>\
            <li class="disabled"><a href="javascript:void(0);" class="waves-effect waves-block" data-id=' + item.id + '>Hapus</a></li>';
            break;
          }

          var st_pegawai_id = 'st' + Date.now() +  Math.round( Math.random() * 100 );

          html += '<div class="col-sm-6 grid-item" data-st="' + encodeURIComponent(JSON.stringify(item)) + '">\
          <div class="card">\
          <div class="header">\
          <button type="button" class="btn ' + approval_class + ' btn-circle waves-effect waves-circle waves-float pull-right" data-toggle="tooltip" data-placement="top" title="" data-original-title="' + approval_tooltip + '" data-id=' + item.id + '><i class="material-icons">' + approval_icon + '</i></button>\
          <div class="media" style="\
          margin-top: 0px;\
          margin-bottom: 0px;\
          ">\
          <div class="media-left">\
          <h1 style="margin-top: 0px;"><span class="label" style="background-color: ' + item.color + ';">' + item.code + '</span></h1>\
          </div>\
          <div class="media-body">\
          <h4 class="" style="margin-top: 0px;">\
          ' + item.no + '\
          </h4>\
          <span class="badge bg-deep-purple">' + formatDate(item.startdate, false) + ' - ' + formatDate(item.enddate, false) + '</span>\
          </div>\
          </div>\
          <p style="margin-bottom: 0px; margin-top: 20px;">' + created + '</p>\
          <p>' + updated + '</p>\
          <div class="btn-group pull-right">\
          <button type="button" class="btn btn-default btn-circle dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="material-icons">menu</i></button>\
          <ul class="dropdown-menu">' 
          + action + 
          '</ul>\
          </div>\
          <span class="badge bg-cyan" style="\
          margin-top: 22px;\
          ">' + item.pegawai.length + ' Pegawai</span>\
          <div class="row"></div>\
          </div>\
          <div class="body" style="padding: 0px">\
          <ul class="demo-choose-skin ' + ( (item.pegawai.length > 4) ? 'slimscroll' : '' ) + '" style="margin-bottom: 0px;">\
          <div id="' + st_pegawai_id + '">\
          <div class="panel" style="margin-bottom: 0px; border-width: 0px">';

          $.each(item.pegawai, function(index, item){
            var 
            st_date_id  = st_pegawai_id + '_pegawai' + index,
            color           = 'teal';
              // color           = randomColor();

              html += '<li data-toggle="collapse" data-target="#' + st_date_id + '" data-parent="#' + st_pegawai_id + '">\
              <div class="' + color + '"></div>\
              <span>' + item.nama + '</span>\
              <span class="font-10">' + item.nip + '</span>\
              <i class="material-icons pull-right col-' + color + '">date_range</i>\
              </li>\
              \
              <li id="'+ st_date_id +'" class="collapse">';

              if (item.dates.length < 1) {
                html += '\n<span class="label bg-' + color + '">Belum Diisi</span>\n';
              }

              $.each(item.dates, function(index, item){
                html += '\n<span class="label bg-' + color + '">' + formatDate(item.startdate, false) + ' - ' + formatDate(item.enddate, false)  + '</span>\n';
              });

              html += '</li>';

            });

          html +=  '</div>\
          </div>\
          </ul>\
          </div>'
          + approvalnote +
          '</div>\
          </div>'

        });

$('#stList').append(html);

$('#searchST').keyup()

var $grid = $('.grid').masonry();

var msnry = $grid.data('masonry');

$grid.imagesLoaded( function() {
  $grid.masonry('reloadItems');
  $grid.masonry('layout');
});

$('.slimscroll').jslimscroll({
  height: '225px',
  color: 'rgba(0,0,0,0.5)',
  size: '6px',
  alwaysVisible: false,
  borderRadius: '0',
  railBorderRadius: '0'
});


} else {
  console.log('Gagal load data dosir');
}
},
error: function(result) {
  st_req = false;
  $('#st-load').hide()
  console.log('Error load data dosir');
}
})
}

function showNotification(colorName, text) {
  var 
  placementFrom   = 'top', 
  placementAlign  =  'center', 
  animateEnter    = '', 
  animateExit     = '';

  if (colorName === null || colorName === '') { colorName = 'bg-black'; }
  if (text === null || text === '') { text = 'Turning standard Bootstrap alerts'; }
  if (animateEnter === null || animateEnter === '') { animateEnter = 'animated fadeInDown'; }
  if (animateExit === null || animateExit === '') { animateExit = 'animated fadeOutUp'; }
  var allowDismiss = true;

  $.notify({
    message: text
  },
  {
    type: colorName,
    allow_dismiss: allowDismiss,
    newest_on_top: true,
    timer: 1000,
    placement: {
      from: placementFrom,
      align: placementAlign
    },
    animate: {
      enter: animateEnter,
      exit: animateExit
    },
    template: '<div data-notify="container" class="bootstrap-notify-container alert alert-dismissible {0} ' + (allowDismiss ? "p-r-35" : "") + '" role="alert">' +
    '<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>' +
    '<span data-notify="icon"></span> ' +
    '<span data-notify="title">{1}</span> ' +
    '<span data-notify="message">{2}</span>' +
    '<div class="progress" data-notify="progressbar">' +
    '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
    '</div>' +
    '<a href="{3}" target="{4}" data-notify="url"></a>' +
    '</div>'
  });
}

function formatDate(date, time){
  var options;
  if (time) {
    options = { year: 'numeric', month: 'short', day: 'numeric', hour: 'numeric', minute: 'numeric', second: 'numeric' };
  } else {
    options = { year: 'numeric', month: 'short', day: 'numeric' };
  }
  return new Date(date).toLocaleDateString('id-ID', options);
}

function randomColor(){
  var color = ['red', 'pink', 'purple', 'deep-purple', 'indigo', 'blue', 'light-blue', 'cyan', 'teal', 'green', 'light-green', 'lime', 'yellow', 'amber', 'orange', 'deep-orange', 'brown'];

  return color[Math.floor(Math.random() * color.length)];
}

function buildGroup(items) {
  if (items.length == 0) { return ''; }

  var html = "";

  $.each(items, function(index, item){
    if (item.children) {
      html += '<optgroup label="'+item.name+'">';
      html += buildGroup(item.children);
      html += '</optgroup>';
    } else {
      html += '<option value='+item.id+'>'+item.name+'</option>';
    }
  })

  return html;
}
</script>
