<style media="screen">
   th { text-align: center; }
   thead tr { color: #fff; background: #00acd6; }
   tbody tr .row-1 { color: #fff; background: #00acd6; }

   .level-1 { font: bold 14px/5px sans-serif; color: #00e; }
   .level-2 { font: bold 13px sans-serif; color: #000; }
   .level-3 { font: italic bold 12px sans-serif; color: #000; }
   .level-4 { font: 12px sans-serif; color: #000; }
   .level-5 { font: 12px sans-serif; color: #000; }
   .level-6 { font: italic 12px sans-serif; color: #000; }
   .level-7 { font: 11px sans-serif; color: #000; padding: 50px 30px 50px 80px;}
   .level-8 { font: 11px sans-serif; color: #7a7a7a; }
   .header  {padding: 0px 2px 0px 15px;}
   .item    {padding: 0px 2px 0px 25px;}
   td{padding: 5px!important}
   .inSatker {text-indent: -47px; margin-left:48px;}
   .tg-rhys{background-color:#317bbe;color:#FFF;text-align:center;vertical-align: middle !important;font-size:14px;font-weight:bold; border-style:solid; border-width:1px;overflow:hidden; word-break:normal;border-color:black;}
   .container {overflow-y: auto;padding: 0px;padding-top: 0px;width: 100%}
   td{padding: 5px!important}
</style>

<div class="row">
   <div class="col-md-12">
      <div class="box box-widget" style="margin-bottom:0px">

         <div class="box-header with border" style="padding-bottom:0px">
            <div class="row">
               <div class="col-md-6">
                  <div class="input-group">
                     <label class="radio-inline">Tampilkan :</label>
                     <label class="radio-inline"><input type="radio" class="radio" name="optradio" value="level-3">Output</label>
                     <label class="radio-inline"><input type="radio" class="radio" name="optradio" value="level-5">Komponen</label>
                     <label class="radio-inline"><input type="radio" class="radio" name="optradio" value="level-7">Akun</label>
                     <label class="radio-inline"><input type="radio" class="radio" name="optradio" value="level-8">Detail</label>
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="input-group">
                     <input id="search" class="form-control" placeholder="Pencarian..." value="" type="text" autocomplete="off">
                     <span class="input-group-btn">
                        <button class="btn btn-default btn-flat"><i class="fa fa-search text-purple" style="padding-top: 3px;padding-bottom: 3px;"></i></button>
                     </span>
                  </div>
               </div>
            </div>
         </div>

         <div class="box-body">
            <div class="container">
               <table class="table table-hover table-bordered table-condensed">
                  <thead>
                     <tr>
                        <th class="tg-rhys" width="" rowspan="2">KODE</th>
                        <th class="tg-rhys" width="39%" rowspan="2">PROGRAM/ KEGIATAN/ OUTPUT/ SUB OUTPUT/ KOMPONEN</th>
                        <th class="tg-rhys" width="48%" colspan="4" style="border-bottom:0px">PERHITUNGAN </th>
                        <th class="tg-rhys" width="3%" rowspan="2">SD/ CP</th>
                     </tr>
                     <tr>
                        <th class="tg-rhys" width="15%">VOLUME</th>
                        <th class="tg-rhys" width="15%">HARGA SATUAN</th>
                        <th class="tg-rhys" width="18%" colspan="2">JUMLAH BIAYA</th>
                     </tr>
                  </thead>

                  <tbody id="data_rkakl">
                  <?php foreach ($tabel as $row) {
                     $class   = ' text-right'; if ($row['level']<=3) $class = ' text-center';
                     $volkeg  = '';  if ($row['volkeg']>0) $volkeg = number_format($row['volkeg'], 0, ',', '.') .' '. $row['satkeg'];
                     $hargasat= '';  if ($row['hargasat']>0) $hargasat = number_format($row['hargasat'], 0, ',', '.');
                     $uraian  = $row['uraian'];

                     if ($row['level']==5) $volkeg = $row['satkeg'];
                     if ($row['level']==7) $uraian = "<u>$uraian</u>";
                     if ($row['level']==8) {
                        if ($row['jumlah'] == 0) {
                           $uraian = '<div class="header"> > '. $row['uraian'] .'</div>';
                           $volkeg = '';
                        } else {
                           $uraian = '<div class="item"> - '. $row['uraian'] .'</div>';
                           $volkeg = number_format($row['volkeg'], 2, ',', '.') .' '. $row['satkeg'];
                        }
                     }
                  ?>

                     <tr class="row-<?php echo $row['level'] ?>">
                        <td class="level-<?= $row['level'] . $class ?>">
                           <span style="display: none"><?=$row['kdkey'] ?>||</span>
                           <?php 
                              if ($row['level']==5) echo substr($row['kode'],13,3);
                              else echo $row['kode']; 
                           ?>
                        </td>
                        <td class="level-<?= $row['level'] ?> text-left"> <?= $uraian ?></td>
                        <td class="level-<?= $row['level'] ?> text-right"><?= $volkeg ?></td>
                        <td class="level-<?= $row['level'] ?> text-right"><?= $hargasat ?></td>
                        <td class="level-<?= $row['level'] ?> text-right"><?= number_format($row['jumlah'], 0, ',', '.') ?></td>
                        <td class="level-<?= $row['level'] ?> text-right"><?= $row['atrib'] ?></td>
                        <td class="level-<?= $row['level'] ?> text-right"> &nbsp; </td>
                     </tr>

                  <?php } ?>
                  </tbody>
               </table>
            </div>
         </div>

      </div>
   </div>
</div>

<script type="text/javascript">
   var $rows = $('#data_rkakl tr'), cari = '';
   $('#search').keyup(function() {
      var val  = $.trim($(this).val()).replace(/ +/g, ' ').toLowerCase(), 
          kode = '';

      $rows.show().filter(function() {
         var text = $(this).text().replace(/\s+/g, ' ').toLowerCase(),
             arr  = text.split('||'),
             nil  = false;  
  
         if (~text.indexOf(val)) { nil = true;  cari = arr[0]; kode += arr[0].trim()+'||'; }
         if (! nil) { if (~text.indexOf(cari)) nil = true; }
         return ! nil;
      }).hide();

      $rows.show().filter(function() {
         var text = $(this).text().replace(/\s+/g, ' ').toLowerCase(),
             arr  = text.split('||'),
             val = arr[0].trim(),
             nil  = false;
         if (~kode.indexOf(val)) { nil = true; }
         if (! nil) { if (~text.indexOf(cari)) nil = true; }
         return ! nil;
      }).hide();
   });

   $('.radio').click(function () {
      var cls = $(this).val();
      if (cls == 'level-3') { $('.level-4, .level-5, .level-6, .level-7, .level-8').hide(); }
      if (cls == 'level-5') { $('.level-4, .level-5').show(); $('.level-6, .level-7, .level-8').hide(); }
      if (cls == 'level-7') { $('.level-4, .level-5, .level-6, .level-7').show(); $('.level-8').hide(); }
      if (cls == 'level-8') { $('.level-4, .level-5, .level-6, .level-7, .level-8').show(); }
   })
</script>
