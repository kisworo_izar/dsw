<link href="<?=base_url('assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css');?>" rel="stylesheet" type="text/css" />
<script src="<?=base_url('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js');?>" type="text/javascript"></script>

<style media="screen">
   td a {display:block;width:100%; color: red}
   A:link,A:visited,A:active,A:hover {text-decoration: none; color: white;}
   .table-bordered > thead > tr > th,
   .table-bordered > thead > tr > td {border-bottom: 2px solid #357CBC;}
   .table-bordered > tbody > tr > td {padding:3px}
   .dt {border:solid 1px #B1B9C1;padding:0px 5px;}
   .hiddenRow {padding: 0 !important;}
   .thd {width:65px;border-right:solid 1px #B1B9C1;border-bottom:solid 1px #B1B9C1; border-top:solid 1px #B1B9C1;text-align: center; background: #357CBC}
   section {position: relative;}
   section.positioned {position: absolute;top:10px;left:10px;}
   .container {overflow-y: auto;padding: 0px;padding-top: 0px;width: 100%}
   table {border-spacing: 1;width:100%;}
   th div{position: absolute;background #357CBC;color:black;border-bottom: solid #357CBC 1px;border-top: solid 1px #E7E9EE;
   padding: 6px 0px;top: 0;line-height: normal;}
   .calculated-width {width: -webkit-calc(100% - 1px);width: calc(100% -1px);}​
   .info{border-top:solid 1px #357CBC;padding:0px;margin:0px}
   .info1{border-top:0px;padding:0px;margin:0px}
   .x { display: inline-block; width: 110px; }
   .p3 {padding:3px}
   .tg-rhys{background-color:#317bbe;color:#FFF;text-align:center;vertical-align: middle !important;font-size:14px;font-weight:bold; border-style:solid; border-width:1px;overflow:hidden; word-break:normal;border-color:black;}
   .center{text-align: center} 
   .hijau{background: #27ae60; color:white}
   .biru{background: #3498db; color:white} .abu{background: #bdc3c7; color:white}
   .merah{background: #e74c3c; color:white} .kuning{background:  #FFD700 ; color: white}
   .inSatker {text-indent: -45px; padding-left: 50px}
</style>


<div class="row">
   <div class="col-md-12">
      <div class="box box-widget">

         <div class="box-header with-border">
            <div class="row">
               <div class="col-md-6"></div>
               <div class="col-md-6">
                  <form role="form" action="<?php echo site_url("monit_satker?q=bb608") ?>" method="post" style="margin-bottom:0px">
                     <div class="input-group">
                        <input type="text" name="cari" class="form-control" placeholder="Pencarian..." value="<?php echo $this->session->userdata('cari') ?>">
                        <span class="input-group-btn">
                           <button type="submit" name="search" value="search" class="btn btn-info btn-flat"><i class="fa fa-search" style="height:16px;margin-top:4px"></i></button>
                        </span>
                        <span class="input-group-btn">
                           <button type="submit" name="search" value="clear" class="btn btn-info btn-flat"><i class="fa fa-times-circle" style="height:16px;margin-top:4px"></i></button>
                        </span>
                     </div>
                  </form>
               </div>
            </div>
         </div>

         <div class="box-body">
            <section class="">
               <div class="container">
                  <table id="iGrid" class="table table-hover table-bordered" style="margin-bottom:0px">
                     <thead>
                        <tr >
                            <td class="tg-rhys" rowspan="2" style="border-bottom:solid white 2px">ID Revisi</td>
                            <td class="tg-rhys" colspan="3" style="border-bottom:solid white 1px">Validasi</td>
                            <td class="tg-rhys" rowspan="2" style="border-bottom:solid white 2px; width:95px">
                               <div class="">Tahap 1<br><span class="small">(Pengajuan)</span></div>
                            </td>
                            <td class="tg-rhys" rowspan="2" style="border-bottom:solid white 2px; width:95px">
                               <div class="">Tahap 2<br><span class="small">(Penelitian)</span></div>
                            </td>
                            <td class="tg-rhys" rowspan="2" style="border-bottom:solid white 2px; width:95px">
                               <div class="">Tahap 3<br><span class="small">(Penelaahan)</span></div>
                            </td>
                            <td class="tg-rhys" rowspan="2" style="border-bottom:solid white 2px; width:95px">
                               <div class="">Tahap 4<br><span class="small">(Penetapan)</span></div>
                            </td>
                            <td class="tg-rhys" rowspan="2" style="border-bottom:solid white 2px; width:95px">
                               <div class="">Tahapan<br><span class="small">Status</span></div>
                            </td>
                        </tr>
                        <tr>
                           <td class="tg-rhys" style="border-bottom:solid white 2px; width:10px">V1</td>
                           <td class="tg-rhys" style="border-bottom:solid white 2px; width:10px">V2</td>
                           <td class="tg-rhys" style="border-bottom:solid white 2px; width:10px">V3</td>
                        </tr>
                     </thead>

                     <tbody id="data_monit">
                        <?php if ($tabel) { ?>
                           <?php foreach ($tabel as $row) { ?>
                              <tr style="margin:0px;">
                                 <td style="padding-left:10px" data-toggle="collapse" data-target=".<?= str_replace('.', '_', $row['rev_id']) ?>">
                                    <span class="text-primary"> <b><?= $row['rev_id'] ?> </b></span> <br>
                                    <span class="small"> Revisi : <strong>
                                       <?php
                                          if ($row['rev_level'] == '1') echo 'DJA';
                                          elseif ($row['rev_level'] == '2') echo 'PA';
                                          else echo 'Kanwil';
                                       ?>
                                       </strong>
                                    </span>
                                 </td>
                                 <td style="" class="small text-center"></td>
                                 <td style="" class="small text-center"></td>
                                 <td style="" class="small text-center"></td>

                                 <?php $w=$row['pus_status']; $w1=''; $j1='';
                                    if ($w=='0') $w1='merah'; if ($w=='1') $w1='biru'; if ($w=='2') $w1='hijau';
                                    if ($row['rev_tahap']>='2' and $w1!='')
                                       $j1 = $this->fc->idtgl($row['pus_tgl']) .'<br><i class="fa fa-clock-o"></i> '. $this->fc->idtgl($row['pus_tgl'], 'jam');
                                    else $j1 = $this->fc->idtgl($row['pus_tgl']) .'<br>'. $row['rev_upload_step'] . ' / 3';
                                 ?>
                                 <td style="" class="small text-center <?= $w1 ?>">
                                    <span data-toggle="tooltip" title="Nomor Tiket <?= $row['rev_id'] ?>  Proses Tahap 1">
                                       <span><?= $j1 ?></span>
                                    </span>
                                 </td>

                                 <?php $w2=''; $j2='';
                                 //Rule DJA
                                 if($row['rev_level']=='1'){
                                    $dja = $row['t2_status']; if($dja=='1')  $w2='biru'; if($dja=='2') $w2='hijau';
                                    if($row['rev_level']=='1' And $w2!='')   $j2= $this->fc->idtgl($row['t2_proses_tgl']) .'<br><i class="fa fa-clock-o"></i> '. $this->fc->idtgl($row['t2_proses_tgl'], 'jam'); else $j2 = "&nbsp ";
                                 }
                                 //Rule Kanwil / PA Start
                                 if($row['rev_level'] !='1'){
                                    $kwpa_s = $row['t2_status']; if($kwpa_s=='1') $w2='biru';
                                    if($row['rev_level']!='1' And $row['rev_tahap']>='2' And $w2!='') $j2= $this->fc->idtgl($row['t2_proses_tgl']) .'<br><i class="fa fa-clock-o"></i> '. $this->fc->idtgl($row['t2_proses_tgl'], 'jam'); else $j2 = '&nbsp';
                                 
                                 //Rule Kanwil / PA End
                                    $kwpa_f = $row['t6_status']; if($kwpa_f=='2') $w2='hijau';
                                    if($row['rev_level']!='1' And $row['rev_tahap']>='2' And $w2!='') $j2= $this->fc->idtgl($row['t6_selesai_tgl']) .'<br><i class="fa fa-clock-o"></i> '. $this->fc->idtgl($row['t6_selesai_tgl'], 'jam'); else $j2 = '&nbsp'; 
                                 }
                                 ?>
                                 <td style="" class="small text-center <?= $w2 ?>">
                                    <span  data-toggle="tooltip" title="Nomor Tiket <?= $row['rev_id'] ?>  Proses Tahap 2">
                                       <span><?= $j2 ?></span>
                                    </span>
                                 </td>

                                 <?php $w3=''; $j3='';
                                 if($row['rev_level']=='1'){
                                    //Rule DJA Start And End
                                    if($row['rev_tahap']<='3'){
                                       $dja = $row['t3_status']; if($dja=='1') $w3='biru'; if($dja=='0') $w3='merah';
                                       if($row['rev_tahap']>='3' And $w3 !='') $j3= $this->fc->idtgl($row['t3_proses_tgl']) .'<br><i class="fa fa-clock-o"></i> '. $this->fc->idtgl($row['t3_proses_tgl'], 'jam');
                                       else $j3 = '&nbsp;';
                                    }
                                    if($row['rev_tahap']>='6'){
                                       $dja = $row['t6_status']; if($dja=='2') $w3='hijau'; if($dja=='0') $w3='merah';
                                       if($row['rev_tahap']>='6' And $w3 !='') $j3= $this->fc->idtgl($row['t6_selesai_tgl']) .'<br><i class="fa fa-clock-o"></i> '. $this->fc->idtgl($row['t6_selesai_tgl'], 'jam');
                                       else $j3 = '&nbsp;';
                                    }
                                 }?>
                                 <td style="" class="small text-center <?= $w3 ?>">
                                    <span data-toggle="tooltip" title="Nomor Tiket <?= $row['rev_id'] ?>  Proses Tahap 3">
                                       <span><?= $j3 ?></span>
                                    </span>
                                 </td>

                                 <?php $sts=$row['t7_status']; $w4=''; $j4='';
                                    if ($sts=='0') $w4='merah'; if ($sts=='1') $w4='biru'; if ($sts=='2') $w4='hijau';
                                    if ($row['rev_tahap']>='7' and $w4!='')
                                       $j4 = $this->fc->idtgl($row['t7_proses_tgl']) .'<br><i class="fa fa-clock-o"></i> '. $this->fc->idtgl($row['t7_proses_tgl'], 'jam');
                                    else $j4 = '&nbsp;';
                                 ?>
                                 <td style="" class="small text-center <?= $w4 ?>">
                                    <span data-toggle="tooltip" title="Nomor Tiket <?= $row['rev_id'] ?>  Proses Tahap 4">
                                       <span><?= $j4 ?></span>
                                    </span>
                                 </td>

                                 <?php $w=$row['rev_status']; $s=$row['rev_tahap']; $w5='';
                                    if ($w=='0') $w5='merah'; if ($w=='1') $w5='biru'; if ($w=='2') $w5='hijau';
                                 ?>

                                 <?php $w8='';
                                    if ($row['rev_tahap']==2) $w=$row['t2_status'];
                                    if ($row['rev_tahap']==3) $w=$row['t3_status'];
                                    if ($row['rev_tahap']==6) $w=$row['t6_status'];
                                    if ($row['rev_tahap']==7) $w=$row['t7_status'];
                                    if ($w=='0') $w8='merah'; if ($w=='1') $w8='biru'; if ($w=='2') $w8='hijau'; if ($w=='9') $w8='abu';
                                 ?>

                                 <td style="" class="text-center text-bold text-muted <?=$w8 ?> "><span style="font-size:25px"><?php if ($row['rev_tahap']=='7') {echo '4';} else {echo $row['rev_tahap'];}?></span> </td>
                              </tr>

                              <tr style="background:#FFF;color:#34495E; ">
                                 <td class="hiddenRow" colspan="9" style="border:solid 0px red">
                                    <div class=" collapse <?= str_replace('.', '_', $row['rev_id']) ?> " style="padding:7px">
                                       <span>ID Revisi : <b><?= $row['rev_id'] ?></b> </span>
                                       <table class="table-hover" style="margin-top:5px">
                                          <tr style="background:#3A99D9; color:#FFF; font-size:14px">
                                             <td class="dt" style="padding:3px; text-align:center;"><b>Kode dan Uraian Satker</b></td>
                                             <td class="dt" style="padding:3px; text-align:center; width:30px"><b>ke</b></td>
                                             <?php $ssb=""; if(!strpos($this->session->userdata('whrdept'),'015')) $ssb="display:none;"?>
                                             <td class="dt" style="padding:3px; text-align:center; width:30px;<?php echo $ssb ?>"><b>SSB</b></td>
                                             <td class="dt" style="padding:3px; text-align:center; width:30px"><b>Rp</b></td>
                                             <td class="dt" style="padding:3px; text-align:center; width:30px"><b>DS</b></td>
                                             <td class="dt" style="padding:3px; text-align:center; width:30px"><b>&nbsp;#&nbsp;</b></td>
                                             <td class="dt" style="padding:3px; text-align:center;"><b>Indikasi</b></td>
                                          </tr>


                                          <?php foreach ($row['satker'] as $val) { ?>
                                             <tr style="font-size:14px">
                                                <td class="dt p3" style="max-width:200px">
                                                   <div class="inSatker"><?= $val['kdsatker'] .' '. $val['uraian'] ?></div>
                                                   <span class="small iMatrik" data-list="<?= $val['link_matrik'] ?>" style="margin-left:50px; color:#357CBC">
                                                      <i class="fa fa-bookmark"></i> Matriks
                                                   </span>
                                                   <span class="small iPok" data-list="<?= $val['link_pok'] ?>"" style="margin-left:10px; color:#357CBC">
                                                      <i class="fa fa-bookmark"></i> POK
                                                   </span>
                                                   <?php if ($val['val_ssb'] > 0) { ?>
                                                      <span class="small iSsb" data-list="<?= $val['link_ssb'] ?>"" style="margin-left:10px; color:#357CBC">
                                                         <i class="fa fa-bookmark"></i> SSB
                                                      </span>
                                                   <?php } ?>
                                                </td>
                                                <td class="dt p3 text-center" style="vertical-align: text-top"><?= $val['revisike'] ?></td>
                                                <td class="dt p3 text-center" style="vertical-align: text-top;<?php echo $ssb ?>">
                                                   <?php if ($val['val_ssb'] > 0) echo $val['val_ssb'] ?>
                                                </td>
                                                <td class="dt p3" style="vertical-align: text-top">
                                                   <?php if ($val['val_pagu']) echo '<span class="fa fa-exclamation-circle text-red"></span>'; ?>
                                                </td>
                                                <td class="dt p3 text-center" style="vertical-align: text-top"> 
                                                   <?php if ($val['val_ds']) echo '<span class="fa fa-exclamation-circle text-red"></span>'; ?>
                                                </td>
                                                <td class="dt p3 text-center" style="vertical-align: text-top">
                                                   <?php if ($val['val_blokir']) echo '<span class="fa fa-exclamation-circle text-red"></span>'; ?>
                                                </td>
                                                <td class="dt p3" style="vertical-align: text-top"><?= $val['indikasi'] ?></td>
                                             </tr>
                                          <?php } ?>
                                       </table>
                                       </div>
                                  </td>
                              </tr>
                           <?php
                           }
                        } else {
                           echo '<tr><td colspan="9" class="text-center text-muted" style="font-size:13px; font-style: italic">Tidak ada data ... </td></tr>';
                        }
                        ?>
                     </tbody>
                  </table>
               </div>
            </section>
         </div>

      </div>
   </div>

</div>


<script type="text/javascript">
   $('.collapse').on('show.bs.collapse', function () {
      $('.collapse.in').collapse('hide');
   });

   $('.iMatrik, .iPok, .iSsb').css('cursor', 'pointer');
   $('.iMatrik, .iPok, .iSsb').click( function(event) {
      window.open( $(this).data('list'), '_blank');
   })
</script>
