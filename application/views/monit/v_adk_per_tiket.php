<link href="<?=base_url('assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css');?>" rel="stylesheet" type="text/css" />
<script src="<?=base_url('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js');?>" type="text/javascript"></script>

<style media="screen">
   .inSatker {text-indent: -45px; padding-left: 50px}
</style>


<div class="box box-widget">
   <input type="hidden" id="aktif">
   <input type="hidden" id="strlen" value="0">

   <div class="box-body" id="divmain">
         <div class="input-group" style="padding-bottom: 7px;">
            <input id="cari" class="form-control" name="cari" type="text" value="">
            <span class="input-group-btn">
               <button class="btn btn-info btn-flat"><i class="fa fa-search" style="height:16px; margin-top:4px"></i></button>
            </span>
         </div>

      <table id="iGrid" class="table table-hover table-bordered">
         <thead>
            <tr style="font-size:12px; color:#fff; background-color:#3568B6; padding:0px !important;">
               <th class="text-center" width=" 5px">No</th>
               <th class="text-center" width="">Kode dan Uraian Satker</th>
               <th class="text-center" width="30px"> Ke </th>
               <th class="text-center" width="30px"> Rp </th>
               <th class="text-center" width="30px"> DS </th>
               <th class="text-center" width="30px"> &nbsp;@&nbsp; </th>
               <th class="text-center" width="20%"> Indikasi </th>
            </tr>
          </thead>
         <tbody>

         

            <?php 
            $no=0; 
            foreach ($tabel as $row) { 
              $no++;
            ?>
              <tr id="" class="All" style="margin: 0px;">
                 <td class="small text-center"><?= $no .'.' ?></td>
                 <td class="small text-left">
                   <div class="inSatker"><?= $row['kode'] .' '. $row['uraian'] ?></div>
                   <span class="small iMatrik" data-list="<?= $row['link_matrik'] ?>" style="margin-left:50px; color:#357CBC">
                      <i class="fa fa-bookmark"></i> Matriks
                   </span>
                   <span class="small iPok" data-list="<?= $row['link_pok'] ?>" style="margin-left:10px; color:#357CBC">
                      <i class="fa fa-bookmark"></i> POK
                   </span>
                  </td>
                 <td class="small text-left"><?= $row['revisike'] ?></td>
                 <td class="small text-left">
                      <?php if ($row['val_pagu']) echo '<span data-toggle="tooltip" title="Perubahan Pagu Anggaran" class="fa fa-exclamation-circle text-red"></span>'; ?>
                 </td>
                 <td class="small text-left">
                      <?php if ($row['val_ds']) echo '<span data-toggle="tooltip" title="Perubahan Pagu Anggaran" class="fa fa-exclamation-circle text-red"></span>'; ?>
                 </td>
                 <td class="small text-left">
                      <?php if ($row['val_blokir']) echo '<span data-toggle="tooltip" title="Perubahan Pagu Anggaran" class="fa fa-exclamation-circle text-red"></span>'; ?>
                 </td>
                 <td class="small text-left" id="" style="padding:5px" onclick=""><?= $row['indikasi'] ?></td>
              </tr>
            <?php } ?>
         </tbody>
      </table>

   </div>
</div>

<script type="text/javascript">
   $('.iMatrik, .iPok').css('cursor', 'pointer');
   $('.iMatrik, .iPok').click( function(event) {
      window.open( $(this).data('list'), '_parent');
   })
</script>
