<div class="row">
    <div class="col-md-9">
        <div class="box box-widget">
            <div class="box-body with-border">
                <form role="form" action="<?php echo site_url("paket/index/TlO6k") ?>" method="post">
                  <div class="input-group" style="margin-bottom:10px">
                    <input type="hidden" name="nmfunction" value="dash">
                    <input type="text" name="cari" class="form-control" placeholder="Pencarian..." value="<?php echo $this->session->userdata('cari') ?>">
                        <span class="input-group-btn">
                          <button type="submit" name="search" value="search" id="search-btn" class="btn btn-default btn-flat"><i class="fa fa-search"></i>
                          </button>
                        </span>
                        <span class="input-group-btn">
                            <button type="submit" name="search" value="clear" class="btn btn-default btn-flat"><i class="fa fa-times-circle"></i></button>
                        </span>
                  </div>
                </form>

              <div class="box-body table-responsive no-padding">
                <table class="table table-hover table-bordered">
                    <tr style="background: #447AB2; color: #fff;">
                        <th class="text-center"># ID</th>
                        <th class="text-center">Tanggal</th>
                        <th class="text-center">Kepada</th>
                        <th class="text-center hidden-xs">Keterangan</th>
                    </tr>

                <?php
                if ($nilai) {
                    foreach ($nilai as $row) { ?>
                        <tr>
                            <td class="text-bold"># <?php echo $row['idpaket'] ?></td>
                            <td><?php echo $this->fc->idtgl( $row['tglrekam'], 'tgljam' ) ?></td>
                            <td><?php echo $row['nmterima'] ?></td>
                            <td class="hidden-xs"><?php echo $row['keterangan'] ?></td>
                        </tr>
                    <?php
                    }
                } else { ?>
                        <tr><td colspan="4" class="text-danger text-center"> Data tersebut tidak ditemukan ...</td></tr>
                <?php } ?>

                </table>
              </div>
              <p class="small text-primary text-right" style="margin-bottom:0px">
                  * Pengambilan paket dari luar negeri harap mempersiapkan biaya paket pos. &nbsp;
              </p>

            </div>

            <div class="box-footer clearfix">
                <?php echo $this->pagination->create_links(); ?>
            </div>
        </div>
    </div>

    <div class="col-md-3">
        <div class="small-box bg-aqua">
            <div class="inner">
                <h3><?php echo $total['baru'] ?></h3>
                <p>Paket belum diambil</p>
            </div>
            <div class="icon">
                <i class="ion ion-bag"></i>
            </div>
            <span class="small-box-footer"> Silahkan Menghubungi Puslay </span>
        </div>

        <!-- <div class="box box-widget">
            <div class="box-body chart-responsive">
                <div class="chart" id="paket-chart" style="height: 220px; position: relative;"></div>
            </div>
        </div> -->

        <div class="info-box bg-yellow" style="margin-bottom:10px">
            <span class="info-box-icon"><i class="fa fa-gift"></i></span>

            <div class="info-box-content">
                <?php $persen=0; if ( $total['total']>0 ) $persen = round( ($total['total'] - $total['baru']) / $total['total'] * 100, 2); ?>
                <span class="info-box-text">Jumlah Paket <b><?php echo $total['tahun'] ?></b></span>
                <span class="info-box-number"><?php echo $total['total'] ?></span>
                <div class="progress">
                    <div class="progress-bar" style="width: <?php echo $persen ?>%"></div>
                </div>
                <span class="progress-description">
                    <?php echo $persen ?>% Sudah diambil
                </span>
            </div>
        </div>
    </div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="<?=base_url('assets/plugins/morris/morris.min.js');?>" type="text/javascript"></script>
<script>
  $(function () {
    "use strict";

    //DONUT CHART
    var donut = new Morris.Donut({
      element: 'paket-chart',
      resize: true,
      colors: ["#F16745", "#FFC65D", "#7BC8A4","#93648D","#801638","#C22326","#41924B", "#4CC3D9"],
      data: [
        {label: "A1", value: 42},
        {label: "A2", value: 54},
        {label: "A3", value: 67},
        {label: "DSP", value: 103},
        {label: "PNBP", value: 93},
        {label: "HPP", value: 53},
        {label: "APBN", value: 83},
        {label: "Sekretariat", value: 20}
      ],
      hideHover: 'auto'
    });

  });
</script>

<script type="text/javascript">
	$('.pagination').addClass('pagination-sm no-margin pull-right');
</script>

<?php $this->load->view('paket/v_paket_rekam') ?>
