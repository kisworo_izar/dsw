
<link href="<?= base_url('assets/plugins/select2/select2.css');?>" rel="stylesheet" type="text/css" />
<link href="<?=base_url('assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css');?>" rel="stylesheet" type="text/css" />
<link href="<?=base_url(); ?>assets/plugins/uploadfile/fileinput.css" rel="stylesheet">
<link href="<?= base_url('assets/dist/css/renja.css') ?>" type="text/css" rel="stylesheet" />

<script src='<?= base_url('assets/plugins/select2/select2.full.min.js');?>'></script>
<script src="<?=base_url('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js');?>" type="text/javascript"></script>
<script src="<?=base_url();?>assets/plugins/uploadfile/fileinput.min.js" type="text/javascript"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>


<style media="screen">
   .tableFixHead    { overflow-y: auto; height: 76%; }
   .tableFixHead th { position: sticky; top: -1px; }
   .ketInfo {color: white; font-weight: bold;background: #74BEDA; padding: 3px 5px}
   .ketDanger {color: white; font-weight: bold;background: red; padding: 3px 5px}
   .ketSuccess {color: white; font-weight: bold;background: green; padding: 3px 5px}
   .ketWarning {color: white; font-weight: bold;background: yellow; padding: 3px 5px}
   table  { border-collapse: collapse; width: 100%; }
   th     { background:#3C6DB0; z-index: 9; color: #FFFFFF}
   td     { padding: 3px 7px!important; }
   .selected {
      background-color:rgb(230, 141, 40)!important;
      color: #FFF!important;
   }

   label {padding-top:7px}
   .select2-container .select2-selection { height: 34px; border-color: #D2D6DE}
   .select2-container--default .select2-selection--single .select2-selection__arrow { margin-top: 3px;}
   .datepicker table tr th { background: #F4F4F4; color: #3C6DB0}
   th.ui-datepicker-week-end, td.ui-datepicker-week-end {display: none;}
</style>


<div class="row">
   <div class="col-md-12">
      <div class="box box-widget" style="padding:10px">
         <div class="box-header " style="padding:0px">
            <div class="tableFixHead">

               <div class="col-sm-12" style="padding:5px 0px">
                  <div class="col-sm-4" style="padding-bottom:3px">
                     <div class="row">
                        <div class="input-group col-md-12">
                           <input id="search" class="form-control" placeholder="Pencarian..." value="" type="text">
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>

         <div class="tableFixHead">
            <table id="iGrid" class="table table-hover table-bordered">
               <thead>
                  <tr>
                     <th class="text-center" width="100px">Tanggal</th>
                     <th class="text-center" >Pegawai</th>
                     <th class="text-center" width="100px">Izin</th>
                     <th class="text-center">Alasan</th>
                     <th class="text-center" width="100px">Izin Mulai</th>
                     <th class="text-center" width="100px">Izin Selesai</th>
                     <th class="text-center" width="75px">Lama</th>
                     <th class="text-center" width="30px">A</th>
                     <th class="text-center" width="30px">S</th>
                     <th class="text-center" width="30px">T</th>
                     <th class="text-center" width="100px">Status</th>
                  </tr>
               </thead>
               <tbody id="data_">
                  <?php
                  if ($izinList) {
                     foreach ($izinList as $izin) { ?>
                        <tr style="margin:0px;">
                           <td style=" width:100px" data-toggle="collapse" data-target=".<?= $izin->id ?>">
                              <div class="text-center"><?= date("d-m-Y", strtotime($izin->tanggalsurat)) ?></div>
                           </td>
                           <td data-toggle="collapse" data-target=".<?= $izin->id ?>">
                              <div class="text-center"><?= $izin->nama ?></div>
                           </td>
                           <td style="width:100px" data-toggle="collapse" data-target=".<?= $izin->id ?>">
                              <div><?= $izin->status ?></div>
                           </td>
                           <td style="" data-toggle="collapse" data-target=".<?= $izin->id ?>">
                              <div><?= $izin->keterangan ?></div>
                           </td>
                           <td style=" width:100px;" data-toggle="collapse" data-target=".<?= $izin->id ?>">
                              <div class="text-center"><?= date("d-m-Y", strtotime($izin->tanggalmulai)) ?></div>
                           </td>
                           <td style=" width:100px;" data-toggle="collapse" data-target=".<?= $izin->id ?>">
                              <div class="text-center"><?= date("d-m-Y", strtotime($izin->tanggalselesai)) ?></div>
                           </td>
                           <td style=" width:100px;" data-toggle="collapse" data-target=".<?= $izin->id ?>">
                              <div class="text-center"><b><?= $izin->jml ?></b> Hari</div>
                           </td>
                           <td class="bg-green-active"></td>
                           <td class=<?php if($izin->setuju_status=='1') echo "bg-green-active"; elseif($izin->setuju_status=='2') echo "bg-red-active"; else echo "bg-yellow-active";  ?>></td>
                           <td class=<?php if($izin->setuju_status=='1'){if($izin->approval=='1') echo "bg-green-active"; elseif($izin->approval=='2') echo "bg-red-active"; else echo "bg-yellow-active";}  ?>></td>
                           <td style=""><?php if($izin->approval=='1') echo "<span class='label label-success'>Diterima</span>"; elseif($izin->approval=='2' or $izin->setuju_status=='2') echo "<span class='label label-danger'>Ditolak</span>"; else echo "<span class='label label-warning'>Proses</span>";  ?></td>
                        </tr>

                        <tr style="background:#ECF0F5;color:#34495E;border:0px; margin:0px">
                           <td class="hiddenRow" colspan="11" style="margin:0px!important; padding:0px!important">
                              <div class="small info <?= $izin->id ?> collapse" aria-expanded="false" style="height: 0px;">
                                 <div class="row" style="margin:10px;" >
                                    <div class="box box-widget" style="margin-bottom:0px">
                                       <div class="box-body">
                                          <div class="col-sm-12" style="padding:0px">
                                             <div class="col-sm-6">
                                                <table style="font-size: 13px">
<!-- <tr>
<td width="120px" class="text-right">Pengajuan (<b> A </b>) &nbsp;</td><td>:</td><td> <?= date("d-m-Y", strtotime($izin->tanggalsurat)) ?> </td>
</tr> -->
<!-- <tr>
<td width="120px" class="text-right">Tanggal Mulai &nbsp;</td><td>:</td><td> <?= date("d-m-Y", strtotime($izin->tanggalmulai)) ?> </td>
</tr> -->
<!-- <tr>
<td width="120px" class="text-right">Tanggal Selesai &nbsp;</td><td>:</td><td> <?= date("d-m-Y", strtotime($izin->tanggalselesai)) ?> </td>
</tr> -->
<tr>
   <td class="text-right">Pengajuan Oleh &nbsp;</td><td>:</td><td><?= $izin->nama ?> </td>
</tr>
<tr>
   <td class="text-right"></td><td></td><td>  <?= $izin->nip18 ?></td>
</tr>
<tr>
   <td class="text-right"></td><td></td><td> <?= $izin->bawahan_jabatan ?></td>
</tr>
<tr>
   <td class="text-right">Dokumen &nbsp;</td><td>:</td><td> <?= ($izin->dokumen == '' ? '' : "<a href='' class='btn btn-social btn-dropbox'><i class='fa fa-cloud-download'></i>".$izin->dokumen."</a>") ?> </td>
</tr>
<tr>
   <td class="text-right" style="vertical-align: top">Catatan &nbsp;</td><td  style="vertical-align: top">:</td><td> <?= $izin->catatan ?> </td>
</tr>
<?php 
if ($izin->status == 'TMH') {
   $datang_time = explode(" ", $izin->tanggalmulai)[1];
   if ($datang_time != '00:00:00') { ?>
<tr><td class="text-right">Jam Datang &nbsp;</td><td>:</td><td> <?= $datang_time ?> </td></tr>
<?php
   }
   $pulang_time = explode(" ", $izin->tanggalselesai)[1];
   if ($pulang_time != '00:00:00') { ?>
<tr><td class="text-right">Jam Pulang &nbsp;</td><td>:</td><td> <?= $pulang_time ?> </td></tr>
<?php
   }
} ?>

</table>
</div>
<div class="col-sm-4">
   <table style="font-size: 13px">
      <tr>
         <td class="text-right">Persetujuan Status (<b> S </b>) &nbsp;</td><td>:</td><td><?php if($izin->setuju_status=='1') echo "<span class='label label-success'>Diterima</span>"; elseif($izin->setuju_status=='2') echo "<span class='label label-danger'>Ditolak</span>"; else echo "<span class='label label-warning'>Proses</span>";  ?></td>
      </tr>
      <tr>
         <td class="text-right">Persetujuan Pada &nbsp;</td><td>:</td><td> <?php if($izin->setuju_waktu==null) echo "-"; else echo date("d-m-Y", strtotime($izin->setuju_waktu)); ?> </td>
      </tr>
      <tr>
         <td class="text-right">Penetapan Status (<b> T </b>) &nbsp;</td><td>:</td><td> <?php if($izin->approval=='1') echo "<span class='label label-success'>Diterima</span>"; elseif($izin->approval=='2') echo "<span class='label label-danger'>Ditolak</span>"; elseif($izin->approval=='3' and $izin->setuju_status=='1') echo "<span class='label label-warning'>Proses</span>"; else echo "-";  ?> </td>
      </tr>
      <tr>
         <td class="text-right">Penetapan Pada &nbsp;</td><td>:</td><td> <?php if($izin->tanggalapproval==null) echo "-"; else echo date("d-m-Y", strtotime($izin->tanggalapproval)) ?> </td>
      </tr>

   </table>
</div>
</div>
</div>
<?php if($izin->setuju_status == null) { ?>
   <div class="box-footer">
      <div class="col-xs-2 pull-right">
         <button type="button" class="btn btn-block btn-success btn-flat" onclick="persetujuan('<?= $izin->id ?>','1')">Setuju</button>
      </div>
      <div class="col-xs-2 pull-right">
         <button type="button" class="btn btn-block btn-danger btn-flat" data-toggle="modal" data-target="#tolakModal" data-izin="<?= $izin->id ?>">Tolak</button>
      </div>
   </div>

<?php } ?>

</div>
</div>
</div>
</td>
</tr>

<?php
}
} else { ?>
   <tr><td colspan="11" class="text-danger"> Data tidak ada ...</td></tr>
<?php } ?>
</tbody>
</table>


</div>

</div>
</div>
</div>

<!-- tolakModal -->
<div class="modal fade" id="tolakModal" tabindex="-1" role="dialog">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Catatan</h4>
         </div>
         <div class="modal-body">
            <!-- <form id="tolakForm" action="<?php echo site_url('milea/persetujuan')?>" method="POST" class="" enctype="multipart/form-data"> -->
               <form id="tolakForm" >
                  <!-- <form> -->
                     <input type="hidden" name="izin" id="izin" >
                     <input type="hidden" name="status" id="status" value="2" >
                     <div class="form-group">
                        <textarea class="form-control" id="catatan" name="catatan"></textarea>
                     </div>
                     <div class="modal-footer">
                        <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Tutup</button>
                        <button id="tolak" type="submit" class="btn btn-danger btn-flat">Tolak</button>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
      <!-- tolakModal End -->

      <script type="text/javascript" src="<?=base_url();?>assets/plugins/bootstrapvalidator/js/bootstrapValidator.js"></script>
      <script type="text/javascript" src="<?=base_url();?>assets/plugins/bootstrapvalidator/js/language/id_ID.js"></script>
      <script type="text/javascript">
         $(function() {
            $('#tolakForm').bootstrapValidator({
               excluded: [':disabled', ':hidden'],
               feedbackIcons: {
                  valid: 'glyphicon glyphicon-ok',
                  invalid: 'glyphicon glyphicon-remove',
                  validating: 'glyphicon glyphicon-refresh'
               },
               fields: {
                  catatan: {
                     validators: {
                        notEmpty: {
                        }
                     }
                  }
               }
            })
            .on('success.form.bv', function(e) {
               e.preventDefault();
               var $form = $(e.target);
               var bv = $form.data('bootstrapValidator');
               persetujuan($('#izin').val(),'2');
            // $.post($form.attr('action'), $form.serialize(), function(result) {
            //    location.reload();
            // }, 'json');
         });
         });

      </script>

      <script type="text/javascript">
         $('#tolakModal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget) ;
            var izin = button.data('izin') 
            $('#izin').val(button.data('izin') );
         // $('#tolak').attr( "onclick", "persetujuan("+izin+","+"'2')" );
      })

         $('.collapse').on('show.bs.collapse', function () {
            $('.collapse.in').collapse('hide');
         });

         var $izins = $('#data_ tr');
         $('#search').keyup(function() {
            var val = $.trim($(this).val()).replace(/ +/g, ' ').toLowerCase();

            $izins.show().filter(function() {
               var text = $(this).text().replace(/\s+/g, ' ').toLowerCase();
               return !~text.indexOf(val);
            }).hide();
         });

         $('.tglStart,.tglEnd').datepicker({
            format: "dd-mm-yyyy",
            language: "id",
            orientation: "bottom auto",
            autoclose: true,
            todayHighlight: true
         }).on('changeDate', function() {
            var idnip = $('#idnip').val();
            sta   = $('#start').val();
            end   = $('#end').val();
            window.location.href = "<?php echo site_url('milea/manual') ?>" +'/'+ idnip +'/'+ sta +'/'+ end;
         });


         $(document).ready(function() {
            $.fn.modal.Constructor.prototype.enforceFocus = function() {};
            $('.select_tandatangan').select2({
               placeholder: "Pilih Penandatangan"
            });
            $('.select_status').select2({
               placeholder: "Pilih Status"
            });
         });

         function persetujuan(id,status){
            var catatan = '';
            if (status === '2') {
               catatan = $('#catatan').val();
            // if(catatan==""){alert ('Catatan Harus Diisi!'); return false;}
         }
         window.location.href = '<?= site_url('milea/persetujuan') ?>' +'/'+ id +'/'+ status + '/' + catatan;
      }

   </script>
