<style media="screen">
 .tableFixHead    { overflow-y: auto; height: 76%; }
 .tableFixHead th { position: sticky; top: -1px; }
 .ketInfo {color: white; font-weight: bold;background: #74BEDA; padding: 3px 5px}
 .ketDanger {color: white; font-weight: bold;background: red; padding: 3px 5px}
 .ketSuccess {color: white; font-weight: bold;background: green; padding: 3px 5px}
 .ketWarning {color: white; font-weight: bold;background: yellow; padding: 3px 5px}
 table  { border-collapse: collapse; width: 100%; }
 th     { background:#3C6DB0; z-index: 9; color: #FFFFFF}
 td     { padding: 3px 7px!important; }
 .selected {
  background-color:rgb(230, 141, 40)!important;
  color: #FFF!important;
}

label {padding-top:7px}
.select2-container .select2-selection { height: 34px; border-color: #D2D6DE}
.select2-container--default .select2-selection--single .select2-selection__arrow { margin-top: 3px;}
.datepicker table tr th { background: #F4F4F4; color: #3C6DB0}
th.ui-datepicker-week-end, td.ui-datepicker-week-end {display: none;}
</style>

<div class="row">
  <div class="col-xs-12">
    <!-- Custom Tabs -->
    <div class="nav-tabs-custom">
      <ul class="nav nav-tabs">
        <li class="active"><a href="#myviolation" data-toggle="tab" aria-expanded="true">PELANGGARAN SAYA</a></li>
        <?php if ($isAllowed) { ?>
          <li class=""><a href="#unitviolation" data-toggle="tab" aria-expanded="false">PELANGGARAN UNIT</a></li>
        <?php } ?>
      </ul>
      <div class="tab-content">
        <div class="tab-pane fade in active" id="myviolation">

          <div class="row">
            <div class="col-md-12">
              <div class="box-group" id="accordion">

                <?php foreach ($pelanggaranHistory as $year => $pelanggaran) { ?>
                  <div class="panel box box-primary">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse<?= $year ?>">
                      <div class="box-header with-border">
                        <h4 class="box-title">
                          <?= $year ?>
                        </h4>
                      </div>
                    </a>
                    <div id="collapse<?= $year ?>" class="panel-collapse collapse <?= ( ($year == date('Y') ? 'in' : '') ) ?>">
                      <div class="box-body">

                        <div class="row">
                          <div class="col-md-6">
                            <?php if ($pelanggaran['minutetotal'] <= 1/3 * $pelanggaranMax) {
                              $colour = 'green';
                            } elseif ($pelanggaran['minutetotal'] <= 2/3 * $pelanggaranMax) {
                              $colour = 'yellow';
                            } else {
                              $colour = 'red';
                            } ?>
                            <div class="info-box bg-<?= $colour ?>">
                              <span class="info-box-icon"><i class="fa fa-balance-scale"></i></span>

                              <div class="info-box-content">
                                <span class="info-box-text">Total Pelanggaran</span>
                                <span class="info-box-number"><?= floor($pelanggaran['minutetotal']/60).' Jam '.($pelanggaran['minutetotal'] % 60).' Menit' ?></span>

                                <div class="progress">
                                  <div class="progress-bar" style="width: <?= $pelanggaran['minutetotal']/$pelanggaranMax * 100 ?>%"></div>
                                </div>
                                <span class="progress-description">
                                  Batas: <?= floor($pelanggaranMax/60).' Jam '.($pelanggaranMax % 60).' Menit' ?> 
                                </span>
                              </div>
                              <!-- /.info-box-content -->
                            </div>
                          </div>

                          <div class="col-md-6">
                            <!-- /.info-box -->
                            <div class="small-box bg-aqua">
                              <div class="inner">
                                <h3><?= number_format($pelanggaran['times']) ?>x</h3>

                                <p>Pelanggaran Terjadi</p>
                              </div>
                              <div class="icon">
                                <i class="fa fa-ban"></i>
                              </div>
                            </div>
                          </div>
                        </div>

                        <div class="row">
                          <div class="col-xs-12">
                            <table class="table table-hover table-bordered">
                              <thead>
                                <tr>
                                  <th class="text-center">Hari</th>
                                  <th class="text-center">Tanggal</th>
                                  <th class="text-center">Status</th>
                                  <th class="text-center">Pelanggaran</th>
                                </tr>
                              </thead>

                              <tbody>
                                <?php foreach ($pelanggaran['detail'] as $detail) { ?>
                                  <tr>
                                    <td><?= $this->fc->idtgl($detail->tgl,'mig') ?></td>
                                    <td><?= $this->fc->idtgl($detail->tgl) ?></td>
                                    <td><?= $detail->status ?></td>
                                    <td class="text-right"><?= number_format($detail->pelanggaran) ?></td>
                                  </tr>
                                <?php } ?>
                              </tbody>
                            </table>
                            <?php if (empty($pelanggaran['detail'])) { ?>
                              <div class="callout callout-success">
                                <h4>Selamat!</h4>

                                <p>Anda tidak pernah melakukan pelanggaran.</p>
                              </div>
                            <?php } ?>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                <?php } ?>
                
              </div>
            </div>
          </div>

          

          
        </div>
        <!-- /.tab-pane -->
        <?php if ($isAllowed) { ?>
          <div class="tab-pane fade in" id="unitviolation">
            <?php $this->load->view('absensi/unitviolation'); ?>
          </div>
        <?php } ?>
        <!-- /.tab-pane -->
      </div>
      <!-- /.tab-content -->
    </div>
    <!-- nav-tabs-custom -->
  </div>
</div>
