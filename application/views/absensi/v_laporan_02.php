<script src="<?=base_url('assets/dist/js/excellentexport.js'); ?>" type="text/javascript"></script>

<style media="screen">
   .container {overflow-y: auto;padding: 0px;padding-top: 0px;width: 100%}
   .j1 {font-weight: bold; text-align: center; margin: 3px}
   .j2 {font-weight: bold; text-align: center; margin: 3px}
   .j3 {text-align: center; margin: 3px}
   tr {font-size: 10pt}
   th { text-align: center; }
   .th_{text-align:center;vertical-align: middle !important;border-style:solid; border-width:1px;overflow:hidden; word-break:normal; padding: 0px!important}
   .merah{background: red}

</style>
<?php
   $judul1 = "DIREKTORAT JENDERAL ANGGARAN";
   $judul2 = "LAPORAN KETERTIBAN PEGAWAI";
   $jData  = 31;
?>


<div class="row" style="margin-bottom:0px; padding-bottom:0px">
   <div class="col-md-12">
      <div class="box box-widget">

         <div class="box-header">
            <h4 class="j1"><?= $judul1; ?></h4>
            <h5 class="j2"><?= $judul2; ?></h5>
            <h5 class="j3">Periode : <?php echo $this->fc->idtgl($start,'hr'); ?> - <?php echo $this->fc->idtgl($end,'hr'); ?> </h5>
            <div style="float: left">
               <table style="margin-top: 10px; margin-bottom: 5px">
                  <tr class="text-bold"><td width="100px">Unit Eselon II</td> <td>:&nbsp; </td><td><?= $pil['es2']['namaorganisasi']; ?></td></tr>
                  <tr class="text-bold"><td width="100px">Unit Eselon III</td><td>:&nbsp; </td><td><?= $pil['es3']['namaorganisasi']; ?></td></tr>
                  <tr class="text-bold"><td width="100px">Unit Eselon IV</td> <td>:&nbsp; </td><td><?= $pil['es4']['namaorganisasi']; ?></td></tr>
               </table>
            </div>
            <div style="width:100px; float: right; margin-top: 40px">
               <?php $nmFile = 'L02 '.$judul2.', '.$this->fc->idtgl($start).' sd '.$this->fc->idtgl($end);?>
               <span class="pull-right">
                  <a download="<?php echo $nmFile.'.xls'?>" href="#" onclick="return ExcellentExport.excel(this, 'cetak', 'Lap 01');"><b>XLS</b></a> &nbsp;&nbsp;
               </span>
            </div>
            <div class="container">

               <table id="iGrid" class="table table-hover table-bordered table-striped" style="width: 100%;table-layout:fixed;">
                  <colgroup>
                     <col width="25px"><col width="210px">
                     <?php for ($i=0; $i < $jData; $i++) {
                     ?>
                        <col width="33px">
                     <?php
                     } ?>
                  </colgroup>
                  <thead>
                     <tr style="color: #fff; background-color: #605CA8;">
                        <th class="th_" rowspan="2" style="border-left: solid 1px #605CA8; border-top: solid 1px #605CA8"> NO </th>
                        <th class="th_" rowspan="2" style="border-top: solid 1px #605CA8"> PEGAWAI </th>
                        <th style="padding:5px; border-bottom:solid 0px; border-top: solid 1px #605CA8" class="th_" colspan="<?= $jData?>"> KODE KEHADIRAN </th>
                     </tr>
                     <tr style="color: #fff; background-color: #605CA8;">
                           <th class="th_">HN</th>
                           <th class="th_">TL1</th>
                           <th class="th_">PSW1</th>
                           <th class="th_">T1P1</th>
                           <th class="th_">TK</th>
                           <th class="th_">TL2</th>
                           <th class="th_">TL3</th>
                           <th class="th_">PSW2</th>
                           <th class="th_">PSW3</th>
                           <th class="th_">PSW4</th>
                           <th class="th_">TIP2</th>
                           <th class="th_">TIP3</th>
                           <th class="th_">TIP4</th>
                           <th class="th_">T2P1</th>
                           <th class="th_">T2P2</th>
                           <th class="th_">T2P3</th>
                           <th class="th_">T2P4</th>
                           <th class="th_">T3P1</th>
                           <th class="th_">T3P2</th>
                           <th class="th_">T3P3</th>
                           <th class="th_">T3P4</th>
                           <th class="th_">CB</th>
                           <th class="th_">CH</th>
                           <th class="th_">CLTN</th>
                           <th class="th_">CP</th>
                           <th class="th_">CSRI</th>
                           <th class="th_">CSRJ</th>
                           <th class="th_">CT</th>
                           <th class="th_">DL</th>
                           <th class="th_">IP</th>
                           <th class="th_">TB</th>

                     </tr>
                  </thead>

                  <tbody>
                     <?php $no=0; foreach($trt as $row) { $no++ ?>
                        <tr class="" style="border-bottom: solid red 1px">
                           <td style="padding: 0px 3px; border: solid #D4D7DB 1px; padding-right: 5px" class="text-right"><?= $no; ?></td>
                           <td style="padding: 0px; border: solid #D4D7DB 1px; padding-left: 5px"><?= $row['nip'] ?><br><?= strtoupper($row['nama']) ?></td>
                           <td style="padding: 0px; border: solid #D4D7DB 1px; text-align: center; vertical-align: middle" ><?= $row['HN'] + $row['RDK']?></td>
                           <td style="padding: 0px; border: solid #D4D7DB 1px; text-align: center; vertical-align: middle" ><?= $row['TL1'] ?></td>
                           <td style="padding: 0px; border: solid #D4D7DB 1px; text-align: center; vertical-align: middle" ><?= $row['PSW1'] ?></td>
                           <td style="padding: 0px; border: solid #D4D7DB 1px; text-align: center; vertical-align: middle" ><?= $row['T1P1'] ?></td>
                           <td style="padding: 0px; border: solid #D4D7DB 1px; text-align: center; vertical-align: middle" ><?= $row['TK'] ?></td>
                           <td style="padding: 0px; border: solid #D4D7DB 1px; text-align: center; vertical-align: middle" ><?= $row['TL2'] ?></td>
                           <td style="padding: 0px; border: solid #D4D7DB 1px; text-align: center; vertical-align: middle" ><?= $row['TL3'] ?></td>
                           <td style="padding: 0px; border: solid #D4D7DB 1px; text-align: center; vertical-align: middle" ><?= $row['PSW2'] ?></td>
                           <td style="padding: 0px; border: solid #D4D7DB 1px; text-align: center; vertical-align: middle" ><?= $row['PSW3'] ?></td>
                           <td style="padding: 0px; border: solid #D4D7DB 1px; text-align: center; vertical-align: middle" ><?= $row['PSW4'] ?></td>
                           <td style="padding: 0px; border: solid #D4D7DB 1px; text-align: center; vertical-align: middle" ><?= $row['T1P2'] ?></td>
                           <td style="padding: 0px; border: solid #D4D7DB 1px; text-align: center; vertical-align: middle" ><?= $row['T1P3'] ?></td>
                           <td style="padding: 0px; border: solid #D4D7DB 1px; text-align: center; vertical-align: middle" ><?= $row['T1P4'] ?></td>
                           <td style="padding: 0px; border: solid #D4D7DB 1px; text-align: center; vertical-align: middle" ><?= $row['T2P1'] ?></td>
                           <td style="padding: 0px; border: solid #D4D7DB 1px; text-align: center; vertical-align: middle" ><?= $row['T2P2'] ?></td>
                           <td style="padding: 0px; border: solid #D4D7DB 1px; text-align: center; vertical-align: middle" ><?= $row['T2P3'] ?></td>
                           <td style="padding: 0px; border: solid #D4D7DB 1px; text-align: center; vertical-align: middle" ><?= $row['T2P4'] ?></td>
                           <td style="padding: 0px; border: solid #D4D7DB 1px; text-align: center; vertical-align: middle" ><?= $row['T3P1'] ?></td>
                           <td style="padding: 0px; border: solid #D4D7DB 1px; text-align: center; vertical-align: middle" ><?= $row['T3P2'] ?></td>
                           <td style="padding: 0px; border: solid #D4D7DB 1px; text-align: center; vertical-align: middle" ><?= $row['T3P3'] ?></td>
                           <td style="padding: 0px; border: solid #D4D7DB 1px; text-align: center; vertical-align: middle" ><?= $row['T3P4'] ?></td>
                           <td style="padding: 0px; border: solid #D4D7DB 1px; text-align: center; vertical-align: middle" ><?= $row['CB'] ?></td>
                           <td style="padding: 0px; border: solid #D4D7DB 1px; text-align: center; vertical-align: middle" ><?= $row['CH'] ?></td>
                           <td style="padding: 0px; border: solid #D4D7DB 1px; text-align: center; vertical-align: middle" ><?= $row['CLTN'] ?></td>
                           <td style="padding: 0px; border: solid #D4D7DB 1px; text-align: center; vertical-align: middle" ><?= $row['CP'] ?></td>
                           <td style="padding: 0px; border: solid #D4D7DB 1px; text-align: center; vertical-align: middle" ><?= $row['CSRI'] ?></td>
                           <td style="padding: 0px; border: solid #D4D7DB 1px; text-align: center; vertical-align: middle" ><?= $row['CSRJ'] ?></td>
                           <td style="padding: 0px; border: solid #D4D7DB 1px; text-align: center; vertical-align: middle" ><?= $row['CT'] ?></td>
                           <td style="padding: 0px; border: solid #D4D7DB 1px; text-align: center; vertical-align: middle" ><?= $row['DL'] ?></td>
                           <td style="padding: 0px; border: solid #D4D7DB 1px; text-align: center; vertical-align: middle" ><?= $row['IP'] ?></td>
                           <td style="padding: 0px; border: solid #D4D7DB 1px; text-align: center; vertical-align: middle" ><?= $row['TB'] ?></td>

                           <!-- <?php foreach($row['isi'] as $nil) { ?> -->
                              <td style="padding: 0px; border: solid #D4D7DB 1px; text-align: center" class="text-right">  </td>
                           <!-- <?php } ?> -->

                        </tr>
                  <?php } ?>
                  </tbody>
               </table>

            </div>
         </div>
      </div>

   </div>
</div>

<div class="" style="display: none">
   <table id="cetak">
      <?php $vCol = $jData + 3; ?>
      <tr><td align="center" colspan="<?php echo $vCol ?>"><b><?= $judul1; ?> </b></td></tr>
      <tr><td align="center" colspan="<?php echo $vCol ?>"><b><?= $judul2; ?> </b></td></tr>
      <tr><td align="center" colspan="<?php echo $vCol ?>">Periode : <?php echo $this->fc->idtgl($start,'hr'); ?> - <?php echo $this->fc->idtgl($end,'hr'); ?></td></tr>
      <tr> <td colspan="<?php echo $vCol ?>"><b>Unit Eselon 2 : <?= $pil['es2']['namaorganisasi']; ?></b></td></tr>
      <tr> <td colspan="<?php echo $vCol ?>"><b>Unit Eselon 3 : <?= $pil['es3']['namaorganisasi']; ?></b></td></tr>
      <tr> <td colspan="<?php echo $vCol ?>"><b>Unit Eselon 4 : <?= $pil['es4']['namaorganisasi']; ?></b></td></tr>

      <tr>
         <td style="vertical-align: middle" align="center" rowspan="2"> <b> NO </b></td>
         <td style="vertical-align: middle" align="center" rowspan="2"> <b> NIP </b></td>
         <td style="vertical-align: middle" align="center" rowspan="2"> <b> NAMA </b></td>
         <td align="center" colspan="<?= $jData?>"> <b> KODE KEHADIRAN </b></td>
      </tr>
      <tr>
         <th>HN</th>
         <th>TL1</th>
         <th>PSW1</th>
         <th>P1P1</th>
         <th>TK</th>
         <th>TL2</th>
         <th>TL3</th>
         <th>PSW2</th>
         <th>PSW3</th>
         <th>PSW4</th>
         <th>TIP2</th>
         <th>TIP3</th>
         <th>TIP4</th>
         <th>T2P1</th>
         <th>T2P2</th>
         <th>T2P3</th>
         <th>T2P4</th>
         <th>T3P1</th>
         <th>T3P2</th>
         <th>T3P3</th>
         <th>T3P4</th>
         <th>CB</th>
         <th>CM</th>
         <th>CLTN</th>
         <th>CAPI</th>
         <th>CSRI</th>
         <th>CSRJ</th>
         <th>CT</th>
         <th>DL</th>
         <th>IP</th>
         <th>TB</th>
      </tr>

      <?php $no=0; foreach($trt as $row) { $no++ ?>
         <tr>
            <td class="text-right"><?= $no; ?></td>
            <td><?= "&nbsp;".$row['nip'] ?></td>
            <td><?= strtoupper($row['nama']) ?></td>
            <td style="text-align: center; vertical-align: middle" ><?= $row['HN'] + $row['RDK'] ?></td>
            <td style="text-align: center; vertical-align: middle" ><?= $row['TL1'] ?></td>
            <td style="text-align: center; vertical-align: middle" ><?= $row['PSW1'] ?></td>
            <td style="text-align: center; vertical-align: middle" ><?= $row['T1P1'] ?></td>
            <td style="text-align: center; vertical-align: middle" ><?= $row['TK'] ?></td>
            <td style="text-align: center; vertical-align: middle" ><?= $row['TL2'] ?></td>
            <td style="text-align: center; vertical-align: middle" ><?= $row['TL3'] ?></td>
            <td style="text-align: center; vertical-align: middle" ><?= $row['PSW2'] ?></td>
            <td style="text-align: center; vertical-align: middle" ><?= $row['PSW3'] ?></td>
            <td style="text-align: center; vertical-align: middle" ><?= $row['PSW4'] ?></td>
            <td style="text-align: center; vertical-align: middle" ><?= $row['T1P2'] ?></td>
            <td style="text-align: center; vertical-align: middle" ><?= $row['T1P3'] ?></td>
            <td style="text-align: center; vertical-align: middle" ><?= $row['T1P4'] ?></td>
            <td style="text-align: center; vertical-align: middle" ><?= $row['T2P1'] ?></td>
            <td style="text-align: center; vertical-align: middle" ><?= $row['T2P2'] ?></td>
            <td style="text-align: center; vertical-align: middle" ><?= $row['T2P3'] ?></td>
            <td style="text-align: center; vertical-align: middle" ><?= $row['T2P4'] ?></td>
            <td style="text-align: center; vertical-align: middle" ><?= $row['T3P1'] ?></td>
            <td style="text-align: center; vertical-align: middle" ><?= $row['T3P2'] ?></td>
            <td style="text-align: center; vertical-align: middle" ><?= $row['T3P3'] ?></td>
            <td style="text-align: center; vertical-align: middle" ><?= $row['T3P4'] ?></td>
            <td style="text-align: center; vertical-align: middle" ><?= $row['CB'] ?></td>
            <td style="text-align: center; vertical-align: middle" ><?= $row['CH'] ?></td>
            <td style="text-align: center; vertical-align: middle" ><?= $row['CLTN'] ?></td>
            <td style="text-align: center; vertical-align: middle" ><?= $row['CAPI'] ?></td>
            <td style="text-align: center; vertical-align: middle" ><?= $row['CSRI'] ?></td>
            <td style="text-align: center; vertical-align: middle" ><?= $row['CSRJ'] ?></td>
            <td style="text-align: center; vertical-align: middle" ><?= $row['CT'] ?></td>
            <td style="text-align: center; vertical-align: middle" ><?= $row['DL'] ?></td>
            <td style="text-align: center; vertical-align: middle" ><?= $row['IP'] ?></td>
            <td style="text-align: center; vertical-align: middle" ><?= $row['TB'] ?></td>
         </tr>
   <?php } ?>

   </table>
</div>
