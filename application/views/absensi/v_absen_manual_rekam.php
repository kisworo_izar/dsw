<!-- Bootstrap time Picker -->
<link rel="stylesheet" href="<?=base_url();?>assets/plugins/timepicker/bootstrap-timepicker.min.css">
<!-- iCheck for checkboxes and radio inputs -->
<link rel="stylesheet" href="<?=base_url();?>assets/plugins/iCheck/all.css">
<style type="text/css">
  #ajuForm .form-control-feedback {
    top: 0;
    right: -15px;
  }
</style>

<!-- Modal -->
<div class="modal fade" id="input" tabindex="-1" role="dialog" aria-labelledby="input" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
    <div class="modal-content">
      <!-- header -->
      <div class="modal-header">
        <h4 class="modal-title">Pengajuan Izin Pemberitahuan </h4>
      </div>
          <form id="ajuForm" action="<?php echo site_url('milea/crud')?>" method="POST" class="" enctype="multipart/form-data">
      <div class="modal-body">
        <div class="container-fluid">
          <!-- Form Pengajuan -->
            <input type="hidden" name="nip" id="nip" value="<?= $data_peg['nip18'] ?>">
            <input type="hidden" name="iduser" value="<?= $data_peg['iduser'] ?>">
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label class="col-md-3 control-label"> </label>
                  <h4 class="col-md-9 control-label">  </h4>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label class="col-md-3 control-label text-right">Izin</label>
                  <div class="col-md-9" style="margin-bottom:5px">
                    <select class="select_status org form-control" data-placeholder="Pilih Izin" name="sts" data-bv-remote-name="isMatchStatusTanggal" id="sts" style="width: 100%" size="5">
                      <option></option>
                      <?php foreach ($hdr['dat'] as $row) {
                        echo '<option value="'. $row['kode'] .'">'. $row['uraian']." ( ".$row['pelanggaran']." )".'</option>';} ?>
                      </select>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group col-md-6">
                    <label class="col-md-6 control-label text-right">Tanggal Mulai</label>
                    <div class="col-md-6" style="margin-bottom:5px">
                      <div class="input-group date date_tgl3">
                        <input type="text"  name="tglmulai" id="tglmulai" class="form-control tglmulai" placeholder="Tanggal Mulai" autocomplete="off" />
                        <span class="input-group-addon"><i class="fa fa-calendar text-primary"></i></span>
                      </div>
                    </div>
                  </div>
                  <div class="form-group col-md-6">
                    <label class="col-md-6 control-label text-right">Tanggal Selesai</label>
                    <div class="col-md-6" style="margin-bottom:5px">
                      <div class="input-group date date_tgl4">
                        <input type="text"  name="tglselesai" id="tglselesai" class="form-control tglselesai" placeholder="Tanggal Selesai" autocomplete="off" />
                        <span class="input-group-addon"><i class="fa fa-calendar text-primary"></i></span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row" id="jam" hidden>
                <div class="col-md-12">
                  <div class="col-md-3">
                  </div>
                  <div class="col-xs-4">
                    <div class="form-group">
                      <div class="col-md-12">
                        <div class="minimal">
                          <label class="control-label">
                            <input type="checkbox" class="" id="datang" value="datang" name="datangpulang[]">
                            Jam Datang
                          </label>
                        </div>
                        <div class="minimal">
                          <label class="control-label">
                            <input type="checkbox" class="" id="pulang" value="pulang" name="datangpulang[]">
                            Jam Pulang
                          </label>
                        </div>
                        <div class="help-block with-errors"></div>
                      </div>
                    </div>
                  </div>
                  <div class="col-xs-4">
                    <div class="form-group ">
                      <div class="col-xs-12">
                        <div class="input-group" style="margin-bottom:5px">
                          <input type="text" class="form-control timepicker" id="datang_jam" name="datang_jam" disabled>
                          <span class="input-group-addon">
                            <i class="fa fa-clock-o text-primary"></i>
                          </span>
                        </div>
                      </div>
                    </div>
                    <div class="form-group ">
                      <div class="col-xs-12">
                        <div class="input-group" style="margin-bottom:5px">
                          <input type="text" class="form-control timepicker" id="pulang_jam" name="pulang_jam" disabled>
                          <span class="input-group-addon">
                            <i class="fa fa-clock-o text-primary"></i>
                          </span>
                        </div>
                      </div>
                    </div>
                  </div>
                  
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="col-md-3 control-label text-right">Keterangan</label>
                    <div class="col-md-9" style="margin-bottom:5px">
                      <textarea class="form-control"  rows="2" name="ket" id="ket" width="100%" autocomplete="off"></textarea>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row" id="dokumen" hidden>
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="col-md-3 control-label text-right">Dokumen (jpg/jpeg/png)</label>
                    <div class="col-md-9" style="margin-bottom:5px">
                      <!-- <input  name="nmfile" id="nmfile" type="hidden" style="height:34px"> -->
                      <input  name="dok" id="dok" autocomplete="off" type="file" class="form-control" style="height:34px" data-show-preview="false" />
                      <input type="hidden" name="nmfile" id="nmfile">
                      <!-- <input  name="nmfile" id="nmfile" class="form-control" type="text" readonly> -->
                      <div id="kv-success-2" class="alert alert-success" style="margin-top:10px;display:none">File Terupload !</div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="col-md-3 control-label"> </label>
                    <h4 class="col-md-9 control-label"> Persetujuan (Atasan / PLH) </h4>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="col-md-3 control-label text-right">NIP</label>
                    <div class="col-md-9" style="margin-bottom:5px;">

                      <select class="select_tandatangan org form-control" placeholder="Pilih Penandatangan" name="penandatangan" id="penandatangan" style="width: 100%" onchange="referTtd(this.value)">
                        <option></option>
                        <?php foreach ($hdr['peg'] as $row) {
                          echo '<option value="'. $row['nip18'] .'">'. $row['nip18'].'  '.$row['nama'].'</option>';
                        } ?>
                      </select>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="col-md-3 control-label text-right">Nama</label>
                    <div class="col-md-9" style="margin-bottom:5px">
                      <input class="form-control" name="nama" id="nama" autocomplete="off" type="text" style="height:34px" readonly>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="col-md-3 control-label text-right">Jabatan</label>
                    <div class="col-md-9" style="margin-bottom:5px">
                      <input class="form-control" name="jab" id="jab" autocomplete="off" type="text" style="height:34px" >
                    </div>
                  </div>
                </div>
              </div>
          </div>
        </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-primary btn-flat">Simpan</button>
                <!-- <button type="button" class="btn btn-primary btn-flat" onclick="simpan()">Simpan</button> -->
              </div>
            </form>
            <!-- Form Pengajuan End -->
      </div>
    </div>
  </div>
  <!-- Modal End -->
  <script type="text/javascript" src="<?=base_url();?>assets/plugins/bootstrapvalidator/js/bootstrapValidator.js"></script>
  <script type="text/javascript" src="<?=base_url();?>assets/plugins/bootstrapvalidator/js/language/id_ID.js"></script>
  <!-- bootstrap time picker -->
  <script src="<?=base_url();?>assets/plugins/timepicker/bootstrap-timepicker.min.js"></script>
  <!-- iCheck 1.0.1 -->
  <script src="<?=base_url();?>assets/plugins/iCheck/icheck.min.js"></script>
  <script type="text/javascript">
    $(function() {
      $('#ajuForm').bootstrapValidator({
        excluded: [':disabled', ':hidden'],
        feedbackIcons: {
          valid: 'glyphicon glyphicon-ok',
          invalid: 'glyphicon glyphicon-remove',
          validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
          sts: {
            validators: {
              notEmpty: {
              },
              remote: {
                type: 'POST',
                url: "<?= site_url('milea/isMatchStatusTanggal') ?>",
                data: function(validator){
                  return {
                    status: validator.getFieldElements('sts').val(),
                    tanggalmulai: validator.getFieldElements('tglmulai').val(),
                    tanggalselesai: validator.getFieldElements("tglselesai").val()
                  };
                }
              }
            }
          },
          tglmulai: {
            validators: {
              notEmpty: {
              },
              date: {
                format: 'DD-MM-YYYY'
              },
              remote: {
                type: 'POST',
                url: "<?= site_url('milea/isMatchStatusTanggal') ?>",
                data: function(validator){
                  return {
                    status: validator.getFieldElements('sts').val(),
                    tanggalmulai: validator.getFieldElements('tglmulai').val(),
                    tanggalselesai: validator.getFieldElements("tglselesai").val()
                  };
                }
              }
            }
          },
          tglselesai: {
            validators: {
              notEmpty: {
              },
              date: {
                format: 'DD-MM-YYYY'
              },
              remote: {
                type: 'POST',
                url: "<?= site_url('milea/isMatchStatusTanggal') ?>",
                data: function(validator){
                  return {
                    status: validator.getFieldElements('sts').val(),
                    tanggalmulai: validator.getFieldElements('tglmulai').val(),
                    tanggalselesai: validator.getFieldElements("tglselesai").val()
                  };
                }
              }
            }
          },
          ket: {
            validators: {
              notEmpty: {
              }
            }
          },
          nmfile: {
            validators: {
              notEmpty: {
              }
            }
          },
          dok: {
            validators: {
              notEmpty: {
              },
              file: {
                extension: 'jpeg,jpg,png',
                type: 'image/jpeg,image/png',
                maxSize: 2097152
              }
            }
          },
          penandatangan: {
            validators: {
              notEmpty: {
              }
            }
          },
          jab: {
            validators: {
              notEmpty: {
              }
            }
          },
          'datangpulang[]': {
            validators: {
              notEmpty: {
              }
            }
          },
          datang_jam: {
            validators: {
              notEmpty: {
              }
            }
          },
          pulang_jam: {
            validators: {
              notEmpty: {
              }
            }
          },
        }
      })      
      .on('success.form.bv', function(e) {
        e.preventDefault();
        var $form = $(e.target);
        var bv = $form.data('bootstrapValidator');

        if ($('#sts').val() == 'TMH') {
          $('#dok').fileinput('upload');
        } else {
          $.post($form.attr('action'), $form.serialize(), function(result) {
            location.reload();
          }, 'json');
        }
      })
      .find('input[name="datangpulang[]"]').iCheck({
        checkboxClass: 'icheckbox_minimal-blue',
        radioClass   : 'iradio_minimal-blue'
      }).on('ifChanged', function(e) {
        var field = $(this).attr('name');
        $('#ajuForm').bootstrapValidator('revalidateField', field);
      });


      $('.timepicker').timepicker({
        minuteStep: 1,
        showMeridian: false,
      })
    });
  </script>

  <script type="text/javascript">

    $(document).ready(function() {
      $(".org").select2({
        minimumResultsForSearch:5,
      });

      $('#penandatangan').select2('val', JSON.parse('<?php echo json_encode($atasan->nip) ?>'));

      $('#datang').on('ifChecked', function(event){
        $('#datang_jam').attr('disabled', false);
        $('#ajuForm').bootstrapValidator('revalidateField', 'datang_jam');
      });
      $('#datang').on('ifUnchecked', function(event){
        $('#datang_jam').attr('disabled', true);
        $('#ajuForm').bootstrapValidator('revalidateField', 'datang_jam');
      });

      $('#pulang').on('ifChecked', function(event){
        $('#pulang_jam').attr('disabled', false);
        $('#ajuForm').bootstrapValidator('revalidateField', 'pulang_jam');
      });
      $('#pulang').on('ifUnchecked', function(event){
        $('#pulang_jam').attr('disabled', true);
        $('#ajuForm').bootstrapValidator('revalidateField', 'pulang_jam');
      });
    });


  </script>

  <script type="text/javascript">
    $('.tglmulai').datepicker({
      startDate: "-<?php echo $krj ?>d",
      daysOfWeekDisabled: [0,6],
      format: "dd-mm-yyyy",
      language: "id",
      orientation: "bottom auto",
      autoclose: true,
      todayHighlight: true,
    })
    .on('changeDate', function(ev) {
      var endDate = $('#tglselesai').datepicker('getDate');

      if ((endDate == null) || (ev.date.valueOf() > endDate.valueOf())) {
        $('#tglselesai').datepicker('setDate', ev.date);
      }
      $('#tglselesai').datepicker('setStartDate', formatDate(ev.date));
      $('#ajuForm').bootstrapValidator('revalidateField', 'tglmulai');
      $('#ajuForm').bootstrapValidator('revalidateField', 'tglselesai');
      $('#ajuForm').bootstrapValidator('revalidateField', 'sts');
    });


    $('.tglsurat').datepicker({
      format: "dd-mm-yyyy",
      language: "id",
      orientation: "bottom auto",
      autoclose: true,
      todayHighlight: true
    })
    .on('changeDate', function() {
    });

    $('.tglselesai').datepicker({
      startDate: "-<?php echo $krj ?>d",
      daysOfWeekDisabled: [0,6],
      format: "dd-mm-yyyy",
      language: "id",
      orientation: "bottom auto",
      autoclose: true,
      todayHighlight: true
    })
    .on('changeDate', function() {
      $('#ajuForm').bootstrapValidator('revalidateField', 'tglmulai');
      $('#ajuForm').bootstrapValidator('revalidateField', 'tglselesai');
      $('#ajuForm').bootstrapValidator('revalidateField', 'sts');
    });

    $('#sts').on('select2:select', function (e) {
      if (e.params.data.id == "TMH") {
        $('#dokumen').show();
        $('#tglmulai').datepicker('setEndDate', '0d');
        var today = new Date();
        var tglmulai = $('#tglmulai').datepicker('getDate');
        $('#tglselesai').datepicker('setDate', tglmulai);
        if (tglmulai > today) {
          $('#tglmulai').datepicker('setDate', 'today');
        }
        $('#tglselesai').attr("readonly", true);
        $('#jam').show()
      } else {
        $('#dokumen').hide();
        $('#tglmulai').datepicker('setEndDate', '+1y');
        $('#tglselesai').attr("readonly", false);
        $('#jam').hide()
      }
      $('#ajuForm').bootstrapValidator('revalidateField', 'nmfile');
      $('#ajuForm').bootstrapValidator('revalidateField', 'dok');

      if (e.params.data.id == "IP") {
        $('#penandatangan').select2('val', JSON.parse('<?php echo json_encode($atasan2->nip) ?>'));
        console.log(JSON.parse('<?php echo json_encode($atasan2->nip) ?>'));
      } else {
        $('#penandatangan').select2('val', JSON.parse('<?php echo json_encode($atasan->nip) ?>'));
      }
      $('#penandatangan').select2().trigger('change');
      $('#ajuForm').bootstrapValidator('revalidateField', 'jab');

      $('#ajuForm').bootstrapValidator('revalidateField', 'tglmulai');
      $('#ajuForm').bootstrapValidator('revalidateField', 'tglselesai');
    });

    $('#penandatangan').on('select2:select', function (e) {
      $('#ajuForm').bootstrapValidator('revalidateField', 'jab');
    });



// Return string date with format dd-mm-yyyy
function formatDate(date){
  var d = new Date(date),
  month = '' + (d.getMonth() + 1),
  day = '' + d.getDate(),
  year = d.getFullYear();

  if (month.length < 2) month = '0' + month;
  if (day.length < 2) day = '0' + day;

  return [day, month, year].join('-');
}

function simpan() {
  var nmfile      = $('#nmfile').val();
// var tglsurat    = $('#tglsurat').val();
var tglmulai    = $('#tglmulai').val();
var tglselesai  = $('#tglselesai').val();
var ket         = $('#ket').val();
var nama        = $('#nama').val();
var jab         = $('#jab').val();

if ($('#sts').val() == 'TMH') {
  if(nmfile==""){alert ('Seluruh Kolom Harus Terisi !'); return false;}
}
if(tglmulai==""){alert ('Seluruh Kolom Harus Terisi !'); return false;}
if(tglselesai==""){alert ('Seluruh Kolom Harus Terisi !'); return false;}
if(ket==""){alert ('Seluruh Kolom Harus Terisi !'); return false;}
if(nama==""){alert ('Seluruh Kolom Harus Terisi !'); return false;}
if(jab==""){alert ('Seluruh Kolom Harus Terisi !'); return false;}

var isian = $('form').serialize();
$.ajax({
  url  : "<?= site_url('milea/crud') ?>",
  type : 'POST',
  data : isian
}).then(function () {
  window.location.reload();
});
}

function referTtd( nil ) {
  var dat = <?= json_encode($hdr['peg']) ?>;
  var str = "Kepala ";
  var dir = "Direktur ";

  $("#nama").val( dat[nil].nama.toUpperCase() );
  if(dat[nil].jabatan.indexOf("Kepala Subdirektorat") >= 0){
    $("#jab").val( str.concat(dat[nil].esl3).toUpperCase() );
  } else if(dat[nil].jabatan.indexOf("Kepala Bagian") >= 0) {
    $("#jab").val( str.concat(dat[nil].esl3).toUpperCase() );
  } else if(dat[nil].jabatan.indexOf("Kepala Seksi") >= 0){
    $("#jab").val( str.concat(dat[nil].esl4).toUpperCase() );
  } else if(dat[nil].jabatan.indexOf("Kepala Subbagian") >=0){
    $("#jab").val( str.concat(dat[nil].esl4).toUpperCase() );
  } else if(dat[nil].jabatan.indexOf("Direktur") >=0){
    $("#jab").val( dir.concat(dat[nil].esl2).toUpperCase() );
  } else if(dat[nil].jabatan.indexOf("Sekretaris Direktorat Jenderal") >=0){
    $("#jab").val( dat[nil].jabatan.toUpperCase());
  } else {
    $("#jab").val( dat[nil].jabatan.toUpperCase() );
  }
}


</script>

<script type="text/javascript">
  $('#dok').fileinput({
    uploadExtraData:  function(previewId, index) {
      var data = { nip : $('#nip').val(), tgl : $('#tglsurat').val(), name : 'dok' };
      return data;
    },
    uploadUrl: "<?php echo site_url('milea/fileupload') ?>",
    autoReplace: true,
    showUpload: false,
    showRemove: false,
    overwriteInitial: true,
    maxFileCount: 1,
  });

  $('#dok').on('filebatchuploadsuccess', function(event, data, extra) {
    var extra = data.extra, response = data.response;
    var out = '';
    $.each(data.files, function(key, file) {
      var fname = file.name;
      out = out + '<li>' + 'Uploaded file # ' + (key + 1) + ' - '  +  fname + ' successfully.' + '</li>';
    });
    $('#kv-success-2 ul').append(out);
    $('#kv-success-2').fadeIn('slow');
    $('#nmfile').val(response.file);

    /* Submit AjuForm */
    var isian = $('#ajuForm').serialize();
    $.ajax({
      url  : "<?= site_url('milea/crud') ?>",
      type : 'POST',
      data : isian
    }).then(function () {
      window.location.reload();
    });
  });

  $('#dok').on('filebatchuploaderror', function(event, data) {
    var response = data.response;
    alert('Gagal upload !');
  });

  $('#dok').on('fileclear', function(event, data) {
    $('#ajuForm').bootstrapValidator('revalidateField', 'dok');
  });

  $('#dok').on('filebrowse', function(event) {
    $('#dok').fileinput('clear');
});

</script>
