
<link href="<?= base_url('assets/plugins/select2/select2.css');?>" rel="stylesheet" type="text/css" />
<link href="<?=base_url('assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css');?>" rel="stylesheet" type="text/css" />
<link href="<?=base_url(); ?>assets/plugins/uploadfile/fileinput.css" rel="stylesheet">
<link href="<?= base_url('assets/dist/css/renja.css') ?>" type="text/css" rel="stylesheet" />

<script src='<?= base_url('assets/plugins/select2/select2.full.min.js');?>'></script>
<script src="<?=base_url('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js');?>" type="text/javascript"></script>
<script src="<?=base_url();?>assets/plugins/uploadfile/fileinput.min.js" type="text/javascript"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>


<style media="screen">
   .tableFixHead    { overflow-y: auto; height: 76%; }
   .tableFixHead th { position: sticky; top: -1px; }
   .ketInfo {color: white; font-weight: bold;background: #74BEDA; padding: 3px 5px}
   .ketDanger {color: white; font-weight: bold;background: red; padding: 3px 5px}
   .ketSuccess {color: white; font-weight: bold;background: green; padding: 3px 5px}
   .ketWarning {color: white; font-weight: bold;background: yellow; padding: 3px 5px}
   table  { border-collapse: collapse; width: 100%; }
   th     { background:#3C6DB0; z-index: 9; color: #FFFFFF}
   td     { padding: 3px 7px!important; }
   .selected {
      background-color:rgb(230, 141, 40)!important;
      color: #FFF!important;
   }

   label {padding-top:7px}
   .select2-container .select2-selection { height: 34px; border-color: #D2D6DE}
   .select2-container--default .select2-selection--single .select2-selection__arrow { margin-top: 3px;}
   .datepicker table tr th { background: #F4F4F4; color: #3C6DB0}
   th.ui-datepicker-week-end, td.ui-datepicker-week-end {display: none;}
</style>

<div class="row">
   <div class="col-md-12">
      <div class="box box-widget">
         <div class="box-body">
            <div class="col-sm-12" style="padding:5px 0px">
               <div class="col-sm-6"></div>

               <!-- Search and Add Izin -->
               <div class="col-sm-6" style="padding-bottom:3px">
                  <div class="row">
                     <div class="col-md-12">
                        <div class="input-group">
                           <input id="search" class="form-control" placeholder="Pencarian..." value="" type="text">
                           <span class="input-group-btn">
                              <button class="btn btn-primary btn-flat pull-right" style="margin-left: 5px; margin-right:0px" data-toggle="modal" data-target="#input">
                                 <i class="fa fa-plus-circle text-white" style="padding:3px 0px"></i>&nbsp; Rekam
                              </button>
                           </span>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- Search and Add Izin End -->
            </div>
         </div>
         <!-- /.box-body -->
      </div>
      <!-- /.box -->
   </div>
</div>

<div class="row">
   <div class="col-md-12">
      <div class="nav-tabs-custom">
         <ul class="nav nav-tabs">
            <li class="active"><a href="#tab_1" data-toggle="tab"><i class="fa fa-user"></i>
            Izin Saya</a></li>
            <li><a href="#tab_2" data-toggle="tab"><i class="fa fa-users"></i>
            Izin Bawahan</a></li>
         </ul>
         <div class="tab-content">
            <a class="btn btn-app bg-blue">
               <i class="fa fa-list"></i> Semua
            </a>
            <a class="btn btn-app">
               <span class="badge bg-red">0</span><i class="fa fa-hourglass-start"></i> Proses
            </a>
            <div class="tab-pane active" id="tab_1">
               <div class="tableFixHead">
                  <table id="iGrid" class="table table-hover table-bordered">
                     <thead>
                        <tr>
                           <th class="text-center" width="100px">Tanggal</th>
                           <th class="text-center">Izin</th>
                           <th class="text-center">Alasan</th>
                           <th class="text-center" width="100px">Izin Mulai</th>
                           <th class="text-center" width="100px">Izin Selesai</th>
                           <th class="text-center" width="75px">Lama</th>
                           <th class="text-center" width="30px">A</th>
                           <th class="text-center" width="30px">S</th>
                           <th class="text-center" width="30px">T</th>
                           <th class="text-center" width="100px">Status</th>
                        </tr>
                     </thead>
                     <tbody id="data_">
                        <?php
                        if ($data_izin['dat']) {
                           foreach ($data_izin['dat'] as $row) { ?>
                              <tr style="margin:0px;">
                                 <td style=" width:100px" data-toggle="collapse" data-target=".<?= $row['id'] ?>">
                                    <div class="text-center"><?= date("d-m-Y", strtotime($row['tanggalsurat'])) ?></div>
                                 </td>
                                 <td style="" data-toggle="collapse" data-target=".<?= $row['id'] ?>">
                                    <div><?= $row['status'] ?></div>
                                 </td>
                                 <td style="" data-toggle="collapse" data-target=".<?= $row['id'] ?>">
                                    <div><?= $row['keterangan'] ?></div>
                                 </td>
                                 <td style=" width:100px;" data-toggle="collapse" data-target=".<?= $row['id'] ?>">
                                    <div class="text-center"><?= date("d-m-Y", strtotime($row['tanggalmulai'])) ?></div>
                                 </td>
                                 <td style=" width:100px;" data-toggle="collapse" data-target=".<?= $row['id'] ?>">
                                    <div class="text-center"><?= date("d-m-Y", strtotime($row['tanggalselesai'])) ?></div>
                                 </td>
                                 <td style=" width:100px;" data-toggle="collapse" data-target=".<?= $row['id'] ?>">
                                    <div class="text-center"><b><?= $row['jml'] ?></b> Hari</div>
                                 </td>
                                 <td class="bg-green-active"></td>
                                 <td class=<?php if($row['setuju_status']=='1') echo "bg-green-active"; elseif($row['setuju_status']=='2') echo "bg-red-active"; else echo "bg-yellow-active";  ?>></td>
                                 <td class=<?php if($row['setuju_status']=='1'){if($row['approval']=='1') echo "bg-green-active"; elseif($row['approval']=='2') echo "bg-red-active"; else echo "bg-yellow-active";}  ?>></td>
                                 <td style=""><?php if($row['approval']=='1') echo "<span class='label label-success'>Diterima</span>"; elseif($row['approval']=='2' or $row['setuju_status']=='2') echo "<span class='label label-danger'>Ditolak</span>"; else echo "<span class='label label-warning'>Proses</span>";  ?></td>
                              </tr>

                              <tr style="background:#ECF0F5;color:#34495E;border:0px; margin:0px">
                                 <td class="hiddenRow" colspan="10" style="margin:0px!important; padding:0px!important">
                                    <div class="small info <?= $row['id'] ?> collapse" aria-expanded="false" style="height: 0px;">
                                       <div class="row" style="margin:10px;" >
                                          <div class="box box-widget" style="margin-bottom:0px">
                                             <div class="box-body">
                                                <div class="col-sm-12" style="padding:0px">
                                                   <div class="col-sm-6">
                                                      <table style="font-size: 13px">
                                                         <tr>
                                                            <td class="text-right">Persetujuan Status (<b> S </b>) &nbsp;</td><td>:</td><td><?php if($row['setuju_status']=='1') echo "<span class='label label-success'>Diterima</span>"; elseif($row['setuju_status']=='2') echo "<span class='label label-danger'>Ditolak</span>"; else echo "<span class='label label-warning'>Proses</span>";  ?></td>
                                                         </tr>
                                                         <tr>
                                                            <td class="text-right">Persetujuan Pada &nbsp;</td><td>:</td><td> <?php if($row['setuju_waktu']==null) echo "-"; else echo date("d-m-Y", strtotime($row['setuju_waktu'])); ?> </td>
                                                         </tr>
                                                         <tr>
                                                            <td class="text-right">Persetujuan Oleh &nbsp;</td><td>:</td><td><?= $row['namapenandatangan'] ?> </td>
                                                         </tr>
                                                         <tr>
                                                            <td class="text-right"></td><td></td><td>  <?= $row['penandatangan'] ?></td>
                                                         </tr>
                                                         <tr>
                                                            <td class="text-right"></td><td></td><td> <?= $row['jabatan'] ?></td>
                                                         </tr>

                                                      </table>
                                                   </div>
                                                   <div class="col-sm-4">
                                                      <table style="font-size: 13px">
                                                         <tr>
                                                            <td class="text-right">Penetapan Status (<b> T </b>) &nbsp;</td><td>:</td><td> <?php if($row['approval']=='1') echo "<span class='label label-success'>Diterima</span>"; elseif($row['approval']=='2') echo "<span class='label label-danger'>Ditolak</span>"; elseif($row['approval']=='3' and $row['setuju_status']=='1') echo "<span class='label label-warning'>Proses</span>"; else echo "-";  ?> </td>
                                                         </tr>
                                                         <tr>
                                                            <td class="text-right">Penetapan Pada &nbsp;</td><td>:</td><td> <?php if($row['tanggalapproval']==null) echo "-"; else date("d-m-Y", strtotime($row['tanggalapproval'])) ?> </td>
                                                         </tr>
                                                         <tr>
                                                            <td class="text-right">Dokumen &nbsp;</td><td>:</td><td> <?= ($row['dokumen'] == '' ? '' : "<a href=".site_url()."/files/upload_milea/".$row['nip']."/".$this->fc->ustgl($row['tanggalsurat'])."_".$row['dokumen']." class='btn btn-social btn-dropbox'><i class='fa fa-cloud-download'></i>".$row['dokumen']."</a>") ?> </td>
                                                         </tr>
                                                         <tr>
                                                            <td class="text-right" style="vertical-align: top">Catatan &nbsp;</td><td  style="vertical-align: top">:</td><td> <?= $row['catatan'] ?> </td>
                                                         </tr>

                                                      </table>
                                                   </div>
                                                </div>
                                             </div>

                                          </div>
                                       </div>
                                    </div>
                                 </td>
                              </tr>

                              <?php
                           }
                        } else { ?>
                           <tr><td colspan="10" class="text-danger"> Data tidak ada ...</td></tr>
                        <?php } ?>
                     </tbody>
                  </table>

                  <?php $this->load->view('absensi/v_absen_manual_rekam'); ?>

               </div>
            </div>
            <!-- /.tab-pane -->
            <div class="tab-pane" id="tab_2">
               The European languages are members of the same family. Their separate existence is a myth.
               For science, music, sport, etc, Europe uses the same vocabulary. The languages only differ
               in their grammar, their pronunciation and their most common words. Everyone realizes why a
               new common language would be desirable: one could refuse to pay expensive translators. To
               achieve this, it would be necessary to have uniform grammar, pronunciation and more common
               words. If several languages coalesce, the grammar of the resulting language is more simple
               and regular than that of the individual languages.
            </div>
            <!-- /.tab-pane -->
         </div>
         <!-- /.tab-content -->
      </div>
      <!-- nav-tabs-custom -->
   </div>
</div>


<script type="text/javascript">
   $('.collapse').on('show.bs.collapse', function () {
      $('.collapse.in').collapse('hide');
   });

   var $rows = $('#data_ tr');
   $('#search').keyup(function() {
      var val = $.trim($(this).val()).replace(/ +/g, ' ').toLowerCase();

      $rows.show().filter(function() {
         var text = $(this).text().replace(/\s+/g, ' ').toLowerCase();
         return !~text.indexOf(val);
      }).hide();
   });

   $('.tglStart,.tglEnd').datepicker({
      format: "dd-mm-yyyy",
      language: "id",
      orientation: "bottom auto",
      autoclose: true,
      todayHighlight: true
   }).on('changeDate', function() {
      var idnip = $('#idnip').val();
      sta   = $('#start').val();
      end   = $('#end').val();
      window.location.href = "<?php echo site_url('milea/manual') ?>" +'/'+ idnip +'/'+ sta +'/'+ end;
   });


   $(document).ready(function() {
      $.fn.modal.Constructor.prototype.enforceFocus = function() {};
      $('.select_tandatangan').select2({
         placeholder: "Pilih Penandatangan"
      });
      $('.select_status').select2({
         placeholder: "Pilih Status"
      });
   });

   function hapus(id,nip){
      window.location.href = '<?= site_url('milea/del') ?>' +'/'+ id +'/'+ nip;
   }

</script>
