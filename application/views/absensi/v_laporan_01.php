<script src="<?=base_url('assets/dist/js/excellentexport.js'); ?>" type="text/javascript"></script>

<style media="screen">
   .container {overflow-y: auto;padding: 0px;padding-top: 0px;width: 100%}
   .j1 {font-weight: bold; text-align: center; margin: 3px}
   .j2 {font-weight: bold; text-align: center; margin: 3px}
   .j3 {text-align: center; margin: 3px}
   tr {font-size: 10pt}
   th { text-align: center; }
   .th_{text-align:center;vertical-align: middle !important;border-style:solid; border-width:1px;overflow:hidden; word-break:normal; padding: 0px!important}
   .merah{background: #E54535}

</style>
<?php
   $judul1 = "DIREKTORAT JENDERAL ANGGARAN";
   $judul2 = "LAPORAN KEHADIRAN PEGAWAI";
   $diff   = abs(strtotime($end)-strtotime($start));
   $jData  = round($diff / 86400) + 1;
   $tgA    = strtotime($start);
   $tgE    = strtotime($end);
?>


<div class="row" style="margin-bottom:0px; padding-bottom:0px">
   <div class="col-md-12">
      <div class="box box-widget">

         <div class="box-header">
            <h4 class="j1"><?= $judul1; ?></h4>
            <h5 class="j2"><?= $judul2; ?></h5>
            <h5 class="j3">Periode : <?php echo $this->fc->idtgl($start,'hr'); ?> - <?php echo $this->fc->idtgl($end,'hr'); ?></h5>
            <div style="float: left">
               <table style="margin-top: 10px; margin-bottom: 5px">
                  <tr class="text-bold"><td width="100px">Unit Eselon II</td> <td>:&nbsp; </td><td><?= $pil['es2']['namaorganisasi']; ?></td></tr>
                  <tr class="text-bold"><td width="100px">Unit Eselon III</td><td>:&nbsp; </td><td><?= $pil['es3']['namaorganisasi']; ?></td></tr>
                  <tr class="text-bold"><td width="100px">Unit Eselon IV</td> <td>:&nbsp; </td><td><?= $pil['es4']['namaorganisasi']; ?></td></tr>
               </table>
            </div>
            <div style="width:100px; float: right; margin-top: 40px">
               <?php $nmFile = 'L01 '.$judul2.', '.$this->fc->idtgl($start).' sd '.$this->fc->idtgl($end);?>
               <span class="pull-right">
                  <a download="<?php echo $nmFile.'.xls'?>" href="#" onclick="return ExcellentExport.excel(this, 'cetak', 'Lap 01');"><b>XLS</b></a> &nbsp;&nbsp;
               </span>
            </div>
            <div class="container">

               <table id="iGrid" class="table table-hover table-bordered table-striped" style="width: 100%;table-layout:fixed;">
                  <colgroup>
                     <col width="21px"><col width="210px">
                     <?php for ($i=0; $i < $jData; $i++) {
                     ?>
                        <col width="35px">
                     <?php
                     } ?>
                  </colgroup>
                  <thead>
                     <tr style="color: #fff; background-color: #605CA8;">
                        <th class="th_" rowspan="2" style="border-left: solid 1px #605CA8; border-top: solid 1px #605CA8"> NO </th>
                        <th class="th_" rowspan="2" style="border-top: solid 1px #605CA8"> PEGAWAI </th>
                        <th style="padding:5px; border-bottom:solid 0px; border-top: solid 1px #605CA8" class="th_" colspan="<?= $jData?>"> TANGGAL </th>
                     </tr>
                     <tr style="color: #fff; background-color: #605CA8;">
                        <?php for ($i=$tgA; $i <= $tgE; $i+=86400) {
                        ?>
                           <th class="th_"><?php echo  date("d", $i) ?></th>
                        <?php
                        } ?>
                     </tr>
                  </thead>

                  <tbody>
                     <?php $no=0; foreach($peg as $row) { $no++ ?>
                        <tr class="" style="border-bottom: solid red 1px">
                           <td style="padding: 0px; border: solid #D4D7DB 1px; padding-right: 5px" class="text-right"><?= $no; ?></td>
                           <td style="padding: 0px; border: solid #D4D7DB 1px; padding-left: 5px"><?= $row['nip'] ?><br><?= strtoupper( $row['nama'] ) ?></td>

                           <?php foreach($row['isi'] as $nil) { ?>
                              <td style="padding: 0px; border: solid #D4D7DB 1px; text-align: center" class="text-right <?php if ($nil['krj']=='0' && $nil['status']=='') echo 'merah' ?>">
                                 <b><?php echo $nil['status'] ?> </b>
                                 <br><?php if($nil['krj']!='0' && $nil['iduser'] !='' && date("H:i:s",strtotime($nil['tgldatang']))!='00:00:00' || $nil['status']=='HL' ) echo date("H:i",strtotime($nil['tgldatang'])); ?><br><?php if($nil['krj']!='0' && $nil['iduser'] !='' && date("H:i:s",strtotime($nil['tgldatang']))!='00:00:00' || $nil['status']=='HL') echo date("H:i",strtotime($nil['tglpulang'])); ?>
                              </td>
                           <?php } ?>

                        </tr>
                  <?php } ?>
                  </tbody>
               </table>

            </div>
         </div>
      </div>

   </div> 
</div>


<div class="" style="display: none">
   <table id="cetak">
      <?php $vCol = ($tgE/86400-$tgA/86400) + 4 ; ?>
      <tr><td align="center" colspan="<?php echo $vCol ?>"><b><?= $judul1; ?> </b></td></tr>
      <tr><td align="center" colspan="<?php echo $vCol ?>"><b><?= $judul2; ?> </b></td></tr>
      <tr><td align="center" colspan="<?php echo $vCol ?>">Periode : <?php echo $this->fc->idtgl($start,'hr'); ?> - <?php echo $this->fc->idtgl($end,'hr'); ?></td></tr>
      <tr> <td colspan="<?php echo $vCol ?>"><b>Unit Eselon 2 : <?= $pil['es2']['namaorganisasi']; ?></b></td></tr>
      <tr> <td colspan="<?php echo $vCol ?>"><b>Unit Eselon 3 : <?= $pil['es3']['namaorganisasi']; ?></b></td></tr>
      <tr> <td colspan="<?php echo $vCol ?>"><b>Unit Eselon 4 : <?= $pil['es4']['namaorganisasi']; ?></b></td></tr>

      <tr>
         <td style="vertical-align: middle" align="center" rowspan="2"> <b> NO </b></td>
         <td style="vertical-align: middle" align="center" rowspan="2"> <b> NIP </b></td>
         <td style="vertical-align: middle" align="center" rowspan="2"> <b> NAMA </b></td>
         <td align="center" colspan="<?= $jData?>"> <b> TANGGAL </b></td>
      </tr>
      <tr>
         <?php for ($i=$tgA; $i <= $tgE; $i+=86400) {
         ?>
            <td align="center"><b><?php echo  date("d", $i) ?></b></td>
         <?php
         } ?>
      </tr>

      <?php $no=0; foreach($peg as $row) { $no++ ?>
         <tr>
            <td valign="top"><?= $no; ?></td>
            <td valign="top"><?= "&nbsp;".$row['nip'] ?></td>
            <td valign="top"><?= strtoupper( $row['nama'] ) ?></td>

            <?php foreach($row['isi'] as $nil) { ?>
               <td valign="top" align="center"> 
                  <b><?php if( $nil['status']=='TMH' ) echo 'HN'; else  echo $nil['status'] ?> </b><br> <?php if($nil['krj']!='0' && date("H:i:s",strtotime($nil['tgldatang']))!='00:00:00' || $nil['status']=='HL' ) echo date("H:i",strtotime($nil['tgldatang'])); ?><br><?php if($nil['krj']!='0' && date("H:i:s",strtotime($nil['tgldatang']))!='00:00:00' || $nil['status']=='HL') echo date("H:i",strtotime($nil['tglpulang'])); ?>
               </td>
            <?php } ?>

         </tr>
      <?php } ?>
   </table>
</div>
