<link href="<?=base_url('assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css');?>" rel="stylesheet" type="text/css" />
<script src="<?=base_url('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js');?>" type="text/javascript"></script>


<div class="row">
    <div class="col-md-9">

        <div class="box box-widget">
            <div class="box-header with-border">
                <h3 class="box-title pull-left" style="margin-top:5px; margin-bottom:5px"><?= $this->fc->idtgl(date('d-m-Y'),'hr')  ?></h3>&nbsp;<span class="small text-info"></span>
                <div class="span5 col-md-4 pull-right" id="sandbox-container" style="border:0px; padding:0px; height:25px">
                    <div class="input-daterange input-group" id="datepicker">
                        <!-- <form action="" method="post" role="form"> -->
                           <input type="text" class="input-sm form-control" value="<?php echo $start ?>" name="start" id="start" style="padding-top:0px; padding-bottom:0px;  height:25px">
                           <span class="input-group-addon" style="border:0px;"> s.d. </span>
                           <input type="text" class="input-sm form-control" value="<?php echo $end ?>" name="end" id="end" style="padding-top:0px; padding-bottom:0px; height:25px">
                        <!-- </form> -->
                    </div>
                </div>

            </div>
            <!-- /.box-header -->
            <div class="box-body with-border">
                <div class="box-body table-responsive no-padding">
                  <table class="table table-striped">
                      <tr style="background: #447AB2; color: #fff;">
                      <th class="text-center" hidden-xs hidden-sm style="width:10px">No</th>
                      <th class="text-center" hidden-xs hidden-sm>Hari</th>
                      <th class="text-center">Tanggal</th>
                      <th class="text-center">Datang</th>
                      <th class="text-center">Pulang</th>
                      <th class="text-center">Status</th>
                    </tr>


                    <?php $no=1; foreach($absensi as $absen){ ?>
                    <tr>
                      <td class="text-center" hidden-xs hidden-sm><?php echo $no; ?></td>
                      <td class="text-center" hidden-xs hidden-sm><?php echo $this->fc->idtgl($absen['tgl'],'mig'); ?></td>
                      <td class="text-center"><?php echo $this->fc->idtgl($absen['tgl']); ?></td>
                      <td class="text-center"><?php echo substr($absen['tgldatang'],11,5); ?></td>
                      <td class="text-center"><?php echo substr($absen['tglpulang'],11,5); ?></td>
                      <td class="text-center"><?php echo $absen['status']; ?></td>
                     <!--  <td>
                          <?php if (substr($absen['tglpulang'],11,2)==12): ?>
                              <?php echo '' ?></td>
                          <?php else: ?>
                              <?php echo substr($absen['keluar'],11,2)+12; echo substr($absen['keluar'],13,3) ?></td>
                          <?php endif; ?> -->
                      <!-- <td>
                          <?php echo $absen['deskripsi']; ?></td> -->
                    </tr>
                  <?php  $no++; }?>

                  </table>
              </div>
            </div>
            <div class="box-footer with-border">
               <p class="text-muted text-small">
                   <a href="https://satudja.kemenkeu.go.id/presensi?q=<?= $idws['link'] ?>">
                       https://satudja.kemenkeu.go.id/presensi?q=<?= $idws['link'] ?>
                   </a>
               </p>

                <p class="small text-muted pull-left" style="margin-bottom:0px">
                    Keterangan : Update data berdasarkan Database Kehadiran.
                </p>
            </div>
        </div>

    </div>


    <div class="col-md-3">
        <!-- Profile Image -->
        <div class="box box-widget">
            <div class="box-body with-border">
                <br>
                <?php
                    $foto_profile="files/profiles/_noprofile.png";
                    if (file_exists("files/profiles/".$this->session->userdata('nip').".gif")) {$foto_profile =  "files/profiles/".$this->session->userdata('nip').".gif";}
                ?>
                <img src="<?php echo site_url($foto_profile); ?>" class="profile-user-img img-responsive img-circle img-bordered-sm" alt="User Image" />
                <h3 class="profile-username text-center"><?php echo $this->session->userdata('nmuser') ?></h3>
                <p class="text-muted text-center"><?php echo $this->session->userdata('jabatan') ?></p>
                <hr>
                    <?php
                        $QRcode="files/QR/_noQR.png";
                        if (file_exists("files/QR/".$idws['user']."_presensi.png")) {$QRcode =  "files/QR/".$idws['user']."_presensi.png";}
                    ?>
                    <img class="profile-user-img img-responsive" src="<?php echo site_url($QRcode); ?>"><br>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
   $('#sandbox-container .input-daterange').datepicker({
      format: "dd-mm-yyyy",
      language: "id",
      orientation: "bottom auto",
      autoclose: true,
      todayHighlight: true
   }).on('changeDate', function() {
         var srt = document.getElementById("start").value;
         var end = document.getElementById("end").value;
         window.location.href = "<?php echo site_url('absensi?q=wu2kS&start=') ?>"+ srt +"&end="+ end;
      });

</script>
