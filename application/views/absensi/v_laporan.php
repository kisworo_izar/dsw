<link href="<?=base_url('assets/plugins/select2/select2.css');?>" rel="stylesheet" type="text/css" />
<script src="<?=base_url('assets/plugins/select2/select2.full.min.js'); ?>" type="text/javascript"></script>
<link href="<?=base_url('assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css');?>" rel="stylesheet" type="text/css" />
<script src="<?=base_url('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js');?>" type="text/javascript"></script>

<div class="row" style="margin-bottom:0px; padding-bottom:0px">
   <div class="col-md-12">
      <div class="box box-widget">

         <div class="box-header" style="padding-bottom:5px">
            <div class="row">
               <div class="col-sm-6" style="padding-bottom:3px">
                  <div class="form-group" style="margin-bottom:0px">
                     <select class="org form-control" id="es2" onchange="ChangeEs2( this.value )">
                        <?php foreach ($pil['es2'] as $row) {
                        $sel = ''; if ($row['kodeorganisasi'] == $pil['pil2']) $sel = 'selected';
                        echo '<option value="'. $row['kodeorganisasi'] .'" '. $sel .'>'. $row['namaorganisasi'] .'</option>';
                        } ?>
                     </select>
                  </div>
               </div>
               <div class="col-sm-6" style="padding-bottom:3px">
                  <div class="form-group" style="margin-bottom:0px">
                     <select class="org form-control" id="es4" onchange="ChangeEs4( this.value )">
                        <?php foreach ($pil['es4'] as $row) {
                        $sel = ''; if ($row['kodeorganisasi'] == $pil['pil4']) $sel = 'selected';
                        echo '<option value="'. $row['kodeorganisasi'] .'" '. $sel .'>'. $row['namaorganisasi'] .'</option>';
                        } ?>
                     </select>
                  </div>
               </div>
            </div>

            <div class="row" style="padding-top:5px">
               <div class="col-sm-6" style="padding-bottom:3px">
                  <div class="form-group" style="margin-bottom:10px">
                     <select class="org form-control" id="es3" onchange="ChangeEs3( this.value )">
                        <?php foreach ($pil['es3'] as $row) {
                        $sel = ''; if ($row['kodeorganisasi'] == $pil['pil3']) $sel = 'selected';
                        echo '<option value="'. $row['kodeorganisasi'] .'" '. $sel .'>'. $row['namaorganisasi'] .'</option>';
                        } ?>
                     </select>
                  </div>
               </div>
               <div class="col-sm-6" style="padding-bottom:3px">
                  <div class="row">
                     <div class="col-md-5">
                        <div class="form-group input-group" style="margin: 0px">
      		                <input type="text" class="form-control tglStart text-center" id="start" value="<?= $pil['start'] ?>" />
      							 <label class="input-group-btn" for="start">
      		                    <span class="btn btn-default btn-flat">
      		                        <i class="fa fa-calendar text-purple" style="padding-top: 3px;padding-bottom: 3px;"></i>
      		                    </span>
      		                </label>
      		            </div>
                     </div>
                     <div class="col-md-2">
                        <div class="text-center" style="padding-top:7px">
                           <span class="form-group" style="border:0px;"> <b>s.d.</b> </span>
                        </div>
                     </div>
                     <div class="col-md-5">
                        <div class="form-group input-group" style="margin: 0px">
      		                <input type="text" class="form-control tglEnd text-center" id="end" value="<?= $pil['end'] ?>" />
      							 <label class="input-group-btn" for="end">
      		                    <span class="btn btn-default btn-flat">
      		                        <i class="fa fa-calendar text-purple" style="padding-top: 3px;padding-bottom: 3px;"></i>
      		                    </span>
      		                </label>
      		            </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>

         <div style="background :#4A4A49; border-radius:0px; border-left: 7px solid #57B3CF;padding: 5px">
            <h4 style="margin:3px;color:#F9F9FA">Presensi</h4>
         </div>
         <div class="box-body">
            <div class="row">
               <div class="col-md-6">
                  <a target="_blank"  style="cursor:pointer" rel="noopener noreferrer" onclick="lhk()">01 Laporan Lengkap Handkey</a>
               </div>
               <div class="col-md-6">
                  <a target="_blank"  style="cursor:pointer" rel="noopener noreferrer" onclick="lkt()">02 Laporan Ketertiban</a>
               </div>
               <div class="col-md-6">
                  <a target="_blank"  style="cursor:pointer" rel="noopener noreferrer" onclick="lpg()">03 Laporan Akumulatif Pelanggaran</a>
               </div>
               <div class="col-md-6">
                  <a target="_blank"  style="cursor:pointer" rel="noopener noreferrer" onclick="lmk()">04 Laporan Uang Makan</a>
               </div>
               <div class="col-md-6">
                  <a target="_blank"  style="cursor:pointer" rel="noopener noreferrer" onclick="lmb()">05 Laporan Lembur</a>
               </div>
            </div>

         </div>

      </div>
   </div>
</div>



<script type="text/javascript">

  $(document).ready(function() {
    $(".org").select2({
      minimumResultsForSearch:5
    });
  });

</script>

<script type="text/javascript">
   function ChangeEs2( nil ){
      window.location.href = '<?= site_url('milea/laporan') ?>' +'/'+nil;

   }

   function ChangeEs3( nil ){
      var es2 = $('#es2').val();
      window.location.href = '<?= site_url('milea/laporan') ?>' +'/'+ es2 +'/'+ nil;

   }

   function ChangeEs4( nil ){
      var es2 = $('#es2').val();
      var es3 = $('#es3').val();
      window.location.href = '<?= site_url('milea/laporan') ?>' +'/'+ es2 +'/'+ es3 + '/'+nil;
   }

   function lhk(){
      var es2 = $('#es2').val();
      es3 = $('#es3').val();
      es4 = $('#es4').val();
      sta = $('#start').val();
      end = $('#end').val();
      window.location.href = '<?= site_url('milea/lap_01') ?>' +'/'+ es2 +'/'+ es3 + '/'+ es4 + '/'+sta+ '/'+end;

   }

   function lkt(){
      var es2 = $('#es2').val();
      es3 = $('#es3').val();
      es4 = $('#es4').val();
      sta = $('#start').val();
      end = $('#end').val();
      window.location.href = '<?= site_url('milea/lap_02') ?>' +'/'+ es2 +'/'+ es3 + '/'+ es4 + '/'+sta+ '/'+end;
   }

   function lpg(){
      var es2 = $('#es2').val();
      es3 = $('#es3').val();
      es4 = $('#es4').val();
      sta = $('#start').val();
      end = $('#end').val();
      window.location.href = '<?= site_url('milea/lap_03') ?>' +'/'+ es2 +'/'+ es3 + '/'+ es4 + '/'+sta+ '/'+end;
   }

   function lmk(){
      var es2 = $('#es2').val();
      es3 = $('#es3').val();
      es4 = $('#es4').val();
      sta = $('#start').val();
      end = $('#end').val();
      window.location.href = '<?= site_url('milea/lap_04') ?>' +'/'+ es2 +'/'+ es3 + '/'+ es4 + '/'+sta+ '/'+end;
   }

   function lmb(){
      var es2 = $('#es2').val();
      es3 = $('#es3').val();
      es4 = $('#es4').val();
      sta = $('#start').val();
      end = $('#end').val();
      window.location.href = '<?= site_url('milea/lap_05') ?>' +'/'+ es2 +'/'+ es3 + '/'+ es4 + '/'+sta+ '/'+end;
   }

   $('.tglStart,.tglEnd').datepicker({
    format: "dd-mm-yyyy",
    language: "id",
    orientation: "bottom auto",
    autoclose: true,
    todayHighlight: true
  })
   .on('changeDate', function() {
        var es2 = $('#es2').val();
            es3 = $('#es3').val();
            es4 = $('#es4').val();
            sta = $('#start').val();
            end = $('#end').val();
        window.location.href = "<?php echo site_url('milea/laporan') ?>" +'/'+ es2 +'/'+ es3 +'/'+ es4 +"/"+ sta+"/"+ end;
      });
</script>
