<!-- Select2 -->
<link rel="stylesheet" href="<?=base_url();?>assets/plugins/select2/dist/css/select2.min.css">
<link href="<?=base_url('assets/dist/css/AdminLTE.css');?>" rel="stylesheet" type="text/css" />
<link href="<?=base_url('assets/dist/css/skins/skin-red.css');?>" rel="stylesheet" type="text/css" />
<!-- Sweetalert Css -->
<link href="<?=base_url();?>assets/plugins/sweetalert/sweetalert.css" rel="stylesheet" />
<!-- Morris chart -->
<link rel="stylesheet" href="<?=base_url();?>assets/plugins/morris.js/morris.css">
<!-- Google Font -->
<link rel="stylesheet"
href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

<div class="row">
    <div class="col-lg-12">
        <div class="box box-solid">
            <div class="box-body">
                <div class="col-lg-12">
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-institution"></i>
                            </div>
                            <select class="form-control input-lg select2" multiple="multiple" data-placeholder="Pilih Unit"
                            style="width: 100%;" id="unit">
                            <?php foreach ($units as $unit) { ?>

                                <option value="<?= $unit->kdso ?>"><?= $unit->nmso ?></option><?php } ?>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-8">
        <div class="box box-solid">
            <div class="box-header with-border">
                <i class="fa fa-calendar-times-o"></i>
                <h3 class="box-title">Total Pelanggaran Jam Kerja</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                    
                </div>
            </div>
            <div class="box-body chart-responsive">
                <canvas id="jam_pelanggaran"></canvas>
            </div>
        </div>

        <div class="box box-solid">
            <div class="box-header with-border">
                <i class="fa fa-user-times"></i>
                <h3 class="box-title">Jumlah Pegawai Melanggar Jam Kerja</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                    
                </div>
            </div>
            <div class="box-body chart-responsive">
                <canvas id="jumlah_pegawai_melanggar" height="150"></canvas>
            </div>
        </div>
    </div>

    <div class="col-lg-4 col-sm-6">
        <div class="box box-solid bg-green-gradient">
            <div class="box-header">
                <i class="fa fa-child"></i>
                <h3 class="box-title">Pegawai Dengan Pelanggaran Jam Kerja Nol</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn bg-green btn-sm" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>
            <div class="box-body" id="pelanggaran_pegawai_nol_box">
                <div class="text-center">
                    <input type="text" class="knob" value="0" data-skin="tron" data-thickness="0.2" data-fgColor="#fff" data-width="300" data-height="300" readonly="readonly" id="pelanggaran_pegawai_nol">
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-4 col-sm-6">
        <div class="box box-solid">
            <div class="box-header with-border">
                <i class="fa fa-bullhorn"></i>
                <h3 class="box-title">Izin Pemberitahuan</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                    
                </div>
            </div>
            <div class="box-body chart-responsive" id="pelanggaran_izin_status_box">
                <canvas id="pelanggaran_izin_status" height="400" width="400"></canvas>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-12">
        <div class="box box-solid" id="pelanggaran_box">
            <div class="box-header">
                <div class="col-sm-6">
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-sort-amount-desc"></i>
                            </div>
                            <select class="form-control select2" style="width: 100%;" id="pelanggaran_sorting">
                                <option value="1">Poin Pelanggaran Terbesar</option>
                                <option value="2">Poin Pelanggaran Terkecil</option>
                                <option value="3">Belum Izin Pemberitahuan Terbanyak</option>
                                <option value="4">Belum Izin Pemberitahuan Tersedikit</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-search"></i>
                            </div>
                            <input type="text" class="form-control pull-right" id="pelanggaran_search">
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-body table-responsive no-padding">
                <div class="overlay" hidden>
                    <i class="fa fa-refresh fa-spin text-purple"></i>
                </div>

                <table class="table table-hover">
                    <thead>
                        <tr class="bg-purple">
                            <th>Pegawai</th>
                            <th>Pelanggaran</th>
                            <th>Izin Pemberitahuan</th>
                        </tr>
                        <tr data-toggle="modal" data-target="#pelanggaran_timeline" id="pelanggaran_row" data-pelanggaran="" hidden>
                            <td data-row="nama">Andre Novelando</td>
                            <td>
                                <div class="progress-group">
                                    <span class="progress-text" data-row="poin_total">250</span>
                                    <span class="progress-number"><?=number_format($poin_max)?></span>
                                    <div class="progress progress-sm active">
                                        <div class="progress-bar progress-bar-striped" style="width: 80%" data-row="poin_total_bar"></div>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <span class="badge bg-aqua"><i class="fa fa-warning "></i>
                                    <b data-row="belum">0</b>
                                </span>
                                <span class="badge bg-yellow"><i class="fa fa-refresh"></i>
                                    <b data-row="proses">0</b>
                                </span>
                                <span class="badge bg-green"><i class="fa fa-check"></i>
                                    <b data-row="sudah">0</b>
                                </span>
                                <span class="badge bg-red"><i class="fa fa-ban"></i>
                                    <b data-row="lewat">0</b>
                                </span>
                            </td>
                        </tr>
                    </thead>
                    <tbody id="pelanggaran_body">

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="pelanggaran_timeline">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Default Modal</h4>
            </div>
            <div class="modal-body" style="background-color: #ecf0f5">
                <li class="time-label" id="tl_time" hidden>
                    <span class="bg-navy" data-tl="tanggal">
                        10 Feb. 2014
                    </span>
                </li>

                <li id="tl_item" hidden>
                    <i class="" data-tl="icon"></i>

                    <div class="timeline-item">
                        <h3 class="timeline-header">
                            <span class="label" data-tl="status">TL1</span>
                            <span class="badge pull-right" data-tl="poin"><i class="fa fa-legal"></i>
                                2000
                            </span>
                        </h3>
                        <div class="timeline-body" data-tl="text">
                        </div>
                    </div>

                </li>
                <ul class="timeline" id="tl">

                </ul>
            </div>
        </div>
    </div>
</div>

<!-- Select2 -->
<script src="<?=base_url()?>assets/plugins/select2/dist/js/select2.full.min.js"></script>
<!-- sweetalert -->
<script type="text/javascript" src="<?=base_url();?>assets/plugins/sweetalert/sweetalert.min.js"></script>
<!-- jQuery Knob Chart -->
<script src="<?=base_url();?>assets/plugins/jquery-knob/dist/jquery.knob.min.js"></script>
<!-- jQuery LoadingOverlay -->
<script src="<?=base_url();?>assets/plugins/gasparesganga-jquery-loading-overlay/loadingoverlay.min.js"></script>
<script src="<?=base_url();?>assets/plugins/chart.js/Chart.min.js"></script>

<script type="text/javascript">
    $(function(){
        'use strict';

        /*Initialize Select2 Elements*/
        $('.select2').select2();

        /* jQueryKnob */
        $(".knob").knob({
            'format' : function (value) {
                return value + '%';
            },
            draw: function () {

                /*"tron" case*/
                if (this.$.data('skin') == 'tron') {

                    var a = this.angle(this.cv)  
                    , sa = this.startAngle         
                    , sat = this.startAngle         
                    , ea                            
                    , eat = sat + a                 
                    , r = true;

                    this.g.lineWidth = this.lineWidth;

                    this.o.cursor
                    && (sat = eat - 0.3)
                    && (eat = eat + 0.3);

                    if (this.o.displayPrevious) {
                        ea = this.startAngle + this.angle(this.value);
                        this.o.cursor
                        && (sa = ea - 0.3)
                        && (ea = ea + 0.3);
                        this.g.beginPath();
                        this.g.strokeStyle = this.previousColor;
                        this.g.arc(this.xy, this.xy, this.radius - this.lineWidth, sa, ea, false);
                        this.g.stroke();
                    }

                    this.g.beginPath();
                    this.g.strokeStyle = r ? this.o.fgColor : this.fgColor;
                    this.g.arc(this.xy, this.xy, this.radius - this.lineWidth, sat, eat, false);
                    this.g.stroke();

                    this.g.lineWidth = 2;
                    this.g.beginPath();
                    this.g.strokeStyle = this.o.fgColor;
                    this.g.arc(this.xy, this.xy, this.radius - this.lineWidth + 1 + this.lineWidth * 2 / 3, 0, 2 * Math.PI, false);
                    this.g.stroke();

                    return false;
                }
            }
        });

        /* DEFAULT FILTERED UNIT */
        var defaultunit = ["<?= $units[0]->kdso ?>"];
        console.log(defaultunit);
        $('#unit').select2('val', [defaultunit], true);
        $('#unit').trigger({
            type: 'select2:select',
            params: {
                data: {
                    id: defaultunit
                }
            }
        });

        $('#pelanggaran_sorting').select2('val', "1", true);
        $('#pelanggaran_sorting').trigger({
            type: 'change',
            params: {
                data: {
                    id: "1"
                }
            }
        });

    });

</script>

<script type="text/javascript">
    /* SELECT2 FILTER UNIT */
    $('#unit').on('change', function (e) {
        jam_pelanggaran();
        jumlah_pegawai_melanggar();
        pelanggaran_pegawai_nol();
        pelanggaran_izin_status();
        pelanggaran_history();
    })

    $("#pelanggaran_search").on("keyup", function() {
        var value = $(this).val().toLowerCase();
        $("#pelanggaran_body tr").filter(function() {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
    });

    /* SELECT2 URUTAN DAFTAR PELANGGARAN */
    $('#pelanggaran_sorting').on('select2:select', function (e) {
        pelanggaran_history();
    })


    /* RIWAYAT PELANGGARAN PEGAWAI */
    $('#pelanggaran_timeline').on('show.bs.modal', function (event) {
        var pelanggaran = $(event.relatedTarget).data('pelanggaran');
        $('#pelanggaran_timeline h4').text(pelanggaran['nama'] + ' - Riwayat Pelanggaran');
        $('#tl').empty();
        $.each(pelanggaran['pelanggaran'], function(index, data){
            var icon, bg, label, text;
            switch(data['izin']){
                case 'belum':
                icon = 'fa fa-warning';
                bg = 'bg-aqua';
                label = 'label-info';
                text = 'Izin belum diajukan';
                break;

                case 'proses':
                icon = 'fa fa-refresh';
                bg = 'bg-yellow';
                label = 'label-warning';
                text = 'Izin sedang diproses';
                break;

                case 'sudah':
                icon = 'fa fa-check';
                bg = 'bg-green';
                label = 'label-success';
                text = 'Izin sudah ditetapkan oleh SDM';
                break;

                case 'lewat':
                icon = 'fa fa-ban';
                bg = 'bg-red';
                label = 'label-danger';
                text = 'Izin tidak bisa diajukan karena sudah melewati batas waktu';
                break;
            }

            var tl_time = $('#tl_time').clone(),
            tl_item = $('#tl_item').clone();

            tl_time.removeAttr('id');
            tl_item.removeAttr('id');
            tl_time.show();
            tl_item.show();

            tl_time.find('[data-tl="tanggal"]').text(toDateID(data['tanggal']));
            tl_time.appendTo('#tl');

            tl_item.find('[data-tl="icon"]').addClass(icon + ' ' + bg);
            tl_item.find('[data-tl="status"]').text(data['status']);
            tl_item.find('[data-tl="status"]').addClass(label);
            tl_item.find('[data-tl="text"]').text(text);
            tl_item.find('[data-tl="poin"]').addClass(bg);
            tl_item.find('[data-tl="poin"]').text(data['poin']);
            tl_item.appendTo('#tl');
        })

        $('#tl').append('<li><i class="fa fa-hand-stop-o bg-black" data-tl="icon"></i></li>');

    })    
</script>

<script type="text/javascript">
    function loading_start(element){
        $(element).LoadingOverlay("show", {
            image       : "",
            progress    : true,
            progressFixedPosition : 'top',
            progressColor: '#605ca8',
        });

        var count     = 0;
        var interval  = setInterval(function(){
            if (count >= 95) {
                clearInterval(interval);
                return;
            }
            count += 5;
            $(element).LoadingOverlay("progress", count);
        }, 300);

        return interval;
    }    

    function loading_end(element){
        $(element).LoadingOverlay("progress", 100);
        $(element).LoadingOverlay('hide', true);
    }

    function toDateID(date){
        var hari = ['Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jum\'at', 'Sabtu'],
        bulan = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
        date = new Date(date);

        return hari[date.getDay()] + ', ' + date.getDate() + ' ' + bulan[date.getMonth()] + ' ' + (date.getYear() < 1000 ? date.getYear() + 1900 : date.getYear());
    }

    function dynamicColors(opacity) {
        return "rgb(" + Math.floor(Math.random() * 255) + "," + Math.floor(Math.random() * 255) + "," + Math.floor(Math.random() * 255) + "," + opacity +")";
    }
</script>

<script type="text/javascript">
    var pelanggaran_history_req = null;
    var pelanggaran_history_interval = null;
    function pelanggaran_history()
    {
        pelanggaran_history_req = $.ajax({
            type: "POST",
            url: '<?= base_url('absensi/pelanggaranHistory');?>',
            data: {
                so: ($('#unit').val()) ? $('#unit').val() : ["<?= $units[0]->kdso ?>"],
                sort: $('#pelanggaran_sorting').val()
            },
            dataType: 'json',
            beforeSend: function(){
                if (pelanggaran_history_req != null) {
                    pelanggaran_history_req.abort();
                    clearInterval(pelanggaran_history_interval);
                }

                pelanggaran_history_interval = loading_start('#pelanggaran_box');
            },
            success: function(rows) {
                $('#pelanggaran_body').empty();
                $.each(rows, function (key, data) {
                    var row = $('#pelanggaran_row').clone();
                    row.show();
                    row.removeAttr('id');
                    row.attr('data-pelanggaran', JSON.stringify(data))
                    row.find('[data-row="nama"]').text(data['nama']);
                    var poin_total = data['poin_total'],
                    poin_max = parseInt('<?=$poin_max?>'),
                    poin_total_bar = '';

                    row.find('[data-row="poin_total"]').text(poin_total.toLocaleString());
                    if (poin_total <= 1/3 * poin_max) {
                        poin_total_bar = 'progress-bar-success';
                    } else if (poin_total <= 2/3 * poin_max) {
                        poin_total_bar = 'progress-bar-warning';
                    } else {
                        poin_total_bar = 'progress-bar-danger';
                    }
                    row.find('[data-row="poin_total_bar"]').addClass(poin_total_bar);
                    row.find('[data-row="poin_total_bar"]').css('width', poin_total/poin_max * 100 + '%');
                    row.find('[data-row="belum"]').text(data['izin_belum'])
                    row.find('[data-row="proses"]').text(data['izin_proses'])
                    row.find('[data-row="sudah"]').text(data['izin_sudah'])
                    row.find('[data-row="lewat"]').text(data['izin_lewat'])

                    row.appendTo('#pelanggaran_body');
                })
                loading_end('#pelanggaran_box');
                return false;
            }
        });

    }
</script>

<script>
    var jumlah_pegawai_melanggar_chart = null,
    jumlah_pegawai_melanggar_req = null,
    jumlah_pegawai_melanggar_interval = null;

    function jumlah_pegawai_melanggar()
    {
        jumlah_pegawai_melanggar_req = $.ajax({
            type: "POST",
            url: '<?= base_url('absensi/pelanggaranBulanan');?>',
            data: {
                so: ($('#unit').val()) ? $('#unit').val() : ["<?= $units[0]->kdso ?>"]
            },
            dataType: 'json',
            beforeSend: function(){
                if (jumlah_pegawai_melanggar_req != null) {
                    jumlah_pegawai_melanggar_req.abort();
                    clearInterval(jumlah_pegawai_melanggar_interval);
                }

                jumlah_pegawai_melanggar_interval = loading_start('#jumlah_pegawai_melanggar');
            },
            success: function(rows) {
                var datasets = [];
                var colors = ['#3c8dbc', '#00c0ef', '#00a65a', '#f39c12', '#f56954', '#001F3F', '#39CCCC', '#605ca8', '#ff851b', '#D81B60', '#111111'];
                var i = 0;
                $.each(rows, function(key, value){
                    datasets.push({
                        'label': key,
                        'data': value,
                        'fill': false,
                        'borderColor': (i < colors.length) ? colors[i] : colors[Math.floor(Math.random() * colors.length)],
                    });
                    i++;
                })

                if (jumlah_pegawai_melanggar_chart != null) {
                    jumlah_pegawai_melanggar_chart.destroy();
                }

                var bulan = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];
                jumlah_pegawai_melanggar_chart = new Chart($("#jumlah_pegawai_melanggar"),{
                    "type":"line",
                    "data":{
                        "labels": bulan.slice(0, new Date().getMonth()+1),
                        "datasets": datasets,
                    },
                    "options":{
                        "legend": {
                            "position": "bottom",
                        }
                    }
                });

                loading_end('#jumlah_pegawai_melanggar');
            }
        });
    }

    
</script>

<script>
    var jam_pelanggaran_chart = null,
    jam_pelanggaran_req = null,
    jam_pelanggaran_interval = null;

    function jam_pelanggaran()
    {
        jam_pelanggaran_req = $.ajax({
            type: "POST",
            url: '<?= base_url('absensi/pelanggaranJam');?>',
            data: {
                so: ($('#unit').val()) ? $('#unit').val() : ["<?= $units[0]->kdso ?>"]
            },
            dataType: 'json',
            beforeSend: function(){
                if (jam_pelanggaran_req != null) {
                    jam_pelanggaran_req.abort();
                    clearInterval(jam_pelanggaran_interval);
                }

                jam_pelanggaran_interval = loading_start('#jam_pelanggaran');
            },
            success: function(rows) {
                var labels = [],
                datas = [],
                backgroundColor = [];
                var colors = ['#3c8dbc', '#00c0ef', '#00a65a', '#f39c12', '#f56954', '#001F3F', '#39CCCC', '#605ca8', '#ff851b', '#D81B60', '#111111'];

                var i = 0;
                $.each(rows, function(key, value){
                    labels.push(key);
                    datas.push(value/60);
                    backgroundColor.push((i < colors.length) ? colors[i] : colors[Math.floor(Math.random() * colors.length)]);
                    i++;
                })

                if (jam_pelanggaran_chart != null) {
                    jam_pelanggaran_chart.destroy();
                }

                jam_pelanggaran_chart = new Chart($('#jam_pelanggaran'),{
                    "type":"horizontalBar",
                    "data":{
                        "labels": labels,
                        "datasets":[
                        {
                            "label":"Pelanggaran",
                            "data": datas,
                            "fill":false,
                            "backgroundColor": backgroundColor,
                            "borderWidth":1
                        }
                        ]
                    },
                    "options":{
                        "scales":{
                            "xAxes":[{
                                "ticks":{
                                    "beginAtZero":true
                                }
                            }]
                        },
                        "legend": {
                            "display": false,
                        },
                        "tooltips": {
                            "callbacks": {
                                "label": function(tooltipItem, data) {
                                    var label = data.datasets[tooltipItem.datasetIndex].label || '';

                                    if (label) {
                                        label += ': ';
                                    }

                                    var jam = Math.floor(tooltipItem.value);
                                    var menit = tooltipItem.value - jam;

                                    label += jam.toLocaleString() + ' Jam ' + Math.round(menit * 60) + ' Menit';

                                    return label;
                                }
                            }
                        }
                    }
                });

                loading_end('#jam_pelanggaran');
            }
        })
    }

    
</script>

<script>
    var pelanggaran_izin_status_chart = null,
    pelanggaran_izin_status_req = null,
    pelanggaran_izin_status_interval = null;

    function pelanggaran_izin_status()
    {
        pelanggaran_izin_status_req = $.ajax({
            type: "POST",
            url: '<?= base_url('absensi/pelanggaranIzinStatus');?>',
            data: {
                so: ($('#unit').val()) ? $('#unit').val() : ["<?= $units[0]->kdso ?>"]
            },
            dataType: 'json',
            beforeSend: function(){
                if (pelanggaran_izin_status_req != null) {
                    pelanggaran_izin_status_req.abort();
                    clearInterval(pelanggaran_izin_status_interval);
                }

                pelanggaran_izin_status_interval = loading_start('#pelanggaran_izin_status_box');
            },
            success: function(data) {
                if (pelanggaran_izin_status_chart != null) {
                    pelanggaran_izin_status_chart.destroy();
                }

                pelanggaran_izin_status_chart = new Chart($('#pelanggaran_izin_status'),{
                    "type":"polarArea",
                    "data":{
                        "labels":["Belum","Proses","Sudah","Lewat Waktu"],
                        "datasets":[{
                            "label":"Izin Pemberitahuan",
                            "data":[data['belum'], data['proses'], data['sudah'], data['lewat']],
                            "backgroundColor":["#00c0ef", "#f39c12", "#00a65a", "#f56954"]
                        }]
                    },
                    "options":{
                        "responsive": true,
                        "maintainAspectRatio": false,
                    }
                });

                loading_end('#pelanggaran_izin_status_box');
            }
        })
    }
    
</script>

<script type="text/javascript">
    var pelanggaran_pegawai_nol_req = null,
    pelanggaran_pegawai_nol_interval = null;

    function pelanggaran_pegawai_nol()
    {
        pelanggaran_pegawai_nol_req = $.ajax({
            type: "POST",
            url: '<?= base_url('absensi/pelanggaranPegawaiNol');?>',
            data: {
                so: ($('#unit').val()) ? $('#unit').val() : ["<?= $units[0]->kdso ?>"]
            },
            dataType: 'json',
            beforeSend: function(){
                if (pelanggaran_pegawai_nol_req != null) {
                    pelanggaran_pegawai_nol_req.abort();
                    clearInterval(pelanggaran_pegawai_nol_interval);
                }

                pelanggaran_pegawai_nol_interval = loading_start('#pelanggaran_pegawai_nol_box');
            },
            success: function(data) {
                $("#pelanggaran_pegawai_nol").val(data);
                $("#pelanggaran_pegawai_nol").trigger('change');

                loading_end('#pelanggaran_pegawai_nol_box');
            }
        })
    }
</script>