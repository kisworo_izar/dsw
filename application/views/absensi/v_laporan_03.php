<!-- cek git -->
<script src="<?=base_url('assets/dist/js/excellentexport.js'); ?>" type="text/javascript"></script>

<style media="screen">
   .container {overflow-y: auto;padding: 0px;padding-top: 0px;width: 100%}
   .j1 {font-weight: bold; text-align: center; margin: 3px}
   .j2 {font-weight: bold; text-align: center; margin: 3px}
   .j3 {text-align: center; margin: 3px}
   tr {font-size: 10pt}
   th { text-align: center; }
   .th_{text-align:center;vertical-align: middle !important;border-style:solid; border-width:1px;overflow:hidden; word-break:normal; padding: 0px!important}
   .merah{background: red}

</style>
<?php
   $judul1 = "DIREKTORAT JENDERAL ANGGARAN";
   $judul2 = "LAPORAN PELANGGARAN AKUMULATIF";
?>


<div class="row" style="margin-bottom:0px; padding-bottom:0px">
   <div class="col-md-12">
      <div class="box box-widget">

         <div class="box-header">
            <h4 class="j1"><?= $judul1; ?></h4>
            <h5 class="j2"><?= $judul2; ?></h5>
            <h5 class="j3">Periode : <?php echo $this->fc->idtgl($start,'hr'); ?> - <?php echo $this->fc->idtgl($end,'hr'); ?></h5>
            <div style="float: left">
               <table style="margin-top: 10px; margin-bottom: 5px">
                  <tr class="text-bold"><td width="100px">Unit Eselon II</td> <td>:&nbsp; </td><td><?= $pil['es2']['namaorganisasi']; ?></td></tr>
                  <tr class="text-bold"><td width="100px">Unit Eselon III</td><td>:&nbsp; </td><td><?= $pil['es3']['namaorganisasi']; ?></td></tr>
                  <tr class="text-bold"><td width="100px">Unit Eselon IV</td> <td>:&nbsp; </td><td><?= $pil['es4']['namaorganisasi']; ?></td></tr>
               </table>
            </div>
            <div style="width:100px; float: right; margin-top: 40px">
               <?php $nmFile = 'L03 '.$judul2.', '.$this->fc->idtgl($start).' sd '.$this->fc->idtgl($end);?>
               <span class="pull-right">
                  <a download="<?php echo $nmFile.'.xls'?>" href="#" onclick="return ExcellentExport.excel(this, 'cetak', 'Lap 03');"><b>XLS</b></a> &nbsp;&nbsp;
               </span>
            </div>

            <div class="container">

               <table id="iGrid" class="table table-hover table-bordered table-striped" style="width: 100%;table-layout:fixed;">
                  <colgroup>
                     <col width="21px"><col width="190"><col width="250"><col width="65">
                  </colgroup>
                  <thead>
                     <tr style="color: #fff; background-color: #605CA8;">
                           <th class="th_">NO</th>
                           <th class="th_">PEGAWAI</th>
                           <th class="th_">UNIT ORGANISASI <br>ESELON II, ESELON III, ESELON IV </th>
                           <th class="th_">TOTAL</th>
                     </tr>
                  </thead>

                  <tbody>
                     <?php $no=0; foreach($plg as $row) { $no++ ?>
                        <tr class="" style="border-bottom: solid red 1px">
                           <td style="padding: 5px; border: solid #D4D7DB 1px;" class="text-right"><?= $no; ?></td>
                           <td style="padding: 5px; border: solid #D4D7DB 1px;"><?= $row['nip'] ?> <br> <?= strtoupper($row['nama']) ?> <br> <?= strtok($row['jbt'], '[') ?></td>
                           <td style="padding: 5px; border: solid #D4D7DB 1px;"><?= $row['es2'] ?> <br> <?= $row['es3'] ?> <br> <?= $row['es4'] ?></td>
                           <td style="padding: 5px; border: solid #D4D7DB 1px; vertical-align: middle; text-align: right"><b><?= floor($row['plg']/60) ?></b> Jam <br> <b><?= $row['plg'] % 60 ?></b> Menit </td>
                        </tr>
                  <?php } ?>
                  </tbody>
               </table>

            </div>
         </div>
      </div>

   </div>
</div>

<div class="" style="display: none">
   <table id="cetak">
      <?php $vCol = 8; ?>
      <tr><td align="center" colspan="<?php echo $vCol ?>"><b><?= $judul1; ?> </b></td></tr>
      <tr><td align="center" colspan="<?php echo $vCol ?>"><b><?= $judul2; ?> </b></td></tr>
      <tr><td align="center" colspan="<?php echo $vCol ?>">Periode : <?php echo $this->fc->idtgl($start,'hr'); ?> - <?php echo $this->fc->idtgl($end,'hr'); ?></td></tr>
      <tr> <td colspan="<?php echo $vCol ?>"><b>Unit Eselon 2 : <?= $pil['es2']['namaorganisasi']; ?></b></td></tr>
      <tr> <td colspan="<?php echo $vCol ?>"><b>Unit Eselon 3 : <?= $pil['es3']['namaorganisasi']; ?></b></td></tr>
      <tr> <td colspan="<?php echo $vCol ?>"><b>Unit Eselon 4 : <?= $pil['es4']['namaorganisasi']; ?></b></td></tr>

      <tr>
         <td style="vertical-align: middle" align="center"><b>NO </b></td>
         <td style="vertical-align: middle" align="center"><b>NIP </b></td>
         <td style="vertical-align: middle" align="center"><b>NAMA </b></td>
         <td style="vertical-align: middle" align="center"><b>JABATAN </b></td>
         <td style="vertical-align: middle" align="center"><b>ESELON II </b></td>
         <td style="vertical-align: middle" align="center"><b>ESELON III </b></td>
         <td style="vertical-align: middle" align="center"><b>ESELON IV </b></td>
         <td style="vertical-align: middle" align="center"><b>TOTAL </b></td>
      </tr>

      <?php $no=0; foreach($plg as $row) { $no++ ?>
         <tr>
            <td style="vertical-align: top"><?= $no; ?></td>
            <td style="vertical-align: top"><?= "&nbsp;".$row['nip'] ?>
            <td style="vertical-align: top"><?= strtoupper( $row['nama'] ) ?></td>
            <td style="vertical-align: top"><?= strtok($row['jbt'], '[') ?></td>
            <td style="vertical-align: top"><?= $row['es2'] ?>
            <td style="vertical-align: top"><?= $row['es3'] ?>
            <td style="vertical-align: top"><?= $row['es4'] ?>
            <td style="vertical-align: top; text-align: right"><b><?= floor($row['plg']/60) ?></b> Jam  <b><?= $row['plg'] % 60 ?></b> Menit </td>
         </tr>
      <?php } ?>
   </table>
</div>
