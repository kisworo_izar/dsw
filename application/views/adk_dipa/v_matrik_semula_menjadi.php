<style media="screen">
   th {padding: 5px!important}
   td {padding: 5px!important}
   td {padding: 5px!important}
   .inSatker {text-indent: -47px; margin-left:48px;}
   .tg-rhys{background-color:#317bbe;color:#FFF;text-align:center;vertical-align: middle !important;font-size:14px;font-weight:bold; border-style:solid; border-width:1px;overflow:hidden; word-break:normal;border-color:black;}
   .container {overflow-y: auto;padding: 0px;padding-top: 0px;width: 100%}
</style>

<script type="text/javascript" src="<?=base_url('assets/dist/js/excellentexport.js'); ?>"></script>

<div class="row">
   <div class="col-md-12">
      <div class="box box-widget">
         <div class="box-body">

         <?php if ($this->thang <= '2020') { ?>
            <div class="container">
               <h3>Pejabat Perbendaharaan</h3>
               <table id="iHal4b" class="table table-hover table-bordered" style="border-spacing: 1; width: 100%;">
                  <thead>
                     <tr style="font-size:12px; color: #fff; background-color: #3568B6;">
                        <th class="text-center tg-rhys"> URAIAN </th>
                        <th class="text-center tg-rhys" style="border-bottom:0px">NAMA PEJABAT (SEMULA)</th>
                        <th class="text-center tg-rhys" style="border-bottom:0px">NAMA PEJABAT (MENJADI)</th>
                     </tr>
                  </thead>

                  <tbody>
                     <?php if (count($tabel['kpa']) > 0) {
                        foreach ($tabel['kpa'] as $row) {
                           $class = ($row['mula'] != $row['jadi']) ? ' text-danger' : '';
                           echo '
                              <tr style="font-size:12px; padding:0px">
                                 <td class="text-left">'. $row['kode'] .'</td>
                                 <td class="text-left'. $class .'"><b>'. $row['mula'] .'</b></td>
                                 <td class="text-left'. $class .'"><b>'. $row['jadi'] .'</b></td>
                              </tr>';
                        }
                     } else {
                        echo '<tr><td colspan="3" class="text-center text-muted" style="font-size:12px; font-style: italic">Tidak ada data yang berbeda ... </td></tr>';
                     } ?>
                  </tbody>
               </table>
            </div>
         <?php } ?>

         <?php if ($tabel['kpa']) { ?>
            <div class="container">
               <h3>Satker BLU</h3>
               <table id="iHal4b" class="table table-hover table-bordered" style="border-spacing: 1; width: 100%;">
                  <thead>
                     <tr style="font-size:12px; color: #fff; background-color: #3568B6;">
                        <th class="text-center tg-rhys"> URAIAN </th>
                        <th class="text-center tg-rhys" style="border-bottom:0px">NILAI (SEMULA)</th>
                        <th class="text-center tg-rhys" style="border-bottom:0px">NILAI (MENJADI)</th>
                     </tr>
                  </thead>

                  <tbody>
                     <?php if (count($tabel['kpa']) > 0) {
                        foreach ($tabel['kpa'] as $row) {
                           $class = ($row['mula'] != $row['jadi']) ? ' text-danger' : '';
                           echo '
                              <tr style="font-size:12px; padding:0px">
                                 <td class="text-left">'. $row['kode'] .'</td>
                                 <td class="text-right'. $class .'"><b>'. $row['mula'] .'</b></td>
                                 <td class="text-right'. $class .'"><b>'. $row['jadi'] .'</b></td>
                              </tr>';
                        }
                     } else {
                        echo '<tr><td colspan="3" class="text-center text-muted" style="font-size:12px; font-style: italic">Tidak ada data yang berbeda ... </td></tr>';
                     } ?>
                  </tbody>
               </table>
            </div>
         <?php } ?>

            <div class="container">
               <h3 style="display:inline;">Matriks</h3>
                     <span class="pull-right" style="padding-right:5px">
                        <a download="MatriksRKA.xls" href="#" onclick="return ExcellentExport.excel(this, 'iMatrik', 'MatriksRKA');">Export ke <b>Excel</b></a>
                     </span>

               <table id="iMatrik" class="table table-hover table-bordered" style="border-spacing: 1; width: 100%; margin-top: 10px">
                  <thead>
                     <tr style="font-size:12px; color: #fff; background-color: #3568B6;">
                        <th class="text-center tg-rhys" rowspan="2" width="80px"> KODE </th>
                        <th class="text-center tg-rhys" rowspan="2" width=""> URAIAN </th>
                        <th class="text-center tg-rhys" colspan="3" style="border-bottom:0px">SEMULA<br>Revisi ke - <?= $revid['idmula'] ?></th>
                        <th class="text-center tg-rhys" colspan="3" style="border-bottom:0px">MENJADI<br>Tiket No.  <?= $revid['idjadi'] ?></th>
                        <th class="text-center tg-rhys" colspan="3" style="border-bottom:0px">SELISIH</th>
                        <th class="text-center tg-rhys" rowspan="2" width=""> ! </th>
                     </tr>
                     <tr style="font-size:12px; color: #fff; background-color: #3568B6;">
                        <th class="text-center tg-rhys" width=" 2%"> Vol </th>
                        <th class="text-center tg-rhys" width="110px"> Jumlah </th>
                        <th class="text-center tg-rhys" width=" 1%"> @ </th>
                        <th class="text-center tg-rhys" width=" 2%"> Vol </th>
                        <th class="text-center tg-rhys" width="110px"> Jumlah </th>
                        <th class="text-center tg-rhys" width=" 1%"> @ </th>
                        <th class="text-center tg-rhys" width=" 2%"> Vol </th>
                        <th class="text-center tg-rhys" width="110px"> Jml </th>
                        <th class="text-center tg-rhys" width=" 1%"> @ </th>
                     </tr>
                  </thead>

                  <tbody>
                     <?php
                     if (count($tabel['matrx']) > 0) {
                        foreach ($tabel['matrx'] as $row) {
                           $kode = $row['kode']; $uraian = $row['uraian'];
                           if ($row['level'] == 'satker')   { $style = 'font-size:14px; font-weight: bold; padding:0px; text-align: center'; }
                           if ($row['level'] == 'program')  { $style = 'font-size:12px; font-weight: bold; padding:0px; text-align: center'; }
                           if ($row['level'] == 'kegiatan') { $style = 'font-size:12px; font-weight: bold; padding:0px; text-align: center'; }
                           if ($row['level'] == 'output')   { 
                              $style  = 'font-size:12px; font-weight: bold; padding:0px; text-align: center';
                              $uraian.= $row['kdpn'];
                           }
                           if ($row['level'] == 'soutput')  { $style = 'font-size:12px; padding:0px; text-align: right'; }
                           if ($row['level'] == 'komponen' or $row['level'] == 'digist') {
                              $style = 'font-size:12px; font-style: italic; padding:0px; text-align: right';
                              $kode = "<span style='padding-left: 10px'>$kode</span>"; $uraian = "<span style='padding-left: 10px'>$uraian</span>";
                           }
                           if ($row['level'] == 'jenbel')   {
                              $style = 'font-size:12px; padding:0px; text-align: right';
                              $kode = "<span style='padding-left: 20px'>$kode</span>"; $uraian = "<span style='padding-left: 20px'>$uraian</span>";
                           }
                           if ($row['level'] == 'akun')     {
                              $style = 'font-size:11px; color: grey; padding:0px; text-align: right';
                              $kode = "<span style='padding-left: 20px'>$kode</span>"; $uraian = "<span style='padding-left: 20px'>$uraian</span>";
                           }

                           $vol = ''; $jml = ''; $blok = '';
                           if ($row['vol1'] != $row['vol2'])  $vol  = 'text-danger';
                           if ($row['jml1'] != $row['jml2'])  $jml  = 'text-danger';
                           if ($row['blok1']!= $row['blok2']) $blok = 'text-danger';
                     ?>
                           <tr id="<?= $row['recid'] ?>" class="" style="<?= $style ?>">
                              <td> <?= $kode ?></td>
                              <td class="text-left"> <?= $uraian ?></td>
                              <td class="text-left  <?= $vol ?>"> <?= $row['vol1'] ?></td>
                              <td class="text-right <?= $jml ?>"> <?= $row['jml1'] ?></td>
                              <td class="text-left  <?= $blok ?>"><?= $row['blok1'] ?></td>
                              <td class="text-left  <?= $vol ?>"> <?= $row['vol2'] ?></td>
                              <td class="text-right <?= $jml ?>"> <?= $row['jml2'] ?></td>
                              <td class="text-left  <?= $blok ?>"><?= $row['blok2'] ?></td>
                              <td class="text-left  <?= $vol ?>"> <?= $row['vol3'] ?></td>
                              <td class="text-right <?= $jml ?>"> <?= $row['jml3'] ?></td>
                              <td class="text-left  <?= $blok ?>"><?= $row['blok3'] ?></td>
                              <td class="text-left"><?= $row['indikasi'] ?></td>
                           </tr>
                     <?php
                        }
                     } else {
                        echo '<tr><td colspan="12" class="text-center text-muted" style="font-size:13px; font-style: italic">Tidak ada data yang berbeda ... </td></tr>';
                     }
                     ?>
                  </tbody>
               </table>
            </div>

            <div class="container">
               <h3>DIPA Hal. III </h3>
               <table id="iHal3" class="table table-hover table-bordered" style="border-spacing: 1; width: 100%;">
                  <thead>
                     <tr style="font-size:12px; color: #fff; background-color: #3568B6;">
                        <th class="text-center tg-rhys" rowspan="2" width="80px"> KODE </th>
                        <th class="text-center tg-rhys" rowspan="2" width="80px"> URAIAN </th>
                        <th class="text-center tg-rhys" colspan="2" style="border-bottom:0px">JANUARI</th>
                        <th class="text-center tg-rhys" colspan="2" style="border-bottom:0px">FEBRUARI</th>
                        <th class="text-center tg-rhys" colspan="2" style="border-bottom:0px">MARET</th>
                        <th class="text-center tg-rhys" colspan="2" style="border-bottom:0px">APRIL</th>
                        <th class="text-center tg-rhys" colspan="2" style="border-bottom:0px">MEI</th>
                        <th class="text-center tg-rhys" colspan="2" style="border-bottom:0px">JUNI</th>
                     </tr>
                     <tr style="font-size:10px; color: #fff; background-color: #3568B6;">
                        <th class="text-center tg-rhys" style="border-bottom:0px">semula</th>
                        <th class="text-center tg-rhys" style="border-bottom:0px">menjadi</th>
                        <th class="text-center tg-rhys" style="border-bottom:0px">semula</th>
                        <th class="text-center tg-rhys" style="border-bottom:0px">menjadi</th>
                        <th class="text-center tg-rhys" style="border-bottom:0px">semula</th>
                        <th class="text-center tg-rhys" style="border-bottom:0px">menjadi</th>
                        <th class="text-center tg-rhys" style="border-bottom:0px">semula</th>
                        <th class="text-center tg-rhys" style="border-bottom:0px">menjadi</th>
                        <th class="text-center tg-rhys" style="border-bottom:0px">semula</th>
                        <th class="text-center tg-rhys" style="border-bottom:0px">menjadi</th>
                        <th class="text-center tg-rhys" style="border-bottom:0px">semula</th>
                        <th class="text-center tg-rhys" style="border-bottom:0px">menjadi</th>
                     </tr>
                  </thead>

                  <tbody>
                     <?php if (count($tabel['hal3']) > 0) {
                        foreach ($tabel['hal3'] as $row) {
                           if ($row['level'] == '1' or $row['level'] == '3') $bold = 'font-weight:bold;'; else $bold = '';
                           $sty01 = ($row['jml01'] != $row['jad01']) ? ' text-danger' : '';
                           $sty02 = ($row['jml02'] != $row['jad02']) ? ' text-danger' : '';
                           $sty03 = ($row['jml03'] != $row['jad03']) ? ' text-danger' : '';
                           $sty04 = ($row['jml04'] != $row['jad04']) ? ' text-danger' : '';
                           $sty05 = ($row['jml05'] != $row['jad05']) ? ' text-danger' : '';
                           $sty06 = ($row['jml06'] != $row['jad06']) ? ' text-danger' : '';

                           if ($row['level'] == '1' or $row['level'] == '3') 
                              echo '
                                 <tr style="font-size:10px; font-weight:bold; padding:0px">
                                    <td class="text-left" colspan="2">'. $row['kode'] .'<br>'. $row['uraian'] .'</td>';
                           else if ($row['level'] == '5') 
                              echo '
                                 <tr style="font-size:10px; font-weight:bold; padding:0px">
                                    <td class="text-left" colspan="2">Perkiraan Penerimaan</td>';
                           else if ($row['level'] == '6') 
                              echo '
                                 <tr style="font-size:10px; padding:0px">
                                    <td class="text-right">&nbsp;</td>
                                    <td class="text-left"> - PNBP ('. $row['kode'] .')</td>';
                           else
                              echo '
                                 <tr style="font-size:10px; padding:0px">
                                    <td class="text-right">'. $row['kode'] .'</td>
                                    <td class="text-left"> '. $row['uraian'] .'</td>';

                           echo '
                                 <td class="text-right'. $sty01 .'">'. number_format($row['jml01']/1000, 0, ',', '.') .'</td>
                                 <td class="text-right'. $sty01 .'">'. number_format($row['jad01']/1000, 0, ',', '.') .'</td>
                                 <td class="text-right'. $sty02 .'">'. number_format($row['jml02']/1000, 0, ',', '.') .'</td>
                                 <td class="text-right'. $sty02 .'">'. number_format($row['jad02']/1000, 0, ',', '.') .'</td>
                                 <td class="text-right'. $sty03 .'">'. number_format($row['jml03']/1000, 0, ',', '.') .'</td>
                                 <td class="text-right'. $sty03 .'">'. number_format($row['jad03']/1000, 0, ',', '.') .'</td>
                                 <td class="text-right'. $sty04 .'">'. number_format($row['jml04']/1000, 0, ',', '.') .'</td>
                                 <td class="text-right'. $sty04 .'">'. number_format($row['jad04']/1000, 0, ',', '.') .'</td>
                                 <td class="text-right'. $sty05 .'">'. number_format($row['jml05']/1000, 0, ',', '.') .'</td>
                                 <td class="text-right'. $sty05 .'">'. number_format($row['jad05']/1000, 0, ',', '.') .'</td>
                                 <td class="text-right'. $sty06 .'">'. number_format($row['jml06']/1000, 0, ',', '.') .'</td>
                                 <td class="text-right'. $sty06 .'">'. number_format($row['jad06']/1000, 0, ',', '.') .'</td>
                              </tr>';
                        }
                     } else {
                        echo '<tr><td colspan="14" class="text-center text-muted" style="font-size:12px; font-style: italic">Tidak ada data yang berbeda ... </td></tr>';
                     } ?>
                  </tbody>
               </table>

               <table id="iHal3" class="table table-hover table-bordered" style="border-spacing: 1; width: 100%;">
                  <thead>
                     <tr style="font-size:12px; color: #fff; background-color: #3568B6;">
                        <th class="text-center tg-rhys" rowspan="2" width="80px"> KODE </th>
                        <th class="text-center tg-rhys" rowspan="2" width="80px"> URAIAN </th>
                        <th class="text-center tg-rhys" colspan="2" style="border-bottom:0px">JULI</th>
                        <th class="text-center tg-rhys" colspan="2" style="border-bottom:0px">AGUSTUS</th>
                        <th class="text-center tg-rhys" colspan="2" style="border-bottom:0px">SEPTEMBER</th>
                        <th class="text-center tg-rhys" colspan="2" style="border-bottom:0px">OKTOBER</th>
                        <th class="text-center tg-rhys" colspan="2" style="border-bottom:0px">NOPEMBER</th>
                        <th class="text-center tg-rhys" colspan="2" style="border-bottom:0px">DESEMBER</th>
                     </tr>
                     <tr style="font-size:10px; color: #fff; background-color: #3568B6;">
                        <th class="text-center tg-rhys" style="border-bottom:0px">semula</th>
                        <th class="text-center tg-rhys" style="border-bottom:0px">menjadi</th>
                        <th class="text-center tg-rhys" style="border-bottom:0px">semula</th>
                        <th class="text-center tg-rhys" style="border-bottom:0px">menjadi</th>
                        <th class="text-center tg-rhys" style="border-bottom:0px">semula</th>
                        <th class="text-center tg-rhys" style="border-bottom:0px">menjadi</th>
                        <th class="text-center tg-rhys" style="border-bottom:0px">semula</th>
                        <th class="text-center tg-rhys" style="border-bottom:0px">menjadi</th>
                        <th class="text-center tg-rhys" style="border-bottom:0px">semula</th>
                        <th class="text-center tg-rhys" style="border-bottom:0px">menjadi</th>
                        <th class="text-center tg-rhys" style="border-bottom:0px">semula</th>
                        <th class="text-center tg-rhys" style="border-bottom:0px">menjadi</th>
                     </tr>
                  </thead>

                  <tbody>
                     <?php if (count($tabel['hal3']) > 0) {
                        foreach ($tabel['hal3'] as $row) {
                           if ($row['level'] == '1' or $row['level'] == '3') $bold = 'font-weight:bold;'; else $bold = '';
                           $sty07 = ($row['jml07'] != $row['jad07']) ? ' text-danger' : '';
                           $sty08 = ($row['jml08'] != $row['jad08']) ? ' text-danger' : '';
                           $sty09 = ($row['jml09'] != $row['jad09']) ? ' text-danger' : '';
                           $sty10 = ($row['jml10'] != $row['jad10']) ? ' text-danger' : '';
                           $sty11 = ($row['jml11'] != $row['jad11']) ? ' text-danger' : '';
                           $sty12 = ($row['jml12'] != $row['jad12']) ? ' text-danger' : '';

                           if ($row['level'] == '1' or $row['level'] == '3') 
                              echo '
                                 <tr style="font-size:10px; font-weight:bold; padding:0px">
                                    <td class="text-left" colspan="2">'. $row['kode'] .'<br>'. $row['uraian'] .'</td>';
                           else if ($row['level'] == '5') 
                              echo '
                                 <tr style="font-size:10px; font-weight:bold; padding:0px">
                                    <td class="text-left" colspan="2">Perkiraan Penerimaan</td>';
                           else if ($row['level'] == '6') 
                              echo '
                                 <tr style="font-size:10px; padding:0px">
                                    <td class="text-right">&nbsp;</td>
                                    <td class="text-left"> - PNBP ('. $row['kode'] .')</td>';
                           else
                              echo '
                                 <tr style="font-size:10px; padding:0px">
                                    <td class="text-right">'. $row['kode'] .'</td>
                                    <td class="text-left"> '. $row['uraian'] .'</td>';

                           echo '
                                 <td class="text-right'. $sty07 .'">'. number_format($row['jml07']/1000, 0, ',', '.') .'</td>
                                 <td class="text-right'. $sty07 .'">'. number_format($row['jad07']/1000, 0, ',', '.') .'</td>
                                 <td class="text-right'. $sty08 .'">'. number_format($row['jml08']/1000, 0, ',', '.') .'</td>
                                 <td class="text-right'. $sty08 .'">'. number_format($row['jad08']/1000, 0, ',', '.') .'</td>
                                 <td class="text-right'. $sty09 .'">'. number_format($row['jml09']/1000, 0, ',', '.') .'</td>
                                 <td class="text-right'. $sty09 .'">'. number_format($row['jad09']/1000, 0, ',', '.') .'</td>
                                 <td class="text-right'. $sty10 .'">'. number_format($row['jml10']/1000, 0, ',', '.') .'</td>
                                 <td class="text-right'. $sty10 .'">'. number_format($row['jad10']/1000, 0, ',', '.') .'</td>
                                 <td class="text-right'. $sty11 .'">'. number_format($row['jml11']/1000, 0, ',', '.') .'</td>
                                 <td class="text-right'. $sty11 .'">'. number_format($row['jad11']/1000, 0, ',', '.') .'</td>
                                 <td class="text-right'. $sty12 .'">'. number_format($row['jml12']/1000, 0, ',', '.') .'</td>
                                 <td class="text-right'. $sty12 .'">'. number_format($row['jad12']/1000, 0, ',', '.') .'</td>
                              </tr>';
                        }
                     } else {
                        echo '<tr><td colspan="14" class="text-center text-muted" style="font-size:12px; font-style: italic">Tidak ada data yang berbeda ... </td></tr>';
                     } ?>
                  </tbody>
               </table>
            </div>

            <div class="container">
               <h3>DIPA Hal. IV A</h3>
               <table id="iHal4b" class="table table-hover table-bordered" style="border-spacing: 1; width: 100%;">
                  <thead>
                     <tr style="font-size:12px; color: #fff; background-color: #3568B6;">
                        <th class="text-center tg-rhys" width="80px"> KODE </th>
                        <th class="text-center tg-rhys" colspan="2" style="border-bottom:0px">URAIAN (SEMULA)</th>
                        <th class="text-center tg-rhys" colspan="2" style="border-bottom:0px">URAIAN (MENJADI)</th>
                     </tr>
                  </thead>

                  <tbody>
                     <?php if (count($tabel['blok']) > 0) {
                        foreach ($tabel['blok'] as $row) {
                           if (strpos('-kegiatan', $row['level'])) $ln = 'style="border-bottom: 1px solid black"'; else $ln = '';
                           if (strpos('-program-kegiatan', $row['level']))  echo '
                              <tr style="font-size:12px; font-weight:bold; padding:0px">
                                 <td class="text-right">'. $row['kode'] .'</td>
                                 <td class="text-left" '. $ln .' colspan="4">'. $row['uraian'] .'</td>
                              </tr>';
                           if (strpos('-output', $row['level']))  echo '
                              <tr style="font-size:12px; font-weight:bold; padding:0px">
                                 <td class="text-right">'. $row['kode'] .'</td>
                                 <td colspan="4">'. $row['uraian'] .'</td>
                             </tr>';

                           if (strpos('-akun', $row['level'])) {
                              $cls = '';
                              if ($row['blok'] != $row['mblok'])  $cls = 'text-danger';
                              echo '
                              <tr style="font-size:12px; padding:0px">
                                 <td class="text-right '. $cls .'">'. $row['kode'] .'</td>
                                 <td class="text-left  '. $cls .'">'. $row['uraian'] .'</td>
                                 <td width="110px" class="text-right '. $cls .'">'. number_format($row['blok']/1000, 0, ',', '.') .'</td>
                                 <td> &nbsp; </td>
                                 <td width="110px" class="text-right '. $cls .'">'. number_format($row['mblok']/1000, 0, ',', '.') .'</td>
                              </tr>';
                           }

                           $cat  = ($row['uraian'] == '') ? '' : ' * '.$row['uraian'];
                           $mcat = (!isset($row['muraian']) or $row['muraian']== '') ? '' : ' * '.$row['muraian'];
                           if (strpos('-catatan', $row['level'])) {
                              $cls = '';
                              if ($row['blok'] != $row['mblok'])  $cls = 'text-danger';
                              echo '
                              <tr style="font-size:12px; padding:0px">
                                 <td class="text-right '. $cls .'"> &nbsp; </td>
                                 <td> &nbsp; '. $cat  .'</td>
                                 <td width="110px" class="text-right '. $cls .'">'. number_format($row['blok']/1000, 0, ',', '.') .'</td>
                                 <td> &nbsp; '. $mcat .'</td>
                                 <td width="110px" class="text-right '. $cls .'">'. number_format($row['mblok']/1000, 0, ',', '.') .'</td>
                              </tr>';
                           }
                        }
                     } else {
                        echo '<tr><td colspan="5" class="text-center text-muted" style="font-size:12px; font-style: italic">Tidak ada data yang berbeda ... </td></tr>';
                     } ?>
                  </tbody>
               </table>
            </div>

            <div class="container">
               <h3>DIPA Hal. IV B</h3>
               <table id="iHal4b" class="table table-hover table-bordered" style="border-spacing: 1; width: 100%;">
                  <thead>
                     <tr style="font-size:12px; color: #fff; background-color: #3568B6;">
                        <th class="text-center tg-rhys" width="80px"> KODE </th>
                        <th class="text-center tg-rhys" colspan="2" style="border-bottom:0px">URAIAN (SEMULA)</th>
                        <th class="text-center tg-rhys" colspan="2" style="border-bottom:0px">URAIAN (MENJADI)</th>
                     </tr>
                  </thead>

                  <tbody>
                     <?php if (count($tabel['catt']) > 0) {
                        foreach ($tabel['catt'] as $row) {
                           if (strpos('-kegiatan', $row['level'])) $ln = 'style="border-bottom: 1px solid black"'; else $ln = '';
                           if (strpos('-program-kegiatan', $row['level']))  echo '
                              <tr style="font-size:12px; font-weight:bold; padding:0px">
                                 <td class="text-right">'. $row['kode'] .'</td>
                                 <td class="text-left" '. $ln .' colspan="4">'. $row['uraian'] .'</td>
                              </tr>';
                           if (strpos('-output', $row['level']))  echo '
                              <tr style="font-size:12px; font-weight:bold; padding:0px">
                                 <td class="text-right">'. $row['kode'] .'</td>
                                 <td>'. $row['uraian'] .'</td>
                                 <td width="110px" class="text-right">'. number_format($row['pagu']/1000, 0, ',', '.') .'</td>
                                 <td> &nbsp; </td>
                                 <td width="110px" class="text-right">'. number_format($row['mpagu']/1000, 0, ',', '.') .'</td>
                              </tr>';
                           if (strpos('-akun', $row['level']))  echo '
                              <tr style="font-size:12px; padding:0px">
                                 <td class="text-right">'. $row['kode'] .'</td>
                                 <td class="text-left" colspan="2">'. $row['uraian'] .'</td>
                                 <td colspan="2"> &nbsp; </td>
                              </tr>';

                           $cat  = ($row['uraian'] == '') ? '' : ' * '.$row['uraian'];
                           $mcat = (!isset($row['muraian']) or $row['muraian']== '') ? '' : ' * '.$row['muraian'];
                           if (strpos('-catatan', $row['level'])) {
                              $cls = '';
                              if ($row['pagu']   != $row['mpagu'])    $cls = 'text-danger';
                              if ($row['uraian'] != $row['muraian'])  $cls = 'text-danger';
                              echo '
                              <tr style="font-size:12px; padding:0px">
                                 <td class="text-right"> &nbsp; </td>
                                 <td class="text-left'. $cls .'"> &nbsp; '. $cat  .'</td>
                                 <td width="110px" class="text-right '. $cls .'">'. number_format($row['pagu']/1000, 0, ',', '.') .'</td>
                                 <td class="text-left'. $cls .'"> &nbsp; '. $mcat .'</td>
                                 <td width="110px" class="text-right '. $cls .'">'. number_format($row['mpagu']/1000, 0, ',', '.') .'</td>
                              </tr>';
                           }
                        }
                     } else {
                        echo '<tr><td colspan="5" class="text-center text-muted" style="font-size:12px; font-style: italic">Tidak ada data yang berbeda ... </td></tr>';
                     } ?>
                  </tbody>
               </table>
            </div>

         </div>
      </div>
   </div>
</div>
