<script type="text/javascript" src="<?=base_url('assets/dist/js/excellentexport.js'); ?>"></script>

<style media="screen">
   th { text-align: center; }
   thead tr { color: #fff; background: #00acd6; }
   tbody tr .row-1 { color: #fff; background: #00acd6; }
   h5 {margin-bottom:0px}
   .level-1 { font: bold 14px/5px sans-serif; color: #00e; }
   .level-2 { font: bold 13px sans-serif; color: #000; }
   .level-3 { font: bold 13px sans-serif; color: #000; }
   .level-4 { font: italic bold 12px sans-serif; color: #000; }
   .level-4p{ font: italic bold 12px sans-serif; color: #C54D37; }
   .level-5 { font: 12px sans-serif; color: #000; }
   .level-6 { font: 12px sans-serif; color: #000; }
   .level-7 { font: italic 12px sans-serif; color: #000; }
   .level-8 { font: 11px sans-serif; color: #000; padding: 50px 30px 50px 80px;}
   .level-9 { font: 11px sans-serif; color: #7a7a7a; }
   .level-9b{ font: 11px sans-serif; color: #DC3545; }
   .level-9s{ font: 11px sans-serif; color: #28A745; }
   .header  {padding: 0px 2px 0px 15px;}
   .item    {padding: 0px 2px 0px 25px;}
   .inSatker {text-indent: -47px; margin-left:48px;}
   .tg-rhys{background-color:#317bbe;color:#FFF;text-align:center;vertical-align: middle !important;font-size:14px;font-weight:bold; border-style:solid; border-width:1px;overflow:hidden; word-break:normal;border-color:black;}
   .container {overflow-y: auto;padding: 0px;padding-top: 0px;width: 100%; height: 600px; border-top: solid 1px #F4F4F4}
   td{padding: 2.5px 5px!important}
   .tableFixHead    { overflow-y: auto; height: 600px; }
   .tableFixHead th { position: sticky; top: -1px; }
   table  { border-collapse: collapse; width: 100%; }
   th     { background:#3C6DB0; z-index: 999 }
</style>

<script type="text/javascript">
   $(document).ready(function() {
      var level = '<?= $level ?>';
      if (level == 'satker') $('#optDetl').click(); else $('#optKmpn').click();
   })
</script>

<div class="row">
   <div class="col-md-12">
      <div class="box box-widget" style="margin-bottom:0px">

         <div class="box-header with border" style="padding-bottom:0px">
            <div class="row">
               <div class="col-md-9" style="padding-top:7px">
                  <div class="input-group">
                     <label class="radio-inline" style="padding-left:0px">Tampilkan :</label>
                     <label class="radio-inline"><input type="radio" id="optSatk" class="radio" name="optradio" value="row-1">
                        Satker &nbsp;<span id="lblSatk" class="small text-muted"></span>
                     </label>
                     <label class="radio-inline"><input type="radio" id="optOutp" class="radio" name="optradio" value="row-4">
                        <?php if ($this->thang >= '2021') echo 'KRO'; else echo 'Output'; ?> &nbsp;<span id="lblOutp" class="small text-muted"></span>
                     </label>
                     <label class="radio-inline"><input type="radio" id="optSOut" class="radio" name="optradio" value="row-5">
                        <?php if ($this->thang >= '2021') echo 'RO'; else echo 'Sub Output'; ?> &nbsp;<span id="lblSOut" class="small text-muted"></span>
                     </label>
                     <label class="radio-inline"><input type="radio" id="optKmpn" class="radio" name="optradio" value="row-6">
                        Komponen &nbsp;<span id="lblKmpn" class="small text-muted"></span>
                     </label>
                     <label class="radio-inline"><input type="radio" id="optAkun" class="radio" name="optradio" value="row-8">
                        Akun &nbsp;<span id="lblAkun" class="small text-muted"></span>
                     </label>
                     <label class="radio-inline"><input type="radio" id="optDetl" class="radio" name="optradio" value="row-9">
                        Detail &nbsp;<span id="lblDetl" class="small text-muted"></span>
                     </label>

                     <span class="pull-right" style="padding-left:20px">
                        <a download="KertasKerja.xls" href="#" onclick="return ExcellentExport.excel(this, 'pvtTable', 'KertasKerja');">Export ke <b>Excel</b></a>
                     </span>
                  </div>
               </div>
               <div class="col-md-3">
                  <div class="input-group">
                     <input id="search" class="form-control" placeholder="Pencarian..." value="" type="text" autocomplete="off">
                     <span class="input-group-btn">
                        <button class="btn btn-default btn-flat"><i class="fa fa-search text-purple" style="padding-top: 3px;padding-bottom: 3px;"></i></button>
                     </span>
                  </div>
               </div>
            </div>
         </div>

         <div class="box-body">
            <div class="tableFixHead">
               <table class="table table-hover table-bordered table-condensed" id="pvtTable">
                  <?php $txtOutput = ($this->thang >= '2021') ? ' Program/ Kegiatan/ KRO/ RO/ Komponen': ' Program/ Kegiatan/ Output/ Sub Output/ Komponen' ?>
                  <thead>
                     <tr>
                        <th class="tg-rhys" width="90px">Kode</th>
                        <th class="tg-rhys" width=""><?php echo $txtOutput; ?></th>
                        <th class="tg-rhys" width="10%">Volume</th>
                        <th class="tg-rhys" width="10%">Harga Satuan</th>
                        <th class="tg-rhys" width="15%" colspan="2">Jumlah</th>
                        <th class="tg-rhys" width="">SD/CP</th>
                     </tr>
                  </thead>

                  <tbody id="data_rkakl">
                  <?php foreach ($tabel as $row) {
                     $class   = ' text-right'; if ($row['level']<=4) $class = ' text-center';
                     $volkeg  = '';  
                        if ($row['volkeg']>0 and fmod($row['volkeg'],1) == 0) $volkeg = number_format($row['volkeg'], 0, ',', '.') .' '. $row['satkeg'];
                        else $volkeg = number_format($row['volkeg'], 2, ',', '.') .' '. $row['satkeg'];
                     $hargasat = '';  if ($row['hargasat']>0) $hargasat = number_format($row['hargasat'], 0, ',', '.');
                     $uraian   = $row['uraian'];

                     if ($row['level']==4) if ($row['sdcp'] == 'PN') $row['level'] = '4p';;
                     if ($row['level']==6) $volkeg = ((int)$row['satkeg'] > 0) ? $row['satkeg'] : '';
                     if ($row['level']==8) $uraian = "<u>$uraian</u>";
                     if ($row['level']==9) {
                        if ($row['jumlah'] == 0) {
                           $uraian = '<div class="header"> > '. $row['uraian'] .'</div>';
                           $volkeg = '';
                        } else {
                           $uraian = '<div class="item"> - '. $row['uraian'] .'</div>';
                           $volkeg = number_format($row['volkeg'], 2, ',', '.') .' '. $row['satkeg'];
                        }
                        if ($row['kdblokir']=='*') $row['level'] = '9b';
                        if ($row['kdsbu']!='')     $row['level'] = '9s';
                     }
                  ?>

                     <tr class="all row-<?= $row['level'] ?>">
                        <td class="level-<?= $row['level'] . $class ?>">
                           <span style="display: none"><?=$row['kdkey'] ?>||</span>
                           <?php
                              if ($row['level']==6) echo substr($row['kode'],13,3);
                              else echo $row['kode'];
                           ?>
                        </td>
                        <td class="level-<?= $row['level'] ?> text-left"> 
                           <?php 
                              if ($row['level']==4) echo "$uraian <br>[". $row['kdib'] ."] Lokasi : ". $row['lok'];
                              else echo $uraian;
                           ?>
                        </td>
                        <td class="level-<?= $row['level'] ?> text-right">
                           <?php if (strpos('-4-4p-5-6-9', $row['level'])) echo $volkeg; ?>
                           
                        </td>
                        <td class="level-<?= $row['level'] ?> text-right"><?= $hargasat ?></td>
                        <td class="level-<?= $row['level'] ?> text-right"><?= number_format($row['jumlah'], 0, ',', '.') ?></td>
                        <td class="level-<?= $row['level'] ?> text-right">
                           <?php
                              if     ($row['level']==66)                          echo '<small>'. $row['atrib'] .'</small>';
                              elseif ($row['level']==9 and $row['kdblokir']=='*') echo '@';
                              elseif ($row['level']==9 and $row['kdsbu']!='')     echo '<small>sbm</small>';
                              else $row['atrib']
                           ?>
                        </td>
                        <td class="level-<?= $row['level'] ?> text-center"><?php if ($row['level']==8) echo '<small>'. $row['sdcp'] .'</small>'; ?></td>
                     </tr>

                  <?php } ?>
                  </tbody>
               </table>
            </div>
         </div>

      </div>
   </div>
</div>

<script type="text/javascript">
   var $rows = $('#data_rkakl tr'), cari = '';
   $('#search').keyup(function() {
      var val  = $.trim($(this).val()).replace(/ +/g, ' ').toLowerCase(),
          kode = '';

      $rows.show().filter(function() {
         var text = $(this).text().replace(/\s+/g, ' ').toLowerCase(),
             arr  = text.split('||'),
             nil  = false;

         if (~text.indexOf(val)) { nil = true;  cari = arr[0]; kode += arr[0].trim()+'||'; }
         if (! nil) { if (~text.indexOf(cari)) nil = true; }
         return ! nil;
      }).hide();

      $rows.show().filter(function() {
         var text = $(this).text().replace(/\s+/g, ' ').toLowerCase(),
             arr  = text.split('||'),
             val = arr[0].trim(),
             nil  = false;
         if (~kode.indexOf(val)) { nil = true; }
         if (! nil) { if (~text.indexOf(cari)) nil = true; }
         return ! nil;
      }).hide();
      optionLbl();
   });

   $('.radio').click(function () {
      var cls = $(this).val();
      if (cls == 'row-1') { $('.all').hide(); $('.row-1').show(); }
      if (cls == 'row-4') { $('.all').hide(); $('.row-1, .row-2, .row-3, .row-4, .row-4p').show(); }
      if (cls == 'row-5') { $('.all').hide(); $('.row-1, .row-2, .row-3, .row-4, .row-4p, .row-5').show(); }
      if (cls == 'row-6') { $('.all').show(); $('.row-7, .row-8, .row-9, .row-9b, .row-9s').hide(); }
      if (cls == 'row-8') { $('.all').show(); $('.row-9, .row-9b, .row-9s').hide(); }
      if (cls == 'row-9') { $('.all').show(); }
      optionLbl();
   })

   function optionLbl() {
      $('#lblSatk').html('('+ $('.row-1:visible').length +')');
      $('#lblOutp').html('('+($('.row-4:visible').length + $('.row-4p:visible').length) +')');
      $('#lblSOut').html('('+ $('.row-5:visible').length +')');
      $('#lblKmpn').html('('+ $('.row-6:visible').length +')');
      $('#lblAkun').html('('+ $('.row-8:visible').length +')');
      $('#lblDetl').html('('+($('.row-9:visible').length + $('.row-9b:visible').length + $('.row-9s:visible').length) +')');
   }
</script>
