<link href="<?=base_url('assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css');?>" rel="stylesheet" type="text/css" />
<script src="<?=base_url('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js');?>" type="text/javascript"></script>

<style type="text/css">
   td{padding: 3px 5px!important}
   .tg-rhys{background-color:#317bbe;color:#FFF;text-align:center;vertical-align: middle !important;font-size:14px;font-weight:bold; border-style:solid; border-width:1px;overflow:hidden; word-break:normal;border-color:black;}
   thead tr { color: #fff; background: #00acd6; }
   tbody tr .row-1 { color: #fff; background: #00acd6; }
   .container {overflow-y: auto;padding: 0px;padding-top: 0px;width: 100%;}
</style>


<div class="row">
  <div class="col-md-12">
    <div class="box box-widget" style="margin:0px">
      <div class="box-header with-border">

        <div class="span5 col-md-4 pull-right" id="sandbox-container" style="border:0px; padding:0px; height:25px">
          <div class="input-daterange input-group" id="datepicker">
            <input type="text" name="start" id="start" class="input-sm form-control text-center" value="<?= $start ?>" style="padding-top:0px; padding-bottom:0px;  height:25px">
            <span class="input-group-addon" style="border:0px;"> s.d. </span>
            <input type="text" name="end" id="end" class="input-sm form-control text-center" value="<?= $end ?>" style="padding-top:0px; padding-bottom:0px; height:25px">
          </div>
        </div>

      </div>
    </div>
  </div>
</div>


<div class="row">
  <div class="col-md-12">

    <div class="box box-widget">
      <div class="box-header with-border">
        <div style="text-align:center">
          <h4 style="margin:5px"> <b> Rekapitulasi Data Revisi Anggaran </b> </h4>
          <h4 style="margin:5px"> <b> Tahun Anggaran 2018 </b> </h4>
          <h5>Periode : <?= $this->fc->idtgl( $start, 'tglfull') ?> s.d. <?= $this->fc->idtgl( $end, 'tglfull') ?></h5>
        </div>

         <div class="container">
            <table class="table table-hover table-bordered" style="width:100%; margin-bottom:0px">
               <thead>
                 <tr style="font-size:12px; color: #fff; background-color: #3568B6;">
                     <th class="text-center tg-rhys" style="padding-top:3px; padding-bottom:3px" rowspan="2">Unit Eselon I </th>
                     <th class="text-center tg-rhys" style="padding-top:3px; padding-bottom:3px" rowspan="2">Satker</th>
                     <th class="text-center tg-rhys" style="padding-top:3px; padding-bottom:3px" colspan="3" style="border-bottom:0px ">Kewenangan</th>
                     <th class="text-center tg-rhys" style="padding-top:3px; padding-bottom:3px" rowspan="2">&sum;</th>
                     <th class="text-center tg-rhys" style="padding-top:3px; padding-bottom:3px" rowspan="2">Pagu</th>
                     <th class="text-center tg-rhys" style="padding-top:3px; padding-bottom:3px" rowspan="2">x&#772;</th>
                 </tr>
                 <tr style="font-size:12px; color: #fff; background-color: #3568B6;">
                     <th class="text-center tg-rhys" style="padding-top:3px; padding-bottom:3px;">DJA</th>
                     <th class="text-center tg-rhys" style="padding-top:3px; padding-bottom:3px;">PA</th>
                     <th class="text-center tg-rhys" style="padding-top:3px; padding-bottom:3px;">Kwl</th>
                  </tr>
              </thead>

              <tbody>
                <?php
                   $jml_satker = 0; $jml_dja = 0; $jml_pa = 0; $jml_kwl = 0; $jml_pagu = 0;
                   foreach ($rekap as $row) {
                      $jml_satker += $row['satker'];
                      $jml_dja += $row['dja'];
                      $jml_pa += $row['pa'];
                      $jml_kwl += $row['kwl'];
                      $jml_pagu += $row['jumlah'];
                ?>
                   <tr style="font-size:14px">
                     <td><?= substr($row['kode'],0,3).'.'.substr($row['kode'],3,2).'&nbsp;&nbsp;'.$row['uraian'] ?></td>
                     <td class="text-right"><?= $row['satker'] ?></td>
                     <td class="text-right"><?= $row['dja'] ?></td>
                     <td class="text-right"><?= $row['pa'] ?></td>
                     <td class="text-right"><?= $row['kwl'] ?></td>
                     <td class="text-right"><?= $row['dja']+$row['pa']+$row['kwl'] ?></td>
                     <td class="text-right"><?= number_format($row['jumlah'], 0, ',', '.') ?></td>
                     <td class="text-right"><?= round(($row['dja']+$row['pa']+$row['kwl'])/$row['satker'],2) ?></td>
                   </tr>
                <?php } ?>

                   <tr style="font-size:14px">
                      <td  class="text-center tg-rhys">Jumlah</td>
                      <td class="tg-rhys" style="text-align:right"><?php  echo number_format($jml_satker, 0, ',', '.'); ?></td>
                      <td class="tg-rhys" style="text-align:right"><?php  echo number_format($jml_dja, 0, ',', '.'); ?></td>
                      <td class="tg-rhys" style="text-align:right"><?php  echo number_format($jml_pa, 0, ',', '.'); ?></td>
                      <td class="tg-rhys" style="text-align:right"><?php  echo number_format($jml_kwl, 0, ',', '.'); ?></td>
                      <td class="tg-rhys" style="text-align:right"><?php  echo number_format($jml_dja+$jml_pa+$jml_kwl, 0, ',', '.'); ?></td>
                      <td class="tg-rhys" style="text-align:right"><?php  echo number_format($jml_pagu, 0, ',', '.'); ?></td>
                      <td class="tg-rhys" style="text-align:right"><?php  echo round(($jml_dja + $jml_pa + $jml_kwl)/$jml_satker,2)  ?></td>
                   </tr>

              </tbody>

            </table>
         </div>

        <h6><b>Catatan: </b><br></h6>
      </div>
    </div>

  </div>
</div>


<script type="text/javascript">
  $('#sandbox-container .input-daterange').datepicker({
    format: "dd-mm-yyyy",
    language: "id",
    orientation: "bottom auto",
    autoclose: true,
    todayHighlight: true
  }).on('changeDate', function() {
    var sta = $('#start').val();
    var end = $('#end').val();
    window.location.href = "<?= site_url('adk_dipa?q=28afa') ?>" +'&start='+ sta +"&end="+ end;
    });
</script>
