<link href="<?=base_url('assets/plugins/select2/select2.css');?>" rel="stylesheet" type="text/css" />
<script src="<?=base_url('assets/plugins/select2/select2.full.min.js'); ?>" type="text/javascript"></script>

<style media="screen">
   td{padding: 5px!important}
   .inSatker {text-indent: -47px; margin-left:48px;}
   .tg-rhys{background-color:#317bbe;color:#FFF;text-align:center;vertical-align: middle !important;font-size:14px;font-weight:bold; border-style:solid; border-width:1px;overflow:hidden; word-break:normal;border-color:black;}
   section {position: relative;}
   section.positioned {position: absolute;top:10px;left:10px;}
   .container {overflow-y: auto;padding: 0px;padding-top: 0px;width: 100%}
   td{padding: 5px!important}
</style>

<div id="main" class="row">
   <div class="col-md-12">
      <div class="box box-widget" style="margin-bottom:0px">
         <input type="hidden" id="aktif">

         <div class="box-header with border" style="padding-bottom:0px">
            <div class="row">
               <div class="col-md-6" style="padding-bottom:3px">
                  <select id="select2" class="org form-control" tabindex="-1" onchange="changeUnit(this.value)" style="width:100%; z-index:50 !important;">
                     <?php foreach ($refr as $row) echo '<option value="'. $row['kode'] .'" '. $row['selected'] .'>'. $row['uraian'] .'</option>'; ?>
                  </select>
               </div>
               <div class="col-md-6">
                  <form role="form" action="<?php echo site_url("adk_dipa?q=0c2d2") ?>" method="post" style="margin-bottom:0px">
                     <div class="input-group">
                        <input type="text" name="cari" class="form-control" placeholder="Pencarian..." value="<?php echo $this->session->userdata('cari') ?>" autocomplete="off">
                        <span class="input-group-btn">
                           <button type="submit" name="search" value="search" class="btn btn-info btn-flat"><i class="fa fa-search" style="height:16px;margin-top:4px"></i></button>
                        </span>
                        <span class="input-group-btn">
                           <button type="submit" name="search" value="clear" class="btn btn-info btn-flat"><i class="fa fa-times-circle" style="height:16px;margin-top:4px"></i></button>
                        </span>
                     </div>
                  </form>
               </div>
            </div>
         </div>


         <div class="box-body" style="padding-top:7px">
            <div class="container">
                     <table id="iGridPok" class="table table-hover table-bordered" style="border-spacing: 0px; width: 100%;">
                        <thead>
                           <tr>
                              <th class="tg-rhys" width="">Nama Satker</th>
                              <th class="tg-rhys" width="45" colspan="2">Revisi</th>
                              <th class="tg-rhys" width="95"> Tanggal </th>
                              <th class="tg-rhys" width="140"> Digitalstamp </th>
                              <th class="tg-rhys" width="150"> Pagu </th>
                           </tr>
                        </thead>

                        <tbody>
                           <?php
                           foreach ($data as $row) {
                              if ($row['ssb'] > '0')  $style = 'bg-danger'; else { $style = 'text-success bg-success'; $row['ssb'] = ''; }
                              if (! strpos($this->session->userdata('whrdept'), '015')) $style = '';
                           ?>
                              <tr id="<?= $row['kode'] ?>" class="satker" style="font-size:14px; padding:0px;cursor:pointer">
                                 <td class="text-left">
                                    <div class="inSatker"><?= $row['kode'] .'&nbsp;&nbsp;'. $row['uraian']?></div>
                                 </td>

                                 <?php if (! strpos($this->session->userdata('whrdept'), '015')) { ?>
                                    <td class="text-center" width="35px" colspan="2"><?= $row['revisike'] ?></td>
                                 <?php } else { ?>
                                    <td class="text-center" width="35px"><?= $row['revisike'] ?></td>
                                    <td class="text-center small <?= $style ?>" width=" 30px"><?= $row['ssb'] ?></td>
                                 <?php } ?>


                                 <td class="text-center"><?= $this->fc->idtgl($row['tgladk'], 'tgl') ?></td>
                                 <td class="text-center"><?= $row['digistamp'] ?></td>
                                 <td class="text-right"> <?= number_format($row['pagu'], 0, ',', '.') ?></td>
                              </tr>

                              <?php foreach ($row['sub'] as $val) { ?>
                                 <tr class="sub-satker sub-<?= $val['kode'] ?>" style="font-size:12px; padding:0px; display:none;">
                                    <td style="border-bottom:solid 1px #ECECEC;">
                                       <div class="pull-left" style="padding-left:50px;"><?= $val['uraian'] ?></div>
                                       <div style="padding-left:150px">
                                          <?php
                                             echo $val['link_matrik'] .' '. $val['link_pok'] .' ';
                                             if ((int)$row['ssb'] > 0) echo $val['link_ssb'];
                                          ?>
                                       </div>
                                    </td>

                                    <?php if (! strpos($this->session->userdata('whrdept'), '015')) { ?>
                                       <td style="border-bottom:solid 1px #ECECEC;" class="text-center" colspan="2"><?= $val['revisike'] ?></td>
                                    <?php } else { ?>
                                       <td style="border-bottom:solid 1px #ECECEC;" class="text-center"><?= $val['revisike'] ?></td>
                                       <td style="border-bottom:solid 1px #ECECEC;" class="text-center"><?= $val['ssb'] ?></td>
                                    <?php } ?>

                                    <td style="border-bottom:solid 1px #ECECEC;" class="text-center"><?= $this->fc->idtgl($val['tgladk'], 'tgl') ?></td>
                                    <td style="border-bottom:solid 1px #ECECEC;" class="text-center"><?= $val['digistamp'] ?> </td>
                                    <td style="border-bottom:solid 1px #ECECEC;" class="text-right"> <?= number_format($val['pagu'], 0, ',', '.') ?></td>
                                 </tr>
                              <?php }  ?>
                           <?php }  ?>
                        </tbody>
                     </table>
                     <div class="pull-right">
                        <?php echo $this->pagination->create_links(); ?>
                     </div>
            </div>
         </div>

   </div>
</div>

<script type="text/javascript">
   $('.satker').click(function() {
      var id = $(this).attr('id');
      $('.sub-satker').hide();
      if ($('#aktif').val() == id) { $('#aktif').val(''); $('.sub-'+id).hide(); }
      else { $('#aktif').val(id); $('.sub-'+id).show(); }
   });

   function changeUnit(nil) {
      window.location.href = '<?= site_url('adk_dipa?q=0c2d2&unit=') ?>' +nil;
   }

   $(document).ready(function() {
     $(".org").select2({
       minimumResultsForSearch:5
     });
   });
</script>
