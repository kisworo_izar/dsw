<style media="screen">
   h5 {margin:5px 0px}
   table {width: 100%; margin-bottom: 15px}
   th {padding: 0px 3px; background: #3568B6; color: #fff}
   td {padding: 0px 3px; border: 1px solid lightgrey}
   .container {overflow-y: auto;padding: 0px;padding-top: 0px;width: 100%}
</style>


<div class="row">
   <div class="col-md-12">
      <div class="box box-widget">

         <div class="box-body">
            <h4>Perbandingan Kode Digital Stamp</h4>
            <section class="">
               <div class="container"><?= $tabel['satk'] ?></div>
            </section><br>

            <h4>Perbandingan Output</h4>
            <section class="">
               <div class="container"><?= $tabel['outp'] ?></div>
            </section><br>

            <h4>Perbandingan Jenis Belanja</h4>
            <section class="">
               <div class="container"><?= $tabel['jenb'] ?></div>
            </section><br>
         </div>

      </div>
   </div>

</div>
