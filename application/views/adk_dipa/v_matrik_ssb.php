<style media="screen">
   td{padding: 5px!important}
   .inSatker {text-indent: -47px; margin-left:48px;}
   .tg-rhys{background-color:#317bbe;color:#FFF;text-align:center;vertical-align: middle !important;font-size:14px;font-weight:bold; border-style:solid; border-width:1px;overflow:hidden; word-break:normal;border-color:black;}
   thead tr { color: #fff; background: #00acd6; }
   tbody tr .row-1 { color: #fff; background: #00acd6; }

   .Kelompok       { font: bold 14px/5px sans-serif; color: #3568B6; }
   .Output         { font: bold 13px sans-serif; color: #000; }
   .Sub_Output     { font: italic bold 12px sans-serif; color: #000; }
   .Komponen       { font: 12px sans-serif; color: #000; }
   .Sub_Komponen   { font: 12px sans-serif; color: #000 }
   .Sub_Komponen_s { font: 12px sans-serif; color: #3568B6; text-decoration: underline;}
   .Akun           { font: 12px sans-serif; color: #000; padding-left: 10px;}
   table { display: block;height: 600px; overflow-y: auto;}
</style>

<div class="box box-widget">
   <div class="box-body">
      <div class="row">

         <div class="col-md-6">
            <h4 style="font-weight: bold; color: #3568B6; margin-top:0px">Data Hasil Validasi</h4>
            <table id="iData" class="table table-hover table-bordered" style="border-spacing: 1; width: 100%;">
               <thead>
                  <tr style="font-size:12px; color: #fff; background-color: #3568B6;">
                     <th class="text-center tg-rhys" width="100px"> Level </th>
                     <th class="text-center tg-rhys" width="10%"> Kode </th>
                     <th class="text-center tg-rhys" width=""> Uraian </th>
                  </tr>
               </thead>
               <tbody id="ssb">
                  <?php
                     $level = '';
                     foreach ($data as $row) { ?>
                        <tr class="<?= str_replace(' ', '_', $row['level']) ?>" style="padding:0px;">
                           <td class="text-left" >
                              <?php if ($level != $row['level']) { $level = $row['level']; echo $row['level']; } ?>
                           </td>
                           <td class="text-left"><?= $row['kode'] ?></td>
                           <td class="text-left"><?= $row['uraian'] ?></td>
                        </tr>
                  <?php } ?>
               </tbody>
            </table>
         </div>

         <div class="col-md-6">
            <h4 style="font-weight: bold; color: #3568B6; margin-top:0px">Tabel SSB</h4>
            <table id="iSSB" class="table table-hover table-bordered" style="border-spacing: 1; width: 100%;">
               <thead>
                  <tr style="font-size:12px; color: #fff; background-color: #3568B6;">
                     <th class="text-center tg-rhys" width="100px"> Level </th>
                     <th class="text-center tg-rhys" width="10%"> Kode </th>
                     <th class="text-center tg-rhys" width=""> Uraian </th>
                  </tr>
               </thead>
               <tbody id="ssb">
                  <?php
                     foreach ($info as $row) {
                        $class = str_replace(' ', '_', $row['level']) .' '; $style = 'padding:0px; ';
                        if ($row['level'] == 'Sub Komponen') $class .= 'Sub_Komponen_s ';
                        if ($row['level'] == 'Akun') {
                           $class .= 'akun_ssb '. str_replace('.', '_', substr($row['kdkey'],0,26));
                           $style .= 'display:none';
                     }
                  ?>
                     <tr id="<?= str_replace('.', '_', substr($row['kdkey'],0,26)) ?>" class="<?= $class ?>" style="<?= $style ?>">
                        <td class="text-left">
                           <?php if ($level != $row['level']) { $level = $row['level']; echo $row['level']; } ?>
                        </td>
                        <td class="text-left"><?= $row['kode'] ?></td>
                        <td class="text-left"><?= $row['uraian'] ?></td>
                     </tr>
                  <?php } ?>
               </tbody>
            </table>
         </div>

      </div>
   </div>
</div>

<script type="text/javascript">
   $('.Sub_Komponen').click(function() {
      var id = $(this).attr('id');
      $('.akun_ssb').hide(); $('.'+id).show();
   });
</script>
