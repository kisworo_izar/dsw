<style media="screen">
   th {padding: 5px!important}
   td {padding: 5px!important}
   .inSatker {text-indent: -47px; margin-left:48px;}
   .tg-rhys{background-color:#317bbe;color:#FFF;text-align:center;vertical-align: middle !important;font-size:14px;font-weight:bold; border-style:solid; border-width:1px;overflow:hidden; word-break:normal;border-color:black;}
   .container {overflow-y: auto;padding: 0px;padding-top: 0px;width: 100%}
   td{padding: 5px!important}
</style>

<div class="row">
   <div class="col-md-12">
      <div class="box box-widget">
         <div class="box-body">

            <div class="container">
               
               <div class="row">
                  <div class="col-md-6"><h3>Matriks</h3></div>
                  <div class="col-md-6">dalam ribuan</div>
               <table id="iMatrik" class="table table-hover table-bordered" style="border-spacing: 1; width: 100%;">
                  <thead>
                     <tr style="font-size:12px; color: #fff; background-color: #3568B6;">
                        <th class="text-center tg-rhys" rowspan="2" width="80px"> KODE </th>
                        <th class="text-center tg-rhys" rowspan="2" width=""> URAIAN </th>
                        <th class="text-center tg-rhys" colspan="3" style="border-bottom:0px">SEMULA</th>
                        <th class="text-center tg-rhys" colspan="3" style="border-bottom:0px">MENJADI</th>
                        <th class="text-center tg-rhys" colspan="3" style="border-bottom:0px">SELISIH</th>
                        <th class="text-center tg-rhys" rowspan="2" width=""> ! </th>
                     </tr>
                     <tr style="font-size:12px; color: #fff; background-color: #3568B6;">
                        <th class="text-center tg-rhys" width=" 2%"> Vol </th>
                        <th class="text-center tg-rhys" width="110px"> Jumlah </th>
                        <th class="text-center tg-rhys" width=" 1%"> @ </th>
                        <th class="text-center tg-rhys" width=" 2%"> Vol </th>
                        <th class="text-center tg-rhys" width="110px"> Jumlah </th>
                        <th class="text-center tg-rhys" width=" 1%"> @ </th>
                        <th class="text-center tg-rhys" width=" 2%"> Vol </th>
                        <th class="text-center tg-rhys" width="110px"> Jml </th>
                        <th class="text-center tg-rhys" width=" 1%"> # </th>
                     </tr>
                  </thead>

                  <tbody>
                     <?php
                     if (count($tabel['matrx']) > 0) {
                        foreach ($tabel['matrx'] as $row) {
                           $kode = $row['kode']; $uraian = $row['uraian'];
                           if ($row['level'] == 'satker')   { $style = 'font-size:14px; font-weight: bold; padding:0px; text-align: center'; }
                           if ($row['level'] == 'program')  { $style = 'font-size:12px; font-weight: bold; padding:0px; text-align: center'; }
                           if ($row['level'] == 'kegiatan') { $style = 'font-size:12px; font-weight: bold; padding:0px; text-align: center'; }
                           if ($row['level'] == 'output')   { $style = 'font-size:12px; font-weight: bold; padding:0px; text-align: center'; }
                           if ($row['level'] == 'soutput')  { $style = 'font-size:12px; padding:0px; text-align: right'; }
                           if ($row['level'] == 'komponen' or $row['level'] == 'digist') {
                              $style = 'font-size:12px; font-style: italic; padding:0px; text-align: right';
                              $kode = "<span style='padding-left: 10px'>$kode</span>"; $uraian = "<span style='padding-left: 10px'>$uraian</span>";
                           }
                           if ($row['level'] == 'jenbel')   {
                              $style = 'font-size:12px; padding:0px; text-align: right';
                              $kode = "<span style='padding-left: 20px'>$kode</span>"; $uraian = "<span style='padding-left: 20px'>$uraian</span>";
                           }
                           if ($row['level'] == 'akun')     {
                              $style = 'font-size:11px; color: grey; padding:0px; text-align: right';
                              $kode = "<span style='padding-left: 20px'>$kode</span>"; $uraian = "<span style='padding-left: 20px'>$uraian</span>";
                           }

                           $vol = ''; $jml = ''; $blok = '';
                           if ($row['vol1'] != $row['vol2'])  $vol  = 'text-danger';
                           if ($row['jml1'] != $row['jml2'])  $jml  = 'text-danger';
                           if ($row['blok1']!= $row['blok2']) $blok = 'text-danger';
                     ?>
                           <tr id="<?= $row['recid'] ?>" class="" style="<?= $style ?>">
                              <td> <?= $kode ?></td>
                              <td class="text-left"> <?= $uraian ?></td>
                              <td class="text-left  <?= $vol ?>"> <?= $row['vol1'] ?></td>
                              <td class="text-right <?= $jml ?>"> <?= $row['jml1'] ?></td>
                              <td class="text-left  <?= $blok ?>"><?= $row['blok1'] ?></td>
                              <td class="text-left  <?= $vol ?>"> <?= $row['vol2'] ?></td>
                              <td class="text-right <?= $jml ?>"> <?= $row['jml2'] ?></td>
                              <td class="text-left  <?= $blok ?>"><?= $row['blok2'] ?></td>
                              <td class="text-left  <?= $vol ?>"> <?= $row['vol3'] ?></td>
                              <td class="text-right <?= $jml ?>"> <?= $row['jml3'] ?></td>
                              <td class="text-left  <?= $blok ?>"><?= $row['blok3'] ?></td>
                              <td class="text-left"><?= $row['indikasi'] ?></td>
                           </tr>
                     <?php
                        }
                     } else {
                        echo '<tr><td colspan="12" class="text-center text-muted" style="font-size:13px; font-style: italic">Tidak ada data yang berbeda ... </td></tr>';
                     }
                     ?>
                  </tbody>
               </table>
            </div>

            <div class="container">
               <h3>DIPA Hal. IV A</h3>
               <table id="iHal4b" class="table table-hover table-bordered" style="border-spacing: 1; width: 100%;">
                  <thead>
                     <tr style="font-size:12px; color: #fff; background-color: #3568B6;">
                        <th class="text-center tg-rhys" width="80px"> KODE </th>
                        <th class="text-center tg-rhys" colspan="2" style="border-bottom:0px">URAIAN (SEMULA)</th>
                        <th class="text-center tg-rhys" colspan="2" style="border-bottom:0px">URAIAN (MENJADI)</th>
                     </tr>
                  </thead>

                  <tbody>
                     <?php if (count($tabel['blok']) > 0) {
                        foreach ($tabel['blok'] as $row) {
                           if (strpos('-kegiatan', $row['level'])) $ln = 'style="border-bottom: 1px solid black"'; else $ln = '';
                           if (strpos('-program-kegiatan', $row['level']))  echo '
                              <tr style="font-size:12px; font-weight:bold; padding:0px">
                                 <td class="text-right">'. $row['kode'] .'</td>
                                 <td class="text-left" '. $ln .' colspan="4">'. $row['uraian'] .'</td>
                              </tr>';
                           if (strpos('-output', $row['level']))  echo '
                              <tr style="font-size:12px; font-weight:bold; padding:0px">
                                 <td class="text-right">'. $row['kode'] .'</td>
                                 <td colspan="4">'. $row['uraian'] .'</td>
                             </tr>';
                           if (strpos('-akun', $row['level']))  echo '
                              <tr style="font-size:12px; padding:0px">
                                 <td class="text-right">'. $row['kode'] .'</td>
                                 <td class="text-left">'. $row['uraian'] .'</td>
                                 <td width="110px" class="text-right">'. number_format($row['blok']/1000, 0, ',', '.') .'</td>
                                 <td> &nbsp; </td>
                                 <td width="110px" class="text-right">'. number_format($row['mblok']/1000, 0, ',', '.') .'</td>
                              </tr>';

                           $cat  = ($row['uraian'] == '') ? '' : ' * '.$row['uraian'];
                           $mcat = (!isset($row['muraian']) or $row['muraian']== '') ? '' : ' * '.$row['muraian'];
                           if (strpos('-catatan', $row['level']))  echo '
                              <tr style="font-size:12px; padding:0px">
                                 <td class="text-right"> &nbsp; </td>
                                 <td> &nbsp; '. $cat  .'</td>
                                 <td width="110px" class="text-right">'. number_format($row['blok']/1000, 0, ',', '.') .'</td>
                                 <td> &nbsp; '. $mcat .'</td>
                                 <td width="110px" class="text-right">'. number_format($row['mblok']/1000, 0, ',', '.') .'</td>
                              </tr>';                        }
                     } else {
                        echo '<tr><td colspan="5" class="text-center text-muted" style="font-size:12px; font-style: italic">Tidak ada data yang berbeda ... </td></tr>';
                     } ?>
                  </tbody>
               </table>
            </div>

            <div class="container">
               <h3>DIPA Hal. IV B</h3>
               <table id="iHal4b" class="table table-hover table-bordered" style="border-spacing: 1; width: 100%;">
                  <thead>
                     <tr style="font-size:12px; color: #fff; background-color: #3568B6;">
                        <th class="text-center tg-rhys" width="80px"> KODE </th>
                        <th class="text-center tg-rhys" colspan="2" style="border-bottom:0px">URAIAN (SEMULA)</th>
                        <th class="text-center tg-rhys" colspan="2" style="border-bottom:0px">URAIAN (MENJADI)</th>
                     </tr>
                  </thead>

                  <tbody>
                     <?php if (count($tabel['catt']) > 0) {
                        foreach ($tabel['catt'] as $row) {
                           if (strpos('-kegiatan', $row['level'])) $ln = 'style="border-bottom: 1px solid black"'; else $ln = '';
                           if (strpos('-program-kegiatan', $row['level']))  echo '
                              <tr style="font-size:12px; font-weight:bold; padding:0px">
                                 <td class="text-right">'. $row['kode'] .'</td>
                                 <td class="text-left" '. $ln .' colspan="4">'. $row['uraian'] .'</td>
                              </tr>';
                           if (strpos('-output', $row['level']))  echo '
                              <tr style="font-size:12px; font-weight:bold; padding:0px">
                                 <td class="text-right">'. $row['kode'] .'</td>
                                 <td>'. $row['uraian'] .'</td>
                                 <td width="110px" class="text-right">'. number_format($row['pagu']/1000, 0, ',', '.') .'</td>
                                 <td> &nbsp; </td>
                                 <td width="110px" class="text-right">'. number_format($row['mpagu']/1000, 0, ',', '.') .'</td>
                              </tr>';
                           if (strpos('-akun', $row['level']))  echo '
                              <tr style="font-size:12px; padding:0px">
                                 <td class="text-right">'. $row['kode'] .'</td>
                                 <td class="text-left" colspan="2">'. $row['uraian'] .'</td>
                                 <td colspan="2"> &nbsp; </td>
                              </tr>';

                           $cat  = ($row['uraian'] == '') ? '' : ' * '.$row['uraian'];
                           $mcat = (!isset($row['muraian']) or $row['muraian']== '') ? '' : ' * '.$row['muraian'];
                           if (strpos('-catatan', $row['level']))  echo '
                              <tr style="font-size:12px; padding:0px">
                                 <td class="text-right"> &nbsp; </td>
                                 <td> &nbsp; '. $cat  .'</td>
                                 <td width="110px" class="text-right">'. number_format($row['pagu']/1000, 0, ',', '.') .'</td>
                                 <td> &nbsp; '. $mcat .'</td>
                                 <td width="110px" class="text-right">'. number_format($row['mpagu']/1000, 0, ',', '.') .'</td>
                              </tr>';                        }
                     } else {
                        echo '<tr><td colspan="5" class="text-center text-muted" style="font-size:12px; font-style: italic">Tidak ada data yang berbeda ... </td></tr>';
                     } ?>
                  </tbody>
               </table>
            </div>

         </div>
      </div>
   </div>
</div>
