<link href="<?=base_url('assets/plugins/select2/select2.css');?>" rel="stylesheet" type="text/css" />
<script src="<?=base_url('assets/plugins/select2/select2.full.min.js'); ?>" type="text/javascript"></script>

<style media="screen">
   td a {display:block;width:100%; color: red}
   A:link,A:visited,A:active,A:hover {text-decoration: none; color: white;}
   .table-bordered > thead > tr > th,
   .table-bordered > thead > tr > td {border-bottom: 2px solid #357CBC;}
   .table-bordered > tbody > tr > td {padding:3px}
   .dt {border:solid 1px #B1B9C1;padding:0px 5px;}
   .hiddenRow {padding: 0 !important;}
   .thd {width:65px;border-right:solid 1px #B1B9C1;border-bottom:solid 1px #B1B9C1; border-top:solid 1px #B1B9C1;text-align: center; background: #357CBC}
   section {position: relative;}
   section.positioned {position: absolute;top:10px;left:10px;}
   .container {overflow-y: auto;padding: 0px;padding-top: 0px;width: 100%}
   table {border-spacing: 1;width:100%;}
   th div{position: absolute;background #357CBC;color:black;border-bottom: solid #357CBC 1px;border-top: solid 1px #E7E9EE;
   padding: 6px 0px;top: 0;line-height: normal;}
   .calculated-width {width: -webkit-calc(100% - 1px);width: calc(100% -1px);}​
   .info{border-top:solid 1px #357CBC;padding:0px;margin:0px}
   .info1{border-top:0px;padding:0px;margin:0px}
   .x { display: inline-block; width: 110px; }
   .p3 {padding:3px}
   .tg-rhys{background-color:#317bbe;color:#FFF;text-align:center;vertical-align: middle !important;font-size:14px;font-weight:bold; border-style:solid; border-width:1px;overflow:hidden; word-break:normal;border-color:black;}
   .center{text-align: center} .hijau{background: #27ae60; color:white}
   .biru{background: #3498db; color:white} .abu{background: #bdc3c7; color:white}
   .merah{background: #e74c3c; color:white} .kuning{background:  #FFD700 ; color: white}
   .inSatker {text-indent: -45px; padding-left: 50px}
</style>


<div class="row">
   <div class="col-md-12">
      <div class="box box-widget">

         <div class="box-header with border" style="padding-bottom:0px">
            <div class="row">
               <div class="col-md-6" style="padding-bottom:3px">
                  <select id="select2" class="org form-control" tabindex="-1" onchange="changeUnit(this.value)" style="width:100%; z-index:50 !important;">
                     <?php foreach ($refr as $row) echo '<option value="'. $row['kode'] .'" '. $row['selected'] .'>'. $row['uraian'] .'</option>'; ?>
                  </select>
               </div>
               <div class="col-md-6">
                  <form role="form" action="<?= site_url("adk_dipa?q=e111f&unit=$unit") ?>" method="post" style="margin-bottom:0px">
                     <div class="input-group">
                        <input type="text" name="cari" class="form-control" placeholder="Pencarian..." value="<?php echo $this->session->userdata('cari') ?>" autocomplete="off">
                        <span class="input-group-btn">
                           <button type="submit" name="search" value="search" class="btn btn-info btn-flat"><i class="fa fa-search" style="height:16px;margin-top:4px"></i></button>
                        </span>
                        <span class="input-group-btn">
                           <button type="submit" name="search" value="clear" class="btn btn-info btn-flat"><i class="fa fa-times-circle" style="height:16px;margin-top:4px"></i></button>
                        </span>
                     </div>
                  </form>
               </div>
            </div>
         </div>

         <div class="box-body" style="padding-top:7px">
            <section class="">
               <div class="container">
                  <table id="iGrid" class="table table-hover table-bordered" style="margin-bottom:0px">
                     <thead>
                            <td class="tg-rhys" style="border-bottom:solid white 2px">
                               <div class="">Satuan Kerja</div>
                            </td>
                            <td class="tg-rhys" style="border-bottom:solid white 2px; width:35px">
                               <div class="">Rev</div>
                            </td>
                            <td class="tg-rhys" style="border-bottom:solid white 2px; width:110px">
                               <div class="">Tgl. DIPA</div>
                            </td>
                            <td class="tg-rhys" style="border-bottom:solid white 2px; width:35px">
                               <div class="">SSB</div>
                            </td>
                            <td class="tg-rhys" style="border-bottom:solid white 2px; width:110px">
                               <div class="">Nilai Pagu</div>
                            </td>
                        </tr>
                     </thead>

                     <tbody id="data_monit">
                        <?php if ($sum) { ?>
                           <tr style="margin:0px;">
                              <td style="" class="small text-Left text-bold"><?= $sum['kode'] .' '. $refr[$sum['kode']]['uraian'] ?></td>
                              <td style="" class="small text-center text-bold" colspan="2"> <?= $sum['satker'] .' satker' ?> </td>
                              <td style="" class="small text-center text-bold"><?= $sum['ssb_akun'] ?></td>
                              <td style="" class="small text-right text-bold"><?= number_format($sum['ssb_pagu'], 0, ',', '.') ?></td>
                           </tr>
                        <?php } ?>

                        <?php if ($data) { ?>
                           <?php foreach ($data as $row) { ?>
                              <tr style="margin:0px;">
                                 <td style="" class="small text-Left" data-toggle="collapse" data-target=".<?= str_replace('.', '_', $row['kode']) ?>">
                                    <?= $row['kode'] .' '. $row['uraian'] ?>
                                 </td>
                                 <td style="" class="small text-center"><?= $row['revisike'] ?></td>
                                 <td style="" class="small text-center"><?= $this->fc->idtgl($row['tgladk'], 'tgl') ?></td>
                                 <td style="" class="small text-center"><?= $row['ssb_akun'] ?></td>
                                 <td style="" class="small text-right"><?= number_format($row['ssb_pagu'], 0, ',', '.') ?></td>
                              </tr>

                              <tr style="background:#FFF;color:#34495E; ">
                                 <td class="hiddenRow" colspan="9" style="border:solid 0px red">
                                    <div class=" collapse <?= str_replace('.', '_', $row['kode']) ?> " style="padding:7px">
                                       <table class="table-hover" style="margin-top:5px">
                                          <tr style="background:#3A99D9; color:#FFF; font-size:14px">
                                             <td class="dt" style="padding:3px; text-align:center; width:70px"><b>Output</b></td>
                                             <td class="dt" style="padding:3px; text-align:center; width:35px"><b>Sub</b></td>
                                             <td class="dt" style="padding:3px; text-align:center; width:70px"><b>Komponen</b></td>
                                             <td class="dt" style="padding:3px; text-align:center"><b>Akun</b></td>
                                             <td class="dt" style="padding:3px; text-align:center; width:110px"><b>Jumlah</b></td>
                                          </tr>


                                          <?php foreach ($row['ssb_array'] as $val) { ?>
                                             <tr style="font-size:14px">
                                                <td class="dt p3" style="vertical-align: text-top; text-align:center;">
                                                   <?= $val['kdgiat'] .'.'. $val['kdoutput'] ?>
                                                </td>
                                                <td class="dt p3" style="vertical-align: text-top; text-align:center;">
                                                   <?= $val['kdsoutput'] ?>
                                                </td>
                                                <td class="dt p3" style="vertical-align: text-top; text-align:center;">
                                                   <?= $val['kdkmpnen'] .'.'. $val['kdskmpnen'] ?>
                                                </td>
                                                <td class="dt p3" style="vertical-align: text-top; text-align:left;">
                                                   <?= $val['kdakun'] .' '. $val['uraian'] ?>
                                                </td>
                                                <td class="dt p3" style="vertical-align: text-top; text-align:right"><?= number_format($val['jumlah'], 0, ',', '.') ?></td>
                                             </tr>
                                          <?php } ?>
                                       </table>
                                       <span>
                                          <span class="small iMatrik" data-list="<?= $row['link_matrik'] ?>" style="margin-left:50px; color:#357CBC">
                                             <i class="fa fa-bookmark"></i> Matriks
                                          </span>
                                          <span class="small iPok" data-list="<?= $row['link_pok'] ?>"" style="margin-left:10px; color:#357CBC">
                                             <i class="fa fa-bookmark"></i> POK
                                          </span>
                                          <span class="small iSsb" data-list="<?= $row['link_ssb'] ?>"" style="margin-left:10px; color:#357CBC">
                                             <i class="fa fa-bookmark"></i> SSB
                                          </span>
                                       </span>
                                    </div>
                                 </td>
                              </tr>
                           <?php
                           }
                        } else {
                           echo '<tr><td colspan="5" class="text-center text-muted" style="font-size:13px; font-style: italic">Tidak ada data ... </td></tr>';
                        }
                        ?>
                     </tbody>
                  </table>
                  <div class="pull-right">
                     <?php echo $this->pagination->create_links(); ?>
                  </div>
               </div>
            </section>
         </div>

      </div>
   </div>

</div>


<script type="text/javascript">
   $('.collapse').on('show.bs.collapse', function () {
      $('.collapse.in').collapse('hide');
   });

   $('.iMatrik, .iPok, .iSsb').css('cursor', 'pointer');
   $('.iMatrik, .iPok, .iSsb').click( function(event) {
      window.open( $(this).data('list'), '_blank');
   })

   function changeUnit(nil) {
      window.location.href = '<?= site_url('adk_dipa?q=e111f&unit=') ?>' +nil;
   }

   $(document).ready(function() {
     $(".org").select2({
       minimumResultsForSearch:5
     });
   });
</script>
