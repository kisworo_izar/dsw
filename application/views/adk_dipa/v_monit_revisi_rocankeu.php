<link href="<?=base_url('assets/plugins/select2/select2.css');?>" rel="stylesheet" type="text/css" />
<script src="<?=base_url('assets/plugins/select2/select2.full.min.js'); ?>" type="text/javascript"></script>

<style media="screen">
   .td_det {border:solid 1px #D2D6DE!important}
   .inSatker {text-indent: -47px; padding-left: 48px!important}
   .tg-rhys{background-color:#317bbe;color:#FFF;text-align:center;vertical-align: middle !important;font-size:14px;font-weight:bold; border-style:solid; border-width:1px;overflow:hidden; word-break:normal;border-color:black;}
   section {position: relative;}
   section.positioned {position: absolute;top:10px;left:10px;}
   .container {overflow-y: auto;padding: 0px;padding-top: 0px;width: 100%}
   td{padding: 5px!important}
</style>

<div id="main" class="row">
   <div class="col-md-12">
      <div class="box box-widget" style="margin-bottom:0px">
         <input type="hidden" id="aktif">

         <div class="box-header with border" style="padding-bottom:0px">
            <div class="row">
               <div class="col-md-6" style="padding-bottom:3px">
                  <select id="select2" class="org form-control" tabindex="-1" onchange="changeUnit(this.value)" style="width:100%; z-index:50 !important;">
                     <?php foreach ($refr as $row) echo '<option value="'. $row['kode'] .'" '. $row['selected'] .'>'. $row['uraian'] .'</option>'; ?>
                  </select>
               </div>
               <div class="col-md-6">
                  <form role="form" action="<?php echo site_url("adk_dipa?q=3b134") ?>" method="post" style="margin-bottom:0px">
                     <div class="input-group">
                        <input type="text" name="cari" class="form-control" placeholder="Pencarian..." value="<?php echo $this->session->userdata('cari') ?>" autocomplete="off">
                        <span class="input-group-btn">
                           <button type="submit" name="search" value="search" class="btn btn-info btn-flat"><i class="fa fa-search" style="height:16px;margin-top:4px"></i></button>
                        </span>
                        <span class="input-group-btn">
                           <button type="submit" name="search" value="clear" class="btn btn-info btn-flat"><i class="fa fa-times-circle" style="height:16px;margin-top:4px"></i></button>
                        </span>
                     </div>
                  </form>
               </div>
            </div>
         </div>


         <div class="box-body" style="padding-top:7px">
            <div class="container">
               <table id="iGridPok" class="table table-hover table-bordered" style="border-spacing: 0px; margin-bottom:0px">
                  <thead>
                     <tr style="font-size:12px; color:#fff; background-color:#3568B6; padding:0px !important;">
                         <td class="tg-rhys" rowspan="3">Kode dan Nomenklatur Satker</td>
                         <td class="tg-rhys" rowspan="3">Rev.</td>
                         <td class="tg-rhys" colspan="3" rowspan="2" style="border-bottom:0px">Revisi</td>
                         <td class="tg-rhys" colspan="4" style="border-bottom:0px">Volume</td>
                         <td class="tg-rhys" colspan="3" rowspan="2" style="border-bottom:0px">Pagu</td>
                         <td class="tg-rhys" rowspan="2" style="border-bottom:0px" width="50px">Tgl</td>
                       </tr>
                       <tr>
                         <td class="tg-rhys" colspan="2" style="border-bottom:0px">Output</td>
                         <td class="tg-rhys" colspan="2" style="border-bottom:0px">Komponen</td>
                       </tr>
                       <tr>
                         <td class="tg-rhys">PA</td>
                         <td class="tg-rhys">Kwl</td>
                         <td class="tg-rhys">DJA</td>
                         <td class="tg-rhys">Awal</td>
                         <td class="tg-rhys">Revisi</td>
                         <td class="tg-rhys">Awal</td>
                         <td class="tg-rhys">Revisi</td>
                         <td class="tg-rhys">Awal</td>
                         <td class="tg-rhys">Revisi</td>
                         <td class="tg-rhys">Usulan</td>
                         <td class="tg-rhys">SD</td>
                       </tr>
                  </thead>

                     <tbody id="data_revisi">
                        <?php  foreach ($data as $row) {
                           $jml_rev = ''; if ($row['pagu_awl'] != $row['pagu_rev'])  $jml_rev = 'text-danger';
                        ?>
                           <tr id="<?= $row['kode'] ?>" class="satker <?= $row['color'] ?>" style="font-size:14px; padding:0px;">
                              <td class="text-left tdp">
                                 <div class="inSatker">  <?= $row['kode'] .'&nbsp;&nbsp;'. $row['uraian']?>
                                 </div>
                              </td>
                              <td class="text-center"><?= $row['revisike'] ?></td>
                              <td class="text-center"><?php if ($row['statusrev'] == 'P') echo '<i class="fa fa-check"></i>' ?></td>
                              <td class="text-center"><?php if ($row['statusrev'] == 'K') echo '<i class="fa fa-check"></i>' ?></td>
                              <td class="text-center"><?php if ($row['statusrev'] == 'D') echo '<i class="fa fa-check"></i>' ?></td>
                              <td class="text-right">&nbsp;</td>
                              <td class="text-right">&nbsp;</td>
                              <td class="text-right">&nbsp;</td>
                              <td class="text-right">&nbsp;</td>
                              <td class="text-right <?= $jml_rev ?>"> <?= number_format($row['pagu_awl'], 0, ',', '.') ?></td>
                              <td class="text-right <?= $jml_rev ?>"> <?= number_format($row['pagu_rev'], 0, ',', '.') ?></td>
                              <td class="text-right"></td>
                              <td class="text-center"><?= substr($this->fc->idtgl($row['tgladk'], 'tgl'), 0, 6) ?></td>
                           </tr>

                           <?php foreach ($row['sub'] as $sub) {

                              if ($sub['level'] == 'Output') {
                                 $kode  = $sub['kode'] .' '. $sub['uraian'];
                                 $style = 'font-size:12px; font-weight:bold; padding:0px; background-color:#ECF0F5; display:none;';
                              }
                              if ($sub['level'] == 'Sub Output') {
                                 $kode  = '<span style="padding-left:10px">'. substr($sub['kode'], -3) .' '. $sub['uraian'] .'</span>';
                                 $style = 'font-size:12px; padding:0px; background-color:#ECF0F5; display:none';
                              }
                              if ($sub['level'] == 'Komponen') {
                                 $kode  = '<span style="padding-left:30px;">'. substr($sub['kode'], -3) .' '. $sub['uraian'] .'</span>';
                                 $style = 'font-size:12px; padding:0px; background-color:#ECF0F5; display:none';
                              }

                              $vol_out = ''; $vol_kmp = ''; $jml_rev = '';
                              if ($sub['out_awl'] != $sub['out_rev'])  $vol_out = 'text-danger';
                              if ($sub['kmp_awl'] != $sub['kmp_rev'])  $vol_kmp = 'text-danger';
                              if ($sub['pagu_awl']!= $sub['pagu_rev']) $jml_rev = 'text-danger';
                             ?>
                             <tr class="sub-satker sub-<?= $row['kode'] ?>" style="<?= $style ?>">
                                <td class="td_det text-left">  <?= $kode ?></td>
                                <td class="td_det text-center">&nbsp;</td>
                                <td class="td_det text-center">&nbsp;</td>
                                <td class="td_det text-center">&nbsp;</td>
                                <td class="td_det text-center">&nbsp;</td>
                                <td class="td_det text-right <?= $vol_out ?>"> <?= $sub['out_awl'] ?></td>
                                <td class="td_det text-right <?= $vol_out ?>"> <?= $sub['out_rev'] ?></td>
                                <td class="td_det text-right <?= $vol_kmp ?>"> <?= $sub['kmp_awl'] ?></td>
                                <td class="td_det text-right <?= $vol_kmp ?>"> <?= $sub['kmp_rev'] ?></td>
                                <td class="td_det text-right <?= $jml_rev ?>"> <?= number_format($sub['pagu_awl'], 0, ',', '.') ?></td>
                                <td class="td_det text-right <?= $jml_rev ?>"> <?= number_format($sub['pagu_rev'], 0, ',', '.') ?></td>
                                <td class="td_det text-right"></td>
                                <td class="td_det text-center"><?= $sub['beban'] ?></td>
                             </tr>
                          <?php } ?>
                       <?php } ?>
                    </tbody>
               </table>
               <div class="pull-right">
                  <?php echo $this->pagination->create_links(); ?>
               </div>
            </div>
         </div>

      </div>
   </div>
</div>

<script type="text/javascript">
   $('.satker').click(function() {
      var id = $(this).attr('id');
      $('.sub-satker').hide();
      if ($('#aktif').val() == id) { $('#aktif').val(''); $('.sub-'+id).hide(); }
      else { $('#aktif').val(id); $('.sub-'+id).show(); }
   });

   function changeUnit(nil) {
      window.location.href = '<?= site_url('adk_dipa?q=3b134&unit=') ?>' + nil;
   }

   $(document).ready(function() {
     $(".org").select2({
       minimumResultsForSearch:5
     });
   });
</script>
