<script src="<?=base_url('assets/plugins/jQuery/jQuery-2.1.3.min.js')?>" type="text/javascript"></script>
<script src="<?=base_url('assets/plugins/w2ui/w2ui-1.5.rc1.min.js')  ?>" type="text/javascript"></script>
<link href="<?= base_url('assets/plugins/w2ui/w2ui-1.5.rc1.min.css') ?>" rel="stylesheet" type="text/css" />

<div id="main" style="width: 100%; height: 400px;"></div>


<script type="text/javascript">
  var table = <?= json_encode($table) ?>, memvar = [], cRuh = '',
    config = {
    layout: { name: 'layout', padding: 0, attr: "align=center",
      panels: [{ type: 'main', minSize: 550, overflow: 'hidden' }],
    },     

    grdtbl: {
      name: 'grdtbl', show: { toolbar: true, footer: true, toolbarAdd: true, toolbarEdit: true, toolbarDelete: true }, recid: 0,
      columns: [
        { field: 'iduser',      caption: '<center>ID User</center>',    size: '80px',   sortable: true, attr: "align=left" },
        { field: 'nmuser',      caption: '<center>Nama User</center>',  size: '100%',   sortable: true, attr: "align=left" },
        { field: 'kddept',      caption: '<center>K/L</center>',        size: '50px',   sortable: true, attr: "align=center" },
        { field: 'kdunit',      caption: '<center>Es. I</center>',      size: '50px',   sortable: true, attr: "align=center" },
        { field: 'nohp',        caption: '<center>No. HP</center>',     size: '100px',  sortable: true, attr: "align=left" },
        { field: 'email',       caption: '<center>Email</center>',      size: '180px',  sortable: true, attr: "align=left" },
        { field: 'idusergroup', caption: '<center>Group</center>',      size: '100px',  sortable: true, attr: "align=left" }
      ],
      onAdd   : function(event){ popupCrud( w2ui.grdtbl.recid, 'Rekam') },
      onEdit  : function(event){ popupCrud( w2ui.grdtbl.recid, 'Ubah' ) },
      onDelete: function(event){ popupCrud( w2ui.grdtbl.recid, 'Hapus' ); event.force = true; },
      onClick : function(event){ w2ui.grdtbl.recid = event.recid },  
      records: table
    }
  };

  $(function () {
    $('#main').css('height', $(window).height() * 0.84);
    $('#main').w2layout(config.layout);
    w2ui.layout.content('main', $().w2grid(config.grdtbl));
  });
</script>

<?php $this->load->view('admin_satudja/v_user_satudja_crud') ?>