<!DOCTYPE html>
<html>
<head>
    <title>W2UI Demo: forms-8</title>
    <!-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
    <script type="text/javascript" src="http://rawgit.com/vitmalina/w2ui/master/dist/w2ui.min.js"></script>
    <link rel="stylesheet" type="text/css" href="http://rawgit.com/vitmalina/w2ui/master/dist/w2ui.min.css" /> -->
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
    <script src="<?= base_url('assets/plugins/w2ui/w2ui-1.5.rc1.min.js') ?>" type="text/javascript"></script>
    <link href="<?= base_url('assets/plugins/w2ui/w2ui-1.5.rc1.min.css') ?>" rel="stylesheet" type="text/css" />

</head>
<body>

<br>
<button class="w2ui-btn" onclick="popupCrud()">Open Form in a Popup</button>
<br><br><br>

<script type="text/javascript">
function popupCrud () {
    if (!w2ui.foo) {
        $().w2form({
            name: 'foo',
            style: 'border: 0px; background-color: transparent;',
            formHTML: $('#iForm').html(),
            fields: [
                { field: 'first_name', type: 'text', required: true },
                { field: 'last_name', type: 'text', required: true },
                { field: 'email', type: 'email' }
            ],
            record: { 
                first_name    : 'John',
                last_name     : 'Doe',
                email         : 'jdoe@email.com'
            },
            actions: {
                "save": function () { this.validate(); },
                "reset": function () { this.clear(); }
            }
        });
    }
    $().w2popup('open', {
        title   : 'Form in a Popup',
        body    : '<div id="form" style="width: 100%; height: 100%;"></div>',
        style   : 'padding: 15px 0px 0px 0px',
        width   : 500,
        height  : 300, 
        showMax : true,
        onToggle: function (event) {
            $(w2ui.foo.box).hide();
            event.onComplete = function () {
                $(w2ui.foo.box).show();
                w2ui.foo.resize();
            }
        },
        onOpen: function (event) {
            event.onComplete = function () {
                // specifying an onOpen handler instead is equivalent to specifying an onBeforeOpen handler, which would make this code execute too early and hence not deliver.
                $('#w2ui-popup #form').w2render('foo');
            }
        }
    });
}
</script>

<script id="iForm" type="text/template">
  <div class="w2ui-page page-0">
    <div class="w2ui-field">
      <label>First Name:</label>
      <div><input name="first_name" type="text" maxlength="100" style="width: 250px"/></div>
    </div>
    <div class="w2ui-field">
      <label>Last Name:</label>
      <div><input name="last_name" type="text" maxlength="100" style="width: 250px"/></div>
    </div>
    <div class="w2ui-field">
      <label>Email:</label>
      <div><input name="email" type="text" style="width: 250px"/></div>
    </div>
  </div>
  <div class="w2ui-buttons">
    <button class="w2ui-btn" name="reset">Reset</button>
    <button class="w2ui-btn" name="save">Save</button>
  </div>
</script>
<!-- </body>
</html> -->