<script type="text/javascript">
  function popupCrud(id, aksi) {
    var memvar = table[id]; cRuh = aksi;
    if (aksi == 'Rekam') memvar = [{idmenu: '', menu: '', link: '', icon: '', urutan: '', aktif: ''}];

    if (!w2ui.foo) {
      $().w2form({
        name: 'foo', style: 'border: 0px; background-color: transparent;',
        formHTML: $('#iForm').html(),
        fields: [
          { field: 'idmenu',type: 'text', required: true },
          { field: 'menu',  type: 'text', required: true },
          { field: 'link',  type: 'text' },
          { field: 'icon',  type: 'text' },
          { field: 'urutan',type: 'text' },
          { field: 'aktif', type: 'text' }
        ],
        record: {idmenu: '', menu: '', link: '', icon: '', urutan: '', aktif: ''},
        actions: {
          "simpan": function(){ this.validate(); crud(aksi); w2popup.close(); },
          "batal" : function(){ w2popup.close(); }
        }
      });
    }

    $().w2popup('open', {
      title   : aksi + ' Tabel Menu',
      body    : '<div id="form" style="width: 100%; height: 100%;"></div>',
      style   : 'padding: 15px 0px 0px 0px',
      width   : 500,
      height  : 330, 
      showMax : false,
      onToggle: function (event) {
        $(w2ui.foo.box).hide();
        event.onComplete = function () {
          $(w2ui.foo.box).show();
          w2ui.foo.resize();
        }
      },
      onOpen: function (event) {
        event.onComplete = function () {
          $('#simpan').innnerHTML = cRuh;
          w2ui.foo.record = memvar;
          $('#w2ui-popup #form').w2render('foo');
        }
      }
    });
  }
</script>

<script id="iForm" type="text/template">
  <div id="isian_form" class="w2ui-page page-0">
    <div class="w2ui-field">
      <input name="aksi" id="aksi" type="hidden" value="Rekam" />
      <label>ID Menu</label>
      <div><input name="idmenu" id="idmenu" type="text" maxlength="100" style="width: 300px"/></div>
    </div>
    <div class="w2ui-field">
      <label>Uraian</label>
      <div><input name="menu" type="text" maxlength="100" style="width: 300px"/></div>
    </div>
    <div class="w2ui-field">
      <label>Link</label>
      <div><input name="link" type="text" style="width: 300px"/></div>
    </div>
    <div class="w2ui-field">
      <label>Icon</label>
      <div><input name="icon" type="text" style="width: 300px"/></div>
    </div>
    <div class="w2ui-field">
      <label>Urutan</label>
      <div><input name="urutan" type="text" style="width: 300px"/></div>
    </div>
    <div class="w2ui-field">
      <label>Aktif</label>
      <div><input name="aktif" type="text" style="width: 300px"/></div>
    </div>
  </div>
  <div class="w2ui-buttons">
    <button class="w2ui-btn" name="batal"  id="batal">Batal</button>
    <button class="w2ui-btn" name="simpan" id="simpan">Simpan</button>
  </div>
</script>

<script type="text/javascript">
  function crud(aksi) {
    $('#aksi').val(aksi);
    var id = $('#idmenu').val(), 
        dataString = $("#isian_form input").serialize();

    $.ajax({
      type: "POST", data: dataString, url: "<?php echo site_url('admin_satudja/crud_menu') ?>",
      success: function(nil) {
        if (nil != '') {
          if (aksi == 'Rekam') table[id] = nil;
          if (aksi == 'Ubah')  table[id] = nil;
          if (aksi == 'Hapus') delete table[id];
          console.log( 'nil :'+ table[id] );
          w2ui.grdtbl.refresh(id);
        }
      }  
    })
  }
</script>
