<script type="text/javascript">
  function popupCrud(id, aksi) {
    var memvar = table[id]; cRuh = aksi;
    if (aksi == 'Rekam') memvar = [{idusergroup: '', nmusergroup: '', kwdept: '', kwunit: '', kwlokasi: '', menu: ''}];

    if (!w2ui.foo) {
      $().w2form({
        name: 'foo', style: 'border: 0px; background-color: transparent;',
        formHTML: $('#iForm').html(),
        fields: [
          { field: 'iduser',      type: 'text', required: true },
          { field: 'nmuser',      type: 'text', required: true },
          { field: 'fullname',    type: 'text' },
          { field: 'kddept',      type: 'text' },
          { field: 'kdunit',      type: 'text' },
          { field: 'nohp',        type: 'text' },
          { field: 'email',       type: 'text' },
          { field: 'idusergroup', type: 'text' },
        ],
        record: {iduser: '', nmuser: '', fullname: '', kddept: '', kdunit: '', nohp: '', email: '', idusergroup: ''},
        actions: {
          "simpan": function(){ this.validate(); crud(aksi); w2popup.close(); },
          "batal" : function(){ w2popup.close(); }
        }
      });
    }
    $().w2popup('open', {
      title   : aksi + ' Tabel User',
      body    : '<div id="form" style="width: 100%; height: 100%;"></div>',
      style   : 'padding: 15px 0px 0px 0px',
      width   : 700,
      height  : 410, 
      showMax : false,
      onToggle: function (event) {
        $(w2ui.foo.box).hide();
        event.onComplete = function () {
          $(w2ui.foo.box).show();
          w2ui.foo.resize();
        }
      },
      onOpen: function (event) {
        event.onComplete = function () {
          $('#simpan').innnerHTML = cRuh;
          w2ui.foo.record = memvar;
          $('#w2ui-popup #form').w2render('foo');

          var subdit = w2ui.grdstk.recid.substr(0,4);
          alert(subdit);
          // $('#parent-1').val(kdgiat).attr('selected',true); changeGiat(kdgiat);
        }
      }
    });
  }
</script>

<script id="iForm" type="text/template">
  <div id="isian_form" class="w2ui-page page-0">
    <div class="w2ui-field">
      <input name="aksi" id="aksi" type="hidden" value="Rekam" />
      <label>ID User</label>
      <div><input name="iduser" id="iduser" type="text" maxlength="100" style="width: 500px"/></div>
    </div>
    <div class="w2ui-field">
      <label>Nama Lengkap</label>
      <div><input name="nmuser" type="text" maxlength="100" style="width: 500px"/></div>
    </div>
    <div class="w2ui-field">
      <label>Kontak</label>
      <div><input name="fullname" type="text" style="width: 500px"/></div>
    </div>
    <div class="w2ui-field">
      <label>K/L</label>
      <div><input name="kddept" type="text" style="width: 40px"/></div>
    </div>
    <div class="w2ui-field">
      <label>Unit</label>
      <div><input name="kdunit" type="text" style="width: 40px"/></div>
    </div>
    <div class="w2ui-field">
      <label>No. HP</label>
      <div><input name="nohp" type="text" style="width: 500px"/></div>
    </div>
    <div class="w2ui-field">
      <label>Email</label>
      <div><input name="email" type="text" style="width: 500px"/></div>
    </div>
    <div class="w2ui-field">
      <label>UserGroup</label>
      <div>
        <select id="subdit" maxlength="100" style="width: 500px" class="org form-control" name="subdit"> 
          <option value="" type="list">**Pilih Kewenangan sesuai Subdit**</option>
          <?php foreach ($table['subdit'] as $row) echo '<option value="'. $row['idusergroup'] .'">'.$row['idusergroup'].' - '.$row['nmusergroup'] .'</option>'; ?>
        </select>
      </div>
    </div>
  </div>
  <div class="w2ui-buttons">
    <button class="w2ui-btn" name="batal"  id="batal">Batal</button>
    <button class="w2ui-btn" name="simpan" id="simpan">Simpan</button>
  </div>
</script>

<script type="text/javascript">
  function crud(aksi) {
    $('#aksi').val(aksi);
    var id = $('#iduser').val(), 
        dataString = $("#isian_form input").serialize();

    $.ajax({
      type: "POST", data: dataString, url: "<?php echo site_url('admin_satudja/crud_user') ?>",
      success: function(nil) {
        if (nil != '') {
          if (aksi == 'Rekam') table[id] = nil;
          if (aksi == 'Ubah')  table[id] = nil;
          if (aksi == 'Hapus') delete table[id];
          console.log( 'nil :'+ table[id] );
          w2ui.grdtbl.refresh(id);
        }
      }  
    })
  }
</script>