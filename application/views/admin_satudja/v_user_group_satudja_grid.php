<script src="<?=base_url('assets/plugins/jQuery/jQuery-2.1.3.min.js')?>" type="text/javascript"></script>
<script src="<?=base_url('assets/plugins/w2ui/w2ui-1.5.rc1.min.js')  ?>" type="text/javascript"></script>
<link href="<?= base_url('assets/plugins/w2ui/w2ui-1.5.rc1.min.css') ?>" rel="stylesheet" type="text/css" />

<div id="main" style="width: 100%; height: 400px;"></div>


<script type="text/javascript">
  var table = <?= json_encode($table) ?>, memvar = [], cRuh = '', 
    config = {
    layout: { name: 'layout', padding: 0, attr: "align=center" ,
      panels: [{ type: 'main', minSize: 550, overflow: 'hidden' }],
    },

    grdtbl: {
      name: 'grdtbl', reorderRows: true, show: { toolbar: true, footer: true, toolbarAdd: true, toolbarDelete: true, toolbarEdit: true }, recid: 0,
      columns: [
        { field: 'idusergroup', caption: '<center>ID Group</center>',       size: '80px',   sortable: true, attr: "align=left" },
        { field: 'nmusergroup', caption: '<center>Nama User Group</center>',size: '100%',   sortable: true, attr: "align=left" },
        { field: 'kwdept',      caption: '<center>Kementerian</center>',    size: '200px',  sortable: true, attr: "align=left" },
        { field: 'kwunit',      caption: '<center>Eselon I</center>',       size: '60px',   sortable: true, attr: "align=left" },
        { field: 'kwlokasi',    caption: '<center>Lokasi</center>',         size: '60px',   sortable: true, attr: "align=left" },
        { field: 'menu',        caption: '<center>Menu</center>',           size: '150px',  sortable: true, attr: "align=left" }
      ],
      onAdd   : function(event){ popupCrud( w2ui.grdtbl.recid, 'Rekam') },
      onEdit  : function(event){ popupCrud( w2ui.grdtbl.recid, 'Ubah' ) },
      onDelete: function(event){ if (event.force ) hapus( w2ui.grdtbl.recid ); },
      onClick : function(event){ w2ui.grdtbl.recid = event.recid },  
      records : table,
    }
  };

  $(function () {
    $('#main').css('height', $(window).height() * 0.84);
    $('#main').w2layout(config.layout);
    w2ui.layout.content('main', $().w2grid(config.grdtbl));
  });
</script>

<?php $this->load->view('admin_satudja/v_user_group_satudja_crud') ?>