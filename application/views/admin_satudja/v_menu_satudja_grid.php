<script src="<?=base_url('assets/plugins/jQuery/jQuery-2.1.3.min.js')?>" type="text/javascript"></script>
<script src="<?=base_url('assets/plugins/w2ui/w2ui-1.5.rc1.min.js')  ?>" type="text/javascript"></script>
<link href="<?= base_url('assets/plugins/w2ui/w2ui-1.5.rc1.min.css') ?>" rel="stylesheet" type="text/css" />

<div id="main" style="width: 100%; height: 400px;"></div>


<script type="text/javascript">
  var table = <?= json_encode($table) ?>, memvar = [], cRuh = '', 
    config = {
    layout: { name: 'layout', padding: 0, attr: "align=center" ,
      panels: [{ type: 'main', minSize: 550, overflow: 'hidden' }],
    },

    grdtbl: {
      name: 'grdtbl', show: { toolbar: true, footer: true, toolbarAdd: true, toolbarDelete: true, toolbarEdit: true }, recid: 0,
      columns: [
        { field: 'idmenu',  caption: '<center>ID Menu</center>',    size: '80px',   sortable: true, attr: "align=center" },
        { field: 'menu',    caption: '<center>Uraian Menu</center>',size: '100%',   sortable: true, attr: "align=left" },
        { field: 'link',    caption: '<center>Link</center>',       size: '150px',  sortable: true, attr: "align=left" },
        { field: 'icon',    caption: '<center>Icon</center>',       size: '120px',  sortable: true, attr: "align=center" },
        { field: 'urutan',  caption: '<center>Urutan</center>',     size: '40px',   sortable: true, attr: "align=center" },
        { field: 'aktif',   caption: '<center>Aktif</center>',      size: '40px',   sortable: true, attr: "align=center" }
      ],
      onAdd   : function(event){ popupCrud( w2ui.grdtbl.recid, 'Rekam') },
      onEdit  : function(event){ popupCrud( w2ui.grdtbl.recid, 'Ubah' ) },
      onDelete: function(event){ popupCrud( w2ui.grdtbl.recid, 'Hapus' ); event.force = true; },
      onClick : function(event){ w2ui.grdtbl.recid = event.recid },  
      records : table,
    }
  };

  $(function () {
    $('#main').css('height', $(window).height() * 0.84);
    $('#main').w2layout(config.layout);
    w2ui.layout.content('main', $().w2grid(config.grdtbl));
  });
</script>

<?php $this->load->view('admin_satudja/v_menu_satudja_crud') ?>