<script type="text/javascript">
  function popupCrud(id, aksi) {
    var memvar = table[id]; cRuh = aksi; cRecid = memvar.recid;
    if (aksi == 'Rekam') memvar = [{idusergroup: '', nmusergroup: '', menu: '', kwdept: '', kwunit: '', kwlokasi: '', recid: ''}];
    console.log(memvar);
    if (!w2ui.foo) {
      $().w2form({
        name: 'foo', style: 'border: 0px; background-color: transparent;',
        formHTML: $('#iForm').html(),
        fields: [
          { field: 'idusergroup', type: 'text', required: true },
          { field: 'nmusergroup', type: 'text', required: true },
          { field: 'kwdept',      type: 'text' },
          { field: 'kwunit',      type: 'text' },
          { field: 'kwlokasi',    type: 'text' },
          { field: 'menu',        type: 'text' }
        ],
        record: {idusergroup: '', nmusergroup: '', kwdept: '', kwunit: '', kwlokasi: '', menu: ''},
        actions: {
          "simpan": function(){ this.validate(); crud(aksi); w2popup.close(); },
          "batal" : function(){ w2popup.close(); }
        }
      });
    }

    $().w2popup('open', {
      title   : aksi + ' Tabel UserGroup',
      body    : '<div id="form" style="width: 100%; height: 100%;"></div>',
      style   : 'padding: 15px 0px 0px 0px',
      width   : 700,
      height  : 330, 
      showMax : false,
      onToggle: function (event) {
        $(w2ui.foo.box).hide();
        event.onComplete = function () {
          $(w2ui.foo.box).show();
          w2ui.foo.resize();
        }
      },
      onOpen: function (event) {
        event.onComplete = function () {
          $('#simpan').innnerHTML = cRuh;
          w2ui.foo.record = memvar;
          alert(w2ui.foo.record)
          $('#w2ui-popup #form').w2render('foo');
        }
      }
    });
  }
</script>

<script id="iForm" type="text/template">
  <div id="isian_form" class="w2ui-page page-0">
    <div class="w2ui-field">
      <input name="aksi"  id="aksi"  type="hidden" value="" />
      <input name="recid" id="recid" type="hidden" value="" />
      <label>ID UserGroup</label>
      <div><input name="idusergroup" id="idusergroup" type="text" maxlength="100" style="width: 500px"/></div>
    </div>
    <div class="w2ui-field">
      <label>Nama Group</label>
      <div><input name="nmusergroup" type="text" maxlength="100" style="width: 500px"/></div>
    </div>
    <div class="w2ui-field">
      <label>K / L</label>
      <div><input name="kwdept" type="text" style="width: 500px"/></div>
    </div>
    <div class="w2ui-field">
      <label>Es. I</label>
      <div><input name="kwunit" type="text" style="width: 500px"/></div>
    </div>
    <div class="w2ui-field">
      <label>Lokasi</label>
      <div><input name="kwlokasi" type="text" style="width: 500px"/></div>
    </div>
    <div class="w2ui-field">
      <label>Menu</label>
      <div><input name="menu" type="text" style="width: 500px"/></div>
    </div>
  </div>
  <div class="w2ui-buttons">
    <button class="w2ui-btn" name="batal"  id="batal">Batal</button>
    <button class="w2ui-btn" name="simpan" id="simpan">Simpan</button>
  </div>
</script>

<script type="text/javascript">
  function crud(aksi) {
    if (aksi == 'Rekam') cRecid = w2ui['grdtbl'].records.length + 1;
    $('#aksi').val(aksi); $('#recid').val(cRecid);
        
    var json = $("#isian_form input").serialize();
    $.ajax({
      type: "POST", data: json, url: "<?php echo site_url('admin_satudja/crud_user_group') ?>",
      success: function(nil) {
        if (nil != '') {
          var json = JSON.parse(nil);
          if (aksi == 'Rekam') w2ui['grdtbl'].add( json );
          if (aksi == 'Ubah')  w2ui.grdtbl.records[cRecid] = json;
          w2ui.grdtbl.refresh();
        }
      }  
    })
  }

  function hapus( recid ) {
    var id  = table[recid].idusergroup,
        str = "aksi=Hapus&recid=&idusergroup="+ id +"&nmusergroup=&kwdept=&kwunit=&kwlokasi=&menu=";

    $.ajax({ type: "POST", data: str, url: "<?php echo site_url('admin_satudja/crud_user_group') ?>" });
  }
</script>
