<script src="<?=base_url('assets/dist/js/excellentexport.js'); ?>" type="text/javascript"></script>

<style type="text/css">
  .no-border { border: 0; box-shadow: none; }
  .tranparan { background-color: #ecf0f5; margin: 0px}
</style>
<div class="row">
   <div class="col-md-12">
      <div class="box box-widget">
         <div class="box-header" style="margin-bottom: 10px">
            <div class="col-md-9" style="padding:0px">
               <div class="form-group" style="margin: 0px">
                 <input type="hidden" id="aktif">
                 <input type="hidden" id="kdso" value="00">
                 <button onclick="perdit(this.value)" value="00" id="kdso00" class="btn btn-flat btn-primary kdso">DJA</button>
                 <button onclick="perdit(this.value)" value="01" id="kdso01" class="btn btn-flat btn-default kdso">Sekretariat</button>
                 <button onclick="perdit(this.value)" value="02" id="kdso02" class="btn btn-flat btn-default kdso">P APBN</button>
                 <button onclick="perdit(this.value)" value="03" id="kdso03" class="btn btn-flat btn-default kdso">Ekontim</button>
                 <button onclick="perdit(this.value)" value="04" id="kdso04" class="btn btn-flat btn-default kdso">PMK</button>
                 <button onclick="perdit(this.value)" value="05" id="kdso05" class="btn btn-flat btn-default kdso">Polhuk Hankam</button>
                 <button onclick="perdit(this.value)" value="06" id="kdso06" class="btn btn-flat btn-default kdso">PNBP</button>
                 <button onclick="perdit(this.value)" value="07" id="kdso07" class="btn btn-flat btn-default kdso">DSP</button>
                 <button onclick="perdit(this.value)" value="08" id="kdso08" class="btn btn-flat btn-default kdso">HPP</button>
               </div>
             </div>

            <div class="col-md-3" style="padding:0px">
               <div class="form-group" style="margin: 0px">
                 <div class="input-group">
                    <span class="pull-right">
                       <a download="<?php echo 'Pegawai.xls'?>" href="#" onclick="return ExcellentExport.excel(this, 'cetak', 'Pegawai');"><b>XLS</b></a> &nbsp;&nbsp;
                    </span>

                   <span class="input-group-btn"><button id="ultah" onclick="met_ultah()" class="btn btn-flat btn-default"><i class="fa fa-birthday-cake"></i></button></span>
                   <input id="cari" class="form-control" placeholder="ketik kata cari" value="<?php echo $this->session->userdata('cari') ?>">
                 </div>
               </div>
            </div>
         </div>


         <?php $odd = true;
         $grp = explode(";", $this->session->userdata('idusergroup'));
         foreach ($tabel as $kontak) {
           $class1 = implode('#', $kontak) .' 00 '. substr($kontak['kdso'],0,2) .' '. $kontak['ultahbln'];
           $photo  = site_url('files/profiles/_noprofile.png');
           $QRcode = site_url("files/QR/_noQR.png");

           if (file_exists('files/profiles/'. $kontak['nip'] .'.gif')) $photo  = site_url('files/profiles/'. $kontak['nip'] .'.gif');
           if (file_exists('files/QR/'. $kontak['iduser'] .'.png'))    $QRcode = site_url('files/QR/'. $kontak['iduser'] .'.png');

           if ($odd) { $odd = false; ?>
             <div class="col-md-6 <?= $class1 ?>" id="id_<?= $kontak['iduser'] ?>" style="display: none; padding: 3px">
               <div class="box box-widget" style="margin-bottom:3px">
                 <div class="box-body with-border" style="height: 180px;">
                   <div class="user-block-dsw">
                     <img class="img-circle" style="margin-top:2px" src="<?= $photo ?>">
                     <img class="hidden-xs img-qr pull-right" src="<?= $QRcode ?>">
                     <span class="username"><a href="#"><?= $kontak['nmuser'] ?></a> &nbsp;
                     <?php if(in_array('004', $grp)){ ?>
                        <a href="<?= site_url() ?>contact/lps/<?= $kontak['iduser'] ?>"> <i class="text-danger fa fa-key"> </i> </a>
                     <?php } ?>
                     </span>
                     <span class="description text-black">
                       NIP <b><?= $kontak['nip'] ?></b> <br>
                       <?= ucwords(strtolower($kontak['pangkat'])) ?> ( <?= $kontak['golongan'] ?> )
                     </span>
                     <span class="description text-black jabatan"><?= $kontak['jabatan'] ?><br></span>
                     <span class="description text-black text-center birthday bg-info" style="display: none">
                       <h5 style="padding-top: 10px; padding-bottom: 0px; margin-bottom: 0px"><b><?= $kontak['birthday'] ?></h5></b><br>
                     </span>
                     <span class="description text-black"><?= $kontak['nmso'] ?><br></span>
                     <span class="description text-black">
                       e Mail : <?= $kontak['email'] ?><br>
                       No HP : <?= $kontak['nohp'] ?>
                       <span class="pull-right">Intern : <?= $kontak['intern'] ?>&nbsp; - &nbsp; Ekstern : <?= $kontak['ekstern'] ?></span><br>
                     </span>
                   </div>
                 </div>
               </div>
             </div>

           <?php } else { $odd = true; ?>
             <div class="col-md-6 <?= $class1 ?>" id="id_<?= $kontak['iduser'] ?>" style="display: none; padding: 3px">
               <div class="box box-widget" style="margin-bottom:3px">
                 <div class="box-body with-border" style="height: 180px;">
                   <div class="user-block-dsw">
                     <img class="img-circle" style="margin-top:2px" src="<?= $photo ?>">
                     <img class="hidden-xs img-qr pull-right" src="<?= $QRcode ?>">
                     <span class="username"><a href="#"><?= $kontak['nmuser'] ?></a> &nbsp;
                     <?php if(in_array('004', $grp)){ ?>
                        <a href="<?= site_url() ?>contact/lps/<?= $kontak['iduser'] ?>"><i class="text-danger fa fa-key"> </i> </a>
                     <?php } ?>
                     </span>
                     <span class="description text-black">
                       NIP <b><?= $kontak['nip'] ?></b> <br>
                       <?= ucwords(strtolower($kontak['pangkat'])) ?> ( <?= $kontak['golongan'] ?> )
                     </span>
                     <span class="description text-black jabatan"><?= $kontak['jabatan'] ?><br></span>
                     <span class="description text-black text-center birthday bg-info" style="display: none">
                       <h5 style="padding-top: 10px; padding-bottom: 0px; margin-bottom: 0px"><b><?= $kontak['birthday'] ?></h5></b><br>
                     </span>
                     <span class="description text-black"><?= $kontak['nmso'] ?><br></span>
                     <span class="description text-black">
                       e Mail : <?= $kontak['email'] ?><br>
                       No HP : <?= $kontak['nohp'] ?>
                       <span class="pull-right">Intern : <?= $kontak['intern'] ?>&nbsp; - &nbsp; Ekstern : <?= $kontak['ekstern'] ?></span><br>
                     </span>
                   </div>
                 </div>
               </div>
             </div>

           <?php }
         } ?>

      </div>
   </div>
</div>




<script type="text/javascript">
  // SETTING AWAL PAGE
  $( document ).ready(function() {
    var kdso = '<?= substr($this->session->userdata('kdso'), 0, 2) ?>';
    perdit( kdso );
  });


  // PENCARIAN
  $("#cari").keyup( function(event) {
  	// $('id_78492'+).hide();
    var kata = $('#cari').val();
    var kdso = $('#kdso').val();
    // alert(kata);
    // alert(kdso);

    $('.jabatan').show();
    $('.birthday').hide();
    $('.'+kdso).show();

    $('.col-md-6').each(function() {
      var id  = $(this).attr('id');
      var str = $(this).text().toLowerCase();
      if (str.indexOf( kata.toLowerCase() ) < 0) $('#'+id).hide();
    });
  });


  // Tombol DIREKTORAT
  function perdit( kdso ) {
    $('#kdso').val( kdso );
    $('.00').hide(); $('.'+kdso).show();
    $('.kdso').removeClass("btn-primary"); $('.kdso').addClass("btn-default");
    $('#kdso'+kdso).addClass("btn-primary"); $('#cari').val('');
    $('.jabatan').show();
    $('.birthday').hide();
  }


  // Tombol DIREKTORAT
  function met_ultah() {
    var b = ['Jan','Feb','Mar','Apr','Mei','Jun','Jul','Agu','Sep','Okt','Nov','Des'];
    var d = new Date(), kata = 'ultah-' + b[ d.getMonth() ];
    var kdso = $('#kdso').val();

    $('.jabatan').hide();
    $('.birthday').show();
    $('.'+kdso).show();

    $('.col-md-6').each(function() {
      var id  = $(this).attr('id');
      var str = $(this).attr('class');
      if (str.indexOf( kata ) < 0) $('#'+id).hide();
    });
  }
</script>
