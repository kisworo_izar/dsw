<div class="row">
    <div class="col-md-9">
        <div class="box box-widget">
            <div class="box-header with-border">
              <h3 class="box-title">Ubah data User</h3>
            </div>
            <form class="form-horizontal" action="<?php echo site_url('profile/crud') ?>" method="post" role="form">
                <div class="box-body">
                    <div class="form-group">
                        <label class="col-sm-3 control-label">ID User</label>
                        <div class="col-sm-9">
                        <input name="iduser" type="text" class="form-control" id="iduser" readonly="readonly" value="<?php echo $this->session->userdata('iduser') ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">NIP</label>
                        <div class="col-sm-9">
                        <input type="text" class="form-control" id="nip" readonly="readonly" value="<?php echo $this->session->userdata('nip') ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-3 control-label">Nama</label>
                        <div class="col-sm-9">
                        <input class="form-control" id="nmuser" readonly="readonly" value="<?php echo $this->session->userdata('nmuser') ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-3 control-label">Nama Alias</label>
                        <div class="col-sm-9">
                        <input name="nmalias" class="form-control" id="nmalias" placeholder="Nama Alias" value="<?php echo $user['nmalias'] ?>">
                        </div>

                    </div>
                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-3 control-label">Password Lama</label>
                        <div class="col-sm-9">
                        <input name="password0" type="password" class="form-control" id="password0" placeholder="Password Lama" >
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-3 control-label">Password Baru</label>
                        <div class="col-sm-9">
                        <input name="password" type="password" class="form-control" id="password" placeholder="Password Baru" >
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-3 control-label">Konfirmasi Password</label>
                        <div class="col-sm-9">
                        <input name="password1" type="password" class="form-control" id="password1" placeholder="Konfirmasi Password Baru" >
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-3 control-label">e Mail</label>
                        <div class="col-sm-9">
                        <input name="email" type="email" class="form-control" id="email" placeholder="Alamat email" value="<?php echo $user['email'] ?>">
                        <span class="small text-info">* Fasilitas <b>Lupa Password</b> akan mengirimkan password kealamat email.</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-3 control-label">Handphone</label>
                        <div class="col-sm-9">
                        <input name="nohp" class="form-control" id="email" placeholder="Nomor Handphone" value="<?php echo $user['nohp'] ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-3 control-label"></label>
                        <div class="col-sm-9">
                           <button type="submit" class="btn btn-flat btn-default">Batal </button>
                          <button type="submit" class="btn btn-flat btn-primary pull-right">Simpan </button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="col-md-3">
        <!-- Profile Image -->
        <div class="box box-widget">
            <div class="box-body with-border">
                <br>
                <?php
                    $foto_profile="files/profiles/_noprofile.png";
                    if (file_exists("files/profiles/".$this->session->userdata('nip').".gif")) {$foto_profile =  "files/profiles/".$this->session->userdata('nip').".gif";}
                ?>
                <img src="<?php echo site_url($foto_profile); ?>" class="profile-user-img img-responsive img-circle img-bordered-sm" alt="User Image" />

                <h3 class="profile-username text-center"><?php echo $this->session->userdata('nmuser') ?></h3>
                <?php if ($user['golongan']): ?>
                   <p class="text-muted text-center"><?php echo ucwords(strtolower($user['pangkat'])) ?> ( <?php echo $user['golongan'] ?> )</p>
                <?php endif; ?>

                <p class="text-muted text-center"><?php echo $this->session->userdata('jabatan') ?></p>



            </div>
        </div>

        <div class="box box-widget">
            <!-- <div class="box-header with-border">
              <h3 class="box-title">About Me</h3>
            </div> -->
            <div class="box-body">
            <i class="fa fa-book margin-r-5"></i> Alias
            <p style="padding-left: 20px"><strong><?php echo $user['nmalias'] ?></strong></p>
            <hr style="margin: 3px 0px">
            <i class="fa fa-envelope-o margin-r-5"></i> Email
            <p style="padding-left: 20px"><strong><?php echo $user['email'] ?></strong></p>
            <hr style="margin: 3px 0px">
            <i class="fa fa-mobile margin-r-5"></i> No Selular
            <p style="padding-left: 20px"><strong><?php echo $user['nohp'] ?></strong></p>
            <hr style="margin: 3px 0px">
            <i class="fa fa-phone margin-r-5"></i> No Ekstern
            <p style="padding-left: 20px"><strong><?php echo $user['ekstern'] ?></strong></p>
            <hr style="margin: 3px 0px">
            <i class="fa fa-phone margin-r-5"></i> No Intern
            <p style="padding-left: 20px"><strong><?php echo $user['intern'] ?></strong></p>
            <hr style="margin: 3px 0px">
            </div>
        </div>
        <!-- /.box
    </div>
</div>
