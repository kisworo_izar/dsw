<div class="row">
    <div class="col-md-9">
        <div class="box box-widget">
            <div class="box-header with-border">
              <h3 class="box-title">Ubah data User</h3>
            </div>
            <form class="form-horizontal">
                <div class="box-body">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">ID User</label>
                        <div class="col-sm-10">
                        <input type="text" class="form-control" id="iduser" value="<?php echo $this->session->userdata('iduser') ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">NIP</label>
                        <div class="col-sm-10">
                        <input type="text" class="form-control" id="nip" value="<?php echo $this->session->userdata('nip') ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-2 control-label">Nama</label>
                        <div class="col-sm-10">
                        <input class="form-control" id="nmuser" value="<?php echo $this->session->userdata('nmuser') ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-2 control-label">Nama Alias</label>
                        <div class="col-sm-10">
                        <input class="form-control" id="nmalias" placeholder="Nama Alias">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-2 control-label">Password</label>
                        <div class="col-sm-10">
                        <input type="password" class="form-control" id="password" placeholder="Password">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-2 control-label">e Mail</label>
                        <div class="col-sm-10">
                        <input type="email" class="form-control" id="email" placeholder="Alamat email">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-2 control-label">Handphone</label>
                        <div class="col-sm-10">
                        <input class="form-control" id="email" placeholder="Nomor Handphone">
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-default">Batal </button>
                    <button type="submit" class="btn btn-info pull-right">Simpan </button>
                </div>
            </form>
        </div>
    </div>

    <div class="col-md-3">
        <!-- Profile Image -->
        <div class="box box-widget">
            <div class="box-body with-border">
                <br>
                <?php
                    $foto_profile="files/profiles/_noprofile.png";
                    if (file_exists("files/profiles/".$this->session->userdata('nip').".gif")) {$foto_profile =  "files/profiles/".$this->session->userdata('nip').".gif";}
                ?>
                <img src="<?php echo site_url($foto_profile); ?>" class="profile-user-img img-responsive img-circle img-bordered-sm" alt="User Image" />

                <h3 class="profile-username text-center"><?php echo $this->session->userdata('nmuser') ?></h3>
                <p class="text-muted text-center"><?php echo $this->session->userdata('jabatan') ?></p>
                <hr>
                    <!-- <strong><i class="fa fa-phone margin-r-5"></i> Kontak</strong> -->
                    <p class="text-muted">
                        Alias<a class="pull-right">@Alias</a>
                    </p>
                    <p class="text-muted">
                        Email<a class="pull-right">mail@depkeu.go.id</a>
                    </p>
                    <p class="text-muted">
                        Handphone<a class="pull-right">08123456789</a>
                    </p>
                    <p class="text-muted">
                        Ekstern<a class="pull-right">02134357120</a>
                    </p>
                    <p class="text-muted">
                        Intern<a class="pull-right">8120</a>
                    </p>
            </div>
        </div>

        <!-- About Me Box -->
        <!-- <div class="box box-widget">
            <div class="box-header with-border">
              <h3 class="box-title">About Me</h3>
            </div>
            <div class="box-body">
            <strong><i class="fa fa-book margin-r-5"></i> Pendidikan</strong>
            <p class="text-muted">
                B.S. in Computer
            </p>
            <hr>
            <strong><i class="fa fa-map-marker margin-r-5"></i> Alamat</strong>
            <p class="text-muted">Malibu, California</p>
            <hr>
            <strong><i class="fa fa-file-text-o margin-r-5"></i> Catatan</strong>
            </div>
        </div> -->
        <!-- /.box -->
    </div>
</div>
