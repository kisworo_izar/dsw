<div class="box-header with border">
  <div class="wel wel-sm">
    <div class="input-group" style="height:35px">
      Tgl dan No. ST
    </div>
  </div>
</div>

<div class="box-body table-responsive" style="padding-top:0px">
  <table id="iGrid" class="table table-hover table-bordered">
    <thead>
      <tr style="background: #3A9131; color: #fff;">
        <th colspan="2" class="text-center">Direktorat / Sub Direktorat / Nama </th>
        <th width="150px" class="text-center">NIP</th>
        <th width="300px" class="text-center">Jabatan</th>
        <th width=" 60px" class="text-center">Golongan</th>
        <th width=" 10px" class="text-center"></th>
        <th width=" 10px" class="text-center"></th>
      </tr>
    </thead>

          <tbody>
            <?php if ($lamp) {
              foreach ($lamp as $row) { ?>
                  <tr class="trPeg" id="<?php echo $row['nip'] ?>" align="justify" />
                    <td class="tdPeg"></td>
                    <td class="tdPeg"><?php echo $row['nmuser'] ?></td>
                    <td class="tdPeg text-center"><?php echo $row['nip'] ?></td>
                    <td class="tdPeg"><?php echo $row['jabatan'] ?></td>
                    <td class="tdPeg text-center"><?php echo $row['golongan'] ?></td>
                    <td class="tdPeg"></td>
                    <td class="tdPeg"><input type="checkbox" name="edit[]" value="<?php echo $row['nip'] ?>"></td>
                  </tr>
                <?php
              }
            } else { echo '<tr align="center" class="text-bold"><td colspan="6">Tidak ada data ... </td></tr>'; } ?>
          </tbody>

  </table>
</div>
