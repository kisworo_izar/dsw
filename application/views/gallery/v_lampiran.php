<div class="row">
  <div class="col-xs-12">
    <div class="box box-widget">

      <div class="box-header with border">
        <div class="wel wel-sm">
 
          <div class="pull-left">
            <div class="input-group">
              <span class="input-group-btn">
                <button id="btn_data" onclick="ButtonTab('data')" class="btn <?php if ($tab=='data') echo 'btn-warning'; ?>">Data Lampiran</button>
              </span>
              <span class="input-group-btn">
                <button id="btn_refr" onclick="ButtonTab('refr')" class="btn <?php if ($tab=='refr') echo 'btn-warning'; ?>">Referensi</button>
              </span>
              <span class="input-group-btn">
                    <button title="Cetak Surat Tugas" type="button" id="cetak" class="btn btn-info btn-flat"><i class="fa fa-file-pdf-o"></i></button>
              </span>
            </div>
              </div>
          </div>

          <div class="pull-right" id="menu-data" style="display:<?php if ($tab=='refr') echo 'none'; ?>;">
            <div class="input-group">
              <span class="input-group-btn">
                <button type="submit" name="search" value="search" class="btn btn-default btn-flat"><i class="fa fa-plus"></i></button>
              </span>
              <span class="input-group-btn">
                <button type="submit" name="search" value="clear" class="btn btn-default btn-flat"><i class="fa fa-minus"></i></button>
              </span>
            </div>
          </div>
          <div class="pull-right" id="menu-refr" style="display:<?php if ($tab=='data') echo 'none'; ?>;">
            <div class="input-group">
              <input class="form-control" name="cari" type="text" value="<?php echo $this->session->userdata('cari') ?>">
              <span class="input-group-btn">
                <button type="submit" name="search" value="search" class="btn btn-default btn-flat"><i class="fa fa-search"></i></button>
              </span>
              <span class="input-group-btn">
                <button type="submit" name="search" value="clear" class="btn btn-default btn-flat"><i class="fa fa-times-circle"></i></button>
              </span>

          </div>

        </div>
      </div>

      <div id="tab-data" style="display:<?php if ($tab=='refr') echo 'none'; ?>;">
        <?php $this->view('gallery/v_lampiran_data'); ?>
      </div>

      <div id="tab-refr" style="display:<?php if ($tab=='data') echo 'none'; ?>;">
        <?php $this->view('gallery/v_lampiran_referensi'); ?>
      </div>

    </div>
  </div>
</div>

<script type="text/javascript">
  function ButtonTab( nil ) {
    if (nil=='data') {
      $("#tab-data").css("display", "inline");  $("#tab-refr").css("display", "none");
      $("#menu-data").css("display", "inline"); $("#menu-refr").css("display", "none");
      $("#btn_data").removeClass("btn-default").addClass("btn-warning").button('refresh');
      $("#btn_refr").removeClass("btn-warning").addClass("btn-default").button('refresh');
    } else {
      $("#tab-data").css("display", "none");  $("#tab-refr").css("display", "inline");
      $("#menu-data").css("display", "none"); $("#menu-refr").css("display", "inline");
      $("#btn_data").removeClass("btn-warning").addClass("btn-default").button('refresh');
      $("#btn_refr").removeClass("btn-default").addClass("btn-warning").button('refresh');
    } 
  }
</script>