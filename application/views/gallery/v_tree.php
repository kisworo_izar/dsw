<script src="<?=base_url('assets/plugins/jQueryUI/jquery-ui.1.11.4.min.js'); ?>" type="text/javascript"></script>

<style media="screen">
	.trE2  { font: bold 13px "Times New Roman"; color: #000; background: #DFF0D8 !important; }
  .tdE2  { border-bottom: 1px solid #3A9131 !important; }
  .trE3  { font: bold 11px "Times New Roman"; color: #000 !important; }
  .tdE3  { padding: 5px 5px 5px 15px !important; }
  .NmPeg { padding: 0px 0px 0px 10px !important; }
  .tdPeg { padding: 0px 0px 0px 2px !important; }
  .jab31 { font-style: italic; font-weight: bold !important; }
  .jab41 { font-weight: bold !important; }

  #iGrid .ui-selecting { background: #FECA40; }
  #iGrid .ui-selected { background: #F39814; color: black; }
</style>

<div class="row">
  <div class="col-xs-12">
    <div class="box box-widget">

      <div class="box-header with border">
        <div class="wel wel-sm">
          <form id="iForm" class="form-inline" role="form" action="<?php echo site_url('approval/persetujuan') ?>" method="post">
            <div class="form-group pull-right">
              <div class="input-group">
                <input type="hidden" name="nmfunction" value="grid">
                <input class="form-control" name="cari" type="text" value="<?php echo $this->session->userdata('cari') ?>">
                <span class="input-group-btn">
                  <button type="submit" name="search" value="search" class="btn btn-default btn-flat"><i class="fa fa-search"></i></button>
                </span>
                <span class="input-group-btn">
                  <button type="submit" name="search" value="clear" class="btn btn-default btn-flat"><i class="fa fa-times-circle"></i></button>
                </span>
              </div>
            </div>
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-btn">
                  <input type="hidden" name="kddata" id="kddata" value="">
                  <input type="hidden" name="kdstatus" id="kdstatus" value="">
                  <button type="submit" name="action" value="" class="btn btn-warning">Menu 1</button>
                </span>
              </div>
              <div class="input-group">
                <span class="input-group-btn">
                  <button type="submit" name="action" value="" class="btn btn-warning">Menu 2</button>
                </span>
              </div>
            </div>
          </form>
        </div>
      </div>

      <div class="box-body table-responsive" style="padding-top:0px">
        <table id="iGrid" class="table table-hover table-bordered">
          <thead>
            <tr style="background: #3A9131; color: #fff;">
              <th colspan="2" class="text-center">Direktorat / Sub Direktorat / Nama </th>
              <th width="150px" class="text-center">NIP</th>
              <th width="300px" class="text-center">Jabatan</th>
              <th width=" 60px" class="text-center">Golongan</th>
              <th width=" 10px" class="text-center"></th>
              <th width=" 10px" class="text-center"></th>
            </tr>
          </thead>

          <tbody>
            <?php if ($dja) {
              foreach ($dja as $row) { 
                if ($row['level']=='1') { ?>
                  <tr class="trE2" id="<?php echo $row['es2'] ?>" align="justify" onclick="show_subdit(this.id)" />
                    <td class="tdE2"><label class="plus_es2" id="<?php echo 'plus_es2'.$row['es2'] ?>"><i class="fa fa-plus-square-o"></i></label></td>
                    <td class="tdE2"><?php echo $row['nmso'] ?>
                    </td>
                    <td class="tdE2"></td>
                    <td class="tdE2"></td>
                    <td class="tdE2"></td>
                    <td class="tdE2"></td>
                    <td class="tdE2"></td>
                  </tr>

                <?php }
                if ($row['level']=='2') { ?>
                  <tr class="trE3 text-bold <?php echo 'es2_'.$row['es2'] ?>" id="<?php echo $row['es3'] ?>" align="justify" onclick="ajax_eselon3(this.id)" style="display:none;" >
                    <td class="tdE3"><label class="plus_es3" id="<?php echo 'plus_es3'.$row['es3'] ?>"><i class="fa fa-plus"></i></label></td>
                    <td class="tdE3"><?php echo $row['nmso'] ?></td>
                    <td class="tdE3"></td>
                    <td class="tdE3"></td>
                    <td class="tdE3"></td>
                    <td class="tdE3"></td>
                    <td class="tdE3"></td>
                  </tr>
                <?php } 
              }
            } else { echo '<tr align="center" class="text-bold"><td colspan="6">Tidak ada data ... </td></tr>'; } ?>
          </tbody>

        </table>
      </div>

    </div>
  </div>
</div>

<script type="text/javascript">
  function show_subdit( idkey ) {
    $(".trE3").hide();
    $(".trPeg").hide();
    $(".es2_"+idkey).show();
    $(".plus_es2").html('<i class="fa fa-plus-square-o"></i>');
    $("#plus_es2"+idkey).html('<i class="fa fa-minus-square-o"></i>');
  }
</script>

<script type="text/javascript">
  function ajax_eselon3( idkey ) {
    $.ajax({
      url : "<?php echo site_url('tree/ajax_eselon3') ?>",
      type: "POST",
      data: { 'idkey': idkey },
    })
      .done( function (msg) {
        $('table tr.child').remove();
        $('#'+idkey).after(msg);
        // alert(msg);
      })
  }
</script>

<script type="text/javascript">
  $(function() {

    // Tandai ROW yang dipilih dalam iGRID
    $( "#iGrid tbody" ).selectable({
      filter: ":not(td)",
        create: function( e, ui ) {
        iGridLink( $() );
      },
      selected: function( e, ui ) {
        var widget = $(this).find('.ui-selected');
        $(ui.unselected).addClass("info");
        iGridLink( widget );
      },
      unselected: function( e, ui ) {
        $(ui.unselected).removeClass("info");
        var widget = $(this).find('.ui-selected');
        iGridLink( widget );
      }
    });

    // Ambil Nilai ROW pada iGRID yang dipilih per-ID sesuai kolom CHILD
    function iGridLink( $selectees ) {
      selected = $.makeArray( $selectees.filter( ".ui-selected" ) );
        selected.reduce( function( a, b ) {
          document.getElementById("kddata").value= $(b).children( "td:nth-child(1)" ).attr('id');
        }, 0
      );
     }

  });
</script>
