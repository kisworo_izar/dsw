<!-- Google Fonts -->
<link href="https://fonts.googleapis.com/css?family=Roboto:400,700&amp;subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
<link href="<?=base_url();?>assets/plugins/materialicons/materialicons.css" rel="stylesheet" type="text/css">
<!-- bootstrapValidator -->
<link rel="stylesheet" href="<?=base_url();?>assets/plugins/bootstrapvalidator/css/bootstrapValidator.min.css"/>
<!-- SweetAlert2 -->
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
<!-- Waves Effect Css -->
<link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/plugins/node-waves/waves.css">
<!-- Animation Css -->
<link href="<?=base_url();?>assets/plugins/animate-css/animate.css" rel="stylesheet" />
<!-- Bootstrap DatePicker Css -->
<link href="<?=base_url();?>assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.css" rel="stylesheet" />
<!-- Bootstrap Select Css -->
<link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/plugins/bootstrap-select/css/bootstrap-select.css">
<!-- Dropify -->
<link href="<?=base_url();?>assets/plugins/dropify/css/dropify.min.css" rel="stylesheet">
<!-- AdminBSB Custom Css -->
<link href="<?=base_url();?>assets/plugins/adminbsb/style.css" rel="stylesheet">

<style type="text/css">
  form .dropify-wrapper p {
    text-align: center;
  }

  .swal2-radio [type="radio"]:not(:checked),
  .swal2-radio [type="radio"]:checked {
    position: unset;
    opacity: 1;
  }

  .form-group .form-line.focused .form-label {
    top: -15px;
  }

  .bs-searchbox .form-control {
    width: 88%;
  }

  .capitalise {
    text-transform:capitalize;
  }

  .bootstrap-select .dropdown-menu {
    width: 100%;
  }

  .bootstrap-select .dropdown-menu li small {
    white-space: normal;
  }

</style>

<div class="row">
  <div class="col-md-12">
    <div class="card" id="menu" hidden>

    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    <div class="m-b-30">
      <button type="button" class="btn bg-blue waves-effect" data-toggle="modal" data-target="#wfh-filter-modal">
        <i class="material-icons">filter_list</i>
        <span>FILTER TAMPILAN</span>
      </button>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-12" id="wfh-isworking">

  </div>
</div>

<div class="row">
  <div class="col-md-12" id="wfh-list">

  </div>
</div>

<div class="row">
  <div class="col-md-12" id="wfh-endpage">

  </div>
</div>

<div class="row" id="st-load" hidden>
  <div class="col-xs-12 align-center">
    <div class="preloader">
      <div class="spinner-layer pl-teal">
        <div class="circle-clipper left">
          <div class="circle"></div>
        </div>
        <div class="circle-clipper right">
          <div class="circle"></div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- WFH - START MODAL -->
<div class="modal fade" id="wfh-start-modal" tabindex="-1" role="dialog" style="display: none;">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="card">
        <div class="header">
          <div class="row clearfix">
            <div class="col-xs-12">
              <h2>WFH/WFO</h2>
            </div>
          </div>
        </div>
        <div class="body">
          <form id="wfh-start-form">
            <br>
            <div class="row clearfix">
              <div class="col-sm-12">
                <label class="form-label">Kerja dari rumah (WFH) atau kantor (WFO)?</label>
                <div class="form-group form-float">
                  <div class="radio">
                    <input name="type" value="wfh" type="radio" id="type_radio_1" class="radio-col-blue">
                    <label for="type_radio_1">WFH</label>
                    <input name="type" value="wfo" type="radio" id="type_radio_2" class="radio-col-blue">
                    <label for="type_radio_2">WFO</label>
                  </div>
                </div>
              </div>
            </div>
            <div class="row clearfix">
              <div class="col-sm-12">
                <label class="form-label">Bagaimana Kondisi Kesehatan Anda?</label>
                <div class="form-group form-float">
                  <div class="radio">
                    <input name="healthtype" value="1" type="radio" id="radio_1" class="radio-col-green">
                    <label for="radio_1">SEHAT</label>
                    <input name="healthtype" value="2" type="radio" id="radio_2" class="radio-col-orange">
                    <label for="radio_2">KURANG FIT</label>
                    <input name="healthtype" value="3" type="radio" id="radio_3" class="radio-col-deep-orange">
                    <label for="radio_3">SAKIT RINGAN</label>
                    <input name="healthtype" value="4" type="radio" id="radio_4" class="radio-col-red">
                    <label for="radio_4">SAKIT BERAT</label>
                  </div>
                </div>
              </div>
            </div>
            <div class="row clearfix">
              <div class="col-sm-12">
                <label class="form-label">Keterangan</label>
                <div class="form-group form-float">
                  <div class="form-line">
                    <textarea name="health" type="text" class="form-control no-resize" rows="5" placeholder="Tulis keterangan bila tidak sehat dan sampaikan kondisi kesehatan keluarga Anda"></textarea> 
                  </div>
                </div>
              </div>
            </div>
            <button type="submit" class="btn btn-primary m-t-15 waves-effect" id="wfh-start-form-save">MULAI KERJA</button>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- WFH MODAL -->
<div class="modal fade" id="wfh-modal" tabindex="-1" role="dialog" style="display: none;">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="card">
        <div class="header">
          <div class="row clearfix">
            <div class="col-xs-12 col-sm-6">
              <h2>Ubah Kerja</h2>
            </div>
          </div>
        </div>
        <div class="body">
          <form id="wfh-form">
            <br>
            <input name="id" type="text" hidden>
            <div class="row clearfix">
              <div class="col-sm-3">
                <div class="form-group form-float">
                  <div class="form-line">
                    <select name="type" class="form-control show-tick capitalise" data-live-search="true" title="Pilih Jenis Kerja">
                      <option value="wfo">WFO</option>
                      <option value="wfh">WFH</option>
                    </select>
                    <label class="form-label">Jenis Kerja</label>
                  </div>
                </div>
              </div>
              <div class="col-sm-6">
                <div class="form-group form-float">
                  <div class="form-line">
                    <select name="atasan" class="form-control show-tick capitalise" data-live-search="true" title="Pilih Atasan">
                    </select>
                    <label class="form-label">Atasan</label>
                  </div>
                </div>
              </div>
              <div class="col-sm-3">
                <div class="form-group form-float">
                  <div class="form-line">
                    <select name="healthtype" class="form-control show-tick capitalise" data-live-search="true" title="Pilih Status Kesehatan">
                      <option value="1">Sehat</option>
                      <option value="2">Kurang Fit</option>
                      <option value="3">Sakit Ringan</option>
                      <option value="4">Sakit Berat</option>
                    </select>
                    <label class="form-label">Status Kesehatan</label>
                  </div>
                </div>
              </div>
            </div>
            <div class="row clearfix">
              <div class="col-sm-12">
                <div class="form-group form-float">
                  <div class="form-line">
                    <textarea name="health" type="text" class="form-control no-resize" rows="5" placeholder="Tulis keterangan bila tidak sehat dan sampaikan kondisi kesehatan keluarga Anda"></textarea> 
                    <label class="form-label">Keterangan Kesehatan</label>
                  </div>
                </div>
              </div>
            </div>
            <button type="submit" class="btn btn-primary m-t-15 waves-effect" id="wfh-form-save">SIMPAN</button>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- TASK MODAL -->
<div class="modal fade" id="task-modal" tabindex="-1" role="dialog" style="display: none;">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="card">
        <div class="header">
          <div class="row clearfix">
            <div class="col-xs-12 col-sm-6">
              <h2></h2>
            </div>
          </div>
        </div>
        <div class="body">
          <form id="task-form">
            <br>
            <input name="id" type="text" hidden>
            <input name="filename" type="text" hidden>
            <input name="wfh_id" type="text" hidden>
            <input name="nip" type="text" hidden>
            <div class="row clearfix">
              <div class="col-sm-12">
                <div class="form-group form-float">
                  <div class="form-line">
                    <input name="title" type="text" class="form-control">
                    <label class="form-label">Nama Pekerjaan</label>
                  </div>
                </div>
              </div>
            </div>
            <div class="row clearfix">
              <div class="col-sm-12">
                <div class="form-group form-float">
                  <div class="form-line">
                    <input name="dokumen" type="file" class="dropify" data-max-file-size-preview="2M" />
                  </div>
                </div>
              </div>
            </div>
            <button type="submit" class="btn btn-primary m-t-15 waves-effect" id="task-form-save">SIMPAN</button>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- WFH - FILTER MODAL -->
<div class="modal fade" id="wfh-filter-modal" tabindex="-1" role="dialog" style="display: none;">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="card">
        <div class="header">
          <div class="row clearfix">
            <div class="col-xs-12 col-sm-6">
              <h2>FILTER TAMPILAN DAFTAR KERJA</h2>
            </div>
          </div>
        </div>
        <div class="body">
          <form id="wfh-filter-form">
            <br>
            <div id="wfh-filter-form-bymenu"></div>
            <div class="row clearfix">
              <div class="col-sm-3">
                <div class="form-group form-float">
                  <div class="form-line">
                    <select name="type" class="form-control show-tick capitalise" data-live-search="true" title="Pilih Jenis Kerja">
                      <option value="-1">Semua</option>
                      <option value="wfo">WFO</option>
                      <option value="wfh">WFH</option>
                    </select>
                    <label class="form-label">Jenis Kerja</label>
                  </div>
                </div>
              </div>
              <div class="col-sm-3">
                <div class="form-group form-float">
                  <div class="form-line">
                    <select name="status" class="form-control show-tick capitalise" data-live-search="true" title="Pilih Status Kerja">
                      <option value="-1">Semua</option>
                      <option value="started">Sedang Bekerja</option>
                      <option value="finished">Verifikasi Atasan</option>
                      <option value="verified">Validasi Surat Tugas</option>
                      <option value="validated">Ditetapkan</option>
                      <option value="notverified">Ditolak</option>
                    </select>
                    <label class="form-label">Status Kerja</label>
                  </div>
                </div>
              </div>
              <div class="col-sm-3">
                <div class="form-group form-float">
                  <div class="form-line">
                    <select name="attendancestatus" class="form-control show-tick capitalise" data-live-search="true" title="Pilih Status Kehadiran">
                      <option value="-1">Semua</option>
                      <option value="HN">Hadir Normal (HN)</option>
                      <option value="T">Terlambat (TL)</option>
                      <option value="P">Pulang Sebelum Waktunya (PSW)</option>
                    </select>
                    <label class="form-label">Status Kehadiran</label>
                  </div>
                </div>
              </div>
              <div class="col-sm-3">
                <div class="form-group form-float">
                  <div class="form-line">
                    <select name="healthtype" class="form-control show-tick capitalise" data-live-search="true" title="Pilih Status Kesehatan">
                      <option value="-1">Semua</option>
                      <option value="1">Sehat</option>
                      <option value="2">Kurang Fit</option>
                      <option value="3">Sakit Ringan</option>
                      <option value="4">Sakit Berat</option>
                    </select>
                    <label class="form-label">Status Kesehatan</label>
                  </div>
                </div>
              </div>
            </div>
            <div class="row clearfix">
              <div class="col-sm-6">
                <div class="form-group form-float">
                  <div class="form-line">
                    <input name="startdate" type="text" class="form-control datepicker" autocomplete="off">
                    <label class="form-label">Dari Tanggal</label>
                  </div>
                </div>
              </div>
              <div class="col-sm-6">
                <div class="form-group form-float">
                  <div class="form-line">
                    <input name="enddate" type="text" class="form-control datepicker" autocomplete="off">
                    <label class="form-label">Sampai Tanggal</label>
                  </div>
                </div>
              </div>
            </div>
            <button type="submit" class="btn btn-primary m-t-15 waves-effect" id="wfh-filter-form-save">FILTER</button>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- imagesloaded -->
<script src="<?=base_url();?>assets/plugins/imagesloaded/imagesloaded.pkgd.min.js"></script>
<!-- Masonry -->
<script src="<?=base_url();?>assets/plugins/masonry/masonry.pkgd.min.js"></script>
<!-- bootstrapValidator -->
<script type="text/javascript" src="<?=base_url();?>assets/plugins/bootstrapvalidator/js/bootstrapValidator.js"></script>
<script type="text/javascript" src="<?=base_url();?>assets/plugins/bootstrapvalidator/js/language/id_ID.js"></script>
<!-- jquery.serializeObject -->
<script type="text/javascript" src="<?=base_url();?>assets/plugins/jQuery.serializeObject/jquery.serializeObject.min.js"></script>
<!-- Bootstrap Select Css -->
<script type="text/javascript" src="<?=base_url();?>assets/plugins/bootstrap-select/js/bootstrap-select.min.js"></script>
<!-- Slimscroll Plugin Js -->
<script src="<?=base_url();?>assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- JQuery Steps Plugin Js -->
<script src="<?=base_url();?>assets/plugins/jquery-steps/jquery.steps.js"></script>
<!-- Bootstrap Notify Plugin Js -->
<script src="<?=base_url();?>assets/plugins/bootstrap-notify/bootstrap-notify.js"></script>
<!-- Waves Effect Plugin Js -->
<script type="text/javascript" src="<?=base_url();?>assets/plugins/node-waves/waves.js"></script>
<!-- Bootstrap Datepicker Plugin Js -->
<script src="<?=base_url();?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<!-- Dropify -->
<script src="<?=base_url();?>assets/plugins/dropify/js/dropify.min.js"></script>
<!-- EasyTimer.js -->
<script src="<?=base_url();?>assets/plugins/easytimer/easytimer.min.js"></script>
<!-- jQuery LoadingOverlay -->
<script src="<?=base_url();?>assets/plugins/gasparesganga-jquery-loading-overlay/loadingoverlay.min.js"></script>
<!-- AdminBSB Custom Js -->
<script src="<?=base_url();?>assets/plugins/adminbsb/admin.js"></script>
<!-- <script src="<?=base_url();?>assets/plugins/adminbsb/demo.js"></script> -->

<script type="text/javascript">
  const 
  NIP = "<?= $this->session->userdata('nip') ?>",
  API_SERVER = "<?= $this->config->item('api_server') ?>";
  
  $(function(){

    /* WFH - START FORM */
    $('#wfh-start-form')
    /* WFH - START FORM - VALIDATOR */
    .bootstrapValidator({
      excluded: [':disabled', ':hidden'],
      feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
        type: {
          validators: {
            notEmpty: {}
          }
        },
        health: {
          validators: {
            notEmpty: {}
          }
        },
        healthtype: {
          validators: {
            notEmpty: {}
          }
        }
      }
    })      
    .on('success.form.bv', function(e) {
      e.preventDefault();
      $('#wfh-start-modal').modal('toggle');

      /*CREATE & UPDATE WFH - START*/
      var data = $('#wfh-start-form').serializeObject();
      data.nip = NIP;

      $.ajax({
        url: API_SERVER + "wfh/start",
        data: data,
        type: 'POST',
        dataType: 'json',
        beforeSend: function() {
          $('#wfh-isworking .card').LoadingOverlay("show", {
            image       : "",
            text        : "Memulai..."
          })
        },
        complete: function() {
          $('#wfh-isworking .card').LoadingOverlay("hide")
        },
        success: function(result) {

          if (result.status == 200) {
            $('#menu .active').click()
            Swal.fire({ title: 'BERHASIL', text: result.values, type: 'success' })
          } else {
            Swal.fire({ title: 'GAGAL', text: result.values, type: 'error' })
          }

        },
        error: function(result) {
          Swal.fire({ title: 'ERROR', type: 'error' })
        }
      });

    })

    /* WFH FORM */
    $('#wfh-form')
    .find('select')
    .selectpicker()
    .change(function(e) {
      $(this).closest('.form-line').addClass('focused')
    })
    .end()
    /* WFH FORM - VALIDATOR */
    .bootstrapValidator({
      excluded: [':disabled', ':hidden'],
      feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
        atasan: {
          validators: {
            notEmpty: {}
          }
        },
        healthtype: {
          validators: {
            notEmpty: {}
          }
        },
        health: {
          validators: {
            notEmpty: {}
          }
        }
      }
    })      
    .on('success.form.bv', function(e) {
      e.preventDefault();
      $('#wfh-modal').modal('toggle');

      /* UPDATE WFH */
      var data = $('#wfh-form').serializeObject();
      data.updatedby = NIP;

      $.ajax({
        url: `${API_SERVER}wfh`,
        data: data,
        type: 'PUT',
        dataType: 'json',
        beforeSend: function() {
          $(`#wfh${data.id}`).LoadingOverlay("show", {
            image       : "",
            text        : "Menyimpan..."
          })
        },
        complete: function() {
          $(`#wfh${data.id}`).LoadingOverlay("hide")
        },
        success: function(result) {

          if (result.status == 200) {
            readWFHById(data.id, $('#menu button.active').attr('data-menu'))
            showNotification('bg-green', `BERHASIL - ${result.values}`)
          } else {
            showNotification('bg-orange', `GAGAL - ${result.values}`)
          }

        },
        error: function(result) {
          showNotification('bg-red', `ERROR - Simpan Pekerjaan`)
        }
      });

    })

    /* WFH MODAL */
    $('#wfh-modal').on('shown.bs.modal', function (event) {

      $('#wfh-form').bootstrapValidator('resetForm', true);

      var wfh = JSON.parse(decodeURIComponent( $(event.relatedTarget).data('wfh') ));

      $('#wfh-form [name="id"]').val(wfh.id)
      $('#wfh-form [name="type"]').selectpicker('val', wfh.type)
      $('#wfh-form [name="atasan"]').selectpicker('val', wfh.atasan)
      $('#wfh-form [name="healthtype"]').selectpicker('val', wfh.healthtype)
      $('#wfh-form [name="health"]').val(wfh.health).parent().addClass('focused')

    })

    /* WFH-FINISH */
    $('#wfh-list').on('click', '.wfh-finish', function(e) {

      let parent = $(this).attr('data-parent');

      e.preventDefault();

      Swal.fire({
        title: 'Sudah selesai bekerja hari ini?',
        type: 'warning',
        confirmButtonText: 'Ya',
        showCancelButton: true,
        cancelButtonText: 'Tidak',
        focusCancel:true,
        reverseButtons: true,
        showLoaderOnConfirm: true,
        preConfirm: () => {}
      }).then((result) => {
        if (result.value) {
          $.ajax({
            url: API_SERVER + "wfh/finish",
            data: { nip: NIP },
            type: 'POST',
            dataType: 'json',
            beforeSend: function() {
              $(parent).LoadingOverlay("show", {
                image       : "",
                text        : "Menyelesaikan..."
              })
            },
            complete: function() {
              $(parent).LoadingOverlay("hide")
            },
            success: function(result) {
              if (result.status == 200) {
                readWFHById(parent.replace("#wfh", ""), $('#menu button.active').attr('data-menu'))
                Swal.fire({ title: 'BERHASIL', text: result.values, type: 'success' })
              } else {
                Swal.fire({ title: 'GAGAL', text: result.values, type: 'error' })
              }
            },
            error: function(result) {
              Swal.fire({ title: 'ERROR', type: 'error' })
            }
          });
        }
      })

    });

    /* TASK FORM */
    $('#task-form')
    /* TASK FORM - VALIDATOR */
    .bootstrapValidator({
      excluded: [':disabled', ':hidden'],
      feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
        title: {
          validators: {
            notEmpty: {}
          }
        },
        dokumen: {
          validators: {
            notEmpty: {}
          }
        }
      }
    })      
    .on('success.form.bv', function(e) {
      e.preventDefault();
      $('#task-modal').modal('toggle');

      let parent = $('#task-form-save').attr('data-parent');

      /*Upload file*/
      var formData = new FormData();
      formData.append('file', $('#task-form [name="dokumen"]')[0].files[0]);
      formData.append('nip', $('#task-form').serializeObject().nip);

      $.ajax({
        url: API_SERVER + "wfh/task/upload",
        data: formData,
        type: 'POST',
        processData: false,
        contentType: false,
        beforeSend: function() {
          $(parent).LoadingOverlay("show", {
            image       : "",
            text        : "Mengunggah..."
          })
        },
        complete: function() {
          $(parent).LoadingOverlay("hide")
        },
        success: function(result) {

          if (result.status == 200) {

            /*CREATE & UPDATE TASK*/
            var data = $('#task-form').serializeObject(),
            type = ($('#task-form-save').attr('data-action') == 'add') ? 'POST' : 'PUT';

            data.createdby = NIP;
            if (result.values) data.filename = result.values;

            $.ajax({
              url: API_SERVER + "wfh/task",
              data: data,
              type: type,
              dataType: 'json',
              beforeSend: function() {
                $(parent).LoadingOverlay("show", {
                  image       : "",
                  text        : "Menyimpan..."
                })
              },
              complete: function() {
                $(parent).LoadingOverlay("hide")
              },
              success: function(result) {

                if (result.status == 200) {
                  readWFHById(data.wfh_id, $('#menu button.active').attr('data-menu'))
                  showNotification('bg-green', `BERHASIL - ${result.values}`)
                } else {
                  showNotification('bg-orange', `GAGAL - ${result.values}`)
                }

              },
              error: function(result) {
                showNotification('bg-red', `ERROR - Simpan Pekerjaan`)
              }
            });

          } else {
            showNotification('bg-orange', `GAGAL - ${result.values}`)
          }
        },
        error: function(result) {
          showNotification('bg-red', `ERROR - Upload dokumen`)
        }
      });
    })

    /* TASK MODAL */
    $('#task-modal').on('shown.bs.modal', function (event) {

      var action = $(event.relatedTarget).data('action');

      if (action == 'edit') {

        $('#task-form').bootstrapValidator('resetForm', true);

        var wfh_task = JSON.parse(decodeURIComponent( $(event.relatedTarget).data('wfh_task') ));
        var wfh_task_nip = $(event.relatedTarget).data('nip');

        $('#task-modal h2').text('Ubah Pekerjaan');

        $('#task-form [name="id"]').val(wfh_task.id)
        $('#task-form [name="wfh_id"]').val(wfh_task.wfh_id)
        $('#task-form [name="nip"]').val(wfh_task_nip)
        $('#task-form [name="title"]').val(wfh_task.title).parent().addClass('focused')
        $('#task-form [name="filename"]').val(wfh_task.filename)

        var defaultFile = (wfh_task.filename) ? ( '<?= base_url().'files/upload_milea/' ?>' + wfh_task_nip + '/wfh/' + wfh_task.filename ) : '';
        var drEvent = $('#task-form [name="dokumen"]').dropify({ defaultFile: defaultFile });

        drEvent.on('dropify.beforeClear', function(event, element){
          $('#task-form').bootstrapValidator('revalidateField', 'dokumen');
        });

        drEvent = drEvent.data('dropify');
        drEvent.resetPreview();
        drEvent.clearElement();
        drEvent.settings.defaultFile = defaultFile;
        drEvent.destroy();
        drEvent.init();

        if (wfh_task.filename) $('#task-form').bootstrapValidator('updateStatus', 'dokumen', 'VALID');

      } else {

        $('#task-modal h2').text('Tambah Pekerjaan');

        var wfh_id = $(event.relatedTarget).data('wfh_id');
        var wfh_task_nip = $(event.relatedTarget).data('nip');

        $('#task-form [name="wfh_id"]').val(wfh_id)
        $('#task-form [name="nip"]').val(wfh_task_nip)
        $('#task-form [name="filename"]').val('')

        var drEvent = $('#task-form [name="dokumen"]').dropify();

        $('#task-form').bootstrapValidator('updateStatus', 'dokumen', 'NOT_VALIDATED');
      }

      $('#task-form-save').attr('data-action', action);
      $('#task-form-save').attr('data-parent', $(event.relatedTarget).data('parent'));

    })

    /* DELETE TASK */
    $('#wfh-list').on('click', '.delete', function() {
      Swal.fire({
        title: 'Hapus Pekerjaan?',
        type: 'warning',
        confirmButtonText: 'Ya',
        showCancelButton: true,
        cancelButtonText: 'Tidak',
        focusCancel:true,
        reverseButtons: true,
        showLoaderOnConfirm: true,
        preConfirm: () => {}
      }).then((result) => {
        if (result.value) {

          let parent = $(this).attr('data-parent');

          $.ajax({
            url: API_SERVER + "wfh/task",
            data: { id: $(this).attr('data-id'), createdby: NIP },
            type: 'DELETE',
            dataType: 'json',
            beforeSend: function() {
              $(parent).LoadingOverlay("show", {
                image       : "",
                text        : "Menghapus..."
              })
            },
            complete: function() {
              $(parent).LoadingOverlay("hide")
            },
            success: function(result) {
              if (result.status == 200) {
                readWFHById(parent.replace("#wfh", ""), $('#menu button.active').attr('data-menu'))
                showNotification('bg-green', `BERHASIL - ${result.values}`)
              } else {
                showNotification('bg-orange', `GAGAL - ${result.values}`)
              }
            },
            error: function(result) {
              showNotification('bg-red', `ERROR - Hapus Pekerjaan`)
            }
          });
        }
      })
    });

    /* WFH-VERIFICATION */
    $('#wfh-list').on('click', '.wfh-verification', function(e) {

      e.preventDefault();

      Swal.fire({
        title: 'Verifikasi WFH/WFO?',
        text: 'Pastikan pelaksanaan WFH/WFO oleh pegawai sesuai ketentuan',
        type: 'question',
        input: 'radio',
        inputOptions: {true: 'Ya, Sesuai!', false: 'Tidak'},
        inputValidator: (value) => {
          if (!value) {
            return 'Harus Diisi!'
          }
        },
        confirmButtonText: 'Ok',
        showCancelButton: true,
        cancelButtonText: 'Batal',
        focusCancel:true,
        reverseButtons: true,
        showLoaderOnConfirm: true,
        preConfirm: () => {}
      }).then((result) => {

        if (result.value) {

          let id = $(this).attr('data-id');
          $.ajax({
            url: API_SERVER + "wfh/verification",
            data: { id: id, value: result.value, nip: NIP },
            type: 'POST',
            dataType: 'json',
            beforeSend: function() {
              $(`#wfh${id}`).LoadingOverlay("show", {
                image       : "",
                text        : "Memverifikasi..."
              })
            },
            complete: function() {
              $(`#wfh${id}`).LoadingOverlay("hide")
            },
            success: function(result) {
              if (result.status == 200) {
                readWFHById(id, $('#menu button.active').attr('data-menu'))
                showNotification('bg-green', `BERHASIL - ${result.values}`)
              } else {
                showNotification('bg-orange', `GAGAL - ${result.values}`)
              }
            },
            error: function(result) {
              showNotification('bg-red', `ERROR - Verifikasi`)
            }
          });

        }
      })

    });

    /* WFH - UNDO */
    $('#wfh-list').on('click', '.wfh-undo', function() {
      Swal.fire({
        title: 'Mundurkan Status WFH?',
        type: 'warning',
        confirmButtonText: 'Ya',
        showCancelButton: true,
        cancelButtonText: 'Tidak',
        focusCancel:true,
        reverseButtons: true,
        showLoaderOnConfirm: true,
        preConfirm: () => {}
      }).then((result) => {
        if (result.value) {

          let id = $(this).attr('data-id');

          $.ajax({
            url: `${API_SERVER}wfh/undo`,
            data: { id: id, by: NIP },
            type: 'POST',
            dataType: 'json',
            beforeSend: function() {
              $(`#wfh${id}`).LoadingOverlay("show", {
                image       : "",
                text        : "Memundurkan..."
              })
            },
            complete: function() {
              $(`#wfh${id}`).LoadingOverlay("hide")
            },
            success: function(result) {
              if (result.status == 200) {
                readWFHById(id, $('#menu button.active').attr('data-menu'))
                showNotification('bg-green', `BERHASIL - ${result.values}`)
              } else {
                showNotification('bg-orange', `GAGAL - ${result.values}`)
              }
            },
            error: function(result) {
              showNotification('bg-red', `ERROR - Mundurkan Status WFH`)
            }
          });
        }
      })
    });

    /* MENU */
    $.ajax({
      url: `${API_SERVER}wfh/menu?nip=${NIP}`,
      type: 'GET',
      dataType: 'json',
      success: function(result) {

        if (result.status == 200) {

          let menus = {
            bawahan: {class: 'bg-blue active', title: 'KERJA SAYA'},
            atasan: {class: '', title: 'KERJA BAWAHAN'},
            admin: {class: '', title: 'KERJA SEMUA'},
          };

          let html = '';

          for (let menu of result.values) {
            html += `
            <div class="col-sm-${12/result.values.length}">
            <button type="button" class="btn btn-block btn-link btn-lg waves-effect ${menus[menu].class}"  data-menu="${menu}">
            <span>${menus[menu].title}</span>
            </button>
            </div>`;
          }

          html = `
          <div class="header">
          <div class="row">
          ${html}
          </div>
          </div>`;

          $('#menu').html(html)

          if (result.values.length > 1) {
            $('#menu').show()
          } else {
            $('#menu').hide()
          }


          /* WFH - READ */
          $('#menu .active').click()

        } else {
          showNotification('bg-orange', `GAGAL - ${result.values}`)
        }

      },
      error: function(result) {
        showNotification('bg-red', `ERROR - Menampilkan Menu`)
      }
    });

    $('#menu').on('click', 'button', function(){

      $('#menu button').removeClass('bg-blue active')
      $(this).addClass('bg-blue active')

      switch ($(this).attr('data-menu')) {

        case 'bawahan':
        $('#wfh-filter-form-bymenu').html('')
        readWFHIsWorking($('#wfh-isworking'))
        break;

        case 'atasan':

        $('#wfh-filter-form-bymenu').html(`
          <div class="row clearfix">
          <div class="col-sm-12">
          <div class="form-group form-float">
          <div class="form-line">
          <select name="bawahan" class="form-control show-tick capitalise" data-live-search="true" title="Pilih Pegawai">
          </select>
          <label class="form-label">Pegawai</label>
          </div>
          </div>
          </div>
          </div>
          `)

        getBawahanWFHReference($('#wfh-filter-form [name="bawahan"]'))

        $('#wfh-isworking').html('')

        break;

        case 'admin':

        $('#wfh-filter-form-bymenu').html(`
          <div class="row clearfix">
          <div class="col-sm-6">
          <div class="form-group form-float">
          <div class="form-line">
          <select name="bawahan" class="form-control show-tick capitalise" data-live-search="true" title="Pilih Pegawai">
          </select>
          <label class="form-label">Pegawai</label>
          </div>
          </div>
          </div>
          <div class="col-sm-6">
          <div class="form-group form-float">
          <div class="form-line">
          <select name="atasan" class="form-control show-tick capitalise" data-live-search="true" title="Pilih Atasan">
          </select>
          <label class="form-label">Atasan</label>
          </div>
          </div>
          </div>
          </div>
          <div class="row clearfix">
          <div class="col-sm-12">
          <div class="form-group form-float">
          <div class="form-line">
          <select name="unit" class="form-control show-tick capitalise" data-live-search="true" title="Pilih Unit">
          </select>
          <label class="form-label">Unit</label>
          </div>
          </div>
          </div>
          </div>
          `)

        getPegawaiReference($('#wfh-filter-form [name="bawahan"],#wfh-filter-form [name="atasan"], #wfh-form [name="atasan"]'))
        getOrganizationReference($('#wfh-filter-form [name="unit"]'))

        $('#wfh-isworking').html('')

        break;
      }

      $('#wfh-filter-form')
      .find('select')
      .selectpicker()
      .change(function(e) {
        $(this).closest('.form-line').addClass('focused')
      })
      .end()

      $('#wfh-filter-form').trigger("reset");
      $('#wfh-filter-form .datepicker').datepicker("update");

      $('#wfh-filter-form-save').click()

    })

    /* WFH - FILTER FORM */
    $('#wfh-filter-form')
    .find('.datepicker')
    .datepicker({
      autoclose: true,
      format: "yyyy-mm-dd",
      language: "id",
      clearBtn: true,
      daysOfWeekDisabled: [0,6],
      todayHighlight: true,
    }).on('changeDate', function(ev) {
      $(this).closest('.form-line').addClass('focused')
    })
    .end()

    $('#wfh-filter-form-save').click(function(e){
      e.preventDefault()
      $('#wfh-filter-modal').modal('hide');

      $('#wfh-list').html('')
      $('#wfh-endpage').html('')

      let query = $('#wfh-filter-form').serializeObject();
      query.menu = $('#menu button.active').attr('data-menu');
      query.page = 1;
      readWFH(query)
    })

    $('#wfh-endpage').on('click', 'button', function(e){
      e.preventDefault()
      readWFH( JSON.parse( decodeURIComponent( $(this).attr('data-query') ) ) )
    })

    /* WFH - READ */
    // $('#menu .active').click()

  })
</script>

<script type="text/javascript">

  function readWFHIsWorking($jquery) {

    $.ajax({
      url: `${API_SERVER}wfh/status?nip=${NIP}`,
      type: 'GET',
      dataType: 'json',
      success: function(result) {

        if (result.status == 200) {
          if (!result.values) {
            $jquery.html(`
              <div class="card">
              <div class="header">
              <div class="row clearfix">
              <div class="col-xs-6">
              <div class="font-10 col-grey">Jam Kantor</div>
              <div class="font-20 font-blod stopwatch">00:00:00</div>
              </div>
              <div class="col-xs-6">
              <button type="button" class="btn btn-block btn-primary btn-lg waves-effect wfh-start" data-toggle="modal" data-target="#wfh-start-modal">
              <i class="material-icons">directions_run</i>
              <span>MULAI KERJA</span>
              </button>
              </div>
              </div>
              </div>
              </div>
              `)
            showOfficeWatch($jquery.find('.stopwatch'))
          } else {
            $jquery.html('')
          }
        } else {
          showNotification('bg-orange', `GAGAL - ${result.values}`)
        }

      },
      error: function(result) {
        showNotification('bg-red', `ERROR - Status WFH hari ini`)
      }
    })

  }

  function readWFHById(id, menu) {

    $.ajax({
      url: `${API_SERVER}wfh/item/${id}`,
      type: 'GET',
      dataType: 'json',
      beforeSend: function() {
        $(`#wfh${id}`).LoadingOverlay("show", {
          image       : "",
          text        : "Memuat..."
        })
      },
      complete: function() {
        $(`#wfh${id}`).LoadingOverlay("hide")
      },
      success: function(result) {

        if (result.status == 200) {
          $(`#wfh${id}`).replaceWith( wfhTemplate(result.values, menu) )
          showOfficeWatch($(`#wfh${id}`).find('.stopwatch'))
        } else {
          showNotification('bg-orange', `GAGAL - ${result.values}`)
        }

      },
      error: function(result) {
        showNotification('bg-red', `ERROR - Memuat item WFH`)
      }
    })

  }

  function readWFH(query){

    $.ajax({
      url: API_SERVER + 'wfh?nip=' + NIP + '&' + $.param(query),
      type: 'GET',
      dataType: 'json',
      beforeSend: function() {
        $('#wfh-endpage').LoadingOverlay("show", {
          image       : "",
          text        : "Memuat..."
        })
      },
      complete: function() {
        $('#wfh-endpage').LoadingOverlay("hide")
      },
      success: function(result) {

        if (result.status == 200) {

          let wfh = result.values;

          if (wfh.length == 0) {
            $('#wfh-endpage').html('<p class="font-italic col-grey align-center">Tidak ada lagi yang ditampilkan atau ubah Filter Tampilan</p>')
            return true;
          }

          var html = '';

          for (let w of wfh) {
            html += wfhTemplate(w, query.menu);
          }

          $('#wfh-list').append(html)
          showOfficeWatch($('#wfh-list').find('.stopwatch'))

          query.page = (query.page) ? query.page + 1 : 1;

          $('#wfh-endpage').html('<div class="align-center"><button type="button" class="btn btn-lg bg-blue waves-effect" data-query="' + encodeURIComponent( JSON.stringify(query) ) + '">MUAT LEBIH BANYAK</button></div>')

        } else {
          showNotification('bg-orange', `GAGAL - ${result.values}`)
        }

      },
      error: function(result) {
        showNotification('bg-red', `ERROR - Mengambil data WFH`)
      }
    })

  }

  function wfhTemplate(wfh, menu) {

    let 
    type = {col: '', text: ''},
    status = '',
    name = '',
    health = '',
    tasks = '',
    upload = '',
    action = '';

    switch (wfh.type) {
      case 'wfh':
      type.col =  'teal';
      type.text = 'WFH';
      break;

      case 'wfo':
      type.col =  'amber';
      type.text = 'WFO';
      break;
    }

    switch (wfh.status) {

      case 'started':

      if (menu != 'bawahan') {
        status = `
        <button type="button" class="btn btn-block bg-orange waves-effect">
        <i class="material-icons">autorenew</i>
        <span>SEDANG KERJA</span>
        </button>
        `;
      } else {
        status = `
        <div class="font-12 col-grey">Jam Kantor</div>
        <div class="font-38 font-bold align-center stopwatch">00:00:00</div>
        <button type="button" class="btn btn-block bg-orange waves-effect wfh-finish" data-parent="` + wfh.id + `">
        <i class="material-icons">directions_walk</i>
        <span>SELESAI KERJA</span>
        </button>
        `;
      }

      break;

      case 'finished':

      if (menu == 'atasan') {
        status = `
        <button type="button" class="btn btn-sm bg-orange waves-effect wfh-verification" data-id="` + wfh.id + `">
        <i class="material-icons">autorenew</i>
        <span>VERIFIKASI ?</span>
        </button>
        `;
      } else {
        status = `
        <button type="button" class="btn btn-sm bg-orange waves-effect">
        <i class="material-icons">autorenew</i>
        <span>VERIFIKASI ATASAN</span>
        </button>
        `;
      }

      break;

      case 'verified':

      status = `
      <button type="button" class="btn btn-sm bg-lime waves-effect">
      <i class="material-icons">autorenew</i>
      <span>VALIDASI SURAT TUGAS</span>
      </button>
      `;

      break;

      case 'validated':

      status = `
      <button type="button" class="btn btn-sm bg-green waves-effect">
      <i class="material-icons">done</i>
      <span>DITETAPKAN</span>
      </button>
      `;

      break;

      case 'notverified':

      status = `
      <button type="button" class="btn btn-sm bg-red waves-effect">
      <i class="material-icons">close</i>
      <span>DITOLAK ATASAN</span>
      </button>
      `;

      break;

    }

    name = `
    <div class="row clearfix font-14" style="padding-top: 34px;">
    <div class="col-sm-6">
    <span class="col-grey">Pegawai: </span>
    <a href="">` + wfh.bawahanname + `</a>
    </div>
    <div class="col-sm-6">
    <span class="col-grey">Atasan: </span>
    <a href="">` + wfh.atasanname + `</a>
    </div>
    </div>
    `;

    let healthtype = {
      1: {col: 'green', text: 'SEHAT'},
      2: {col: 'orange', text: 'KURANG FIT'},
      3: {col: 'deep-orange', text: 'SAKIT RINGAN'},
      4: {col: 'red', text: 'SAKIT BERAT'},
    };

    if (wfh.health) {
      health = `
      <div class="font-14 p-t-5">
      <span class="col-grey">Kesehatan: </span>
      ` + ( (healthtype[wfh.healthtype]) ? '<span class="badge bg-' + healthtype[wfh.healthtype].col + '">' + healthtype[wfh.healthtype].text + '</span>' : '' ) + `
      </div>
      <div class="font-12"><span class="col-grey" style="white-space: pre-line;">` + wfh.health + `</span></div>
      `;
    }

    if (wfh.tasks.length) { 

      tasks = `
      <div class="table-responsive" style="margin-top: 20px;">
      <table class="table table-hover">
      <tbody>
      `;

      for (let task of wfh.tasks ) { 

        tasks += `
        <tr>
        <td class="col-grey">` + task.title + `</td>
        <td>
        <div class="pull-right">
        <a class="btn btn-sm bg-blue waves-effect" href="<?= base_url().'files/upload_milea/' ?>` + wfh.nip + `/wfh/` + task.filename + `" target="_blank">
        <i class="material-icons">file_download</i>
        </a>`;

        if ( wfh.upload || (menu != 'bawahan' && wfh.status == 'finished') ) {
          tasks += `
          <button type="button" class="btn btn-sm bg-yellow waves-effect" data-toggle="modal" data-target="#task-modal" data-action="edit" data-wfh_task="` + encodeURIComponent( JSON.stringify(task) ) + `" data-nip="` + wfh.nip + `" data-parent="#wfh` + wfh.id + `">
          <i class="material-icons">edit</i>
          </button>
          <button type="button" class="btn btn-sm bg-red waves-effect delete" data-id="` + task.id + `" data-parent="#wfh` + wfh.id + `">
          <i class="material-icons">delete</i>
          </button>
          `;
        }

        tasks += `
        </div>
        </td>
        </tr>
        `;
      } 

      tasks +=`
      </tbody>
      </table>
      </div>
      `;

    } 

    if ( wfh.upload || (menu != 'bawahan' && wfh.status == 'finished') ) {
      upload = `
      <button type="button" class="btn btn-sm bg-cyan waves-effect" data-toggle="modal" data-target="#task-modal" data-action="add" data-wfh_id="` + wfh.id + `" data-nip="` + wfh.nip + `" data-parent="#wfh` + wfh.id + `">
      <i class="material-icons">add</i>
      </button>
      `;
    }

    if (menu == 'admin') {
      action = `
      <div class="header">
      <div class="row clearfix">
      <div class="col-xs-12">
      <button type="button" class="btn btn-sm bg-blue waves-effect" data-toggle="modal" data-target="#wfh-modal" data-action="edit" data-wfh="${encodeURIComponent( JSON.stringify(wfh) )}">
      <i class="material-icons">edit</i>
      <span>UBAH</span>
      </button>
      <button type="button" class="btn btn-sm bg-amber waves-effect wfh-undo" data-id="${wfh.id}">
      <i class="material-icons">undo</i>
      <span>MUNDURKAN STATUS</span>
      </button>
      </div>
      </div>
      </div>`;
    }

    let wfh_html = `

    <div class="card" id="wfh` + wfh.id + `">
    <div class="header">
    <div class="row clearfix">
    <div class="col-xs-12">
    <div class="pull-right">
    ` + status + `
    </div>
    <div class="font-22">
    <span class="label bg-${type.col}">${type.text}</span>
    ${new Date(wfh.starttime).toLocaleString('id-ID', { dateStyle: 'full' })}
    </div>
    <div class="font-14">
    <span class="label bg-` + ( (wfh.attendancestatus == 'HN') ? 'green' : 'red' ) + `">` + wfh.attendancestatus + `</span>
    <span class="label bg-indigo">Datang: ` + new Date(wfh.starttime).toLocaleString('id-ID', { hour: '2-digit', minute: '2-digit' }) + `</span>
    <span class="label bg-indigo">Pulang: ` + ( (wfh.endtime) ? new Date(wfh.endtime).toLocaleString('id-ID', { hour: '2-digit', minute: '2-digit' }) : '-' ) + `</span>
    </div>
    ` + name + `
    ` + health + `
    </div>
    </div>
    </div>
    <div class="header">
    <div class="row clearfix">
    <div class="col-xs-12">
    ` + upload + `
    <span class="label bg-purple">Hasil Pekerjaan: ` + wfh.tasks.length + ` Berkas</span>
    ` + tasks + `
    </div>
    </div>
    </div>
    ${action}
    </div>
    `;

    return wfh_html;

  }

  function getPegawaiReference($jquery) {

    $.ajax({
      url: API_SERVER + 'pegawai',
      type: 'GET',
      dataType: 'json',
      success: function(result) {
        if (result.status == 200) {
          var html = '<option value="-1">Semua</option>';
          $.each(result.values, function(index, item){
            html += '<option value="'+item.nip18+'">'+item.nama+' - '+item.nip18+'</option>'
          })
          $jquery.html(html).selectpicker('refresh').trigger('change');
        } else {
          console.log('Gagal load data pegawai');
        }
      }
    })

  }

  function getOrganizationReference($jquery) {

    $.ajax({
      url: API_SERVER + 'organization?id=1',
      type: 'GET',
      dataType: 'json',
      success: function(result) {
        if (result.status == 200) {
          var html = organizationOption([ result.values ], 0);
          $jquery.html(html).selectpicker('refresh').trigger('change');
        } else {
          console.log('Gagal load data pegawai');
        }
      }
    })

  }

  function organizationOption(orgs, level) {

    let html = '';

    for (let org of orgs) {
      html += '<option value="'+org.id+'" class="m-l-' + level*10 + '">' + org.name + '</option>';
      if (org.children) html += organizationOption(org.children, level+1);
    }

    return html;

  }

  function getBawahanWFHReference($jquery) {

    $.ajax({
      url: API_SERVER + 'wfh/bawahan?nip=' + NIP,
      type: 'GET',
      dataType: 'json',
      success: function(result) {
        if (result.status == 200) {
          var html = '<option value="-1">Semua</option>';
          $.each(result.values, function(index, item){
            html += '<option value="'+item.nip18+'">'+item.nama+' - '+item.nip18+'</option>'
          })
          $jquery.html(html).selectpicker('refresh').trigger('change');
        } else {
          console.log('Gagal load data pegawai');
        }
      }
    })

  }

  function showNotification(colorName, text) {
    var 
    placementFrom   = 'top', 
    placementAlign  =  'center', 
    animateEnter    = '', 
    animateExit     = '';

    if (colorName === null || colorName === '') { colorName = 'bg-black'; }
    if (text === null || text === '') { text = 'Turning standard Bootstrap alerts'; }
    if (animateEnter === null || animateEnter === '') { animateEnter = 'animated fadeInDown'; }
    if (animateExit === null || animateExit === '') { animateExit = 'animated fadeOutUp'; }
    var allowDismiss = true;

    $.notify({
      message: text
    },
    {
      type: colorName,
      allow_dismiss: allowDismiss,
      newest_on_top: true,
      timer: 1000,
      placement: {
        from: placementFrom,
        align: placementAlign
      },
      animate: {
        enter: animateEnter,
        exit: animateExit
      },
      template: '<div data-notify="container" class="bootstrap-notify-container alert alert-dismissible {0} ' + (allowDismiss ? "p-r-35" : "") + '" role="alert">' +
      '<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>' +
      '<span data-notify="icon"></span> ' +
      '<span data-notify="title">{1}</span> ' +
      '<span data-notify="message">{2}</span>' +
      '<div class="progress" data-notify="progressbar">' +
      '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
      '</div>' +
      '<a href="{3}" target="{4}" data-notify="url"></a>' +
      '</div>'
    });
  }

  function showOfficeWatch($jquery) {

    let start;

    $.ajax({
      url: API_SERVER + 'wfh/time',
      type: 'GET',
      dataType: 'json',
      beforeSend: function() {
        start = new Date().getTime();
      },
      success: function(result) {

        if (result.status == 200) {

          let secondsServer = ( new Date(result.values).getTime() - new Date( new Date(result.values).toLocaleDateString() ).getTime() ) / 1000;

          $jquery.each(function(e){

            let s = $(this);

            let seconds = secondsServer + ( ( new Date().getTime() - start ) / 2000 );

            var timer = new easytimer.Timer();
            timer.start({ precision: 'seconds', startValues: {seconds: seconds } });
            timer.addEventListener('secondsUpdated', function (e) {
              s.html(timer.getTimeValues().toString());
            });

          })

        } else {
          showNotification('bg-orange', `GAGAL - ${result.values}`)
        }

      },
      error: function(result) {
        showNotification('bg-red', `ERROR - Jam Kantor`)
      }
    })

  }

</script>

<script type="text/javascript">

  function getPegawaiOpt() {
    $.ajax({
      url: '<?= $this->config->item('API_SERVER').'pegawai' ?>',
      type: 'GET',
      dataType: 'json',
      success: function(result) {
        if (result.status == 200) {
          var html = '';
          $.each(result.values, function(index, item){
            html += '<option value="'+item.nip18+'">'+item.nama+' - '+item.nip18+'</option>'
          })
          $('select.select-pegawai').append(html).selectpicker('refresh');

          getAtasan()
        } else {
          console.log('Gagal load data pegawai');
        }
      }
    })
  }

  function getAtasan() {
    $.ajax({
      url: API_SERVER + 'pegawai/atasan?nip=' + NIP,
      type: 'GET',
      dataType: 'json',
      success: function(result) {
        if (result.status == 200) {
          let atasan = result.values;
          $('#wfh-start-form [name="atasannip"]').selectpicker('val', atasan.nip18)
        } else {
          console.log('Gagal load data atasan', result.values);
        }
      }
    })
  }

  function formatDate(date, time){
    var options;
    if (time) {
      options = {dateStyle: "medium", timeStyle: "medium"};
    } else {
      options = {dateStyle: "medium"};
    }
    return new Date(date).toLocaleDateString('id-ID', options);
  }

</script>