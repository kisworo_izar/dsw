<div class="row">
    <div class="col-md-9">
        <div class="box box-widget">
            <div class="box-body with-border">
                <form role="form" action="<?php echo site_url("pengumuman?q=bnQYV") ?>" method="post">
                  <div class="input-group">
                    <input type="hidden" name="nmfunction" value="dash">
                    <input type="text" name="cari" class="form-control" placeholder="Pencarian..." value="<?php echo $this->session->userdata('cari') ?>">
                        <span class="input-group-btn">
                          <button type="submit" name="search" value="search" id="search-btn" class="btn btn-default btn-flat"><i class="fa fa-search"></i>
                          </button>
                        </span>
                      <span class="input-group-btn">
                        <button type="submit" name="search" value="clear" class="btn btn-default btn-flat"><i class="fa fa-times-circle"></i></button>
                        </span>
                  </div>
                </form>
            </div>

            <?php foreach ($pengumuman as $row) {
                $arr = explode('<p>', $row['pengumuman']);
                $pengumuman = trim( $arr[1] );
                $lengkapnya = str_replace($pengumuman, '', $row['pengumuman']);
            ?>

            <div class="box-footer box-comments" style="background:white">
                <img class="img-circle img-sm img-bordered-dsw" src="<?php echo base_url("files/images/depkeu_round.png") ?>">
                <div class="comment-text" style="bg-color:white !important">
                    <span class="username">
                        Sekretariat
                        <span class="text-muted pull-right"><?php echo $this->fc->idtgl($row['tglrekam'], 'hari') ?>
                            <!-- &nbsp;<i class="fa fa-bookmark text-aqua"></i> -->
                        </span>
                    </span>
                    <a href="#" onclick="showHide('<?php echo 'hidden_div'.$row['idpengumuman'] ?>'); return false;"><?php echo $row['judul'] ?></a><br>
                    <?php
                    if ($row['gambar']) {
                        echo '<img style="float:left; width:95px !important; height:75px !important; margin: 5px 10px 5px 0px" src="'. base_url("files/pengumuman") .'/'. $row['gambar'] .'">';
                    }
                    echo $pengumuman ;
                    echo '<div id="'. 'hidden_div'.$row['idpengumuman'] .'" style="display: none;">'. $lengkapnya .'</div>';
                    if ($row['file']) {
                        echo '<i class="small">File : </i>
                        <a href="'.base_url("files/pengumuman") .'/'. $row['file'] .'"  target=_blank><i class=" small fa fa-file-pdf-o"></i> <i class="small">'. $row['file'] .'</a></i>';
                    }
                    ?>
                </div>
            </div>
            <?php }; ?>
        </div>
        <?php echo $this->pagination->create_links(); ?>
    </div>
     <div class="col-md-3 hidden-xs" style="margin-bottom:20px">
         <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
             <div class="carousel-inner">
                 <div class="item active">
                     <a href="#"><img src="<?php echo base_url("files/images/transform.gif") ?>"></a>
                 </div>
             </div>
         </div>
     </div>

    <div class="col-md-3 hidden-xs">
        <div class="box box-widget">
            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                    <?php
                    $min = 1; $max = 55;
                    $ran = range($min, $max);
                    shuffle($ran);

                    for ($i=$min; $i < $max; $i++) {
                    echo '<div class="item';
                        if ($i==1) echo ' active'; echo '">';
                    ?>
                    <img src="<?php echo base_url("files/poster/poster$ran[$i]"); ?>.jpg">
                    </div>
                    <?php } ?>
                </div>
                <!-- <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev"></a>
                <a class="right carousel-control" href="#carousel-example-generic" data-slide="next"></a> -->
            </div>
        </div>
    </div>

</div>

<script type="text/javascript">
    function showHide(obj) {
        var div = document.getElementById(obj);
            if (div.style.display == 'none') {
            div.style.display = '';
        } else {
            div.style.display = 'none';
        }
    }
</script>
<script type="text/javascript">
	$('.pagination').addClass('pagination-sm no-margin pull-right');
</script>
