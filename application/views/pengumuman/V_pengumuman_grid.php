<script src="<?=base_url('assets/plugins/jQueryUI/jquery-ui.1.11.4.min.js'); ?>" type="text/javascript"></script>

<style media="screen">
	th {
    	text-align: center;
  	}
  	thead tr {
    	color: #fff;
    	background: #00acd6;
  	}

  	#iGrid .ui-selecting { background: #FECA40; }
  	#iGrid .ui-selected { background: #F39814; color: white; }
</style>

<div class="row">
	<div class="col-md-12">
		<div class="box box-widget">
			<div class="box-header with border">
				<div class="wel wel-sm">
					<form id="iForm" class="form-inline" role="form" action="<?php echo site_url("pengumuman/cari") ?>" method="post">
						<div class="form-group pull-right">
							<div class="input-group">
                    			<input type="hidden" name="nmfunction" value="grid">
                				<input class="form-control" name="cari" type="text" value="<?php echo $this->session->userdata('cari') ?>">
            					<span class="input-group-btn">
            						<button type="submit" name="search" value="search" class="btn btn-info btn-flat"><i class="fa fa-search"></i></button>
            					</span>
            					<span class="input-group-btn">
            						<button type="submit" name="search" value="clear" class="btn btn-info btn-flat"><i class="fa fa-times-circle"></i></button>
                				</span>
                			</div>
                		</div>
                		<div class="form-group">
                			<div class="input-group">
	                			<span class="input-group-btn">
                    				<input type="hidden" id="idpengumuman" value="">
									<button type="button" id="addrow"  class="btn btn-warning" onclick="window.location.href='http://10.242.142.52/pengumuman/rekam/r'">Rekam</button>
					  				<button type="button" id="editrow" class="btn btn-warning" value="2">Ubah</button>
					  				<button type="button" id="delrow"  class="btn btn-warning">Hapus</button>
	                			</span>
                			</div>
                		</div>
                	</form>
                </div>
            </div>   <!-- box header  -->


			<div class="box-body">
				<table id="iGrid" class="table table-hover table-bordered">
					<thead>
					<tr>
						<th>Tanggal</th>
						<th>Judul</th>
						<th>Isi Pengumuman</th>
						<th>Gambar / File </th>
						<th>Pemilik</th>
					</tr>
					</thead>
				<tbody>
					<?php
					if ($table) {
						foreach ($table as $row) { ?>
							<tr>
								<td id="<?php echo $row['idpengumuman'] ?>"><?php echo $row['tglrekam'] ?></td>
								<td><?php echo $row['judul']; if ($row['toplist']=='1') echo '<label style="color:#ff0000"> ***</label>'; ?>
								</td>
								<td><?php echo $row['pengumuman'] ?></td>
								<td><?php echo $row['gambar'] .'<br>'. $row['file'] ?></td>
								<td><?php echo $row['iduser'] .'<br>'. $row['kdso'] ?></td>
							</tr>
						<?php
						}
					} else { ?>
							<tr><td colspan="4" class="text-danger"> Data tersebut tidak ditemukan ...</td></tr>
					<?php } ?>
				</tbody>
				</table>
			</div> <!-- box body -->

			<div class="box-footer clearfix">
				<?php echo $this->pagination->create_links(); ?>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$('.pagination').addClass('pagination-sm no-margin pull-right');
</script>

<script type="text/javascript">
	$(function() {

		// Tandai ROW yang dipilih dalam iGRID
		$( "#iGrid tbody" ).selectable({
			filter: ":not(td)",
				create: function( e, ui ) {
				iGridLink( $() );
			},
			selected: function( e, ui ) {
				var widget = $(this).find('.ui-selected');
				$(ui.unselected).addClass("info");
				iGridLink( widget );
			},
			unselected: function( e, ui ) {
				$(ui.unselected).removeClass("info");
				var widget = $(this).find('.ui-selected');
				iGridLink( widget );
			}
		});

		// ONCLICK untuk Rekam / Ubah / Hapus
		$("#addrow").on("click", function() {
			window.location.href = "<?php echo site_url('pengumuman/rekam/r') ?>" ;
		});
		$("#editrow").on("click", function() {
			window.location.href = "<?php echo site_url('pengumuman/rekam/u') ?>" +"/"+ document.getElementById("idpengumuman").value;
		});
		$("#delrow").on("click", function() {
			window.location.href = "<?php echo site_url('pengumuman/rekam/h') ?>" +"/"+ document.getElementById("idpengumuman").value;
		});

		// Ambil Nilai ROW pada iGRID yang dipilih per-ID sesuai kolom CHILD
		function iGridLink( $selectees ) {
			selected = $.makeArray( $selectees.filter( ".ui-selected" ) );
				selected.reduce( function( a, b ) {
					document.getElementById("idpengumuman").value= $(b).children( "td:nth-child(1)" ).attr('id');
				}, 0 
			);
		 }

	}); 
</script>