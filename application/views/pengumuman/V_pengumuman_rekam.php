<link href="<?php echo base_url(); ?>assets/plugins/uploadfile/fileinput.css" rel="stylesheet">
<script src="<?php echo base_url();?>assets/plugins/uploadfile/fileinput.min.js" type="text/javascript"></script>

<section class="content">
    <div class='row'>
        <div class='col-md-12'>

            <div class='box box-info'>
                <div class='box-header'>
                    <h3 class='box-title'><?php echo $ruh ?> Data Pengumuman</h3>
                    <!-- tools box -->
                    <div class="pull-right box-tools">
                        <button class="btn btn-info btn-sm" data-widget='collapse' data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                        <button class="btn btn-info btn-sm" data-widget='remove' data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
                    </div><!-- /. tools -->
                </div><!-- /.box-header -->

                <form action="<?php echo site_url('pengumuman/crud') ?>" method="post" role="form" enctype="multipart/form-data">
                <div class='box-body pad'>
                    <div class="form-group">
                        <label>Judul :</label>
                        <div class="input-group">
                            <div class="input-group-addon"><i class="fa fa-newspaper-o"></i></div>
                            <input name="ruh" type="hidden" value="<?php echo $ruh ?>" />
                            <input name="idpengumuman" type="hidden" value="<?php echo $table['idpengumuman'] ?>" />
                            <input name="judul" type="text" id="" class="form-control" value="<?php echo $table['judul'] ?>" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Gambar : </label> &nbsp; <span class="text-muted"><?php echo $table['gambar'] ?></span>
                        <div class="input-group">
                            <div class="input-group-addon"><i class="fa fa-image"></i></div>
                            <input name="gambar" type="file" class="file" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Files :</label> &nbsp; <span class="text-muted"><?php echo $table['file'] ?></span>
                        <div class="input-group">
                            <div class="input-group-addon"><i class="fa fa-external-link"></i></div>
                            <input name="file" type="file" class="file" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Pengumuman :</label>
                        <textarea id="editor1" name="pengumuman" rows="10" cols="80"><?php echo $table['pengumuman'] ?></textarea>
                    </div>
                    <div class="form-group">
                        <label>Top List : &nbsp;</label>
                        <input name="toplist" type="checkbox" <?php if ($table['toplist']=='1') echo 'checked' ?> value="1"> &nbsp; Pengumuman paling atas
                    </div>
                </div>

                <div class="box-footer">
                    <div class=" pull-right">
                        <input name="simpan" type="submit" id="simpan" class="btn btn-primary" value="<?php echo $ruh ?>">
                    </div>
                </div>

                </form>
            </div><!-- /.box -->

        </div><!-- /.col-->
    </div><!-- ./row -->
</section><!-- /.content -->

<script src="<?php echo base_url();?>assets/plugins/ckeditor/ckeditor.js"></script>
<script type="text/javascript">
    $(function () {
        CKEDITOR.replace('editor1');
    });
</script>

<script type="text/javascript">
    $("#excel").fileinput({
        uploadUrl: "<?php echo site_url('upload/fileupload') ?>", 
        allowedFileExtensions : ['15','16','jpg','xlsx','doc','docx','rtf','pdf'],
        overwriteInitial: true,
        maxFileSize: 1000,
        maxFilesNum: 10,
        minFileCount: 1,
        maxFileCount: 5,
        dropZoneTitle: 'Drag & Drop file ADK RKA-KL dan TOR/RAB disini ...',
        msgInvalidFileExtension: "Invalid extension file {name}. Hanya {extensions} files yang bisa diproses ...",
        slugCallback: function(filename) {
            return filename.replace('(', '_').replace(']', '_');
        }
    });
</script>