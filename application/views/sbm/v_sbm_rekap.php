<div class="box box-widget">
  <input type="hidden" id="aktif">
  <input type="hidden" id="strlen" value="0">

  <div class="box-body">
    <table id="iGrid" class="table table-hover table-bordered" style="border-spacing: 1; width: 100%;">
      <div class="input-group" style="padding-bottom: 7px;">
           <input type="hidden" name="nmfunction" value="grid">
           <input id="cari" class="form-control" name="cari" type="text" value="" autocomplete="off" placeholder="Pencarian ...">
            <span class="input-group-btn">
               <button onclick="history.go(-1)" class="btn btn-info btn-flat"><i class="fa fa-search" style="height:16px;margin-top:4px"></i>
               </button>
            </span>
      </div>

      <thead style="font-size:13px">
        <tr style="color: #fff; background-color: #3568B6;">
          <th class="text-center"> No. </th>
          <th class="text-center"> Kelompok </th>
          <th class="text-center"> Ket </th>
        </tr>
      </thead>

      <tbody style="font-size:13px">
          <tr id="tr_crud" style="display: none" bgcolor="#eee">
            <td colspan="6"><div id="crud_form"><?php $this->load->view('sbm/v_sbm_ket'); ?></div></td>
          </tr>

        <?php foreach ($tabel as $row) { ?>

          <tr id="id_<?= $row['kode'] ?>" class="All" style="margin: 0px">
            <td class="text-left"><b><?= $row['kode'] ?></b></td>
            <td id="td_<?= $row['kode'] ?>" class="text-left" data-list="<?= implode('#', $row) ?>"  onclick="go_detil( this.id )">
            <div  style="word-wrap: break-word;">
               <?= $row['uraian'] ?>
            </div>
            </td>
            <td class="text-center"><i class="fa fa-file-text-o"></i></td>
          </tr>

        <?php } ?>
      </tbody>
    </table>

  </div>
</div>

<script type="text/javascript">
  $( document ).ready(function() {
    $('#crud_form').slideUp().addClass('CloseForm');
  });

  $("#cari").keyup( function(event) {
    var kata = $('#cari').val();
    $('#tr_crud').css('display', 'none').insertAfter( $('#tr_cari') );

    if (kata.length > $('#strlen').val()) {
      $('#iGrid > tbody  > tr:visible').each(function() {
        var id  = $(this).attr('id');
        var str = $('#'+id).text().toLowerCase();
        if (str.indexOf( kata.toLowerCase() ) < 0) $('#'+id).hide();
      });
    } else {
      $('#iGrid > tbody  > tr:hidden').each(function() {
        var id  = $(this).attr('id');
        var str = $('#'+id).text().toLowerCase();
        if (str.indexOf( kata.toLowerCase() ) >= 0) $('#'+id).show();
      });
    }
    $('#strlen').val( kata.length );
  });

  function go_detil( idkey ) {
    var key = idkey.substr(3, 3);
    window.location.href = "<?= site_url('sbm?q=0558b&kode=') ?>" + key;
  }

  function move_row( idkey ) {
    if ( $('#aktif').val()==idkey ) {
      animeCrud('close');
    } else {
      animeCrud( idkey );
      var arr = $('#'+idkey).data('list').split("#");
      $("form input").each(function(){ $(this).val(arr[ $(this).attr('id') ]); })
      $('#Rekam').hide(); $('#Ubah, #Hapus').show();
    }
  }

  function newadd() {
    if ( $('#aktif').val()=='tr_cari' ) {
      animeCrud('close');
    } else {
      animeCrud('tr_cari');
      $("form input").each(function(){ $(this).val(''); })
      $('#Rekam').show(); $('#Ubah, #Hapus').hide();
    }
  }

  function animeCrud( aksi ) {
    if (aksi == 'close') {
      $('#aktif').val('');
      $('#crud_form').slideUp('slow').addClass('CloseForm').removeClass('OpenForm');
      setTimeout(function(){ $('#tr_crud').css('display', 'none'); }, 500);
    } else {
      $('#aktif').val(aksi);
      $('#tr_crud').css('display', 'none');
      if ($('#crud_form').attr('class') == 'OpenForm') $('#crud_form').slideUp('fast');
      $('#tr_crud').css('display', '').insertAfter( $('#'+aksi) );
      $('#crud_form').slideDown().addClass('OpenForm').removeClass('CloseForm');
    }
  }
</script>
