<div class="box box-widget">
  <input type="hidden" id="aktif">
  <input type="hidden" id="strlen" value="0">

  <div class="box-body">
    <table id="iGrid" class="table table-hover table-bordered" style="border-spacing: 1; width: 100%;">
      <div class="input-group" style="padding-bottom: 7px;">
           <input type="hidden" name="nmfunction" value="grid">
           <input id="cari" class="form-control" name="cari" type="text" value="" autocomplete="off" placeholder="Pencarian ...">
            <span class="input-group-btn">
               <button onclick="history.go(-1)" class="btn btn-info btn-flat"><i class="fa fa-arrow-left" style="height:16px;margin-top:4px"></i>
               </button>
            </span>
      </div>

      <thead style="font-size:12px">
        <tr style="color: #fff; background-color: #3568B6;">
          <th class="text-center" width="70%"> Uraian </th>
          <th class="text-center" width="10%"> Satuan </th>
          <th class="text-center" width="10%"> Biaya </th>
        </tr>
      </thead>

      <tbody style="font-size:12px">
         <?php foreach ($tabel as $row) {
               if ($row['biaya'] == 0) {?>
                  <tr id="id_<?= $row['kdsbu'] ?>" class="Sub" style="margin: 0px" onclick="move_row( this.id )">
                   <td colspan="3" class="text-left"><b>  <?= $row['nmsbu'] ?> </b></td>
                 </tr>
         <?php } else{?>
                  <tr id="id_<?= $row['kdsbu'] ?>" class="All" style="margin: 0px" onclick="move_row( this.id )">
                   <td class="text-left" style="padding:5px 5px 5px 25px">  <?= $row['nmsbu'] ?></td>
                   <td class="text-center" style="padding:5px"><?= $row['satuan'] ?></td>
                   <td class="text-right" style="padding:5px"> <?= number_format($row['biaya'], 0, ',', '.') ?></td>
                 </tr>
         <?php } } ?>

      </tbody>
    </table>

  </div>
</div>

<script type="text/javascript">
  $("#cari").keyup( function(event) {
    var kata = $('#cari').val();
    $('#tr_crud').css('display', 'none').insertAfter( $('#tr_cari') );

    if (kata.length > $('#strlen').val()) {
      $('#iGrid > tbody  > tr:visible').each(function() {
        var id  = $(this).attr('id');
        var str = $('#'+id).text().toLowerCase();
        if ($(this).attr('class') == 'All' && str.indexOf( kata.toLowerCase() ) < 0) $('#'+id).hide();
      });
    } else {
      $('#iGrid > tbody  > tr:hidden').each(function() {
        var id  = $(this).attr('id');
        var str = $('#'+id).text().toLowerCase();
        if (str.indexOf( kata.toLowerCase() ) >= 0) $('#'+id).show();
      });
    }
    $('#strlen').val( kata.length );
  });
</script>
