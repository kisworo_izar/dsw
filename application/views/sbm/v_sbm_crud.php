<link  href="<?= base_url('assets/plugins/uploadfile/fileinput.css') ?>" rel="stylesheet">
<script src="<?= base_url('assets/plugins/uploadfile/fileinput.min.js') ?>" type="text/javascript"></script>

<style media="screen">
  .form-group { margin: 0px;}
  .btn-crud { margin-top: 10px;}
</style>


<div class="box box-widget" style="margin: 0px 0px 0px 0px;">
  <div class="box-body">

    <form action="" enctype="multipart/form-data">
      <div class="col-sm-12 pull-left">
        <div class="form-group">
          <label class="col-sm-2 text-left">ID</label>
          <div class="col-sm-10"><input id="0" name="idgallery" type="text" readonly class="form-control"></div>
        </div>
        <div class="form-group">
          <label class="col-sm-2 text-left">Photo</label>
          <div class="col-sm-4"><input id="1" name="photo" type="text" readonly class="form-control" placeholder="File Photo [jpg, png]"></div>
          <div class="col-sm-6"><input type="file" name="gallery" id="gallery" class="file" data-show-preview="false"></div>
        </div>
        <div class="form-group">
          <label class="col-sm-2 text-left">Judul</label>
          <div class="col-sm-10"><input id="2" name="judul" type="text" class="form-control" placeholder="Judul photo"></div>
        </div>
        <div class="form-group">
          <label class="col-sm-2 text-left">Tags</label>
          <div class="col-sm-10"><input id="3" name="tag" type="text" class="form-control" placeholder="Tagging photo diawali tanda #"></div>
        </div>
        <div class="form-group">
          <label class="col-sm-2 text-left">Likes</label>
          <div class="col-sm-10"><input id="4" name="likes" type="text" readonly class="form-control"></div>
        </div>
        <div class="form-group">
          <label class="col-sm-2 text-left">User</label>
          <div class="col-sm-10"><input id="5" name="iduser" type="text" readonly class="form-control"></div>
        </div>
        <div class="form-group">
          <label class="col-sm-2 text-left">Tanggal</label>
          <div class="col-sm-10"><input id="6" name="tglupload" type="text" readonly class="form-control"></div>
        </div>

        <div class="form-group text-center">
          <button id="Rekam" class="btn btn-info btn-crud" onclick="crud( this.id )">Rekam</button>
          <button id="Ubah"  class="btn btn-warning btn-crud" onclick="crud( this.id )">Ubah</button>
          <button id="Hapus" class="btn btn-danger btn-crud" onclick="crud( this.id )">Hapus</button>
        </div>
      </div>
    </form>

  </div>
</div>

<script type="text/javascript">
  $('#gallery').fileinput({
    uploadExtraData:  function(previewId, index) { var data = { name: $(this).attr('id') }; return data; },
    uploadUrl: "<?php echo site_url('photography/admin_image_upload') ?>",
    allowedFileExtensions: ['jpg','png'],
    overwriteInitial: true
  });

  $('#gallery').on('filebatchuploadsuccess', function(event, data, extra) {
    var extra = data.extra, response = data.response;
    $('#1, #2').val( data.response.nmfile );
  });

  $('#gallery').on('filebatchuploaderror', function(event, data) {
    alert('Gagal upload !');
  });

  function crud( aksi ) {
    $('form').submit(function(e){
      e.preventDefault();
      $.ajax({
        url : "<?php echo site_url('sbm/admin_crud/gallery') ?>",
        type: "POST",
        data: $('form').serialize() +'&aksi='+ aksi,
        success: function(pesan) { window.location.href = "<?php echo site_url('sbm/admin_gallery') ?>"; }
      });
    });
  }    
</script>
