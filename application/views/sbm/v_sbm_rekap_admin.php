<div class="box box-widget">
  <input type="hidden" id="aktif">
  <input type="hidden" id="strlen" value="0">
  
  <div class="box-body">
    <table id="iGrid" class="table table-hover table-bordered" style="border-spacing: 1; width: 100%;">
      <thead>
        <tr style="color: #fff; background-color: #3c8dbc;">
          <th class="text-center" width="10%"> No. </th>
          <th class="text-center" width="80%"> Kelompok </th>
          <th class="text-center" width="10%"> Ket </th>
        </tr>
        <tr id="tr_cari" style="margin: 0px" bgcolor="#fff">
          <td colspan="6">
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-btn">
                  <!-- <button id="btn_data" onclick="newadd()" class="btn btn-default"><i class="fa fa-plus"></i></button> -->
                  <button class="btn btn-default"><i class="fa fa-search"></i></button>
                </span>
                <input id="cari" class="form-control" placeholder="ketik kata cari" value="">
              </div>      
            </div>
          </td>
        </tr>
      </thead>

      <tbody>
          <tr id="tr_crud" style="display: none" bgcolor="#eee">
            <td colspan="6"><div id="crud_form"><?php $this->load->view('sbm/v_sbm_crud'); ?></div></td>
          </tr>

        <?php foreach ($tabel as $row) { ?>
          
          <tr id="id_<?= $row['kode'] ?>" class="All" style="margin: 0px">
            <td class="text-left"><b><?= $row['kode'] ?></b></td>
            <td id="td_<?= $row['kode'] ?>" class="text-left" data-list="<?= implode('#', $row) ?>"  onclick="go_detil( this.id )"><?= $row['uraian'] ?></td>
            <td id="kt_<?= $row['kode'] ?>" class="text-left"><?= $row['ket'] ?></td>
          </tr>

        <?php } ?>
      </tbody>
    </table>

  </div>
</div>

<script type="text/javascript">
  // FIRST START
  $( document ).ready(function() {
    $('#crud_form').slideUp().addClass('CloseForm');
  });

  // PENCARIAN
  $("#cari").keyup( function(event) {
    var kata = $('#cari').val();
    $('#tr_crud').css('display', 'none').insertAfter( $('#tr_cari') );

    if (kata.length > $('#strlen').val()) {
      $('#iGrid > tbody  > tr:visible').each(function() {
        var id  = $(this).attr('id');
        var str = $('#'+id).text().toLowerCase();
        if (str.indexOf( kata.toLowerCase() ) < 0) $('#'+id).hide();
      });
    } else {
      $('#iGrid > tbody  > tr:hidden').each(function() {
        var id  = $(this).attr('id');
        var str = $('#'+id).text().toLowerCase();
        if (str.indexOf( kata.toLowerCase() ) >= 0) $('#'+id).show();
      });
    }
    $('#strlen').val( kata.length );
  });

  // Buka Detil SBM
  function go_detil( idkey ) {
    var key = idkey.substr(3, 3);
    window.location.href = "<?php echo site_url('sbm/detil_sbm') ?>" +'/'+ key;
  }

  // Click ROW untuk Buka Form CRUD
  function move_row( idkey ) {
    if ( $('#aktif').val()==idkey ) {
      animeCrud('close');
    } else { 
      animeCrud( idkey );
      var arr = $('#'+idkey).data('list').split("#");
      $("form input").each(function(){ $(this).val(arr[ $(this).attr('id') ]); })
      $('#Rekam').hide(); $('#Ubah, #Hapus').show();
    }
  }

  // Click Tombol PLUS untuk Buka Form CRUD
  function newadd() {
    if ( $('#aktif').val()=='tr_cari' ) {
      animeCrud('close');
    } else {
      animeCrud('tr_cari');
      $("form input").each(function(){ $(this).val(''); })
      $('#Rekam').show(); $('#Ubah, #Hapus').hide(); 
    }
  }

  // Animasi Slide Up Down Form Crud biar smooth
  function animeCrud( aksi ) {
    if (aksi == 'close') {
      $('#aktif').val('');
      $('#crud_form').slideUp('slow').addClass('CloseForm').removeClass('OpenForm');
      setTimeout(function(){ $('#tr_crud').css('display', 'none'); }, 500);
    } else {
      $('#aktif').val(aksi);
      $('#tr_crud').css('display', 'none');
      if ($('#crud_form').attr('class') == 'OpenForm') $('#crud_form').slideUp('fast');
      $('#tr_crud').css('display', '').insertAfter( $('#'+aksi) );
      $('#crud_form').slideDown().addClass('OpenForm').removeClass('CloseForm');
    }
  }
</script>


