<!-- Main content -->
<section class="content">
  <div class='row'>
    <div class='col-md-12'>

      <div class='box box-info'>
        <div class='box-header'>
          <h3 class='box-title'>Editor Informasi <small>Advanced and full of features</small></h3>
          <!-- tools box -->
          <div class="pull-right box-tools">
            <button class="btn btn-info btn-sm" data-widget='collapse' data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
            <button class="btn btn-info btn-sm" data-widget='remove' data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
          </div><!-- /. tools -->
        </div><!-- /.box-header -->
        <form action="<?php echo site_url('info/save') ?>" method="post" role="form">
          <div class='box-body pad'>
            <div class="form-group">
              <label>Judul :</label>
              <div class="input-group">
                <div class="input-group-addon"><i class="fa fa-newspaper-o"></i></div>
                <input name="judul" type="text" id="" class="form-control" />
              </div>
            </div>
            <div class="form-group">
              <label>Gambar :</label>
              <div class="input-group">
                <div class="input-group-addon"><i class="fa fa-image"></i></div>
                <input name="gambar" type="text" id="" class="form-control" />
              </div>
            </div>
            <div class="form-group">
              <label>Sumber :</label>
              <div class="input-group">
                <div class="input-group-addon"><i class="fa fa-external-link"></i></div>
                <input name="sumber" type="text" id="" class="form-control" />
              </div>
            </div>
            <div class="form-group">
              <label>Artikel :</label>
              <textarea id="editor1" name="artikel" rows="10" cols="80">This is my textarea to be replaced with CKEditor.</textarea>
            </div>
          </div>
          <div class="box-footer">
            <div class=" pull-right">
              <input name="simpan" type="submit" id="simpan" class="btn btn-primary" value="Simpan">
            </div>
          </div>
        </form>
      </div><!-- /.box -->

    </div><!-- /.col-->
  </div><!-- ./row -->
</section><!-- /.content -->

<script src="<?php echo base_url();?>assets/plugins/ckeditor/ckeditor.js"></script>
<script type="text/javascript">
  $(function () {
    CKEDITOR.replace('editor1');
  });
</script>
