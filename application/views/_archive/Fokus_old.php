<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Fokus extends CI_Controller {

	var $param;	//Create a variable for the entire controller

	function __construct() {
		parent::__construct();
        $this->load->model('dsw_model');
	}

	public function index() {
        if (! $this->session->userdata('isLoggedIn') ) redirect("login/show_login");
		$this->menu();
	}

	public function menu() {
		$menu = $this->uri->segment(1);

		$data['menu'] = $this->dsw_model->get_menu( $menu );
		$data['bread']= array('header'=>'Fokus DJA', 'subheader'=>'Fokus DJA');
		$data['view'] = "fokus/bidang";
		$this->load->view('main/utama', $data);
		// print_r($data['menu']);
	}

	public function tes() {
		// echo "string";exit;
		$menu = $this->uri->segment(3);
		if (empty($menu)) $menu = '200';
		$data['info'] = $this->info_model->get_info();
	//	$data['menu'] = $this->info_model->get_menu();
	//	$data['side'] = $this->info_model->get_sidebar( $menu );
		$data['bread']= array('header'=>'Fokus DJA', 'subheader'=>'Fokus DJA');
		$data['view'] = "fokus/bidang";
		$this->load->view('main/utama', $data);
		// print_r($data['menu']);
	}


}
