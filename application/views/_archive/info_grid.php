<script src="<?=base_url('assets/plugins/slimScroll/jquery.slimscroll.min.js'); ?>" type="text/javascript"></script>    
<script src="<?=base_url('assets/plugins/jQueryUI/jquery-ui.1.11.4.min.js'); ?>" type="text/javascript"></script>

<style media="screen">
  th {
    text-align: center;
  }
  thead tr {
    color: #fff;
    background: #00acd6;
  }

  #iGrid .ui-selecting { background: #FECA40; }
  #iGrid .ui-selected { background: #F39814; color: white; }
</style>

<div class="row">
  <div class="col-md-12">
    <div class="box box-warning">
      <div class="box-header with-border">
        <div class="well well-sm"> 
          <form id="iForm" class="form-inline" role="form" action="" method="post">
            <div class="form-group pull-right">
              <div class="input-group">
                <input class="form-control" id="cari" type="text">
                <span class="input-group-btn">
                  <button class="btn btn-info btn-flat" type="button"><i class="fa fa-search"></i></button>
                </span>
                <span class="input-group-btn">
                  <button class="btn btn-info btn-flat" type="button"><i class="fa fa-times-circle"></i></button>
                </span>
              </div>
            </div>
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-btn">
                  <span class="btn btn-info">Detail</span>
                  <a href="<?php echo site_url('info/rekam') ?>" id="addrow" class="btn btn-default">Rekam</a>
                  <button type="button" id="editrow" class="btn btn-default" data-toggle="modal" data-target="#myModal">Ubah</button>
                  <button type="button" id="delrow" class="btn btn-default" data-toggle="modal" data-target="#myModal">Hapus</button>
                </span>
              </div>
            </div>
          </form>
        </div>          
      </div><!-- /.box-header -->
      <div class="box-body">
        <table id="iGrid" class="table table-hover table-bordered">
          <thead>
            <tr>
              <th style="width: 3%">No.</th>
              <th style="width:15%">Judul</th>
              <th>Artikel</th>
              <th style="width:15%">Gambar</th>
              <th style="width:15%">Sumber</th>
            </tr>
          </thead>
          <tbody>
          <tr>
            <?php 
            $no = 0;
            foreach ($data as $row) { 
              $no++;
              $artikel = explode('<hr />', $row['artikel']);
            ?>
              <tr>
              <td class="text-left"><?php echo $no ?></td>
              <td class="text-left"><?php echo $row['judul'] ?></td>
              <td class="text-left"><?php echo $artikel[0] ?></td>
              <td class="text-left"><?php echo $row['gambar'] ?></td>
              <td class="text-left"><?php echo $row['sumber'] ?></td>
              <tr>
            <?php } ?>
          </tbody>
        </table>
      </div><!-- /.box-body -->
      <div class="box-footer clearfix">
        <?php echo $this->pagination->create_links(); ?>
      </div>
    </div><!-- /.box -->
  </div><!-- /.col -->
</div><!-- /.row -->

<script type="text/javascript">
  $('.pagination').addClass('pagination-sm no-margin pull-right');
</script>

<script type="text/javascript">
  $(function() {

    // SELECTED ROW - iGRID
    function iGridLink( $selectees ) {
      selected = $.makeArray( $selectees.filter( ".ui-selected" ) );
      selected.reduce( function( a, b ) {
        // document.getElementById("kduser").value = $(b).children( "td:nth-child(2)" ).text();
        }, 0 
      );
     }

    $( "#iGrid tbody" ).selectable({
          filter: ":not(td)",
          create: function( e, ui ) {
              iGridLink( $() );
          },
          selected: function( e, ui ) {
              var widget = $(this).find('.ui-selected');
              $(ui.unselected).addClass("info");
              iGridLink( widget );
          },
          unselected: function( e, ui ) {
              $(ui.unselected).removeClass("info");
              var widget = $(this).find('.ui-selected');
              iGridLink( widget );
          }
    });

    // ONCLICK untuk Rekam / Ubah / Hapus
    $("#addrow").on("click", function() {
      document.getElementById("kduser").value = ''; document.getElementById("nmuser").value = '';
      document.getElementById("passuser1").value = '';document.getElementById("passuser2").value = '';
      document.getElementById("kdlevel").value = '3';  document.getElementById("kdwenang").value = ''; 
      document.getElementById("nohp").value = '';   document.getElementById("simpan").value = 'Tambah';
    });
    $("#editrow").on("click", function() {
      document.getElementById("simpan").value = 'Ubah';
    });
    $("#delrow").on("click", function() {
      document.getElementById("simpan").value = 'Hapus';
    });
  }); 
</script>