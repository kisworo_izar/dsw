<script src="<?=base_url('assets/plugins/slimScroll/jquery.slimscroll.min.js'); ?>" type="text/javascript"></script>    
<script src="<?=base_url('assets/plugins/jQueryUI/jquery-ui.1.11.4.min.js'); ?>" type="text/javascript"></script>

<section class="content">
  <div class="example-modal">
    <div class="modal fade" id="myModal">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Rekam User</h4>
          </div>

          <form action="<?php echo site_url('user/save') ?>" method="post" role="form">
          <div class="modal-body">
 
            <div class="row">
              <div class="col-md-12">
                <div class="box-body">
                  <div class="form-group">
                    <div class="col-xs-12">
                      <input type="hidden" name="nilai" value="" />
                      <label class="control-label col-xs-3 hidden-phone" for="kduser">User ID</label>
                      <div class="col-xs-4" id="nouser">
                        <input name="kduser" type="text" id="kduser" class="form-control" value="">
                      </div>
                    </div>
                    <div class="col-xs-12">
                      <label class="control-label col-xs-3 hidden-phone" for="passuser">Password</label>
                      <div class="col-xs-4" id="nopass1">
                        <input name="passuser1" type="password" id="passuser1" class="form-control" value="">
                      </div>
                      <div class="col-xs-4" id="nopass2">
                        <input name="passuser2" type="password" id="passuser2" class="form-control" value="">
                      </div>
                    </div>
                    <div class="col-xs-12">
                      <label class="control-label col-xs-3 hidden-phone" for="nmuser">Nama User</label>
                      <div class="col-xs-8">
                        <input name="nmuser" type="text" id="nmuser" class="form-control" required value="">
                      </div>
                    </div>
                    <div class="col-xs-12">
                      <label class="control-label col-xs-3 hidden-phone" for="kdso">Eselon</label>
                      <div class="col-xs-8">
                        <select name="kdso" id="kdso" class="form-control">
                          <?php foreach ($kdso as $row) { echo "<option value='".$row['kdso']."'>".$row['nmso']."</option>"; } ?>
                        </select>
                      </div>
                    </div>
                    <div class="col-xs-12">
                      <label class="control-label col-xs-3 hidden-phone" for="kdlevel">Level</label>
                      <div class="col-xs-4">
                        <select name="kdlevel" id="kdlevel" class="form-control">
                          <?php foreach ($level as $row) { echo "<option value='".$row['kdlevel']."'>".$row['nmlevel']."</option>"; } ?>
                        </select>
                      </div>
                    </div>
                    <div class="col-xs-12">
                      <label class="control-label col-xs-3 hidden-phone" for="kdwenang">Wenang</label>
                      <div class="col-xs-4">
                        <select name="kdwenang" id="kdwenang" class="form-control">
                          <option value="1">Read Write</option>
                          <option value="2">Read Only</option>
                        </select>
                      </div>
                    </div>
                    <div class="col-xs-12">
                      <label class="control-label col-xs-3 hidden-phone" for="nohp">No. HP</label>
                      <div class="col-xs-4">
                        <input name="nohp" type="text" id="nohp" class="form-control" value="">
                      </div>
                    </div>
                  </div>
              </div>  <!--/.col (right) -->
            </div>  <!-- /.row -->

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
            <input name="simpan" type="submit" id="simpan" class="btn btn-primary" value="Simpan">
          </div>
          </form>

        </div>
      </div>
    </div>
  </div>
</section>
<script type="text/javascript">
  $(function() {

    // SELECTED ROW - iGRID
    function iGridLink( $selectees ) {
      selected = $.makeArray( $selectees.filter( ".ui-selected" ) );
      selected.reduce( function( a, b ) {
        document.getElementById("kduser").value = $(b).children( "td:nth-child(2)" ).text();
        document.getElementById("nmuser").value = $(b).children( "td:nth-child(3)" ).text();
        document.getElementById("kdlevel").value = $(b).children( "td:nth-child(4)" ).attr('id');
        document.getElementById("kdwenang").value = $(b).children( "td:nth-child(5)" ).attr('id');
        document.getElementById("nohp").value = $(b).children( "td:nth-child(6)" ).text();
        }, 0 
      );
     }

    $( "#iGrid tbody" ).selectable({
          filter: ":not(td)",
          create: function( e, ui ) {
              iGridLink( $() );
          },
          selected: function( e, ui ) {
              var widget = $(this).find('.ui-selected');
              $(ui.unselected).addClass("info");
              iGridLink( widget );
          },
          unselected: function( e, ui ) {
              $(ui.unselected).removeClass("info");
              var widget = $(this).find('.ui-selected');
              iGridLink( widget );
          }
    });

    // ONCLICK untuk Rekam / Ubah / Hapus
    $("#addrow").on("click", function() {
      document.getElementById("kduser").value = ''; document.getElementById("nmuser").value = '';
      document.getElementById("passuser1").value = '';document.getElementById("passuser2").value = '';
      document.getElementById("kdlevel").value = '3';  document.getElementById("kdwenang").value = ''; 
      document.getElementById("nohp").value = '';   document.getElementById("simpan").value = 'Tambah';
    });
    $("#editrow").on("click", function() {
      document.getElementById("simpan").value = 'Ubah';
    });
    $("#delrow").on("click", function() {
      document.getElementById("simpan").value = 'Hapus';
    });
  }); 
</script>