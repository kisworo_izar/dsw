<?php
class M_contact extends CI_Model {
	public function get_data() {
		$whr  = "";
		$cari = $this->session->userdata('cari');

		if ($cari) {
			$whr = " and " ;
			$arr = explode(' ', $cari);
			for ($i=0; $i<count($arr); $i++) {
				$str  = $arr[$i];
				$whr .= "nmuser like '%$str%' or nip like '%$str%' ";
				if ($i<count($arr)-1) {
					$whr .= " or ";
				}
			}
		}

		// cari query data dari t_user
		$query = $this->db->query("Select *, b.nmso, b.intern, b.ekstern From t_user a Left Join t_so b On a.kdso=b.kdso where kdpeg='1' $whr order by a.nmuser ");
		$hasil = $this->fc->ToArr( $query->result_array(), 'iduser');
		return $hasil;
	}

}
