<!DOCTYPE html>
<html>
<head>
  <?php $this->load->view('main/utama_link'); ?>
  <style type="text/css">
    .col-center-block {
      float: none;
      display: block;
      margin: 0 auto;
      margin-top: 50px !important; 
    }
    .box-body { border-top: 1px solid #f2f2f2; }
  </style>
</head>
<body>

  <?php 
    $status = array('Ditolak','Proses','Selesai','','','','','','',''); 
    $rev_id = '';  if ($revisi) $rev_id = $revisi['rev_id'];
  ?>

  <div class="row">
    <div class="col-md-10 col-center-block">
      <div class="box box-widget with-border">
        <div class="box-body">

          <form class="form-horizontal" method="post" action="<?= site_url('revisi/tracking/result') ?>" role="form">
            <div class="col-sm-12 text-center" style="padding:0px;background:#3C8DBC;margin:5px 0px 15px 0px">
              <label class="checkbox-custom-label" style="color:#FFF;padding:5px 15px">TRACKING REVISI ANGGARAN</label>
            </div>

            <div class="form-group jarak awal">
              <div class="col-md-6 col-center-block text-center">
                <div class="form-group form-inline">                            
                  <label for="rev_id">No. Tiket : &nbsp;</label>
                  <input type="text" name="rev_id" id="rev_id" class="form-control" placeholder="ketik no tiket revisi" value="<?= $rev_id ?>">
                </div>            
              </div>
            </div>
          </form>


          <?php if ($revisi) { ?>
          <form class="form-horizontal" role="form">
            <div class="col-sm-12 text-center" style="padding:0px;background:#3C8DBC;margin:5px 0px 15px 0px">
              <label class="checkbox-custom-label" style="color:#FFF;padding:5px 15px">KEMENTERIAN / LEMBAGA</label>
            </div>

            <div class="form-group jarak awal">
              <label class="col-sm-2 rev text-right">K/L : </label>
              <div class="col-sm-8" style="padding:0px">
                <input class="form-control" disabled value="<?= $revisi['kl_dept'] .' '. $revisi['nmdept'] ?>" />
              </div>
            </div>

            <div class="form-group jarak awal" id="tab-unit">
              <label class="col-sm-2 rev text-right">Unit Eselon I : </label>
              <div class="col-sm-8" style="padding:0px">
                <input class="form-control" disabled value="<?= $revisi['kl_unit'] .' '. $revisi['nmunit'] ?>" />
              </div>
            </div>

            <div class="form-group jarak">
                <label class="col-sm-2 rev text-right">Pejabat : </label>
                <div class="col-sm-8" style="padding:0px">
                  <input class="form-control" disabled value="<?= $revisi['kl_pjb_jab'] ?>" />
                </div>
            </div>

            <div class="form-group jarak">
              <label class="col-sm-2 rev text-right">No. Surat : </label>
              <div class="col-sm-3" style="padding:0px">
                <input class="form-control" disabled value="<?= $revisi['kl_surat_no'] ?>" >
              </div>
              <label class="col-sm-2 rev text-right">Tanggal Surat : </label>
              <div class="col-sm-3" style="padding:0px">
                <input class="form-control" disabled value="<?php echo $this->fc->idtgl( $revisi['kl_surat_tgl'], 'hari') ?>" >
              </div>
            </div>
            <div class="form-group jarak">
              <label class="col-sm-2 rev text-right">Hal : </label>
              <div class="col-sm-8" style="padding:0px">
                <input class="form-control" disabled value="<?= $revisi['kl_surat_hal'] ?>" >
              </div>
            </div>

            <div class="col-sm-12 text-center" style="padding:0px;background:#3C8DBC;margin:5px 0px 15px 0px">
              <label class="checkbox-custom-label" style="color:#FFF;padding:5px 15px">TRACKING REVISI</label>
            </div>

            <div class="form-group jarak awal">
              <label class="col-sm-2 rev text-right">Diterima Puslay : </label>
              <div class="col-sm-3" style="padding:0px">
                <input class="form-control" disabled value="<?php echo $this->fc->idtgl( $revisi['pus_tgl'], 'full') ?>" >
              </div>
            </div>

            <?php $status2=$revisi['t2_status']; if ($revisi['t3_status']) $status2=$revisi['t3_status']; if ($revisi['t4_status']) $status2=$revisi['t4_status']; ?>
            <?php $tanggal2=$revisi['t2_selesai_tgl']; if ($revisi['t3_selesai_tgl']) $tanggal2=$revisi['t3_selesai_tgl']; if ($revisi['t4_selesai_tgl']) $tanggal2=$revisi['t4_selesai_tgl']; ?>
            <div class="form-group jarak awal">
              <label class="col-sm-2 rev text-right">Penelaahan : </label>
              <div class="col-sm-3" style="padding:0px">
                <input class="form-control" disabled value="<?php echo $this->fc->idtgl( $revisi['t2_proses_tgl'], 'full') ?>" >
              </div>
              <label class="col-sm-1 rev text-right">s.d : </label>
              <div class="col-sm-3" style="padding:0px">
                <input class="form-control" disabled value="<?php echo $this->fc->idtgl( $tanggal2, 'full') ?>" >
              </div>
              <div class="col-sm-1" style="padding:0px">
                <input class="form-control" disabled value="<?= $status[ $status2 ] ?>" >
              </div>
            </div>

            <div class="form-group jarak awal">
              <label class="col-sm-2 rev text-right">Perbaikan  : </label>
              <div class="col-sm-3" style="padding:0px">
                <input class="form-control" disabled value="<?php echo $this->fc->idtgl( $revisi['t5_proses_tgl'], 'full') ?>" >
              </div>
              <label class="col-sm-1 rev text-right">s.d : </label>
              <div class="col-sm-3" style="padding:0px">
                <input class="form-control" disabled value="<?php echo $this->fc->idtgl( $revisi['t5_selesai_tgl'], 'full') ?>" >
              </div>
              <div class="col-sm-1" style="padding:0px">
                <input class="form-control" disabled value="<?php if($revisi['t5_proses_tgl']) echo $status[ $revisi['t5_status'] ]; ?>" >
              </div>
            </div>

            <div class="form-group jarak awal">
              <label class="col-sm-2 rev text-right">Dok. Lengkap  : </label>
              <div class="col-sm-3" style="padding:0px">
                <input class="form-control" disabled value="<?php echo $this->fc->idtgl( $revisi['t6_proses_tgl'], 'full') ?>" >
              </div>
              <label class="col-sm-1 rev text-right">s.d : </label>
              <div class="col-sm-3" style="padding:0px">
                <input class="form-control" disabled value="<?php echo $this->fc->idtgl( $revisi['t6_selesai_tgl'], 'full') ?>" >
              </div>
              <div class="col-sm-1" style="padding:0px">
                <input class="form-control" disabled value="<?php if($revisi['t6_proses_tgl']) echo $status[ $revisi['t6_status'] ]; ?>" >
              </div>
            </div>

            <div class="form-group jarak awal">
              <label class="col-sm-2 rev text-right">Penetapan  : </label>
              <div class="col-sm-3" style="padding:0px">
                <input class="form-control" disabled value="<?php echo $this->fc->idtgl( $revisi['t7_proses_tgl'], 'full') ?>" >
              </div>
              <label class="col-sm-1 rev text-right">s.d : </label>
              <div class="col-sm-3" style="padding:0px">
                <input class="form-control" disabled value="<?php echo $this->fc->idtgl( $revisi['t7_selesai_tgl'], 'full') ?>" >
              </div>
              <div class="col-sm-1" style="padding:0px">
                <input class="form-control" disabled value="<?php if($revisi['t6_proses_tgl']) echo $status[ $revisi['t7_status'] ]; ?>" >
              </div>
            </div>
          </form>        
          <?php } ?>
        
        </div>
      </div>
    </div>
  </div>

</body>
</html>