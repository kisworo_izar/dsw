<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Forum1 extends CI_Controller {

	var $param;	//Create a variable for the entire controller

	function __construct() {
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
    	$this->load->model('forum_model');
        $this->session->set_userdata( array('cari' => '') );
	}

	public function index() {
    	if (! $this->session->userdata('isLoggedIn') ) redirect("login/show_login");

		// Find Lastest Thread
		$query = $this->db->query("Select tglpost, idparent From d_forum Order By tglpost Desc Limit 1");
		$latest= $query->row();

		// Then Find Sub Kategori of Latest Thread
		$query = $this->db->query("Select idparent, idchild From d_forum_room Where idchild=$latest->idparent");
		$thread= $query->row();

		$this->forum_grid( 1, $thread->idparent );
	}

	public function forum_grid($idparent, $idchild, $idgrand=null) {
		$data['d_room']= $this->forum_model->sql_room();
		$data['d_head']= $this->forum_model->sql_head($idparent, $idchild);
		$data['d_sub'] = $this->forum_model->sql_sub($idparent, $idchild);

		$thread = $this->forum_model->sql_thread( $idparent, $idchild );
		if ($idgrand!=null) {
			foreach ($thread as $key=>$value) { if (substr($key,0,2)!=$idgrand) unset( $thread[$key] ); }
		}
		$data['thread'] = $thread;

		$data['menu']  = $this->dsw_model->get_menu( $this->uri->segment(1) );
		$data['bread'] = array('header'=>'Diskusi DJA','subheader'=>'Diskusi DJA');
		$data['view']  = "forum/v_forum";
		$this->load->view('main/utama', $data);
	}

	public function forum_post($idparent, $idchild) {
		$row = $this->forum_model->post_reply($idparent, $idchild);
		$config = $this->fc->pagination( site_url("forum/forum_post/$idparent/$idchild/"), count($row), 10, '5');
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		$this->pagination->initialize($config);

		$data['idparent'] = $idparent; $data['idchild'] = $idchild;
		$data['d_post'] = array_slice($row, $this->uri->segment(5), 10);
 		$data['quote'] = null;
		$data['url'] = "forum/forum_post/$idparent/$idchild";

		$data['menu']  = $this->dsw_model->get_menu( $this->uri->segment(1) );
		$data['bread'] = array('header'=>'Forum','subheader'=>'Forum');
		$data['view']  = "forum/v_forum_thread";
		$this->load->view('main/utama', $data);
	}

	public function tanggapan($idparent, $idchild) {
		$iduser	 = $this->session->userdata('iduser');
		$comment = $_POST['comment'];
		$nmfile  = $_POST['nmfile'];

		$query = $this->db->query("insert into d_forum (idparent,idchild,iduser,tglpost,post,attach) values ($idparent,$idchild,'$iduser',current_timestamp(),\"$comment\",\"$nmfile\")");
		redirect( site_url("forum/forum_post/$idparent/$idchild") );
	}

	public function create_thread($action=null, $idparent=null, $idchild=null) {
		$ruh = 'Posting'; if ($this->uri->segment(3)=='u') $ruh = 'Ubah'; if ($this->uri->segment(3)=='h') $ruh = 'Hapus'; if ($this->uri->segment(3)=='c') $ruh = 'Catatan';
		$data['ruh'] = $ruh;
		$data['pic'] = $this->forum_model->get_pic();
		$data['group'] = $this->forum_model->room_option($idparent, $idchild);

		if ($ruh=='Ubah' or $ruh=='Catatan')  {
			$data['d_room'] = $this->forum_model->post_room($idparent);
			$data['d_head'] = $this->forum_model->post_head($idparent, $idchild);
			$data['group']  = $this->forum_model->room_option($data['d_room']['idparent'], $data['d_room']['idchild']);
		}

		$data['menu']  = $this->dsw_model->get_menu( $this->uri->segment(1) );
		$data['bread'] = array('header'=>'Forum','subheader'=>'Buat Thread');
		$data['view']  = "forum/v_create_thread";
		$this->load->view('main/utama', $data);
		// echo "<pre>"; print_r($data['group']);
	}

	public function ajax_pic() {
		$msg = $this->forum_model->ajax_pic( $_POST['idkey'] );
		echo $msg;
	}

	public function save_thread() {
		$url = $this->forum_model->save_thread();
		redirect( "$url" );
	}

	public function test($idparent=null, $idchild=null) {
		echo chr(rand(97,122)) . chr(rand(97,122)) . rand();
	}



	public function save_reply($idparent, $idchild) {
		$url	= $_POST['url'];
		$iduser	= $this->session->userdata('iduser');
		$reply  = str_replace('"', "'", $_POST['reply']);

		$query = $this->db->query("insert into d_forum (idparent,idchild,iduser,tglpost,post) values ($idparent,$idchild,'$iduser',current_timestamp(),\"$reply\")");
		redirect( site_url($url) );
	}

	public function upload_image() {
	    $allowed = array('png', 'jpg', 'gif','zip');
	    if(isset($_FILES['file']) && $_FILES['file']['error'] == 0) {
	     	$extension = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
	     	if(!in_array(strtolower($extension), $allowed)) {
	     		echo '{"status":"error"}';
	     		exit;
	    	}

	    	if(move_uploaded_file($_FILES['file']['tmp_name'],'files/forum/'.$_FILES['file']['name'])) {
	     		$tmp = 'images/'.$_FILES['file']['name'];
	     		$new = '../images/'.$_FILES['file']['name']; //adapt path to your needs;
	     		if(copy($tmp,$new)){
	     			echo 'images/'.$_FILES['file']['name'];
			    }
	     		exit;
	    	}
	    }
	    echo '{"status":"error"}';
	    exit;
	}

	public function forum_reply($idparent, $idchild) {
		$row = $this->forum_model->post_reply($idparent, $idchild);
		$config = $this->fc->pagination( site_url("forum/forum_post/$idparent/$idchild/"), count($row), 10, '5');
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		$this->pagination->initialize($config);

		$data['d_room']= $this->forum_model->post_room($idparent);
		$data['d_head']= $this->forum_model->post_head($idparent, $idchild);
		$data['d_post']= array_slice($row, $this->uri->segment(5), 10);
		$data['d_top'] = $this->forum_model->sql_top();
		$data['d_last']= $this->forum_model->sql_last();

		$data['menu']  = $this->dsw_model->get_menu( $this->uri->segment(1) );
		$data['bread'] = array('header'=>'Forum','subheader'=>'Forum');
		$data['view']  = "forum/v_forum_post";
		$this->load->view('main/utama', $data);
	}

	public function fileupload() {
		if (empty($_FILES['files'])) {
		    echo json_encode( array('error'=>'No files found for upload.') );
		    return;
		}
		$attach = $_FILES['files'];
		$success = null;
		$paths = array();
		$filenames = $attach['name'];

		// loop and process files
		for($i=0; $i < count($filenames); $i++){
			// $user  = $this->session->userdata('iduser');
			// $query = $this->db->query("Insert Into d_upload (tglupload,nmfile,iduser) Values (Current_TimeStamp(),'". $filenames[$i] ."','$user')");
			// $query = $this->db->query("Select Max(idupload) idupload From d_upload");
			// $idmax = $query->row_array();

		    $target = "files/attach" .DIRECTORY_SEPARATOR. sprintf('%06d',$idmax['idupload']) ."-". $filenames[$i];
		    if(move_uploaded_file($attach['tmp_name'][$i], $target)) {
		        $success = true;
		        array_push($paths, $target);
		    } else {
		        $success = false;
		        break;
		    }
		}

		// check and process based on successful status
		if ($success === true) {
		    $output = array('path' => $paths);
		} elseif ($success === false) {
		    $output = array('error'=>'Error while uploading Excel File. Contact the system administrator');
		    foreach ($paths as $file) {
		        unlink($file);
		    }
		} else {
		    $output = array('error'=>'No files were processed.');
		}
		echo json_encode($output);
 	}

	public function ajax_reply( $id=null ) {
		$msg =  "
					<div id=\"div-summer\">
						<div class=\"form-group\">
				            <label>Reply :</label>
				            <textarea id=\"summernote\" name=\"reply\"></textarea>
				        </div>
						<div class=\"input-group-btn\">
							<button name=\"simpan\" type=\"submit\" class=\"btn btn-xm btn-primary pull-right\">Reply</button>
						</div>
					</div>

					<script src=\"http://localhost/dsw/assets/plugins/summernote/summernote.min.js\"></script>
					<link rel=\"stylesheet\" href=\"http://localhost/dsw/assets/plugins/summernote/summernote.css ?>\">

				";
		// $msg =  $this->load->view('forum/v_forum_reply');
		if ($id==null) { return $msg; } 
			else echo $msg;
	}

 	public function reply($idparent, $idchild, $idforum=null) {
 		$data['quote'] = null;
 		if ($idforum) {
			$query = $this->db->query("Select a.iduser, nmuser, nip, post From d_forum a Left Join t_user b On a.iduser=b.iduser Where idparent=$idparent And idchild=$idchild And idforum=$idforum");
			$data['quote'] = $query->row_array();
		}

		$data['idparent']= $idparent;
		$data['idchild'] = $idchild;
		$data['url'] 	 = "home1/menu";

		$data['menu']  = $this->dsw_model->get_menu( $this->uri->segment(1) );
		$data['bread'] = array('header'=>'Forum','subheader'=>'Forum');
		$data['view']  = "forum/v_forum_reply";
		$this->load->view('main/utama', $data);
	}

}
