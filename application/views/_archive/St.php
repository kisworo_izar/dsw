<?php
class St extends CI_Controller {
	function __construct() {
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
	    $this->load->model('dsw_model');
	    $this->load->model('St_model');
	    $this->load->model('st_lampiran_model');
	}

	public function index() {
    	if (! $this->session->userdata('isLoggedIn') ) redirect("login/show_login");
		$this->grid(7, 'data', '01');
	}

	public function grid() {
		$row = $this->St_model->get_data();
		$config = $this->fc->pagination( site_url("st/grid"), count($row), 10, '3');
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
	    $config['cur_tag_close'] = '</a></li>';
		$this->pagination->initialize($config);

		$data['table'] = array_slice($row, $this->uri->segment(3), 10);
		$data['menu']  = $this->dsw_model->get_menu( $this->uri->segment(1) );
		$data['bread'] = array('header'=>'Admin Surat Tugas', 'subheader'=>'Admin Surat Tugas');
		$data['view']  = "st/V_st_grid";
		$this->load->view('main/utama', $data);
	}

	function rekam() {
		$ruh = 'Rekam'; if ($this->uri->segment(3)=='u') $ruh = 'Ubah'; if ($this->uri->segment(3)=='h') $ruh = 'Hapus';
		$data['ruh']   = $ruh;
		$data['table'] = $this->St_model->get_row( $this->uri->segment(4) );
		//session seselon II user
		$es=substr($this->session->userdata('kdso'),0,2);
		$data['pjb'] = $this->St_model->get_pjb($es); //var_dump($data['pjb']);exit() ;
		$data['menu']  = $this->dsw_model->get_menu( $this->uri->segment(1) );
		$data['bread'] = array('header'=>'Admin Surat Tugas', 'subheader'=>'Admin Surat Tugas');
		$data['view']  = "st/V_st_rekam";
		$this->load->view('main/utama', $data);
	}

	function crud() {
		$this->St_model->save();
		redirect("St/grid");

	}

	function cari() {
		if ($_POST['search'] == 'search') {
			$this->session->set_userdata('cari', $_POST['cari']);
		} else {
			$this->session->set_userdata('cari', '');
		}

		if ($_POST['nmfunction']=='dash') {
			$this->dash();
		} else {
			$this->grid();
		}
	}

	public function dash() {
		$row = $this->St_model->get_data();
		$config = $this->fc->pagination( site_url("slideshow/dash"), count($row), 10, '3');
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
	    $config['cur_tag_close'] = '</a></li>';
		$this->pagination->initialize($config);

		$data['slideshow'] = array_slice($row, $this->uri->segment(3), 10);

		$data['menu']  = $this->dsw_model->get_menu( $this->uri->segment(1) );
		$data['bread'] = array('header'=>'Arsip slideshow', 'subheader'=>'Slideshow');
		$data['view'] = "slideshow/V_st_list";
		$this->load->view('main/utama', $data);
		// $this->fc->browse( $data['nilai'] );

	}


	function pdf( $idst ) {
		$data['st']  = $this->St_model->get_st( $idst );
		//session seselon II user
		$es=substr($this->session->userdata('kdso'),0,2) ; //echo $a; 
		$data['dir']  = $this->St_model->get_dir($es);
		$data['almt1']  = $this->St_model->get_alm1($es);
		$data['almt2']  = $this->St_model->get_alm2($es);		
		$data['menu']  = $this->dsw_model->get_menu( $this->uri->segment(1) );
		$data['bread'] = array('header'=>'Admin Surat Tugas', 'subheader'=>'Admin Surat Tugas');
		$data['view']  = "st/cetakst";
		$this->load->view('st/cetakst', $data);
		
		$html = $this->output->get_output();

		// Convert to PDF
		$this->dompdf = new DOMPDF();
		$this->dompdf->load_html($html);
		$this->dompdf->set_paper('A4','portraite');
		$this->dompdf->render();
		$this->dompdf->stream('cetakst'.'.pdf',array('Attachment'=>0));
	}

}