<?php
class St_model extends CI_Model {

	public function get_data() {
		$whr  = "";
		$cari = $this->session->userdata('cari');

		if ($cari) {
			$whr = " where " ;
			$arr = explode(' ', $cari);
			for ($i=0; $i<count($arr); $i++) {
				$str  = $arr[$i];
				$whr .= " keterangan like '%$str%' or gambar like '%$str%' ";
				if ($i<count($arr)-1) {
					$whr .= " or ";
				}
			}
		}

		// cari query data dari d_paket
		$query = $this->db->query("SELECT *  from d_st $whr order by idst desc ");
		$hasil = $this->fc->ToArr( $query->result_array(), 'idst');
		return $hasil;
	}

	public function get_row( $idst=null ) {
		if ( is_null($idst) ) $idst='999999';
		$query = $this->db->query("select *  from d_st where idst=$idst");
		$hasil = $query->row_array();
		return $hasil;
	}

	public function get_pjb( $es) {
		$query = $this->db->query("SELECT * FROM t_user WHERE LEFT(jabatan,3)='DIR' AND LEFT(kdso,2)='$es'; ");
		$hasil = $query->result_array();
		return $hasil;
	}

	public function save(  ) {
		$action	= $_POST['ruh'];
		$idst	= $_POST['idst'];
		$jnsst	= $_POST['jnsst'];
		$tglst  = $_POST['tglst'];
		$nost	= $_POST['nost'];
		$hal	= $_POST['hal'];
		$tgawal	= $_POST['tgl_awal']; 
		$tgakhir= $_POST['tgl_akhir']; 
		$nippjb= $_POST['nippjb']; 

		/*if ($gambar) move_uploaded_file($gambar["tmp_name"], "files/slideshow/".$gambar['name']);*/

		if ($action=='Rekam') {
			$this->db->query( "INSERT INTO d_st (tglst,nost,halst,tgawal,tgakhir,jns_st,ttdnip) 
									values('$tglst','$nost','$hal','$tgawal','$tgakhir','$jnsst','$nippjb') " );
		}

		if ($action=='Ubah') {
			$this->db->query( "UPDATE d_st SET tglst='$tglst', 
												nost='$nost', 
												halst='$hal', 
												tgawal='$tgawal', 
												tgakhir='$tgakhir',
												ttdnip='$nippjb' 
										WHERE idst='$idst'" );
			//if ( $gambar['name'] != '' )  	$this->db->query("update d_slideshow set gambar='". $gambar['name'] ."' where idslideshow=$idslideshow" );
		}
		if ($action=='Hapus') {
			$this->db->query( "DELETE FROM d_st WHERE idst='$idst'" );
		}
		return;
	}
	
	public function get_st($idst){
	 	$query=$this->db->query("select *  from d_st where idst=$idst");
	 	return $query->row_array();
	}

	public function get_dir($es){
	 	$query=$this->db->query("SELECT nmso  FROM l_so WHERE SUBSTRING(kdso,1,2)='$es' AND nmso1!='' ");
	 	return $query->row_array();
	}

	public function get_alm1($es){
	 	$query=$this->db->query("SELECT header1  FROM l_so WHERE SUBSTRING(kdso,1,2)='$es' AND nmso1!='' ");
	 	return $query->row_array();
	 	
	}

	public function get_alm2($es){
	 	$query=$this->db->query("SELECT header2  FROM l_so WHERE SUBSTRING(kdso,1,2)='$es' AND nmso1!='' ");
	 	return $query->row_array();
	}
}
