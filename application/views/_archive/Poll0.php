<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Poll extends CI_Controller {

	function __construct() {
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
	    $this->load->model('m_poll');
	}

	public function index() {
        if (! $this->session->userdata('isLoggedIn') ) redirect("login/show_login");
		$this->session->set_userdata( array('cari' => '') );
		$this->menu();
	}

	public function menu() {
		$data['judul'] = $this->m_poll->get_judul();
		$data['poll']  = $this->m_poll->get_data();
		$data['menu']  = $this->dsw_model->get_menu( $this->uri->segment(1) );
		$data['bread'] = array('header'=>'&nbsp;', 'subheader'=>'Polling');
		$data['view']  = "poll/v_poll";
		$this->load->view('main/utama', $data);
	}

	public function hasil() {
		$data['hsl_graph'] = $this->m_poll->get_hasil();
		$data['hsl_data']  = $this->m_poll->get_hasil();
		$data['hsl_nama']  = $this->m_poll->get_nama();
		$data['hsl_item']  = $this->m_poll->get_item();
		$data['hsl_total'] = $this->m_poll->get_total();
		$data['hsl_jml']   = $this->m_poll->get_jml();

		$data['judul'] = $this->m_poll->get_judul();
		$data['menu']  = $this->dsw_model->get_menu( $this->uri->segment(1) );
		$data['bread'] = array('header'=>'Hasil Polling', 'subheader'=>'Polling');
		$data['view']  = "poll/v_poll_hasil";
		$this->load->view('main/utama', $data);
	}

	public function ajax_simpan() {
		$arr = $_POST['pil'];
		if ( count($arr)!=3 )  $msg = 'Silahkan pilih <B>3 (Tiga)</B> pilihan Anda.';
		if ( count($arr)==3 ) $msg = $this->m_poll->save_vote( $arr );
		echo $msg;
	}

	function cari() {
		if ($_POST['search'] == 'search') {
			$this->session->set_userdata('cari', $_POST['cari']);
		} else {
			$this->session->set_userdata('cari', '');
		}
		$this->menu();
	}

	public function grid() {
		$row = $this->m_poll->get_grid();
		$config = $this->fc->pagination( site_url("poll/grid"), count($row), 15, '3');
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		$this->pagination->initialize($config);

		$data['poll']= array_slice($row, $this->uri->segment(3), 15);
		$data['menu'] = $this->dsw_model->get_menu( $this->uri->segment(1) );
		$data['bread']= array('header'=>'Manajemen Polling', 'subheader'=>'Data Polling');
		$data['view'] = "poll/v_poll_grid";
		$this->load->view('main/utama', $data);
	}

	public function rekam() {
		$ruh = 'Rekam'; if ($this->uri->segment(3)=='u') $ruh = 'Ubah'; if ($this->uri->segment(3)=='h') $ruh = 'Hapus';
		$data['ruh']  = $ruh;
		$data['poll'] = $this->m_poll->get_row( $this->uri->segment(4) );

		$data['menu'] = $this->dsw_model->get_menu( $this->uri->segment(1) );
		$data['bread']= array('header'=>'Admin Polling', 'subheader'=>'Adminstrator');
		$data['view'] = "poll/v_poll_rekam";
		$this->load->view('main/utama', $data);
	}

	function crud() {
		$this->m_poll->save();
		redirect("poll/grid");
	}

	public function ajax_layer() {
		$idkey = $_POST['idkey'];
		$msg = $this->m_poll->get_child( $idkey );
		// $msg = 'ok';
		echo $msg;
	}

	function test() {
		$data['hsl_nama']	= $this->m_poll->get_total();
		$this->fc->browse( $data['hsl_nama'] );
	}

	function pdf() {
		ini_set('max_execution_time', 0); 
		ini_set('memory_limit','2048M');
		 $data['poll']  = $this->m_poll->get_cetak();
		 $data['menu']  = $this->dsw_model->get_menu( $this->uri->segment(1) );
		 $data['bread'] = array('header'=>'Hasil Polling', 'subheader'=>'Polling');
		 $data['view']  = "poll/cetakpdf";
		 $this->load->view('poll/cetakpdf', $data);
		
		 $html = $this->output->get_output();

		// // Convert to PDF
		 $this->dompdf = new DOMPDF();
		 $this->dompdf->load_html($html);
		 $this->dompdf->set_paper('A4','landscape');
		 $this->dompdf->render();
		 $this->dompdf->stream('cetaklampiran'.'.pdf',array('Attachment'=>0));

	}


}
