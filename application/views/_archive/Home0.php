<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

	var $param;	//Create a variable for the entire controller

	function __construct() {
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
        $this->load->model('dsw_model');
    	$this->load->model('forum_model');
	}

	public function index() {
        if (! $this->session->userdata('isLoggedIn') ) redirect("login/show_login");
		$this->menu();
	}

	public function menu() {
		$data['d_rss']      = $this->dsw_model->get_rss();
		$data['absensi']    = $this->dsw_model->get_absen();
		$data['d_moncap'] 	= $this->dsw_model->get_moncap();
		$data['slideshow'] 	= $this->dsw_model->get_slideshow();
		$data['pengumuman'] = $this->dsw_model->get_pengumuman();
		$data['d_surat']	= $this->dsw_model->get_surat();
		$data['d_top'] 		= $this->forum_model->sql_top(5);
		$data['d_last']		= $this->forum_model->sql_last(5);
		$data['d_forum']    = $this->dsw_model->get_forum();

		$data['menu'] = $this->dsw_model->get_menu( $this->uri->segment(1) );
		$data['bread']= array('header'=>'Dashboard', 'subheader'=>'Dashboard');
		$data['view'] = "main/dashboard";
		$this->load->view('main/utama', $data);
	}

	public function pengumuman() {
		$this->load->model('Pengumuman_model');
		$row = $this->Pengumuman_model->get_data();
		$data['pengumuman'] = array_slice($row, $this->uri->segment(3), 10);

		$config = $this->fc->pagination( site_url("home/pengumuman"), count($row), 10, '3');
		$this->pagination->initialize($config);

		$data['menu'] = $this->dsw_model->get_menu( $this->uri->segment(1) );
		$data['bread']= array('header'=>'Arsip Pengumuman', 'subheader'=>'Pengumuman');
		$data['view'] = "pengumuman/V_pengumuman_list";
		$this->load->view('main/utama', $data);
	}

	public function tanggapan() {
		$idparent = $this->uri->segment(3);
		$idchild  = $this->uri->segment(4);
		$iduser	  = $this->session->userdata('iduser');
		$comment  = $_POST['comment'];
		$nmfile   = $_POST['nmfile'];

		$query = $this->db->query("insert into d_forum (idparent,idchild,iduser,tglpost,post,attach) values ($idparent,$idchild,'$iduser',current_timestamp(),\"$comment\",\"$nmfile\")");
		redirect('home');
	}

	public function fileupload() {
		if (empty($_FILES['files'])) {
		    echo json_encode( array('error'=>'No files found for upload.') ); 
		    return; 
		}
		
		$images = $_FILES['files'];
		$success = null;
		$paths = array();
		$filenames = $images['name'];
		 
		// loop and process files
		for($i=0; $i < count($filenames); $i++){
		    $ext = explode('.', basename($filenames[$i]));
		    $target = "files/forum" . DIRECTORY_SEPARATOR . $filenames[$i]; 
		    if(move_uploaded_file($images['tmp_name'][$i], $target)) {
		        $success = true;
		        array_push($paths, $target);
		    } else {
		        $success = false;
		        break;
		    }
		}
		 
		// check and process based on successful status 
		if ($success === true) {
		    $output = array('path' => $paths);
		} elseif ($success === false) {
		    $output = array('error'=>'Error while uploading Excel File. Contact the system administrator');
		    foreach ($paths as $file) {
		        unlink($file);
		    }
		} else {
		    $output = array('error'=>'No files were processed.');
		}
		echo json_encode($output);
 	}

	public function test() {
		// $data['menu'] = $this->dsw_model->get_menu( $this->uri->segment(1) );
		// $this->fc->browse( $data['menu'] );
	}

	public function tanggapan2() {
		$idparent = $this->uri->segment(3);
		$idchild  = $this->uri->segment(4);
		$iduser	  = $this->session->userdata('iduser');
		$comment  = $_POST['comment'];

		$query = $this->db->query("insert into d_forum (idparent,idchild,iduser,tglpost,post) values ($idparent,$idchild,'$iduser',current_timestamp(),\"$comment\")");
		redirect('home');
	}

}
