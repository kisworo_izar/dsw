<link href="<?=base_url('assets/plugins/bxslider/jquery.bxslider.css');?>" rel="stylesheet" type="text/css" />

<script src="<?=base_url('assets/plugins/bxslider/jquery.bxslider.min.js'); ?>" type="text/javascript"></script>
<script src="<?=base_url('assets/plugins/jQuery/jQuery.Shorten.1.0.js'); ?>" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $(".more").shorten({
            "showChars" : 150,
            "moreText"  : "<i class='small'> Read More</i>",
            "lessText"  : " <i class='small'> Less</i>"
        });
    });
</script>

<script type="text/javascript">
$(document).ready(function(){
    $('.bxslider').bxSlider({
     mode: 'fade',
     pause: 7000,
     infiniteLoop: true,
     controls: false,
     pager: false,
     auto: true,
     randomStart: true,
     captions: true
   });
});
</script>

<div class="row">

    <div class="col-md-5">
      <div class="dsw-info-box">
        <span class="dsw-info-box-icon bg-yellow"><i class="fa fa-lightbulb-o"></i></span>
        <div class="dsw-info-box-content">
          <span class="dsw-info-box-text"><b>Perbaikan Berkelanjutan</b> - Ide Perbaikan</span>
          <a href="http://intranet.anggaran.depkeu.go.id/forum/forum_grid/1/4">
              <span class="dsw-info-box-text text-danger">Forum</span>
          </a>
        </div>
      </div>
    </div>


    <div class="col-md-4">
        <div class="dsw-info-box" style="padding-right:45px">
        <span class="dsw-info-box-icon bg-orange"><i class="fa fa-rss"></i></span>
            <div id="text-carousel" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner" style="position:absolute; left:45px">

                    <?php
                    $i=1;
                    foreach ($d_rss as $row) {
                    ?>
                        <div class="item <?php if ($i==1) echo 'active'?>">
                            <div class="carousel-content dsw-info-box-content" style="margin-left:0px">
                                <span class="dsw-info-box-text">
                                    <b><?php echo $row['site'] ?></b>  <?php echo $this->fc->idtgl($row['pubdate'],'full') ?>
                                </span>
                                <span class="dsw-info-box-text">
                                    <a href="<?php echo $row['link'] ?>" target="_blank"><?php echo $row['berita'] ?></a>
                                </span>
                            </div>
                        </div>
                    <?php
                        $i++;
                    }
                    ?>

                </div>
            </div>
        </div>
    </div>



  <div class="col-md-3">
    <div class="dsw-info-box">
        <?php
        if ($absensi) {
          $i=1;
          foreach ($absensi as $row) {
            if ($i==1) { ?>
                <?php if (strtotime($row['masuk']) < strtotime( substr($row['masuk'],0,11).'07:31:00' )) { $bgabsen='bg-green'; } else { $bgabsen='bg-red'; } ?>
                <span class="dsw-info-box-icon <?php echo $bgabsen ?>"> <a href="<?php echo site_url('absensi') ?>" style="color:white">
                    <i class="fa fa-clock-o"></i></a></span>
                <div class="dsw-info-box-content">
                    <span class="dsw-info-box-text"><b><?php echo $this->fc->idtgl($row['tanggal'],'hari'); ?></b></span>
                    <span class="dsw-info-box-number text-yellow"><?php echo substr($row['masuk'],11,5) ?> |
            <?php } else { ?>
                        <span class="small text-muted"><?php echo substr($row['masuk'],11,5) ?> - <?php echo substr($row['keluar'],11,2) + 12; echo substr($row['keluar'],13,3) ?></span>
                    </span>
            <?php }
            $i++;
          } ?>
        </div>
        <?php } else { echo '&nbsp;'; } ?>
    </div>
  </div>
</div>


<div class="box box-widget" style="margin-top:10px">
    <div class="box-header with-border">
        <h3 class="box-title"> Monitoring Capaian</h3>
        <div class="box-tools">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            <!-- <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button> -->
        </div>
    </div>
    <div class="box-body" style="padding-top:5px">
        <div class="row">
            <?php foreach ($d_moncap as $row) { ?>
            <div class="col-sm-4 col-md-2">
                <?php
                    if ( $row['progres'] < 50 ) { $warna='red'; }
                    elseif ( $row['progres'] < 75 ) { $warna='yellow'; }
                    else { $warna='green'; }
                ?>
                <h4 class="control-sidebar-subheading" style="margin-bottom:3px; margin-top:3px"><a href="<?php echo site_url('fokus/bidang/'.$row['id_bidang']) ?>">Bidang <b><?php echo $row['id_bidang'] ?></b></a>
                    <span class="text-muted pull-right"><b><?php echo $row['jumlah'] ?></b> <span class="hidden-md">Out.</span>
                        <span class="small"> &nbsp;<?php echo $row['progres'] ?>%</span>
                    </span>
                </h4>
                <!-- <div class="progress-bar progress-bar-danger" style="width: <?php echo $row['progres'] ?>%"></div> -->
                <!-- <div class="progress" style="margin-bottom:5px">
                   <div class="progress-bar progress-bar-<?php echo $warna ?>" role="progressbar" aria-valuemin="<?php echo $row['progres'] ?>" aria-valuemax="100" style="width:<?php echo $row['progres'] ?>%"><?php echo $row['progres'] ?>%</div>
               </div> -->

               <div class="progress xxs" style="margin-bottom:5px">
                  <div class="progress-bar progress-bar-<?php echo $warna ?>" role="progressbar" aria-valuemin="<?php echo $row['progres'] ?>" aria-valuemax="100" style="width:<?php echo $row['progres'] ?>%"></div>
               </div>


            </div>
            <?php } ?>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="box box-widget">
            <div class="bxslider">
                <?php foreach ($slideshow as $row) { ?>
                <li>
                    <a target='_blank' href="<?php echo $row['link'] ?>">
                        <!-- <img src="<?php echo site_url('files/slideshow').'/'. $row['gambar'] ?>" -->
                        <img src="<?php echo 'files/slideshow/'. $row['gambar'] ?>"
                        title="<?php echo $row['keterangan'] ?>"/>
                    </a>
                </li>
                <?php } ?>
            </div>
        </div>

        <div class="box box-widget" style="margin-bottom:0px;">
            <div class="box-header with-border">
                <!-- <h3 class="box-title" style="color:#DD4B39">Top Thread</h3> -->
                <span class="box-title">Forum</span>
                <div class="small box-tools" style="padding-top:5px">
                    <a href="<?php echo site_url('forum'); ?>"><i class="fa fa-link"></i></a>
                </div>
            </div>
        </div>

        <div class="box box-widget" style="margin-bottom:0px;">
            <div class="box-header with-border" style="border-radius:0px;border-left: 1px solid #DD4B39; padding-top:3px; padding-bottom:3px; background-color:#F7F7F7">
                <span class="box-title" style="color:#DD4B39">Top Thread</span>
            </div>
            <div class="box-body" style="padding:0px 10px 0px 10px">
                <ul class="products-list product-list-in-box">

                    <?php foreach ($d_top as $row) { ?>
                    <li class="item" style="padding:3px">
                        <div class="product-info" style="margin-left:0px">
                            <span class="small product-description">
                                <a href="<?php if ($this->session->userdata('idusergroup')!='999') echo site_url('forum/forum_post').'/'. $row['idparent'] .'/'. $row['idchild'] ?>"><?php echo $row['thread'] ?></a>
                                <span class="small text text-danger pull-right">Replies : <b><?php echo $row['reply']-1 ?></b></span>
                            </span>
                        </div>
                    </li>
                    <?php } ?>

                </ul>
            </div>
        </div>

        <div class="box box-widget">
            <div class="box-header with-border" style="border-radius:0px;border-left: 1px solid orange; padding-top:3px; padding-bottom:3px; background-color:#F7F7F7">
                <h3 class="box-title" style="color:orange">Last Post</h3>
            </div>

            <!-- <div class="box-body" style="padding:0px 10px 0px 10px">
                <ul class="products-list product-list-in-box">

                    <?php foreach ($d_last as $row) { ?>
                    <li class="item" style="padding:3px">
                        <div class="product-info" style="margin-left:0px">
                            <span class="small product-description">
                                <a href="<?php if ($this->session->userdata('idusergroup')!='999') echo site_url('forum/forum_post').'/'. $row['idparent'] .'/'. $row['idchild'] ?>"><?php echo $row['thread'] ?></a>
                                <span class="small text text-primary pull-right"><?php echo $this->fc->idtgl( $row['tglpost'], 'tgljam' ) ?></span>
                            </span>
                        </div>
                    </li>
                    <?php } ?>

                </ul>
            </div> -->

            <?php foreach ($d_forum as $row) { ?>
                <div class="box box-widget">
                    <div class="box-header with-border">
                        <div class="user-block">
                            <?php
                                $foto_profile="files/profiles/_noprofile.png";
                                if (file_exists("files/profiles/".$row['nip'].".gif")) {$foto_profile =  "files/profiles/".$row['nip'].".gif";}
                            ?>
                            <img class="img-circle img-bordered-dsw" src="<?php echo site_url($foto_profile); ?>">
                            <span class="username">

                                <span class="username" style="margin-left:0px">
                                    <span class="profile-tooltip">
                                        <span class="profile-tooltip-item"><?php echo $row['nmuser'] ?> </span>
                                            <span class="profile-tooltip-content"><img src="<?php echo site_url($foto_profile); ?>">
                                            <span class="profile-tooltip-text" style="font-size:18px; padding-top:5px">
                                                <?php echo $row['nmuser'] ?>
                                            </span><br>
                                            <span class="profile-tooltip-text"><?php echo trim($row['jabatan']) ?> </span><br>
                                            <span class="profile-tooltip-text"><?php echo $row['nmso'] ?></span>
                                        </span>
                                    </span>
                                </span>

                            </span>
                            <span class="description"><?php echo '<b>'. $row['nmhome'] .'</b> - '. $this->fc->idtgl( $row['tglcreate'], 'full') ?></span>
                        </div>
                        <div class="box-tools">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <h4 class="attachment-heading" style="margin-top:0px; ">
                        <!-- <i class="fa fa-comments-o"></i> -->
                        <a href="<?php echo site_url('forum/forum_post').'/'. $row['idparent'] .'/'. $row['idchild'] ?>"><?php echo $row['judul'] ?></h4></a>
                        <?php if ($row['gambar']) { ?>
                            <img width="180px" class="img-responsive pad" src="files/forum/<?php echo $row['gambar'] ?>" style="float:left;padding-left:0px; padding-top:0px">
                        <?php } ?>
                        <span class="comment">
                            <?php
                                echo $row['post'];
                                if ($row['attach']) {
                                    echo '<i class="small">File : </i>
                                    <a href="'.base_url("files/forum") .'/'. $row['attach'] .'"  target=_blank> &nbsp;<i class=" small fa fa-file-pdf-o"></i> <i class="small">'. $row['attach'] .'</a></i>';
                                }
                            ?>
                        </span>
                    </div>

                    <?php if ($row['jumlah']>4 and $this->session->userdata('idusergroup') != '999') { ?>
                    <div class="box-footer">
                        <div class="small text-center">
                            <a href="<?php echo site_url('forum/forum_post').'/'. $row['idparent'] .'/'. $row['idchild'] ?>">Tampilkan Seluruh Tanggapan </a>
                        </div>
                    </div>
                    <?php } ?>

                    <div class="box-footer box-comments">

                    <?php if ($row['thread']) {
                        foreach ($row['thread'] as $col) { ?>
                            <div class="box-comment">
                                <?php
                                    $foto_profile="files/profiles/_noprofile.png";
                                    if (file_exists("files/profiles/".$col['nip'].".gif")) {$foto_profile =  "files/profiles/".$col['nip'].".gif";}
                                ?>
                                <img class="img-circle img-bordered-dsw" src="<?php echo site_url($foto_profile); ?>">
                                <div class="comment-text">
                                    <span class="username">
                                        <span class="profile-tooltip">
                                            <span class="profile-tooltip-item"><?php echo $col['nmuser'] ?> </span>
                                                <span class="profile-tooltip-content"><img src="<?php echo site_url($foto_profile); ?>">
                                                <span class="profile-tooltip-text" style="font-size:18px; padding-top:5px">
                                                    <?php echo $col['nmuser'] ?>
                                                </span><br>
                                                <span class="profile-tooltip-text"><?php echo trim($col['jabatan']) ?> </span><br>
                                                <span class="profile-tooltip-text"><?php echo $col['nmso'] ?></span>
                                            </span>
                                        </span>

                                        <span class="text-muted pull-right"><?php echo $this->fc->idtgl( $col['tglpost'], 'full') ?></span>
                                    </span>
                                    <div class="comment more">
                                        <?php
                                            echo trim($col['post']);
                                        ?>
                                    </div>
                                </div>
                            </div>
                    <?php
                        }
                    } ?>
                    </div>

                    <?php if ($this->session->userdata('idusergroup') != '999') { ?>
                    <div class="box-footer">
                        <form action="<?php echo site_url('home/tanggapan/'. $row['idparent'] .'/'. $row['idchild']) ?>" method="post">
                            <img class="img-responsive img-circle img-sm img-bordered-dsw" src=
                            <?php
                                $foto_profile="files/profiles/_noprofile.png";
                                if (file_exists("files/profiles/".$this->session->userdata('nip').".gif")) {$foto_profile =  "files/profiles/".$this->session->userdata('nip').".gif";}
                                echo $foto_profile;
                            ?> alt=<?php $this->session->userdata('nmuser')?>>
                            <div class="img-push">
                                <input type="text" name="comment" class="form-control input-sm" value="" placeholder="Tanggapan ...">
                            </div>
                        </form>
                    </div>
                    <?php } ?>
                </div>
            <?php } ?>

        </div>

    </div>

    <div class="col-md-6">
        <div class="box box-widget">
            <div class="box-header with-border">
                <h3 class="box-title text"> Pengumuman</h3>
                <div class="small box-tools" style="padding-top:5px">
                    <a href="<?php echo site_url('home/pengumuman'); ?>"><i class="fa fa-link"></i></a>
                </div>
            </div>

            <?php
            $first = 1;
            foreach ($pengumuman as $row) {
                $arr = explode('<p>', $row['pengumuman']);
                $pengumuman = trim( $arr[1] );
                $lengkapnya = str_replace($pengumuman, '', $row['pengumuman']);

                if ($first==1) {
                    $first++; ?>

                    <!-- pengumuman terbaru -->
                    <div class="box-body" style="background-color:#F7F7F7" >
                        <div class="attachment-block clearfix" style="padding:0px; margin-bottom:0px">
                            <?php
                            if ($row['gambar']) {
                                echo '<img class="attachment-img" src="'. base_url("files/pengumuman") .'/'. $row['gambar'] .'">';
                            } else {
                                echo '<img class="attachment-img" src="'. base_url("files/images/depkeu_news.png") .'">';
                            }
                            ?>
                            <div class="attachment-pushed">
                                <span class="username">
                                    <b>Sekretariat</b>
                                    <span class="small text-muted pull-right"><?php echo $this->fc->idtgl($row['tglrekam'], 'hari') ?></span>
                                </span>
                                <h4 class="attachment-heading"><a href="#" onclick="showHide('<?php echo 'hidden_div'.$row['idpengumuman'] ?>'); return false;"><?php echo $row['judul'] ?></a>
                                </h4>

                                <div class="attachment-text">
                                    <?php
                                    echo $pengumuman ;
                                    echo '<div id="'. 'hidden_div'.$row['idpengumuman'] .'" style="display: none;">'. $lengkapnya .'</div>';
                                    if ($row['file']) {
                                        echo '<i class="small">File : </i>
                                        <a href="'.base_url("files/pengumuman") .'/'. $row['file'] .'"  target=_blank> &nbsp;<i class=" small fa fa-file-pdf-o"></i> <i class="small">'. $row['file'] .'</a></i>';
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- <div class="<?php if ($first==2) echo 'box-footer'; ?> box-comments"> -->
                    <div class="box-footer box-comments" style="background:white">
                <?php } else { ?>

                        <div class="box-comment">
                            <img class="img-circle img-sm img-bordered-dsw" src="<?php echo base_url("files/images/depkeu_round.png") ?>">
                            <div class="comment-text">
                                <span class="username">
                                    Sekretariat
                                    <span class="text-muted pull-right"><?php echo $this->fc->idtgl($row['tglrekam'], 'hari') ?></span>
                                </span>
                                <a href="#" onclick="showHide('<?php echo 'hidden_div'.$row['idpengumuman'] ?>'); return false;"><?php echo $row['judul'] ?></a><br>
                                <?php
                                if ($row['gambar']) {
                                    echo '<img style="width:95px !important; height:75px !important; margin: 5px 10px 5px 0px" src="'. base_url("files/pengumuman") .'/'. $row['gambar'] .'">';
                                }
                                echo $pengumuman ;
                                echo '<div id="'. 'hidden_div'.$row['idpengumuman'] .'" style="display: none;">'. $lengkapnya .'</div>';
                                if ($row['file']) {
                                    echo '<i class="small">File : </i>
                                    <a href="'.base_url("files/pengumuman") .'/'. $row['file'] .'" target=_blank><i class=" small fa fa-file-pdf-o"></i> <i class="small">'. $row['file'] .'</a></i>';
                                }
                                ?>
                            </div>
                        </div>

                <?php $first++;
                }
           } ?>
                    </div>

            <div class="box-footer">
                <div class="small text-center">
                    <a href="<?php echo site_url('home/pengumuman') ?>">Tampilkan Seluruh Pengumumam </a>
                </div>
            </div>
        </div>

        <div class="box box-widget">
            <div class="box-header">
                <h3 class="box-title"> Surat Terkini</h3>
                <div class="box-tools">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <!-- <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button> -->
                </div>
            </div>

            <?php
            foreach ($d_surat as $row) {
                $golsurat = 'ND-';
                if ($row['golsurat']=='1' and $row['jenis']=='10') $golsurat = 'RSM-';
                if ($row['golsurat']=='2') $golsurat = 'UND-';
            ?>
                <div class="box-footer box-comments" style="background:white; padding-bottom:0px">
                    <div class="box-comment">
                        <img class="img-circle img-sm img-bordered-dsw" src="<?php echo base_url("files/images/depkeu_round.png") ?>">
                        <div class="comment-text">
                            <span class="username">
                                No Agd : <?php echo $golsurat . $row['noagenda'] . $row['noagenda1'] ?>
                                <?php
                                if ( date('d-m-Y', strtotime($row['tgagenda']))== date('d-m-Y') ) {
                                    echo '<span class="text-muted pull-right">'. substr($row['tgagenda'],11,5) .' WIB</span>';
                                } else {
                                    echo '<span class="text-muted pull-right">'. $this->fc->idtgl( $row['tgagenda'], 'hr' ) .'</span>';
                                } ?>
                            </span>
                            <span class="small text-info"><?php echo $row['uraian'] ?></span><br>
                            <?php echo $row['perihal'] ?>
                        </div>
                    </div>
                </div>

            <?php } ?>

            <!-- <div class="box-footer">
                <div class="small text-center">
                    <a href="<?php echo site_url('persuratan') ?>">Tampilkan Monitoring Disposisi Surat </a>
                </div>
            </div> -->

        </div>
        <!--  -->
        <!--  -->
    </div>
    <!-- <div class="col-md-6">
        <div class="box box-widget">
            <div class="carousel slide">
                 <div class="carousel-inner">
                     <div class="item active">
                         <a href="#"><img src="../files/pengumuman/APBN_2016_pdf.jpg"></a>
                     </div>
                 </div>
            </div>
        </div>
    </div> -->


</div>




<script type="text/javascript">
    function showHide(obj) {
        var div = document.getElementById(obj);
            if (div.style.display == 'none') {
            div.style.display = '';
        } else {
            div.style.display = 'none';
        }
    }
</script>
