<style media="screen">
  th {
    text-align: center;
  }
  thead tr {
    color: #fff;
    background: #00acd6;
  }

  #iGrid .ui-selecting { background: #FECA40; }
  #iGrid .ui-selected { background: #F39814; color: white; }
</style>

<div class="row">
  <div class="col-md-12">
    <div class="box box-warning">
      <div class="box-header with-border">
        <div class="well well-sm"> 
          <form id="iForm" class="form-inline" role="form" action="" method="post">
            <div class="form-group pull-right">
              <div class="input-group">
                <input class="form-control" id="cari" type="text">
                <span class="input-group-btn">
                  <button class="btn btn-info btn-flat" type="button"><i class="fa fa-search"></i></button>
                </span>
                <span class="input-group-btn">
                  <button class="btn btn-info btn-flat" type="button"><i class="fa fa-times-circle"></i></button>
                </span>
              </div>
            </div>
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-btn">
                  <span class="btn btn-info">Detail</span>
                  <button type="button" id="addrow" class="btn btn-default" data-toggle="modal" data-target="#myModal">Rekam</button>
                  <button type="button" id="editrow" class="btn btn-default" data-toggle="modal" data-target="#myModal">Ubah</button>
                  <button type="button" id="delrow" class="btn btn-default" data-toggle="modal" data-target="#myModal">Hapus</button>
                </span>
              </div>
            </div>
          </form>
        </div>          
      </div><!-- /.box-header -->
      <div class="box-body">
        <table id="iGrid" class="table table-hover table-bordered">
          <thead>
            <tr>
              <th style="width: 3%">No.</th>
              <th style="width:10%">User ID</th>
              <th>Nama User</th>
              <th style="width:25%">Eselon</th>
              <th style="width:15%">Level</th>
              <th style="width:12%">Akses</th>
              <th style="width:12%">No. HP</th>
            </tr>
          </thead>
          <tbody>
          <tr>
            <?php 
            $arlevel  = array('level','Admin','User','Non User','Manager');
            $arwenang = array('Admin','Read Write','Read Only');
            $no = 0;
            foreach ($data as $row) { 
              $no++;
            ?>
              <tr>
              <td class="text-left"><?php echo $no ?></td>
              <td class="text-left"><?php echo $row->kduser ?></td>
              <td class="text-left"><?php echo $row->nmuser ?></td>
              <td class="text-left"><?php echo $row->nmso ?></td>
              <td class="text-center" id="<?php echo $row->kdlevel ?>"><?php echo $level[$row->kdlevel]['nmlevel'] ?></td>
              <td class="text-center" id="<?php echo $row->kdwenang ?>"><?php echo $arwenang[ $row->kdwenang ] ?></td>
              <td class="text-left"><?php echo $row->nohp ?></td>
              <tr>
            <?php } ?>
          </tbody>
        </table>
      </div><!-- /.box-body -->
      <div class="box-footer clearfix">
        <?php echo $this->pagination->create_links(); ?>
      </div>
    </div><!-- /.box -->
  </div><!-- /.col -->
</div><!-- /.row -->

<script type="text/javascript">
  $('.pagination').addClass('pagination-sm no-margin pull-right');
</script>

<?php $this->load->view('admin/user_rekam') ?>

