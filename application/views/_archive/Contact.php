<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Contact extends CI_Controller {

	var $param;	//Create a variable for the entire controller

	function __construct() {
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
	    $this->load->model('m_contact');
	}

	public function index() {
        if (! $this->session->userdata('isLoggedIn') ) redirect("login/show_login");
		$this->session->set_userdata( array('cari' => '') );
		$this->menu();
	}

	public function menu() {
		$row  = $this->m_contact->get_data();
		$config = $this->fc->pagination( site_url("contact/menu"), count($row), 12, '3');
		$this->pagination->initialize($config);

		$data['contact']  = array_slice($row, $this->uri->segment(3), 12);
		$data['menu']  = $this->dsw_model->get_menu( $this->uri->segment(1) );
		$data['bread']= array('header'=>'Pegawai DJA', 'subheader'=>'Pegawai');
		$data['view'] = "user/v_contact";
		$this->load->view('main/utama', $data);
		// print_r($data['menu']);
	}

	function cari() {
		if ($_POST['search'] == 'search') {
			$this->session->set_userdata('cari', $_POST['cari']);
		} else {
			$this->session->set_userdata('cari', '');
		}
		$this->menu();
	}

}
