<div class="row">
    <div class="col-md-12">
        <!-- <div class="box-body with-border hidden-md hidden-xs" style="font-size:20px; padding-top:0px">
            <?php
                $a=range("A","Z");
                foreach($a as $char)
                echo "<a href='#'>".$char."</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
            ?>
        </div> -->

        <div class="box box-widget">
            <!-- <div class="box-body with-border"> -->
            <form role="form" action="<?php echo site_url("contact/cari") ?>" method="post">
              <div class="input-group">
                <!-- <input type="hidden" name="nmfunction" value="dash"> -->
                <input type="text" name="cari" class="form-control" placeholder="Pencarian NIP dan Nama" value="<?php echo $this->session->userdata('cari') ?>">
                    <span class="input-group-btn">
                      <button type="submit" name="search" value="search" id="search-btn" class="btn btn-default btn-flat" style="border-top-width: 2px"><i class="fa fa-search"></i>
                      </button>
                    </span>
                    <span class="input-group-btn">
                    <button type="submit" name="search" value="clear" class="btn btn-default btn-flat" style="border-top-width: 2px"><i class="fa fa-times-circle"></i></button>
                    </span>
              </div>
            </form>
            <!-- </div> -->
        </div>
    </div>

    <?php
        foreach ($contact as $kontak) { ?>

            <div class="col-md-6">
                <div class="box box-widget">
                    <div class="box-body with-border">
                        <div class="user-block-dsw">
                            <?php
                                $foto_profile="files/profiles/_noprofile.png";
                                if (file_exists("files/profiles/".$kontak['nip'].".gif")) {$foto_profile =  "files/profiles/".$kontak['nip'].".gif";}
                            ?>
                            <img class="img-circle" style="margin-top:2px" src="<?php echo site_url($foto_profile); ?>">
                            <?php
                                $QRcode="files/QR/_noQR.png";
                                if (file_exists("files/QR/".$kontak['iduser'].".png")) {$QRcode =  "files/QR/".$kontak['iduser'].".png";}
                            ?>
                            <img class="hidden-xs img-qr pull-right" src="<?php echo site_url($QRcode); ?>">
                            <span class="username"><a href="#"><?php echo $kontak['nmuser'] ?></a></span>
                            <span class="description text-black">NIP <b><?php echo $kontak['nip'] ?></b> &nbsp;&nbsp;&nbsp;
                                <?php echo ucwords(strtolower($kontak['pangkat'])) ?> ( <?php echo $kontak['golongan'] ?> )</span>
                            <span class="description text-black"><?php echo $kontak['jabatan'] ?><br></span>
                            <span class="description text-black"><?php echo $kontak['nmso'] ?><br></span>
                            <span class="description text-black">
                                e Mail : <?php echo $kontak['email'] ?><br>
                                No HP : <?php echo $kontak['nohp'] ?>
                                <span class="pull-right">Intern : <?php echo $kontak['intern'] ?>&nbsp; - &nbsp;
                                    Ekstern : <?php echo $kontak['ekstern'] ?></span><br>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
    <?php } ?>


</div>
    <?php echo $this->pagination->create_links(); ?>
