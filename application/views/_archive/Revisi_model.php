<?php
class Revisi_model extends CI_Model {

	public function get_data( $depuni=null, $start, $end ) {
		$tgl = array('pus_tgl','t2_proses_tgl','t2_selesai_tgl','t3_proses_tgl','t3_selesai_tgl','t4_proses_tgl','t4_selesai_tgl','t5_proses_tgl','t5_selesai_tgl','t6_proses_tgl','t6_selesai_tgl','t7_proses_tgl','t7_selesai_tgl');
		$whr = "Where rev_tahun='".  date('Y') ."' And (";
		for ($i=0; $i<count($tgl); $i++) {
			$whr .= $tgl[$i] ." Between '$start' And '$end' ";
			if ($i<count($tgl)-1) $whr .= " Or "; else $whr .= ")";
		}
		$whr = "";
		// if ($depuni!=null) $whr .= " And  concat(kl_dept,'.',kl_unit)='$depuni' ";

		if (! (strrpos($this->session->userdata('idusergroup'), '001') or strrpos($this->session->userdata('idusergroup'), '002')) ) {
			$query = $this->db->query("Select kddept From revisi_wenang Where kdso='". $this->session->userdata('kdso') ."'");
			$hasil = $query->row_array();
			$whr = "Where kl_dept In ('". str_replace(",", "','", $hasil['kddept']) ."')";
		}

		$query = $this->db->query("Select rev_tahun, rev_id, rev_tahap, rev_status, kl_dept, '' nmdept, concat(kl_dept,'.',kl_unit) kl_unit, '' nmunit, kl_surat_no, kl_surat_tgl, kl_surat_hal,
			pus_status, pus_tgl,
			t2_status, if(t2_status='1',t2_proses_tgl,t2_selesai_tgl) t2_tgl,
			t3_status, if(t3_status='1',t3_proses_tgl,t3_selesai_tgl) t3_tgl,
			t4_status, if(t4_status='1',t4_proses_tgl,t4_selesai_tgl) t4_tgl,
			t5_status, if(t5_status='1',t5_proses_tgl,t5_selesai_tgl) t5_tgl,
			t6_status, if(t6_status='1',t6_proses_tgl,t6_selesai_tgl) t6_tgl,
			t7_status, if(t7_status='1',t7_proses_tgl,t7_selesai_tgl) t7_tgl
			From revisi $whr Order By rev_tahun Desc, rev_id Desc");
		$hasil = $this->fc->ToArr( $query->result_array(), 'rev_id');

		$dbref = $this->load->database('ref', TRUE);
		$query = $dbref->query("Select kddept kode, nmdept uraian, '1' level From t_dept Where kddept ". $this->fc->InVar($hasil, 'kl_dept') );
		$dept  = $this->fc->ToArr( $query->result_array(), 'kode');

		$query = $dbref->query("Select concat(kddept,'.',kdunit) kode, nmunit uraian, '2' level From t_unit Where concat(kddept,'.',kdunit) ". $this->fc->InVar($hasil, 'kl_unit') );
		$unit  = $this->fc->ToArr( $query->result_array(), 'kode');

		foreach ($hasil as $key=>$value) {
			if (array_key_exists($value['kl_dept'], $dept)) $hasil[$key]['nmdept'] = $dept[ $value['kl_dept'] ]['uraian'];
			if (array_key_exists($value['kl_unit'], $unit)) $hasil[$key]['nmunit'] = $unit[ $value['kl_unit'] ]['uraian'];
		}


		$data['revisi'] = $hasil;
		$data['depuni'] = $this->fc->array_index( array_merge_recursive($dept, $unit), 'kode');
		$data['kode']   = $depuni;
		$data['start'] 	= $this->fc->idtgl( $start );
		$data['end']   	= $this->fc->idtgl( $end );
		return $data;
	}

	public function get_rev_id( $dept, $unit ) {
		$thang ='2017';  
		$query = $this->db->query("Select Right(rev_id,3) noid From revisi Where rev_tahun='2017' And kl_dept='$dept' And kl_unit='$unit' Order By 1 Desc");
		$hasil = $query->row_array();

		$arr = array();
		if (!$hasil) $arr['rev_id'] = "$thang.$dept.$unit.001";
		else $arr['rev_id'] = "$thang.$dept.$unit.". sprintf('%03d', (int)$hasil['noid']+1);
		
		$query = $this->db->query("Select *, nmso From revisi_wenang a Left Join t_so b On a.kdso=b.kdso Where a.kddept like '%$dept%'");
		$hasil = $query->row_array();
		if ($hasil) { 
			$arr['rev_kdso'] = $hasil['kdso']; 
			$arr['rev_nmso'] = $hasil['nmso'] .' [ext: '. $hasil['intern'].'] - CP #1: '. $hasil['cp1'] .'['. $hasil['hp1'] .'] - CP #2: '. $hasil['cp2'] .'['. $hasil['hp2'] .']';
		} else { $arr['rev_kdso'] = ''; $arr['rev_nmso'] = ''; }
		return $arr;
	}

	public function get_row( $rev_id ) {
		$query = $this->db->query("Select rev_id, rev_tahun, kl_dept, kl_unit, kl_surat_no, kl_surat_tgl, kl_surat_hal, kl_pjb_jab, kl_pjb_nama, kl_pjb_nip, kl_email, kl_telp, doc_usulan_revisi, doc_matrix, doc_rka, doc_adk, doc_dukung_sepakat, doc_dukung_hal4, doc_dukung_lainnya, pus_catatan, kl_email From revisi Where rev_id='$rev_id'");
		$hasil = $query->row_array();

		if ($hasil) {
			$dbref = $this->load->database('ref', TRUE);
			$query = $dbref->query("Select nmdept From t_dept Where kddept='". $hasil['kl_dept'] ."'");
			$dept  = $query->row_array();
			$query = $dbref->query("Select nmunit From t_unit Where kddept='". $hasil['kl_dept'] ."' And kdunit='". $hasil['kl_unit'] ."'");
			$unit  = $query->row_array();

			$hasil['nmdept'] = $dept['nmdept'];
			$hasil['nmunit'] = $unit['nmunit'];
		}
		return $hasil;
	}

	public function save_form1() {
		$simpan			= $_POST['simpan'];
		$rev_tahun		= '2017';
		$rev_id			= $_POST['rev_id'];
		$rev_jns_upload	= $_POST['rev_jns_upload'];
		$rev_kdso		= $_POST['rev_kdso'];

		$kl_dept		= $_POST['kl_dept'];
		$kl_unit		= $_POST['kl_unit'];
		$kl_surat_no	= $_POST['kl_surat_no'];
		$kl_surat_tgl	= $this->fc->ustgl( $_POST['kl_surat_tgl'], 'hari');
		$kl_surat_hal	= $_POST['kl_surat_hal'];
		$kl_pjb_jab		= $_POST['kl_pjb_jab'];
		$kl_pjb_nama	= $_POST['kl_pjb_nama'];
		$kl_pjb_nip		= $_POST['kl_pjb_nip'];
		$kl_email		= $_POST['kl_email'];
		$kl_telp		= $_POST['kl_telp'];

		$doc_usulan_revisi	= $_POST['doc_usulan_revisi'];
		$doc_matrix			= $_POST['doc_matrix'];
		$doc_rka			= $_POST['doc_rka'];
		$doc_adk			= $_POST['doc_adk'];
		$doc_dukung_sepakat	= $_POST['doc_dukung_sepakat'];
		$doc_dukung_hal4	= $_POST['doc_dukung_hal4'];
		$doc_dukung_lainnya	= $_POST['doc_dukung_lainnya'];

		$pus_userid		= $this->session->userdata('iduser');
		$pus_catatan	= str_replace('"', "'", $_POST['pus_catatan']);
		$pus_ip			= 'xx.xx.xx';

		if ($rev_jns_upload=='1') {		// Data Awal Revisi
			if ($simpan=='terima') { $rev_tahap = '2'; $t2_status = '9'; $pus_status = '2'; } 	// Terima Dokumen
			if ($simpan=='kembali'){ $rev_tahap = '1'; $t2_status = '';  $pus_status = '0'; } 	// Pengembalian
			$this->db->query( "Insert Into revisi (rev_tahun, rev_id, rev_kdso, rev_tahap, kl_dept, kl_unit, kl_surat_no, kl_surat_tgl, kl_surat_hal, kl_pjb_jab, kl_pjb_nama, kl_pjb_nip, kl_email, kl_telp, doc_usulan_revisi, doc_matrix, doc_rka, doc_adk, doc_dukung_sepakat, doc_dukung_hal4, doc_dukung_lainnya, pus_userid, pus_catatan, pus_tgl, pus_status, pus_ip, t2_status) Values ('$rev_tahun', '$rev_id', '$rev_kdso', '$rev_tahap', '$kl_dept', '$kl_unit', '$kl_surat_no', '$kl_surat_tgl', '$kl_surat_hal', '$kl_pjb_jab', '$kl_pjb_nama', '$kl_pjb_nip', '$kl_email', '$kl_telp', '$doc_usulan_revisi', '$doc_matrix', '$doc_rka', '$doc_adk', '$doc_dukung_sepakat', '$doc_dukung_hal4', '$doc_dukung_lainnya', '$pus_userid', \"$pus_catatan\", current_timestamp(), '$pus_status', '$pus_ip', '$t2_status')" );
		}

		if ($rev_jns_upload=='2') {		// Data Dukung Tambahan
			$pus_status = '2'; 	// Sukses
			$this->db->query( "Insert Into revisi_perbaikan (rev_tahun, rev_id, kl_surat_no, kl_surat_tgl, kl_surat_hal, kl_pjb_jab, kl_pjb_nama, kl_pjb_nip, doc_usulan_revisi, doc_matrix, doc_rka, doc_adk, doc_dukung_sepakat, doc_dukung_hal4, doc_dukung_lainnya, pus_userid, pus_catatan, pus_status, pus_tgl, pus_ip) Values ('$rev_tahun', '$rev_id', '$kl_surat_no', '$kl_surat_tgl', '$kl_surat_hal', '$kl_pjb_jab', '$kl_pjb_nama', '$kl_pjb_nip', '$doc_usulan_revisi', '$doc_matrix', '$doc_rka', '$doc_adk', '$doc_dukung_sepakat', '$doc_dukung_hal4', '$doc_dukung_lainnya', '$pus_userid', \"$pus_catatan\", current_timestamp(), '$pus_status', '$pus_ip')" );

			$t6_status = '9';
			$this->db->query( "Update revisi Set rev_tahap='6', t6_status='$t6_status' Where rev_id='$rev_id'" );
		}
		return $rev_id;
	}

	public function get_tracking( $rev_id ) {
		$query = $this->db->query("Select rev_tahun, rev_id, rev_tahap, rev_status, kl_dept, '' nmdept, concat(kl_dept,'.',kl_unit) kl_unit, '' nmunit, kl_surat_no, kl_surat_tgl, kl_surat_hal, kl_pjb_jab,
			pus_status, pus_tgl,
			t2_status, t2_proses_tgl, t2_selesai_tgl,
			t3_status, t3_proses_tgl, t3_selesai_tgl,
			t4_status, t4_proses_tgl, t4_selesai_tgl,
			t5_status, t5_proses_tgl, t5_selesai_tgl,
			t6_status, t6_proses_tgl, t6_selesai_tgl,
			t7_status, t7_proses_tgl, t7_selesai_tgl
			From revisi Where rev_id='$rev_id'");
		$hasil = $query->row_array();

		$dbref = $this->load->database('ref', TRUE);
		$query = $dbref->query("Select kddept, nmdept From t_dept Where kddept='". $hasil['kl_dept'] ."'" );
		$dept  = $this->fc->ToArr( $query->result_array(), 'kddept');

		$query = $dbref->query("Select concat(kddept,'.',kdunit) kdunit, nmunit From t_unit Where concat(kddept,'.',kdunit)='". $hasil['kl_unit'] ."'");
		$unit  = $this->fc->ToArr( $query->result_array(), 'kdunit');

		if (array_key_exists($hasil['kl_dept'], $dept)) $hasil['nmdept'] = $dept[ $hasil['kl_dept'] ]['nmdept'];
		if (array_key_exists($hasil['kl_unit'], $unit)) $hasil['nmunit'] = $unit[ $hasil['kl_unit'] ]['nmunit'];
		return $hasil;
	}

	public function get_pdf($rev_id){
		$query = $this->db->query("Select *, '' nmdept, '' nmunit From revisi Where rev_id='$rev_id'");
		$hasil = $query->row_array();

		$dbref = $this->load->database('ref', TRUE);
		$query = $dbref->query("Select nmdept From t_dept Where kddept='". $hasil['kl_dept'] ."'");
		$dept  = $query->row_array();

		$query = $dbref->query("Select nmunit From t_unit Where kddept='". $hasil['kl_dept'] ."' And kdunit='". $hasil['kl_unit'] ."'");
		$unit  = $query->row_array();

		$hasil['nmdept'] = $dept['nmdept'];
		$hasil['nmunit'] = $unit['nmunit'];
		return $hasil;
	}

	public function email_revisi( $rev_id, $file ) {
		
		$email = array();
		// $email['idubsb@gmail.com'] = 'Setya Budi';
		// $email['krisman_gea@kemenkeu.go.id'] = 'Krisman Natalius Gea';
		$email['kisworodsp@gmail.com'] 			= 'Kisworo';
		$email['danie.satrio@kemenkeu.go.id'] 	= 'Danie Satrio';
		// $email['kartikawati@kemenkeu.go.id'] = 'Ika Kartikawati';
		// $email['masterkenari@gmail.com'] = 'Ade Kurnia';
		// $email['myrealone.passion@gmail.com'] = 'Masria';
		// $email['sutarsonosukardi@yahoo.co.id'] = 'Sutarsoono';

		$subject = "Revisi - Tanda Terima #$rev_id";
		$body	 = "Pengajuan revisi Anda sudah diterima Pusat Layanan DJA dengan Tiket No. $rev_id";
		$attach  = array( $file => "http://10.242.142.52/files/revisi/$rev_id/" .'/'. $file );
		$this->fc->send_mail( 'revisi', $email, $subject, $body, $attach);
	 	return $attach;
	}

}
