<font face="arial, helvetica, sans-serif">
<style>
.vertical_line{margin-top:3px;height:3px; width:700px;background:#000;}
.vertical_line2{margin-top:2px;height:1px; width:700px;background:#000;}
.wrapperss{
    margin-left: 18pt;
    background: white; margin: 0 auto;
    margin-right: 15pt;
}
.table{
  float: left;
  width:;
}
.tabelku{
      width: 100px;
      height: 100px;
}
</style>

<table border="0" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td width="50"><img src="<?=base_url('files/cuti/header.jpg'); ?>" width="96" height="100%" /></td>
<td width="527">
<table>
<tbody>
    <tr>
        <td style="font-size: 13pt;text-align:center"><strong>KEMENTERIAN KEUANGAN REPUBLIK INDONESIA</strong></td>
    </tr>
    <tr>
        <td style="font-size: 11pt;text-align:center"><strong>DIREKTORAT JENDERAL ANGGARAN</strong></td>
    </tr>
    <tr>
        <td style="font-size: 11pt;text-align:center"><strong><?php  echo $dir['nmso']; ?></strong></td>
    </tr>
    <tr style="margin-top:100px;">
      <td></td>
    </tr>
    <tr>
        <td style="font-size: 7pt;text-align:center"><?php  echo $almt1['header1']; ?></td>
    </tr>
    <tr>
        <td style="font-size: 7pt;text-align:center"><?php  echo $almt2['header2']; ?></td>
    </tr>
</tbody>
</table>

</td>
</tr>
</tbody>
</table>
</font>

<div class="vertical_line">
</div>
<div class="vertical_line2">
</div>
&nbsp;
<font face="arial, helvetica, sans-serif" style="font-size: 12pt;">
<table style="text-align:center;margin-left:180px;">
    <tr>
        <td><strong>SURAT TUGAS</strong></td>
    </tr>
    <tr>
      <td>(RAPAT DALAM KANTOR)</td>
    </tr>
    <tr>
        <td><?php echo $st['nost'] ?></td>
    </tr>
</table>
</font>


<div class="wrapperss">
  <p style="text-align: justify;font-size: 12pt;"><font face="arial,helvetica, sans-serif">&nbsp; &nbsp; &nbsp;Kami pejabat yang bertanda tangan dibawah ini, memberikan tugas kepada pejabat/pegawai untuk mengikuti rapat dalam kantor dalam rangka <?php echo $st['halst']; ?>, dengan agenda sebagai berikut :</font></p>
&nbsp;
  <font face="arial,helvetica, sans-serif">
  <table border="0" cellpadding="0" cellspacing="0" style="width:600px; margin-left:0px;font-size: 12pt;">
    <tbody>
      <tr>
        <td>Hari/tanggal</td>
        <td>:</td>
        <td><?php echo $this->fc->idtgl($st['tgawal'],'tglfull')?></td>
      </tr>
      <tr>
        <td>Waktu</td>
        <td>:</td>
        <td>17.00 s.d. selesai</td>
      </tr>
      <tr>
        <td>Tempat</td>
        <td>:</td>
        <td>Ruang Rapat Direktorat Jenderal Anggaran</td>
      </tr>
    </tbody>
  </table>
  </font>
</div>
    <p style="margin-left:0px;margin-top:15px;text-align: justify; font-size:12pt;"><font face="arial,helvetica, sans-serif">&nbsp; &nbsp; &nbsp; &nbsp;Untuk peserta rapat dalam kantor dari Ditjen Anggaran untuk melakukan absen handkey seperti biasa.
    </font>
    </p>

    <p style="margin-left:3.8px;text-align: justify; font-size:12pt;"><font face="arial,helvetica, sans-serif">&nbsp; &nbsp; &nbsp; &nbsp;Seluruh biaya pada kegiatan dimaksud dialokasikan pada DIPA Direktorat Jenderal Anggaran TA 2016 (kode 015.03.07.0653.002.053)
    </font>
    </p>

    <p style="margin-left:0px;text-align: justify; font-size:12pt;"><font face="arial,helvetica, sans-serif">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Demikian untuk dilaksanakan dengan penuh tanggung jawab.
    </font>
    </p>
</div>

<font face="arial,helvetica, sans-serif">
  <table style="margin-left: 310px; font-size: 12pt; height: 63px;margin-top:10px;" border="0">
    <tbody>
    <tr>
      <td>&nbsp;</td>
      <td>Ditetapkan di Jakarta</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>Pada tanggal 4 Agustus 2016</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>Direktur,</td>
    </tr>
    </tbody>
  </table>
</font>
<p>&nbsp;</p>
<p>&nbsp;</p>


<font face="arial,helvetica, sans-serif">
<table style="margin-left:317px;font-size:12pt;">
    <tbody>
        <tr>
            <td>Agung Widiadi</td>
        </tr>
        <tr>
            <td>NIP 196505111989101001</td>
        </tr>
    </tbody>
</table>
</font>
<p>&nbsp;</p>
<p>&nbsp;</p>
</div>

<font face="arial, helvetica, sans-serif">
<table style="text-align:justify;font-size:12pt;">
    <tr>
        <td>Tembusan&nbsp;:</td>
    </tr>
    <tr>
        <td>1. Kepala Bagian Kepegawaian Setditjen Anggaran</td>
    </tr>
    <tr>
        <td>2. Pegawai yang bersangkutan</td>
    </tr>
</table>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
</font>


<font face="arial, helvetica, sans-serif">
      <table style="text-align:justify;font-size:12pt;margin-left:285px;">
          <tr>
              <td>Lampiran</td>
          </tr>
          <tr>
              <td>Surat Tugas <?php  echo ucwords($dir['nmso']); ?></td>
          </tr>
          <tr>
              <td>Nomor: ST- /AG.7/2016</td>
          </tr>
          <tr>
              <td>Tanggal: Agustus 2016</td>
          </tr>
</table>
</font>

<font face="arial, helvetica, sans-serif" style="font-size: 12pt;">
<table style="text-align:center;margin-left:180px;margin-top:5px;">
    <tr>
        <td>Daftar Peserta Rapat Dalam kantor</td>
    </tr>
    <tr>
      <td><?php echo $st['halst']; ?></td>
    </tr>
    <tr>
        <td><?php echo $this->fc->idtgl($st['tgawal'],'tglfull')?></td>
    </tr>
</table>
</font>

<table border="1px" width="300px" style="border-collapse:collapse;border-spacing:0;margin-top:10px;">
      <thead>
        <tr>
          <th style="width:15px;height:40px;">No</th>
          <th style="width:200px;">Nama</th>
          <th style="width:180px;">NIP</th>
          <th style="width:130px;">PANGKAT</th>
          <th style="width:150px;">UNIT</th>
        </tr>
      </thead>

      <tbody>
         <?php 
         for ($i=0; $i <40 ; $i++) { 
          
         ?>
            <tr>
                  <td style="width:30px;height:30px;"></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
            </tr>
          <?php } ?>
      </tbody>
</table>