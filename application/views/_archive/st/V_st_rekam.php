<link href="<?php echo base_url(); ?>assets/plugins/uploadfile/fileinput.css" rel="stylesheet">
<script src="<?php echo base_url();?>assets/plugins/uploadfile/fileinput.min.js" type="text/javascript"></script>
<script src="<?=base_url('assets/plugins/datepicker/bootstrap-datepicker.js');?>" type="text/javascript"></script>
<script src="<?=base_url('assets/plugins/summernote/summernote.min.js'); ?>"></script>
<link rel="stylesheet" href="<?=base_url('assets/plugins/summernote/summernote.css'); ?>">

<style>
.datepicker{z-index:1151 !important;}
</style>

<script>
$(function(){
    $("#tanggal").datepicker({
  format:'yyyy-mm-dd'
    });

    $("#tanggal2").datepicker({
  format:'yyyy-mm-dd'
    });

    $("#tanggal3").datepicker({
  format:'yyyy-mm-dd'
    });

   /* $("#tanggal4").datepicker({
  format:'yyyy-mm-dd'
    });

    $("#tanggal5").datepicker({
  format:'yyyy-mm-dd'
    });

    $("#tanggal6").datepicker({
  format:'yyyy-mm-dd'
    });
    $("#tanggal7").datepicker({
  format:'yyyy-mm-dd'
    });
    $("#tanggal8").datepicker({
  format:'yyyy-mm-dd'
    });
    $("#tanggal9").datepicker({
  format:'yyyy-mm-dd'
    });*/

});

</script>

<section class="content">
    <div class='row'>
        <!-- <div class='col-md-12'> -->
            <div class='box box-widget'>
                <div class='box-header'>
                    <h3 class='box-title'><?php echo $ruh ?> Data Surat Tugas</h3>
                </div><!-- /.box-header -->

                <form action="<?php echo site_url('St/crud') ?>" method="post" role="form" enctype="multipart/form-data">
                <div class='box-body pad'>

                    <div class="form-group">
                        <label>Jenis ST :</label>
                        <div class="input-group">
                            <input name="ruh" type="hidden" value="<?php echo $ruh ?>" />
                            <input type="radio" name="jnsst" value="1" checked onchange="handleClick(this.value);"  required> &nbsp;&nbsp;RDK  
                        </div>
                       <!-- <div class="input-group">
                            <input name="ruh" type="hidden" value="<?php echo $ruh ?>" />
                            <input type="radio" name="jnsst" value="2" checked onchange="handleClick(this.value);"  required> &nbsp;&nbsp;Dinas Dalam Kota  
                        </div>
                        <div class="input-group">
                            <input name="ruh" type="hidden" value="<?php echo $ruh ?>" />
                            <input type="radio" name="jnsst" value="3" checked onchange="handleClick(this.value);"  required> &nbsp;&nbsp;Dinas Luar Kota  
                        </div>
                        <div class="input-group">
                            <input name="ruh" type="hidden" value="<?php echo $ruh ?>" />
                            <input type="radio" name="jnsst" value="4" checked onchange="handleClick(this.value);"  required> &nbsp;&nbsp;Diklat  
                        </div>
                         <div class="input-group">
                            <input type="hidden" id="jnsst" value="1" />
                            <input type="radio" name="jnsst" id="optRadio1" value="5" checked onchange="handleClick(this.value);" > &nbsp;&nbsp;Tugas Belajar  
                        </div> -->
                    </div>

                    <div class="form-group">
                        <label>Tanggal ST :</label>
                        <div class="input-group">
                            <div class="input-group-addon"><i class="fa fa-newspaper-o"></i></div>
                            <input name="ruh" type="hidden" value="<?php echo $ruh ?>" />
                            <input name="tglst" type="text" id="tanggal" placeholder="Tanggal Surat Tugas" class="form-control" value="<?php echo $table['tglst'] ?>"required/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label>Nomor ST :</label>
                        <div class="input-group">
                            <div class="input-group-addon"><i class="fa fa-newspaper-o"></i></div>
                            <input name="ruh" type="hidden" value="<?php echo $ruh ?>" />
                            <input name="idst" type="hidden" value="<?php echo $table['idst'] ?>" />
                            <input name="nost" type="text" id="" placeholder="Nomor Surat Tugas" class="form-control" value="<?php echo $table['nost'] ?>" required/>
                        </div>
                    </div>

                    
                    <div class="form-group">
                        <label>Hal :</label>
                        <div class="input-group">
                            <div class="input-group-addon"><i class="fa fa-newspaper-o"></i></div>
                            <input name="ruh" type="hidden" value="<?php echo $ruh ?>" />
                            <input name="hal" type="text" id="" placeholder="Tanggal Surat Tugas" class="form-control" value="<?php echo $table['halst'] ?>" required />
                        </div>
                    </div>

                    <div class="form-group">
                        <label>Tanggal Awal :</label>
                        <div class="input-group">
                            <div class="input-group-addon"><i class="fa fa-newspaper-o"></i></div>
                            <input name="ruh" type="hidden" value="<?php echo $ruh ?>" />
                            <input name="tgl_awal" type="text" id="tanggal2" placeholder="Tanggal ST Awal" class="form-control" value="<?php echo $table['tgawal'] ?>" required/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label>Tanggal Akhir :</label>
                        <div class="input-group">
                            <div class="input-group-addon"><i class="fa fa-newspaper-o"></i></div>
                            <input name="ruh" type="hidden" value="<?php echo $ruh ?>" />
                            <input name="tgl_akhir" type="text" id="tanggal3" placeholder="Tanggal ST Akhir" class="form-control" value="<?php echo $table['tgakhir'] ?>" required/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label>Pejabat Penanda Tangan :</label>
                        <div class="input-group">
                            <div class="input-group-addon"><i class="fa fa-newspaper-o"></i></div>
                            <input name="ruh" type="hidden" value="<?php echo $ruh ?>" />
                            <select required name="nippjb">    
                                <option> -- Pejabat --  </option>
                                <?php   
                                    foreach ($pjb as $r) {
                               echo "<option value='$r[nip]'>$r[fullname]</option>";
                            }
                         ?>
                            </select>
                        </div>
                    </div>

                        
                   <!--  <div class="form-group">
                        <label>Status :</label>
                        <div class="input-group">
                            <div class="input-group-addon"><i class="fa fa-newspaper-o"></i></div>
                            <input name="ruh" type="hidden" value="<?php echo $ruh ?>" />
                            <input name="status" type="radio" id="tanggal3" placeholder="Tanggal ST Akhir" class="form-control" value="<?php echo $table['tgakhir'] ?>" required/>
                        </div>
                    </div> -->


                </div>

                <div class="box-footer">
                    <div class=" pull-right">
                        <input name="simpan" type="submit" id="simpan" class="btn btn-primary" value="<?php echo $ruh ?>">
                    </div>
                </div>

                </form>
            </div>

        <!-- </div> -->
    </div><!-- ./row -->
</section><!-- /.content -->
