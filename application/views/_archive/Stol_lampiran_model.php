<?php 
class Stol_lampiran_model extends CI_Model {

	public function get_grid($idst, $tab, $es2) {
		// Query Data Lampiran
		$sql = "Select a.nip, nmuser, jabatan, pangkat, golongan, nmso 
				From d_st_peg a Left Join t_user b On a.nip=b.nip 
				Left Join t_so c On Concat(Left(b.kdso,2),'0000')=c.kdso
				Where idst=$idst Order By 1";
		$query = $this->db->query( $sql );
		$data['lamp']= $query->result_array();

		$query  = $this->db->query("Select Left(kdso,2) es2, nmso From t_so Where Left(kdso,2)!='00' And Right(kdso,4)='0000' Order By 1");
		$data['dja'] = $query->result_array();

		$query = $this->db->query("select tglst, nost, perihal from st where idst=$idst "); 
		$data['st'] = $query->row_array();

		// Query ESELON 3 - Sub Direktorat & Pegawai
		$query = $this->db->query("Select concat(kdso,'00','000000000000000000') kdkey, kdso, nmso nmuser, '' nip, '' jabatan, '' golongan, '' kdeselon, '1' level From t_so Where Left(kdso,2)='$es2' And Right(kdso,4)!='0000' And Right(kdso,2)='00'");
		$esln3 = $query->result_array();

		$sql = "Select concat(kdso,kdeselon,nip) kdkey, concat(Left(kdso,4),'00') kdso, 
					nmuser, nip, jabatan, pangkat, golongan, kdeselon, '2' level
				From t_user 
				Where Left(kdso,2)='$es2' And kdpeg='1' 
					And nip Not In (Select Nip From d_st_peg)
				Order By kdso,kdeselon,nmuser";
		$query = $this->db->query( $sql );
		$pegaw = $query->result_array();
		$data['peg'] = $this->fc->array_index( array_merge_recursive($esln3, $pegaw), 'kdkey' );

		$data['idst']= $idst;  
		$data['tab'] = $tab;  
		$data['es2'] = $es2;
		return $data;
	}

	public function sql_head($idparent, $idchild) {
		$query = $this->db->query("Select a.*, nmuser, nip From d_forum_room a Left Join t_user b On a.iduser=b.iduser Where idparent=$idparent and idchild=$idchild");
		return $query->row_array();
	}

	


}