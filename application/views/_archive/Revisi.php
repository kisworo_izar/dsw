<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Revisi extends CI_Controller {

	function __construct() {
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
        $this->load->model('dsw_model');
        $this->load->model('revisi_model');
	}

	public function index() {
        if (! $this->session->userdata('isLoggedIn') ) redirect("login/show_login");
		$this->dash();
	}

	public function dash( $depuni=null, $start=null, $end=null ) {
		if ($start==null) { $start = date('01-m-Y'); $end = date('t-m-Y'); }
		$data = $this->revisi_model->get_data( $depuni, $this->fc->ustgl($start), $this->fc->ustgl($end) );

		$data['menu']  = $this->dsw_model->get_menu( $this->uri->segment(1) );
		$data['bread'] = array('header'=>'Monitoring Revisi Anggaran', 'subheader'=>'Revisi Anggaran');
		$data['view']  = "revisi/v_dash";
		$this->load->view('main/utama', $data);
	}

	public function form1( $rev_id=null ) {
		if ( $rev_id==null ) $rev_id = -1;
		$data['revisi'] = $this->revisi_model->get_row( $rev_id );

		$data['menu']  = $this->dsw_model->get_menu( $this->uri->segment(1) );
		$data['bread'] = array('header'=>'&nbsp;', 'subheader'=>'Revisi Anggaran');
		$data['view']  = "revisi/v_form1";
		$this->load->view('main/utama', $data);
	}

	public function get_rev_id( $type ) {
		if ( $type=='awal' ) echo json_encode( $this->revisi_model->get_rev_id( $_POST['dept'], $_POST['unit'] ) );		
		if ( $type=='tambah' ) echo json_encode( $this->revisi_model->get_row( $_POST['rev_id'] ) );		
	}

	public function crud_form1() {		
		$rev_id = $this->revisi_model->save_form1();
		// $this->pdf( $rev_id );
		redirect("revisi/dash");
	}

	public function fileupload_form1() {
		$path = 'files/revisi/' . $_POST['rev_id'];
		$old  = umask(0);
		is_dir($path) || mkdir($path, 0777, true);
		umask($old);

		$file = $_FILES[ $_POST['name'] ];
		$resp = array('uploaded' => 'Gagal');
		if ($file) {
			move_uploaded_file($file["tmp_name"], $path .'/'. $file['name']);
			$resp = array('uploaded' => $file['name']);
		}
		echo json_encode($resp);
 	}

 	public function pdf( $rev_id ){
 		$data['revisi']= $this->revisi_model->get_pdf($rev_id);
		$data['view']  = "revisi/cetak_tanda_terima";
		$data['menu']  = $this->dsw_model->get_menu( $this->uri->segment(1) );
		$data['bread'] = array('header'=>'Admin Surat Tugas', 'subheader'=>'Admin Surat Tugas');

		$this->load->view('revisi/cetak_tanda_terima', $data);

		// Convert to PDF
 		$html = $this->output->get_output();
		$this->dompdf->load_html($html);
		$this->dompdf->set_paper('A4','portraite');
		$this->dompdf->render();

		$file = "tanda_terima_tiket_$rev_id.pdf";
		$this->dompdf->stream($file, array('Attachment'=>0));

		// file_put_contents("files/revisi/$rev_id/$file", $this->dompdf->output());
		// $this->revisi_model->email_revisi( $rev_id, $file );
 	}

	public function tracking( $type=null ) {
		$data['revisi'] = '';
		if ($type==null) $this->load->view('revisi/v_tracking', $data);
		else {
			$rev_id = $_POST['rev_id'];
			$data['revisi'] = $this->revisi_model->get_tracking( $rev_id );
			if ($data['revisi']) $this->load->view('revisi/v_tracking', $data);
			else show_404();
		}
	}

	public function test() {
		$this->db->query("ALTER TABLE revisi ADD COLUMN qrcode CHAR(32)");
		// $query = $this->db->query("Select doc_usulan_revisi, doc_matrix, doc_rka, doc_adk, doc_dukung_sepakat, doc_dukung_hal4, doc_dukung_lainnya From revisi Where rev_id='2017.018.04.001'");
		// $docs  = $query->row_array();
		// echo '<pre>'; print_r( $docs );
	}

}
