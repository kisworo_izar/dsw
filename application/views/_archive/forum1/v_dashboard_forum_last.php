<?php
    // if ($limit==null) 
    $limit=5;
    $query = $this->db->query("Select concat(idparent,'.',idchild) idkey, idparent, idchild, max(tglpost) tglpost, count(*) reply From d_forum Group By 1,2,3 Order By tglpost Desc Limit $limit");
    $d_last= $this->fc->ToArr( $query->result_array(), 'idkey');

    foreach ($d_last as $key=>$value) {
        // Forum Room
        $query = $this->db->query("Select nmroom From d_forum_room where idparent=". $value['idparent'] ." and idchild=". $value['idchild']);
        $room  = $query->row_array();
        $d_last[ $key ]['nmroom'] = $room['nmroom'];

        // Forum Home of Room
        $query = $this->db->query("Select thread From d_forum where idparent=". $value['idparent'] ." and idchild=". $value['idchild'] ." and idforum=1");
        $home  = $query->row_array();
        $d_last[ $key ]['thread'] = $home['thread'];
    }
?>

<div class="box box-widget" style="border-bottom: 1px solid #D2D6DE">
    <div style="background :#fff; border-radius:0px; border-left: 5px solid orange;padding: 5px">
        <b style="color:orange">Last Post</b>
    </div>

    <div class="box-body" style="padding:0px 10px 0px 10px">
        <ul class="products-list product-list-in-box">
            <?php foreach ($d_last as $row) { 
                $url = site_url('forum1/forum_post').'/'. $row['idparent'] .'/'. $row['idchild'];
                if ($this->session->userdata('idusergroup')=='999') $url = '#'; ?>
                <li class="item" style="padding:3px">
                    <div class="product-info" style="margin-left:0px">
                        <span class="small product-description">
                            <a href="<?php echo $url ?>"><?php echo $row['thread'] ?></a>
                            <span class="small text text-danger pull-right">Replies : <b><?php echo $row['reply']-1 ?></b></span>
                        </span>
                    </div>
                </li>
            <?php } ?>
        </ul>
    </div>
</div>

