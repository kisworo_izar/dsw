<script src="<?=base_url('assets/plugins/summernote/summernote.min.js'); ?>"></script>
<link rel="stylesheet" href="<?=base_url('assets/plugins/summernote/summernote.css'); ?>">

<script src="<?php echo base_url();?>assets/plugins/uploadfile/fileinput.min.js" type="text/javascript"></script>
<link href="<?php echo base_url(); ?>assets/plugins/uploadfile/fileinput.css" rel="stylesheet">


<form action="<?php echo site_url("forum1/save_reply/$idparent/$idchild") ?>" id="postForm" method="post" role="form" enctype="multipart/form-data" onsubmit="return postForm()">
    <div class="div-reply">
        <div class="form-group">
            <label>Tanggapan :</label>
            <input name="url" type="hidden" value="<?php echo $url ?>" />

            <textarea id="summernote" name="reply">
                <?php if ($quote) { ?>
                    <blockquote>
                        <?php
                            $foto_profile="files/profiles/_noprofile.png";
                            if (file_exists("files/profiles/".$quote['nip'].".gif")) { $foto_profile =  "files/profiles/".$quote['nip'].".gif"; } 
                        ?>
                        <div class="box-comment">
                            <span class="profile-tooltip"><img class="img-circle img-sm" src="<?php echo site_url($foto_profile); ?>" /></span>
                            <div class="comment-text">
                                <span class="username">
                                    <span class="profile-tooltip-item"><?php echo $quote['nmuser'] ?> </span>
                                </span>
                                <?php echo $this->fc->read_more( $quote['post'] ); ?>
                            </div>
                        </div>
                    </blockquote><br>
                <?php } ?>
            </textarea>
        </div>
		<div class="input-group-btn">
			<button name="simpan" type="submit" class="btn btn-xm btn-primary pull-right">Post</button>
		</div>
    </div>
</form>


<script type="text/javascript">
    $(document).ready(function() {
        var nil = 350;
        var str = window.location.pathname;
        if (str.indexOf("home")>0) nil = 50;
        $('#summernote').summernote({
            height: nil,
            placeholder: 'Isikan posting Anda disini ...',
            onImageUpload: function(files, editor, welEditable) {
                sendFile(files[0], editor, welEditable);
            }
        });

        function sendFile(file,editor,welEditable) {
            data = new FormData();
            data.append("file", file);
            $.ajax({
                url: "<?php echo site_url('forum1/upload_image') ?>",
                data: data,
                cache: false,
                contentType: false,
                processData: false,
                success: function(data){
                    alert(data);
                    $('.summernote').summernote("insertImage", data, 'filename');
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    console.log(textStatus+" "+errorThrown);
                }
            });
        }
    });

    var postForm = function() {
        var content = $('textarea[name="content"]').html($('#summernote').code());
    }
</script>


