<script src="<?=base_url('assets/plugins/summernote/summernote.min.js'); ?>"></script>
<link rel="stylesheet" href="<?=base_url('assets/plugins/summernote/summernote.css'); ?>">

<link href="<?php echo base_url(); ?>assets/plugins/uploadfile/fileinput.css" rel="stylesheet">
<script src="<?php echo base_url();?>assets/plugins/uploadfile/fileinput.min.js" type="text/javascript"></script>


<div class="row">
    <div class="col-md-9">
        <div class="box box-widget" style="border-top: 1px solid #D2D6DE; margin-bottom:10px">
            <form action="<?php echo site_url('forum1/save_thread') ?>" id="postForm" method="post" role="form" enctype="multipart/form-data" onsubmit="return postForm()">
                <div class="box-header" style="background :#F7F7F7; border-bottom:1px solid #D2D6DE; padding:0px">

                    <?php if ($ruh!='Catatan') { ?>
                        <div class="form-group" style="margin:10px">
                            <label>Forum dan Sub Forum :</label>
                            <div class="input-group" style="height:35px">
                                <div class="input-group-addon"><i class="fa fa-bars"></i></div>
                                <select style="width:100%;height:35px" name="group" id="pilih" <?php if ($ruh=='Ubah') echo 'disabled' ?> onchange="ajax_pic(this.value)" >
                                <option value="" hidden>** Pilih Forum dan Sub Forum **</option>
                                    <?php
                                    foreach ($group as $row) {
                                        if ( $row['idparent']== 1) {
                                            if ( $row['idchild'] > 2) echo '</optgroup>'; // Clossing Group
                                            echo '<optgroup label="'. $row['nmroom'] .'">';
                                        } else {
                                            echo '<option value="'. $row['kdkey'] .'" '. $row['selected'] .'>&nbsp;&nbsp;&nbsp;'.  $row['nmroom'] .'</option>';
                                        }
                                    } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group" style="margin:10px">
                            <label>P I C :</label>
                            <div class="input-group" style="height:35px">
                                <div class="input-group-addon"><i class="fa fa-user"></i></div>
                                <select style="width:100%;height:35px" name="pic" id="pic">
                                    <?php 
                                    if ( count($pic)>1 ) echo '<option hidden>** Pilih PIC untuk Thread **</option>';
                                    foreach ($pic as $row) {
                                        if ( substr($row['kdkey'],-1)=='1') {
                                            if ( substr($row['kdkey'],-1)=='2') echo '</optgroup>'; // Clossing Group
                                            echo '<optgroup label="'. $row['fullname'] .'">';
                                        } else {
                                            $selected = ''; if ($row['iduser']==$d_head['pic']) $selected = ' selected';
                                            echo '<option value="'. $row['iduser'] .'"'. $selected .'>&nbsp;&nbsp;&nbsp;'.  $row['fullname'] .'</option>';
                                        }
                                    } ?>
                                </select>
                            </div>
                        </div>
                    <?php } ?>

                    <div class="form-group" style="margin:10px">
                        <label>Judul Thread :</label>
                        <div class="input-group">
                            <div class="input-group-addon"><i class="fa fa-newspaper-o"></i></div>
                            <input type="text" name="thread" class="form-control" value="<?php if ($ruh=='Ubah' or $ruh=='Catatan') echo $d_head['thread'] ?>" placeholder="Judul thread Anda, maksimal 200 Karakter">
                        </div>
                    </div>
                    <div class="form-group" style="margin:10px">
                    <label><?php if ($ruh=='Catatan dari PIC') { echo 'Catatan :'; } else { echo 'Tulisan :'; } ?></label>
                        <textarea id="summernote" name="content">
                            <?php 
                                if ($ruh=='Ubah') echo $d_head['post']; 
                                if ($ruh=='Catatan') echo $d_head['catatan']; 
                            ?>
                        </textarea>
                    </div>

                    <?php if ($ruh!='Catatan') { ?>
                        <div class="form-group" style="margin:10px">
                            <label>Files :</label> &nbsp; <span class="text-muted"><?php if ($ruh=='Ubah') echo $d_head['attach'] ?></span>
                            <div class="input-group">
                                <div class="input-group-addon"><i class="fa fa-external-link"></i></div>
                                <input name="file" type="file" class="file" />
                            </div>
                        </div>
                    <?php } ?>

                </div>

                <div class="box-footer" style="background :#F7F7F7; border-bottom:1px solid #D2D6DE; border-radius:0px;">
        			<span class="input-group-btn">
                        <input name="ruh" type="hidden" value="<?php echo $ruh ?>" />
                        <input name="idparent" type="hidden" value="<?php if ($ruh=='Ubah' or $ruh=='Catatan') echo $d_head['idparent'] ?>" />
                        <input name="idchild" type="hidden" value="<?php if ($ruh=='Ubah' or $ruh=='Catatan') echo $d_head['idchild'] ?>" />
		  				<button name="simpan" type="submit" class="btn btn-xm btn-primary pull-right"><?php echo $ruh ?></button>
        			</span>
                </div>
            </form>
        </div>
    </div>

    <div class="col-md-3 hidden-xs"><?php $this->load->view('forum1/v_forum_top_thread'); ?></div>
    <div class="col-md-3 hidden-xs"><?php $this->load->view('forum1/v_forum_last_thread'); ?></div>
</div>


<script type="text/javascript">
    $(document).ready(function() {
        $('#summernote').summernote({
            height: 350,
            placeholder: 'Isikan posting Anda disini ...',
            onImageUpload: function(files, editor, welEditable) {
                sendFile(files[0], editor, welEditable);
            }
        });

        function sendFile(file,editor,welEditable) {
            data = new FormData();
            data.append("file", file);
            $.ajax({
                url: "<?php echo site_url('forum1/upload_image') ?>",
                data: data,
                cache: false,
                contentType: false,
                processData: false,
                success: function(data){
                    alert(data);
                    $('.summernote').summernote("insertImage", data, 'filename');
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    console.log(textStatus+" "+errorThrown);
                }
            });
        }
    });

    var postForm = function() {
        var content = $('textarea[name="content"]').html($('#summernote').code());
    }
</script>

<script type="text/javascript">
  function ajax_pic( idkey ) {
    $.ajax({
      url : "<?php echo site_url('forum1/ajax_pic') ?>",
      type: "POST",
      data: { 'idkey': idkey },
    })
      .done( function (msg) {
        document.getElementById("pic").innerHTML = msg;
      })
  }
</script>

<script type="text/javascript">
    $("#files").fileinput({
        uploadUrl: "<?php echo site_url('upload/fileupload') ?>",
        allowedFileExtensions : ['15','16','jpg','xlsx','doc','docx','rtf','pdf'],
        overwriteInitial: true,
        maxFileSize: 1000,
        maxFilesNum: 10,
        minFileCount: 1,
        maxFileCount: 5,
        dropZoneTitle: 'Drag & Drop file ADK RKA-KL dan TOR/RAB disini ...',
        msgInvalidFileExtension: "Invalid extension file {name}. Hanya {extensions} files yang bisa diproses ...",
        slugCallback: function(filename) {
            return filename.replace('(', '_').replace(']', '_');
        }
    });
</script>
