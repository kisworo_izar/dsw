<?php
class Stol extends CI_Controller {
	function __construct() {
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
	    $this->load->model('dsw_model');
	    $this->load->model('stol_model');
	}

	public function index() {
    	if (! $this->session->userdata('isLoggedIn') ) redirect("login/show_login");
		$this->grid(7, 'data', '01');
	}

	public function grid() {
		$row = $this->stol_model->get_data();
		$config = $this->fc->pagination( site_url("stol/grid"), count($row), 10, '3');
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
	    $config['cur_tag_close'] = '</a></li>';
		$this->pagination->initialize($config);

		$data['table'] = array_slice($row, $this->uri->segment(3), 10);
		$data['menu']  = $this->dsw_model->get_menu( $this->uri->segment(1) );
		$data['bread'] = array('header'=>'Admin Surat Tugas', 'subheader'=>'Admin Surat Tugas');
		$data['view']  = "stol/v_stol_grid";
		$this->load->view('main/utama', $data);
	}


	function rekam() {
		$ruh  = 'Rekam'; if ($this->uri->segment(3)=='u') $ruh = 'Ubah'; if ($this->uri->segment(3)=='h') $ruh = 'Hapus';
		$data = $this->stol_model->get_row( $ruh );

		$data['menu']  = $this->dsw_model->get_menu( $this->uri->segment(1) );
		$data['bread'] = array('header'=>"$ruh Data Surat Tugas", 'subheader'=>'Admin Surat Tugas');
		$data['view']  = "stol/v_stol_rekam";
		$this->load->view('main/utama', $data);
	}

	function crud() {
		$this->stol_model->save();
		redirect("stol/grid");
	}

	function cari() {
		if ($_POST['search'] == 'search') {
			$this->session->set_userdata('cari', $_POST['cari']);
		} else {
			$this->session->set_userdata('cari', '');
		}

		if ($_POST['nmfunction']=='dash') {
			$this->dash();
		} else {
			$this->grid();
		}
	}

	public function dash() {
		$row = $this->stol_model->get_data();
		$config = $this->fc->pagination( site_url("slideshow/dash"), count($row), 10, '3');
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
	    $config['cur_tag_close'] = '</a></li>';
		$this->pagination->initialize($config);

		$data['slideshow'] = array_slice($row, $this->uri->segment(3), 10);

		$data['menu']  = $this->dsw_model->get_menu( $this->uri->segment(1) );
		$data['bread'] = array('header'=>'Arsip slideshow', 'subheader'=>'Slideshow');
		$data['view'] = "slideshow/V_stol_list";
		$this->load->view('main/utama', $data);
		// $this->fc->browse( $data['nilai'] );

	}

	function pdf( $idst ) {
		$data['stol']  = $this->stol_model->get_st( $idst );
		$es=substr($this->session->userdata('kdso'),0,2) ; //echo $a; 
		$data['dir']  = $this->stol_model->get_dir($es);
		$data['almt1']  = $this->stol_model->get_alm1($es);
		$data['almt2']  = $this->stol_model->get_alm2($es);
		$data['paragraf1']=$this->stol_model->get_par1($idst);
		$data['paragraf2']=$this->stol_model->get_par2($idst);
		// $data['pejabat'] = $this->stol_model->get_pejabat($idst);	
		$data['menu']  = $this->dsw_model->get_menu( $this->uri->segment(1) );
		$data['bread'] = array('header'=>'Admin Surat Tugas', 'subheader'=>'Admin Surat Tugas');
		$data['view']  = "stol/cetakst";
		$this->load->view('stol/cetakst', $data);
		
		$html = $this->output->get_output();

		// Convert to PDF
		$this->dompdf->load_html($html);
		$this->dompdf->set_paper('A4','portraite');
		$this->dompdf->render();
		$this->dompdf->stream( 'StRDK'.'.pdf',array('Attachment'=>0) );
	}

	function test() {
		$query = $this->db->query("Select nip, fullname, jabatan From t_user Where jabatan Like 'direktur%' Or jabatan Like 'sekretaris direktorat%' ");
		$data['ttdnip'] = $query->result_array();
		// echo '<pre>'; print_r($data['ttdnip']);
		$js_array = json_encode($data['ttdnip']);
		echo "var javascript_array = ". $js_array . ";\n";
	}

}
