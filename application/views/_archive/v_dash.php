<link href="<?=base_url('assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css');?>" rel="stylesheet" type="text/css" />
<script src="<?=base_url('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js');?>" type="text/javascript"></script>

<style media="screen">
    th{text-align: center;background: #F3F6FB;color:#34495e}
    td a {display:block;width:100%;}
    A:link,A:visited,A:active,A:hover {text-decoration: none; color: white;}
    .center{text-align: center}
    .hijau{background: #27ae60; color:white}
    .biru{background: #3498db; color:white}
    .abu{background: #bdc3c7; color:white}
    .merah{background: #e74c3c; color:white}
    .table-bordered > thead > tr > th,
    .table-bordered > thead > tr > td {border-bottom: 2px solid #E7E9EE;}
    .table-bordered > tbody > tr > td {padding:3px}
    /*.hiddenRow {padding: 0 !important; border: solid 1px red !important}*/
    .hiddenRow {padding: 0 !important;}
    .info{border-top:solid 1px #E7E9EE;padding:0px;margin:0px}
    .info1{border-top:0px;padding:0px;margin:0px}
    .x { display: inline-block; width: 95px; }

    section {position: relative;}
    section.positioned {position: absolute;top:10px;left:10px;}
    .container {overflow-y: auto;padding: 0px;width: 100%}
    /*.container {overflow-y: auto;height: 200px;padding: 0px;width: 100%}*/
    table {border-spacing: 1;width:100%;}
    th {height: 0;line-height: 0;padding-top: 0;padding-bottom: 0;color: #F3F6FB;border: none;white-space: nowrap;border:solid 1px red}
    th div{position: absolute; background: #F3F6FB;color: #34495e;border-bottom: solid #E7E9EE 1px;border-top: solid 1px #E7E9EE;
        padding: 6px 25px 6px 13px;top: 0;line-height: normal;}
    th:first-child div{border: none;}
    .calculated-width {width: -moz-calc(100% - 650px);width: -webkit-calc(100% - 650px);width: calc(100% - 650px);}​
</style>

<div class="row" ng-app="app" >
    <div class="col-md-12">
        <div class="box box-widget">
            <div class="box-header with-border">
                <div class="span5 col-md-4 pull-left" id="sandbox-container" style="border:0px; padding:0px; height:25px">
                <!-- <div class="form-group">
                    <div class="input-group">
                        <span class="input-group-btn">
                            <button type="button" id="addrow"  class="btn btn-xs btn-flat btn-default" data-toggle="modal" data-target="#myModal" style="height:25px">...</button>
                        </span>
                    </div>
                </div> -->
                </div>
                <div class="span5 col-md-4 pull-right" id="sandbox-container" style="border:0px; padding:0px; height:25px">
                    <div class="input-daterange input-group" id="datepicker">
                           <input type="text" class="input-sm form-control" value="<?php echo $start ?>" name="start" id="start" style="padding-top:0px; padding-bottom:0px;  height:25px">
                           <span class="input-group-addon" style="border:0px;"> s.d. </span>
                           <input type="text" class="input-sm form-control" value="<?php echo $end ?>" name="end" id="end" style="padding-top:0px; padding-bottom:0px; height:25px">
                    </div>
                </div>
            </div>
            <div class="box-body">

                <section class="">
                <div class="container" style="height:450px">
				<table id="iGrid" class="table table-hover table-bordered">
					<thead>
					<tr class="header">
						<th style="width:50px;padding:4px 0px">Tiket
                            <div style="width:50px;border-bottom: solid #E7E9EE 1px;border-top: solid 1px #E7E9EE">Tiket</div>
                        </th>
						<th style="padding:4px 0px">
                            <div>&nbsp</div>
                        </th>
						<th style="padding:4px 0px">
                            <div class="calculated-width" style="padding-left:0px">Kementerian/Lembaga - Unit</div>
                        </th>
                        <th style="width:73px;padding:4px 0px"><div>T1</div>
                        </th>
                        <th style="width:73px;padding:4px 0px"><div>T2</div>
                        </th>
                        <th style="width:73px;padding:4px 0px"><div>T3</div>
                        </th>
					</tr>
					</thead>
				    <tbody>
						<tr style="margin:0px;border:solid 1px red">
							<td class="text-bold text-primary" style="text-align:right;padding-right:7px" data-toggle="collapse" data-target=".212">212</td>
							<td class="text-bold" style="width:7px;border:solid 0px;border-bottom:solid 1px #F4F4F4"  data-toggle="collapse" data-target=".212">015</td>
							<td style="padding-left:0px;border:solid 0px;border-bottom:solid 1px #F4F4F4" data-toggle="collapse" data-target=".212">
                                <div class="text-bold">Kementerian Keuangan</div>
                                <div>015.03 Direktorat Jenderal Anggaran</div>
                            </td>
							<td class="small text-center hijau">
                                <a href="#" data-toggle="tooltip" title="Nomor Tiket 212 - Proses Tahap 1">
                                <span>07-09-2016</span><br>
                                <span><i class="fa fa-clock-o"></i> 08:30</span></a>
                            </td>
							<td class="small text-center hijau">
                                <a href="#" data-toggle="tooltip" title="Nomor Tiket 212 - Proses Tahap 2">
                                <span>07-09-2016</span><br>
                                <span><i class="fa fa-clock-o"></i> 08:30</span></a>
                            </td>
							<td class="small text-center biru">
                                <a href="#" data-toggle="tooltip" title="">
                                    <span><i class="fa fa-clock-o"></i></span>
                                </a>
                            </td>
						</tr>


                        <tr style="background:#FFF;color:#34495E">
                            <td class="hiddenRow" colspan="2">
                                <div class="small collapse info 212"></div>
                            </td>
                            <td class="hiddenRow" colspan="5">
                                <div class="small collapse info 212"  style="padding-left:0px">
                                    <span class="x">Nomor Tiket</span>: <b>212-2016</b><br>
                                    <span class="x">Nomor Surat</span>: SRT-087.Rev.01/2016<br>
                                    <span class="x">Tanggal Surat</span>: Senin, 05 September 2016<br>
                                    <span class="x">Perihal</span>: Revisi Anggaran self blocking<br>
                                </div>
                            </td>
                        </tr>

						<tr>
                            <td class="text-bold text-primary" style="text-align:right;padding-right:7px" data-toggle="collapse" data-target=".200">200</td>
							<td class="text-bold" style="width:7px;border:solid 0px;border-bottom:solid 1px #F4F4F4"  data-toggle="collapse" data-target=".200">015</td>
							<td style="padding-left:0px;border:solid 0px;border-bottom:solid 1px #F4F4F4" data-toggle="collapse" data-target=".200">
                                <div class="text-bold">
                                    Kementerian Keuangan
                                </div>
                                <div>
                                    015.03 Direktorat Jenderal Anggaran
                                </div>
                            </td>

							<td class="small text-center hijau">
                                <a href="#" data-toggle="tooltip" title="Nomor Tiket 200 - Proses Tahap 1">
                                </a>
                            </td>
							<td class="small text-center merah">
                                <span>07-09-2016</span><br>
                                <span><i class="fa fa-clock-o"></i> 08:30</span>
                            </td>
							<td class="small text-center abu">
                            </td>
						</tr>


                        <tr style="background:#FFF;color:#34495E">
                            <td class="hiddenRow" colspan="2">
                                <div class="small collapse info 200"></div>
                            </td>
                            <td class="hiddenRow" colspan="4">
                                <div class="small collapse info 200"  style="padding-left:0px">
                                    <span class="x">Nomor Tiket</span>: <b>200-2016</b><br>
                                    <span class="x">Nomor Surat</span>: SRT-08.Rev.01.KEU/2016<br>
                                    <span class="x">Tanggal Surat</span>: Minggu, 04 September 2016<br>
                                    <span class="x">Perihal</span>: Revisi Anggaran Penghematan<br>
                                </div>
                            </td>
                        </tr>



					</tbody>
				</table>
                </div>
                </section>

			</div> <!-- box body -->
        </div>
    </div>
</div>

<script type="text/javascript">
    $('.collapse').on('show.bs.collapse', function () {
    $('.collapse.in').collapse('hide');
    });
</script>
<script type="text/javascript">
   $('#sandbox-container .input-daterange').datepicker({
      format: "dd-mm-yyyy",
      language: "id",
      orientation: "bottom auto",
      autoclose: true,
      todayHighlight: true
   }).on('changeDate', function() {
         var srt = document.getElementById("start").value;
         var end = document.getElementById("end").value;
        //  window.location.href = "<?php echo site_url('absensi/cari?start=') ?>"+ srt +"&end="+ end;
      });

</script>
