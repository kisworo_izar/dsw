<?php
class Stol_model extends CI_Model {

	public function get_data() {
		$whr  = "";
		$cari = $this->session->userdata('cari');

		if ($cari) {
			$whr = " where " ;
			$arr = explode(' ', $cari);
			for ($i=0; $i<count($arr); $i++) {
				$str  = $arr[$i];
				$whr .= " keterangan like '%$str%' or gambar like '%$str%' ";
				if ($i<count($arr)-1) {
					$whr .= " or ";
				}
			}
		}

		// cari query data dari d_paket
		$query = $this->db->query("SELECT *  from st $whr order by idst desc ");
		$hasil = $this->fc->ToArr( $query->result_array(), 'idst');
		return $hasil;
	}

	public function get_row( $ruh ) {
		$idst =' 99999'; if (!is_null($this->uri->segment(4))) $idst = $this->uri->segment(4);
		$query = $this->db->query("Select * From st Where idst=$idst");
		$data['table'] = $query->row_array();

		$query = $this->db->query("Select * From st_jenis Order By 1");
		$data['jnsst'] = $this->fc->ToArr( $query->result_array(), 'jnsst');

		$query = $this->db->query("Select nip, fullname, jabatan From t_user Where jabatan Like 'direktur%' Or jabatan Like 'sekretaris direktorat%' ");
		$data['ttdnip'] = $this->fc->ToArr( $query->result_array(), 'nip');

		$query = $this->db->query("Select nmrapat, letak From t_rapat_ruang Order By 1");
		$data['ruangrapat'] = $this->fc->ToArr( $query->result_array(), 'nmrapat');

		$data['ruh'] = $ruh;
		return $data;
	}

	public function save(  ) {
		$action		= $_POST['ruh'];
		$idst		= $_POST['idst']; 
		$tglst		= $this->fc->ustgl( $_POST['tglst'], 'hari'); 
		$nost		= $_POST['nost']; 
		$jnsst		= $_POST['jnsst']; 
		$perihal	= $_POST['perihal']; 
		$paragraf1	= $_POST['paragraf1']; 
		$tglawal	= $this->fc->ustgl( $_POST['tglawal'], 'hari'); 
		$waktu		= $_POST['waktu']; 
		$tempat		= $_POST['tempat']; 
		$paragraf2	= $_POST['paragraf2']; 
		$ttdlokasi	= $_POST['ttdlokasi']; 
		$ttdjabatan	= $_POST['ttdjabatan']; 
		$ttdnama	= $_POST['ttdnama']; 
		$ttdnip		= $_POST['ttdnip']; 
		$tembusan	= $_POST['tembusan']; 

		if ($action=='Rekam') {
			$this->db->query( "Insert Into st (tglst,nost,perihal,jnsst) Values ('$tglst','$nost','$perihal',$jnsst)" );
		}
		if ($action=='Ubah') {
			$this->db->query( "Update st Set tglst='$tglst', nost='$nost', jnsst='$jnsst', perihal=\"$perihal\", paragraf1=\"$paragraf1\", tglawal='$tglawal', waktu='$waktu', tempat=\"$tempat\", paragraf2=\"$paragraf2\", ttdlokasi='$ttdlokasi', ttdjabatan='$ttdjabatan', ttdnama='$ttdnama', ttdnip='$ttdnip', tembusan='$tembusan' Where idst=$idst" );
		}
		if ($action=='Hapus') {
			$this->db->query( "delete from d_st where idst=$idst" );
		}
		return;
	}

	public function get_st($idst){
	 	$query=$this->db->query("select * from st where idst=$idst");
	 	return $query->row_array();
	}

	public function get_dir($es){
	 	$query=$this->db->query("SELECT nmso  FROM l_so WHERE SUBSTRING(kdso,1,2)='$es' AND nmso1!='' ");
	 	return $query->row_array();
	}

	public function get_alm1($es){
	 	$query=$this->db->query("SELECT header1  FROM l_so WHERE SUBSTRING(kdso,1,2)='$es' AND nmso1!='' ");
	 	return $query->row_array();
	 	
	}

	public function get_alm2($es){
	 	$query=$this->db->query("SELECT header2  FROM l_so WHERE SUBSTRING(kdso,1,2)='$es' AND nmso1!='' ");
	 	return $query->row_array();
	}

	public function get_par1($idst){
		$query=$this->db->query("SELECT paragraf1 FROM st WHERE idst=$idst");
		return $query->row_array();
	}
	public function get_par2($idst){
		$query=$this->db->query("SELECT paragraf2 FROM st WHERE idst=$idst");
		return $query->row_array();
	}

	// public function get_pejabat($idst){
	// 	$query=$this->db->query("select idst,eselon2,jabatan FROM st a LEFT JOIN t_user b ON CONCAT(LEFT(b.kdso,2),'0000')=b.kdso where idst=$idst");
	// 	return $query->row_array();
	// }
}
