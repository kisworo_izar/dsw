<script src="<?=base_url('assets/plugins/jQuery/jQuery.Shorten.1.0.js'); ?>" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $(".more").shorten({
            "showChars" : 150,
            "moreText"  : "<i class='small'> Read More</i>",
            "lessText"  : " <i class='small'> Less</i>"
        });
    });
</script>

<div class="row">
    <div class="col-md-6">
        <div class="dsw-info-box" style="padding-right:45px">
        <span class="dsw-info-box-icon bg-orange"><i class="fa fa-rss"></i></span>
            <div id="text-carousel" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner" style="position:absolute; left:45px">

                    <?php
                    $i=1;
                    foreach ($d_rss as $row) {
                    ?>
                        <div class="item <?php if ($i==1) echo 'active'?>">
                            <div class="carousel-content dsw-info-box-content" style="margin-left:0px">
                                <span class="dsw-info-box-text">
                                    <b><?php echo $row['site'] ?></b>  <?php echo $this->fc->idtgl($row['pubdate'],'full') ?>
                                </span>
                                <span class="dsw-info-box-text">
                                    <a href="<?php echo $row['link'] ?>" target="_blank"><?php echo $row['berita'] ?></a>
                                </span>
                            </div>
                        </div>
                    <?php
                        $i++;
                    }
                    ?>

                </div>
            </div>
        </div>
    </div>

  <div class="col-md-3">
    <div class="dsw-info-box">
      <span class="dsw-info-box-icon bg-light-blue"><i class="fa fa-newspaper-o"></i></span>
      <div class="dsw-info-box-content">
        <span class="dsw-info-box-text">BKF Daily News [dev]</span>
        <a href="http://www.fiskal.kemenkeu.go.id/data/document/daily-news/Daily%2031122015.pdf" target="_blank"><span class="dsw-info-box-number">31 Des 2015</span></a>
      </div>
    </div>
  </div>

  <div class="col-md-3">
    <div class="dsw-info-box">
        <?php $i=1;  
          foreach ($absensi as $row) { 
            if ($i==1) { ?>
                <?php if (strtotime($row['masuk']) < strtotime( substr($row['masuk'],0,11).'07:31:00' )) { $bgabsen='bg-green'; } else { $bgabsen='bg-red'; } ?>
                <span class="dsw-info-box-icon <?php echo $bgabsen ?>"><i class="fa fa-clock-o"></i></span>
                <div class="dsw-info-box-content">
                    <span class="dsw-info-box-text"><b><?php echo $this->fc->idtgl($row['tanggal'],'hari'); ?></b></span>
                    <span class="dsw-info-box-number text-yellow"><?php echo substr($row['masuk'],11,5) ?> | 
            <?php } else { ?>
                        <span class="small text-muted"><?php echo substr($row['masuk'],11,5) ?> - <?php echo substr($row['keluar'],11,2) + 12; echo substr($row['keluar'],13,3) ?></span>
                    </span>
            <?php } 
            $i++;
          } ?>
      </div>
    </div>
  </div>
</div>


<div class="box box-widget">
    <div class="box-header with-border">
        <h3 class="box-title"> Monitoring Capaian</h3>
        <div class="box-tools">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
        </div>
    </div>
    <div class="box-body" style="padding-top:0px">
        <div class="row">
            <?php foreach ($d_moncap as $row) { ?>
            <div class="col-sm-4 col-md-2">
                <?php
                    if ( $row['progres'] < 50 ) { $warna='red'; }
                    elseif ( $row['progres'] < 75 ) { $warna='yellow'; }
                    else { $warna='green'; }
                ?>
                <h4 class="control-sidebar-subheading" style="margin-bottom:3px"><a href="<?php echo site_url('fokus/bidang/'.$row['id_bidang']) ?>">Bidang <b><?php echo $row['id_bidang'] ?></b></a>
                    <span class="text-muted pull-right"><b><?php echo $row['jumlah'] ?></b> Output</span>
                </h4>
                <div class="progress-bar progress-bar-danger" style="width: <?php echo $row['progres'] ?>%">
                </div>
                <div class="progress" style="margin-bottom:5px">
                   <div class="progress-bar progress-bar-<?php echo $warna ?>" role="progressbar" aria-valuemin="<?php echo $row['progres'] ?>" aria-valuemax="100" style="width:<?php echo $row['progres'] ?>%"><?php echo $row['progres'] ?>%</div>
               </div>
            </div>
            <?php } ?>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">

        <!-- hot thread  -->
        <div class="box box-widget">
            <div class="box-header with-border">
                <h3 class="box-title">Hot Thread</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool"> Seluruhnya</button>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body" style="padding:0px 10px 10px 10px">
                <ul class="products-list product-list-in-box">

                    <?php foreach ($hotthread as $row) { ?>
                    <li class="item" style="padding:0px">
                        <div class="product-info" style="margin-left:0px">
                            <!-- <a href="#" class="product-title"><-?php echo $row['nmhome'] ?></a> -->
                            <span class="small product-description">
                                <a href="<?php echo site_url('forum/forum_post').'/'. $row['idparent'] .'/'. $row['idchild'] ?>"><?php echo $row['nmroom'] ?></a>
                                <span class="small text text-primary pull-right"><i class="fa fa-reply-all"></i> : <?php echo $row['thread']-1 ?></span>
                            </span>
                        </div>
                    </li>
                    <?php } ?>

                </ul>
            </div>
        </div>

        <!-- last comment -->
        <?php foreach ($d_forum as $row) { ?>
            <div class="box box-widget">
                <div class="box-header with-border">
                    <div class="user-block">
                        <img class="img-circle img-bordered-dsw" src="files/profiles/<?php echo $row['nip'] ?>.gif" alt="User Image">
                        <span class="username"><a href="#"><?php echo $row['nmuser'] ?></a></span>
                        <span class="description"><?php echo '<b>'. $row['nmhome'] .'</b> - '. $this->fc->idtgl( $row['tglcreate'], 'full') ?></span>
                    </div>
                    <div class="box-tools">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <h4 class="attachment-heading" style="margin-top:0px; ">
                    <!-- <i class="fa fa-comments-o"></i> -->
                    <a href="<?php echo site_url('forum/forum_post').'/'. $row['idparent'] .'/'. $row['idchild'] ?>"><?php echo $row['nmroom'] ?></h4></a>
                    <?php if ($row['gambar']) { ?>
                        <img width="180px" class="img-responsive pad" src="files/forum/<?php echo $row['gambar'] ?>" style="float:left;padding-left:0px; padding-top:0px">
                    <?php } ?>
                    <p><?php echo $row['post'] ?></p>
                </div>
                <!-- /.box-body -->

                <?php if ($row['jumlah']>4) { ?>
                <div class="box-footer">
                    <div class="small text-center">
                        <a href="<?php echo site_url('forum/forum_post').'/'. $row['idparent'] .'/'. $row['idchild'] ?>">Tampilkan Seluruh Tanggapan </a>
                    </div>
                </div>
                <?php } ?>

                <div class="box-footer box-comments">

                <?php if ($row['thread']) {
                    foreach ($row['thread'] as $col) { ?>
                    <div class="box-comment">
                        <!-- User image -->
                        <img class="img-circle img-sm img-bordered-dsw" src="files/profiles/<?php echo $col['nip'] ?>.gif" alt="User Image">
                        <div class="comment-text">
                            <span class="username">
                                <?php echo $col['nmuser'] ?>
                                <span class="text-muted pull-right"><?php echo $this->fc->idtgl( $col['tglpost'], 'full') ?></span>
                            </span><!-- /.username -->
                            <div class="comment more"><?php echo trim($col['post']) ?></div>
                        </div>
                    </div>
                <?php
                    }
                } ?>
                </div>

                <div class="box-footer">
                    <form action="<?php echo site_url('home/tanggapan/'. $row['idparent'] .'/'. $row['idchild']) ?>" method="post">
                        <img class="img-responsive img-circle img-sm img-bordered-dsw" src=
                        <?php
                            $foto_profile="files/profiles/_noprofile.png";
                            if (file_exists("files/profiles/".$this->session->userdata('nip').".gif")) {$foto_profile =  "files/profiles/".$this->session->userdata('nip').".gif";}
                            echo $foto_profile;
                        ?> alt=<?php $this->session->userdata('nmuser')?>>
                        <div class="img-push">
                            <input type="text" name="comment" class="form-control input-sm" value="" placeholder="Tanggapan ...">
                        </div>
                    </form>
                </div>
            </div>
        <?php } ?>

    </div>

    <div class="col-md-6">
        <div class="box box-widget">
            <div class="box-header with-border">
                <h3 class="box-title text"> Pengumuman</h3>
                <div class="box-tools">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
            </div>

            <?php
            $first = 1;
            foreach ($pengumuman as $row) {
                $arr = explode('<p>', $row['pengumuman']);
                $pengumuman = trim( $arr[1] );
                $lengkapnya = str_replace($pengumuman, '', $row['pengumuman']);

                if ($first==1) {
                    $first++; ?>

                    <!-- pengumuman terbaru -->
                    <div class="box-body" style="background-color:#F7F7F7" >
                        <div class="attachment-block clearfix" style="padding:0px; margin-bottom:0px">
                            <?php
                            if ($row['gambar']) {
                                echo '<img class="attachment-img" src="'. base_url("files/pengumuman") .'/'. $row['gambar'] .'">';
                            } else {
                                echo '<img class="attachment-img" src="'. base_url("files/images/depkeu_news.png") .'">';
                            }
                            ?>
                            <div class="attachment-pushed">
                                <span class="username">
                                    <b>Sekretariat</b>
                                    <span class="small text-muted pull-right"><?php echo $this->fc->idtgl($row['tglrekam'], 'hari') ?></span>
                                </span>
                                <h4 class="attachment-heading"><a href="#" onclick="showHide('<?php echo 'hidden_div'.$row['idpengumuman'] ?>'); return false;"><?php echo $row['judul'] ?></a>
                                </h4>

                                <div class="attachment-text">
                                    <?php
                                    echo $pengumuman ;
                                    echo '<div id="'. 'hidden_div'.$row['idpengumuman'] .'" style="display: none;">'. $lengkapnya .'</div>';
                                    if ($row['file']) {
                                        echo '<i class="small">File : </i>
                                        <a href="'.base_url("files/pengumuman") .'/'. $row['file'] .'"  target=_blank> &nbsp;<i class=" small fa fa-file-pdf-o"></i> <i class="small">'. $row['file'] .'</a></i>';
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- <div class="<?php if ($first==2) echo 'box-footer'; ?> box-comments"> -->
                    <div class="box-footer box-comments" style="background:white">
                <?php } else { ?>

                        <div class="box-comment">
                            <img class="img-circle img-sm img-bordered-dsw" src="<?php echo base_url("files/images/depkeu_round.png") ?>">
                            <div class="comment-text">
                                <span class="username">
                                    Sekretariat
                                    <span class="text-muted pull-right"><?php echo $this->fc->idtgl($row['tglrekam'], 'hari') ?></span>
                                </span>
                                <a href="#" onclick="showHide('<?php echo 'hidden_div'.$row['idpengumuman'] ?>'); return false;"><?php echo $row['judul'] ?></a><br>
                                <?php
                                if ($row['gambar']) {
                                    echo '<img style="width:95px !important; height:75px !important; margin: 5px 10px 5px 0px" src="'. base_url("files/pengumuman") .'/'. $row['gambar'] .'">';
                                }
                                echo $pengumuman ;
                                echo '<div id="'. 'hidden_div'.$row['idpengumuman'] .'" style="display: none;">'. $lengkapnya .'</div>';
                                if ($row['file']) {
                                    echo '<i class="small">File : </i>
                                    <a href="'.base_url("files/pengumuman") .'/'. $row['file'] .'" target=_blank><i class=" small fa fa-file-pdf-o"></i> <i class="small">'. $row['file'] .'</a></i>';
                                }
                                ?>
                            </div>
                        </div>

                <?php $first++;
                }
           } ?>
                    </div>

            <div class="box-footer">
                <div class="small text-center">
                    <a href="<?php echo site_url('home/pengumuman') ?>">Tampilkan Seluruh Pengumumam </a>
                </div>
            </div>
        </div>

        <div class="box box-widget">
            <div class="box-header">
                <h3 class="box-title"> Surat Terkini</h3>
                <div class="box-tools">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
            </div>

            <?php
            foreach ($d_surat as $row) {
                $golsurat = 'ND-';
                if ($row['golsurat']=='1' and $row['jenis']=='10') $golsurat = 'RSM-';
                if ($row['golsurat']=='2') $golsurat = 'UND-';
            ?>
                <div class="box-footer box-comments" style="background:white; padding-bottom:0px">
                    <div class="box-comment">
                        <img class="img-circle img-sm img-bordered-dsw" src="<?php echo base_url("files/images/depkeu_round.png") ?>">
                        <div class="comment-text">
                            <span class="username">
                                No Agd : <?php echo $golsurat . $row['noagenda'] . $row['noagenda1'] ?>
                                <?php
                                if ( date('d-m-Y', strtotime($row['tgagenda']))== date('d-m-Y') ) {
                                    echo '<span class="text-muted pull-right">'. substr($row['tgagenda'],11,5) .' WIB</span>';
                                } else {
                                    echo '<span class="text-muted pull-right">'. $this->fc->idtgl( $row['tgagenda'], 'hr' ) .'</span>';
                                } ?>
                            </span>
                            <span class="small text-info"><?php echo $row['uraian'] ?></span><br>
                            <?php echo $row['perihal'] ?>
                        </div>
                    </div>
                </div>

            <?php } ?>

            <!-- <div class="box-footer">
                <div class="small text-center">
                    <a href="<?php echo site_url('persuratan') ?>">Tampilkan Monitoring Disposisi Surat </a>
                </div>
            </div> -->

        </div>
        <!--  -->
        <!--  -->
    </div>
    <!-- <div class="col-md-6">
        <div class="box box-widget">
            <div class="carousel slide">
                 <div class="carousel-inner">
                     <div class="item active">
                         <a href="#"><img src="../files/pengumuman/APBN_2016_pdf.jpg"></a>
                     </div>
                 </div>
            </div>
        </div>
    </div> -->


</div>




<script type="text/javascript">
    function showHide(obj) {
        var div = document.getElementById(obj);
            if (div.style.display == 'none') {
            div.style.display = '';
        } else {
            div.style.display = 'none';
        }
    }
</script>
