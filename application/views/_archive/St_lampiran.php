<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class St_lampiran extends CI_Controller {

	function __construct() {
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
    	$this->load->model('st_lampiran_model');
        $this->session->set_userdata( array('cari' => '') );
	}

	public function index() {
		$this->grid($idst, 'data', '01');
		/*$this->grid(7, 'data', '01');*/
	}

	public function grid($idst, $tab, $es2) {
		$data = $this->st_lampiran_model->get_grid($idst, $tab, $es2);
		$data['menu']  = $this->dsw_model->get_menu( $this->uri->segment(1) ); 
		$data['bread'] = array('header'=>'Lampiran Surat Tugas','subheader'=>'ST Online');
		$data['view']  = "gallery/v_lampiran";
		//$data['view']  = "gallery/v_lampiran_data";
		$this->load->view('main/utama', $data);
	}

	public function save_peg() {
		$peg = $_POST['peg'];
		
		foreach ($peg as $row) {
		$id=$this->uri->segment(3); //echo $id; exit();	
			$sql="INSERT INTO d_st_peg (idst,nip) values ($id,'$row')"; //echo $sql; exit();
			$query = $this->db->query($sql);
		}
		$es=substr($this->session->userdata('kdso'),0,2) ; //echo $a; 
		redirect("st_lampiran/grid/$id/data/$es",'refresh');

		//redirect("st_lampiran/lampiran/$id",'refresh');
	}

	public function test() {
		$hasil = $this->st_lampiran_model->get_grid(7, 'data', '01');
		echo "<pre>"; print_r($hasil);
		// $this->fc->browse( $hasil );
	}

	public function ajax_eselon3_deleted() {
		// Query USER - Sub Direktorat
		$idkey = $_POST['idkey'];
		$query = $this->db->query("Select nmuser, nip, jabatan, golongan, kdso, kdeselon From t_user Where Left(kdso,4)='$idkey' Order By kdso,kdeselon,nmuser");

		$msg = '';
		foreach ($query->result_array() as $row) {
			$jab = 'jab' . $row['kdeselon'];
			$msg .= '
				<tr class="trPeg" id="'. $row['nip'] .'" align="justify" />
					<td class="tdPeg"></td>
					<td class="NmPeg '. $jab .'">'. $row['nmuser'] .'</td>
					<td class="tdPeg '. $jab .' text-center">'. $row['nip'] .'</td>
					<td class="tdPeg '. $jab .'">'. $row['jabatan'] .'</td>
					<td class="tdPeg '. $jab .' text-center">'. $row['golongan'] .'</td>
					<td class="tdPeg '. $jab .'"></td>
					<td class="tdPeg"><input type="checkbox" name="check1"></td>
				</tr>';
		}
		echo $msg; 
	}

	//cetak lampiran
	function pdf( $idst ) {
		$data['st']  = $this->St_model->get_st( $idst );
		$data['menu']  = $this->dsw_model->get_menu( $this->uri->segment(1) );
		$data['bread'] = array('header'=>'Admin Surat Tugas', 'subheader'=>'Admin Surat Tugas');
		$data['view']  = "st/cetakst";
		$this->load->view('st/cetakst', $data);
		
		$html = $this->output->get_output();

		// Convert to PDF
		$this->dompdf = new DOMPDF();
		$this->dompdf->load_html($html);
		$this->dompdf->set_paper('A4','portraite');
		$this->dompdf->render();
		$this->dompdf->stream('cetakst'.'.pdf',array('Attachment'=>0));
	}


}
