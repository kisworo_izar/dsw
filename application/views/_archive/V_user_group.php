<div class="row">
	<div class="col-md-12">
		<div class="box box-widget">
			<div class="box-header with border">
				<div class="wel wel-sm">
					<form id="iForm" class="form-inline" role="form" action="<?php echo site_url("paket/cari") ?>" method="post">
						<div class="form-group pull-right">
							<div class="input-group">
                    			<input type="hidden" name="nmfunction" value="grid">
                				<input class="form-control" name="cari" type="text" value="<?php echo $this->session->userdata('cari') ?>">
            					<span class="input-group-btn">
            						<button type="submit" name="search" value="search" class="btn btn-info btn-flat"><i class="fa fa-search"></i></button>
            					</span>
            					<span class="input-group-btn">
            						<button type="submit" name="search" value="clear" class="btn btn-info btn-flat"><i class="fa fa-times-circle"></i></button>
                				</span>
                			</div>
                		</div>
                	</form>
                </div>
            </div>   <!-- box header  -->


			<div class="box-body">
				<h2>User Group</h2>
				<table id="iGrid" class="table table-hover table-bordered">
					<thead>
					<tr>
						<th>ID Group</th>
						<th>Group User</th>
						<th>Menu</th>
					</tr>
					</thead>
				<tbody>
					<?php foreach ($group as $row) { ?>
						<tr>
							<td><?php echo $row['idusergroup'] ?></td>
							<td><?php echo $row['nmusergroup'] ?></td>
							<td>
								<?php foreach ($menu as $nil) { 
									echo $nil['idmenu'] ." - \t - \t". $nil['menu'] .'<br>';
								} ?>
							</td>
						</tr>
					<?php } ?>
				</tbody>
				</table>
			</div> <!-- box body -->

		</div>
	</div>
</div>

