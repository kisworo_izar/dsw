<script src="<?=base_url('assets/plugins/summernote/summernote.min.js'); ?>"></script>
<link rel="stylesheet" href="<?=base_url('assets/plugins/summernote/summernote.css'); ?>">

<script src="<?php echo base_url();?>assets/plugins/uploadfile/fileinput.min.js" type="text/javascript"></script>
<link href="<?php echo base_url(); ?>assets/plugins/uploadfile/fileinput.css" rel="stylesheet">

<div class="row">
    <div class="col-md-9">
        <div class="box box-widget" style="border-top: 1px solid #D2D6DE; margin-bottom:10px">
            <form action="<?php echo site_url('forum/save_thread') ?>" id="postForm" method="post" role="form" enctype="multipart/form-data" onsubmit="return postForm()">
                <div class="box-header" style="background :#F7F7F7; border-bottom:1px solid #D2D6DE; padding:0px">
                    <div class="form-group" style="margin:10px">
                        <label>Tulisan :</label>
                        <textarea id="summernote" name="content"></textarea>
                    </div>
                    <div class="form-group" style="margin:10px">
                        <label>Files :</label> 
                        <div class="input-group">
                            <div class="input-group-addon"><i class="fa fa-external-link"></i></div>
                            <input name="file" type="file" class="file" />
                        </div>
                    </div>
        			<span class="input-group-btn">
		  				<button name="simpan" type="submit" class="btn btn-xm btn-primary pull-right">Submit</button>
        			</span>
                </div>
            </form>
        </div>
    </div>

</div>


<script type="text/javascript">
    $(document).ready(function() {
        $('#summernote').summernote({
            height: 350,
            placeholder: 'Isikan posting Anda disini ...',
            onImageUpload: function(files, editor, welEditable) {
                sendFile(files[0], editor, welEditable);
            }
        });

        function sendFile(file,editor,welEditable) {
            data = new FormData();
            data.append("file", file);
            $.ajax({
                url: "<?php echo site_url('forum/upload_image') ?>",
                data: data,
                cache: false,
                contentType: false,
                processData: false,
                success: function(data){
                    alert(data);
                    $('.summernote').summernote("insertImage", data, 'filename');
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    console.log(textStatus+" "+errorThrown);
                }
            });
        }
    });

    var postForm = function() {
        var content = $('textarea[name="content"]').html($('#summernote').code());
    }
</script>

<script type="text/javascript">
    $("#files").fileinput({
        uploadUrl: "<?php echo site_url('upload/fileupload') ?>", 
        allowedFileExtensions : ['15','16','jpg','xlsx','doc','docx','rtf','pdf'],
        overwriteInitial: true,
        maxFileSize: 1000,
        maxFilesNum: 10,
        minFileCount: 1,
        maxFileCount: 5,
        dropZoneTitle: 'Drag & Drop file ADK RKA-KL dan TOR/RAB disini ...',
        msgInvalidFileExtension: "Invalid extension file {name}. Hanya {extensions} files yang bisa diproses ...",
        slugCallback: function(filename) {
            return filename.replace('(', '_').replace(']', '_');
        }
    });
</script>