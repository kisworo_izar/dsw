<link rel="stylesheet" href="<?=base_url('assets/plugins/select2/select2.css'); ?>">
<script src="<?=base_url('assets/plugins/select2/select2.full.min.js'); ?>"></script>

<style media="screen">
  th { text-align: center; }
    thead tr { color: #fff; background: #00acd6; }
    tbody tr .row-1 { color: #fff; background: #00acd6; }

  .level-1 { font: bold 14px/5px sans-serif; color: #00e; }
  .level-2 { font: bold 13px sans-serif; color: #000; }
  .level-3 { font: italic bold 12px sans-serif; color: #000; }
  .level-4 { font: 12px sans-serif; color: #000; }
  .level-5 { font: 12px sans-serif; color: #000; }
  .level-6 { font: italic 12px sans-serif; color: #000; }
  .level-7 { font: 11px sans-serif; color: #000; padding: 50px 30px 50px 80px;}
  .level-8 { font: 11px sans-serif; color: #7a7a7a; }

  .item {padding: 0px 2px 0px 20px;}
</style>

<div class="row">
  <div class="col-md-12">

      <div class="box-body">
        <table class="table table-hover table-bordered table-condensed">
          <thead>
            <tr>
              <th width="10%" rowspan="2">KODE<br>&nbsp;</th>
              <th width="39%" rowspan="2">PROGRAM/KEGIATAN/OUTPUT<br>SUB OUTPUT / KOMPONEN</th>
              <th width="48%" colspan="4">PERHITUNGAN </th>
              <th width="3%" rowspan="2">SD/<br>CP</th>
            </tr>
            <tr>
              <th width="15%">VOLUME</th>
              <th width="15%">HARGA SATUAN</th>
              <th width="18%" colspan="2">JUMLAH BIAYA</th>
            </tr>
          </thead>
          <tbody>

            <?php foreach ($tabel as $row) { 
                $class   = ' text-right'; if ($row['level']<=3) $class = ' text-center';
                $volkeg  = '';  if ($row['volkeg']>0) $volkeg = number_format($row['volkeg'], 0, ',', '.') .' '. $row['satkeg'];
                $hargasat= '';  if ($row['hargasat']>0) $hargasat = number_format($row['hargasat'], 0, ',', '.');
                $uraian  = $row['uraian'];  

                if ($row['level']==7) $uraian = "<u>$uraian</u>";
                if ($row['level']==8) {
                  $uraian = '<div class="item">'. $row['uraian'] .'</div>';
                  $volkeg = number_format($row['volkeg'], 2, ',', '.') .' '. $row['satkeg'];
                }
              ?>

              <tr class="row-<?php echo $row['level'] ?>">
                <td class="level-<?php echo $row['level']; echo $class ?>"><?php echo $row['kode'] ?></td>
                <td class="level-<?php echo $row['level'] ?> text-left"><?php echo $uraian ?></td>
                <td class="level-<?php echo $row['level'] ?> text-right"><?php echo $volkeg ?></td>
                <td class="level-<?php echo $row['level'] ?> text-right"><?php echo $hargasat ?></td>
                <td class="level-<?php echo $row['level'] ?> text-right"><?php echo number_format($row['jumlah'], 0, ',', '.') ?></td>
                <td class="level-<?php echo $row['level'] ?> text-right"><?php echo $row['atrib'] ?></td>
                <td class="level-<?php echo $row['level'] ?> text-right"> &nbsp; </td>
              </tr>

            <?php } ?>

          </tbody>
        </table>

      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  function myFunction() {
    var output = document.getElementById("mySelect").value;
    window.location.href = "<?php echo site_url('rkakl/report/') ?>" +'/'+ output;
  }
</script>
