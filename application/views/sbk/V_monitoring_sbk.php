<link rel="stylesheet" href="<?=base_url('assets/plugins/select2/select2.css'); ?>">
<script src="<?=base_url('assets/plugins/select2/select2.full.min.js'); ?>"></script>

<style media="screen">
  th { text-align: center; }
    thead tr { color: #fff; background: #00acd6; }
    tbody tr .row-1 { color: #fff; background: #00acd6; }

  .level-1 { font: bold 14px/5px sans-serif; color: #00e; }
  .level-2 { font: bold 13px sans-serif; color: #000; }
  .level-3 { font: italic bold 12px sans-serif; color: #000; }
  .level-4 { font: 12px sans-serif; color: #000; }
  .level-5 { font: 12px sans-serif; color: #000; }
  .level-6 { font: italic 12px sans-serif; color: #000; }
  .level-7 { font: 11px sans-serif; color: #000; padding: 50px 30px 50px 80px;}
  .level-8 { font: 11px sans-serif; color: #7a7a7a; }

  .item {padding: 0px 2px 0px 20px;}
</style>

<div class="row">
  <div class="col-md-12">
    <div class="box box-widget">

      <div class="box-header with border">
        <div class="wel wel-sm">
          <form id="iForm" class="form-inline" role="form" action="" method="post">
            <div class="form-group">
              <div class="input-group">
                <div style="margin: 0px">
                  <select class="js-output form-control" id="mySelect" onchange="myFunction()">
                    <option value=""></option>
                    <?php
                    $hit = 0;
                    foreach ($tabel as $row) {
                      if ( $row['level'] == 1) {
                          if ( $hit!=0) { echo '</optgroup>'; $hit++; }  // Clossing Group
                          echo '<optgroup label="'. $row['kode'].' '.substr($row['uraian'],0,50) .'">';
                      }
                      if ( $row['level'] == 2) {
                          $selc=''; if ($row['kdkey']==$depuni) $selc='selected';
                          echo '<option value="'. $row['kdkey'] .'" '. $selc .'> &nbsp;'. $row['kode'].' '.substr($row['uraian'],0,50) .'</option>';
                      }
                    } ?>
                  </select>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>

      <?php
        if ( $depuni!=null ) {
         foreach ($tabel as $key=>$value) {
           if ( substr($value['kdkey'],0,6) != $depuni ) unset( $tabel[$key] );
         }
        }
      ?>


      <div class="box-body">
        <table class="table table-hover table-bordered table-condensed">
          <thead>
            <tr>
              <th width=" 8%">Kode</th>
              <th width="40%">Uraian</th>
              <th width="15%">Volume</th>
              <th width="15%">SBK 2016</th>
              <th width="15%">Prakiraan SBK 2017</th>
              <!-- <th width=" 7%"> % </th> -->
            </tr>
          </thead>
          <tbody>

            <?php foreach ($tabel as $row) {
              $volkeg = ''; if ($row['vol']!='') $volkeg = number_format($row['vol'], 0, ',', '.') .' '. $row['sat'];
            ?>

              <tr class="row-<?php echo $row['level'] ?>">
                <td class="level-<?php echo $row['level'] ?> text-center"><?php echo $row['kode'] ?></td>
                <td class="level-<?php echo $row['level'] ?> text-left"><?php echo $row['uraian'] ?></td>
                <td class="level-<?php echo $row['level'] ?> text-center"><?php echo $volkeg ?></td>

                <?php if ($row['level']!=4) { ?>
                  <td class="level-<?php echo $row['level'] ?> text-left" colspan="3"><?php echo number_format($row['jumlah'], 0, ',', '.') ?></td>
                <?php } else {
                  $jml_16 = '<a href="'. site_url('sbk/kertas_kerja/2016/'.$row['kode']) .'">'. number_format($row['jumlah'], 0, ',', '.') .'</a>';
                  $jml_17 = '<a href="'. site_url('sbk/kertas_kerja/2017/'.$row['kode']) .'">'. number_format($row['exercise'], 0, ',', '.') .'</a>';
                ?>
                  <td class="level-<?php echo $row['level'] ?> text-right"><?php echo $jml_16 ?></td>
                  <td class="level-<?php echo $row['level'] ?> text-right"><?php echo $jml_17 ?></td>
                  <!-- <td class="level-<?php echo $row['level'] ?> text-center"><?php echo number_format($row['persen'], 2, ',', '.') ?></td> -->
                <?php } ?>
              </tr>

            <?php } ?>

          </tbody>
        </table>

      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  function myFunction() {
    var depuni = document.getElementById("mySelect").value;
    window.location.href = "<?php echo site_url('sbk/monitoring/') ?>" +'/'+ depuni;
  }
</script>
