<script src="<?=base_url('assets/plugins/slimScroll/jquery.slimscroll.min.js'); ?>" type="text/javascript"></script>    
<script src="<?=base_url('assets/plugins/jQueryUI/jquery-ui.1.11.4.min.js'); ?>" type="text/javascript"></script>
<link rel="stylesheet" href="<?=base_url('assets/plugins/jQueryUI/jquery-ui.css'); ?>">

 <link rel="stylesheet" href="<?=base_url('assets/plugins/select2/select2.min.css'); ?>" type="text/css" />
<link href="<?=base_url('assets/dist/css/AdminLTE.min.css');?>" rel="stylesheet" type="text/css" />
 <script src="<?=base_url('assets/plugins/select2/select2.full.min.js');?>"></script>
<style>
/* Important part */
.modal-dialog{
    overflow-y: initial !important
}
.modal-body{
    overflow-y: auto;
}</style>   

    <div class="modal fade" id="myModal">
      <div class="modal-dialog">
        <div class="modal-content">
          <form action="<?php echo site_url('iku/crud') ?>" method="post" role="form">
            <div class="modal-header">
              <h4 id="judul-form" class="modal-title">Data Paket</h4>
            </div>
            <div class="modal-body">
              <div id="view_modal">
               <table class="table table-hover table-stripted">
                  <tr><th width="50%">Parameter</th><th>Value</th> </tr>
                  <tr><td style="text-align:right;"><b>ID</b></td><td id = "no"></td></tr>
                  <tr><td style="text-align:right;"><b>Nama</b></td><td id = "namaData"></td></tr>
                  <tr><td style="text-align:right;"><b>Perspektif</b></td><td id = "perspektifData"></td></tr>
                  <tr><td style="text-align:right;"><b>Sasaran Strategis</b></td><td id = "sasaranStrategisData"></td></tr>
                  <tr><td style="text-align:right;"><b>Desc Sasaran Strategis</b></td><td id="descSasaranStrategisData"></td></tr>
                  <tr><td style="text-align:right;"><b>IKU Monitoring</b></td><td id = "IKUMonitoringData"></td></tr>
                  <tr><td style="text-align:right;"><b>IKU Atasan</b></td><td id = "IKUAtasanData"></td></tr>
                  <tr><td style="text-align:right;"><b>IKU</b></td><td id = "IKUData"></td></tr>
                  <tr><td style="text-align:right;"><b>Definisi</b></td><td id = "definisiData"></td></tr>
                  <tr><td style="text-align:right;"><b>Formula</b></td><td id = "formulaData"></td></tr>
                  <tr><td style="text-align:right;"><b>Tujuan</b></td><td id = "tujuanData"></td></tr>
                  <tr><td style="text-align:right;"><b>Satuan Pengukuran</b></td><td id = "satuanPengukuranData"></td></tr>
                  <tr><td style="text-align:right;"><b>Jenis Aspek Target</b></td><td id = "jenisAspekTargetData"></td></tr>
                  <tr><td style="text-align:right;"><b>Tingkat Kendali IKU</b></td><td id = "tingkatKendaliIKUData"></td></tr>
                  <tr><td style="text-align:right;"><b>Tingkat Validitas IKU</b></td><td id = "tingkatValiditasIKUData"></td></tr>
                  <tr><td style="text-align:right;"><b>Pihak Penanggung Jawab IKU</b></td><td id = "pihakPJIKUData"></td></tr>
                  <tr><td style="text-align:right;"><b>Pihak Penyedia Jasa</b></td><td id = "pihakPenyediaJasaData"></td></tr>
                  <tr><td style="text-align:right;"><b>Sumber Data</b></td><td id = "sumberDataData"></td></tr>
                  <tr><td style="text-align:right;"><b>Jenis Cascading IKU</b></td><td id = "jenisCascadingIKU"></td></tr>
                  <tr><td style="text-align:right;"><b>Metode Cascading</b></td><td id = "metodeCascading"></td></tr>
                  <tr><td style="text-align:right;"><b>Jenis Konsolidasi Periode</b></td><td id="jenisKonsolidasiPeriodeData"></td></tr>
                  <tr><td style="text-align:right;"><b>Jenis Konsolidasi Lokasi</b></td><td id = "jenisKonsolidasiLokasiData"></td></tr>
                  <tr><td style="text-align:right;"><b>Polarisasi Indikator Kinerja</b></td><td id = "polarisasiIndikatorKinerjaData"></td></tr>
                  <tr><td style="text-align:right;"><b>Periode Pelaporan</b></td><td id = "periodePelaporanData"></td></tr>
                  <tr><td style="text-align:right;"><b>Konversi 120</b></td><td id = "konversi120Data"></td></tr>
                  <tr><td style="text-align:right;"><b>File Document</b></td><td><a  id = "fileDocumentData" href="#" ></a></td></tr>
                  <tr><td style="text-align:right;"><b>Created Time</b></td><td id = "createdTimeStampData"></td></tr>
                  <tr><td style="text-align:right;display:none;"><b>IdPeriode</b></td><td style="display:none;" id = "idPeriodeData"></td></tr>
               </table>
               <?php /*       <input name="id" id="id" type="text" class="form-control" readonly="readonly" value="">
               Tgl. Rekam :
                <input name="tglrekam" id="tglrekam" type="text" class="form-control" readonly="readonly" value="">
                Penerima :
                <input name="idterima" id="idterima" type="hidden" value="">
                <input name="nmterima" id="nmterima" type="text" class="form-control" value="">*/ ?>
              </div>              

              <!-- jika klik rekam tampilkan field ini -->  

               <input id="action" name="action" value="" type="hidden" />

              <div id="input">
                  <div class="box-body">
                    <div class="form-group" style="display:none">
                      <label class="col-sm-5 control-label" style="text-align: right">ID</label>
                      <div class="col-sm-7">
                        <div class="form-group">  
                           <input id="idForm" value="" name="idForm" class="form-control" />
                        </div><!-- /.form-group -->
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-5 control-label" style="text-align: right">Perspektif</label>
                      <div class="col-sm-7">
                        <div class="form-group">  
                          <select id="perspektifForm" name="perspektifForm" class="form-control select2" style="width: 100%;">
                            <option value=""><i>- SELECT -</i></option>
                            <?php foreach($PERSPEKTIF->result() as $row): ?>
                              <option value="<?php echo $row->id; ?>"><?php echo $row->name; ?></option>
                            <?php endforeach; ?>
                          </select>
                        </div><!-- /.form-group -->
                      </div>
                    </div><br/><br/>
                    <div class="form-group">
                      <label class="col-sm-5 control-label" style="text-align: right">Sasaran Strategis</label>
                      <div class="col-sm-7">
                        <div class="form-group">  
                          <select id="sasaranStrategisForm" name="sasaranStrategisForm" class="form-control select2" style="width: 100%;">
                            <option value=""><i>- SELECT -</i></option>
                            <?php foreach($SASARAN_STRATEGIS->result() as $row): ?>
                              <option value="<?php echo $row->id; ?>"><?php echo $row->name; ?></option>
                            <?php endforeach; ?>
                          </select>
                        </div><!-- /.form-group -->
                      </div>
                    </div><br/><br/>
                    <div class="form-group">
                      <label class="col-sm-5 control-label" style="text-align: right">Desc Sasaran Strategis</label>
                      <div class="col-sm-7">
                        <div class="form-group">  
                          <textarea id="descSasaranStrategisForm" name="descSasaranStrategisForm" class="form-control" placeholder="Deskripsi Sasaran Strategis ..." rows="3"></textarea>
                        </div><!-- /.form-group -->
                      </div>
                    </div><br/><br/>
                    <div class="form-group">
                      <label class="col-sm-5 control-label" style="text-align: right">Indikator Kinerja Utama</label>
                      <div class="col-sm-7">
                        <div class="form-group">  
                          <select id="IKUForm" name="IKUForm" class="form-control select2" style="width: 100%;">
                            <option value=""><i>- SELECT -</i></option>
                            <?php foreach($IKU->result() as $row): ?>
                              <option value="<?php echo $row->id; ?>"><?php echo $row->name; ?></option>
                            <?php endforeach; ?>
                          </select>
                        </div><!-- /.form-group -->
                      </div>
                    </div><br/><br/>
                    <div class="form-group">
                      <label class="col-sm-5 control-label" style="text-align: right">Definisi</label>
                      <div class="col-sm-7">
                        <div class="form-group">  
                          <textarea id="definisiForm" name="definisiForm" class="form-control" placeholder="Definisi ..." rows="3"></textarea>
                        </div><!-- /.form-group -->
                      </div>
                    </div><br/><br/>
                    <div class="form-group">
                      <label class="col-sm-5 control-label" style="text-align: right">Formula</label>
                      <div class="col-sm-7">
                        <div class="form-group">  
                          <textarea id="formulaForm" name="formulaForm" class="form-control" placeholder="Formula ..." rows="3"></textarea>
                        </div><!-- /.form-group -->
                      </div>
                    </div><br/><br/>
                    <div class="form-group">
                      <label class="col-sm-5 control-label" style="text-align: right">Tujuan</label>
                      <div class="col-sm-7">
                        <div class="form-group">  
                          <textarea id="tujuanForm" name="tujuanForm" class="form-control" placeholder="Tujuan ..." rows="3"></textarea>
                        </div><!-- /.form-group -->
                      </div>
                    </div><br/><br/>
                    <div class="form-group">
                      <label class="col-sm-5 control-label" style="text-align: right">Satuan Pengukuran</label>
                      <div class="col-sm-7">
                        <div class="form-group">  
                          <select id="satuanPengukuranForm" name="satuanPengukuranForm" class="form-control select2" style="width: 100%;">
                            <option value=""><i>- SELECT -</i></option>
                            <?php foreach($SATUAN_PENGUKURAN->result() as $row): ?>
                              <option value="<?php echo $row->id; ?>"><?php echo $row->name; ?></option>
                            <?php endforeach; ?>
                          </select>
                        </div><!-- /.form-group -->
                      </div>
                    </div><br/><br/>
                    <div class="form-group">
                      <label class="col-sm-5 control-label" style="text-align: right">Jenis Aspek Target</label>
                      <div class="col-sm-7">
                        <div class="form-group">  
                          <select id="jenisAspekTargetForm" name="jenisAspekTargetForm" class="form-control select2" style="width: 100%;">
                            <option value=""><i>- SELECT -</i></option>
                            <?php foreach($JENIS_ASPEK_TARGET->result() as $row): ?>
                              <option value="<?php echo $row->id; ?>"><?php echo $row->name; ?></option>
                            <?php endforeach; ?>
                          </select>
                        </div><!-- /.form-group -->
                      </div>
                    </div><br/><br/>
                    <div class="form-group">
                      <label class="col-sm-5 control-label" style="text-align: right">Tingkat Kendali IKU</label>
                      <div class="col-sm-7">
                        <div class="form-group">  
                          <select id="tingkatKendaliIKUForm"  name="tingkatKendaliIKUForm" class="form-control select2" style="width: 100%;">
                            <option value=""><i>- SELECT -</i></option>
                            <?php foreach($TINGKAT_KENDALI_IKU->result() as $row): ?>
                              <option value="<?php echo $row->id; ?>"><?php echo $row->name; ?></option>
                            <?php endforeach; ?>
                          </select>
                        </div><!-- /.form-group -->
                      </div>
                    </div><br/><br/>
                    <div class="form-group">
                      <label class="col-sm-5 control-label" style="text-align: right">Tingkat Validitas IKU</label>
                      <div class="col-sm-7">
                        <div class="form-group">  
                          <select id="tingkatValiditasIKUForm" name="tingkatValiditasIKUForm"  class="form-control select2" style="width: 100%;">
                            <option value=""><i>- SELECT -</i></option>
                            <?php foreach($TINGKAT_VALIDITAS_IKU->result() as $row): ?>
                              <option value="<?php echo $row->id; ?>"><?php echo $row->name; ?></option>
                            <?php endforeach; ?>
                          </select>
                        </div><!-- /.form-group -->
                      </div>
                    </div><br/><br/>
                    <div class="form-group">
                      <label class="col-sm-5 control-label" style="text-align: right">Pihak Penanggung Jawab IKU</label>
                      <div class="col-sm-7">
                        <div class="form-group">  
                          <textarea id="pihakPJIKUForm" name="pihakPJIKUForm" class="form-control" placeholder="Pihak Penanggung Jawab IKU ..." rows="3"></textarea>
                        </div><!-- /.form-group -->
                      </div>
                    </div><br/><br/>
                    <div class="form-group">
                      <label class="col-sm-5 control-label" style="text-align: right">Pihak Penyedia Jasa</label>
                      <div class="col-sm-7">
                        <div class="form-group">  
                          <textarea id="pihakPenyediaJasaForm" name="pihakPenyediaJasaForm" class="form-control" placeholder="Pihak Penyedia Jasa ..." rows="3"></textarea>
                        </div><!-- /.form-group -->
                      </div>
                    </div><br/><br/>
                    <div class="form-group">
                      <label class="col-sm-5 control-label" style="text-align: right">Sumber Data</label>
                      <div class="col-sm-7">
                        <div class="form-group">  
                          <textarea id="sumberDataForm" name="sumberDataForm" class="form-control" placeholder="Sumber Data ..." rows="3"></textarea>
                        </div><!-- /.form-group -->
                      </div>
                    </div><br/><br/>
                    <div class="form-group">
                      <label class="col-sm-5 control-label" style="text-align: right">Jenis Cascading IKU</label>
                      <div class="col-sm-7">
                        <div class="form-group">  
                          <select id="jenisCascadingIKUForm" name="jenisCascadingIKUForm"  class="form-control select2" style="width: 100%;">
                            <option value=""><i>- SELECT -</i></option>
                            <?php foreach($JENIS_CASCADING_IKU->result() as $row): ?>
                              <option value="<?php echo $row->id; ?>"><?php echo $row->name; ?></option>
                            <?php endforeach; ?>
                          </select>
                        </div><!-- /.form-group -->
                      </div>
                    </div><br/><br/>
                    <div class="form-group">
                      <label class="col-sm-5 control-label" style="text-align: right">Metode Cascading</label>
                      <div class="col-sm-7">
                        <div class="form-group">  
                          <select id="metodeCascadingForm" name="metodeCascadingForm"  class="form-control select2" style="width: 100%;">
                            <option value=""><i>- SELECT -</i></option>
                            <?php foreach($METODE_CASCADING->result() as $row): ?>
                              <option value="<?php echo $row->id; ?>"><?php echo $row->name; ?></option>
                            <?php endforeach; ?>
                          </select>
                        </div><!-- /.form-group -->
                      </div>
                    </div><br/><br/>
                    <div class="form-group">
                      <label class="col-sm-5 control-label" style="text-align: right">Jenis Konsolidasi Periode</label>
                      <div class="col-sm-7">
                        <div class="form-group">  
                          <select id="jenisKonsolidasiPeriodeForm" name="jenisKonsolidasiPeriodeIdForm"  class="form-control select2" style="width: 100%;">
                            <option value=""><i>- SELECT -</i></option>
                            <?php foreach($JENIS_KONSOLIDASI_PERIODE->result() as $row): ?>
                              <option value="<?php echo $row->id; ?>"><?php echo $row->name; ?></option>
                            <?php endforeach; ?>
                          </select>
                        </div><!-- /.form-group -->
                      </div>
                    </div><br/><br/>
                    <div class="form-group">
                      <label class="col-sm-5 control-label" style="text-align: right">Jenis Konsolidasi Lokasi</label>
                      <div class="col-sm-7">
                        <div class="form-group">  
                          <select id="jenisKonsolidasiLokasiForm" name="jenisKonsolidasiLokasiForm"  class="form-control select2" style="width: 100%;">
                            <option value=""><i>- SELECT -</i></option>
                            <?php foreach($JENIS_KONSOLIDASI_LOKASI->result() as $row): ?>
                              <option value="<?php echo $row->id; ?>"><?php echo $row->name; ?></option>
                            <?php endforeach; ?>
                          </select>
                        </div><!-- /.form-group -->
                      </div>
                    </div><br/><br/>
                    <div class="form-group">
                      <label class="col-sm-5 control-label" style="text-align: right">Polarisasi Indikator Kinerja</label>
                      <div class="col-sm-7">
                        <div class="form-group">  
                          <select id="polarisasiIndikatorKinerjaForm" name="polarisasiIndikatorKinerjaForm"  class="form-control select2" style="width: 100%;">
                            <option value=""><i>- SELECT -</i></option>
                            <?php foreach($POLARISASI_INDIKATOR_KINERJA->result() as $row): ?>
                              <option value="<?php echo $row->id; ?>"><?php echo $row->name; ?></option>
                            <?php endforeach; ?>
                          </select>
                        </div><!-- /.form-group -->
                      </div>
                    </div><br/><br/>
                    <div class="form-group">
                      <label class="col-sm-5 control-label" style="text-align: right">Periode Pelaporan</label>
                      <div class="col-sm-7">
                        <div class="form-group">  
                          <select id="periodePelaporanForm" name="periodePelaporanForm"  class="form-control select2" style="width: 100%;">
                            <option value=""><i>- SELECT -</i></option>
                            <?php foreach($PERIODE_PELAPORAN->result() as $row): ?>
                              <option value="<?php echo $row->id; ?>"><?php echo $row->name; ?></option>
                            <?php endforeach; ?>
                          </select>
                        </div><!-- /.form-group -->
                      </div>
                    </div><br/><br/>
                    <div class="form-group">
                      <label class="col-sm-5 control-label" style="text-align: right">Konversi 120</label>
                      <div class="col-sm-7">
                        <div class="form-group">  
                          <select id="konversi120Form" name="konversi120Form"  class="form-control select2" style="width: 100%;">
                            <option value=""><i>- SELECT -</i></option>
                              <option value="Y">YA</option>
                              <option value="N">TIDAK</option>
                          </select>
                        </div><!-- /.form-group -->
                      </div>
                    </div><br/><br/>
                  </div>
              </div>
              <div id="null" style="text-align:center"> Silahkan Pilih Baris Terlebih Dahulu . . .</div>
              <div id="hapus" style="text-align:center"> Anda Yakin Ingin Menghapus Baris Ini ?</div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
              <input name="simpan" type="submit" id="simpan" class="btn btn-primary" value="Tambah">
            </div>
          </form>

        </div>
      </div>
    </div>

<script type="text/javascript">
  $(function() {

    $(".select2").select2();
    // Tandai ROW yang dipilih dalam iGRID
    $( "#iGrid tbody" ).selectable({
          filter: ":not(td)",
          create: function( e, ui ) {
              iGridLink( $() );
          },
          selected: function( e, ui ) {
              var widget = $(this).find('.ui-selected');
              $(ui.unselected).addClass("info");
              iGridLink( widget );
          },
          unselected: function( e, ui ) {
              $(ui.unselected).removeClass("info");
              var widget = $(this).find('.ui-selected');
              iGridLink( widget );
          }
    });

    // ONCLICK untuk Rekam / Ubah / Hapus
    $("#view").on("click", function(e) {
      if(document.getElementById("no").innerHTML != ''){
        $("#simpan").hide();
        document.getElementById("judul-form").innerHTML = 'Lihat Data IKU';
        $("#input").hide();
        $("#view_modal").show();
        $("#null").hide();
        $("#hapus").hide();
        $(".modal-body").css("height","500px");
      }else{
        e.preventDefault();
        $("#input").hide();
        $("#hapus").hide();
        $("#view_modal").hide();
        $("#simpan").hide();
        $("#null").show();
        $(".modal-body").css("height","auto");
      }
    });
/*
    $("#buat").on("click", function() {
      document.getElementById("simpan").value = 'Simpan';
      document.getElementById("judul-form").innerHTML = 'Buat Data IKU';
      $("#input").show();
      $("#view_modal").hide();
      $("#null").hide();
      $(".modal-body").css("height","500px");
      $("#action").val('Rekam');
      $("#simpan").show();
        $("#hapus").hide();
    });

    $("#editrow").on("click", function(e) {
     // document.getElementById("tglambil").value = "<?php //echo $this->fc->idtgl( date('Y-m-d H:i:s'), 'full' ) ?>"; 
      if(document.getElementById("no").innerHTML != ''){
        document.getElementById("simpan").value = 'Ubah';
        document.getElementById("judul-form").innerHTML = 'Ubah Data IKU';
        $("#rekam").hide();
        $("#simpan").show();
        $("#action").val('Ubah');
        $(".modal-body").css("height","500px");
        $("#view_modal").hide();
        $("#input").show();
        $("#null").hide();
        $("#hapus").hide();
      }else{
        e.preventDefault();
        $("#input").hide();
        $("#view_modal").hide();
        $("#simpan").hide();
        $("#hapus").hide();
        $("#null").show();
        $(".modal-body").css("height","auto");
      }
    });
*/
    $("#editrow").on("click", function(e) {
     // document.getElementById("tglambil").value = "<?php //echo $this->fc->idtgl( date('Y-m-d H:i:s'), 'full' ) ?>"; 

      if(document.getElementById("no").innerHTML != ''){
        var id = document.getElementById("no").innerHTML;
        var url = "<?php echo base_url(); ?>iku/form_edit/"+id;
        window.location.href = url;
      }else{
        e.preventDefault();
        $("#input").hide();
        $("#hapus").hide();
        $("#view_modal").hide();
        $("#simpan").hide();
        $("#null").show();
        $(".modal-body").css("height","auto");
      }
    });

    $("#delrow").on("click", function(e) {
      if(document.getElementById("no").innerHTML != ''){
        document.getElementById("simpan").value = 'Hapus';
        document.getElementById("judul-form").innerHTML = 'Hapus Paket';
        e.preventDefault();
        $("#input").hide();
        $("#action").val('Hapus');
        $("#view_modal").hide();
        $("#simpan").show();
        $("#hapus").show();
        $("#null").hide();
        $(".modal-body").css("height","auto");
      }else{
        e.preventDefault();
        $("#input").hide();
        $("#view_modal").hide();
        $("#simpan").hide();
        $("#hapus").hide();
        $("#null").show();
        $(".modal-body").css("height","auto");
      }
    });

    // Ambil Nilai ROW pada iGRID yang dipilih per-ID sesuai kolom CHILD
    function iGridLink( $selectees ) {
      selected = $.makeArray( $selectees.filter( ".ui-selected" ) );
      selected.reduce( function( a, b ) {
        document.getElementById("no").innerHTML   = $(b).children( "td:nth-child(1)" ).text();
        document.getElementById("namaData").innerHTML   = $(b).children( "td:nth-child(2)" ).text();
        document.getElementById("perspektifData").innerHTML = $(b).children( "td:nth-child(3)" ).text();
        document.getElementById("sasaranStrategisData").innerHTML   = $(b).children( "td:nth-child(4)" ).text();
        document.getElementById("IKUMonitoringData").innerHTML   = $(b).children( "td:nth-child(5)" ).text();
        document.getElementById("IKUAtasanData").innerHTML   = $(b).children( "td:nth-child(6)" ).text();
        document.getElementById("IKUData").innerHTML   = $(b).children( "td:nth-child(7)" ).text();
        document.getElementById("definisiData").innerHTML   = $(b).children( "td:nth-child(8)" ).text();
        document.getElementById("formulaData").innerHTML   = $(b).children( "td:nth-child(9)" ).text();
        document.getElementById("tujuanData").innerHTML   = $(b).children( "td:nth-child(10)" ).text();
        document.getElementById("satuanPengukuranData").innerHTML   = $(b).children( "td:nth-child(11)" ).text();
        document.getElementById("jenisAspekTargetData").innerHTML   = $(b).children( "td:nth-child(12)" ).text();
        document.getElementById("tingkatKendaliIKUData").innerHTML   = $(b).children( "td:nth-child(13)" ).text();
        document.getElementById("tingkatValiditasIKUData").innerHTML   = $(b).children( "td:nth-child(14)" ).text();
        document.getElementById("pihakPJIKUData").innerHTML   = $(b).children( "td:nth-child(15)" ).text();
        document.getElementById("pihakPenyediaJasaData").innerHTML   = $(b).children( "td:nth-child(16)" ).text();
        document.getElementById("sumberDataData").innerHTML   = $(b).children( "td:nth-child(17)" ).text();
        document.getElementById("jenisCascadingIKU").innerHTML   = $(b).children( "td:nth-child(18)" ).text();
        document.getElementById("metodeCascading").innerHTML   = $(b).children( "td:nth-child(19)" ).text();
        document.getElementById("jenisKonsolidasiPeriodeData").innerHTML  = $(b).children( "td:nth-child(20)" ).text();
        document.getElementById("jenisKonsolidasiLokasiData").innerHTML  = $(b).children( "td:nth-child(21)" ).text();
        document.getElementById("polarisasiIndikatorKinerjaData").innerHTML  = $(b).children( "td:nth-child(22)" ).text();
        document.getElementById("periodePelaporanData").innerHTML  = $(b).children( "td:nth-child(23)" ).text();
        document.getElementById("konversi120Data").innerHTML  = $(b).children( "td:nth-child(23)" ).text();
        document.getElementById("descSasaranStrategisData").innerHTML  = $(b).children( "td:nth-child(25)" ).text();
        document.getElementById("fileDocumentData").innerHTML  = $(b).children( "td:nth-child(26)" ).text();
        document.getElementById("fileDocumentData").href="<?php echo base_url(); ?>iku/download_file?file="+$(b).children( "td:nth-child(26)" ).text(); 
        document.getElementById("createdTimeStampData").innerHTML  = $(b).children( "td:nth-child(27)" ).text();
        document.getElementById("idPeriodeData").innerHTML  = $(b).children( "td:nth-child(28)" ).text();


       // document.getElementById("no").value   = $(b).children( "td:nth-child(1)" ).text();
        //document.getElementById("perspektifForm").value = $(b).children( "td:nth-child(2)" ).text();
        
        
        $("#idForm").val($(b).children( "td:nth-child(1)" ).text());

        $('#perspektifForm option').filter(function () { return $(this).html() == $(b).children( "td:nth-child(2)" ).text() }).attr('selected', 'selected'); 
        $("#select2-perspektifForm-container").attr('title', $(b).children( "td:nth-child(2)" ).text()); 
        $("#select2-perspektifForm-container").html($(b).children( "td:nth-child(2)" ).text()); 

        
        $('#sasaranStrategisForm option').filter(function () { return $(this).html() == $(b).children( "td:nth-child(3)" ).text() }).attr('selected', 'selected'); 
        $("#select2-sasaranStrategisForm-container").attr('title', $(b).children( "td:nth-child(3)" ).text()); 
        $("#select2-sasaranStrategisForm-container").html($(b).children( "td:nth-child(3)" ).text()); 

        $("#descSasaranStrategisForm").val($(b).children( "td:nth-child(22)" ).text()); 

        $('#IKUForm option').filter(function () { return $(this).html() == $(b).children( "td:nth-child(4)" ).text() }).attr('selected', 'selected'); 
        $("#select2-IKUForm-container").attr('title', $(b).children( "td:nth-child(4)" ).text()); 
        $("#select2-IKUForm-container").html($(b).children( "td:nth-child(4)" ).text()); 

        $("#definisiForm").val($(b).children( "td:nth-child(5)" ).text()); 
        $("#formulaForm").val($(b).children( "td:nth-child(6)" ).text()); 
        $("#tujuanForm").val($(b).children( "td:nth-child(7)" ).text()); 

        $('#satuanPengukuranForm option').filter(function () { return $(this).html() == $(b).children( "td:nth-child(8)" ).text() }).attr('selected', 'selected'); 
        $("#select2-satuanPengukuranForm-container").attr('title', $(b).children( "td:nth-child(8)" ).text()); 
        $("#select2-satuanPengukuranForm-container").html($(b).children( "td:nth-child(8)" ).text()); 

        $('#jenisAspekTargetForm option').filter(function () { return $(this).html() == $(b).children( "td:nth-child(9)" ).text() }).attr('selected', 'selected'); 
        $("#select2-jenisAspekTargetForm-container").attr('title', $(b).children( "td:nth-child(9)" ).text()); 
        $("#select2-jenisAspekTargetForm-container").html($(b).children( "td:nth-child(9)" ).text()); 

       $('#tingkatKendaliIKUForm option').filter(function () { return $(this).html() == $(b).children( "td:nth-child(10)" ).text() }).attr('selected', 'selected'); 
        $("#select2-tingkatKendaliIKUForm-container").attr('title', $(b).children( "td:nth-child(10)" ).text()); 
        $("#select2-tingkatKendaliIKUForm-container").html($(b).children( "td:nth-child(10)" ).text()); 

        $('#tingkatValiditasIKUForm option').filter(function () { return $(this).html() == $(b).children( "td:nth-child(11)" ).text() }).attr('selected', 'selected'); 
        $("#select2-tingkatValiditasIKUForm-container").attr('title', $(b).children( "td:nth-child(11)" ).text()); 
        $("#select2-tingkatValiditasIKUForm-container").html($(b).children( "td:nth-child(11)" ).text()); 

        $("#pihakPJIKUForm").val($(b).children( "td:nth-child(12)" ).text()); 
        $("#pihakPenyediaJasaForm").val($(b).children( "td:nth-child(13)" ).text()); 
        $("#sumberDataForm").val($(b).children( "td:nth-child(14)" ).text()); 

        $('#jenisCascadingIKUForm option').filter(function () { return $(this).html() == $(b).children( "td:nth-child(15)" ).text() }).attr('selected', 'selected'); 
        $("#select2-jenisCascadingIKUForm-container").attr('title', $(b).children( "td:nth-child(15)" ).text()); 
        $("#select2-jenisCascadingIKUForm-container").html($(b).children( "td:nth-child(15)" ).text()); 

        $('#metodeCascadingForm option').filter(function () { return $(this).html() == $(b).children( "td:nth-child(16)" ).text() }).attr('selected', 'selected'); 
        $("#select2-metodeCascadingForm-container").attr('title', $(b).children( "td:nth-child(16)" ).text()); 
        $("#select2-metodeCascadingForm-container").html($(b).children( "td:nth-child(16)" ).text()); 

        $('#jenisKonsolidasiPeriodeForm option').filter(function () { return $(this).html() == $(b).children( "td:nth-child(17)" ).text() }).attr('selected', 'selected'); 
        $("#select2-jenisKonsolidasiPeriodeForm-container").attr('title', $(b).children( "td:nth-child(17)" ).text()); 
        $("#select2-jenisKonsolidasiPeriodeForm-container").html($(b).children( "td:nth-child(17)" ).text()); 

        $('#jenisKonsolidasiLokasiForm option').filter(function () { return $(this).html() == $(b).children( "td:nth-child(18)" ).text() }).attr('selected', 'selected'); 
        $("#select2-jenisKonsolidasiLokasiForm-container").attr('title', $(b).children( "td:nth-child(18)" ).text()); 
        $("#select2-jenisKonsolidasiLokasiForm-container").html($(b).children( "td:nth-child(18)" ).text()); 

        $('#polarisasiIndikatorKinerjaForm option').filter(function () { return $(this).html() == $(b).children( "td:nth-child(19)" ).text() }).attr('selected', 'selected'); 
        $("#select2-polarisasiIndikatorKinerjaForm-container").attr('title', $(b).children( "td:nth-child(19)" ).text()); 
        $("#select2-polarisasiIndikatorKinerjaForm-container").html($(b).children( "td:nth-child(19)" ).text()); 

        $('#periodePelaporanForm option').filter(function () { return $(this).html() == $(b).children( "td:nth-child(20)" ).text() }).attr('selected', 'selected'); 
        $("#select2-periodePelaporanForm-container").attr('title', $(b).children( "td:nth-child(20)" ).text()); 
        $("#select2-periodePelaporanForm-container").html($(b).children( "td:nth-child(20)" ).text()); 

        $('#konversi120Form option').filter(function () { return $(this).html() == $(b).children( "td:nth-child(21)" ).text() }).attr('selected', 'selected'); 
        $("#select2-konversi120Form-container").attr('title', $(b).children( "td:nth-child(21)" ).text()); 
        $("#select2-konversi120Form-container").html($(b).children( "td:nth-child(21)" ).text()); 

        }, 0 
      );
     }

  }); 
</script>
    <div class="modal fade bs-example-modal-lg" id="periode">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <form action="<?php echo site_url('iku/crud') ?>" method="post" role="form">
            <div class="modal-header">
              <h4 id="judul-form" class="modal-title">Table Data Periode Pelaporan</h4>
            </div>
            <div class="modal-body">
              <div id="view_modal">
              <div id = "view_periode_table">
                  <table id="table_periode"class="table table-hover table-stripted table-bordered">
                    <thead id="head_table">
                    </thead>
                    <tbody id="body_table">
                    </tbody>
                   </table>
               </div>
                  <div id = "add_periode_form" style="display:none">
                    <div class="box-body">
                        <div class="form-group">
                          <label id="ket" class="col-sm-2 control-label" style="text-align: right">TAHUN</label>
                          <div class="col-sm-7">
                            <div class="form-group">  
                               <input type="hidden" id="tipe_periode_table" value="" class="form-control" />
                               <input id="value_periode_table" value=""  class="form-control" />
                            </div><!-- /.form-group -->
                          </div>
                          <div class="col-sm-3">
                               <button id="simpan_periode" class="simpan_table_periode btn btn-success">Simpan</button>
                          </div>
                        </div>
                     <hr>
                     </div>
                     <hr>
                  </div>
                  <span class="input-group-btn">
                      <span style="padding-right:10px;"><button id="add_tahun" type="button" class="btn btn-success"><i class="fa fa-plus"></i> Add Tahun</button>></span>
                      <span style="padding-right:10px;"> <button id="add_tipe" type="button" class="btn btn-info"><i class="fa fa-plus"></i> Add Tipe</button></span>
                      <span style="padding-right:10px;"><button id="add_periode" type="button" class="btn btn-warning"><i class="fa fa-plus"></i> Add Periode</button></span>
                  </span>
              </div>              
            </div>
            <div class="modal-footer">
              <button id="periode-btn-table" type="button" class="btn btn-success pull-left">Periode Button</button>
              <button id="close" type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
              <input name="simpan" type="submit" id="simpan" class="btn btn-primary" value="Tambah">
            </div>
          </form>

        </div>
      </div>
    </div>
<script>
    $("#periode-btn-table").on("click", function(e) { 
      $("#add_periode_form").hide();
      $("#view_periode_table").show();
      e.preventDefault();
      $.ajax({
        type: "POST",
        url: "iku/periode",
        data: { 
            id : $("#idPeriodeData").html()
            },
        cache: false,
        success: function(a){
           $('#head_table').empty();
           $('#body_table').empty();
          var datax = a.split("=========JOIN=========");
           $('#head_table').append(datax[0]);
           $('#body_table').append(datax[1]);
        } 
      });
    });


    $("#add_tahun").on("click", function(e) { 
      e.preventDefault();
      $("#tipe_periode_table").val('tahun');
      $("#view_periode_table").hide();
      $("#add_periode_form").show();
      $("#ket").text("Tahun");
  });
    $("#add_tipe").on("click", function(e) { 
      e.preventDefault();
      $("#tipe_periode_table").val('tipe');
      $("#view_periode_table").hide();
      $("#add_periode_form").show();
      $("#ket").text("Tipe");
  });
    $("#add_periode").on("click", function(e) { 
      e.preventDefault();
      $("#tipe_periode_table").val('periode');
      $("#view_periode_table").hide();
      $("#add_periode_form").show();
      $("#ket").text("Periode");
  });

 $("#simpan_periode").on("click", function(e) { 
      e.preventDefault();
      $.ajax({
        type: "POST",
        url: "iku/parameter_periode_add",
        data: { 
            id : $("#idPeriodeData").html(),
            type : $("#tipe_periode_table").val(),
            value : $("#value_periode_table").val()
            },
        cache: false,
        success: function(a){
          $("#add_tahun_form").show();
          $("#view_periode_table").hide();
        } 
      });
  });

  </script>