<script src="<?=base_url('assets/plugins/slimScroll/jquery.slimscroll.min.js'); ?>" type="text/javascript"></script>    
<script src="<?=base_url('assets/plugins/jQueryUI/jquery-ui.1.11.4.min.js'); ?>" type="text/javascript"></script>
<link rel="stylesheet" href="<?=base_url('assets/plugins/jQueryUI/jquery-ui.css'); ?>">

 <link rel="stylesheet" href="<?=base_url('assets/plugins/select2/select2.min.css'); ?>" type="text/css" />
<link href="<?=base_url('assets/dist/css/AdminLTE.min.css');?>" rel="stylesheet" type="text/css" />
 <script src="<?=base_url('assets/plugins/select2/select2.full.min.js');?>"></script>
<style>
/* Important part */
.modal-dialog{
    overflow-y: initial !important
}
.modal-body{
    overflow-y: auto;
}</style>   

    <div class="modal fade" id="myModal">
      <div class="modal-dialog">
        <div class="modal-content">
          <form action="<?php echo site_url('iku/crud_master') ?>" method="post" role="form">
            <div class="modal-header">
              <h4 id="judul-form" class="modal-title">Data Master IKU</h4>
            </div>
            <div class="modal-body">       

              <!-- jika klik rekam tampilkan field ini -->  
              <input id="action" name="action" value="" type="hidden" />
              <input id="type" name="type" value="" type="hidden" />
              <input id="idForm" name="idForm" value="" type="hidden" />
              <div id="input">
                  <div class="box-body">
                    <div class="form-group">
                      <label id="master-name" class="col-sm-3 control-label" style="text-align: right"></label>
                      <div class="col-sm-9">
                          <div class="form-group">  
                             <input id="nameForm" value="" name="nameForm" class="form-control" />
                          </div><!-- /.form-group -->
                      </div>
                    </div>
                  </div>
              </div>
              <div id="null" style="text-align:center"> Silahkan Pilih Baris Terlebih Dahulu . . .</div>
              <div id="hapus" style="text-align:center"> Anda Yakin Ingin Menghapus Baris Ini ?</div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
              <input name="simpan" type="submit" id="simpan" class="btn btn-primary" value="Tambah">
            </div>
          </form>

        </div>
      </div>
    </div>
	

<script type="text/javascript">
  $(function() {

    $(".select2").select2();
    // Tandai ROW yang dipilih dalam iGRID
    $( "#iGrid tbody" ).selectable({
          filter: ":not(td)",
          create: function( e, ui ) {
              iGridLink( $() );
          },
          selected: function( e, ui ) {
              var widget = $(this).find('.ui-selected');
              $(ui.unselected).addClass("info");
              iGridLink( widget );
          },
          unselected: function( e, ui ) {
              $(ui.unselected).removeClass("info");
              var widget = $(this).find('.ui-selected');
              iGridLink( widget );
          }
    });

    $(".edit").on("click", function() {
      document.getElementById("simpan").value = 'Simpan';
      document.getElementById("judul-form").innerHTML = 'Edit Data Master IKU';
      $("#input").show();
      $("#null").hide();
      $(".modal-body").css("height","100px");
      var explode = this.id.split(";");  
      $("#type").val(explode[1]);
      $("#master-name").text(explode[0]);
      $("#nameForm").val(explode[3]);
      $("#idForm").val(explode[2]);
      $("#action").val('Ubah');
      $("#simpan").show();
      $("#hapus").hide();
    });

    $(".add").on("click", function() {
      document.getElementById("simpan").value = 'Edit';
      document.getElementById("judul-form").innerHTML = 'Buat Data Master IKU';
      $("#input").show();
      $("#null").hide();
      $(".modal-body").css("height","100px");
      var explode = this.id.split(";");  
      $("#type").val(explode[1]);
      $("#master-name").text(explode[0]);
      $("#action").val('Rekam');
      $("#simpan").show();
      $("#hapus").hide();
    });

    $(".delete").on("click", function() {
      document.getElementById("simpan").value = 'Hapus';
      document.getElementById("judul-form").innerHTML = 'Hapus Data Master IKU';
      $("#input").hide();
      $("#null").hide();
      $(".modal-body").css("height","100px");
      var explode = this.id.split(";");  
      $("#type").val(explode[1]);
      $("#master-name").text(explode[0]);
      $("#nameForm").val(explode[3]);
      $("#idForm").val(explode[2]);
      $("#action").val('Hapus');
      $("#simpan").show();
      $("#hapus").show();
    });

    $("#editrow").on("click", function(e) {
     // document.getElementById("tglambil").value = "<?php echo $this->fc->idtgl( date('Y-m-d H:i:s'), 'full' ) ?>"; 
      if(document.getElementById("no").innerHTML != ''){
        document.getElementById("simpan").value = 'Ubah';
        document.getElementById("judul-form").innerHTML = 'Ubah Data IKU';
        $("#rekam").hide();
        $("#simpan").show();
        $("#action").val('Ubah');
        $(".modal-body").css("height","500px");
        $("#view_modal").hide();
        $("#input").show();
        $("#null").hide();
        $("#hapus").hide();
      }else{
        e.preventDefault();
        $("#input").hide();
        $("#view_modal").hide();
        $("#simpan").hide();
        $("#hapus").hide();
        $("#null").show();
        $(".modal-body").css("height","auto");
      }
    });

    $("#delrow").on("click", function(e) {
      if(document.getElementById("no").innerHTML != ''){
        document.getElementById("simpan").value = 'Hapus';
        document.getElementById("judul-form").innerHTML = 'Hapus Paket';
        e.preventDefault();
        $("#input").hide();
        $("#action").val('Hapus');
        $("#view_modal").hide();
        $("#simpan").show();
        $("#hapus").show();
        $("#null").hide();
        $(".modal-body").css("height","auto");
      }else{
        e.preventDefault();
        $("#input").hide();
        $("#view_modal").hide();
        $("#simpan").hide();
        $("#hapus").hide();
        $("#null").show();
        $(".modal-body").css("height","auto");
      }
    });

    // Ambil Nilai ROW pada iGRID yang dipilih per-ID sesuai kolom CHILD
    function iGridLink( $selectees ) {
      selected = $.makeArray( $selectees.filter( ".ui-selected" ) );
      selected.reduce( function( a, b ) {
        document.getElementById("no").innerHTML   = $(b).children( "td:nth-child(1)" ).text();
        document.getElementById("perspektifData").innerHTML = $(b).children( "td:nth-child(2)" ).text();
        document.getElementById("sasaranStrategisData").innerHTML   = $(b).children( "td:nth-child(3)" ).text();
        document.getElementById("IKUData").innerHTML   = $(b).children( "td:nth-child(4)" ).text();
        document.getElementById("definisiData").innerHTML   = $(b).children( "td:nth-child(5)" ).text();
        document.getElementById("formulaData").innerHTML   = $(b).children( "td:nth-child(6)" ).text();
        document.getElementById("tujuanData").innerHTML   = $(b).children( "td:nth-child(7)" ).text();
        document.getElementById("satuanPengukuranData").innerHTML   = $(b).children( "td:nth-child(8)" ).text();
        document.getElementById("jenisAspekTargetData").innerHTML   = $(b).children( "td:nth-child(9)" ).text();
        document.getElementById("tingkatKendaliIKUData").innerHTML   = $(b).children( "td:nth-child(10)" ).text();
        document.getElementById("tingkatValiditasIKUData").innerHTML   = $(b).children( "td:nth-child(11)" ).text();
        document.getElementById("pihakPJIKUData").innerHTML   = $(b).children( "td:nth-child(12)" ).text();
        document.getElementById("pihakPenyediaJasaData").innerHTML   = $(b).children( "td:nth-child(13)" ).text();
        document.getElementById("sumberDataData").innerHTML   = $(b).children( "td:nth-child(14)" ).text();
        document.getElementById("jenisCascadingIKU").innerHTML   = $(b).children( "td:nth-child(15)" ).text();
        document.getElementById("metodeCascading").innerHTML   = $(b).children( "td:nth-child(16)" ).text();
        document.getElementById("jenisKonsolidasiPeriodeData").innerHTML  = $(b).children( "td:nth-child(17)" ).text();
        document.getElementById("jenisKonsolidasiLokasiData").innerHTML  = $(b).children( "td:nth-child(18)" ).text();
        document.getElementById("polarisasiIndikatorKinerjaData").innerHTML  = $(b).children( "td:nth-child(19)" ).text();
        document.getElementById("periodePelaporanData").innerHTML  = $(b).children( "td:nth-child(20)" ).text();
        document.getElementById("konversi120Data").innerHTML  = $(b).children( "td:nth-child(21)" ).text();
        document.getElementById("descSasaranStrategisData").innerHTML  = $(b).children( "td:nth-child(22)" ).text();
        document.getElementById("createdTimeStampData").innerHTML  = $(b).children( "td:nth-child(23)" ).text();
        document.getElementById("idPeriodeData").innerHTML  = $(b).children( "td:nth-child(24)" ).text();


       // document.getElementById("no").value   = $(b).children( "td:nth-child(1)" ).text();
        //document.getElementById("perspektifForm").value = $(b).children( "td:nth-child(2)" ).text();
        
        
        $("#idForm").val($(b).children( "td:nth-child(1)" ).text());

        $('#perspektifForm option').filter(function () { return $(this).html() == $(b).children( "td:nth-child(2)" ).text() }).attr('selected', 'selected'); 
        $("#select2-perspektifForm-container").attr('title', $(b).children( "td:nth-child(2)" ).text()); 
        $("#select2-perspektifForm-container").html($(b).children( "td:nth-child(2)" ).text()); 

        
        $('#sasaranStrategisForm option').filter(function () { return $(this).html() == $(b).children( "td:nth-child(3)" ).text() }).attr('selected', 'selected'); 
        $("#select2-sasaranStrategisForm-container").attr('title', $(b).children( "td:nth-child(3)" ).text()); 
        $("#select2-sasaranStrategisForm-container").html($(b).children( "td:nth-child(3)" ).text()); 

        $("#descSasaranStrategisForm").val($(b).children( "td:nth-child(22)" ).text()); 

        $('#IKUForm option').filter(function () { return $(this).html() == $(b).children( "td:nth-child(4)" ).text() }).attr('selected', 'selected'); 
        $("#select2-IKUForm-container").attr('title', $(b).children( "td:nth-child(4)" ).text()); 
        $("#select2-IKUForm-container").html($(b).children( "td:nth-child(4)" ).text()); 

        $("#definisiForm").val($(b).children( "td:nth-child(5)" ).text()); 
        $("#formulaForm").val($(b).children( "td:nth-child(6)" ).text()); 
        $("#tujuanForm").val($(b).children( "td:nth-child(7)" ).text()); 

        $('#satuanPengukuranForm option').filter(function () { return $(this).html() == $(b).children( "td:nth-child(8)" ).text() }).attr('selected', 'selected'); 
        $("#select2-satuanPengukuranForm-container").attr('title', $(b).children( "td:nth-child(8)" ).text()); 
        $("#select2-satuanPengukuranForm-container").html($(b).children( "td:nth-child(8)" ).text()); 

        $('#jenisAspekTargetForm option').filter(function () { return $(this).html() == $(b).children( "td:nth-child(9)" ).text() }).attr('selected', 'selected'); 
        $("#select2-jenisAspekTargetForm-container").attr('title', $(b).children( "td:nth-child(9)" ).text()); 
        $("#select2-jenisAspekTargetForm-container").html($(b).children( "td:nth-child(9)" ).text()); 

       $('#tingkatKendaliIKUForm option').filter(function () { return $(this).html() == $(b).children( "td:nth-child(10)" ).text() }).attr('selected', 'selected'); 
        $("#select2-tingkatKendaliIKUForm-container").attr('title', $(b).children( "td:nth-child(10)" ).text()); 
        $("#select2-tingkatKendaliIKUForm-container").html($(b).children( "td:nth-child(10)" ).text()); 

        $('#tingkatValiditasIKUForm option').filter(function () { return $(this).html() == $(b).children( "td:nth-child(11)" ).text() }).attr('selected', 'selected'); 
        $("#select2-tingkatValiditasIKUForm-container").attr('title', $(b).children( "td:nth-child(11)" ).text()); 
        $("#select2-tingkatValiditasIKUForm-container").html($(b).children( "td:nth-child(11)" ).text()); 

        $("#pihakPJIKUForm").val($(b).children( "td:nth-child(12)" ).text()); 
        $("#pihakPenyediaJasaForm").val($(b).children( "td:nth-child(13)" ).text()); 
        $("#sumberDataForm").val($(b).children( "td:nth-child(14)" ).text()); 

        $('#jenisCascadingIKUForm option').filter(function () { return $(this).html() == $(b).children( "td:nth-child(15)" ).text() }).attr('selected', 'selected'); 
        $("#select2-jenisCascadingIKUForm-container").attr('title', $(b).children( "td:nth-child(15)" ).text()); 
        $("#select2-jenisCascadingIKUForm-container").html($(b).children( "td:nth-child(15)" ).text()); 

        $('#metodeCascadingForm option').filter(function () { return $(this).html() == $(b).children( "td:nth-child(16)" ).text() }).attr('selected', 'selected'); 
        $("#select2-metodeCascadingForm-container").attr('title', $(b).children( "td:nth-child(16)" ).text()); 
        $("#select2-metodeCascadingForm-container").html($(b).children( "td:nth-child(16)" ).text()); 

        $('#jenisKonsolidasiPeriodeForm option').filter(function () { return $(this).html() == $(b).children( "td:nth-child(17)" ).text() }).attr('selected', 'selected'); 
        $("#select2-jenisKonsolidasiPeriodeForm-container").attr('title', $(b).children( "td:nth-child(17)" ).text()); 
        $("#select2-jenisKonsolidasiPeriodeForm-container").html($(b).children( "td:nth-child(17)" ).text()); 

        $('#jenisKonsolidasiLokasiForm option').filter(function () { return $(this).html() == $(b).children( "td:nth-child(18)" ).text() }).attr('selected', 'selected'); 
        $("#select2-jenisKonsolidasiLokasiForm-container").attr('title', $(b).children( "td:nth-child(18)" ).text()); 
        $("#select2-jenisKonsolidasiLokasiForm-container").html($(b).children( "td:nth-child(18)" ).text()); 

        $('#polarisasiIndikatorKinerjaForm option').filter(function () { return $(this).html() == $(b).children( "td:nth-child(19)" ).text() }).attr('selected', 'selected'); 
        $("#select2-polarisasiIndikatorKinerjaForm-container").attr('title', $(b).children( "td:nth-child(19)" ).text()); 
        $("#select2-polarisasiIndikatorKinerjaForm-container").html($(b).children( "td:nth-child(19)" ).text()); 

        $('#periodePelaporanForm option').filter(function () { return $(this).html() == $(b).children( "td:nth-child(20)" ).text() }).attr('selected', 'selected'); 
        $("#select2-periodePelaporanForm-container").attr('title', $(b).children( "td:nth-child(20)" ).text()); 
        $("#select2-periodePelaporanForm-container").html($(b).children( "td:nth-child(20)" ).text()); 

        $('#konversi120Form option').filter(function () { return $(this).html() == $(b).children( "td:nth-child(21)" ).text() }).attr('selected', 'selected'); 
        $("#select2-konversi120Form-container").attr('title', $(b).children( "td:nth-child(21)" ).text()); 
        $("#select2-konversi120Form-container").html($(b).children( "td:nth-child(21)" ).text()); 

        }, 0 
      );
     }

  }); 
</script>
    <div class="modal fade bs-example-modal-lg" id="periode">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <form action="<?php echo site_url('iku/crud') ?>" method="post" role="form">
            <div class="modal-header">
              <h4 id="judul-form" class="modal-title">Table Data Periode Pelaporan</h4>
            </div>
            <div class="modal-body">
              <div id="view_modal">
              <div id = "view_periode_table">
                  <table id="table_periode"class="table table-hover table-stripted table-bordered">
                    <thead id="head_table">
                    </thead>
                    <tbody id="body_table">
                    </tbody>
                   </table>
               </div>
                  <div id = "add_periode_form" style="display:none">
                    <div class="box-body">
                        <div class="form-group">
                          <label id="ket" class="col-sm-2 control-label" style="text-align: right">TAHUN</label>
                          <div class="col-sm-7">
                            <div class="form-group">  
                               <input type="hidden" id="tipe_periode_table" value="" class="form-control" />
                               <input id="value_periode_table" value=""  class="form-control" />
                            </div><!-- /.form-group -->
                          </div>
                          <div class="col-sm-3">
                               <button id="simpan_periode" class="simpan_table_periode btn btn-success">Simpan</button>
                          </div>
                        </div>
                     <hr>
                     </div>
                     <hr>
                  </div>
                  <span class="input-group-btn">
                      <span style="padding-right:10px;"><button id="add_tahun" type="button" class="btn btn-success"><i class="fa fa-plus"></i> Add Tahun</button>></span>
                      <span style="padding-right:10px;"> <button id="add_tipe" type="button" class="btn btn-info"><i class="fa fa-plus"></i> Add Tipe</button></span>
                      <span style="padding-right:10px;"><button id="add_periode" type="button" class="btn btn-warning"><i class="fa fa-plus"></i> Add Periode</button></span>
                  </span>
              </div>              
            </div>
            <div class="modal-footer">
              <button id="periode-btn-table" type="button" class="btn btn-success pull-left">Periode Button</button>
              <button id="close" type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
              <input name="simpan" type="submit" id="simpan" class="btn btn-primary" value="Tambah">
            </div>
          </form>

        </div>
      </div>
    </div>
<script>
    $("#periode-btn-table").on("click", function(e) { 
      $("#add_periode_form").hide();
      $("#view_periode_table").show();
      e.preventDefault();
      $.ajax({
        type: "POST",
        url: "iku/periode",
        data: { 
            id : $("#idPeriodeData").html()
            },
        cache: false,
        success: function(a){
           $('#head_table').empty();
           $('#body_table').empty();
          var datax = a.split("=========JOIN=========");
           $('#head_table').append(datax[0]);
           $('#body_table').append(datax[1]);
        } 
      });
    });


    $("#add_tahun").on("click", function(e) { 
      e.preventDefault();
      $("#tipe_periode_table").val('tahun');
      $("#view_periode_table").hide();
      $("#add_periode_form").show();
      $("#ket").text("Tahun");
  });
    $("#add_tipe").on("click", function(e) { 
      e.preventDefault();
      $("#tipe_periode_table").val('tipe');
      $("#view_periode_table").hide();
      $("#add_periode_form").show();
      $("#ket").text("Tipe");
  });
    $("#add_periode").on("click", function(e) { 
      e.preventDefault();
      $("#tipe_periode_table").val('periode');
      $("#view_periode_table").hide();
      $("#add_periode_form").show();
      $("#ket").text("Periode");
  });

 $("#simpan_periode").on("click", function(e) { 
      e.preventDefault();
      $.ajax({
        type: "POST",
        url: "iku/parameter_periode_add",
        data: { 
            id : $("#idPeriodeData").html(),
            type : $("#tipe_periode_table").val(),
            value : $("#value_periode_table").val()
            },
        cache: false,
        success: function(a){
          $("#add_tahun_form").show();
          $("#view_periode_table").hide();
        } 
      });
  });

  </script>