
<style media="screen">
	th {
    text-align: center;
  	}
  	thead tr {
    	color: #fff;
    	background: #00acd6;
  	}

  	#iGrid .ui-selecting { background: #FECA40; }
  	#iGrid .ui-selected { background: #F39814; color: white; }

  	.ui-autocomplete { height: 200px; overflow-y: scroll; overflow-x: hidden;}
</style>

<div class="row">
	<div class="col-md-12">
		<div class="box box-widget">
			<div class="box-header with border">
				<div class="wel wel-sm">
					<form id="iForm" class="form-inline" role="form" action="<?php echo site_url("paket/cari") ?>" method="post">
						<div class="form-group pull-right">
							<div class="input-group">
                    			<input type="hidden" name="nmfunction" value="grid">
                				<input class="form-control" name="cari" type="text" value="<?php echo $this->session->userdata('cari') ?>">
            					<span class="input-group-btn">
            						<button type="submit" name="search" value="search" class="btn btn-info btn-flat"><i class="fa fa-search"></i></button>
            					</span>
            					<span class="input-group-btn">
            						<button type="submit" name="search" value="clear" class="btn btn-info btn-flat"><i class="fa fa-times-circle"></i></button>
                				</span>
                			</div>
                		</div>
                		<div class="form-group">
                			<div class="input-group">
	                			<span class="input-group-btn">
									<span style="padding-right:10px;"><button type="button" id="view"  class="btn btn-info" data-toggle="modal" data-target="#myModal"><i class="fa fa-eye"> Lihat</i></button></span>
									<span style="padding-right:10px;"><a type="button" id="buat" class="btn btn-success" <?php /*data-toggle="modal" data-target="#myModal"*/ ?> href="<?php echo base_url(); ?>iku/form_add"><i class="fa fa-plus"> Buat</i></a></span>
					  				<span style="padding-right:10px;"><a type="button" id="editrow" class="btn btn-warning"  data-toggle="modal" data-target="#myModal" href="#"><i class="fa fa-edit"> Ubah</i></a></span>
					  				<span style="padding-right:10px;"><button type="button" id="delrow"  class="btn btn-danger" data-toggle="modal" data-target="#myModal"><i class="fa fa-trash"> Hapus</i></button></span>
	                			</span>
                			</div>
                		</div>
                	</form>
                </div>
            </div>   <!-- box header  -->


			<div class="box-body">
				<h2><?php echo $judul ?></h2>
				<table id="iGrid" class="table table-hover table-bordered">
					<thead>
					<tr>
						<th width="4%">ID</th>
						<th>Name</th>
						<th>Perspektif</th>
						<th>Sasaran Strategis</th>
						<th>IKU Monitoring</th>
						<th>IKU</th>
						<th style="display:none">Satuan Pengukuran</th>
						<th style="display:none">Jenis Aspek Target</th>
						<th style="display:none">Tingkat Kendali IKU</th>
						<th style="display:none">Tingkat Validitas IKU</th>
						<th style="display:none">Jenis Cascading IKU</th>
						<th style="display:none">Metode Cascading</th>
						<th style="display:none">jenis Konsolidasi Periode</th>
						<th style="display:none">Jenis Konsolidasi Lokasi</th>
						<th style="display:none">Polarisasi Indikator Kinerja</th>
						<th style="display:none">Periode Pelaporan</th>
						<th style="display:none">File Document</th>
						<th>Created Time</th>
					</tr>
					</thead>
				<tbody>
					<?php
					if ($nilai) {
						foreach ($nilai as $row) { ?>
							<tr>
								<td id="row_1"><?php echo $row['id'] ?></td>
								<td id="row_2"><?php echo $row['fullname'] ?></td>
								<td id="row_3"><?php echo $row['perspektif'] ?></td>
								<td id="row_4"><?php echo $row['sasaran_strategis'] ?></td>
								<td id="row_5"><?php echo $row['iku_monitoring'] ?></td>
								<td id="row_6" style="display:none"><?php echo $row['iku_atasan'] ?></td>
								<td id="row_7"><?php echo $row['iku'] ?></td>
								<td id="row_8" style="display:none"><?php echo $row['definisi'] ?></td>
								<td id="row_9" style="display:none"><?php echo $row['formula'] ?></td>
								<td id="row_10" style="display:none"><?php echo $row['tujuan'] ?></td>
								<td id="row_11" style="display:none"><?php echo $row['satuan_pengukuran'] ?></td>
								<td id="row_12" style="display:none"><?php echo $row['jenis_aspek_target'] ?></td>
								<td id="row_13" style="display:none"><?php echo $row['tingkat_kendali_iku'] ?></td>
								<td id="row_14" style="display:none"><?php echo $row['tingkat_validitas_iku'] ?></td>
								<td id="row_15" style="display:none"><?php echo $row['pihakPJIku'] ?></td>
								<td id="row_16" style="display:none"><?php echo $row['pihakPenyediaJasa'] ?></td>
								<td id="row_17" style="display:none"><?php echo $row['sumberData'] ?></td>
								<td id="row_18" style="display:none"><?php echo $row['jenis_cascading_iku'] ?></td>
								<td id="row_19" style="display:none"><?php echo $row['metode_cascading'] ?></td>
								<td id="row_20" style="display:none"><?php echo $row['jenis_konsolidasi_periode'] ?></td>
								<td id="row_21" style="display:none"><?php echo $row['jenis_konsolidasi_lokasi'] ?></td>
								<td id="row_22" style="display:none"><?php echo $row['polarisasi_indikator_kinerja'] ?></td>
								<td id="row_23" style="display:none"><?php echo $row['periode_pelaporan'] ?></td>
								<td id="row_24" style="display:none"><?php if($row['konversi120'] == 'Y'){ echo 'YA';}elseif($row['konversi120'] == 'N'){echo 'TIDAK';} ?></td>
								<td id="row_25" style="display:none"><?php echo $row['descSasaranStrategis'] ?></td>
								<td id="row_26" style="display:none"><?php echo $row['file_document'] ?></td>
								<td id="row_27" ><?php echo $row['createdTimeStamp'] ?></td>
								<td id="row_28" style="display:none" ><?php echo $row['idPeriode'] ?></td>
							</tr>
						<?php
						}
					} else { ?>
							<tr><td colspan="5" class="text-danger"> Data tersebut tidak ditemukan ...</td></tr>
					<?php } ?>
				</tbody>
				</table>
			</div> <!-- box body -->

			<div class="box-footer clearfix">
        		<div class="form-group">
        			<div class="input-group">
            			<span class="input-group-btn">
			  				<a href="<?php echo base_url(); ?>iku" class="btn btn-primary"><i class="fa fa-reply"> IKU Manual</i></a>
            			</span>&nbsp;
            			<span class="input-group-btn">
			  				<span style="padding-right:10px;">
			  					<a href="<?php echo base_url(); ?>iku/master" class="btn btn-primary"><i class="fa fa-mail-forward"> Master IKU</i></a>
			  				</span>
            			</span>
						<?php echo $this->pagination->create_links(); ?>
        			</div>
        		</div>
			</div>
		</div>
		</div>
</div>

<script type="text/javascript">
	$('.pagination').addClass('pagination-sm no-margin pull-right');
</script>

<?php $this->load->view('iku/V_paket_rekam') ?>
