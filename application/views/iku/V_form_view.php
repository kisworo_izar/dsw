
<style media="screen">
	th {
    text-align: center;
  	}
  	thead tr {
    	color: #fff;
    	background: #00acd6;
  	}

  	#iGrid .ui-selecting { background: #FECA40; }
  	#iGrid .ui-selected { background: #F39814; color: white; }

  	.ui-autocomplete { height: 200px; overflow-y: scroll; overflow-x: hidden;}
</style>

<div class="row">
	<div class="col-md-12">
		<div class="box box-primary">
			<form  method="POST" action="<?php echo base_url(); ?>iku/crud" enctype="multipart/form-data">
				<div class="box-header with-border">
					<i class="fa fa-edit"></i>
					<h3 class="box-title"><?php echo $judul ?></h3>
				</div> <!-- box body -->

				<div class="box-body clearfix">
		    		<div class="form-group">
		    			<?php echo $table; ?>
		    		</div>
				</div>
				<div class="box-header with border">
					<div class="wel wel-sm">
		        		<div class="form-group">
		        			<div class="input-group col-sm-12">
									<span style="padding-right:10px;"><a type="button" id="back"  class="btn btn-warning" <?php /*data-toggle="modal" data-target="#myModal"*/ ?> href="<?php echo base_url(); ?>iku"><i class="fa  fa-mail-reply"> Back</i></a></span>
					  				<?php /*<span style="padding-right:10px;float:right;">
					  					<button type="reset"  id="resetselect" class="btn btn-danger" value="Reset" <?php /* data-toggle="modal" data-target="#myModal"*//*?>>
					  						<i class="fa  fa-times"> Reset</i>
					  					</button>*/ ?>
					  				</span>	
									<span style="padding-right:10px;float:right;"><a href="<?php echo base_url(); ?>iku/form_edit/<?php echo $this->uri->segment(3); ?>" id="edit" class="btn btn-warning"><i class="fa fa-pencil"> Edit</i></a></span>
									
		        			</div>
		        		</div>
		            </div>
		        </div>   <!-- box header  -->
			</form>
		</div>
	</div>
</div>


<?php //$this->load->view('iku/V_paket_rekam') ?>
<script src="<?=base_url('assets/plugins/slimScroll/jquery.slimscroll.min.js'); ?>" type="text/javascript"></script>    
<script src="<?=base_url('assets/plugins/jQueryUI/jquery-ui.1.11.4.min.js'); ?>" type="text/javascript"></script>
<link rel="stylesheet" href="<?=base_url('assets/plugins/jQueryUI/jquery-ui.css'); ?>">

 <link rel="stylesheet" href="<?=base_url('assets/plugins/select2/select2.min.css'); ?>" type="text/css" />
<link href="<?=base_url('assets/dist/css/AdminLTE.min.css');?>" rel="stylesheet" type="text/css" />
 <script src="<?=base_url('assets/plugins/select2/select2.full.min.js');?>"></script>
<style>

/* Important part */
.modal-dialog{
    overflow-y: initial !important
}
.modal-body{
    overflow-y: auto;
}</style>   


<script type="text/javascript">

  $(function() {

    $( "#definisiForm" ).autocomplete({
      source: function(req, add){
          $.ajax({
            url: "<?php echo base_url();?>iku/lookup",
            dataType: 'json',
            type: 'POST',
			data: { 
					term : req,
					type: 'definisi'
				  },
            success:
            function(data){
              if(data.response =='true'){
                add(data.message);
              }
            }
          });
        },
    });


    $( "#formulaForm" ).autocomplete({
      source: function(req, add){
          $.ajax({
            url: "<?php echo base_url();?>iku/lookup",
            dataType: 'json',
            type: 'POST',
			data: { 
					term : req,
					type: 'formula'
				  },
            success:
            function(data){
              if(data.response =='true'){
                add(data.message);
              }
            }
          });
        },
    });

    $( "#tujuanForm" ).autocomplete({
      source: function(req, add){
          $.ajax({
            url: "<?php echo base_url();?>iku/lookup",
            dataType: 'json',
            type: 'POST',
			data: { 
					term : req,
					type: 'tujuan'
				  },
            success:
            function(data){
              if(data.response =='true'){
                add(data.message);
              }
            }
          });
        },
    });
    $("#resetselect").on("click", function(e) { 
		 $("select").each(function() { this.selectedIndex = 0 });
    	alert('asd');
  });

    $(".select2").select2();
    // Tandai ROW yang dipilih dalam iGRID
    $( "#iGrid tbody" ).selectable({
          filter: ":not(td)",
          create: function( e, ui ) {
              iGridLink( $() );
          },
          selected: function( e, ui ) {
              var widget = $(this).find('.ui-selected');
              $(ui.unselected).addClass("info");
              iGridLink( widget );
          },
          unselected: function( e, ui ) {
              $(ui.unselected).removeClass("info");
              var widget = $(this).find('.ui-selected');
              iGridLink( widget );
          }
    });

    // Ambil Nilai ROW pada iGRID yang dipilih per-ID sesuai kolom CHILD
    function iGridLink( $selectees ) {
      selected = $.makeArray( $selectees.filter( ".ui-selected" ) );
     }

  }); 
</script>


<script type="text/javascript">
    $(document).ready(function(){
        $('.select-periode').change(function(){
            $.post("<?php echo base_url();?>iku/getPeriode?id="+$('#periodePelaporanForm').val(),{},function(obj){
                $('#periode_form').html(obj);
				$("#total-column").val(0);

            });
        });
    });
</script>
