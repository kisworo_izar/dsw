<script src="<?=base_url('assets/plugins/slimScroll/jquery.slimscroll.min.js'); ?>" type="text/javascript"></script>    
<script src="<?=base_url('assets/plugins/jQueryUI/jquery-ui.1.11.4.min.js'); ?>" type="text/javascript"></script>
<link rel="stylesheet" href="<?=base_url('assets/plugins/jQueryUI/jquery-ui.css'); ?>">

 <link rel="stylesheet" href="<?=base_url('assets/plugins/select2/select2.min.css'); ?>" type="text/css" />
<link href="<?=base_url('assets/dist/css/AdminLTE.min.css');?>" rel="stylesheet" type="text/css" />
 <script src="<?=base_url('assets/plugins/select2/select2.full.min.js');?>"></script>
<style>
/* Important part */
.modal-dialog{
    overflow-y: initial !important
}
.modal-body{
    overflow-y: auto;
}</style>   

    <div class="modal fade" id="myModal">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <form action="<?php echo site_url('iku/crud') ?>" method="post" role="form">
            <div class="modal-header">
              <h4 id="judul-form" class="modal-title">Data Paket</h4>
            </div>
            <div class="modal-body">
              <input id="idClick" name="idClick" value="" type="hidden" readonly />
              <input id="tahunPeriode" name="tahunPeriode" value="<?php echo $existingYear;?>" type="hidden" readonly />
              <div id="view_modal"></div>              
              <div id="notyet" style="text-align:center"> Silahkan Pilih Baris Dan IKU Terlebih Dahulu . . .</div>
              <div id="null" style="text-align:center"> Silahkan Pilih IKU Dari Baris yang Dipilih . . .</div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
              <input name="simpan" type="submit" id="simpan" class="btn btn-primary" value="Simpan">
            </div>
          </form>

        </div>
      </div>
    </div>

    <div class="modal fade" id="myPrint">
      <div class="modal-dialog">
        <div class="modal-content">
          <form action="<?php echo site_url('iku/download') ?>" method="post" role="form">
            <div class="modal-header">
              <h4 id="judul-form-print" class="modal-title">Print IKU</h4>
            </div>
            <div class="modal-body">
              <input id="idClick-print" name="idClick" value="" type="hidden" readonly />
              <input id="tahunPeriode-print" name="tahunPeriode" value="<?php echo $existingYear;?>" type="hidden" readonly />
              <div id="view_modal-print"></div>              
              <div id="notyet-print" style="text-align:center"> Silahkan Pilih Baris Terlebih Dahulu . . .</div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-success pull-right">Download</button>
              <input name="simpan" type="submit" id="simpan-print" class="btn btn-primary" value="Simpan">
            </div>
          </form>

        </div>
      </div>
    </div>

<script type="text/javascript">
  $(function() {

    $(".select2").select2();
    // Tandai ROW yang dipilih dalam iGRID
    $( "#iGrid tbody" ).selectable({
          filter: ":not(td)",
          create: function( e, ui ) {
              iGridLink( $() );
          },
          selected: function( e, ui ) {
              var widget = $(this).find('.ui-selected');
              $(ui.unselected).addClass("info");
              iGridLink( widget );
          },
          unselected: function( e, ui ) {
              $(ui.unselected).removeClass("info");
              var widget = $(this).find('.ui-selected');
              iGridLink( widget );
          }
    });

    // ONCLICK untuk Rekam / Ubah / Hapus
    $("#view").on("click", function(e) {
		if($("#idClick").val() != ''){
			  if($("#select-iku-"+$("#idClick").val()).val() != '' && $("#select-iku-"+$("#idClick").val()).val() != '- IKU -'){
			  var iku = $("#select-iku-"+$("#idClick").val()).val();
				e.preventDefault();
				$.post('<?php echo base_url(); ?>iku/body_modal_view',
					{
					  iku:iku
					  },
					function(html){
						$("#view_modal").html(html);
						$("#judul-form").html('Lihat Data Manual IKU Tahun : '+$("#tahunPeriode").val());
						$("#simpan").hide();
						$("#view_modal").show();
						$("#null").hide();
						$("#notyet").hide();
						$(".modal-body").css("height","100%");
					}   
				);
			  }else{
				e.preventDefault();
				$("#hapus").hide();
				$("#view_modal").hide();
				$("#simpan").hide();
				$("#null").show();
				$("#notyet").hide();
				$(".modal-body").css("height","auto");
			}
		}else{
				e.preventDefault();
				$("#hapus").hide();
				$("#view_modal").hide();
				$("#simpan").hide();
				$("#null").hide();
				$("#notyet").show();
				$(".modal-body").css("height","auto");		
		}
		
	});
	
   /* $("#print").on("click", function(e) {
		if($("#idClick").val() != ''){
			e.preventDefault();
			$.post('<?php echo base_url(); ?>iku/print_iku_select',
				{
				  id:$("#idClick").val()
				  },
				function(html){
					$("#view_modal-print").html(html);
					$("#judul-form-print").html('Print IKU');
					$("#simpan-print").hide();
					$("#view_modal-print").show();
					$("#null-print").hide();
					$("#notyet-print").hide();
					$(".modal-body").css("height","100%");
				}   
			);
		}else{
				e.preventDefault();
				$("#hapus-print").hide();
				$("#view_modal-print").hide();
				$("#simpan-print").hide();
				$("#null-print").hide();
				$("#notyet-print").show();
				$(".modal-body").css("height","auto");		
		}
		
	});*/

    $('#select-tahun').on('change', function() {

      var str = window.location.pathname.split('/')[1];
      var url = window.location.protocol + "//" + window.location.host + "/" + str.replace("/", "")+'/iku?tahun='+this.value; 
     document.location.href = url ;
    });

    $("#entry").on("click", function(e) {
      if($("#idForm").val() != ''){
        e.preventDefault();
        $.post('iku/body_modal_entry',
            {
              id:$("#idForm").val(),
              tahun:$("#tahunPeriode").val()
              },
            function(html){
                $("#view_modal").html(html);
                $("#judul-form").html('Lihat Data Manual IKU Tahun : '+$("#tahunPeriode").val());
                $("#simpan").show();
                $("#view_modal").show();
                $("#null").hide();
                $(".modal-body").css("height","100%");
            }   
        );
      }else{
        e.preventDefault();
        $("#view_modal").hide();
        $("#simpan").hide();
        $("#null").show();
        $(".modal-body").css("height","auto");
      }
    });

    // Ambil Nilai ROW pada iGRID yang dipilih per-ID sesuai kolom CHILD
    function iGridLink( $selectees ) {
      selected = $.makeArray( $selectees.filter( ".ui-selected" ) );
      selected.reduce( function( a, b ) {
        $("#idClick").val($(b).children( "td:nth-child(1)" ).text());
		
        }, 0 
      );
     }

  }); 

 $("#simpan").on("click", function(e) { 
      e.preventDefault();
      var datas = {};
      for(var i=1;i<=$("#total_rows").val();i++){
       datas[i] = {};
       if($("#periode-"+(i)+"-target").val() == ''){ var valtarget = 0; }else{ var valtarget = $("#periode-"+(i)+"-target").val(); }
        datas[i]['TARGET'] = valtarget;
       if($("#periode-"+(i)+"-realisasi").val() == ''){ var valrealisasi = 0; }else{ var valrealisasi = $("#periode-"+(i)+"-realisasi").val(); }
        datas[i]['REALISASI'] = valrealisasi;
      }        

      $.ajax({
        type: "POST",
        url: "iku/simpan_periode_pelaporan",
        data: { 
            idIku : $("#idForm").val(),
            periodePelaporanId : $("#periodePelaporanId").val(),
            tahun : $("#tahunPeriode").val(),
            datas : JSON.stringify(datas)
            },
        cache: false,
        success: function(a){
          if(a == 1){
            var notif = '<div id="notif-show" class="alert alert-info alert-dismissable"><button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button><h4><i class="icon fa fa-info"></i>Data Berhasil Disimpan!</h4></div>';
               $('#myModal').modal('hide');
              $("#notif").html(notif);
          }else{
             var notif = '<div id="notif-show" class="alert alert-danger alert-dismissable"><button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button><h4><i class="icon fa fa-info"></i>Data Gagal Disimpan!</h4></div>';
               $('#myModal').modal('hide');
              $("#notif").html(notif);           
          }
        } 
      });
  });

  </script>