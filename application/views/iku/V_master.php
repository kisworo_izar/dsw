
<style media="screen">
	th {
    text-align: center;
  	}
  	thead tr {
    	color: #fff;
    	background: #00acd6;
  	}

  	#iGrid .ui-selecting { background: #FECA40; }
  	#iGrid .ui-selected { background: #F39814; color: white; }

  	.ui-autocomplete { height: 200px; overflow-y: scroll; overflow-x: hidden;}
</style>



<div class="row">
	<div class="col-md-6">
		<div class="box box-solid" >
                <div class="box-header with-border">
                  <h3 class="box-title"></h3>
                </div><!-- /.box-header -->
                <div class="box-body">
					<div class="box-group" id="accordion">

                    <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->

					<div class="panel box box-danger">
					 <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
					  <div class="box-header with-border">
					    <h4 class="box-title">
					        <span style="color:#3c8dbc">
					            Master Perspektif
					        </span>
					    </h4>
					  </div>
					 </a>
                      <div id="collapseOne" class="panel-collapse collapse">
                        <div class="box-body">
						<table class="table table-hover table-striped table-bordered">
							<tr>
		                      <th style="width: 10px">No</th>
		                      <th>Name</th>
		                      <th width="33%">Action</th>
		                    </tr>
		                    <?php $x = 1; ?>
		                    <?php foreach($PERSPEKTIF->result() as $row): ?>
		                    <tr>
		                      <td><?php echo $x; ?></td>
		                      <td><?php echo $row->name; ?></td>
		                      <td><button id="PERSPEKTIF;1;<?= $row->id; ?>;<?= $row->name; ?>" type="button" class="edit btn btn-xs btn-warning" data-toggle="modal" data-target="#myModal"> Ubah</i></button>
		                      	<button id="PERSPEKTIF;1;<?= $row->id; ?>;<?= $row->name; ?>" type="button" class="delete btn btn-xs btn-danger" data-toggle="modal" data-target="#myModal"> <i class="fa fa-trash"> Hapus</i></button></td>
		                    </tr>
		                    <?php $x = $x + 1; ?>
		                <?php endforeach; ?>
						</table>
                        </div>
                        <div class="box-footer">
						<button type="button" id="PERSPEKTIF;1" class="add btn btn-sm btn-success" data-toggle="modal" data-target="#myModal"><i class="fa fa-plus"> Tambah</i></button></div>
                      </div>
					</div>



					<div class="panel box box-danger">
					 <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
					  <div class="box-header with-border">
					    <h4 class="box-title">
					        <span style="color:#3c8dbc">
					             Master Sasaran Strategis
					        </span>
					    </h4>
					  </div>
					 </a>
                      <div id="collapseTwo" class="panel-collapse collapse">
                        <div class="box-body">
						<table class="table table-hover table-striped table-bordered">
							<tr>
		                      <th style="width: 10px">No</th>
		                      <th>Name</th>
		                      <th width="32%">Action</th>
		                    </tr>
		                    <?php $x = 1; ?>
		                    <?php foreach($SASARAN_STRATEGIS->result() as $row): ?>
		                    <tr>
		                      <td><?php echo $x; ?></td>
		                      <td><?php echo $row->name; ?></td>
		                      <td><button id="SASARAN STRATEGIS;2;<?= $row->id; ?>;<?= $row->name; ?>" type="button" class="edit btn btn-xs btn-warning" data-toggle="modal" data-target="#myModal"> Ubah</i></button>
		                      	<button id="SASARAN STRATEGIS;2;<?= $row->id; ?>;<?= $row->name; ?>" type="button" class="delete btn btn-xs btn-danger" data-toggle="modal" data-target="#myModal"> <i class="fa fa-trash"> Hapus</i></button>
								</td>
		                    </tr>
		                    <?php $x = $x + 1; ?>
		                <?php endforeach; ?>
						</table>
                        </div>
                        <div class="box-footer">
						<button type="button" id="SASARAN STRATEGIS;2" class="add btn btn-sm btn-success" data-toggle="modal" data-target="#myModal"><i class="fa fa-plus"> Tambah</i></button></div>
                      </div>
					</div>

					<?php /*
					<div class="panel box box-danger">
					 <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
					  <div class="box-header with-border">
					    <h4 class="box-title">
					        <span style="color:#3c8dbc">
                            Master IKU Monitoring
					        </span>
					    </h4>
					  </div>
					 </a>
                      <div id="collapseThree" class="panel-collapse collapse">
                       <div class="box-body">
						<table class="table table-hover table-striped table-bordered">
							<tr>
		                      <th style="width: 10px">No</th>
		                      <th>Name</th>
		                      <th width="32%">Action</th>
		                    </tr>
		                    <?php $x = 1; ?>
		                    <?php foreach($IKU->result() as $row): ?>
		                    <tr>
		                      <td><?php echo $x; ?></td>
		                      <td><?php echo $row->name; ?></td>
		                      <td><button id="IKU;3;<?= $row->id; ?>;<?= $row->name; ?>" type="button" class="edit btn btn-xs btn-warning" data-toggle="modal" data-target="#myModal"> Ubah</i></button>
		                      	<button id="IKU;3;<?= $row->id; ?>;<?= $row->name; ?>" type="button" class="delete btn btn-xs btn-danger" data-toggle="modal" data-target="#myModal"> <i class="fa fa-trash"> Hapus</i></button>
								</td>
		                    </tr>
		                    <?php $x = $x + 1; ?>
		                <?php endforeach; ?>
						</table>
                        </div>
                        <div class="box-footer">
						<button type="button" id="IKU;3" class="add btn btn-sm btn-success" data-toggle="modal" data-target="#myModal"><i class="fa fa-plus"> Tambah</i></button></div>
                      </div>
					</div>
					*/ ?>

					<div class="panel box box-danger">
					 <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour">
					  <div class="box-header with-border">
					    <h4 class="box-title">
					        <span style="color:#3c8dbc">
                            Master Satuan Pengukuran
					        </span>
					    </h4>
					  </div>
					 </a>
                      <div id="collapseFour" class="panel-collapse collapse">
                        <div class="box-body">
						<table class="table table-hover table-striped table-bordered">
							<tr>
		                      <th style="width: 10px">No</th>
		                      <th>Name</th>
		                      <th width="32%">Action</th>
		                    </tr>
		                    <?php $x = 1; ?>
		                    <?php foreach($SATUAN_PENGUKURAN->result() as $row): ?>
		                    <tr>
		                      <td><?php echo $x; ?></td>
		                      <td><?php echo $row->name; ?></td>
		                      <td><button id="SATUAN PENGUKURAN;4;<?= $row->id; ?>;<?= $row->name; ?>" type="button" class="edit btn btn-xs btn-warning" data-toggle="modal" data-target="#myModal"> Ubah</i></button>
		                      	<button id="SATUAN PENGUKURAN;4;<?= $row->id; ?>;<?= $row->name; ?>" type="button" class="delete btn btn-xs btn-danger" data-toggle="modal" data-target="#myModal"> <i class="fa fa-trash"> Hapus</i></button>
								</td>
		                    </tr>
		                    <?php $x = $x + 1; ?>
		                <?php endforeach; ?>
						</table>
                        </div>
                        <div class="box-footer">
						<button type="button" id="SATUAN PENGUKURAN;4" class="add btn btn-sm btn-success" data-toggle="modal" data-target="#myModal"><i class="fa fa-plus"> Tambah</i></button></div>
                      </div>
					</div>


					<div class="panel box box-danger">
					 <a data-toggle="collapse" data-parent="#accordion" href="#collapseFive">
					  <div class="box-header with-border">
					    <h4 class="box-title">
					        <span style="color:#3c8dbc">
                            Master Jenis Aspek Target
					        </span>
					    </h4>
					  </div>
					 </a>
                      <div id="collapseFive" class="panel-collapse collapse">
                        <div class="box-body">
						<table class="table table-hover table-striped table-bordered">
							<tr>
		                      <th style="width: 10px">No</th>
		                      <th>Name</th>
		                      <th width="32%">Action</th>
		                    </tr>
		                    <?php $x = 1; ?>
		                    <?php foreach($JENIS_ASPEK_TARGET->result() as $row): ?>
		                    <tr>
		                      <td><?php echo $x; ?></td>
		                      <td><?php echo $row->name; ?></td>
		                      <td><button id="JENIS ASPEK TARGET;5;<?= $row->id; ?>;<?= $row->name; ?>" type="button" class="edit btn btn-xs btn-warning" data-toggle="modal" data-target="#myModal"> Ubah</i></button>
		                      	<button id="JENIS ASPEK TARGET;5;<?= $row->id; ?>;<?= $row->name; ?>" type="button" class="delete btn btn-xs btn-danger" data-toggle="modal" data-target="#myModal"> <i class="fa fa-trash"> Hapus</i></button>
								</td>
		                    </tr>
		                    <?php $x = $x + 1; ?>
		                <?php endforeach; ?>
						</table>
                        </div>
                        <div class="box-footer">
						<button type="button" id="JENIS ASPEK TARGET;5" class="add btn btn-sm btn-success" data-toggle="modal" data-target="#myModal"><i class="fa fa-plus"> Tambah</i></button></div>
                      </div>
					</div>


					<div class="panel box box-danger">
					 <a data-toggle="collapse" data-parent="#accordion" href="#collapseSix">
					  <div class="box-header with-border">
					    <h4 class="box-title">
					        <span style="color:#3c8dbc">
                            Master Tingkat Kendali IKU
					        </span>
					    </h4>
					  </div>
					 </a>
                      <div id="collapseSix" class="panel-collapse collapse">
                        <div class="box-body">
						<table class="table table-hover table-striped table-bordered">
							<tr>
		                      <th style="width: 10px">No</th>
		                      <th>Name</th>
		                      <th width="32%">Action</th>
		                    </tr>
		                    <?php $x = 1; ?>
		                    <?php foreach($TINGKAT_KENDALI_IKU->result() as $row): ?>
		                    <tr>
		                      <td><?php echo $x; ?></td>
		                      <td><?php echo $row->name; ?></td>
		                      <td><button id="TINGKAT KENDALI IKU;6;<?= $row->id; ?>;<?= $row->name; ?>" type="button" class="edit btn btn-xs btn-warning" data-toggle="modal" data-target="#myModal"> Ubah</i></button>
		                      	<button id="TINGKAT KENDALI IKU;6;<?= $row->id; ?>;<?= $row->name; ?>" type="button" class="delete btn btn-xs btn-danger" data-toggle="modal" data-target="#myModal"> <i class="fa fa-trash"> Hapus</i></button>
								</td>
		                    </tr>
		                    <?php $x = $x + 1; ?>
		                <?php endforeach; ?>
						</table>
                        </div>
                        <div class="box-footer">
						<button type="button" id="TINGKAT KENDALI IKU;6" class="add btn btn-sm btn-success" data-toggle="modal" data-target="#myModal"><i class="fa fa-plus"> Tambah</i></button></div>
                      </div>
					</div>

					<div class="panel box box-danger">
					 <a data-toggle="collapse" data-parent="#accordion" href="#collapseSeven">
					  <div class="box-header with-border">
					    <h4 class="box-title">
					        <span style="color:#3c8dbc">
                            Master Tingkat Validitas IKU
					        </span>
					    </h4>
					  </div>
					 </a>
                      <div id="collapseSeven" class="panel-collapse collapse">
                        <div class="box-body">
						<table class="table table-hover table-striped table-bordered">
							<tr>
		                      <th style="width: 10px">No</th>
		                      <th>Name</th>
		                      <th width="32%">Action</th>
		                    </tr>
		                    <?php $x = 1; ?>
		                    <?php foreach($TINGKAT_VALIDITAS_IKU->result() as $row): ?>
		                    <tr>
		                      <td><?php echo $x; ?></td>
		                      <td><?php echo $row->name; ?></td>
		                      <td><button id="TINGKAT VALIDITAS IKU;7;<?= $row->id; ?>;<?= $row->name; ?>" type="button" class="edit btn btn-xs btn-warning" data-toggle="modal" data-target="#myModal"> Ubah</i></button>
		                      	<button id="TINGKAT VALIDITAS IKU;7;<?= $row->id; ?>;<?= $row->name; ?>" type="button" class="delete btn btn-xs btn-danger" data-toggle="modal" data-target="#myModal"> <i class="fa fa-trash"> Hapus</i></button>
								</td>
		                    </tr>
		                    <?php $x = $x + 1; ?>
		                <?php endforeach; ?>
						</table>
                        </div>
                        <div class="box-footer">
						<button type="button" id="TINGKAT VALIDITAS IKU;7" class="add btn btn-sm btn-success" data-toggle="modal" data-target="#myModal"><i class="fa fa-plus"> Tambah</i></button></div>
                      </div>
					</div>

                  </div>
                  </div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="box box-solid" >
                <div class="box-header with-border">
                  <h3 class="box-title"></h3>
                </div><!-- /.box-header -->
                <div class="box-body">
					<div class="box-group" id="accordion2">

					<div class="panel box box-danger">
					 <a data-toggle="collapse" data-parent="#accordion2" href="#collapseEight">
					  <div class="box-header with-border">
					    <h4 class="box-title">
					        <span style="color:#3c8dbc">
                            Master Jenis Cascading IKU
					        </span>
					    </h4>
					  </div>
					 </a>
                      <div id="collapseEight" class="panel-collapse collapse">
                        <div class="box-body">
						<table class="table table-hover table-striped table-bordered">
							<tr>
		                      <th style="width: 10px">No</th>
		                      <th>Name</th>
		                      <th width="32%">Action</th>
		                    </tr>
		                    <?php $x = 1; ?>
		                    <?php foreach($JENIS_CASCADING_IKU->result() as $row): ?>
		                    <tr>
		                      <td><?php echo $x; ?></td>
		                      <td><?php echo $row->name; ?></td>
		                      <td><button id="JENIS CASCADING IKU;8;<?= $row->id; ?>;<?= $row->name; ?>" type="button" class="edit btn btn-xs btn-warning" data-toggle="modal" data-target="#myModal"> Ubah</i></button>
		                      	<button id="TINGKAT VALIDITAS IKU;7;<?= $row->id; ?>;<?= $row->name; ?>" type="button" class="delete btn btn-xs btn-danger" data-toggle="modal" data-target="#myModal"> <i class="fa fa-trash"> Hapus</i></button>
								</td>
		                    </tr>
		                    <?php $x = $x + 1; ?>
		                <?php endforeach; ?>
						</table>
                        </div>
                        <div class="box-footer">
						<button type="button" id="JENIS CASCADING IKU;8" class="add btn btn-sm btn-success" data-toggle="modal" data-target="#myModal"><i class="fa fa-plus"> Tambah</i></button></div>
                      </div>
					</div>


					<div class="panel box box-danger">
					 <a data-toggle="collapse" data-parent="#accordion2" href="#collapseNine">
					  <div class="box-header with-border">
					    <h4 class="box-title">
					        <span style="color:#3c8dbc">
                            Master Metode Cascading
					        </span>
					    </h4>
					  </div>
					 </a>
                      <div id="collapseNine" class="panel-collapse collapse">
                        <div class="box-body">
						<table class="table table-hover table-striped table-bordered">
							<tr>
		                      <th style="width: 10px">No</th>
		                      <th>Name</th>
		                      <th width="32%">Action</th>
		                    </tr>
		                    <?php $x = 1; ?>
		                    <?php foreach($METODE_CASCADING->result() as $row): ?>
		                    <tr>
		                      <td><?php echo $x; ?></td>
		                      <td><?php echo $row->name; ?></td>
		                      <td><button id="METODE CASCADING;9;<?= $row->id; ?>;<?= $row->name; ?>" type="button" class="edit btn btn-xs btn-warning" data-toggle="modal" data-target="#myModal"> Ubah</i></button>
		                      	<button id="METODE CASCADING;9;<?= $row->id; ?>;<?= $row->name; ?>" type="button" class="delete btn btn-xs btn-danger" data-toggle="modal" data-target="#myModal"> <i class="fa fa-trash"> Hapus</i></button>
								</td>
		                    </tr>
		                    <?php $x = $x + 1; ?>
		                <?php endforeach; ?>
						</table>
                        </div>
                        <div class="box-footer">
						<button type="button" id="METODE CASCADING;9" class="add btn btn-sm btn-success" data-toggle="modal" data-target="#myModal"><i class="fa fa-plus"> Tambah</i></button></div>
                      </div>
					</div>


					<div class="panel box box-danger">
					 <a data-toggle="collapse" data-parent="#accordion2" href="#collapseTen">
					  <div class="box-header with-border">
					    <h4 class="box-title">
					        <span style="color:#3c8dbc">
                            Master Jenis Konsolidasi Periode
					        </span>
					    </h4>
					  </div>
					 </a>
                      <div id="collapseTen" class="panel-collapse collapse">
                        <div class="box-body">
						<table class="table table-hover table-striped table-bordered">
							<tr>
		                      <th style="width: 10px">No</th>
		                      <th>Name</th>
		                      <th width="32%">Action</th>
		                    </tr>
		                    <?php $x = 1; ?>
		                    <?php foreach($JENIS_KONSOLIDASI_PERIODE->result() as $row): ?>
		                    <tr>
		                      <td><?php echo $x; ?></td>
		                      <td><?php echo $row->name; ?></td>
		                      <td><button id="JENIS KONSOLIDASI PERIODE;10;<?= $row->id; ?>;<?= $row->name; ?>" type="button" class="edit btn btn-xs btn-warning" data-toggle="modal" data-target="#myModal"> Ubah</i></button>
		                      	<button id="JENIS KONSOLIDASI PERIODE;10;<?= $row->id; ?>;<?= $row->name; ?>" type="button" class="delete btn btn-xs btn-danger" data-toggle="modal" data-target="#myModal"> <i class="fa fa-trash"> Hapus</i></button>
								</td>
		                    </tr>
		                    <?php $x = $x + 1; ?>
		                <?php endforeach; ?>
						</table>
                        </div>
                        <div class="box-footer">
						<button type="button" id="JENIS KONSOLIDASI PERIODE;10" class="add btn btn-sm btn-success" data-toggle="modal" data-target="#myModal"><i class="fa fa-plus"> Tambah</i></button></div>
                      </div>
					</div>

					<div class="panel box box-danger">
					 <a data-toggle="collapse" data-parent="#accordion2" href="#collapseEleven">
					  <div class="box-header with-border">
					    <h4 class="box-title">
					        <span style="color:#3c8dbc">
                            Master Jenis Konsolidasi Kinerja
					        </span>
					    </h4>
					  </div>
					 </a>
                      <div id="collapseEleven" class="panel-collapse collapse">
                        <div class="box-body">
						<table class="table table-hover table-striped table-bordered">
							<tr>
		                      <th style="width: 10px">No</th>
		                      <th>Name</th>
		                      <th width="32%">Action</th>
		                    </tr>
		                    <?php $x = 1; ?>
		                    <?php foreach($JENIS_KONSOLIDASI_LOKASI->result() as $row): ?>
		                    <tr>
		                      <td><?php echo $x; ?></td>
		                      <td><?php echo $row->name; ?></td>
		                      <td><button id="JENIS KONSOLIDASI LOKASI;11;<?= $row->id; ?>;<?= $row->name; ?>" type="button" class="edit btn btn-xs btn-warning" data-toggle="modal" data-target="#myModal"> Ubah</i></button>
		                      	<button id="JENIS KONSOLIDASI LOKASI;11;<?= $row->id; ?>;<?= $row->name; ?>" type="button" class="delete btn btn-xs btn-danger" data-toggle="modal" data-target="#myModal"> <i class="fa fa-trash"> Hapus</i></button>
								</td>
		                    </tr>
		                    <?php $x = $x + 1; ?>
		                <?php endforeach; ?>
						</table>
                        </div>
                        <div class="box-footer">
						<button type="button" id="JENIS KONSOLIDASI LOKASI;11" class="add btn btn-sm btn-success" data-toggle="modal" data-target="#myModal"><i class="fa fa-plus"> Tambah</i></button></div>
                      </div>
					</div>

					<div class="panel box box-danger">
					 <a data-toggle="collapse" data-parent="#accordion2" href="#collapseTwelve">
					  <div class="box-header with-border">
					    <h4 class="box-title">
					        <span style="color:#3c8dbc">
                            Master Polarisasi Indikator Kinerja
					        </span>
					    </h4>
					  </div>
					 </a>
                      <div id="collapseTwelve" class="panel-collapse collapse">
                        <div class="box-body">
						<table class="table table-hover table-striped table-bordered">
							<tr>
		                      <th style="width: 10px">No</th>
		                      <th>Name</th>
		                      <th width="32%">Action</th>
		                    </tr>
		                    <?php $x = 1; ?>
		                    <?php foreach($POLARISASI_INDIKATOR_KINERJA->result() as $row): ?>
		                    <tr>
		                      <td><?php echo $x; ?></td>
		                      <td><?php echo $row->name; ?></td>
		                      <td><button id="POLARISASI INDIKATOR KINERJA;12;<?= $row->id; ?>;<?= $row->name; ?>" type="button" class="edit btn btn-xs btn-warning" data-toggle="modal" data-target="#myModal"> Ubah</i></button>
		                      	<button id="POLARISASI INDIKATOR KINERJA;12;<?= $row->id; ?>;<?= $row->name; ?>" type="button" class="delete btn btn-xs btn-danger" data-toggle="modal" data-target="#myModal"> <i class="fa fa-trash"> Hapus</i></button>
								</td>
		                    </tr>
		                    <?php $x = $x + 1; ?>
		                <?php endforeach; ?>
						</table>
                        </div>
                        <div class="box-footer">
						<button type="button" id="POLARISASI INDIKATOR KINERJA;12" class="add btn btn-sm btn-success" data-toggle="modal" data-target="#myModal"><i class="fa fa-plus"> Tambah</i></button></div>
                      </div>
					</div>

					<div class="panel box box-danger">
					 <a data-toggle="collapse" data-parent="#accordion2" href="#collapseThirteen">
					  <div class="box-header with-border">
					    <h4 class="box-title">
					        <span style="color:#3c8dbc">
                            Master Periode Pelaporan
					        </span>
					    </h4>
					  </div>
					 </a>
                      <div id="collapseThirteen" class="panel-collapse collapse">
                        <div class="box-body">
						<table class="table table-hover table-striped table-bordered">
							<tr>
		                      <th style="width: 10px">No</th>
		                      <th>Name</th>
		                      <th width="32%">Action</th>
		                    </tr>
		                    <?php $x = 1; ?>
		                    <?php foreach($PERIODE_PELAPORAN->result() as $row): ?>
		                    <tr>
		                      <td><?php echo $x; ?></td>
		                      <td><?php echo $row->name; ?></td>
		                      <td><button id="PERIODE PELAPORAN;13;<?= $row->id; ?>;<?= $row->name; ?>" type="button" class="edit btn btn-xs btn-warning" data-toggle="modal" data-target="#myModal"> Ubah</i></button>
		                      	<button id="PERIODE PELAPORAN;13;<?= $row->id; ?>;<?= $row->name; ?>" type="button" class="delete btn btn-xs btn-danger" data-toggle="modal" data-target="#myModal"> <i class="fa fa-trash"> Hapus</i></button>
								</td>
		                    </tr>
		                    <?php $x = $x + 1; ?>
		                <?php endforeach; ?>
						</table>
                        </div>
                        <div class="box-footer">
						<button type="button" id="PERIODE PELAPORAN;13" class="add btn btn-sm btn-success" data-toggle="modal" data-target="#myModal"><i class="fa fa-plus"> Tambah</i></button></div>
                      </div>
					</div>

                  </div>
                  </div>
		</div>
	</div>
</div> 






<script type="text/javascript">
	$('.pagination').addClass('pagination-sm no-margin pull-right');
</script>

<?php $this->load->view('iku/V_input_master') ?>

