
<style media="screen">
	th {
    text-align: center;
  	}
  	thead tr {
    	color: #fff;
    	background: #00acd6;
  	}

  	#iGrid .ui-selecting { background: #FECA40; }
  	#iGrid .ui-selected { background: #F39814; color: white; }

  	.ui-autocomplete { height: 200px; overflow-y: scroll; overflow-x: hidden;}
</style>

<div class="row">
	<div class="col-md-12">
		<div class="box box-primary">
			<form  method="POST" action="<?php echo base_url(); ?>iku/crud" enctype="multipart/form-data">
				<div class="box-header with-border">
					<i class="fa fa-edit"></i>
					<h3 class="box-title"><?php echo $judul ?></h3>
				</div> <!-- box body -->
				<div class="box-body clearfix">
		    		<div class="form-group">
		    			<div class="input-group col-sm-12">
							<div id="input">
			                  <div class="box-body">
			                    <div class="form-group" style="display:none">
			                      <label class="col-sm-5 control-label" style="text-align: right">ID</label>
			                      <div class="col-sm-7">
			                        <div class="form-group">  
			                           <input id="action" value="Rekam" name="action" class="form-control" />
			                           <input id="idForm" value="" name="idForm" class="form-control" />
			                        </div><!-- /.form-group -->
			                      </div>
			                    </div>
			                    <div class="form-group" 
									<?php if($this->session->userdata('idusergroup') != '001'){ ?> style="display:none" <?php } ?>>
			                      <label class="col-sm-3 control-label" style="text-align: right">User</label>
			                      <div class="col-sm-9">
			                        <div class="form-group col-sm-8">  
									<?php if($this->session->userdata('idusergroup') != '001'){ ?>
									  <input id="userForm"name="userForm" class="form-control" value="<?php echo $this->session->userdata('nip'); ?>"/>
									<?php }else{ ?>
			                          <select id="userForm" name="userForm" class="form-control select2" style="width: 100%;">
			                            <option value=""><i>- SELECT -</i></option>
			                            <?php foreach($USER->result() as $row): ?>
			                              <option value="<?php echo $row->nip; ?>"><?php echo $row->fullname; ?></option>
			                            <?php endforeach; ?>
			                          </select>
									<?php } ?>
			                        </div><!-- /.form-group --> 
									<button type="button" data-toggle="tooltip" title="Pilih user yang akan dibuatkan IKU" class="btn btn-box-tool">
										<i style="font-size:18px;" class="fa fa-fw fa-question-circle"></i>
									</button>
			                      </div>
			                    </div>&nbsp;
			                    <div class="form-group">
			                      <label class="col-sm-3 control-label" style="text-align: right">Perspektif</label>
			                      <div class="col-sm-9">
			                        <div class="form-group col-sm-8">  
			                          <select id="perspektifForm" name="perspektifForm" class="form-control select2" style="width: 100%;">
			                            <option value=""><i>- SELECT -</i></option>
			                            <?php foreach($PERSPEKTIF->result() as $row): ?>
			                              <option value="<?php echo $row->id; ?>"><?php echo $row->name; ?></option>
			                            <?php endforeach; ?>
			                          </select>
			                        </div><!-- /.form-group -->
									<button type="button" data-toggle="tooltip" title="Pilih nama perspektif balanced scorecard di mana indikator kinerja tersebut berada. Hanya diisi oleh pemilik peta strategi" class="btn btn-box-tool">
										<i style="font-size:18px;" class="fa fa-fw fa-question-circle"></i>
									</button>
			                      </div>
			                    </div>
			                    <div class="form-group">
			                      <label class="col-sm-3 control-label" style="text-align: right">Sasaran Strategis</label>
			                      <div class="col-sm-9">
			                        <div class="form-group col-sm-8">  
			                          <select id="sasaranStrategisForm" name="sasaranStrategisForm" class="form-control select2" style="width: 100%;">
			                            <option value=""><i>- SELECT -</i></option>
			                            <?php foreach($SASARAN_STRATEGIS->result() as $row): ?>
			                              <option value="<?php echo $row->id; ?>"><?php echo $row->name; ?></option>
			                            <?php endforeach; ?>
			                          </select>
			                        </div><!-- /.form-group -->
									<button type="button" data-toggle="tooltip" title="Pilih kode sasaran strategis yang diikuti nama sasaran strategisnya" class="btn btn-box-tool">
										<i style="font-size:18px;" class="fa fa-fw fa-question-circle"></i>
									</button>
			                      </div>
			                    </div>
			                    <div class="form-group">
			                      <label class="col-sm-3 control-label" style="text-align: right">Desc Sasaran Strategis</label>
			                      <div class="col-sm-9">
			                        <div class="form-group col-sm-8">  
			                          <textarea id="descSasaranStrategisForm" name="descSasaranStrategisForm" class="form-control" placeholder="Deskripsi Sasaran Strategis ..." rows="3"></textarea>
			                        </div><!-- /.form-group -->
									<button type="button" data-toggle="tooltip" title="Tuliskan uraian SS dimaksud yang meliputi pengertian, alasan, ruang lingkup, dan tujuan SS tersebut" class="btn btn-box-tool">
										<i style="font-size:18px;" class="fa fa-fw fa-question-circle"></i>
									</button>
			                      </div>
			                    </div>&nbsp;
			                    <div class="form-group">
			                     <?php /* <label class="col-sm-5 control-label" style="text-align: right">Indikator Kinerja Utama Monitoring</label>
			                      <div class="col-sm-7">
			                        <div class="form-group">  
			                          <select id="IKUMonitoringForm" name="IKUMonitoringForm" class="form-control select2" style="width: 100%;">
			                            <option value=""><i>- SELECT -</i></option>
			                            <?php foreach($IKUMONITORING->result() as $row): ?>
			                              <option value="<?php echo $row->id; ?>"><?php echo $row->name; ?></option>
			                            <?php endforeach; ?>
			                          </select>
			                        </div><!-- /.form-group -->
			                      </div>
			                    </div>*/ ?>
			                    <div class="form-group">
			                      <label class="col-sm-3 control-label" style="text-align: right">Indikator Kinerja Utama Atasan</label>
			                      <div class="col-sm-9">
			                        <div class="form-group col-sm-8">  
			                          <select id="IKUAtasanForm" name="IKUAtasanForm" class="form-control select2" style="width: 100%;">
			                            <option value=""><i>- SELECT -</i></option>
			                            <?php foreach($IKUATASAN->result() as $row): ?>
			                              <option value="<?php echo $row->id; ?>"><?php echo $row->fullname.' - '.$row->name; ?></option>
			                            <?php endforeach; ?>
			                          </select>
			                        </div><!-- /.form-group -->
									<button type="button" data-toggle="tooltip" title="Pilih IKU atasan yang akan mereferensi pengisian berdasarkan IKU atasan, kosongkan bila IKU diisi manual" class="btn btn-box-tool">
										<i style="font-size:18px;" class="fa fa-fw fa-question-circle"></i>
									</button>
			                      </div>
			                    </div>
			                    <div class="form-group">
			                      <label class="col-sm-3 control-label" style="text-align: right">Indikator Kinerja Utama </label>
			                      <div class="col-sm-9">
			                        <div class="form-group col-sm-8">  
			                          <input id="IKUForm" value="" name="IKUForm" class="form-control" />
			                        </div><!-- /.form-group -->
									<button type="button" data-toggle="tooltip" title="Tuliskan kode IKU yang diikuti nama indikator kinerja utama" class="btn btn-box-tool">
										<i style="font-size:18px;" class="fa fa-fw fa-question-circle"></i>
									</button>
			                      </div>
			                    </div>&nbsp;
			                    <div class="form-group">
			                      <label class="col-sm-3 control-label" style="text-align: right">Definisi</label>
			                      <div class="col-sm-9">
			                        <div class="form-group col-sm-8">  
			                          <textarea id="definisiForm" name="definisiForm" class="form-control" placeholder="Definisi ..." rows="3"></textarea>
			                        </div><!-- /.form-group -->
									<button type="button" data-toggle="tooltip" title="Tuliskan uraian mengenai IKU yang mencangkup: Definisi" class="btn btn-box-tool">
										<i style="font-size:18px;" class="fa fa-fw fa-question-circle"></i>
									</button>
			                      </div>
			                    </div>&nbsp;
			                    <div class="form-group">
			                      <label class="col-sm-3 control-label" style="text-align: right">Formula</label>
			                      <div class="col-sm-9">
			                        <div class="form-group col-sm-8">  
			                          <textarea id="formulaForm" name="formulaForm" class="form-control" placeholder="Formula ..." rows="3"></textarea>
			                        </div><!-- /.form-group -->
									<button type="button" data-toggle="tooltip" title="Tuliskan uraian mengenai IKU yang mencangkup: Formula" class="btn btn-box-tool">
										<i style="font-size:18px;" class="fa fa-fw fa-question-circle"></i>
									</button>
			                      </div>
			                    </div>&nbsp;
			                    <div class="form-group">
			                      <label class="col-sm-3 control-label" style="text-align: right">Tujuan</label>
			                      <div class="col-sm-9">
			                        <div class="form-group col-sm-8">  
			                          <textarea id="tujuanForm" name="tujuanForm" class="form-control" placeholder="Tujuan ..." rows="3"></textarea>
			                        </div><!-- /.form-group -->
									<button type="button" data-toggle="tooltip" title="Tuliskan uraian mengenai IKU yang mencangkup: Tujuan" class="btn btn-box-tool">
										<i style="font-size:18px;" class="fa fa-fw fa-question-circle"></i>
									</button>
			                      </div>
			                    </div>&nbsp;
			                    <div class="form-group">
			                      <label class="col-sm-3 control-label" style="text-align: right">Satuan Pengukuran</label>
			                      <div class="col-sm-9">
			                        <div class="form-group col-sm-8">  
			                          <select id="satuanPengukuranForm" name="satuanPengukuranForm" class="form-control select2" style="width: 100%;">
			                            <option value=""><i>- SELECT -</i></option>
			                            <?php foreach($SATUAN_PENGUKURAN->result() as $row): ?>
			                              <option value="<?php echo $row->id; ?>"><?php echo $row->name; ?></option>
			                            <?php endforeach; ?>
			                          </select>
			                        </div><!-- /.form-group -->
									<button type="button" data-toggle="tooltip" title="Pilih unit pengukuran yang digunakan untuk menunjukan kuantitas indikator kinerja, misal %, Rp, USD, kali, buah, orang" class="btn btn-box-tool">
										<i style="font-size:18px;" class="fa fa-fw fa-question-circle"></i>
									</button>
			                      </div>
			                    </div>
			                    <div class="form-group">
			                      <label class="col-sm-3 control-label" style="text-align: right">Jenis Aspek Target</label>
			                      <div class="col-sm-9">
			                        <div class="form-group col-sm-8">  
			                          <select id="jenisAspekTargetForm" name="jenisAspekTargetForm" class="form-control select2" style="width: 100%;">
			                            <option value=""><i>- SELECT -</i></option>
			                            <?php foreach($JENIS_ASPEK_TARGET->result() as $row): ?>
			                              <option value="<?php echo $row->id; ?>"><?php echo $row->name; ?></option>
			                            <?php endforeach; ?>
			                          </select>
			                        </div><!-- /.form-group -->
									<button type="button" data-toggle="tooltip" title="Pilih jenis target berdasarkan metode BSC pada SKP: Kuantitas/output, kualitas/mutu, waktu, biaya (jika ada)" class="btn btn-box-tool">
										<i style="font-size:18px;" class="fa fa-fw fa-question-circle"></i>
									</button>
			                      </div>
			                    </div>
			                    <div class="form-group">
			                      <label class="col-sm-3 control-label" style="text-align: right">Tingkat Kendali IKU</label>
			                      <div class="col-sm-9">
			                        <div class="form-group col-sm-8">  
			                          <select id="tingkatKendaliIKUForm"  name="tingkatKendaliIKUForm" class="form-control select2" style="width: 100%;">
			                            <option value=""><i>- SELECT -</i></option>
			                            <?php foreach($TINGKAT_KENDALI_IKU->result() as $row): ?>
			                              <option value="<?php echo $row->id; ?>"><?php echo $row->name; ?></option>
			                            <?php endforeach; ?>
			                          </select>
			                        </div><!-- /.form-group -->
									<button type="button" data-toggle="tooltip" title="Pilih tingkat kendali indikator kinerja utama" class="btn btn-box-tool">
										<i style="font-size:18px;" class="fa fa-fw fa-question-circle"></i>
									</button>
			                      </div>
			                    </div>
			                    <div class="form-group">
			                      <label class="col-sm-3 control-label" style="text-align: right">Tingkat Validitas IKU</label>
			                      <div class="col-sm-9">
			                        <div class="form-group col-sm-8">  
			                          <select id="tingkatValiditasIKUForm" name="tingkatValiditasIKUForm"  class="form-control select2" style="width: 100%;">
			                            <option value=""><i>- SELECT -</i></option>
			                            <?php foreach($TINGKAT_VALIDITAS_IKU->result() as $row): ?>
			                              <option value="<?php echo $row->id; ?>"><?php echo $row->name; ?></option>
			                            <?php endforeach; ?>
			                          </select>
			                        </div><!-- /.form-group -->
									<button type="button" data-toggle="tooltip" title="Pilih tingkat validitas indikator kinerja utama" class="btn btn-box-tool">
										<i style="font-size:18px;" class="fa fa-fw fa-question-circle"></i>
									</button>
			                      </div>
			                    </div>
			                    <div class="form-group">
			                      <label class="col-sm-3 control-label" style="text-align: right">Pihak Penanggung Jawab IKU</label>
			                      <div class="col-sm-9">
			                        <div class="form-group col-sm-8">  
			                          <textarea id="pihakPJIKUForm" name="pihakPJIKUForm" class="form-control" placeholder="Pihak Penanggung Jawab IKU ..." rows="3"></textarea>
			                        </div><!-- /.form-group -->
									<button type="button" data-toggle="tooltip" title="Tuliskan unit/individu pada level dibawahnya yang bertanggung jawab terhadap pencapaian IKU tersebut, apabila IKU tersebut tidak dicascade maka penanggung jawab IKU adalah unit/individu bersangkutan" class="btn btn-box-tool">
										<i style="font-size:18px;" class="fa fa-fw fa-question-circle"></i>
									</button>
			                      </div>
			                    </div>&nbsp;
			                    <div class="form-group">
			                      <label class="col-sm-3 control-label" style="text-align: right">Pihak Penyedia Jasa</label>
			                      <div class="col-sm-9">
			                        <div class="form-group col-sm-8">  
			                          <textarea id="pihakPenyediaJasaForm" name="pihakPenyediaJasaForm" class="form-control" placeholder="Pihak Penyedia Jasa ..." rows="3"></textarea>
			                        </div><!-- /.form-group -->
									<button type="button" data-toggle="tooltip" title="Tuliskan unit/individu yang bertanggung jawab terhadap penyedia jasa" class="btn btn-box-tool">
										<i style="font-size:18px;" class="fa fa-fw fa-question-circle"></i>
									</button>
			                      </div>
			                    </div>&nbsp;
			                    <div class="form-group">
			                      <label class="col-sm-3 control-label" style="text-align: right">Sumber Data</label>
			                      <div class="col-sm-9">
			                        <div class="form-group col-sm-8">  
			                          <textarea id="sumberDataForm" name="sumberDataForm" class="form-control" placeholder="Sumber Data ..." rows="3"></textarea>
			                        </div><!-- /.form-group -->
									<button type="button" data-toggle="tooltip" title="Tuliskan nama dokumen sebagai sumber data untuk mengisi formula IKU" class="btn btn-box-tool">
										<i style="font-size:18px;" class="fa fa-fw fa-question-circle"></i>
									</button>
			                      </div>
			                    </div>&nbsp;
			                    <div class="form-group">
			                      <label class="col-sm-3 control-label" style="text-align: right">Jenis Cascading IKU</label>
			                      <div class="col-sm-9">
			                        <div class="form-group col-sm-8">  
			                          <select id="jenisCascadingIKUForm" name="jenisCascadingIKUForm"  class="form-control select2" style="width: 100%;">
			                            <option value=""><i>- SELECT -</i></option>
			                            <?php foreach($JENIS_CASCADING_IKU->result() as $row): ?>
			                              <option value="<?php echo $row->id; ?>"><?php echo $row->name; ?></option>
			                            <?php endforeach; ?>
			                          </select>
			                        </div><!-- /.form-group -->
									<button type="button" data-toggle="tooltip" title="Pilih jenis cascading pada indikator kinerja utama" class="btn btn-box-tool">
										<i style="font-size:18px;" class="fa fa-fw fa-question-circle"></i>
									</button>
			                      </div>
			                    </div>
			                    <div class="form-group">
			                      <label class="col-sm-3 control-label" style="text-align: right">Metode Cascading</label>
			                      <div class="col-sm-9">
			                        <div class="form-group col-sm-8">  
			                          <select id="metodeCascadingForm" name="metodeCascadingForm"  class="form-control select2" style="width: 100%;">
			                            <option value=""><i>- SELECT -</i></option>
			                            <?php foreach($METODE_CASCADING->result() as $row): ?>
			                              <option value="<?php echo $row->id; ?>"><?php echo $row->name; ?></option>
			                            <?php endforeach; ?>
			                          </select>
			                        </div><!-- /.form-group -->
									<button type="button" data-toggle="tooltip" title="Pilih metode cascading (tidak diisi untuk jenis cascading IKU non-cascading)" class="btn btn-box-tool">
										<i style="font-size:18px;" class="fa fa-fw fa-question-circle"></i>
									</button>
			                      </div>
			                    </div>
			                    <div class="form-group">
			                      <label class="col-sm-3 control-label" style="text-align: right">Jenis Konsolidasi Periode</label>
			                      <div class="col-sm-9">
			                        <div class="form-group col-sm-8">  
			                          <select id="jenisKonsolidasiPeriodeForm" name="jenisKonsolidasiPeriodeIdForm"  class="form-control select2" style="width: 100%;">
			                            <option value=""><i>- SELECT -</i></option>
			                            <?php foreach($JENIS_KONSOLIDASI_PERIODE->result() as $row): ?>
			                              <option value="<?php echo $row->id; ?>"><?php echo $row->name; ?></option>
			                            <?php endforeach; ?>
			                          </select>
			                        </div><!-- /.form-group -->
									<button type="button" data-toggle="tooltip" title="Pilih jenis kondoslidasi periode" class="btn btn-box-tool">
										<i style="font-size:18px;" class="fa fa-fw fa-question-circle"></i>
									</button>
			                      </div>
			                    </div>
			                    <div class="form-group">
			                      <label class="col-sm-3 control-label" style="text-align: right">Jenis Konsolidasi Lokasi</label>
			                      <div class="col-sm-9">
			                        <div class="form-group col-sm-8">  
			                          <select id="jenisKonsolidasiLokasiForm" name="jenisKonsolidasiLokasiForm"  class="form-control select2" style="width: 100%;">
			                            <option value=""><i>- SELECT -</i></option>
			                            <?php foreach($JENIS_KONSOLIDASI_LOKASI->result() as $row): ?>
			                              <option value="<?php echo $row->id; ?>"><?php echo $row->name; ?></option>
			                            <?php endforeach; ?>
			                          </select>
			                        </div><!-- /.form-group -->
									<button type="button" data-toggle="tooltip" title="Pilih jenis konsolidasi Lokasi" class="btn btn-box-tool">
										<i style="font-size:18px;" class="fa fa-fw fa-question-circle"></i>
									</button>
			                      </div>
			                    </div>
			                    <div class="form-group">
			                      <label class="col-sm-3 control-label" style="text-align: right">Polarisasi Indikator Kinerja</label>
			                      <div class="col-sm-9">
			                        <div class="form-group col-sm-8">  
			                          <select id="polarisasiIndikatorKinerjaForm" name="polarisasiIndikatorKinerjaForm"  class="form-control select2" style="width: 100%;">
			                            <option value=""><i>- SELECT -</i></option>
			                            <?php foreach($POLARISASI_INDIKATOR_KINERJA->result() as $row): ?>
			                              <option value="<?php echo $row->id; ?>"><?php echo $row->name; ?></option>
			                            <?php endforeach; ?>
			                          </select>
			                        </div><!-- /.form-group -->
									<button type="button" data-toggle="tooltip" title="Pilih polarisasi indikator kinerja utama" class="btn btn-box-tool">
										<i style="font-size:18px;" class="fa fa-fw fa-question-circle"></i>
									</button>
			                      </div>
			                    </div>
			                    <div class="form-group">
			                      <label class="col-sm-3 control-label" style="text-align: right">Periode Pelaporan</label>
			                      <div class="col-sm-9">
			                        <div class="form-group col-sm-8">  
			                          <select id="periodePelaporanForm" name="periodePelaporanForm"  class="select-periode form-control select2" style="width: 100%;" required>
			                            <option value=""><i>- SELECT -</i></option>
			                            <?php foreach($PERIODE_PELAPORAN->result() as $row): ?>
			                              <option value="<?php echo $row->id; ?>"><?php echo $row->name; ?></option>
			                            <?php endforeach; ?>
			                          </select>
			                        </div><!-- /.form-group -->
									<button type="button" data-toggle="tooltip" title="Pilih periode pelaporan untuk mengisi table periode" class="btn btn-box-tool">
										<i style="font-size:18px;" class="fa fa-fw fa-question-circle"></i>
									</button>
			                      </div>
			                    </div>
			                    <div class="form-group">
			                      <label class="col-sm-3 control-label" style="text-align: right">Konversi 120</label>
			                      <div class="col-sm-9">
			                        <div class="form-group col-sm-8">  
			                          <select id="konversi120Form" name="konversi120Form"  class="form-control select2" style="width: 100%;">
			                            <option value=""><i>- SELECT -</i></option>
			                              <option value="Y">YA</option>
			                              <option value="N">TIDAK</option>
			                          </select>
			                        </div><!-- /.form-group -->
									<button type="button" data-toggle="tooltip" title="Pilih konversi 120" class="btn btn-box-tool">
										<i style="font-size:18px;" class="fa fa-fw fa-question-circle"></i>
									</button>
			                      </div>
			                    </div>
			                    <div class="form-group">
			                      <label class="col-sm-3 control-label" style="text-align: right">File Dokumen</label>
			                      <div class="col-sm-9">
			                        <div class="form-group col-sm-8">  
           								 <input type="file" name="userfile" size="9999" type="file" multiple="multiple"  />
			                        </div><!-- /.form-group -->
									<button type="button" data-toggle="tooltip" title="Masukin file dokumen apabila terdapat dokumen yang harus diinput/disimpan (jika diperlukan)" class="btn btn-box-tool">
										<i style="font-size:18px;" class="fa fa-fw fa-question-circle"></i>
									</button>
			                      </div>
			                    </div>
			                  </div>
			              </div>
						<input value="0" type="hidden" name="total-column" id="total-column" />

		    			</div></br>
						<div id="periode_form" class="col-sm-12" style="text-align:center">
						<label><i>Untuk Menginput Jumlah Periode Pelaporan Silahkan Pilih Periode Pelaporan Terlebih Dahulu!</i></label>
							<?php /*<div class="col-sm-11">
								<table class="table table-striped table-bordered table-hover table-condensed">
								<thead><tr><th>asd</th><th>qwe</th><th>rty</th></tr></thead>
								<tbody>
									<tr><td>dfg</td><td>rty</td><td>dfg</td></tr>
									<tr><td>dfg</td><td>rty</td><td>dfg</td></tr>
									<tr><td>dfg</td><td>rty</td><td>dfg</td></tr>
									</tbody></table>
							</div>
							<div class="col-sm-1">
								<button class="btn btn-primary">ADD</button>
							</div> */ ?>
						</div>
		    		</div>
				</div>
				<div class="box-header with border">
					<div class="wel wel-sm">
		        		<div class="form-group">
		        			<div class="input-group col-sm-12">
									<span style="padding-right:10px;"><a type="button" id="back"  class="btn btn-warning" <?php /*data-toggle="modal" data-target="#myModal"*/ ?> href="<?php echo base_url(); ?>iku"><i class="fa  fa-mail-reply"> Back</i></a></span>
					  				<?php /*<span style="padding-right:10px;float:right;">
					  					<button type="reset"  id="resetselect" class="btn btn-danger" value="Reset" <?php /* data-toggle="modal" data-target="#myModal"*//*?>>
					  						<i class="fa  fa-times"> Reset</i>
					  					</button>*/ ?>
					  				</span>
									<span style="padding-right:10px;float:right;">
										<button type="submit" id="buat" class="btn btn-success" <?php /*data-toggle="modal" data-target="#myModal"*/ ?> href="<?php echo base_url(); ?>iku/form_add">
											<i class="fa fa-check"> Submit</i>
										</button>
									</span>
		        			</div>
		        		</div>
		            </div>
		        </div>   <!-- box header  -->
			</form>
		</div>
	</div>
</div>


<?php //$this->load->view('iku/V_paket_rekam') ?>
<script src="<?=base_url('assets/plugins/slimScroll/jquery.slimscroll.min.js'); ?>" type="text/javascript"></script>    
<script src="<?=base_url('assets/plugins/jQueryUI/jquery-ui.1.11.4.min.js'); ?>" type="text/javascript"></script>
<link rel="stylesheet" href="<?=base_url('assets/plugins/jQueryUI/jquery-ui.css'); ?>">

 <link rel="stylesheet" href="<?=base_url('assets/plugins/select2/select2.min.css'); ?>" type="text/css" />
<link href="<?=base_url('assets/dist/css/AdminLTE.min.css');?>" rel="stylesheet" type="text/css" />
 <script src="<?=base_url('assets/plugins/select2/select2.full.min.js');?>"></script>
<style>

/* Important part */
.modal-dialog{
    overflow-y: initial !important
}
.modal-body{
    overflow-y: auto;
}</style>   


<script type="text/javascript">

  $(function() {

  $("#userForm").hide();
  $("#perspektifForm").hide();
  $("#sasaranStrategisForm").hide();
  $("#IKUAtasanForm").hide();
  $("#satuanPengukuranForm").hide();
  $("#jenisAspekTargetForm").hide();
  $("#tingkatKendaliIKUForm").hide();
  $("#tingkatValiditasIKUForm").hide();
  $("#jenisCascadingIKUForm").hide();
  $("#metodeCascadingForm").hide();
  $("#jenisKonsolidasiPeriodeForm").hide();
  $("#jenisKonsolidasiLokasiForm").hide();
  $("#polarisasiIndikatorKinerjaForm").hide();
  $("#periodePelaporanForm").hide();
  $("#konversi120Form").hide();

    $( "#definisiForm" ).autocomplete({
      source: function(req, add){
          $.ajax({
            url: "<?php echo base_url();?>iku/lookup",
            dataType: 'json',
            type: 'POST',
			data: { 
					term : req,
					type: 'definisi'
				  },
            success:
            function(data){
              if(data.response =='true'){
                add(data.message);
              }
            }
          });
        },
    });


    $( "#formulaForm" ).autocomplete({
      source: function(req, add){
          $.ajax({
            url: "<?php echo base_url();?>iku/lookup",
            dataType: 'json',
            type: 'POST',
			data: { 
					term : req,
					type: 'formula'
				  },
            success:
            function(data){
              if(data.response =='true'){
                add(data.message);
              }
            }
          });
        },
    });

    $( "#tujuanForm" ).autocomplete({
      source: function(req, add){
          $.ajax({
            url: "<?php echo base_url();?>iku/lookup",
            dataType: 'json',
            type: 'POST',
			data: { 
					term : req,
					type: 'tujuan'
				  },
            success:
            function(data){
              if(data.response =='true'){
                add(data.message);
              }
            }
          });
        },
    });
    $("#resetselect").on("click", function(e) { 
		 $("select").each(function() { this.selectedIndex = 0 });
    	alert('asd');
  });

    $(".select2").select2();
    // Tandai ROW yang dipilih dalam iGRID
    $( "#iGrid tbody" ).selectable({
          filter: ":not(td)",
          create: function( e, ui ) {
              iGridLink( $() );
          },
          selected: function( e, ui ) {
              var widget = $(this).find('.ui-selected');
              $(ui.unselected).addClass("info");
              iGridLink( widget );
          },
          unselected: function( e, ui ) {
              $(ui.unselected).removeClass("info");
              var widget = $(this).find('.ui-selected');
              iGridLink( widget );
          }
    });

    // Ambil Nilai ROW pada iGRID yang dipilih per-ID sesuai kolom CHILD
    function iGridLink( $selectees ) {
      selected = $.makeArray( $selectees.filter( ".ui-selected" ) );
     }

  }); 
</script>


<script type="text/javascript">
    $(document).ready(function(){
        $('.select-periode').change(function(){
            $.post("<?php echo base_url();?>iku/getPeriode?id="+$('#periodePelaporanForm').val(),{},function(obj){
                $('#periode_form').html(obj);
				$("#total-column").val(0);

            });
        });
    });

    $('#IKUAtasanForm').on('change', function() {

		$.post("<?php echo base_url();?>iku/getAtasan?id="+this.value,{},function(obj){

			var datas = $.parseJSON(obj);
			$("#definisiForm").val(datas.definisi);
			$("#formulaForm").val(datas.formula);
			$("#tujuanForm").val(datas.tujuan);
			$("#pihakPJIKUForm").val(datas.pihakPJIku);
			$("#pihakPenyediaJasaForm").val(datas.pihakPenyediaJasa);
			$("#sumberDataForm").val(datas.sumberData);
			$("#satuanPengukuranForm").val(datas.satuanPengukuranId).change();
			$("#jenisAspekTargetForm").val(datas.jenisAspekTargetId).change();
			$("#tingkatKendaliIKUForm").val(datas.tingkatKendaliIkuId).change();
			$("#tingkatValiditasIKUForm").val(datas.tingkatValiditasIkuId).change();
			$("#jenisCascadingIKUForm").val(datas.jenisCascadingIkuId).change();
			$("#metodeCascadingForm").val(datas.metodeCascadingId).change();
			$("#jenisKonsolidasiPeriodeForm").val(datas.jenisKonsolidasiPeriodeId).change();
			$("#jenisKonsolidasiLokasiForm").val(datas.jenisKonsolidasiLokasiId).change();
			$("#polarisasiIndikatorKinerjaForm").val(datas.polarisasiIndikatorKinerjaId).change();
			$("#periodePelaporanForm").val(datas.periodePelaporanId).change();
			$("#konversi120Form").val(datas.konversi120).change();

		});

    });

</script>
