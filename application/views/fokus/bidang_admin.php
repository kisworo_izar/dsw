<!--  -->
<script src="<?=base_url('assets/plugins/jQuery/jquery.validate.min.js');?>" type="text/javascript"></script>
<script src="<?=base_url('assets/plugins/datepicker/bootstrap-datepicker.js');?>" type="text/javascript"></script>

<script src="<?=base_url('assets/plugins/summernote/summernote.min.js'); ?>"></script>
<link rel="stylesheet" href="<?=base_url('assets/plugins/summernote/summernote.css'); ?>">

<style>
.datepicker{z-index:1151 !important;}
</style>

<script>
$(function(){
    $("#tanggal").datepicker({
  format:'yyyy-mm-dd'
    });

    $("#tanggal2").datepicker({
  format:'yyyy-mm-dd'
    });

    $("#tanggal3").datepicker({
  format:'yyyy-mm-dd'
    });

    $("#tanggal4").datepicker({
  format:'yyyy-mm-dd'
    });

    $("#tanggal5").datepicker({
  format:'yyyy-mm-dd'
    });

    $("#tanggal6").datepicker({
  format:'yyyy-mm-dd'
    });
    $("#tanggal7").datepicker({
  format:'yyyy-mm-dd'
    });
    $("#tanggal8").datepicker({
  format:'yyyy-mm-dd'
    });
    $("#tanggal9").datepicker({
  format:'yyyy-mm-dd'
    });

});

</script>
<!--  -->
<div class="row">

    <div class="col-md-12">
        <div class="box box-widget">
            <div class="box-header with-border">
                <h3 class="box-title"><i class="fa fa-bar-chart"></i> Monitoring </3><span class="text-primary">Bidang <?php  echo $this->uri->segment(3); ?></span>
            </div>

          <?php $this->load->view('fokus/modal_add_moncap'); ?>

                
            <div class="box-body with-border">
                <div class="box-body table-responsive no-padding">
                  <table class="table table-striped">
                    <tr style="background:#eeeeee">
                      <th class="text-center" width="1%">Bidang</th>
                      <th class="text-center" width="1%">No</th>
                      <th class="text-center" width="30%">Output</th>
                      <th class="hidden-xs" width="5%">Volume</th>
                      <th class="text-center" width="2%">Progres</th>
                      <th class="text-center" width="10%">Waktu</th>
                      <th class="hidden-xs hidden-sm text-center" width="30%">Catatan</th>
                      <th class="hidden-xs hidden-sm text-center" width="10%">Option</th>
                    </tr>

                    <?php $no=1;foreach($moncap as $mcp){ ?>
                      <tr>
                        <td class="text-center"><?php echo $mcp->id_bidang; ?></td>
                        <td><?php echo $no; ?></td>
                        <td class="text-justify"><?php echo $mcp->output; ?></td>
                        <td class="text-justify" width="20%"><?php echo $mcp->volume; ?></td>
                        <td class="text-center"><?php 
                          //echo $mcp->progres;
                          $progres=$mcp->progres;
                          if ($progres <= 30) {
                            echo "<span class='badge bg-red'>$progres %</span>";
                          }
                          elseif ($progres <= 70) {
                            echo "<span class='badge bg-yellow'>$progres %</span>";
                          }
                          elseif ($progres <= 100) {
                            echo "<span class='badge bg-green'>$progres %</span>";
                          }
                        ?>
                        </td>
                        <td class="text-center"><?php echo $mcp->tgl_batas; ?></td>
                        <td class="text-justify"><?php echo $mcp->catatan; ?></td>
                        <td class="text-center"> 
                          <button type="button" class="btn btn-info btn-xs" data-toggle="modal" data-target="#moncap_edit<?php echo  $mcp->id_moncap; ?>">
                          <span class="fa fa-pencil"> </span></button>

                          <button type="button" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#moncap_hapus<?php echo  $mcp->id_moncap; ?>">
                          <span class="fa fa-trash"> </span></button>
                        </td>

                        <!-- MODAL untuk EDIT -->
                          <div id="moncap_edit<?php echo  $mcp->id_moncap; ?>" class="modal modal-info fade" role="dialog">
                            <div class="modal-dialog modal-lg">
                              <!-- Modal content-->
                              <div class="modal-content">
                                <div class="modal-header">
                                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                                  <h4 class="modal-title">Edit Monitoring Capaian</h4>
                                </div>
                                    <div class="box box-info">
                                      <!-- /.box-header -->
                                      <!-- form start -->
                                      <form class="form-horizontal"  method="POST" id="frm-mhs" action="<?php echo site_url('fokus/bidang_admin/edit'); ?>">

                                      <input type="hidden" name="id_moncap" class="form-control" value="<?php  echo $mcp->id_moncap; ?>" >
                                      <input type="hidden" name="kd_jenis" class="form-control" value="<?php  echo $mcp->kd_jenis; ?>" >
                                        <div class="box-body">

                                          <div class="box-body">
                                            <div class="form-group">
                                              <label for="nmodul" class="col-sm-2 control-label">Bidang</label>
                                              <div class="col-sm-1">
                                                <input type="text" name="id_bidang" class="form-control" value="<?php echo  $mcp->id_bidang; ?>" readonly=readonly>
                                              </div>
                                            </div>
                                          </div>

                                          <div class="box-body">
                                            <div class="form-group">
                                              <label for="nmodul" class="col-sm-2 control-label">Output</label>
                                              <div class="col-sm-10">
                                                <script type="text/javascript">
                                                      $(document).ready(function() {
                                                          $('#summernotes<?php echo  $mcp->id_moncap; ?>').summernote({
                                                              height: 150,
                                                              placeholder: 'Isikan posting Anda disini ...',
                                                              onImageUpload: function(files, editor, welEditable) {
                                                                  sendFile(files[0], editor, welEditable);
                                                              }
                                                          });

                                                          function sendFile(file,editor,welEditable) {
                                                              data = new FormData();
                                                              data.append("file", file);
                                                              $.ajax({
                                                                  url: "<?php echo site_url('forum/upload_image') ?>",
                                                                  data: data,
                                                                  cache: false,
                                                                  contentType: false,
                                                                  processData: false,
                                                                  success: function(data){
                                                                      alert(data);
                                                                      $('.summernotes<?php echo  $mcp->id_moncap; ?>').summernote("insertImage", data, 'filename');
                                                                  },
                                                                  error: function(jqXHR, textStatus, errorThrown) {
                                                                      console.log(textStatus+" "+errorThrown);
                                                                  }
                                                              });
                                                          }
                                                      });

                                                      var postForm = function() {
                                                          var content = $('textarea[name="content"]').html($('#summernotes<?php echo  $mcp->id_moncap; ?>').code());
                                                      }
                                                  </script>
                                                <textarea id="summernotes<?php echo  $mcp->id_moncap; ?>" name="output" class="form-control" required><?php echo  $mcp->output; ?></textarea>
                                              </div>
                                            </div>
                                          </div>

                                          <div class="box-body">
                                            <div class="form-group">
                                              <label for="nmodul" class="col-sm-2 control-label">Volume</label>
                                              <div class="col-sm-4">
                                                <input type="text" name="volume" class="form-control" value="<?php echo  $mcp->volume; ?>" required>
                                              </div>
                                            </div>
                                          </div>
                                          
                                          <div class="box-body">
                                            <div class="form-group">
                                              <label for="nmodul" class="col-sm-2 control-label">Progres</label>
                                              <div class="col-sm-10">
                                                <select required name="progres">
                                                  <option value="<?php echo  $mcp->progres; ?>" selected><?php echo  $mcp->progres; ?></option>
                                                  <?php 
                                                    for ($i=0; $i <= 100 ; $i++) { 
                                                      echo"<option value='$i'>$i</option>";
                                                    }
                                                   ?>
                                                </select> %
                                              </div>
                                            </div>
                                          </div>
                                        
                                          

                                          <div class="box-body">
                                            <div class="form-group">
                                              <label for="nmodul" class="col-sm-2 control-label">Batas Waktu</label>
                                              <div class="col-sm-3">
                                              <!-- Seharian di kantor plototin laptop untuk ni datepicker T_T -->
                                              <script>
                                                  $(function(){
                                                      $("#tanggal<?php echo  $mcp->id_moncap; ?>").datepicker({
                                                    format:'yyyy-mm-dd'
                                                      });
                                                  });
                                                  </script>
                                                <input type="text" name="tgl_batas" class="form-control" id="tanggal<?php echo  $mcp->id_moncap; ?>" value="<?php echo  $mcp->tgl_batas; ?>" required>
                                              </div>
                                            </div>
                                          </div>
                                          

                                          <div class="box-body">
                                            <div class="form-group">
                                              <label for="nmodul" class="col-sm-2 control-label">Catatan</label>
                                              <div class="col-sm-10">
                                                <textarea class="form-control" name="catatan" required><?php echo  $mcp->catatan; ?></textarea>
                                              </div>
                                            </div>
                                          </div>
                                          

                                        </div>
                                        <!-- /.box-body -->

                                        <!-- /.box-footer -->
                                    </div>
                                  

                                    <div class="modal-footer">
                                      <button type="submit" class="btn btn-sm btn-default pull-left">Simpan</button>
                                      <button type="button" class="btn btn-sm btn-default pull-right" data-dismiss="modal">Close</button> 
                                    </div>
                                  </div>
                                </form>
                              </div>
                            </div>
                            
                          <!-- MODAL untuk EDIT -->

                          <!-- MODAL untuk HAPUS -->
                            <div id="moncap_hapus<?php echo  $mcp->id_moncap; ?>" class="modal fade" role="dialog">
                              <div class="modal-dialog modal-lg">
                                <!-- Modal content-->
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">Hapus Monitoring</h4>
                                  </div>
                                  
                                      <div class="col-md-12">
                                        <div class="alert alert-danger alert-dismissible">
                                        
                                          <h4><i class="icon fa fa-ban"></i> Perhatian!</h4>
                                          Anda akan menghapus data monitoring.
                                        </div>
                                        <form class="form-horizontal"  method="POST"  action="<?php echo site_url('fokus/bidang_admin/del'); ?>">
                                        <input type="hidden" name="id_moncap" class="form-control" value="<?php  echo $mcp->id_moncap; ?>" >
                                        <input type="hidden" name="id_bidang" class="form-control" value="<?php  echo $mcp->id_bidang; ?>" >
                                          <div class="box-body">

                                            <div class="box-body">
                                              <div class="form-group">
                                                <label for="nmodul" class="col-sm-2 control-label">Bidang</label>
                                                <div class="col-sm-1">
                                                  <input type="text" name="bidang" class="form-control" value="<?php echo  $mcp->id_bidang; ?>" disabled>
                                                </div>
                                              </div>
                                            </div>

                                            <div class="box-body">
                                              <div class="form-group">
                                                <label for="nmodul" class="col-sm-2 control-label">Output</label>
                                                <div class="col-sm-10">
                                                  <textarea class="form-control" name="output" disabled><?php echo  $mcp->output; ?></textarea>
                                                </div>
                                              </div>
                                            </div>
                                            
                                            <div class="box-body">
                                              <div class="form-group">
                                                <label for="nmodul" class="col-sm-2 control-label">Progres</label>
                                                <div class="col-sm-10">
                                                  <select disabled name="progres">
                                                    <option value="<?php echo  $mcp->progres; ?>" selected><?php echo  $mcp->progres; ?></option>
                                                    <?php 
                                                      for ($i=0; $i <= 100 ; $i++) { 
                                                        echo"<option value='$i'>$i</option>";
                                                      }
                                                     ?>
                                                  </select> %
                                                </div>
                                              </div>
                                            </div>
                                          
                                            

                                            <div class="box-body">
                                              <div class="form-group">
                                                <label for="nmodul" class="col-sm-2 control-label">Batas Waktu</label>
                                                <div class="col-sm-3">
                                                <!-- Seharian di kantor plototin laptop untuk ni datepicker T_T -->
                                                <script>
                                                    $(function(){
                                                        $("#tanggalmcp<?php echo  $mcp->id_moncap; ?>").datepicker({
                                                      format:'yyyy-dd-mm'
                                                        });
                                                    });
                                                    </script>
                                                  <input type="text" name="waktu" class="form-control" id="tanggalmcp<?php echo  $mcp->id_moncap; ?>" value="<?php echo  $mcp->tgl_batas; ?>" disabled>
                                                </div>
                                              </div>
                                            </div>
                                            

                                            <div class="box-body">
                                              <div class="form-group">
                                                <label for="nmodul" class="col-sm-2 control-label">Catatan</label>
                                                <div class="col-sm-10">
                                                  <textarea class="form-control" name="catatan" disabled><?php echo  $mcp->catatan; ?></textarea>
                                                </div>
                                              </div>
                                            </div>
                                            
                                          </div>
                                          <!-- /.box-body -->
                                          <div class="box-footer">
                                            <button type="submit" class="btn btn-danger">Hapus</button>
                                            <button type="submit" class="btn btn-default pull-right" data-dismiss="modal">Batal</button>

                                          </div>
                                          <!-- /.box-footer -->
                                        </form>
                                        <!-- /.box-body -->

                                      </div>

                                    <div class="modal-footer">
                                        <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
                                    </div>
                                  </div>

                                </div>
                              </div>
                          <!-- MODAL untuk HAPUS -->

                      </tr>
                    <?php  $no++; }?>
                  </table>
              </div>
            </div>
            <div class="box-footer with-border">
                <p class="small" style="margin-bottom:0px">
                    Catatan : <br>
                    Monitoring Capaian Bidang ...
                </p>
            </div>
        </div>

<!-- MONITORING KEGIATAN -->
        <div class="box box-widget">
            <div class="box-header with-border">
                <h3 class="box-title"><i class="fa fa-bar-chart"></i> Monitoring Kegiatan </3><span class="text-primary">Bidang <?php  echo $this->uri->segment(3); ?></span>
            </div>
            <div class="box-body with-border">
                <div class="box-body table-responsive no-padding">
                  <table class="table" width="30%">
                    <tr style="background:#eeeeee">
                      <th class="text-center" width="1%">No</th>
                      <th class="text-center" width="30%">Uraian</th>
                      <th class="text-center" width="2%">Gambar</th>
                    </tr>
                    

                    <?php $no=1; foreach($monkeg as $mkg){ ?>
                      <tr>
                        <td><?php echo $no; ?></td>
                        <td class="text-justify"><?php echo $mkg->output; ?></td>
                        <td class="text-justify"><img style="float:right; width:95px !important; height:75px !important; margin: 5px 10px 5px 0px" src=" <?php echo base_url('files/moncap/');?>/<?php  echo $mkg->link_img;  ?> ">
                        </td>
                        <!-- <td class="text-justify"><?php echo $mcp->tgl_batas; ?></td>
                        <td class="text-justify"><?php echo $mcp->catatan; ?></td> -->
                      </tr>
                    <?php  $no++; }?>


                  </table>
              </div>
            </div>
            
            <div class="box-footer with-border">
                <p class="small" style="margin-bottom:0px">
                    Catatan : <br>
                    Monitoring Capaian Bidang ...
                </p>
            </div>
        </div>

<!-- Materi Inisiatif ##################################################################################################################################### -->
        <div class="box box-widget">
            <div class="box-header with-border">
                <h3 class="box-title"><i class="fa fa-slideshare"></i> Materi Inisiatif</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body with-border">
                <div class="box-body table-responsive no-padding">
                  <table class="table table-striped">
                    <tr style="background:#eeeeee">
                      <th class="hidden-xs">No</th>
                      <th>Materi</th>
                      <th class="hidden-xs">Fokus TKDJA</th>
                      <th>Fokus DJA</th>
                      <th>Catatan</th>
                      <th>Option</th>
                    </tr>

                  <?php $no=1; foreach($matin as $mt){ ?>
                    <tr>
                      <td><?php echo $no; ?></td>
                      <td class="text-justify"><?php echo $mt->output; ?></td>
                      <td class="text-justify">
                          <i class="fa fa-clock-o text-green"></i> <?php echo $mt->tgl_tkda_a; ?><br>
                          <i class="fa fa-clock-o text-red"></i> <?php echo $mt->tgl_tkda_z; ?> 
                        </td>
                      <td class="text-justify">
                          <i class="fa fa-clock-o text-green"></i> <?php echo $mt->tgl_da_a; ?><br>
                          <i class="fa fa-clock-o text-red"></i> <?php echo $mt->tgl_da_z; ?> 
                        </td>
                      <td class="text-justify"><?php echo $mt->catatan; ?></td>
                      <td class="text-center">
                        <button type="button" class="btn btn-info btn-xs" data-toggle="modal" data-target="#matin_edit<?php echo  $mt->id_moncap; ?>">
                        <span class="fa fa-pencil"> </span></button>

                        <button type="button" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#matin_hapus<?php echo  $mt->id_moncap; ?>">
                        <span class="fa fa-trash"> </span></button>
                      </td>

                        <!-- MODAL untuk EDIT -->
                          <div id="matin_edit<?php echo  $mt->id_moncap; ?>" class="modal modal-info fade" role="dialog">
                            <div class="modal-dialog modal-lg">
                              <!-- Modal content-->
                              <div class="modal-content">
                                <div class="modal-header">
                                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                                  <h4 class="modal-title">Edit Monitoring Capaian</h4>
                                </div>
                                    <div class="box box-info">
                                      <!-- /.box-header -->
                                      <!-- form start -->
                                      <form class="form-horizontal"  method="POST" id="frm-mhs" action="<?php echo site_url('fokus/bidang_admin/edit'); ?>">

                                      <input type="hidden" name="id_moncap" class="form-control" value="<?php  echo $mt->id_moncap; ?>" >
                                      <input type="hidden" name="kd_jenis" class="form-control" value="<?php  echo $mt->kd_jenis; ?>" >
                                        <div class="box-body">

                                          <div class="box-body">
                                            <div class="form-group">
                                              <label for="nmodul" class="col-sm-2 control-label">Bidang</label>
                                              <div class="col-sm-1">
                                                <input type="text" name="id_bidang" class="form-control" value="<?php echo  $mt->id_bidang; ?>" required>
                                              </div>
                                            </div>
                                          </div>

                                          <div class="box-body">
                                            <div class="form-group">
                                              <label for="nmodul" class="col-sm-2 control-label">Materi</label>
                                              <div class="col-sm-10">
                                                <textarea class="form-control" name="output" required><?php echo  $mt->output; ?></textarea>
                                              </div>
                                            </div>
                                          </div>

                                          <div class="box-body">
                                            <div class="form-group">
                                              <label for="nmodul" class="col-sm-2 control-label">TKDJA</label>
                                                <div class="col-sm-4">
                                                  <div class="input-group">
                                                    <div class="input-group-addon bg-green">
                                                      <i class="fa fa-calendar"></i>
                                                    </div>
                                                    <script>
                                                      $(function(){
                                                          $("#tanggaltkda_a<?php echo  $mt->id_moncap; ?>").datepicker({
                                                        format:'yyyy-dd-mm'
                                                          });
                                                      });
                                                    </script>
                                                    <input type="text" name="tgl_tkda_a" class="form-control" id="tanggaltkda_a<?php echo  $mt->id_moncap; ?>" value="<?php echo $mt->tgl_tkda_a; ?>" placeholder="Mulai" required>
                                                  </div>
                                                </div>

                                                <div class="col-sm-4">
                                                  <div class="input-group">
                                                    <div class="input-group-addon bg-red">
                                                      <i class="fa fa-calendar"></i>
                                                    </div>
                                                        <script>
                                                        $(function(){
                                                            $("#tanggaltkda_z<?php echo  $mt->id_moncap; ?>").datepicker({
                                                          format:'yyyy-dd-mm'
                                                            });
                                                        });
                                                      </script>
                                                    <input type="text" name="tgl_tkda_z" class="form-control" id="tanggaltkda_z<?php echo  $mt->id_moncap; ?>" value="<?php echo $mt->tgl_tkda_z; ?>" placeholder="Selesai" required>
                                                  </div>
                                                </div>
                                            </div>
                                          </div>

                                          <div class="box-body">
                                            <div class="form-group">
                                              <label for="nmodul" class="col-sm-2 control-label">DJA</label>
                                                <div class="col-sm-4">
                                                  <div class="input-group">
                                                    <div class="input-group-addon bg-green">
                                                      <i class="fa fa-calendar"></i>
                                                    </div>
                                                    <script>
                                                      $(function(){
                                                          $("#tanggalda_a<?php echo  $mt->id_moncap; ?>").datepicker({
                                                        format:'yyyy-dd-mm'
                                                          });
                                                      });
                                                    </script>
                                                    <input type="text" name="tgl_da_a" class="form-control" id="tanggalda_a<?php echo $mt->id_moncap ?>" value="<?php echo $mt->tgl_da_a; ?>" placeholder="Mulai" required>
                                                  </div>
                                                </div>
                                                
                                                <div class="col-sm-4">
                                                  <div class="input-group">
                                                    <div class="input-group-addon bg-red">
                                                      <i class="fa fa-calendar"></i>
                                                    </div>
                                                    <script>
                                                      $(function(){
                                                          $("#tanggalda_z<?php echo  $mt->id_moncap; ?>").datepicker({
                                                        format:'yyyy-dd-mm'
                                                          });
                                                      });
                                                    </script>
                                                    <input type="text" name="tgl_da_z" class="form-control" id="tanggalda_z<?php echo $mt->id_moncap ?>" value="<?php echo $mt->tgl_da_z; ?>" placeholder="Selesai" required>
                                                  </div>
                                                </div>
                                            </div>
                                          </div>
                                          

                                          <div class="box-body">
                                            <div class="form-group">
                                              <label for="nmodul" class="col-sm-2 control-label">Catatan</label>
                                              <div class="col-sm-10">
                                                <textarea class="form-control" name="catatan" required><?php echo  $mt->catatan; ?></textarea>
                                              </div>
                                            </div>
                                          </div>
                                          

                                        </div>
                                        <!-- /.box-body -->

                                        <!-- /.box-footer -->
                                    </div>
                                  

                                    <div class="modal-footer">
                                      <button type="submit" class="btn btn-sm btn-default pull-left">Simpan</button>
                                      <button type="button" class="btn btn-sm btn-default pull-right" data-dismiss="modal">Close</button> 
                                    </div>
                                  </div>
                                </form>
                              </div>
                            </div>
                            
                          <!-- MODAL untuk EDIT -->

                          <!-- MODAL untuk HAPUS -->
                            <div id="matin_hapus<?php echo  $mt->id_moncap; ?>" class="modal fade" role="dialog">
                              <div class="modal-dialog modal-lg">
                                <!-- Modal content-->
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">Hapus Monitoring</h4>
                                  </div>
                                  
                                      <div class="col-md-12">
                                        <div class="alert alert-danger alert-dismissible">
                                        
                                          <h4><i class="icon fa fa-ban"></i> Perhatian!</h4>
                                          Anda akan menghapus data monitoring.
                                        </div>
                                        <form class="form-horizontal"  method="POST"  action="<?php echo site_url('fokus/bidang_admin/del'); ?>">
                                        <input type="text" name="id_moncap" class="form-control" value="<?php  echo $mt->id_moncap; ?>" >
                                        <input type="hidden" name="id_bidang" class="form-control" value="<?php  echo $mcp->id_bidang; ?>" >
                                          <div class="box-body">

                                            <div class="box-body">
                                              <div class="form-group">
                                                <label for="nmodul" class="col-sm-2 control-label">Bidang</label>
                                                <div class="col-sm-1">
                                                  <input type="text" name="bidang" class="form-control" value="<?php echo  $mcp->id_bidang; ?>" disabled>
                                                </div>
                                              </div>
                                            </div>

                                            <div class="box-body">
                                              <div class="form-group">
                                                <label for="nmodul" class="col-sm-2 control-label">Materi</label>
                                                <div class="col-sm-10">
                                                  <textarea class="form-control" name="output" disabled><?php echo  $mt->output; ?></textarea>
                                                </div>
                                              </div>
                                            </div>


                                            <div class="box-body">
                                              <div class="form-group">
                                                <label for="nmodul" class="col-sm-2 control-label">TKDJA</label>
                                                  <div class="col-sm-4">
                                                    <div class="input-group">
                                                      <div class="input-group-addon bg-green">
                                                        <i class="fa fa-calendar"></i>
                                                      </div>
                                                      <input type="text" name="tgl_tkda_a" class="form-control" id="tanggaltkda_a<?php echo  $mt->id_moncap; ?>" value="<?php echo $mt->tgl_tkda_a; ?>" placeholder="Mulai" disabled>
                                                    </div>
                                                  </div>

                                                  <div class="col-sm-4">
                                                    <div class="input-group">
                                                      <div class="input-group-addon bg-red">
                                                        <i class="fa fa-calendar"></i>
                                                      </div>
                                                          <script>
                                                          $(function(){
                                                              $("#tanggaltkda_z<?php echo  $mt->id_moncap; ?>").datepicker({
                                                            format:'yyyy-dd-mm'
                                                              });
                                                          });
                                                        </script>
                                                      <input type="text" name="tgl_tkda_z" class="form-control" id="tanggaltkda_z<?php echo  $mt->id_moncap; ?>" value="<?php echo $mt->tgl_tkda_z; ?>" placeholder="Selesai" disabled>
                                                    </div>
                                                  </div>
                                              </div>
                                            </div>

                                            <div class="box-body">
                                              <div class="form-group">
                                                <label for="nmodul" class="col-sm-2 control-label">DJA</label>
                                                  <div class="col-sm-4">
                                                    <div class="input-group">
                                                      <div class="input-group-addon bg-green">
                                                        <i class="fa fa-calendar"></i>
                                                      </div>
                                                      <input type="text" name="tgl_da_a" class="form-control" id="tanggalda_a<?php echo $mt->id_moncap ?>" value="<?php echo $mt->tgl_da_a; ?>" placeholder="Mulai" disabled>
                                                    </div>
                                                  </div>
                                                  
                                                  <div class="col-sm-4">
                                                    <div class="input-group">
                                                      <div class="input-group-addon bg-red">
                                                        <i class="fa fa-calendar"></i>
                                                      </div>
                                                      <input type="text" name="tgl_da_z" class="form-control" id="tanggalda_z<?php echo $mt->id_moncap ?>" value="<?php echo $mt->tgl_da_z; ?>" placeholder="Selesai" disabled>
                                                    </div>
                                                  </div>
                                              </div>
                                            </div>
                                              

                                            <div class="box-body">
                                              <div class="form-group">
                                                <label for="nmodul" class="col-sm-2 control-label">Catatan</label>
                                                <div class="col-sm-10">
                                                  <textarea class="form-control" name="catatan" disabled><?php echo  $mt->catatan; ?></textarea>
                                                </div>
                                              </div>
                                            </div>
                                            
                                          </div>
                                          <!-- /.box-body -->
                                          <div class="box-footer">
                                            <button type="submit" class="btn btn-danger">Hapus</button>
                                            <button type="submit" class="btn btn-default pull-right" data-dismiss="modal">Batal</button>

                                          </div>
                                          <!-- /.box-footer -->
                                        </form>
                                        <!-- /.box-body -->

                                      </div>

                                    <div class="modal-footer">
                                        <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
                                    </div>
                                  </div>

                                </div>
                              </div>
                          <!-- MODAL untuk HAPUS -->

                    </tr>
                  <?php  $no++; }?>
                  
                  </table>
              </div>
            </div>
            <div class="box-footer with-border">
                <p class="small text-muted pull-right hidden-xs">
                    Keterangan :
                    <i class="fa fa-clock-o text-green"></i> Waktu Mulai &nbsp;&nbsp;
                    <i class="fa fa-clock-o text-red"></i> Waktu Selesai
                </p>
                <p class="small" style="margin-bottom:0px">
                    Catatan : <br>
                    Materi inisiatif ...
                </p>
            </div>
            <div class="box-footer box-comments">
                <div class="box-comment" style="padding-bottom:0px">
                    <!-- User image -->
                    <img class="img-circle img-sm img-bordered-dsw" src="files/profiles/user1.jpg" alt="User Image">
                    <div class="comment-text">
                        <span class="username">
                            Hardi Lubis
                            <span class="text-muted pull-right">Kamis, 26 Nov 2015 8:03 PM</span>
                        </span><!-- /.username -->
                        Materi inisiatif perlu diperinci lebih lanjut.
                    </div>
                    <!-- /.comment-text -->
                </div>
                <!-- /.box-comment -->
            </div>
            <div class="box-footer">
                <form action="#" method="post">
                    <img class="img-responsive img-circle img-sm img-bordered-dsw" src=
                    <?php
                        $foto_profile="files/profiles/_noprofile.png";
                        if (file_exists("files/profiles/".$this->session->userdata('nip').".jpg")) {$foto_profile =  "files/profiles/".$this->session->userdata('nip').".jpg";}
                        echo $foto_profile;
                    ?> alt=<?php $this->session->userdata('nmuser')?>
                            img-push is used to add margin to elements next to floating images>
                    <div class="img-push">
                        <input type="text" class="form-control input-sm" placeholder="Tanggapan ...">
                    </div>
                </form>
            </div>
        </div>
    </div>

    
</div>

