<div class="box-body">
<button type="button" class="btn btn-info btn-xs" data-toggle="modal" data-target="#myModal"><span class="fa fa-plus"> </span> Tambah Monitoring</button>  
</div>

<!--content modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Tambah Monitoring</h4>
      </div>


          <div class="box box-info">

            <div class="box-body">
              <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                  <li class="active"><a href="#tab_1" data-toggle="tab">Monitoring Capaian</a></li>
                  <li><a href="#tab_2" data-toggle="tab">Monitoring Kegiatan</a></li>
                  <li><a href="#tab_3" data-toggle="tab">Materi Inisiatif</a></li>
                </ul>

                <div class="tab-content">
                  <div class="tab-pane active" id="tab_1">

                    <div class="box-body">
                      <form method="POST" action="<?php echo site_url('fokus/bidang_admin/add'); ?>" >
                        <input type="hidden" name="kd_jenis" value="1">
                          <div class="box-body">
                            <div class="form-group">
                              <label for="nmodul" class="col-sm-2 control-label">Bidang</label>
                              <div class="col-sm-2">
                              <select required name="id_bidang" class="form-control">
                                <?php foreach($bid as $bidang){ 
                                    if ($this->uri->segment(3)==$bidang->id_bidang) { ?>
                                      <option value="<?php echo $bidang->id_bidang; ?>" selected><?php echo $bidang->nmbidang; ?></option>
                                   <?php }
                                    else{ ?>
                                      <option value="<?php echo $bidang->id_bidang; ?>" ><?php echo $bidang->nmbidang; ?></option>
                                    <?php }?>
                                    
                                <?php } ?>
                              </select>
                              </div>
                            </div>
                          </div>

                           

                          <!-- <div class="box-body">
                            <div class="form-group">
                              <label for="nmodul" class="col-sm-2 control-label">Jenis Monitoring</label>
                              <div class="col-sm-4">
                              <select required name="kd_jenis" class="form-control" >
                                <?php foreach($jns as $jenis){ ?>
                                    <option value="<?php echo $jenis->kd_jenis; ?>"><?php echo $jenis->nmjenis; ?></option>
                                <?php } ?>
                              </select>
                              </div>
                            </div>
                          </div> -->

                          <div class="box-body">
                            <div class="form-group">
                              <label for="nmodul" class="col-sm-2 control-label">Output</label>
                              <div class="col-sm-10">
                                <textarea id="summernote" name="output" class="form-control" required></textarea>
                              </div>
                            </div>
                          </div>

                          <!-- <div class="box-body">
                            <div class="form-group">
                              <label for="nmodul" class="col-sm-2 control-label">Output</label>
                              <div class="col-sm-10">
                                <textarea name="output" class="form-control" required></textarea>
                              </div>
                            </div>
                          </div> -->

                          <div class="box-body">
                            <div class="form-group">
                              <label for="nmodul" class="col-sm-2 control-label">Volume</label>
                              <div class="col-sm-4">
                                <input type="text" name="volume" class="form-control" required>
                              </div>
                            </div>
                          </div>

                          <div class="box-body">
                            <div class="form-group">
                              <label for="nmodul" class="col-sm-2 control-label">Progres</label>
                              <div class="col-sm-2">
                                <div class="input-group">
                                    <select required name="progres" class="form-control" >
                                      <?php 
                                        for ($i=0; $i <= 100 ; $i++) { 
                                          echo"<option value='$i'>$i</option>";
                                        }
                                       ?>
                                    </select>
                                    <div class="input-group-addon">
                                      <i class="fa fa-percent"></i>
                                    </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="box-body">
                            <div class="form-group">
                              <label for="nmodul" class="col-sm-2 control-label">Waktu</label>
                                <div class="col-sm-4">
                                  <div class="input-group">
                                    <div class="input-group-addon">
                                      <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" name="tgl_batas" class="form-control" id="tanggal" required>
                                  </div>
                                </div>
                            </div>
                          </div>

                          <div class="box-body">
                            <div class="form-group">
                              <label for="nmodul" class="col-sm-2 control-label">Catatan</label>
                              <div class="col-sm-10">
                                <textarea name="catatan" class="form-control" required></textarea>
                              </div>
                            </div>
                          </div>
                          <!-- <input type="text" id="tanggal"> -->
                          <div class="box-body">
                          <button type="submit" class="btn bg-green pull-right">Simpan</button>
                          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                          </div>
                          </form>
                        </div>

                  </div>
                  <!-- /.tab-pane -->
                  <div class="tab-pane" id="tab_2">

                    <div class="box-body">
                      <form method="POST" action="<?php echo site_url('fokus/upload'); ?>" enctype="multipart/form-data">
                        <input type="hidden" name="kd_jenis" value="2">

                          <div class="box-body">
                            <div class="form-group">
                              <label for="nmodul" class="col-sm-2 control-label">Bidang</label>
                              <div class="col-sm-2">
                              <select required name="id_bidang" class="form-control"  >
                                <?php foreach($bid as $bidang){ 
                                    if ($this->uri->segment(3)==$bidang->id_bidang) { ?>
                                      <option value="<?php echo $bidang->id_bidang; ?>" selected><?php echo $bidang->nmbidang; ?></option>
                                   <?php }
                                    else{ ?>
                                      <option value="<?php echo $bidang->id_bidang; ?>" disabled><?php echo $bidang->nmbidang; ?></option>
                                    <?php }?>
                                    
                                <?php } ?>
                              </select>
                              </div>
                            </div>
                          </div>

                          <div class="box-body">
                            <div class="form-group">
                              <label for="nmodul" class="col-sm-2 control-label">Judul</label>
                              <div class="col-sm-10">
                                <input type="text" name="output" class="form-control" required>
                              </div>
                            </div>
                          </div>

                          <div class="box-body">

                            <div class="form-group">
                              <label for="nmodul" class="col-sm-2 control-label">Gambar</label>
                              <div class="col-sm-10">
                                <input name="gambar" type="file" class="file" required>
                              </div>
                            </div>
                          </div>
                          <!-- <input type="text" id="tanggal"> -->
                          <div class="box-body">
                          <button type="submit" class="btn bg-green pull-right">Simpan</button>
                          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                          </div>
                          </form>
                          
                        </div>
                  </div>
                  <!-- /.tab-pane -->
                  <div class="tab-pane" id="tab_3">

                    <div class="box-body">
                      <form method="POST" action="<?php echo site_url('fokus/bidang_admin/add'); ?>" >
                        <input type="hidden" name="kd_jenis" value="3">
                          <div class="box-body">
                            <div class="form-group">
                              <label for="nmodul" class="col-sm-2 control-label">Bidaxng</label>
                              <div class="col-sm-2">
                              <select required name="id_bidang" class="form-control"  >
                                <?php foreach($bid as $bidang){ 
                                    if ($this->uri->segment(3)==$bidang->id_bidang) { ?>
                                      <option value="<?php echo $bidang->id_bidang; ?>" selected><?php echo $bidang->nmbidang; ?></option>
                                   <?php }
                                    else{ ?>
                                      <option value="<?php echo $bidang->id_bidang; ?>" disabled><?php echo $bidang->nmbidang; ?></option>
                                    <?php }?>
                                    
                                <?php } ?>
                              </select>
                              </div>
                            </div>
                          </div>

                          <div class="box-body">
                            <div class="form-group">
                              <label for="nmodul" class="col-sm-2 control-label">Materi</label>
                              <div class="col-sm-10">
                                <textarea name="output" class="form-control" required></textarea>
                              </div>
                            </div>
                          </div>

                          <div class="box-body">
                            <div class="form-group">
                              <label for="nmodul" class="col-sm-2 control-label">TKDJA</label>
                                <div class="col-sm-4">
                                  <div class="input-group">
                                    <div class="input-group-addon bg-green">
                                      <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" name="tgl_tkda_a" class="form-control" id="tanggal2" placeholder="Mulai" required>
                                  </div>
                                </div>

                                <div class="col-sm-4">
                                  <div class="input-group">
                                    <div class="input-group-addon bg-red">
                                      <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" name="tgl_tkda_z" class="form-control" id="tanggal3" placeholder="Selesai" required>
                                  </div>
                                </div>
                            </div>
                          </div>

                          <div class="box-body">
                            <div class="form-group">
                              <label for="nmodul" class="col-sm-2 control-label">DJA</label>
                                <div class="col-sm-4">
                                  <div class="input-group">
                                    <div class="input-group-addon bg-green">
                                      <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" name="tgl_da_a" class="form-control" id="tanggal4" placeholder="Mulai" required>
                                  </div>
                                </div>
                                
                                <div class="col-sm-4">
                                  <div class="input-group">
                                    <div class="input-group-addon bg-red">
                                      <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" name="tgl_da_z" class="form-control" id="tanggal5" placeholder="Selesai" required>
                                  </div>
                                </div>
                            </div>
                          </div>

                          <div class="box-body">
                            <div class="form-group">
                              <label for="nmodul" class="col-sm-2 control-label">Catatan</label>
                              <div class="col-sm-10">
                                <textarea name="catatan" class="form-control" required></textarea>
                              </div>
                            </div>
                          </div>
                          <!-- <input type="text" id="tanggal"> -->
                          <div class="box-body">
                          <button type="submit" class="btn bg-green pull-right">Simpan</button>
                          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                          </div>
                          </form>
                        </div>
                  </div>
                  <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
              </div>
              <!-- nav-tabs-custom -->
            </div>

            
                <!-- /.box-footer -->
            </div>
          

            <!-- <div class="modal-footer">
               
            </div> -->
          </div>
    </div>
  </div>

<script type="text/javascript">
    $(document).ready(function() {
        $('#summernote').summernote({
            height: 150,
            placeholder: 'Isikan posting Anda disini ...',
            onImageUpload: function(files, editor, welEditable) {
                sendFile(files[0], editor, welEditable);
            }
        });

        function sendFile(file,editor,welEditable) {
            data = new FormData();
            data.append("file", file);
            $.ajax({
                url: "<?php echo site_url('forum/upload_image') ?>",
                data: data,
                cache: false,
                contentType: false,
                processData: false,
                success: function(data){
                    alert(data);
                    $('.summernote').summernote("insertImage", data, 'filename');
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    console.log(textStatus+" "+errorThrown);
                }
            });
        }
    });

    var postForm = function() {
        var content = $('textarea[name="content"]').html($('#summernote').code());
    }
</script>