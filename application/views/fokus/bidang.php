<div class="row">
    <div class="col-md-9">
        <div class="box box-widget">
            <div class="box-header with-border">
                <h3 class="box-title"><i class="fa fa-bar-chart"></i> Monitoring Capaian </3><span class="text-primary">Bidang <?php  echo $this->uri->segment(3); ?></span>
            </div>
            <div class="box-body with-border">
                <div class="box-body table-responsive no-padding">
                  <table class="table">
                    <tr style="background: #00acd6; color: #fff;">
                      <th style="width:10px" class="hidden-xs hidden-sm">No</th>
                      <th style="width:10px">Output</th>
                      <th style="width:10px">Progres</th>
                    </tr>
                    <?php $no=1; foreach($moncap as $mcp){ ?>
                      <tr>
                        <td class="hidden-xs hidden-sm"><?php echo $no; ?></td>
                        <td class="text-justify">
                          <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree<?php echo $mcp->id_moncap; ?>">
                          <?php echo $mcp->output; ?></a>
                            <div id="collapseThree<?php echo $mcp->id_moncap; ?>" class="panel-collapse collapse" style="background-color:#F4F4F4; padding:5px">
                                <dl class="small dl-horizontal" style="margin-bottom:0px;">
                                    <dt style="padding-left:0px">Volume</dt>
                                        <dd><?php echo $mcp->volume; ?></dd>
                                    <dt>Batas</dt>
                                        <dd><?php echo $this->fc->idtgl($mcp->tgl_batas ,'hari'); ?></dd>
                                    <dt>Catatan</dt>
                                        <dd><?php echo $mcp->catatan; ?></dd>
                              </dl>
                            </div>
                          </td>
                        <td><?php
                          $progres=$mcp->progres;
                          if ($progres <= 30) {
                            echo "<span class='badge bg-red'>$progres %</span>";
                          }
                          elseif ($progres <= 70) {
                            echo "<span class='badge bg-yellow'>$progres %</span>";
                          }
                          elseif ($progres <= 100) {
                            echo "<span class='badge bg-green'>$progres %</span>";
                          }
                        ?>
                        </td>
                      </tr>
                    <?php  $no++; }?>
                  </table>

              </div>
            </div>
        </div>

        <div class="box box-widget">
            <div class="box-header with-border">
                <h3 class="box-title"><i class="fa fa-bar-chart"></i> Monitoring Kegiatan </3><span class="text-primary">Bidang <?php  echo $this->uri->segment(3); ?></span>
            </div>
            <div class="box-body with-border">
                <div class="box-body table-responsive no-padding">
                  <table class="table">
                      <tr style="background: #00acd6; color: #fff;">
                      <th class="hidden-xs hidden-sm">No</th>
                      <th>Uraian</th>
                      <th></th>
                    </tr>
                    <?php $no=1; foreach($monkeg as $mkg){ ?>
                      <tr>
                        <td style="width:10px" class="hidden-xs hidden-sm"><?php echo $no; ?></td>
                        <td><?php echo $mkg->output; ?></td>
                        <td><a data-toggle="modal" href="#mkg<?php echo  $mkg->id_moncap; ?>"> <img style="width:95px !important; height:75px !important; margin: 5px 10px 5px 0px" src=" <?php echo base_url('files/moncap/');?>/<?php  echo $mkg->link_img;  ?> "></a>
                        </td>
                          <!-- MODAL untuk EDIT -->
                          <div id="mkg<?php echo  $mkg->id_moncap; ?>" class="modal  fade" role="dialog">
                            <div class="modal-dialog modal-lg">
                              <!-- Modal content-->
                              <div class="modal-content">
                                <div class="modal-header">
                                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                                  <h4 class="modal-title"><?php echo $mkg->output; ?></h4>
                                </div>
                                <div class="box">
                                  <div class="box-info">
                                    <img style="float:middle; width:800px !important; height:600px !important; margin: 5px 10px 5px 0px" src=" <?php echo base_url('files/moncap/');?>/<?php  echo $mkg->link_img;  ?> ">
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                      </tr>
                    <?php  $no++; }?>
                  </table>
              </div>
            </div>

            <!-- <div class="box-footer with-border">
                <p class="small" style="margin-bottom:0px">
                    Catatan : <br>
                    Monitoring Capaian Bidang ...
                </p>
            </div> -->
        </div>

        <div class="box box-widget">
            <div class="box-header with-border">
                <h3 class="box-title"><i class="fa fa-bar-chart"></i> Materi Inisiatif </3><span class="text-primary">Bidang <?php  echo $this->uri->segment(3); ?></span>
            </div>
            <!-- /.box-header -->
            <div class="box-body with-border">
                <div class="box-body table-responsive no-padding">
                  <table class="table table-striped">
                      <tr style="background: #00acd6; color: #fff;">
                      <th style="width:10px" hidden-xs hidden-sm>No</th>
                      <th>Materi</th>
                      <th hidden-xs hidden-sm>Fokus TKDJA</th>
                      <th>Fokus DJA</th>
                      <th>Catatan</th>
                    </tr>

                  <?php $no=1; foreach($matin as $mt){ ?>
                    <tr>
                      <td hidden-xs hidden-sm><?php echo $no; ?></td>
                      <td><?php echo $mt->output; ?></td>
                      <td hidden-xs hidden-sm>

                          <i class="fa fa-clock-o text-green"></i> <?php echo $this->fc->idtgl($mt->tgl_tkda_a); ?><br>
                          <i class="fa fa-clock-o text-red"></i> <?php echo $this->fc->idtgl($mt->tgl_tkda_z); ?>
                        </td>
                      <td>
                          <i class="fa fa-clock-o text-green"></i> <?php echo $this->fc->idtgl($mt->tgl_da_a); ?><br>
                          <i class="fa fa-clock-o text-red"></i> <?php echo $this->fc->idtgl($mt->tgl_da_z); ?>
                        </td>
                      <td><?php echo $mt->catatan; ?></td>
                    </tr>
                  <?php  $no++; }?>

                  </table>
              </div>
            </div>
            <div class="box-footer with-border">
                <p class="small text-muted pull-right hidden-xs" style="margin-bottom:0px">
                    Keterangan :
                    <i class="fa fa-clock-o text-green"></i> Waktu Mulai &nbsp;&nbsp;
                    <i class="fa fa-clock-o text-red"></i> Waktu Selesai
                </p>
                <!-- <p class="small" style="margin-bottom:0px">
                    Catatan : <br>
                    Materi inisiatif ...
                </p> -->
            </div>

            <!-- comment  -->
            <!-- <div class="box-footer box-comments">
                <div class="box-comment" style="padding-bottom:0px">
                    <img class="img-circle img-sm img-bordered-dsw" src="files/profiles/user1.jpg" alt="User Image">
                    <div class="comment-text">
                        <span class="username">
                            Hardi Lubis
                            <span class="text-muted pull-right">Kamis, 26 Nov 2015 8:03 PM</span>
                        </span>
                        Materi inisiatif perlu diperinci lebih lanjut.
                    </div>
                </div>
            </div> -->

            <!-- <div class="box-footer">
                <form action="#" method="post">
                    <?php
                        $foto_profile="files/profiles/_noprofile.png";
                        if (file_exists("files/profiles/".$this->session->userdata('nip').".gif")) {$foto_profile =  "files/profiles/".$this->session->userdata('nip').".gif";}
                    ?>
                    <img src="<?php echo site_url($foto_profile); ?>" class="img-responsive img-circle img-sm img-bordered-dsw" />
                    <div class="img-push">
                        <input type="text" class="form-control input-sm" placeholder="Tanggapan ...">
                    </div>
                </form>
            </div> -->
        </div>
    </div>

    <div class="col-md-3">
        <div class="box box-widget">
            <div class="box-header with-border">
                <h3 class="box-title"><i class="fa fa-sitemap"></i> Bidang <?php echo $this->uri->segment(3); ?></h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <i class="fa fa-trophy margin-r-5"></i> Initiative Champion
                <p class="text-muted">
                    <?php echo $bidang[0]->inisiatif;?>
                </p>
                <hr style="margin-top:5px">
                <i class="fa fa-users margin-r-5"></i> Anggota
                <br>
                    <img class="img-responsive pad" src="<?php echo base_url("files/moncap/bidang".$bidang[0]->id_bidang.".jpg") ?>" style="padding:10px 0px">
                    <p class="small text-muted">
                        <?php
                        $first = 1;
                        foreach ($anggota as $row) {
                          echo $row['nmuser'] ;
                          if ($first == 1) echo '<i class="fa fa-star-o text-yellow"></i>';
                          echo "<br>";
                          $first++;
                        } ?>
                    </p>
                    <hr style="margin-top:5px">
                <i class="fa fa-pencil margin-r-5"></i> Note
                    <div class="text-muted">
                        <p class="text-muted">
                            <?php echo $bidang[0]->Catatan;?>
                        </p>
                    </div>
            </div>
        </div>
    </div>
</div>
