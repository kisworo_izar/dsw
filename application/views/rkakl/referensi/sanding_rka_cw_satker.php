<link href="<?=base_url('assets/plugins/select2/select2.css');?>" rel="stylesheet" type="text/css" />
<script src="<?=base_url('assets/plugins/select2/select2.full.min.js'); ?>" type="text/javascript"></script>

<div class="row">
  <div class="col-md-12">
    <div class="box box-widget">
      <div class="box-header with-border" style="padding: 7px 10px 3px 10px">
        <div class="col-md-2" style="padding-left:0px">
          <div class="form-group">
            <label class="col-sm-3" style="padding:7px 0px">Tahun </label>
            <div class="col-sm-9">
              <select class="js-ta form-control" name="">
                <option value="option" selected="selected">2017</option>
              </select>
            </div>
          </div>
        </div>
        <div class="col-md-9" style="padding:0px">
           <div class="form-group">
             <label class="col-sm-2" style="padding:7px 0px; text-align:right">Cari </label>
             <div class="col-sm-6">
               <input id="cari" onkeyup="myCari()" class="form-control" placeholder="Pencarian Kode dan/atau Nomenklatur Satker">
            </div>
             <div class="col-sm-4">
                 <div class="col-sm-12" style="padding-top:7px">
                     <input id="chkS" onclick="tampil()" class="checkbox-custom" type="checkbox" value="S">
                        <label for="chkS" class="small text-black checkbox-custom-label">Sama&nbsp;&nbsp;</label>
                     <input id="chkK" onclick="tampil()" class="checkbox-custom" type="checkbox" checked value="K">
                        <label for="chkK" class="small text-red checkbox-custom-label">Kode&nbsp;&nbsp;</label>
                     <input id="chkU" onclick="tampil()" class="checkbox-custom" type="checkbox" checked value="U">
                        <label for="chkU" class="small checkbox-custom-label text-orange">Uraian&nbsp;&nbsp;</label>
                     <input id="chkD" onclick="tampil()" class="checkbox-custom" type="checkbox" checked value="D">
                        <label for="chkD" class="small checkbox-custom-label text-blue">Data&nbsp;&nbsp;</label>
                 </div>
            </div>
        </div>
        </div>
        <div class="col-md-1" style="padding:0px">
          <div class="form-group  pull-right" style="padding:5px 0px; margin:0px">
            <div class="col-sm-12" style="padding:5px 10px; background:#3D81A5; color:white">
               Update
            </div>
          </div>
        </div>
      </div>

      <div class="box-body">
        <section class="">
          <div class="container" style="height:650px">
            <table id="iGrid" class="table table-hover table-bordered">
              <thead>
                <tr>
                  <th><div class="thd" style="width:100%;background:#E7E9EE;border-right:solid 1px #B1B9C1">Kode</div></th>
                  <th><div class="thd">Uraian</div></th>
                  <th colspan="7"><div class="thd">Server Ref RKA-KL</div></th>
                  <th colspan="7"><div class="thd">Server Custom Web</div></th>
                  <th><div class="thd">&nbsp;</div></th>
                  <th><div class="thd">&nbsp;v</div></th>
                </tr>
              </thead>

              <tbody id="myTable">
                 <tr class="small"><td style="padding:0px; margin:0px;font-size:80%">&nbsp;</td></tr>
               <?php if ($rsatker) {
                  foreach ($rsatker as $row) {
                     switch ($row['ruh']) {
                     case 'D':
                       $xtr = ' style="background:#ecfdfe"';
                       $xtd = '<td class="text-center text-bold bg-blue">D</td>';
                       break;
                     case 'U':
                       $xtr = ' style="background:#fff5ed"';
                       $xtd = '<td class="text-center text-bold bg-orange">U</td>';
                       break;
                     case 'K':
                       $xtr = ' style="background:#fad2d2"';
                       $xtd = '<td class="text-center text-bold bg-red">K</td>';
                       break;
                     default:
                       $xtr = ''; $xtd = '<td>&nbsp;</td>';
                       break; } ?>
                  <tr <?= $xtr ?> class="cls_<?= $row['ruh'] ?>" >
                     <td><?= $row['kode']; ?></td>
                     <td style="border-right:solid 1px #B1B9C1"><?= $row['uraian']; ?></td>
                     <td><?= $row['rka_kddept']; ?></td>
                     <td><?= $row['rka_kdunit']; ?></td>
                     <td><?= $row['rka_kdlokasi']; ?></td>
                     <td><?= $row['rka_kdkabkota']; ?></td>
                     <td><?= $row['rka_kddekon']; ?></td>
                     <td><?= $row['rka_kdpusda']; ?></td>
                     <td style="border-right:solid 1px #B1B9C1"><?= $row['rka_kdkppn']; ?></td>
                     <td><?= $row['cw_kddept']; ?></td>
                     <td><?= $row['cw_kdunit']; ?></td>
                     <td><?= $row['cw_kdlokasi']; ?></td>
                     <td><?= $row['cw_kdkabkota']; ?></td>
                     <td><?= $row['cw_kddekon']; ?></td>
                     <td><?= $row['cw_kdpusda']; ?></td>
                     <td style="border-right:solid 1px #B1B9C1"><?= $row['cw_kdkppn']; ?></td>
                     <?= $xtd ?>
                     <td><input type="checkbox" <?php if ($row['ruh']!=='K') echo "disabled"; ?>></td>
                  </tr>
                  <?php } }  else { ?>
                     <tr><td colspan="6" class="text-danger">Data Kegiatan tidak ada</td></tr>
                  <?php } ?>
               </tbody>
            </table>
          </div>
        </section>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  $(document).ready(function() {
    $(".js-ta").select2({
      minimumResultsForSearch: 5
    });
  });

  function tampil() {
     var vS = $('#chkS').is(':checked');
     var vU = $('#chkU').is(':checked');
     var vD = $('#chkD').is(':checked');
     var vK = $('#chkK').is(':checked');
     if (vS) $('.cls_S').show(); else $('.cls_S').hide();
     if (vU) $('.cls_U').show(); else $('.cls_U').hide();
     if (vD) $('.cls_D').show(); else $('.cls_D').hide();
     if (vK) $('.cls_K').show(); else $('.cls_K').hide();
  }

  function myCari() {
  var input, filter, table, tr, td, i;
  input = document.getElementById("cari");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");

  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[1];
    if (td) {
      if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }
  }
}
</script>

<style media="screen">
    td a {display:block;width:100%; color: red}
    A:link,A:visited,A:active,A:hover {text-decoration: none; color: white;}
    .table-bordered > thead > tr > th,
    .table-bordered > thead > tr > td {border-bottom: 2px solid #E7E9EE;}
    .table-bordered > tbody > tr > td {padding:3px}
    .dt {border:solid 1px #B1B9C1;padding:0px 5px;}
    .hiddenRow {padding: 0 !important;}
    .info{border-top:solid 1px #E7E9EE;padding:0px;margin:0px}
    .thd{border-top:solid 1px #B1B9C1;border-bottom:solid 1px #B1B9C1;border-left:solid 1px #B1B9C1;margin-left:-8px;padding-left:2px}
    section {position: relative;}
    section.positioned {position: absolute;top:10px;left:10px;}
    .container {overflow-y: auto;padding: 0px;padding-top: 0px;width: 100%}
    table {border-spacing: 1;width:100%;}
    th div{position: absolute;background #E7E9EE;color:black;border-bottom: solid #E7E9EE 1px;border-top: solid 1px #E7E9EE;
        padding: 6px 0px;top: 0;line-height: normal;}
    .checkbox-custom, .radio-custom{opacity: 0;position: absolute;}
    .checkbox-custom, .checkbox-custom-label, .radio-custom, .radio-custom-label{
        display: inline-block;vertical-align: middle;margin: 0px;cursor: pointer;}
    .checkbox-custom-label, .radio-custom-label {position: relative;}
    .checkbox-custom + .checkbox-custom-label:before, .radio-custom + .radio-custom-label:before{
        content: '';background: #fff;border: 1px solid #DFDFDF;display: inline-block;vertical-align: middle;
        width: 15px;height: 15px;padding: 2px;margin-right: 3px;text-align: center;}
    .checkbox-custom:checked + .checkbox-custom-label:before{background: #3C8DBC;}
    .checkbox-custom:focus + .checkbox-custom-label, .radio-custom:focus + .radio-custom-label {outline: 0px solid #ddd;}
</style>
