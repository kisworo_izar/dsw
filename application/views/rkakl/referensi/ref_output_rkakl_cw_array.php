<link href="<?=base_url('assets/plugins/select2/select2.css');?>" rel="stylesheet" type="text/css" />
<script src="<?=base_url('assets/plugins/select2/select2.full.min.js'); ?>" type="text/javascript"></script>


<div class="row">
  <div class="col-md-12">
    <div class="box box-widget">
         
      <div class="box-header with-border" style="padding: 7px 10px 3px 10px">
        <div class="col-md-2" style="padding-left:0px">
          <div class="form-group">
            <label class="col-sm-3" style="padding:7px 0px">Tahun </label>
            <div class="col-sm-9">
              <select class="js-ta form-control" name="">
                <option value="option" selected="selected">2017</option>
              </select>
            </div>
          </div>
        </div>
        <div class="col-md-8" style="padding-left:0px">
          <div class="form-group">
            <label class="col-sm-1" style="padding:7px 0px">KL-Unit </label>
            <div class="col-sm-11">

               <select class="js-ta form-control" name="">
                  <option value="">&nbsp;&nbsp;&nbsp;SELURUH KEMENTERIAN - UNIT ESELON I</option>
                  <?php
                  $hit = 0;
                  foreach ($depuni as $row) {
                   if ( $row['level'] == 1) {
                       if ( $hit!=0) { echo '</optgroup>'; $hit++; }  // Clossing Group
                       echo '<optgroup label="'. $row['kode'].' '.substr($row['uraian'],0,150) .'">';
                   }
                   if ( $row['level'] == 2) {
                       $selc=''; if ($row['kode']==$kode) $selc='selected';
                       echo '<option value="'. $row['kode'] .'" '. $selc .'> &nbsp;'. $row['kode'].' '.substr($row['uraian'],0,150) .'</option>';
                   }
                  } ?>
               </select>

            </div>
          </div>
        </div>
        <div class="col-md-2" style="padding:0px">
          <div class="form-group  pull-right" style="padding:7px 0px; margin:0px">
            <div class="col-sm-12" style="padding-top:2px;padding-right:0px">
               <i class="fa fa-file-pdf-o"></i>&nbsp;&nbsp;
               <i class="fa fa-file-excel-o"></i>&nbsp;&nbsp;
               <i class="fa fa-print"></i>
            </div>
          </div>
        </div>
      </div>

      <div class="box-header with-border" style="padding:5px 10px">
        <div class="col-md-4 text-bold" style="padding-left:0px; color:#3A80A7">Server RKAKL</div>

        <div class="col-md-4 text-bold" style="padding-left:0px;padding-right:0px;color:#3A80A7">
           <div class="col-sm-12">
              <div class="text-center">
                 <input id="chk-F00" class="checkbox-custom" name="chk-F00" type="checkbox">
                 <label for="chk-F00" class="small text-black checkbox-custom-label" style="margin-left:-17px">Show All&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                 <input id="chk-F01" class="checkbox-custom" name="chk-F01" type="checkbox" checked>
                 <label for="chk-F01" class="small checkbox-custom-label text-success" style="margin-left:-17px">Insert&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                 <input id="chk-F02" class="checkbox-custom" name="chk-F02" type="checkbox" checked>
                 <label for="chk-F02" class="small checkbox-custom-label text-orange" style="margin-left:-17px">Update&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                 <input id="chk-F03" class="checkbox-custom" name="chk-F03" type="checkbox">
                 <label for="chk-F03" class="small checkbox-custom-label text-red" style="margin-left:-17px">Delete</label>
              </div>
           </div>
        </div>
        <div class="col-md-4 text-bold pull-right" style="padding:0px; color:#3A80A7">
           <div class="pull-right">Server CW</div>
        </div>
      </div>

      <div class="box-body">
        <section class="">
          <div class="container" style="height:650px">
            
            <table id="iGrid" class="table table-hover table-bordered">
   					  <thead>
      					<tr>
                  <th style="width:7%;padding:0px"><div class="thd1" style="border-left:solid 1px #B1B9C1">Kode</div></th>
                  <th style="padding-left:0px;padding-bottom:1px">&nbsp;
                     <div class="thd">Nomenklatur RKAKL</div></th>
                  <th style="width:10%;padding:0px"><div class="thd2" style="border-left:solid 1px #B1B9C1">Satuan</div></th>
                  <th style="width:7%;padding:0px"><div class="thd1" style="border-left:solid 1px #B1B9C1">Kode</div></th>
                  <th style="padding-left:0px;padding-bottom:1px">&nbsp;
      							<div class="thd">Nomenklatur CW</div>
                  </th>
                  <th style="width:10%;padding:0px"><div class="thd2" style="border-left:solid 1px #B1B9C1">Satuan</div></th>
                  <th style="width:20px;padding:0px"><div class="thd3" style="border-left:solid 1px #B1B9C1">*</div></th>
      					</tr>
   					  </thead>

   				    <tbody>
                <?php if ($giatout) {
                  foreach ($giatout as $row) {
                    if ($row['lvl']=='1') { ?>
                      <tr class="text-bold" style="">
                        <td style="border-bottom:solid 1px #B1B9C1"><?php echo $row['rka_kode']; ?></td>
                        <td style="border-bottom:solid 1px #B1B9C1" colspan="6"><?php echo $row['rka_uraian']; ?></td>
                      </tr>
                          
                    <?php } else {
                      switch ($row['ruh']) {
                      case 'i':
                         $xtr = ' style="background:#e7faf1"';
                         $xtd = '<td class="text-center text-bold bg-green">I</td>';
                         break;
                      case 'u':
                         $xtr = ' style="background:#fff5ed"';
                         $xtd = '<td class="text-center text-bold bg-orange">U</td>';
                         break;
                      case 'd':
                         $xtr = ' style="background:#fad2d2"';
                         $xtd = '<td class="text-center text-bold bg-red">D</td>';
                         break;
                      default:
                         $xtr = ''; $xtd = '<td>&nbsp;</td>';
                         break;
                    }?>
                             
                      <tr <?php echo $xtr ?>>
                        <td><?php echo $row['rka_kode']; ?></td>
                        <td><?php echo $row['rka_uraian']; ?></td>
                        <td><?php echo $row['rka_sat']; ?></td>
                        <td><?php echo $row['cw_kode']; ?></td>
                        <td><?php echo $row['cw_uraian']; ?></td>
                        <td><?php echo $row['cw_sat']; ?></td>
                        <?php echo $xtd ?>
                      </tr>
                  
                <?php } } } else { ?>
                      <tr><td colspan="6" class="text-danger">Data Output tidak ada</td></tr>
                <?php } ?>
   					  </tbody>
   				  </table>

          </div>
        </section>
			</div>

    </div>
  </div>
</div>

<script type="text/javascript">
  $(document).ready(function() {
    $(".js-ta").select2({
      minimumResultsForSearch:5
    });
  });
</script>

<style media="screen">
    td a {display:block;width:100%; color: red}
    A:link,A:visited,A:active,A:hover {text-decoration: none; color: white;}
    .table-bordered > thead > tr > th,
    .table-bordered > thead > tr > td {border-bottom: 2px solid #E7E9EE;}
    .table-bordered > tbody > tr > td {padding:3px}
    .dt {border:solid 1px #B1B9C1;padding:0px 5px;}
    .hiddenRow {padding: 0 !important;}
    .info{border-top:solid 1px #E7E9EE;padding:0px;margin:0px}
    .info1{border-top:0px;padding:0px;margin:0px}
    .x { display: inline-block; width: 110px; }
    .p3 {padding:3px}
    .thd, .thd1, .thd2, .thd3 {border-right:solid 1px #B1B9C1;border-bottom:solid 1px #B1B9C1; border-top:solid 1px #B1B9C1; padding-left:7px; background: #E7E9EE}
   .thd {width:40%; padding-left: 3px}
   .thd1 {width:7%; padding-left: 3px}
   .thd2 {width:10%; padding-left: 3px}
   .thd3 {width:20px}
    section {position: relative;}
    section.positioned {position: absolute;top:10px;left:10px;}
    .container {overflow-y: auto;padding: 0px;padding-top: 0px;width: 100%}
    table {border-spacing: 1;width:100%;}
    th div{position: absolute;background #E7E9EE;color:black;border-bottom: solid #E7E9EE 1px;border-top: solid 1px #E7E9EE;
        padding: 6px 0px;top: 0;line-height: normal;}
    .calculated-width {width: -moz-calc(100% - 1px)/2;width: -webkit-calc(100% - 1px)/2;width: calc(100% -1px)/2;background: #E7E9EE}​
    .checkbox-custom, .radio-custom{opacity: 0;position: absolute;}
    .checkbox-custom, .checkbox-custom-label, .radio-custom, .radio-custom-label{
        display: inline-block;vertical-align: middle;margin: 0px;cursor: pointer;}
    .checkbox-custom-label, .radio-custom-label {position: relative;}
    .checkbox-custom + .checkbox-custom-label:before, .radio-custom + .radio-custom-label:before{
        content: '';background: #fff;border: 1px solid #DFDFDF;display: inline-block;vertical-align: middle;
        width: 15px;height: 15px;padding: 2px;margin-right: 3px;text-align: center;}
    .checkbox-custom:checked + .checkbox-custom-label:before{background: #3C8DBC;}
    .checkbox-custom:focus + .checkbox-custom-label, .radio-custom:focus + .radio-custom-label {outline: 0px solid #ddd;}
</style>