<!-- Google Fonts -->
<link href="https://fonts.googleapis.com/css?family=Roboto:400,700&amp;subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
<link href="<?=base_url();?>assets/plugins/materialicons/materialicons.css" rel="stylesheet" type="text/css">
<!-- bootstrapValidator -->
<link rel="stylesheet" href="<?=base_url();?>assets/plugins/bootstrapvalidator/css/bootstrapValidator.min.css"/>
<!-- SweetAlert2 -->
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
<!-- Waves Effect Css -->
<link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/plugins/node-waves/waves.css">
<!-- Bootstrap DatePicker Css -->
<link href="<?=base_url();?>assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.css" rel="stylesheet" />
<!-- Bootstrap Select Css -->
<link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/plugins/bootstrap-select/css/bootstrap-select.css">
<!-- Dropify -->
<link href="<?=base_url();?>assets/plugins/dropify/css/dropify.min.css" rel="stylesheet">
<!-- AdminBSB Custom Css -->
<link href="<?=base_url();?>assets/plugins/adminbsb/style.css" rel="stylesheet">

<style type="text/css">
  form .dropify-wrapper p {
    text-align: center;
  }

  .swal2-radio [type="radio"]:not(:checked),
  .swal2-radio [type="radio"]:checked {
    position: unset;
    opacity: 1;
  }

  .form-group .form-line.focused .form-label {
    top: -15px;
  }

  .bs-searchbox .form-control {
    width: 88%;
  }

  .capitalise {
    text-transform:capitalize;
  }
</style>

<div class="row">
  <div class="col-md-12">
    <div class="card">
      <div class="header">
        <button type="button" class="btn btn-primary btn-lg waves-effect" data-toggle="modal" data-target="#dosirModal" data-action="add">
          <i class="material-icons">add</i>
        </button>
        <button type="button" class="btn btn-primary btn-lg waves-effect" data-toggle="modal" data-target="#dosirFilterModal" data-action="add">
          <i class="material-icons">filter_list</i>
        </button>
        <div class="col-xs-6 col-sm-4 pull-right">
          <div class="input-group pull-right">
            <div class="form-line">
              <input type="text" class="form-control" placeholder="Cari..." id="searchDosir">
            </div>
            <span class="input-group-addon">
              <i class="material-icons">search</i>
            </span>
          </div>
        </div>
        <!-- <a href="<?=base_url();?>dosir/group" class="btn btn-info btn-lg pull-right waves-effect" role="button">KATEGORI</a> -->
      </div>
    </div>
    <div class="grid capitalise" id="dosirList">

    </div>
  </div>
</div>

<div class="modal fade" id="dosirModal" tabindex="-1" role="dialog" style="display: none;">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="card">
        <div class="header">
          <div class="row clearfix">
            <div class="col-xs-12 col-sm-6">
              <h2 id="modalTitle"></h2>
            </div>
          </div>
        </div>
        <div class="body">
          <form id="dosirForm">
            <br>
            <input name="id" type="text" id="id" hidden>
            <div class="row clearfix">
              <div class="col-sm-4">
                <div class="form-group form-float">
                  <div class="form-line">
                    <select name="group_id" class="form-control show-tick capitalise" data-live-search="true"  title="Pilih Kategori" id="group_id">

                    </select>
                    <label class="form-label">Kategori</label>
                  </div>
                </div>
              </div>
              <div class="col-sm-4">
                <div class="form-group form-float">
                  <div class="form-line">
                    <input name="title" type="text" class="form-control" id="title">
                    <label class="form-label">Judul Dokumen</label>
                  </div>
                </div>
              </div>
              <div class="col-sm-4">
                <div class="form-group form-float">
                  <div class="form-line">
                    <input name="date" type="text" class="form-control" id="date">
                    <label class="form-label">Tanggal Dokumen</label>
                  </div>
                </div>
              </div>
            </div>
            <div class="row clearfix">
              <div class="col-sm-12">
                <div class="form-group form-float">
                  <div class="form-line">
                    <input name="dokumen" type="file" class="dropify" data-max-file-size-preview="2M" id="dokumen" />
                  </div>
                </div>
              </div>
            </div>
            <button type="submit" class="btn btn-primary m-t-15 waves-effect" id="saveDosir">SIMPAN</button>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="dosirFilterModal" tabindex="-1" role="dialog" style="display: none;">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="card">
        <div class="header">
          <div class="row clearfix">
            <div class="col-xs-12 col-sm-6">
              <h2 id="modalTitle">Filter Daftar Dosir</h2>
            </div>
          </div>
        </div>
        <div class="body">
          <form id="dosirFilterForm">
            <br>
            <div class="row clearfix">
              <div class="col-sm-6">
                <div class="form-group form-float">
                  <div class="form-line">
                    <select name="group_id" class="form-control show-tick selectpicker capitalise" data-live-search="true" data-size="5" title="Pilih Kategori" >
                      <option value=" ">Semua</option>

                    </select>
                    <label class="form-label">Kategori</label>
                  </div>
                </div>
              </div>
              <div class="col-sm-6">
                <div class="form-group form-float">
                  <div class="form-line">
                    <select name="approval" class="form-control show-tick capitalise" title="Pilih Status" >
                      <option value=" ">Semua</option>
                      <option value="0">Menunggu Penetapan</option>
                      <option value="1">Ditetapkan</option>
                      <option value="2">Ditolak</option>
                    </select>
                    <label class="form-label">Status</label>
                  </div>
                </div>
              </div>
            </div>
            <div class="row clearfix">
              <div class="col-sm-6">
                <div class="form-group form-float">
                  <div class="form-line">
                    <input name="startdate" type="text" class="form-control datepicker">
                    <label class="form-label">Dari Tanggal</label>
                  </div>
                </div>
              </div>
              <div class="col-sm-6">
                <div class="form-group form-float">
                  <div class="form-line">
                    <input name="enddate" type="text" class="form-control datepicker">
                    <label class="form-label">Sampai Tanggal</label>
                  </div>
                </div>
              </div>
            </div>
            <button type="button" class="btn btn-primary m-t-15 waves-effect" id="filterDosir">FILTER</button>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- imagesloaded -->
<script src="<?=base_url();?>assets/plugins/imagesloaded/imagesloaded.pkgd.min.js"></script>
<!-- Masonry -->
<script src="<?=base_url();?>assets/plugins/masonry/masonry.pkgd.min.js"></script>
<!-- bootstrapValidator -->
<script type="text/javascript" src="<?=base_url();?>assets/plugins/bootstrapvalidator/js/bootstrapValidator.js"></script>
<script type="text/javascript" src="<?=base_url();?>assets/plugins/bootstrapvalidator/js/language/id_ID.js"></script>
<!-- jquery.serializeObject -->
<script type="text/javascript" src="<?=base_url();?>assets/plugins/jQuery.serializeObject/jquery.serializeObject.min.js"></script>
<!-- Bootstrap Select Css -->
<script type="text/javascript" src="<?=base_url();?>assets/plugins/bootstrap-select/js/bootstrap-select.js"></script>
<!-- Waves Effect Plugin Js -->
<script type="text/javascript" src="<?=base_url();?>assets/plugins/node-waves/waves.js"></script>
<!-- Bootstrap Datepicker Plugin Js -->
<script src="<?=base_url();?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<!-- Dropify -->
<script src="<?=base_url();?>assets/plugins/dropify/js/dropify.min.js"></script>
<!-- AdminBSB Custom Js -->
<script src="<?=base_url();?>assets/plugins/adminbsb/admin.js"></script>
<!-- <script src="<?=base_url();?>assets/plugins/pmd-admin/js/propeller.min.js"></script> -->

<!-- Select2 js-->
<!-- <script type="text/javascript" src="<?=base_url();?>assets/plugins/pmd-admin/components/select2/js/select2.full.js"></script> -->
<script type="text/javascript">
  $(function(){

    /*Dosir Form Validator*/
    $('#dosirForm').bootstrapValidator({
      excluded: [':disabled', ':hidden'],
      feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
        nip: {
          validators: {
            notEmpty: {}
          }
        },
        title: {
          validators: {
            notEmpty: {}
          }
        },
        date: {
          validators: {
            notEmpty: {},
            date: { format: 'YYYY-MM-DD' }
          }
        },
        group_id: {
          validators: {
            notEmpty: {}
          }
        },
        dokumen: {
          validators: {
            notEmpty: {},
            file: {
              extension: 'pdf',
              type: 'application/pdf',
              maxSize: 2097152
            }
          }
        }
      }
    })      
    .on('success.form.bv', function(e) {
      e.preventDefault();
      $('#dosirModal').modal('toggle');

      /*Upload file*/
      var formData = new FormData();
      formData.append('file', $('#dokumen')[0].files[0]);
      formData.append('nip', '<?= $this->session->userdata('nip') ?>');

      $.ajax({
        url: "<?= $this->config->item('api_server') ?>dosir/pegawai/upload",
        data: formData,
        type: 'POST',
        processData: false,
        contentType: false,
        success: function(result) {
          if (result.status == 200) {

            /*CREATE & UPDATE Dosir*/
            var data = $('#dosirForm').serializeObject(),
            type = ($('#saveDosir').attr('data-action') == 'add') ? 'POST' : 'PUT';

            data.filename = result.values.file;
            data.thumbnailname = result.values.thumbnail;
            data.updatedby = "<?= $this->session->userdata('nip') ?>";
            data.nip = '<?= $this->session->userdata('nip') ?>';

            $.ajax({
              url: "<?= $this->config->item('api_server') ?>dosir/pegawai",
              data: data,
              type: type,
              crossDomain: true,
              dataType: 'json',
              success: function(result) {
                if (result.status == 200) {
                  Swal.fire({ title: 'Dosir pegawai tersimpan', type: 'success' })
                  dosir();
                } else {
                  Swal.fire({ title: 'Dosir pegawai gagal tersimpan!', type: 'error' })
                }
              },
              error: function(result) {
                Swal.fire({ title: 'Dosir pegawai error!', type: 'error' })
              }
            });
          } else {
            Swal.fire({ title: 'Dokumen gagal tersimpan!', type: 'error' })
          }
        },
        error: function(result) {
          Swal.fire({ title: 'Dokumen error!', type: 'error' })
        }
      });
    })

    /*Dosir Form*/
    $('#dosirModal').on('shown.bs.modal', function (event) {
      /*clear form*/
      var action = $(event.relatedTarget).data('action');

      if (action == 'edit') {
        $('#dosirForm').bootstrapValidator('resetForm', true);

        var dosir = $(event.relatedTarget).data('dosir');

        $('#modalTitle').text('Edit Dosir #' + dosir.id);

        $('#id').val(dosir.id);
        $('#title').val(dosir.title);
        $('#title').parent().addClass('focused');
        $('#date').datepicker('setDate', new Date(dosir.date));
        $('#group_id').selectpicker('val', dosir.group_id);

        var defaultFile = (dosir.filename) ? '<?=base_url();?>files/upload_milea/' + dosir.nip + '/' + dosir.filename : '';
        var drEvent = $('#dokumen').dropify({ defaultFile: defaultFile });

        drEvent.on('dropify.beforeClear', function(event, element){
          $('#dosirForm').bootstrapValidator('revalidateField', 'dokumen');
        });

        drEvent = drEvent.data('dropify');
        drEvent.resetPreview();
        drEvent.clearElement();
        drEvent.settings.defaultFile = defaultFile;
        drEvent.destroy();
        drEvent.init();

        if (dosir.filename) $('#dosirForm').bootstrapValidator('updateStatus', 'dokumen', 'VALID');
      } else {
        $('#modalTitle').text('Tambah Dosir');

        var drEvent = $('#dokumen').dropify();
        drEvent = drEvent.data('dropify');
        drEvent.resetPreview();
        drEvent.clearElement();
        $('#dosirForm').bootstrapValidator('updateStatus', 'dokumen', 'NOT_VALIDATED');
      }

      $('#saveDosir').attr('data-action', action);
    })

    /*READ Dosir*/
    dosir();

    /*FILTER Dosir*/
    $('#filterDosir').click(function(){
      $('#dosirFilterModal').modal('toggle');
      dosir();
    })

    /*SEARCH Dosir*/
    $('#searchDosir').keyup(function(e){
      console.log($('#searchDosir').val())
      $('#dosirList .grid-item').each(function() {
        if ($(this).text().toLowerCase().indexOf( $('#searchDosir').val().toLowerCase() ) < 0){
          $(this).hide()
        } else {
          $(this).show()
        }
      });
      var $grid = $('.grid').masonry();
      $grid.masonry('reloadItems');
      $grid.masonry('layout');
    })

    /*DELETE dosir*/
    $('#dosirList').on('click', 'a.deleteDosir', function() {
      Swal.fire({
        title: 'Hapus Dosir?',
        type: 'warning',
        confirmButtonText: 'Ya',
        showCancelButton: true,
        cancelButtonText: 'Tidak',
        focusCancel:true,
        reverseButtons: true,
        showLoaderOnConfirm: true,
        preConfirm: () => {}
      }).then((result) => {
        if (result.value) {
          $.ajax({
            url: "<?= $this->config->item('api_server') ?>dosir/pegawai",
            data: {id: $(this).attr('data-id')},
            type: 'DELETE',
            crossDomain: true,
            dataType: 'json',
            success: function(result) {
              if (result.status == 200) {
                Swal.fire({ title: 'Dosir pegawai dihapus!', type: 'success' })
                dosir();
              } else {
                Swal.fire({ title: 'Dosir pegawai gagal dihapus!', type: 'error' })
              }
            },
            error: function(result) { Swal.fire({ title: 'Dosir pegawai error!', type: 'error' }) }
          });
        }
      })
    });

    /*APPROVING dosir*/
    /*$('#dosirList').on('click', '.btn-approval', function() {
      Swal.fire({
        title: 'Tetapkan Dosir?',
        type: 'question',
        input: 'radio',
        inputOptions: {1: 'Ya, Tetapkan!', 2: 'Tolak'},
        inputValidator: (value) => {
          if (!value) {
            return 'Pilih salah satu!'
          }
        },
        confirmButtonText: 'Simpan',
        showCancelButton: true,
        cancelButtonText: 'Batal',
        focusCancel:true,
        reverseButtons: true,
        showLoaderOnConfirm: true,
        preConfirm: () => {}
      }).then((result) => {
        if (result.value) {
          console.log($(this).attr('data-id'))
          $.ajax({
            url: "<?= $this->config->item('api_server') ?>dosir/pegawai/approval",
            data: {id: $(this).attr('data-id'), approval: result.value, approvedby: "<?= $this->session->userdata('nip') ?>"},
            type: 'POST',
            dataType: 'json',
            success: function(result) {
              if (result.status == 200) {
                Swal.fire({ title: 'Dosir pegawai ditetapkan!', type: 'success' })
                dosir();
              } else {
                Swal.fire({ title: 'Dosir pegawai gagal ditetapkan!', type: 'error' })
              }
            },
            error: function(result) { Swal.fire({ title: 'Dosir pegawai error!', type: 'error' }) }
          });
        }
      })
    });*/

    /*Dosir Group Reference*/
    $.ajax({
      url: '<?= $this->config->item('api_server') ?>dosir/group',
      type: 'GET',
      crossDomain: true,
      dataType: 'json',
      success: function(result) {
        if (result.status == 200) {
          $('select[name="group_id"]').append(buildGroup(result.values)).selectpicker('refresh');
        } else {
          console.log('Gagal load data dosir group');
        }
      }
    })

    /*Bootstrap datepicker plugin*/
    $('#dosirFilterModal .datepicker').datepicker({
      autoclose: true,
      format: "yyyy-mm-dd",
      language: "id",
    }).on('changeDate', function(ev) {
      $(this).parent().addClass('focused');
    });

    $('#date').datepicker({
      autoclose: true,
      format: "yyyy-mm-dd",
      language: "id",
    })
    .on('changeDate', function(ev) {
      $(this).parent().addClass('focused');
      $('#dosirForm').bootstrapValidator('revalidateField', 'date');
    });

    /*Dropify*/
    $('.dropify').dropify({
      messages: {
        'default': 'Drag dan drop file/dokumen di sini atau klik',
        'replace': 'Drag dan drop atau klik untuk mengganti',
        'remove':  'Hapus',
        'error':   'Ooops, terjadi kesalahan.'
      }
    });

    /*Masonry*/
    $('.grid').masonry({percentPosition: true});

    /*Tooltip*/
    $('body').tooltip({selector: '[data-toggle="tooltip"]'});

    function dosir(){
      $.ajax({
        url: '<?= $this->config->item('api_server') ?>dosir/pegawai/?nip=<?= $this->session->userdata('nip') ?>&'+$('#dosirFilterForm').serialize(),
        type: 'GET',
        crossDomain: true,
        dataType: 'json',
        success: function(result) {
          if (result.status == 200) {
            var html = '';
            if (result.values.length < 1) {
              html = '<div class="alert alert-warning col-xs-12">Tidak ada Dosir yang ditampilkan</div>';
            }
            $.each(result.values, function(index, item){
              var filelink = (item.filename) ? '<?=base_url();?>' + 'files/upload_milea/' + item.nip + '/' + item.filename : '';
              var data_pdf_thumbnail_file = (item.filename) ? 'data-pdf-thumbnail-file="'+filelink+'"' : '';
              var src = (item.thumbnailname) ? '<?=base_url();?>' + 'files/upload_milea/' + item.nip + '/' + item.thumbnailname : '';
              var approval_icon, approval_tooltip, approval_class;
              var updatedby = (item.updatedby == '<?= $this->session->userdata('nip') ?>') ? item.updatedby_name : 'Bagian SDM'

              switch(item.approval){
                case 0:
                approval_icon     = 'gavel';
                approval_tooltip  = 'Menunggu penetapan oleh Bagian SDM';
                approval_class    = 'btn-warning';
                break;

                case 1:
                approval_icon     = 'check';
                approval_tooltip  = 'Ditetapkan oleh Bagian SDM - '+formatDate(item.approvedat, true);
                approval_class    = 'btn-success';
                break;

                case 2:
                approval_icon     = 'close';
                approval_tooltip  = 'Dietapkan oleh Bagian SDM - '+formatDate(item.approvedat, true);
                approval_class    = 'btn-danger';
                break;
              }
              
              html += '<div class="col-sm-6 col-md-4 grid-item">\
              <div class="thumbnail card">\
              <img '+data_pdf_thumbnail_file+' src="'+src+'">\
              <div class="caption">\
              <h4 style="word-wrap: break-word;">'+item.title+'</h4>\
              <p>\
              <span class="badge bg-cyan pull-right">'+item.pegawai_name+'</span>\
              <span class="badge bg-teal">'+formatDate(item.date)+'</span>\
              </p>\
              <p>\
              '+item.group_name+'\
              </p>\
              <p><small><span class="text-muted">Terakhir diubah oleh <a href="#">'+updatedby+'</a> - '+formatDate(item.updatedat, true)+'</span></small></p>\
              <p>\
              <div class="btn-group">\
              <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">\
              <i class="material-icons">menu</i>\
              </button>\
              <ul class="dropdown-menu">\
              <li><a href="#" class="waves-effect waves-block" data-toggle="modal" data-target="#dosirModal" data-action="edit" data-dosir=\''+JSON.stringify(item)+'\'>Edit</a></li>\
              <li><a href="'+filelink+'" target="_blank" class=" waves-effect waves-block">Download</a></li>\
              <li role="separator" class="divider"></li>\
              <li><a href="#" class="waves-effect waves-block deleteDosir" data-id='+item.id+'>Hapus</a></li>\
              </ul>\
              </div>\
              <button type="button" class="btn '+approval_class+' btn-circle waves-effect waves-circle waves-float pull-right" data-toggle="tooltip" data-placement="top" title="" data-original-title="'+approval_tooltip+'" data-id='+item.id+'>\
              <i class="material-icons">'+approval_icon+'</i>\
              </button>\
              </p>\
              </div>\
              </div>\
              </div>';
            });
            $('#dosirList').html(html);

            var $grid1 = $('.grid').imagesLoaded( function() {
              var $grid = $('.grid').masonry();
              $grid.masonry('reloadItems');
              $grid.masonry('layout');
            });
          } else {
            console.log('Gagal load data dosir');
          }
        },
        error: function(result) {
          console.log('Error load data dosir');
        }
      })
}

function buildGroup(items) {
  if (items.length == 0) { return ''; }

  var html = "";

  $.each(items, function(index, item){
    if (item.children) {
      html += '<optgroup label="'+item.name+'">';
      html += buildGroup(item.children);
      html += '</optgroup>';
    } else {
      html += '<option value='+item.id+'>'+item.name+'</option>';
    }
  })

  return html;
}
})
</script>

<script type="text/javascript">
  function formatDate(date, time){
    var options;
    if (time) {
      options = { year: 'numeric', month: 'short', day: 'numeric', hour: 'numeric', minute: 'numeric', second: 'numeric' };
    } else {
      options = { year: 'numeric', month: 'short', day: 'numeric' };
    }
    return new Date(date).toLocaleDateString('id-ID', options);
  }
</script>