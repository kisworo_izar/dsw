<div class="box-header with-border revHead">
    <h4 class="box-title" style="color:#FFF;padding-left:5px">3. PENDAPAT</h4>
</div>

<div class="form-group" style="padding-left:10px;margin-bottom:0px">
    <h5 class="col-sm-12 text-bold" style="color:#3C8DBC;padding-left:20px">A. DASAR HUKUM</h5>
</div>

<div class="form-group" style="margin-left:15px;margin-bottom:3px">
    <div class="col-sm-10" style="padding:0px">
        <textarea name="f3_dasarhukum" class="form-control" placeholder="Uraian dasar hukum penetapan revisi" rows="5"><?= $nd['nota']['f3_dasarhukum'] ?></textarea>
    </div>
</div>

<div class="form-group" style="padding-left:10px;margin-bottom:0px">
    <h5 class="col-sm-12 text-bold" style="color:#3C8DBC;padding-left:20px">B. PENETAPAN REVISI ANGGARAN</h5>
</div>

<div class="form-group jarak">
    <div class="col-sm-10" style="padding:5px 0px 0px 30px">
        <label>Berdasarkan hal-hal tersebut diatas maka: </label>
    </div>
    <div class="form-group jarak" style="padding:0px 15px 5px 30px;margin-bottom:0px">
        <input type="hidden" name="f4_usulan_rev">
        <?php
            $arr = array(
                'usulan revisi dapat ditetapkan',
                'usulan revisi tidak dapat ditetapkan',
                'usulan revisi dapat ditetapkan sebagian, dan sebagian ditolak'
            );
            for ($i=0; $i<count($arr); $i++) {
                if ($arr[$i] == $nd['nota']['f4_usulan_rev']) $chk='checked'; else $chk='';
                echo "
                <label class='col-sm-12'>
                    <div>
                        <input name='radio-rekomendasi' type='radio' id='radio-$i' class='radio-custom' $chk>
                        <label for='radio-$i' class='radio-custom-label'>". $arr[$i] ."</label>
                    </div>
                </label>";
            }
        ?>
    </div>
</div>


<div class="form-group" style="margin-left:15px;margin-bottom:3px">
    <div class="col-sm-10" style="padding:0px">
        <label>Catatan : </label>
        <textarea name="f3_ket_catatan" class="form-control" placeholder="Catatan penetapan revisi anggaran" rows="5"><?= $nd['nota']['f3_ket_catatan'] ?></textarea>
    </div>
</div>

<br>
