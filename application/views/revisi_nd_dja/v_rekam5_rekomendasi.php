
<div class="box-header with-border revHead">
    <h4 class="box-title" style="color:#FFF;padding-left:5px">4. REKOMENDASI</h4>
</div>
<div class="form-group jarak" style="margin-bottom:0px">
    <div class="col-sm-10" style="padding:10px 0px 0px 30px">
        <label>Dengan penetapan revisi anggaran dimaksud, DHP RKA-K/L menjadi sebagai berikut: </label>
    </div>
</div>

<div  class="col-sm-10" style="margin-left:30px"><span class="pull-right"><i>Ribuan rupiah &nbsp;</i></span></div>

<div ng-controller="ctrltabel" ng-init="rms=<?= $nd['nota']['pagu_rm_mula'] ?>; rmm=<?= $nd['nota']['pagu_rm_jadi'] ?>; pns=<?= $nd['nota']['pagu_pnbp_mula'] ?>; pnm=<?= $nd['nota']['pagu_pnbp_jadi'] ?>; phs=<?= $nd['nota']['pagu_phln_mula'] ?>; phm=<?= $nd['nota']['pagu_phln_jadi'] ?>; sbs=<?= $nd['nota']['pagu_sbsn_mula'] ?>; sbm=<?= $nd['nota']['pagu_sbsn_jadi'] ?>; pdptpjks=<?= $nd['nota']['pagu_pdpt_pajak_mula'] ?>; pdptpjkm=<?= $nd['nota']['pagu_pdpt_pajak_jadi'] ?>; pdptpnbps=<?= $nd['nota']['pagu_pdpt_pnbp_mula'] ?>; pdptpnbpm=<?= $nd['nota']['pagu_pdpt_pnbp_jadi'] ?>; hal4s=<?= $nd['nota']['pagu_hal_4_mula'] ?>; hal4m=<?= $nd['nota']['pagu_hal_4_jadi'] ?>" class="form-group col-sm-10" style="border:solid 1px #3C8DBC;margin:0px 0px 0px 15px;padding:0px">

    <div class="col-sm-12" style="padding:0px;">
        <div class="tjudul1 col-sm-3 line_r line_b">Uraian</div>
        <div class="tjudul1 col-sm-3 line_r line_b">Semula</div>
        <div class="tjudul1 col-sm-3 line_r line_b">Menjadi</div>
        <div class="tjudul1 col-sm-3 line_b">Selisih</div>
    </div>
    <div class="col-sm-12" style="padding:0px">
        <div class="tjudul2 col-sm-3 line_r" style="text-align:left;border-bottom:solid 1px #88BAD6">Pagu</div>
        <div class="tjudul2 col-sm-3 line_r line_b" style="padding-right:12px" id="">{{rms+pns+phs+sbs | number}}</div>
        <div class="tjudul2 col-sm-3 line_r line_b" style="padding-right:12px" id="">{{rmm+pnm+phm+sbm | number}}</div>
        <div class="tjudul2 col-sm-3 line_b" style="padding-right:12px" id="">{{(rmm+pnm+phm+sbm)-(rms+pns+phs+sbs) | number}}</div>
    </div>
    <div class="col-sm-12" style="padding:0px;border-bottom:solid 1px #88BAD6">
        <div class="tjudul3 col-sm-3 line_r" style="border-right:solid 1px white">Rincian Sumber Dana :</div>
        <div class="tjudul3 col-sm-3 line_r" style="border-right:solid 1px white">&nbsp;</div>
        <div class="tjudul3 col-sm-3 line_r" style="border-right:solid 1px white">&nbsp;</div>
        <div class="tjudul3 col-sm-3">&nbsp;</div>
    </div>
    <div class="col-sm-12" style="padding:0px">
        <div class="tjudul3 col-sm-3 line_r line_b2">RM</div>
        <div class="tdata col-sm-3 line_r">
            <input name="pagu_rm_mula" type="text" class="form-control rp_norm line_b2" ng-model="rms" ui-number-mask="0" min="numberWith2Decimals" style="background:#effbee">
        </div>
        <div class="tdata col-sm-3 line_r">
            <input name="pagu_rm_jadi" type="text" class="form-control rp_norm line_b2" ng-model="rmm" ui-number-mask="0" min="numberWith2Decimals" style="background:#effbee">
        </div>
        <div class="tdata col-sm-3 line_b2 selisih" id="">{{rmm-rms | number}}</div>
    </div>
    <div class="col-sm-12" style="padding:0px">
        <div class="tjudul3 col-sm-3 line_r line_b2">PNBP/BLU</div>
        <div class="tdata col-sm-3 line_r">
            <input name="pagu_pnbp_mula" type="text" class="form-control rp_norm line_b2" ng-model="pns" ui-number-mask="0" min="numberWith2Decimals" style="background:#effbee">
        </div>
        <div class="tdata col-sm-3 line_r">
            <input name="pagu_pnbp_jadi" type="text" class="form-control rp_norm line_b2" ng-model="pnm" ui-number-mask="0" min="numberWith2Decimals" style="background:#effbee">
        </div>
        <div class="tdata col-sm-3 line_b2 selisih" id="">{{pnm-pns | number}}</div>
    </div>
    <div class="col-sm-12" style="padding:0px">
        <div class="tjudul3 col-sm-3 line_r line_b2">PHLN/PHDN</div>
        <div class="tdata col-sm-3 line_r">
            <input name="pagu_phln_mula" type="text" class="form-control rp_norm line_b2" ng-model="phs" ui-number-mask="0" min="numberWith2Decimals" style="background:#effbee">
        </div>
        <div class="tdata col-sm-3 line_r">
            <input name="pagu_phln_jadi" type="text" class="form-control rp_norm line_b2" ng-model="phm" ui-number-mask="0" min="numberWith2Decimals" style="background:#effbee">
        </div>
        <div class="tdata col-sm-3 line_b2 selisih" id="">{{phm-phs | number}}</div>
    </div>
    <div class="col-sm-12" style="padding:0px">
        <div class="tjudul3 col-sm-3 line_r" style="border-bottom:solid 1px #88BAD6">SBSN-PBS</div>
        <div class="tdata col-sm-3 line_r">
            <input name="pagu_sbsn_mula" type="text" class="form-control rp_norm line_b" ng-model="sbs" ui-number-mask="0" min="numberWith2Decimals" style="background:#effbee">
        </div>
        <div class="tdata col-sm-3 line_r">
            <input name="pagu_sbsn_jadi" type="text" class="form-control rp_norm line_b" ng-model="sbm" ui-number-mask="0" min="numberWith2Decimals" style="background:#effbee">
        </div>
      <div class="tdata col-sm-3 line_b selisih" id="">{{sbm-sbs | number}}</div>
    </div>



    <div class="col-sm-12" style="padding:0px">
        <div class="tjudul2 col-sm-3 line_r" style="text-align:left;border-bottom:solid 1px #88BAD6">Target Pendapatan</div>
        <div class="tjudul2 col-sm-3 line_r line_b" style="padding-right:12px" id="">{{pdptpjks+pdptpnbps | number}}</div>
        <div class="tjudul2 col-sm-3 line_r line_b" style="padding-right:12px" id="">{{pdptpjkm+pdptpnbpm | number}}</div>
        <div class="tjudul2 col-sm-3 line_b" style="padding-right:12px" id="">{{(pdptpjkm+pdptpnbpm)-(pdptpjks+pdptpnbps) | number}}</div>
    </div>
    <div class="col-sm-12" style="padding:0px">
        <div class="tjudul3 col-sm-3 line_r line_b2">Perpajakan</div>
        <div class="tdata col-sm-3 line_r">
            <input name="pagu_pdpt_pajak_mula" type="text" class="form-control rp_norm line_b2" ng-model="pdptpjks" ui-number-mask="0" min="numberWith2Decimals" style="background:#effbee">
        </div>
        <div class="tdata col-sm-3 line_r">
            <input name="pagu_pdpt_pajak_jadi" type="text" class="form-control rp_norm line_b2" ng-model="pdptpjkm" ui-number-mask="0" min="numberWith2Decimals" style="background:#effbee">
        </div>
        <div class="tdata col-sm-3 line_b2 selisih" id="">{{pdptpjkm-pdptpjks | number}}</div>
    </div>
    <div class="col-sm-12" style="padding:0px">
        <div class="tjudul3 col-sm-3 line_r line_b2">PNBP</div>
        <div class="tdata col-sm-3 line_r">
            <input name="pagu_pdpt_pnbp_mula" type="text" class="form-control rp_norm line_b2" ng-model="pdptpnbps" ui-number-mask="0" min="numberWith2Decimals" style="background:#effbee">
        </div>
        <div class="tdata col-sm-3 line_r">
            <input name="pagu_pdpt_pnbp_jadi" type="text" class="form-control rp_norm line_b2" ng-model="pdptpnbpm" ui-number-mask="0" min="numberWith2Decimals" style="background:#effbee">
        </div>
        <div class="tdata col-sm-3 line_b2 selisih" id="">{{pdptpnbpm-pdptpnbps | number}}</div>
    </div>

<!--
    <div class="col-sm-12" style="padding:0px">
        <div class="tjudul2 col-sm-3 line_r" style="text-align:left;border-bottom:solid 1px #88BAD6">Target Pendapatan</div>
        <div class="tdata col-sm-3 line_r">
            <input name="pagu_target_mula" type="text" class="form-control rp_norm line_b" style="font-weight:bold;background:#effbee" ng-model="pdpts" ui-number-mask="0" min="numberWith2Decimals">
        </div>
        <div class="tdata col-sm-3 line_r">
            <input name="pagu_target_jadi" type="text" class="form-control rp_norm line_b" style="font-weight:bold;background:#effbee" ng-model="pdptm" ui-number-mask="0" min="numberWith2Decimals">
        </div>
        <div class="tdata col-sm-3 line_b selisih" id="">{{pdptm-pdpts | number}}</div>
    </div>
-->

    <div class="col-sm-12" style="padding:0px; border-top:solid 1px #88BAD6">
        <div class="tjudul3 col-sm-3 line_r text-bold">Catatan Halaman IV DIPA</div>
        <div class="tdata col-sm-3 line_r">
            <input name="pagu_hal_4_mula" type="text" class="form-control rp_norm" style="font-weight:bold;background:#effbee" ng-model="hal4s" ui-number-mask="0" min="numberWith2Decimals">
        </div>
        <div class="tdata col-sm-3 line_r">
            <input name="pagu_hal_4_jadi" type="text" class="form-control rp_norm" style="font-weight:bold;background:#effbee" ng-model="hal4m" ui-number-mask="0" min="numberWith2Decimals">
        </div>
        <div class="tdata col-sm-3 selisih" id="">{{hal4m-hal4s | number}}</div>
    </div>
</div>

<div class="small col-sm-10" style="margin-top:5px;margin-bottom:10px">
    <b>Keterangan : </b>Isikan data pada kolom berwarna <span style="color:#039103">HIJAU MUDA</span> -
    Gunakan tombol <b>TAB</b> untuk pindah kolom isian.
</div>

<div class="form-group" style="margin-left:15px;margin-bottom:3px">
    <div class="col-sm-10" style="padding:0px">
        <label for="">Catatan : </label>
        <textarea name="f4_catatan" class="form-control" placeholder="Catatan Rekomendasi" rows="5"><?= $nd['nota']['f4_catatan'] ?></textarea>
    </div>
</div>


<script type="text/javascript">
    $('input[name="radio-rekomendasi"]').change(function(){
        var id = $(this).attr('id');
        var label = $('#'+id).next("label").text();
        $('input[name="f4_usulan_rev"]').val(label);
    });
</script>
