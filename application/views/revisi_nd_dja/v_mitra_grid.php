<div class="box box-widget">
  <input type="hidden" id="aktif">
  
  <div class="box-body">
    <table id="iGrid" class="table table-hover table-bordered" style="border-spacing: 1; width: 100%;">
      <thead>
        <tr style="color: #fff; background-color: #3c8dbc;">
          <th class="text-center" width=" 7%">Tahun</th>
          <th class="text-center" width=""   >K/L - Unit Eselon I</th>
          <th class="text-center" width="20%">Email</th>
          <th class="text-center" width="20%">Contact Person<br>Phone</th>
        </tr>
        <tr id="tr_cari" style="margin: 0px" bgcolor="#fff">
          <td colspan="4">
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-btn">
                  <button id="btn_data" onclick="newadd()" class="btn btn-default"><i class="fa fa-plus"></i></button>
                </span>
                <input id="cari" class="form-control" placeholder="ketik kata cari" value="">
              </div>      
            </div>
          </td>
        </tr>
      </thead>

      <tbody>
          <tr id="tr_crud" style="display: none" bgcolor="#eee">
            <td colspan="4"><div id="crud_form"><?php $this->load->view('revisi_nd_dja/v_mitra_crud'); ?></div></td>
          </tr>

        <?php foreach ($tabel as $row) { ?>
          
          <tr id="id_<?= $row['kddept'].$row['kdunit'] ?>" class="<?= implode('#', $row) .'# All ' ?>" style="margin:0px;" onclick="move_row( this.id )">
            <td class="text-left"><?= $row['tahun'] ?></td>
            <td class="text-left">
              <?php 
                if ($row['nmdept']) echo '<strong>'. $row['nmdept'] .'</strong><br>';
                echo '['. $row['kddept'] .'.'. $row['kdunit'] .'] '. $row['nmunit'];
              ?>
            </td>
            <td class="text-left"><?= $row['email'] ?></td>
            <td class="text-left"><?= $row['cp'] .'<br>'. $row['phone'] ?></td>
          </tr>

        <?php } ?>
      </tbody>
    </table>

  </div>
</div>

<script type="text/javascript">
  // PENCARIAN
  $("#cari").keyup( function(event) {
    var kata = $('#cari').val();

    $('#tr_crud').insertAfter( $('#tr_cari') );
    $('#tr_crud').css('display', 'none');

    $('.All').show();
    $('#iGrid > tbody  > tr').each(function() {
      var id  = $(this).attr('id');
      var str = $('#'+id).text().toLowerCase();
      if (str.indexOf( kata.toLowerCase() ) < 0) $('#'+id).hide();
    });
  });


  // Click ROW untuk Buka Form CRUD
  function move_row( idkey ) {
    if ( $('#aktif').val()==idkey ) {
      $('#aktif').val('');
      // $('#crud_form').slideUp('slow').addClass('CloseForm').removeClass('OpenForm');
      // $("#tr_crud").delay(500).hide();
      $('#tr_crud').delay(2500).css('display', 'none');
    } else { 
      var arr = $('#'+idkey).attr('class').replace(' text-bold','').split("#");
      $('#tr_crud').css('display', '');
      // if ($('#crud_form'). $('#crud_form').slideUp('slow');
      // if ($('#crud_form').attr('class') == 'OpenForm') $('#crud_form').slideUp();
      $('#tr_crud').insertAfter( $('#'+idkey) );
      // $('#crud_form').slideDown('slow').addClass('OpenForm').removeClass('CloseForm');

      $("#0, #1, #2").prop('readonly', true);
      $("input[ name='isian[]' ]").each(function () { $(this).val(arr[ $(this).attr('id') ]); })
      $('#Rekam').hide(); $('#Ubah').show(); $('#Hapus').show(); $('#aktif').val(idkey);
    }
  }

  function animeCrud( aksi ) {
    // if (aksi == 'close') $('#crud_form').slideUp('slow').addClass('CloseForm').removeClass('OpenForm');
    // if (aksi == 'open') {
    //   if ($('#crud_form').attr('class') == 'OpenForm') $('#crud_form').slideUp();
    // }
  }

  // Click Tombol PLUS untuk Buka Form CRUD
  function newadd() {
    if ( $('#aktif').val()=='tr_crud' ) {
      $('#aktif').val('');
      $('#tr_crud').css('display', 'none');
    } else {
      $("#0, #1, #2").prop('readonly', false);
      $('#tr_crud').css('display', ''); 
      $('#tr_crud').insertAfter( $('#tr_cari') );
      $("input[ name='isian[]' ]").each(function () { $(this).val(''); })
      $('#Rekam').show(); $('#Ubah').hide(); $('#Hapus').hide(); $('#aktif').val('tr_crud');
    }
  }

</script>


