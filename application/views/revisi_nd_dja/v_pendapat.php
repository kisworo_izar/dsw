<div class="box-header with-border revHead">
    <h4 class="box-title" style="color:#FFF;padding-left:5px">3. PENDAPAT</h4>
</div>
<div class="form-group jarak" style="padding:10px 0px 5px 15px;margin-bottom:0px">
    <div class="col-sm-10">
        <?php echo $nd['pendapat']['pendapat']; ?>
    </div>
</div>
<div class="form-group jarak" style="padding:0px 5px 10px 15px;margin-bottom:0px">
    <label class="col-sm-7 rev">
        3.1 <?php echo $nd['pendapat']['hal1']; ?>
    </label>
    <div class="col-sm-3" style="padding:7px 0px 0px 0px">
        <select name="f3_batasan_rev" class="3-31 form-control">
            <?php $arr = array('Melanggar', 'Tidak dilanggar');
            foreach ($arr as $row) {
                if ($nd['nota']['f3_batasan_rev'] == $row) $sel = 'selected="selected"'; else $sel = '';
                echo "<option value='$row' $sel>$row</option>";
            } ?>
        </select>
    </div>
    <label class="col-sm-7 rev">
        3.2 <?php echo $nd['pendapat']['hal2']; ?>
    </label>
    <div class="col-sm-3" style="padding:7px 0px 0px 0px">
        <select name="f3_jns_wenang" class="3-32 form-control">
            <?php $arr = array('Kewenangan DJA', 'Kewenangan DJA & DJPb');
            foreach ($arr as $row) {
                if ($nd['nota']['f3_jns_wenang'] == $row) $sel = 'selected="selected"'; else $sel = '';
                echo "<option value='$row' $sel>$row</option>";
            } ?>
        </select>
    </div>
    <label class="col-sm-7 rev">
        3.3 <?php echo $nd['pendapat']['hal3']; ?>
    </label>
    <div class="col-sm-3" style="padding:7px 0px 0px 0px">
        <select name="f3_dok_usulan" class="3-33 form-control">
            <?php $arr = array('Lengkap', 'Tidak Lengkap');
            foreach ($arr as $row) {
                if ($nd['nota']['f3_dok_usulan'] == $row) $sel = 'selected="selected"'; else $sel = '';
                echo "<option value='$row' $sel>$row</option>";
            } ?>
        </select>
    </div>
    <label class="col-sm-7 rev">
        3.4 <?php echo $nd['pendapat']['hal4']; ?>
    </label>
    <div class="col-sm-3" style="padding:7px 0px 0px 0px">
        <select name="f3_dok_pendukung" class="3-34 form-control">
            <?php $arr = array('Lengkap', 'Tidak Lengkap', 'Tidak Diperlukan');
            foreach ($arr as $row) {
                if ($nd['nota']['f3_dok_pendukung'] == $row) $sel = 'selected="selected"'; else $sel = '';
                echo "<option value='$row' $sel>$row</option>";
            } ?>
        </select>
    </div>
</div>
<div class="form-group jarak">
    <div class="col-sm-10" style="padding:5px 0px 5px 30px">
        <label>Keterangan / Catatan / Hasil analisa : </label>
        <textarea name="f3_ket_catatan" class="form-control" placeholder="Keterangan / Catatan / Hasil analisa Pendapat" rows="5"><?= $nd['nota']['f3_ket_catatan'] ?></textarea>
    </div>
</div>
