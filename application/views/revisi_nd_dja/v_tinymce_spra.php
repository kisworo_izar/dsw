<script src="<?=base_url('assets/plugins/tinymce/tinymce.min.js'); ?>"></script>
<script type="text/javascript">
   tinymce.init({
      selector: "textarea",
      content_css: "<?=base_url('assets/plugins/tinymce/myStyle.css'); ?>, <?=base_url('assets/bootstrap/css/bootstrap.min.css'); ?>",
      fontsize_formats: '7pt 8pt 9pt 10pt 11pt 12pt 13pt 14pt',
      toolbar: "styleselect",
      style_formats: [
        // {title: 'Line height 20px', selector: 'p div', styles: {lineHeight: '20px'}},
        {title: 'Paragraph', selector: 'p', styles: {'text-indent': '2.5em'}},
        {title: 'Penomoran Level 1', selector: 'ol', styles: {'padding-left':'20px'}},
        {title: 'Penomoran Level 2', selector: 'ol', styles: {'padding-left':'20px','padding-top':'5px'}},
        {title: 'Penomoran Level 3', selector: 'ol', styles: {'padding-left':'20px','padding-top':'5px'}},
        {title: 'Line Spacing Penomoran 5px', selector: 'li', styles: {'padding-top':'5px'}},
     ],
     format: {
      removeformat: [
        {selector: 'b,strong,em,i,font,u,strike', remove : 'all', split : true, expand : false, block_expand: true, deep : true},
        {selector: 'span', attributes : ['style', 'class'], remove : 'empty', split : true, expand : false, deep : true},
        {selector: '*', attributes : ['style', 'class'], split : false, expand : false, deep : true}
      ]
  },
      content_style: ".mce-content-body {font-size:11pt;font-family:Arial,sans-serif;}",
      theme: "modern",
      height: "600",
      width: "210mm",
      plugins: [
         "advlist autolink lists link image charmap print preview hr anchor pagebreak",
         "searchreplace wordcount visualblocks visualchars code fullscreen",
         "insertdatetime nonbreaking save table contextmenu directionality",
         "template paste textcolor textpattern"
      ],
      toolbar1: "insertfile undo redo | styleselect | fontsizeselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | print",
      entity_encoding: 'raw',
      setup: function (editor) {
            editor.on('change', function () {
                tinymce.triggerSave();
            });
        }
   });
</script>

<div class="row" ng-app="app" >
   <div class="col-md-12">
      <div class="box box-widget">
         <div style="float:left; margin-right:20px">
            <form method="post">
                <input type="hidden" name="rev_id" id="rev_id" value=<?= $this->uri->segment(3) ?>>
                <textarea id="mytextarea" name="mytextarea"><?= $teks ?></textarea>
            </form>
         </div>
         <div style="float:left">
            <button type="button" name="button" onclick='simpan_spra($("#mytextarea").val())' class="btn btn-info btn-flat text-bold"><i class="fa fa-save"> </i>&nbsp;&nbsp;Simpan SPRA</button>
         </div>
      </div>
   </div>
</div>

<script type="text/javascript">
  function simpan_spra(nil) {
    var rev_id = $('#rev_id').val();
    var isian  = "isian="+nil+"&rev_id="+rev_id;
    $.ajax({
      type: "POST", data: isian, url: "<?php echo site_url('revisi_nd_dja/editor_spra_save') ?>" ,
      success: function(nil) { 
        alert("Data Tersimpan");
      }  
    })
  }
</script>
