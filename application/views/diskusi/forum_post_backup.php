  <div class="row">
            <div class="col-xs-8">
            <div class="col-xs-12">

              <!-- Box Comment -->
              <div class="box box-widget">
                <div class='box-header with-border'>
                  <div class='user-block'>
                    <img class='img-circle' src='files/images/foto52.jpg' alt='user image'>
                    <span class='username'><a href="#"><?php echo $post['post']; ?></a></span>
                    <span class='description'>Shared publicly - 7:30 PM Today</span>
                  </div><!-- /.user-block -->
                  <div class='box-tools'>
                    <button class='btn btn-box-tool' data-toggle='tooltip' title='Mark as read'><i class='fa fa-circle-o'></i></button>
                    <button class='btn btn-box-tool' data-widget='collapse'><i class='fa fa-minus'></i></button>
                    <button class='btn btn-box-tool' data-widget='remove'><i class='fa fa-times'></i></button>
                  </div><!-- /.box-tools -->
                </div><!-- /.box-header -->
                <div class='box-body'>
                  <!-- post text -->
                  <p><?php echo $post['post']; ?></p>
                  <!-- Attachment -->
                  <div class="attachment-block clearfix">
                    <img class="attachment-img" src="<?php echo base_url(); ?>assets/dist/img/photo1.png" alt="attachment image">
                    <div class="attachment-pushed">
                      <h4 class="attachment-heading"><a href="http://www.lipsum.com/">Lorem ipsum text generator</a></h4>
                     <!--  <div class="attachment-text">
                        Description about the attachment can be placed here.
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry... <a href="#">more</a>
                      </div> --><!-- /.attachment-text -->
                    </div><!-- /.attachment-pushed -->
                  </div><!-- /.attachment-block -->

                  <!-- Social sharing buttons -->
                  <button class='btn btn-default btn-xs'><i class='fa fa-share'></i> Share</button>
                  <button class='btn btn-default btn-xs'><i class='fa fa-thumbs-o-up'></i> Like</button>
                  <span class='pull-right text-muted'>45 likes - 2 comments</span>
                </div><!-- /.box-body -->
                <div class='box-footer box-comments'>
                  <div class='box-comment'>
                    <!-- User image -->
                    <img class='img-circle img-sm' src='<?php echo base_url(); ?>assets/dist/img/user3-128x128.jpg' alt='user image'>
                    <div class='comment-text'>
                      <span class="username">
                        Maria Gonzales
                        <span class='text-muted pull-right'>8:03 PM Today</span>
                      </span><!-- /.username -->
                      It is a long established fact that a reader will be assets/distracted
                      by the readable content of a page when looking at its layout.
                    </div><!-- /.comment-text -->
                  </div><!-- /.box-comment -->
                  <div class='box-comment'>
                    <!-- User image -->
                    <img class='img-circle img-sm' src='<?php echo base_url(); ?>assets/dist/img/user5-128x128.jpg' alt='user image'>
                    <div class='comment-text'>
                      <span class="username">
                        Nora Havisham
                        <span class='text-muted pull-right'>8:03 PM Today</span>
                      </span><!-- /.username -->
                      The point of using Lorem Ipsum is that it has a more-or-less
                      normal assets/distribution of letters, as opposed to using
                      'Content here, content here', making it look like readable English.
                    </div><!-- /.comment-text -->
                  </div><!-- /.box-comment -->
                </div><!-- /.box-footer -->
                <div class="box-footer">
                  <form action="#" method="post">
                    <img class="img-responsive img-circle img-sm" src="<?php echo base_url(); ?>assets/dist/img/user4-128x128.jpg" alt="alt text">
                    <!-- .img-push is used to add margin to elements next to floating images -->
                    <div class="img-push">
                      <input type="text" class="form-control input-sm" placeholder="Press enter to post comment">
                    </div>
                  </form>
                </div><!-- /.box-footer -->
              </div><!-- /.box -->
            </div>

             <div class="col-xs-12">
            
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Sub Forum</h3>
                  <div class="box-tools">
                    <!-- <div class="input-group" style="width: 150px;">
                      <input type="text" name="table_search" class="form-control input-sm pull-right" placeholder="Search">
                      <div class="input-group-btn">
                        <button class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
                      </div>
                    </div> -->
                  </div>
                </div><!-- /.box-header -->
                <div class="box-body table-responsive">
                  <table class="table table-hover">
                    <tr>
                      <th>Sub Forum</th>
                      <th>Stats</th>
                      <th>Lats Post</th>
                    </tr>
                    <tr>
                      <td>
                         <div class='box-comment'>
                          <!-- User image -->
                          <img class='img-circle img-sm' src='<?php echo base_url(); ?>assets/dist/img/user3-128x128.jpg' alt='user image'>
                          <div class='comment-text'>
                            <span class="username">
                              <!-- Maria Gonzales -->
                              <!-- <span class='text-muted pull-right'>8:03 PM Today</span>
                            </span> --><!-- /.username -->
                            The Longue lorem ipsum dolar sit agdg asd fsadfasd f ads 
                          </div><!-- /.comment-text -->
                        </div><!-- /.box-comment -->
                        <div class='box-comment'>
                      </td>
                      
                      <td>
                         <div class='box-comment'>
                          <div class='comment-text'>
                            <span class="username">
                              <!-- Maria Gonzales -->
                            Replies : <br>
                            Views :
                          </div><!-- /.comment-text -->
                        </div><!-- /.box-comment -->
                        <div class='box-comment'>
                      </td>
                      <td>
                         <div class='box-comment'>
                          <div class='comment-text'>
                            <span class="username">
                              <!-- Maria Gonzales -->
                            lorem ipsum dolar sit amet <br>
                            2015-12-11 by mamat
                          </div><!-- /.comment-text -->
                        </div><!-- /.box-comment -->
                        <div class='box-comment'>
                      </td>
                    </tr>
                    <tr>
                      <td>
                         <div class='box-comment'>
                          <!-- User image -->
                          <img class='img-circle img-sm' src='<?php echo base_url(); ?>assets/dist/img/user3-128x128.jpg' alt='user image'>
                          <div class='comment-text'>
                            <span class="username">
                              <!-- Maria Gonzales -->
                              <!-- <span class='text-muted pull-right'>8:03 PM Today</span>
                            </span> --><!-- /.username -->
                            The Longue lorem ipsum dolar sit agdg asd fsadfasd f ads 
                          </div><!-- /.comment-text -->
                        </div><!-- /.box-comment -->
                        <div class='box-comment'>
                      </td>
                      
                      <td>
                         <div class='box-comment'>
                          <div class='comment-text'>
                            <span class="username">
                              <!-- Maria Gonzales -->
                            Replies : <br>
                            Views :
                          </div><!-- /.comment-text -->
                        </div><!-- /.box-comment -->
                        <div class='box-comment'>
                      </td>
                      <td>
                         <div class='box-comment'>
                          <div class='comment-text'>
                            <span class="username">
                              <!-- Maria Gonzales -->
                            lorem ipsum dolar sit amet <br>
                            2015-12-11 by mamat
                          </div><!-- /.comment-text -->
                        </div><!-- /.box-comment -->
                        <div class='box-comment'>
                      </td>
                    </tr>
                    <tr>
                      <td>
                         <div class='box-comment'>
                          <!-- User image -->
                          <img class='img-circle img-sm' src='<?php echo base_url(); ?>assets/dist/img/user3-128x128.jpg' alt='user image'>
                          <div class='comment-text'>
                            <span class="username">
                              <!-- Maria Gonzales -->
                              <!-- <span class='text-muted pull-right'>8:03 PM Today</span>
                            </span> --><!-- /.username -->
                            The Longue lorem ipsum dolar sit agdg asd fsadfasd f ads 
                          </div><!-- /.comment-text -->
                        </div><!-- /.box-comment -->
                        <div class='box-comment'>
                      </td>
                      
                      <td>
                         <div class='box-comment'>
                          <div class='comment-text'>
                            <span class="username">
                              <!-- Maria Gonzales -->
                            Replies : <br>
                            Views :
                          </div><!-- /.comment-text -->
                        </div><!-- /.box-comment -->
                        <div class='box-comment'>
                      </td>
                      <td>
                         <div class='box-comment'>
                          <div class='comment-text'>
                            <span class="username">
                              <!-- Maria Gonzales -->
                            lorem ipsum dolar sit amet <br>
                            2015-12-11 by mamat
                          </div><!-- /.comment-text -->
                        </div><!-- /.box-comment -->
                        <div class='box-comment'>
                      </td>
                    </tr>
                    <tr>
                      <td>
                         <div class='box-comment'>
                          <!-- User image -->
                          <img class='img-circle img-sm' src='<?php echo base_url(); ?>assets/dist/img/user3-128x128.jpg' alt='user image'>
                          <div class='comment-text'>
                            <span class="username">
                              <!-- Maria Gonzales -->
                              <!-- <span class='text-muted pull-right'>8:03 PM Today</span>
                            </span> --><!-- /.username -->
                            The Longue lorem ipsum dolar sit agdg asd fsadfasd f ads 
                          </div><!-- /.comment-text -->
                        </div><!-- /.box-comment -->
                        <div class='box-comment'>
                      </td>
                      
                      <td>
                         <div class='box-comment'>
                          <div class='comment-text'>
                            <span class="username">
                              <!-- Maria Gonzales -->
                            Replies : <br>
                            Views :
                          </div><!-- /.comment-text -->
                        </div><!-- /.box-comment -->
                        <div class='box-comment'>
                      </td>
                      <td>
                         <div class='box-comment'>
                          <div class='comment-text'>
                            <span class="username">
                              <!-- Maria Gonzales -->
                            lorem ipsum dolar sit amet <br>
                            2015-12-11 by mamat
                          </div><!-- /.comment-text -->
                        </div><!-- /.box-comment -->
                        <div class='box-comment'>
                      </td>
                    </tr>
                    <tr>
                      <td>
                         <div class='box-comment'>
                          <!-- User image -->
                          <img class='img-circle img-sm' src='<?php echo base_url(); ?>assets/dist/img/user3-128x128.jpg' alt='user image'>
                          <div class='comment-text'>
                            <span class="username">
                              <!-- Maria Gonzales -->
                              <!-- <span class='text-muted pull-right'>8:03 PM Today</span>
                            </span> --><!-- /.username -->
                            The Longue lorem ipsum dolar sit agdg asd fsadfasd f ads 
                          </div><!-- /.comment-text -->
                        </div><!-- /.box-comment -->
                        <div class='box-comment'>
                      </td>
                      
                      <td>
                         <div class='box-comment'>
                          <div class='comment-text'>
                            <span class="username">
                              <!-- Maria Gonzales -->
                            Replies : <br>
                            Views :
                          </div><!-- /.comment-text -->
                        </div><!-- /.box-comment -->
                        <div class='box-comment'>
                      </td>
                      <td>
                         <div class='box-comment'>
                          <div class='comment-text'>
                            <span class="username">
                              <!-- Maria Gonzales -->
                            lorem ipsum dolar sit amet <br>
                            2015-12-11 by mamat
                          </div><!-- /.comment-text -->
                        </div><!-- /.box-comment -->
                        <div class='box-comment'>
                      </td>
                    </tr>
                  </table>
                </div><!-- /.box-body -->
                <div class="box-footer clearfix">
                  <ul class="pagination pagination-sm no-margin pull-right">
                    <li><a href="#">&laquo;</a></li>
                    <li><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">&raquo;</a></li>
                  </ul>
                </div>
              </div><!-- /.box -->



            </div>

          </div>

            <div class="col-md-4">
              <div class="col-md-12">

              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Sticky Sub Forum</h3>
                  <div class="box-tools">
                    <!-- <div class="input-group" style="width: 150px;">
                      <input type="text" name="table_search" class="form-control input-sm pull-right" placeholder="Search">
                      <div class="input-group-btn">
                        <button class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
                      </div>
                    </div> -->
                  </div>
                </div><!-- /.box-header -->
                <div class="box-body table-responsive">
                  <table class="table table-hover">
                    <tr>
                      <td>
                         <div class='box-comment'>
                          <!-- User image -->
                          <img class='img-circle img-sm' src='<?php echo base_url(); ?>assets/dist/img/user3-128x128.jpg' alt='user image'>
                          <div class='comment-text'>
                            <span class="username">
                              <!-- Maria Gonzales -->
                              <!-- <span class='text-muted pull-right'>8:03 PM Today</span>
                            </span> --><!-- /.username -->
                            The Longue lorem ipsum dolar sit agdg asd fsadfasd f ads <br>
                             <div align="right">Replies : 1 Jt - Views : 5 Jt
                            </div>
                          </div><!-- /.comment-text -->
                        </div><!-- /.box-comment -->
                        <div class='box-comment'>
                      </td>
                    </tr>
                    <tr>
                      <td>
                         <div class='box-comment'>
                          <!-- User image -->
                          <img class='img-circle img-sm' src='<?php echo base_url(); ?>assets/dist/img/user3-128x128.jpg' alt='user image'>
                          <div class='comment-text'>
                            <span class="username">
                              <!-- Maria Gonzales -->
                              <!-- <span class='text-muted pull-right'>8:03 PM Today</span>
                            </span> --><!-- /.username -->
                            The Longue lorem ipsum dolar sit agdg asd fsadfasd f ads <br>
                             <div align="right">Replies : 1 Jt - Views : 5 Jt
                            </div>
                          </div><!-- /.comment-text -->
                        </div><!-- /.box-comment -->
                        <div class='box-comment'>
                      </td>
                    </tr>
                    <tr>
                      <td>
                         <div class='box-comment'>
                          <!-- User image -->
                          <img class='img-circle img-sm' src='<?php echo base_url(); ?>assets/dist/img/user3-128x128.jpg' alt='user image'>
                          <div class='comment-text'>
                            <span class="username">
                              <!-- Maria Gonzales -->
                              <!-- <span class='text-muted pull-right'>8:03 PM Today</span>
                            </span> --><!-- /.username -->
                            The Longue lorem ipsum dolar sit agdg asd fsadfasd f ads <br>
                             <div align="right">Replies : 1 Jt - Views : 5 Jt
                            </div>
                          </div><!-- /.comment-text -->
                        </div><!-- /.box-comment -->
                        <div class='box-comment'>
                      </td>
                    </tr>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
              </div>
              <div class="col-md-12">
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Top Threads</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table class="table">
                    <tr>
                      <td>Update software bla bla bla bla blabla bla <br>2015-12-90 by Mamat solikin</td>
                    </tr>
                    <tr>
                      <td>Update software bla bla bla bla blabla bla <br>2015-12-90 by Mamat solikin</td>
                    </tr>
                    <tr>
                      <td>Update software bla bla bla bla blabla bla <br>2015-12-90 by Mamat solikin</td>
                    </tr>
                    <tr>
                      <td>Update software bla bla bla bla blabla bla <br>2015-12-90 by Mamat solikin</td>
                    </tr>
                    <tr>
                      <td>Update software bla bla bla bla blabla bla <br>2015-12-90 by Mamat solikin</td>
                    </tr>
                    <tr>
                      <td>Update software bla bla bla bla blabla bla <br>2015-12-90 by Mamat solikin</td>
                    </tr>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
              </div>
            </div><!-- /.col -->
          </div>