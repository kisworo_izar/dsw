  <div class="row">
            <div class="col-xs-8">
            <div class="col-xs-12">
              <!-- Box Comment -->
              <div class="box box-widget">
                <div class='box-header with-border'>
                  <div class='user-block'>
                    <img class='img-circle' src="<?php echo base_url(); ?>files/profiles/<?php echo $firstpost['nip'];?>.gif" alt='user image'>
                    <span class='username'><a href="#"><?php echo strtoupper($room['nmroom']); ?></a></span>
                    <span class='description'><?php echo $this->fc->idtgl($firstpost['tglpost'],'full') ; ?></span>
                  </div><!-- /.user-block -->
                  <div class='box-tools'>
                    <button class='btn btn-box-tool' data-toggle='tooltip' title='Mark as read'><i class='fa fa-circle-o'></i></button>
                    <button class='btn btn-box-tool' data-widget='collapse'><i class='fa fa-minus'></i></button>
                    <button class='btn btn-box-tool' data-widget='remove'><i class='fa fa-times'></i></button>
                  </div><!-- /.box-tools -->
                </div><!-- /.box-header -->
                <div class='box-body'>
                  <!-- post text -->
                  <?php if ($firstpost['attach']): ?>
                  <div class="attachment-block clearfix">
                    <img class="attachment-img" src="<?php echo base_url(); ?>assets/dist/img/photo1.png" alt="attachment image">
                  </div><!-- /.attachment-block -->
                  <?php endif ?>
                  <p><?php echo $firstpost['post']; ?></p>
                  <!-- <button class='btn btn-default btn-xs'><i class='fa fa-thumbs-o-up'></i> Like</button>
                  <span class='pull-right text-muted'>45 likes - 2 comments</span> -->
                </div><!-- /.box-body -->
                <div class='box-footer box-comments'>
                <?php if (!empty($post)): ?>
                <?php foreach ($post as $key => $value): ?>
                  <div class='box-comment'>
                    <!-- User image -->
                    <img class='img-circle img-sm' src="<?php echo base_url(); ?>files/profiles/<?php echo $value['nip']; ?>.gif" alt='user image'>
                    <div class='comment-text'>
                      <span class="username">
                        <?php echo $value['nmuser']; ?>
                        <span class='text-muted pull-right'><?php echo $this->fc->idtgl($value['tglpost'],'full') ; ?></span>
                      </span><!-- /.username -->
                      <?php echo $value['post']; ?>
                    </div><!-- /.comment-text -->
                  </div><!-- /.box-comment -->
                <?php endforeach ?>
                <?php endif ?>
                </div><!-- /.box-footer -->
                <div class="box-footer">
                  <form action="<?php echo site_url('forum/addkomen') ?>" method="post" role="form" enctype="multipart/form-data">
                    <input type="hidden"  name="uri3" value="<?php echo $this->uri->segment(3) ?>">
                    <input type="hidden"  name="uri4" value="<?php echo $this->uri->segment(4) ?>">
                    <input type="hidden"  name="idparent" value="<?php echo $this->uri->segment(3) ?>">
                    <input type="hidden"  name="idchild" value="<?php echo $this->uri->segment(4) ?>">
                    <img class="img-responsive img-circle img-sm" src="<?php echo base_url(); ?>files/profiles/_noprofile.png" alt="alt text">
                    <!-- .img-push is used to add margin to elements next to floating images -->
                    <div class="img-push">
                      <input name="komen" type="text" class="form-control input-sm" placeholder="Tekan Enter untuk menambahkan komentar">
                      <input type="submit" style="position: absolute; left: -9999px; width: 1px; height: 1px;"
       tabindex="-1" value="Tambah">
                    </div>
                  </form>
                </div><!-- /.box-footer -->
              </div><!-- /.box -->
            </div>


          </div>

            <div class="col-md-4">
              <div class="col-md-12">
              <div class="box">
                <div class="box-header">

                  <h3 class="box-title">Posting Terakhir</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table class="table">
                    <?php foreach ($last_post as $key => $value): ?>
                    
                    <tr>
                      <td> 
                     <a href="<?php echo base_url(); ?>forum/forum_post/<?php echo $value['idparent']; ?>/<?php echo $value['idchild']; ?>">
                      <?php echo $value['post']; ?> 
                      </a>
                      <br><span class="text-red"><?php echo $this->fc->idtgl($value['tglpost']); ?></span> by <span class="text-light-blue"><?php echo $value['nmuser']; ?></span>
                      </td>
                    </tr>
                  <?php endforeach ?>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
              </div>
            </div><!-- /.col -->
          </div>