<!DOCTYPE html>
<html>
<head>
  <?php $this->load->view('main/utama_link'); ?>
  <style media="screen">
    th {
      text-align: center;
    }
    thead tr {
      color: #fff;
      background: #00acd6;
    }

    #iGrid .ui-selecting { background: #FECA40; }
    #iGrid .ui-selected { background: #F39814; color: white; }

    .ui-autocomplete { height: 200px; overflow-y: scroll; overflow-x: hidden;}
  </style>
</head>

<body>
  <div class="row">
    <div class="col-md-12">
      <div class="box box-warning">
   
        <div class="box-header with-border">
          <div class="well well-sm"> 
            <form id="iForm" class="form-inline" role="form" action="<?php echo site_url("c_paket/cari") ?>" method="post">
              <div class="form-group pull-right">
                <div class="input-group">
                  <input class="form-control" name="cari" type="text" value="<?php echo $this->session->userdata('cari') ?>">
                  <span class="input-group-btn">
                    <button type="submit" name="search" value="search" class="btn btn-info btn-flat"><i class="fa fa-search"></i></button>
                  </span>
                  <span class="input-group-btn">
                    <button type="submit" name="search" value="clear" class="btn btn-info btn-flat"><i class="fa fa-times-circle"></i></button>
                  </span>
                </div>
              </div>
              <div class="form-group">
                <div class="input-group">
                  <span class="input-group-btn">
                    <button type="button" id="addrow" class="btn btn-info" data-toggle="modal" data-target="#myModal">Paket Tiba</button>
                    <button type="button" id="editrow" class="btn btn-info" data-toggle="modal" data-target="#myModal">Paket Diambil</button>
                    <button type="button" id="delrow" class="btn btn-info" data-toggle="modal" data-target="#myModal">Hapus</button>
                  </span>
                </div>
              </div>
            </form>
          </div>          
        </div><!-- /.box-header -->

        <div class="box-body">
          <h2><?php echo $judul ?></h2>
          <table id="iGrid" class="table table-hover table-bordered">
            <thead>
              <tr>
                <th>Paket ID</th>
                <th>Tgl. Tiba</th>
                <th>Penerima</th>
                <th>Tgl. Diambil</th>
                <th>Pengambil</th>
                <th>Keterangan</th>
              </tr>
            </thead>

            <tbody>
            <?php foreach ($nilai as $row) { ?>
              <tr>
                <td><?php echo $row['idpaket'] ?></td>
                <td><?php echo $row['tglrekam'] ?></td>
                <td id="<?php echo $row['iduser'] ?>"><?php echo $row['nmuser'] ?></td>
                <td><?php echo $row['tglambil'] ?></td>
                <td id="<?php echo $row['idterima'] ?>"><?php echo $row['nmterima'] ?></td>
                <td><?php echo $row['keterangan'] ?></td>
              </tr>
            <?php } ?>
            </tbody>
          </table>
        </div><!-- /.box-body -->

        <div class="box-footer clearfix">
          <?php echo $this->pagination->create_links(); ?>
        </div><!-- /.box-footer -->

      </div><!-- /.box -->
    </div><!-- /.col -->
  </div><!-- /.row -->


  <script type="text/javascript">
    $('.pagination').addClass('pagination-sm no-margin pull-right');
  </script>

  <?php $this->load->view('v_paket_rekam') ?>

</body>
</html>
