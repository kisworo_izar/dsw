         
          <div class="row">
            <div class="col-xs-8">
              <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title"><i class="fa fa-comment-o"></i> Room Diskusi</h3>
                  <div class="box-tools">
                    <!-- <div class="input-group" style="width: 150px;">
                      <input type="text" name="table_search" class="form-control input-sm pull-right" placeholder="Search">
                      <div class="input-group-btn">
                        <button class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
                      </div>
                    </div> -->
                  </div>
                </div><!-- /.box-header -->
                <div class="box-body table-responsive">
                  <table class="table table-stripped">
                    

                    <tr>
                      <th><i class="fa fa-group"></i> Room</th>
                      <th><i class="fa fa-trophy"> Stats</th> 
                    </tr>
                  <?php foreach ($room as $key => $value): ?>
                    <tr>
                      <td>
                         <div class='box-comment'>
                          <!-- User image -->
                          <!-- <img class='img-circle img-sm' src='../../dist/img/user3-128x128.jpg' alt='user image'> -->
                          <div class='comment-text'>
                            <span class="username">
                              <!-- Maria Gonzales -->
                              <!-- <span class='text-muted pull-right'>8:03 PM Today</span>
                            </span> --><!-- /.username -->
                            <span class=""><a href="<?php echo base_url(); ?>forum/forum_post/<?php echo $value['idparent']; ?>/<?php echo $value['idchild']; ?>"><?php echo strtoupper($value['nmroom']); ?></a></span>  <br>
                            <span class="text-red"><?php echo $this->fc->idtgl($value['tglcreate'],'full'); ?></span> by <span class="text-light-blue"><?php echo $value['nmuser']; ?></span>

                          </div><!-- /.comment-text -->
                        </div><!-- /.box-comment -->
                        <div class='box-comment'>
                      </td>
                      
                      <td>
                         <div class='box-comment'>
                          <div class='comment-text'>
                            <span class="username">
                              <!-- Maria Gonzales -->
                            Replies : <span class="text-yellow"></span><br>
                            Views : <span class="text-yellow"></span>
                          </div><!-- /.comment-text -->
                        </div><!-- /.box-comment -->
                        <div class='box-comment'>
                      </td>
                    </tr>
                  <?php endforeach ?>
                  </table>
                   <tr>
                      <td><a href="<?php echo site_url('forum/faddroom') ?>" class="btn btn-primary">Tambah</a></td>
                    </tr>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div>
          </div>

            <div class="col-md-4">
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title"><i class="fa fa-comment-o"></i> Posting Terakhir</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table class="table">
                  <?php foreach ($last_post as $key => $value): ?>
                    
                    <tr>
                      <td> 
                     <a href="<?php echo base_url(); ?>forum/forum_post/<?php echo $value['idparent']; ?>/<?php echo $value['idchild']; ?>">
                      <?php echo $value['post']; ?> 
                      </a>
                      <br><span class="text-red"><?php echo $this->fc->idtgl($value['tglpost']); ?></span> by <span class="text-light-blue"><?php echo $value['nmuser']; ?></span>
                      </td>
                    </tr>
                  <?php endforeach ?>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->

            </div><!-- /.col -->
          </div>