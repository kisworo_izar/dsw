
          <div class="row">
            <!-- left column -->
            <div class="col-md-12">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Tambah Room</h3>
                   <?php if ($this->session->flashdata('pesan')): ?>
                    <br>
                    <div class="alert alert-success" role="alert"><?php echo $this->session->flashdata('pesan'); ?></div>
                <?php endif ?>
                </div><!-- /.box-header -->
                <!-- form start -->
                <?php echo form_open_multipart('forum/addpost'); ?>
                  <div class="box-body">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Induk Room</label>
                       <!-- select -->
                       <?php $attr = "id='selectError' required class='form-control' " ?>
                    <?php // $attr = "id='selectError' required class= form-control " ?>
                    <?php echo form_dropdown("kdinduk",$optionindukroom,"",$attr);?>

                         <!--  <select class="form-control">
                            <option>option 1</option>
                            <option>option 2</option>
                            <option>option 3</option>
                            <option>option 4</option>
                            <option>option 5</option>
                          </select> -->
                    </div>
                    <div class="form-group">
                      <label>Nama Room</label>
                      <input  type="text" name="nmroom" class="form-control" rows="3" placeholder=""/>
                    </div>
                    <div class="form-group">
                      <label>Prakata</label>
                      <textarea name="prakata" class="form-control" rows="3" placeholder=""></textarea>
                    </div>
                    <div class="form-group">
                      <label for="exampleInputFile">Attachment</label>
                      <input name="berkas" type="file" id="exampleInputFile">
                      <p class="help-block">jpg,doc,pdf</p>
                    </div>
                  </div><!-- /.box-body -->

                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Tambah</button>
                  </div>
                </form>
              </div><!-- /.box -->

             

                </div><!-- /.box-body -->
              </div><!-- /.box -->

            </div><!--/.col (left) -->
            <!-- right column -->
           
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!--/.col (right) -->
          </div>   <!-- /.row -->