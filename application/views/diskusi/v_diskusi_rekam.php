<script src="<?=base_url('assets/plugins/slimScroll/jquery.slimscroll.min.js'); ?>" type="text/javascript"></script>    
<script src="<?=base_url('assets/plugins/jQueryUI/jquery-ui.1.11.4.min.js'); ?>" type="text/javascript"></script>
<link rel="stylesheet" href="<?=base_url('assets/plugins/jQueryUI/jquery-ui.css'); ?>">
<script>
  $(function() {
    var t_user = <?php echo $t_user; ?>;
    $( "#nmuser" ).autocomplete({
      source: t_user,
      select: function( event, ui ) {
        document.getElementById("iduser").value = ui.item.data;
      }
    });

    $( "#nmterima" ).autocomplete({
      source: t_user,
      select: function( event, ui ) {
        document.getElementById("idterima").value = ui.item.data;
      }
    });

    $("#nmuser, #nmterima").autocomplete("option", "appendTo", "#myModal");
  });
</script>

    <div class="modal fade" id="myModal">
      <div class="modal-dialog">
        <div class="modal-content">

          <form action="<?php echo site_url('c_paket/crud') ?>" method="post" role="form">
            <div class="modal-header">
              <h4 id="judul-form" class="modal-title">Rekam User</h4>
            </div>
            <div class="modal-body">
              Paket ID :
              <input name="idpaket" id="idpaket" type="text" class="form-control" readonly="readonly" value="">

              <div id="rekam">
                Tgl. Rekam :
                <input name="tglrekam" id="tglrekam" type="text" class="form-control" readonly="readonly" value="">
                User ID :
                <input name="iduser" id="iduser" type="hidden" value="">
                <input name="nmuser" id="nmuser" type="text" class="form-control" value="">
              </div>
              <div id="ambil">
                Tgl. Ambil :
                <input name="tglambil" id="tglambil" type="text" class="form-control" readonly="readonly" value="">
                Pengambil :
                <input name="idterima" id="idterima" type="hidden" class="form-control" value="">
                <input name="nmterima" id="nmterima" type="text" class="form-control" value="">
              </div>

              Keterangan :
              <input name="keterangan" id="keterangan" type="text" class="form-control" value="">
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
              <input name="simpan" type="submit" id="simpan" class="btn btn-primary" value="Tambah">
            </div>
          </form>

        </div>
      </div>
    </div>

<script type="text/javascript">
  $(function() {

    // Tandai ROW yang dipilih dalam iGRID
    $( "#iGrid tbody" ).selectable({
          filter: ":not(td)",
          create: function( e, ui ) {
              iGridLink( $() );
          },
          selected: function( e, ui ) {
              var widget = $(this).find('.ui-selected');
              $(ui.unselected).addClass("info");
              iGridLink( widget );
          },
          unselected: function( e, ui ) {
              $(ui.unselected).removeClass("info");
              var widget = $(this).find('.ui-selected');
              iGridLink( widget );
          }
    });

    // ONCLICK untuk Rekam / Ubah / Hapus
    $("#addrow").on("click", function() {
      document.getElementById("idpaket").value = ''; 
      document.getElementById("iduser").value = ''; 
      document.getElementById("nmuser").value = ''; 
      document.getElementById("tglrekam").value = '<?php echo date("Y-m-d H:i:s") ?>';
      document.getElementById("idterima").value = '';  
      document.getElementById("nmterima").value = '';  
      document.getElementById("tglambil").value = ''; 
      document.getElementById("keterangan").value = '';   
      document.getElementById("simpan").value = 'Rekam';
      document.getElementById("judul-form").innerHTML = 'Paket Baru Tiba';
      $("#rekam").show();
      $("#ambil").hide();
    });
    $("#editrow").on("click", function() {
      document.getElementById("tglambil").value = '<?php echo date("Y-m-d H:i:s") ?>'; 
      document.getElementById("simpan").value = 'Ambil';
      document.getElementById("judul-form").innerHTML = 'Paket Diambil';
      $("#rekam").hide();
      $("#ambil").show();
    });
    $("#delrow").on("click", function() {
      document.getElementById("simpan").value = 'Hapus';
      document.getElementById("judul-form").innerHTML = 'Hapus Paket';
      $("#rekam").show();
      $("#ambil").show();
    });

    // Ambil Nilai ROW pada iGRID yang dipilih per-ID sesuai kolom CHILD
    function iGridLink( $selectees ) {
      selected = $.makeArray( $selectees.filter( ".ui-selected" ) );
      selected.reduce( function( a, b ) {
        document.getElementById("idpaket").value = $(b).children( "td:nth-child(1)" ).text();
        document.getElementById("tglrekam").value = $(b).children( "td:nth-child(2)" ).text();
        document.getElementById("iduser").value = $(b).children( "td:nth-child(3)" ).attr('id');
        document.getElementById("nmuser").value = $(b).children( "td:nth-child(3)" ).text();
        document.getElementById("tglambil").value = $(b).children( "td:nth-child(4)" ).text();
        document.getElementById("idterima").value = $(b).children( "td:nth-child(5)" ).attr('id');
        document.getElementById("nmterima").value = $(b).children( "td:nth-child(5)" ).text();
        document.getElementById("keterangan").value = $(b).children( "td:nth-child(6)" ).text();
        }, 0 
      );
     }

  }); 
</script>