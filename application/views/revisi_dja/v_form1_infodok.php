<?php 
  $disabled=''; $chec1=''; $chec2=''; $chec3=''; $chec4=''; $chec5=''; $chec6=''; $chec7=''; 
  // if ($revisi) { $disabled='disabled'; }
  if ($revisi['doc_usulan_revisi']) $chec1='checked'; else $chec1='';
  if ($revisi['doc_matrix']) $chec2='checked'; else $chec2='';
  if ($revisi['doc_rka']) $chec3='checked'; else $chec3='';
  if ($revisi['doc_adk']) $chec4='checked'; else $chec4='';
  if ($revisi['doc_dukung_sepakat']) $chec5='checked'; else $chec5='';
  if ($revisi['doc_dukung_hal4']) $chec6='checked'; else $chec6='';
  if ($revisi['doc_dukung_lainnya']) $chec7='checked'; else $chec7='';
?>

<div class="col-sm-12 text-center" style="padding:0px;background:#3C8DBC;margin:10px 0px">
    <label for="checkbox-5" class="checkbox-custom-label" style="color:#FFF;padding:5px 15px">
        INFORMASI PEJABAT PENGUSUL REVISI ANGGARAN
    </label>
</div>
<div class="form-group jarak">
    <label class="col-sm-2 rev text-right">Pejabat : </label>
    <div class="col-sm-8" style="padding:0px">
        <input type="text" id="kl_pjb_jab" name="kl_pjb_jab" class="form-control" placeholder="Pejabat Unit Eselon I" required value="<?= $revisi['kl_pjb_jab'] ?>" >
    </div>
</div>

<div class="form-group jarak">
    <label class="col-sm-2 rev text-right">Nama Pejabat : </label>
    <div class="col-sm-4" style="padding:0px">
        <input type="text" id="kl_pjb_nama" name="kl_pjb_nama" class="form-control" placeholder="Nama Pejabat" required value="<?= $revisi['kl_pjb_nama'] ?>" >
    </div>
    <label class="col-sm-2 rev text-right">NIP/NRP : </label>
    <div class="col-sm-2" style="padding:0px">
        <input type="text" name="kl_pjb_nip" class="form-control" placeholder="NIP atau NRP" value="<?= $revisi['kl_pjb_nip'] ?>" >
    </div>
</div>
<div class="col-sm-12 text-center" style="padding:0px;background:#3C8DBC;margin:10px 0px">
    <label for="checkbox-5" class="checkbox-custom-label" style="color:#FFF;padding:5px 15px">
        KELENGKAPAN DOKUMEN USULAN REVISI ANGGARAN
    </label>
</div>
<div>
    <div class="form-group jarak">
        <label class="col-sm-2 rev text-right"></label>
        <div class="col-sm-8" style="padding:0px">
            <input id="chk-F01"  class="checkbox-custom" name="chk-F01" type="checkbox" <?= "$chec1 $disabled" ?> onclick="toggle('dok1')">
            <label for="chk-F01" class="checkbox-custom-label fdok">
                Surat usulan revisi yang ditandatangani pejabat eselon I 
            </label>
        </div>
    </div>
    <div class="form-group jarak" id="dok1" style="display:none">
        <label class="col-sm-2 rev text-right"></label>
        <div class="col-sm-8" style="padding:0px 0px 0px 30px">
            <input type="text" name="doc_usulan_revisi" id="doc_usulan_revisi" style="width: 100%" <?= "$disabled" ?> value="<?= $revisi['doc_usulan_revisi'] ?>">
            <input type="file" name="file1" id="file1" class="file" data-show-preview="false">
        </div>
    </div>
    <div class="form-group jarak">
        <label class="col-sm-2 rev text-right"></label>
        <div class="col-sm-8" style="padding:0px">
            <input id="chk-F02" class="checkbox-custom" name="chk-F02" type="checkbox" <?= "$chec2 $disabled" ?> onclick="toggle('dok2')">
            <label for="chk-F02" class="checkbox-custom-label fdok">
                Matriks perubahan (semula menjadi) 
            </label>
        </div>
    </div>
    <div class="form-group jarak" id="dok2" style="display:none">
        <label class="col-sm-2 rev text-right"></label>
        <div class="col-sm-8" style="padding:0px 0px 0px 30px">
            <input type="text" name="doc_matrix" id="doc_matrix" style="width: 100%" <?= "$disabled" ?> value="">
            <input type="file" name="file2" id="file2" class="file" data-show-preview="false">
        </div>
    </div>
    <div class="form-group jarak">
        <label class="col-sm-2 rev text-right"></label>
        <div class="col-sm-8" style="padding:0px">
            <input id="chk-F03" class="checkbox-custom" name="chk-F03" type="checkbox" <?= "$chec3 $disabled" ?> onclick="toggle('dok3')">
            <label for="chk-F03" class="checkbox-custom-label fdok">
                Rencana Kerja dan Anggaran (RKA) Satker
            </label>
        </div>
    </div>
    <div class="form-group jarak" id="dok3" style="display:none">
        <label class="col-sm-2 rev text-right"></label>
        <div class="col-sm-8" style="padding:0px 0px 0px 30px">
            <input type="text" name="doc_rka" id="doc_rka" style="width: 100%" <?= "$disabled" ?> value="">
            <input type="file" name="file3" id="file3" class="file" data-show-preview="false">
        </div>
    </div>
    <div class="form-group jarak">
        <label class="col-sm-2 rev text-right"></label>
        <div class="col-sm-8" style="padding:0px">
            <input id="chk-F04" class="checkbox-custom" name="chk-F04" type="checkbox" <?= "$chec4 $disabled" ?> onclick="toggle('dok4')">
            <label for="chk-F04" class="checkbox-custom-label fdok">
                Arsip Data Komputer (ADK) RKA-K/L DIPA revisi Satker
            </label>
        </div>
    </div>
    <div class="form-group jarak" id="dok4" style="display:none">
        <label class="col-sm-2 rev text-right"></label>
        <div class="col-sm-8" style="padding:0px 0px 0px 30px">
            <input type="text" name="doc_adk" id="doc_adk" style="width: 100%" <?= "$disabled" ?> value="">
            <input type="file" name="file4" id="file4" class="file" data-show-preview="false">
        </div>
    </div>
    <div class="form-group jarak">
        <label class="col-sm-2 rev text-right"></label>
        <div class="col-sm-8" style="padding:0px">
            <input id="chk-F05" class="checkbox-custom" name="chk-F05" type="checkbox" <?= "$chec5 $disabled" ?> onclick="toggle('dok5')">
            <label for="chk-F05" class="checkbox-custom-label fdok">
                Dokumen pendukung terkait sesuai hasil kesepakatan antara K/L dengan DJA dalam pembahasan usulan revisi anggaran
            </label>
        </div>
    </div>
    <div class="form-group jarak" id="dok5" style="display:none">
        <label class="col-sm-2 rev text-right"></label>
        <div class="col-sm-8" style="padding:0px 0px 0px 30px">
            <input type="text" name="doc_dukung_sepakat" id="doc_dukung_sepakat" style="width: 100%" <?= "$disabled" ?> value="">
            <input type="file" name="file5" id="file5" class="file" data-show-preview="false">
        </div>
    </div>
    <div class="form-group jarak">
        <label class="col-sm-2 rev text-right"></label>
        <div class="col-sm-8" style="padding:0px">
            <input id="chk-F06" class="checkbox-custom" name="chk-F06" type="checkbox" <?= "$chec6 $disabled" ?> onclick="toggle('dok6')">
            <label for="chk-F06" class="checkbox-custom-label fdok">
                Dokumen pendukung terkait perubahan/penghapusan catatan dalam halaman IV DIPA
            </label>
        </div>
    </div>
    <div class="form-group jarak" id="dok6" style="display:none">
        <label class="col-sm-2 rev text-right"></label>
        <div class="col-sm-8" style="padding:0px 0px 0px 30px">
            <input type="text" name="doc_dukung_hal4" id="doc_dukung_hal4" style="width: 100%" <?= "$disabled" ?> value="">
            <input type="file" name="file6" id="file6" class="file" data-show-preview="false">
        </div>
    </div>
    <div class="form-group jarak">
        <label class="col-sm-2 rev text-right"></label>
        <div class="col-sm-8" style="padding:0px">
            <input id="chk-F07" class="checkbox-custom" name="chk-F07" type="checkbox" <?= "$chec7 $disabled" ?> onclick="toggle('dok7')">
            <label for="chk-F07" class="checkbox-custom-label fdok">
                Dokumen pendukung terkait lainnya
            </label>
        </div>
    </div>
    <div class="form-group jarak" id="dok7" style="display:none">
        <label class="col-sm-2 rev text-right"></label>
        <div class="col-sm-8" style="padding:0px 0px 0px 30px">
            <input type="text" name="doc_dukung_lainnya" id="doc_dukung_lainnya" style="width: 100%" <?= "$disabled" ?> value="">
            <input type="file" name="file7" id="file7" class="file" data-show-preview="false">
        </div>
    </div>
    <div class="form-group jarak">
        <label class="col-sm-2 rev text-right">Catatan : </label>
        <div class="col-sm-8" style="padding:0px">
            <textarea name="pus_catatan" class="form-control" placeholder="Catatan" <?= $disabled ?> rows="5"><?= $revisi['pus_catatan'] ?></textarea> 
        </div>
    </div>
</div>

<script type="text/javascript">
    $('#file1, #file2, #file3, #file5, #file6, #file7').fileinput({
        uploadExtraData:  function(previewId, index) {
            var data = { rev_id : $("#rev_id").val(), name: $(this).attr('id') };
            return data;
        },
        uploadUrl: "<?php echo site_url('revisi/fileupload_form1') ?>",
        allowedFileExtensions : ['17','jpg','xlsx','doc','docx','rtf','pdf','rar','zip'],
        msgInvalidFileExtension: "Invalid extension file !",
        overwriteInitial: true,
        maxFileCount: 5
        //maxFileSize: 30000
    });

    $('#file4').fileinput({
        uploadExtraData:  function(previewId, index) {
            var data = { rev_id : $("#rev_id").val(), name: $(this).attr('id') };
            return data;
        },
        uploadUrl: "<?php echo site_url('revisi/fileupload_form1') ?>",
        allowedFileExtensions : ['16','s16','17','s17','18','s18','19','s19','rar','zip'],
        overwriteInitial: true
        //maxFileSize: 30000
    });

    $('#file1, #file2, #file3, #file4, #file5, #file6, #file7').on('filebatchuploadsuccess', function(event, data, extra) {
        var extra = data.extra, response = data.response;
        if (extra.name=='file1') name = 'doc_usulan_revisi';
        if (extra.name=='file2') name = 'doc_matrix';
        if (extra.name=='file3') name = 'doc_rka';
        if (extra.name=='file4') name = 'doc_adk';
        if (extra.name=='file5') name = 'doc_dukung_sepakat';
        if (extra.name=='file6') name = 'doc_dukung_hal4';
        if (extra.name=='file7') name = 'doc_dukung_lainnya';

        $('#'+name).val( response.uploaded );
        $('#'+name).attr('type','text'); $('#'+extra.name).css('display', 'none');
    });
    $('#file1, #file2, #file3, #file4, #file5, #file6, #file7').on('filebatchuploaderror', function(event, data) {
        var response = data.response;
        alert('Gagal upload !');
    });
</script>