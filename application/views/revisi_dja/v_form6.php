<style media="screen">
.checkbox-custom-label{
    display: inline-block;vertical-align: middle;margin: 0px;}
.inforev{padding-top:0px;padding-left:0px;}
.infotext{font-weight: normal}
.infojarak{margin-bottom:0px;}
.jarak{margin-bottom:8px;}
</style>


<div class="row" ng-app="app" >
    <div class="col-md-12">
        <div class="box box-widget">
            <div class="box-header with-border">
              <h3 style="margin:0px">F6. Kelengkapan Dokumen</h3>
            </div>
            <form name="myForm" class="form-horizontal" action="<?=site_url('revisi_dja/crud_form6')?>" onsubmit="" method="post" 
            role="form">
                <div class="box-body">
                    <?php $this->load->view("revisi_dja/v_info"); ?>
                </div>
                <div class="box-footer">
                    <div class="col-sm-12 pull-right">
                        <div class="pull-right">
                        <input type="hidden" name="rev_id"  id="" value="<?php echo $rev_id ?>" >
                            <button type="submit" id="proses" class="btn btn-info">Proses Kelengkapan Dokumen</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
