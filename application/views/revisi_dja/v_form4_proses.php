<link href="<?=base_url('assets/bootstrap/css/bootstrap-datetimepicker.min.css');?>" rel="stylesheet" type="text/css" />
<script src="<?=base_url('assets/plugins/datepicker/moment.min.js');?>" type="text/javascript"></script>
<script src="<?=base_url('assets/plugins/datepicker/id.js');?>" type="text/javascript"></script>
<script src="<?=base_url('assets/bootstrap/js/bootstrap-datetimepicker.min.js');?>" type="text/javascript"></script>
<link href="<?=base_url(); ?>assets/plugins/uploadfile/fileinput.css" rel="stylesheet">
<script src="<?=base_url();?>assets/plugins/uploadfile/fileinput.min.js" type="text/javascript"></script>

<style media="screen">
.checkbox-custom, .radio-custom{opacity: 0;position: absolute;}
.checkbox-custom, .checkbox-custom-label, .radio-custom, .radio-custom-label{
    display: inline-block;vertical-align: middle;margin: 0px;}
.checkbox-custom-label, .radio-custom-label {position: relative;}
.checkbox-custom + .checkbox-custom-label:before, .radio-custom + .radio-custom-label:before{
    content: '';background: #fff;border: 1px solid #DFDFDF;display: inline-block;vertical-align: middle;
    width: 20px;height: 20px;padding: 2px;margin-right: 5px;text-align: center;}
.checkbox-custom:checked + .checkbox-custom-label:before{background: #3C8DBC;}
.radio-custom + .radio-custom-label:before{border-radius: 50%;}
.radio-custom:checked + .radio-custom-label:before {background: #3C8DBC;}
.checkbox-custom:focus + .checkbox-custom-label, .radio-custom:focus + .radio-custom-label {outline: 0px solid #ddd;}
.inforev{padding-top:0px;padding-left:0px;}
.infotext{font-weight: normal}
.infojarak{margin-bottom:0px;}
.jarak{margin-bottom:8px;}
.fdok{padding-left:30px;text-indent:-30px}
</style>

<script>
function toggle_opt($opt) {
 if( $opt == 'setuju' ){
     document.getElementById("persetujuan").style.display = '';
 }else{
     document.getElementById("persetujuan").style.display = 'none';
    }
}
</script>

<script type="text/javascript">
    $(function () {
        $('.datetimepicker1').datetimepicker({
            format : 'YYYY-MM-DD',
        });
    });
</script>

<?php
    $disb = ''; $terima = ''; $tolak = ''; $sesi=$this->session->userdata('idusergroup');$tidak = ''; $ya = '';
    if ( ($proses['t4_status']=='2') And ($sesi <> '001') ) { $disb = 'disabled'; $terima = 'checked'; $tidak = 'checked'; $no = $proses['t4_ba_no']; $tgl= $proses['t4_ba_tgl']; $ba=$proses['t4_catatan']; $file= $proses['t4_ba_file']; $catatan = $proses['t4_perbaikan_catatan']; }
    if ( ($proses['t4_status']=='1') And ($sesi <> '001') ) { $disb = ''; $tolak = ''; $ya = ''; $no = ''; $tgl= ''; $ba=''; $file= ''; $catatan = '' ;  }
    if ( ($proses['t4_status']=='2') And ($sesi == '001') ) {  $terima = 'checked'; }
    if ( ($proses['t4_status']=='0') And ($sesi == '001') ) {  $tolak = 'checked'; }
?>

<div class="row" ng-app="app" >
    <div class="col-md-12">
        <div class="box box-widget">
            <div class="box-header with-border">
              <h3 style="margin:0px">F4. Penelaahan</h3>
            </div>
            <form name="myForm" class="form-horizontal" action="<?=site_url('Revisi_dja/crud_form4_proses') ?>" onsubmit="" method="post" role="form">
                <div class="box-body">
                    <?php $this->load->view('revisi_dja/v_info'); ?>

                    <div class="col-sm-12 text-center" style="padding:0px;background:#3C8DBC;margin:5px 0px 15px 0px">
                        <label class="checkbox-custom-label" style="color:#FFF;padding:5px 15px">
                            HASIL PENELAAHAN REVISI ANGGARAN
                        </label>
                    </div>

                    <div style="margin-top:20px">
                        <div style="padding-top:20px">
                            <div class="form-group jarak">
                                <label class="col-sm-2 rev text-right">Berita Acara : </label>
                                <div class="col-sm-8" style="padding:0px">
                                    <textarea class="form-control" placeholder="Berita Acara Hasil Penelaahan" id="t4_catatan" value=" " name="t4_catatan" rows="5" <?php echo $disb ?>><?php echo $ba ?></textarea>
                                </div>
                            </div>
                            <div id="nd" style="display:">
                                <div class="form-group jarak">
                                    <label class="col-sm-2 rev text-right">Nomor BA : </label>
                                    <div class="col-sm-3" style="padding:0px">
                                        <input name="t4_ba_no" class="form-control" placeholder="Nomor Surat" id="" value="<?php echo $no ?>" <?php echo $disb ?>>
                                    </div>
                                    <label class="col-sm-2 rev text-right"  style="padding-right:0px">Tanggal BA : </label>
                                    <div class="col-sm-3" style="padding-right:0px">
                                        <div class="input-group date datetimepicker1">
                                            <input name="t4_ba_tgl" id="t4_ba_tgl" class="form-control" placeholder="Tanggal Surat" <?php echo $disb ?> value='<?php echo $tgl ?>'>
                                            <span class="input-group-addon">
                                                <i class="fa fa-calendar text-primary"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group jarak">
                                    <label class="col-sm-2 rev text-right">File BA :</label>
                                    <div class="col-sm-8" style="padding:0px">
                                        <?php if ($proses['t4_status']=='1') { ?>
                                        <input type="text" name="t4_ba_file" id="t4_ba_file" style="width: 100%" value="" >
                                        <input id="form4" name="form4" type="file" class="file" data-show-preview="false" >
                                        <?php } else { ?>
                                        <input name="t4_file" value="<?php echo $file ?>" <?php echo "$disb" ?> class="form-control" >   
                                        <?php } ?>
                                    </div>
                                </div>
                                <div class="form-group jarak">
                                    <label class="col-sm-2 rev text-right">Persetujuan : </label>
                                    <div class="col-sm-8" style="padding:0px">
                                        <label class="col-sm-12" style="padding-left:0px">
                                            <input id="radio-1" class="radio-custom" name="radio-group1" type="radio"<?php echo "$tidak $disb" ?> checked onclick="toggle_opt('tidak')">
                                            <label for="radio-1" class="radio-custom-label">Tanpa Persetujuan Menteri Keuangan</label>
                                        </label>
                                        <label class="col-sm-12" style="padding-left:0px">
                                            <input id="radio-2" class="radio-custom" name="radio-group1" type="radio" <?php echo "$ya  $disb" ?> onclick="toggle_opt('setuju')">
                                            <label for="radio-2" class="radio-custom-label">Dengan Persetujuan Menteri Keuangan</label>
                                        </label>
                                    </div>
                                </div>

                                <div id="persetujuan" style="display:none">
                                    <div class="form-group jarak">
                                        <label class="col-sm-2 rev text-right">No. Surat : </label>
                                        <div class="col-sm-3" style="padding:0px">
                                            <input name="t4_menteri_no" class="form-control" placeholder="Nomor Surat" id="" value="" <?php echo $disb ?> >
                                        </div>
                                        <label class="col-sm-2 rev text-right"  style="padding-right:0px">Tanggal Surat : </label>
                                        <div class="col-sm-3" style="padding-right:0px">
                                            <div class="input-group date datetimepicker1">
                                                <input name="t4_menteri_tgl" id="t4_menteri_tgl" type="text" class="form-control" placeholder="Tanggal Surat" <?php echo $disb ?>>
                                                <span class="input-group-addon">
                                                    <i class="fa fa-calendar text-primary"></i>
                                                </span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group jarak">
                                        <label class="col-sm-2 rev text-right">File Surat :</label>
                                        <div class="col-sm-8" style="padding:0px">
                                           
                                            <input type="hidden" name="t4_menteri_file" id="t4_menteri_file">
                                            <input id="form4_menteri" name="form4_menteri" type="file" class="file" data-show-preview="false" <?php echo $disb ?>>
                                            
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group jarak">
                                    <label class="col-sm-2 rev text-right">Perbaikan : </label>
                                    <div class="col-sm-8" style="padding:0px">
                                        <label class="col-sm-12" style="padding-left:0px">
                                            <input id="radio-3" class="radio-custom" name="radio-group2" value="1" type="radio" checked <?php echo "$tidak $disb" ?>>
                                            <label for="radio-3" class="radio-custom-label">Tanpa Perbaikan Dokumen</label>
                                        </label>
                                        <label class="col-sm-12" style="padding-left:0px">
                                            <input id="radio-4" class="radio-custom" name="radio-group2" type="radio" value="2" <?php echo "$ya $disb" ?>>
                                            <label for="radio-4" class="radio-custom-label">Dengan Perbaikan Dokumen</label>
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group jarak">
                                    <label class="col-sm-2 rev text-right">Catatan : </label>
                                    <div class="col-sm-8" style="padding:0px">
                                        <textarea class="form-control" placeholder="Catatan" name="t4_perbaikan_catatan" rows="5" <?php echo $disb ?> > <?php echo $catatan ?> </textarea>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <div class="col-sm-12 pull-right">
                        <div class="pull-right">
                            <input type="hidden" name="rev_id" id="rev_id" value="<?php echo $rev_id ?>">
                            <button type="submit" id="proses" onclick="<?= $rev_id ?>" class="btn btn-primary" <?php echo $disb ?>>Proses </button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>



<!-- New -->
<script type="text/javascript">
    $('#form4').fileinput({
        uploadExtraData: function(previewID,index){
            var data = {rev_id : $("#rev_id").val(), name: 'form4'};
            return data;
        },
        uploadUrl: "<?php echo site_url('Revisi_dja/fileupload_form4')  ?>",
        allowedFileExtensions : ['pdf'],
        msgInvalidFileExtension: "Invalid extension file !",
        overwriteInitial: true,
        maxFileCount: 1,
        maxFileSize: 5000
    });
    $('#form4').on('filebatchuploadsuccess', function(event, data, extra) {
      var extra = data.extra, response = data.response;
      $('#t4_ba_file').val( response.uploaded );
      $('#t4_ba_file').attr('type','text'); $('#form4').css('display', 'none');
    });
    $('#form4').on('filebatchuploaderror', function(event, data) {
      var response = data.response;
      alert('Gagal upload !');
    });
</script>

<!-- New -->
<script type="text/javascript">
    $('#form4_menteri').fileinput({
        uploadExtraData: function(previewID,index){
            var data = {rev_id : $("#rev_id").val(), name: 'form4_menteri'};
            return data;
        },
        uploadUrl: "<?php echo site_url('Revisi_dja/fileupload_form4')  ?>",
        allowedFileExtensions : ['pdf'],
        msgInvalidFileExtension: "Invalid extension file !",
        overwriteInitial: true,
        maxFileCount: 1,
        maxFileSize: 5000
    });
    $('#form4_menteri').on('filebatchuploadsuccess', function(event, data, extra) {
      var extra = data.extra, response = data.response;
      $('#t4_menteri_file').val( response.uploaded );
      $('#t4_menteri_file').attr('type','text'); $('#form4_menteri').css('display', 'none');
    });
    $('#form4_menteri').on('filebatchuploaderror', function(event, data) {
      var response = data.response;
      alert('Gagal upload !');
    });
</script>


<script type="text/javascript">
    function (rev_id) {
        window.location.href = "<?php echo site_url('revisi_45/fileupload_form4') ?>" +'/'+ rev_id;
    }
</script>
