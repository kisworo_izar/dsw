<link href="<?=base_url('assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css');?>" rel="stylesheet" type="text/css" />
<script src="<?=base_url('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js');?>" type="text/javascript"></script>

<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;border:solid 1px #ABADAF;}
.tg td{padding:5px;overflow:hidden;word-break:normal; border: solid 1px #ABADAF}
.tg th{font-weight:normal;padding:5px 5px;overflow:hidden;word-break:normal; border: solid 1px #ABADAF}
.tg .tg-i4jz{font-weight:bold;background-color:#bbdaff;text-align:right;vertical-align:center}
.tg .tg-u227{font-weight:bold;background-color:#bbdaff;text-align:center}
.tg .tg-lqy6{text-align:right;vertical-align:center}
.tg .tg-ofqg{font-weight:bold;background-color:#bbdaff;text-align:center;vertical-align:center}
.tg .tg-yw4l{vertical-align:top}
</style>


<div class="row">
  <div class="col-md-12">
    <div class="box box-widget" style="margin:0px">
      <div class="box-header with-border">

        <div class="span5 col-md-4 pull-right" id="sandbox-container" style="border:0px; padding:0px; height:25px">
          <div class="input-daterange input-group" id="datepicker">
            <input type="text" class="input-sm form-control text-center" value="<?= $start ?>" name="start" id="start" style="padding-top:0px; padding-bottom:0px;  height:25px">
            <span class="input-group-addon" style="border:0px;"> s.d. </span>
            <input type="text" class="input-sm form-control text-center" value="<?= $end ?>" name="end" id="end" style="padding-top:0px; padding-bottom:0px; height:25px">
          </div>
        </div>

      </div>
    </div>
  </div>
</div>


<div class="row">
  <div class="col-md-12">

    <div class="box box-widget">
      <div class="box-header with-border">
        <div style="text-align:center">
          <h4 style="margin:5px">Data Revisi Anggaran</h4>
          <h5 style="margin:5px">Tahun Anggaran 2017</h5>
        </div>

        <table class="tg" style="width:100%;">
        <tr>
          <th class="tg-u227" rowspan="2">Unit Eselon I</th>
          <th class="tg-ofqg" rowspan="2">Jumlah<br>Revisi</th>
          <th class="tg-ofqg" colspan="5">Revisi &lt; 5 Hari Kerja</th>
          <th class="tg-ofqg">Tepat Waktu</th>
          <th class="tg-ofqg" colspan="4">Revisi &gt; 5 Hari Kerja</th>
        </tr>
        <tr>
          <td class="tg-ofqg">1 Hari</td>
          <td class="tg-ofqg">2 Hari</td>
          <td class="tg-ofqg">3 Hari</td>
          <td class="tg-ofqg">4 Hari</td>
          <td class="tg-ofqg">Total</td>
          <td class="tg-ofqg">5 Hari</td>
          <td class="tg-ofqg">6 Hari</td>
          <td class="tg-ofqg">7 Hari</td>
          <td class="tg-ofqg">&gt;7 Hari</td>
          <td class="tg-ofqg">Total</td>
        </tr>

        <?php foreach ($rekap as $key=>$row) { 
          if ($key <> 'total') { ?>
          <tr>
            <td class="tg-yw4l"><?= $row['nmso'] ?></td>
            <td class="tg-lqy6"><?= $row['jml'] ?></td>
            <td class="tg-lqy6"><?= $row['h1'] ?></td>
            <td class="tg-lqy6"><?= $row['h2'] ?></td>
            <td class="tg-lqy6"><?= $row['h3'] ?></td>
            <td class="tg-lqy6"><?= $row['h4'] ?></td>
            <td class="tg-lqy6"><?= $row['jml4'] ?></td>
            <td class="tg-lqy6"><?= $row['h5'] ?></td>
            <td class="tg-lqy6"><?= $row['h6'] ?></td>
            <td class="tg-lqy6"><?= $row['h7'] ?></td>
            <td class="tg-lqy6"><?= $row['h8'] ?></td>
            <td class="tg-lqy6"><?= $row['jml8'] ?></td>
          </tr>
        
        <?php } else { ?>
          <tr>
            <td class="tg-ofqg"><?= $row['nmso'] ?></td>
            <td class="tg-i4jz"><?= $row['jml'] ?></td>
            <td class="tg-i4jz"><?= $row['h1'] ?></td>
            <td class="tg-i4jz"><?= $row['h2'] ?></td>
            <td class="tg-i4jz"><?= $row['h3'] ?></td>
            <td class="tg-i4jz"><?= $row['h4'] ?></td>
            <td class="tg-i4jz"><?= $row['jml4'] ?></td>
            <td class="tg-i4jz"><?= $row['h5'] ?></td>
            <td class="tg-i4jz"><?= $row['h6'] ?></td>
            <td class="tg-i4jz"><?= $row['h7'] ?></td>
            <td class="tg-i4jz"><?= $row['h8'] ?></td>
            <td class="tg-i4jz"><?= $row['jml8'] ?></td>
          </tr>
        <?php } } ?>

        </table>
        <h6><b>Catatan: </b><br>Perhitungan Revisi Anggaran dimulai pada saat dokumen telah diterima secara lengkap</h6>
      </div>
    </div>

  </div>
</div>


<script type="text/javascript">
  $('#sandbox-container .input-daterange').datepicker({
    format: "dd-mm-yyyy",
    language: "id",
    orientation: "bottom auto",
    autoclose: true,
    todayHighlight: true
  }).on('changeDate', function() {
    var sta = $('#start').val();
    var end = $('#end').val();
    window.location.href = "<?= site_url('revisi/report') ?>" +'/'+ sta +"/"+ end;
    });
</script>