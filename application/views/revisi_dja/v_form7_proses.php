<link href="<?=base_url('assets/bootstrap/css/bootstrap-datetimepicker.min.css');?>" rel="stylesheet" type="text/css" />
<script src="<?=base_url('assets/plugins/datepicker/moment.min.js');?>" type="text/javascript"></script>
<script src="<?=base_url('assets/plugins/datepicker/id.js');?>" type="text/javascript"></script>
<script src="<?=base_url('assets/bootstrap/js/bootstrap-datetimepicker.min.js');?>" type="text/javascript"></script>
<link href="<?=base_url(); ?>assets/plugins/uploadfile/fileinput.css" rel="stylesheet">
<script src="<?=base_url();?>assets/plugins/uploadfile/fileinput.min.js" type="text/javascript"></script>

<style media="screen">
.checkbox-custom, .radio-custom{opacity: 0;position: absolute;}
.checkbox-custom, .checkbox-custom-label, .radio-custom, .radio-custom-label{
    display: inline-block;vertical-align: middle;margin: 0px;}
.inforev{padding-top:0px;padding-left:0px;}
.infotext{font-weight: normal}
.infojarak{margin-bottom:0px;}
.jarak{margin-bottom:8px;}
.fdok{padding-left:30px;text-indent:-30px}
</style>

<script type="text/javascript">
    $(function () {
        $('.datetimepicker1').datetimepicker({
            format : 'YYYY-MM-DD',
            locale : 'id'
        });
    });
</script>



<div class="row" ng-app="app" >
    <div class="col-md-12">
        <div class="box box-widget">
            <div class="box-header with-border">
              <h3 style="margin:0px">F7. Penetapan Revisi</h3>
            </div>
            <form name="myForm" class="form-horizontal" action="<?=site_url('Revisi_dja/crud_form7_proses')?>" onsubmit="" method="post" role="form">
                <div class="box-body">
                    <?php $this->load->view('revisi_dja/v_info'); ?>

                    <div class="col-sm-12 text-center" style="padding:0px;background:#3C8DBC;margin:5px 0px 15px 0px">
                        <label class="checkbox-custom-label" style="color:#FFF;padding:5px 15px">
                            PENETAPAN DAN PENGESAHAN REVISI ANGGARAN
                        </label>
                    </div>

                    <div style="margin-top:20px">
                        <div style="padding-top:20px">
                        <!-- tmbahan -->
                            <div class="form-group jarak">
                                <label class="col-sm-2 rev text-right">Revisi Ke : </label>
                                <div class="col-sm-8" style="padding:0px">
                                    <input name="rev_ke" class="form-control" placeholder="Revisi ke" id="" value="" >
                                </div>
                            </div>
                            <div class="form-group jarak">
                                <label class="col-sm-2 rev text-right">No. Nota Dinas : </label>
                                <div class="col-sm-3" style="padding:0px">
                                    <input name="t7_nd_no" class="form-control" placeholder="Nomor Nota Dinas" id="" value="" >
                                </div>
                                <label class="col-sm-2 rev text-right"  style="padding-right:0px">Tanggal Nota Dinas : </label>
                                <div class="col-sm-3" style="padding-right:0px">
                                    <div class="input-group date datetimepicker1">
                                        <input name="t7_nd_tgl" id="tglsurat" type="text" class="form-control" placeholder="Tanggal Surat Penetapan">
                                        <span class="input-group-addon">
                                            <i class="fa fa-calendar text-primary"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group jarak">
                                <label class="col-sm-2 rev text-right">File Nota Dinas :</label>
                                <div class="col-sm-8" style="padding:0px">
                                    <input type="hidden" name="t7_nd_file" id="t7_nd_file" value="">
                                    <input id="t7_nd_file_u" name="t7_nd_file_u" type="file" class="file" data-show-preview="false">
                                </div>
                            </div>

                        <!-- akhir tambahan -->

                            <div class="form-group jarak">
                                <label class="col-sm-2 rev text-right">No. Surat : </label>
                                <div class="col-sm-3" style="padding:0px">
                                    <input name="t7_sp_no" class="form-control" placeholder="Nomor Nota Dinas " id="" value="" >
                                </div>
                                <label class="col-sm-2 rev text-right"  style="padding-right:0px">Tanggal Surat : </label>
                                <div class="col-sm-3" style="padding-right:0px">
                                    <div class="input-group date datetimepicker1">
                                        <input name="t7_sp_tgl" id="tglsurat" type="text" class="form-control" placeholder="Tanggal Nota Dinas">
                                        <span class="input-group-addon">
                                            <i class="fa fa-calendar text-primary"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group jarak">
                                <label class="col-sm-2 rev text-right">File Surat :</label>
                                <div class="col-sm-8" style="padding:0px">
                                    <input type="hidden" name="t7_sp_file" id="t7_sp_file" value="">
                                    <input id="t7_sp_file_u" name="t7_sp_file_u" type="file" class="file" data-show-preview="false" placeholder="File Surat">
                                </div>
                            </div>

                            <div class="form-group jarak">
                                <label class="col-sm-2 rev text-right">Catatan : </label>
                                <div class="col-sm-8" style="padding:0px">
                                    <textarea class="form-control" placeholder="Catatan Penetapan" name="t7_catatan" rows="5"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="box-footer">
                    <div class="col-sm-12 pull-right">
                        <div class="pull-right">
                            <input type="hidden" name="rev_id"  id="" value="<?php echo $rev_id ?>" >
                            <button type="submit" id="proses" class="btn btn-primary">Proses </button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">
    $("#t7_nd_file_u").fileinput({
        uploadUrl: "<?php echo site_url("revisi_7/fileupload_form7/t7_nd_file_u/$rev_id") ?>",
        allowedFileExtensions : ['17','jpg','xlsx','doc','docx','rtf','pdf'],
        overwriteInitial: true,
        maxFileSize: 1000,
        slugCallback: function(filename) {
            return filename.replace('(', '_').replace(']', '_');
        }
    });
    $("#t7_nd_file_u").on('filebatchuploadcomplete', function(event, files, extra) {
        var id = event.target.id;
        var name = '';
        if (id=='t7_nd_file_u') name = 't7_nd_file';
        $("#"+name).val( $("#"+id).val() );
        alert($("#"+id).val());
    });
</script>

<script type="text/javascript">
    $("#t7_sp_file_u").fileinput({
        uploadUrl: "<?php echo site_url("revisi_7/fileupload_form7/t7_sp_file_u/$rev_id") ?>",
        allowedFileExtensions : ['17','jpg','xlsx','doc','docx','rtf','pdf'],
        overwriteInitial: true,
        maxFileSize: 1000,
        slugCallback: function(filename) {
            return filename.replace('(', '_').replace(']', '_');
        }
    });
    $("#t7_sp_file_u").on('filebatchuploadcomplete', function(event, files, extra) {
        var id = event.target.id;
        var name = '';
        if (id=='t7_sp_file_u') name = 't7_sp_file';
        $("#"+name).val( $("#"+id).val() );
        alert($("#"+id).val());
    });
</script>

 <!-- catatan
<!-- kasih tanggal penetapan manual dan revisi ke -->