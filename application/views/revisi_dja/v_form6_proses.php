<link href="<?=base_url('assets/bootstrap/css/bootstrap-datetimepicker.min.css');?>" rel="stylesheet" type="text/css" />
<script src="<?=base_url('assets/plugins/datepicker/moment.min.js');?>" type="text/javascript"></script>
<script src="<?=base_url('assets/plugins/datepicker/id.js');?>" type="text/javascript"></script>
<script src="<?=base_url('assets/bootstrap/js/bootstrap-datetimepicker.min.js');?>" type="text/javascript"></script>
<link href="<?=base_url(); ?>assets/plugins/uploadfile/fileinput.css" rel="stylesheet">
<script src="<?=base_url();?>assets/plugins/uploadfile/fileinput.min.js" type="text/javascript"></script>

<style media="screen">
.checkbox-custom, .radio-custom{opacity: 0;position: absolute;}
.checkbox-custom, .checkbox-custom-label, .radio-custom, .radio-custom-label{
    display: inline-block;vertical-align: middle;margin: 0px;}
.checkbox-custom-label, .radio-custom-label {position: relative;}
.checkbox-custom + .checkbox-custom-label:before, .radio-custom + .radio-custom-label:before{
    content: '';background: #fff;border: 1px solid #DFDFDF;display: inline-block;vertical-align: middle;
    width: 20px;height: 20px;padding: 2px;margin-right: 5px;text-align: center;}
.checkbox-custom:checked + .checkbox-custom-label:before{background: #3C8DBC;}
.radio-custom + .radio-custom-label:before{border-radius: 50%;}
.radio-custom:checked + .radio-custom-label:before {background: #3C8DBC;}
.checkbox-custom:focus + .checkbox-custom-label, .radio-custom:focus + .radio-custom-label {outline: 0px solid #ddd;}
.inforev{padding-top:0px;padding-left:0px;}
.infotext{font-weight: normal}
.infojarak{margin-bottom:0px;}
.jarak{margin-bottom:8px;}
.fdok{padding-left:30px;text-indent:-30px}
</style>
<script type="text/javascript">
    $(function () {
        $('.datetimepicker1').datetimepicker({
            format : 'YYYY-MM-DD',
            locale : 'id'
        });
    });
</script>

<?php $req =' ' ?>
<script>
function toggle_opt($opt) {
switch ($opt) {
    case 'sesuai':
        document.getElementById("hasil").style.display = 'none';
        document.getElementById("proses").innerHTML = 'Proses';
        document.getElementById("t6_surat_file").required = false;
        $('#proses').removeClass('btn-info');
        $('#proses').addClass('btn-primary');
        break;
    case 'tidaksesuai':
        document.getElementById("hasil").style.display = 'none';
        document.getElementById("proses").innerHTML = 'Proses';
        document.getElementById("t6_surat_file").required = false;
        $('#proses').removeClass('btn-info');
        $('#proses').addClass('btn-primary');
        break;
    case 'tidaklengkap':
        document.getElementById("hasil").style.display = '';
        document.getElementById("proses").innerHTML = 'Proses Surat Permintaan';
        document.getElementById("t6_surat_file").required = false;
        $('#proses').removeClass('btn-primary');
        $('#proses').addClass('btn-info');
        break;
    }
}
</script>

<?php
    $disb = ''; $terima = ''; $tolak = ''; $sesi=$this->session->userdata('idusergroup');$tidak = ''; $ya = '';
    if ( ($proses['t6_status']=='2') And ($sesi <> '001') ) { $disb = 'disabled'; $terima = 'checked'; $tidak = 'checked'; $isi = $proses['t6_catatan']; }
    if ( ($proses['t6_status']=='0') And ($sesi <> '001') ) { $disb = 'disabled'; $tolak = 'checked'; $ya = 'checked'; $isi = $proses['t6_catatan']; }
    if ( ($proses['t6_status']=='1') And ($sesi <> '001') ) { $disb = ''; $tolak = ''; $ya = ''; $isi = ''; }
    if ( ($proses['t6_status']=='1') And ($sesi == '001') ) { $disb = ''; $tolak = ''; $ya = ''; $isi = ''; }
    if ( ($proses['t6_status']=='2') And ($sesi == '001') ) {  $terima = 'checked'; $isi = $proses['t6_catatan']; }
    if ( ($proses['t6_status']=='0') And ($sesi == '001') ) {  $tolak = 'checked';  $isi = $proses['t6_catatan']; }
?>

<div class="row" ng-app="app" >
    <div class="col-md-12">
        <div class="box box-widget">
            <div class="box-header with-border">
              <h3 style="margin:0px">F6. Kelengkapan Dokumen</h3>
            </div>
            <form name="myForm" class="form-horizontal" action="<?=site_url('revisi_dja/crud_form6_proses')?>" onsubmit="" method="post" role="form">
                <div class="box-body">
                    <?php $this->load->view('revisi_dja/v_info'); ?>
                     <?php $this->load->view('revisi_dja/v_info_files'); ?>

                    

                            <div class="col-sm-12 text-center" style="padding:0px;background:#3C8DBC;margin:5px 0px 15px 0px">
                                <label class="checkbox-custom-label" style="color:#FFF;padding:5px 15px">
                                    HASIL PENELITIAN KELENGKAPAN PERBAIKAN DOKUMEN USULAN REVISI
                                </label>
                            </div>
                            <div class="form-group jarak">
                                <label class="col-sm-2 rev text-right">Hasil : </label>
                                <div class="col-sm-8" style="padding:0px">
                                    <label class="col-sm-12" style="padding-left:0px">
                                        <input id="radio-sesuai" class="radio-custom" value="1" name="radio-group" type="radio" checked  onclick="toggle_opt('sesuai')">
                                        <label for="radio-sesuai" class="radio-custom-label">Lengkap Sesuai</label>
                                    </label>
                                    <label class="col-sm-12" style="padding-left:0px">
                                        <input id="radio-tidaksesuai" class="radio-custom" value="2" name="radio-group" type="radio"  onclick="toggle_opt('tidaksesuai')">
                                        <label for="radio-tidaksesuai" class="radio-custom-label">Lengkap tidak sesuai / Tidak Lengkap tanpa Perbaikan / Ditolak</label>
                                    </label>
                                    <label class="col-sm-12" style="padding-left:0px">
                                        <input id="radio-tidaklengkap" class="radio-custom" value="3" name="radio-group" type="radio"  onclick="toggle_opt('tidaklengkap')">
                                        <label for="radio-tidaklengkap" class="radio-custom-label">Tidak Lengkap butuh perbaikan</label>
                                    </label>
                                </div>
                            </div>
                            <div>
                                <div id="hasil" style="display:none">
                                    <div class="form-group jarak">
                                        <label class="col-sm-2 rev text-right">No. Surat : </label>
                                        <div class="col-sm-3" style="padding:0px">
                                            <input name="t6_surat_no" class="form-control" placeholder="Nomor Surat" id="" value="" >
                                        </div>
                                        <label class="col-sm-2 rev text-right"  style="padding-right:0px">Tanggal Surat : </label>
                                        <div class="col-sm-3" style="padding-right:0px">
                                            <div class="input-group date datetimepicker1">
                                                <input name="t6_surat_tgl" id="tglsurat" type="text" class="form-control" placeholder="Tanggal Surat">
                                                <span class="input-group-addon">
                                                    <i class="fa fa-calendar text-primary"></i>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                     <div class="form-group jarak">
                                        <label class="col-sm-2 rev text-right">File Surat :</label>
                                        <div class="col-sm-8" style="padding:0px">
                                            <input type="text" name="t6_surat_file" id="t6_surat_file"  value="">
                                            <input type="file" name="form6" id="form6" class="file" data-show-preview="false">
                                        </div>
                                    </div>   
                                </div>
                            </div>
                            <div class="form-group jarak">
                                <label class="col-sm-2 rev text-right">Catatan : </label>
                                <div class="col-sm-8" style="padding:0px">
                                    <textarea class="form-control" placeholder="Catatan hasil penelitian dokumen" name="t6_catatan" rows="5"><?php echo "$isi" ?></textarea>
                                </div>
                            </div>
                       
                
                            <div class="box-footer">
                                <div class="col-sm-12 pull-right">
                                    <div class="pull-right">
                                       <input type="hidden" name="rev_id"  id="rev_id" value="<?php echo $rev_id ?>" >
                                        <button type="submit" id="proses" class="btn btn-primary">Proses </button>
                                    </div>
                                </div>
                            </div>
                </form>
          </div>
    </div>
</div>

<script type="text/javascript">
    $('#form6').fileinput({
        uploadExtraData: function(previewID,index){
            var data = {rev_id : $("#rev_id").val(), name: 'form6'};
            return data;
        },
        uploadUrl: "<?php echo site_url('revisi_dja/fileupload_form6')  ?>",
        allowedFileExtensions : ['pdf'],
        msgInvalidFileExtension: "Invalid extension file !",
        overwriteInitial: true,
        maxFileCount: 1,
        maxFileSize: 5000
    });
    $('#form6').on('filebatchuploadsuccess', function(event, data, extra) {
      var extra = data.extra, response = data.response;
      $('#t6_surat_file').val( response.uploaded );
      $('#t6_surat_file').attr('type','text'); $('#form6').css('display', 'none');
    });
    $('#form6').on('filebatchuploaderror', function(event, data) {
      var response = data.response;
      alert('Gagal upload !');
    });
</script>