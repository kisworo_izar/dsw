<link href="<?=base_url(); ?>assets/plugins/uploadfile/fileinput.css" rel="stylesheet">
<script src="<?=base_url();?>assets/plugins/uploadfile/fileinput.min.js" type="text/javascript"></script>
<link href="<?=base_url('assets/bootstrap/css/bootstrap-datetimepicker.min.css');?>" rel="stylesheet" type="text/css" />
<script src="<?=base_url('assets/plugins/datepicker/moment.min.js');?>" type="text/javascript"></script>
<script src="<?=base_url('assets/plugins/datepicker/id.js');?>" type="text/javascript"></script>
<script src="<?=base_url('assets/bootstrap/js/bootstrap-datetimepicker.min.js');?>" type="text/javascript"></script>

<script type="text/javascript">
    $(function () {
        $('.datetimepicker1').datetimepicker({
            format : 'YYYY-MM-DD',
            locale : 'id'
        });
    });
    $(function () {
        $('.datetimepicker2').datetimepicker({
            format : 'YYYY-MM-DD',
            locale : 'id'
        });
    });
</script>

<style media="screen">
.checkbox-custom-label{
    display: inline-block;vertical-align: middle;margin: 0px;}
.inforev{padding-top:0px;padding-left:0px;}
.infotext{font-weight: normal}
.infojarak{margin-bottom:0px;}
.jarak{margin-bottom:8px;}
</style>


<div class="row" ng-app="app" >
    <div class="col-md-12">
        <div class="box box-widget">
            <div class="box-header with-border">
              <h3 style="margin:0px">F7. Penetapan Revisi</h3>
            </div>
            <form name="myForm" class="form-horizontal" action="<?=site_url('revisi_dja/crud_form7')?>" onsubmit="" method="post" role="form">
                <div class="box-body">
                    <?php $this->load->view('revisi_dja/v_info'); ?>
                </div>


                <div class="col-sm-12 text-center" style="padding:0px;background:#3C8DBC;margin:5px 0px 15px 0px">
                        <label class="checkbox-custom-label" style="color:#FFF;padding:5px 15px">
                            NOTA DINAS REVISI ANGGARAN
                        </label>
                    </div>

                    <div style="margin-top:20px">
                        <div style="padding-top:20px">
                            <div class="form-group jarak">
                                <label class="col-sm-2 rev text-right">No. ND : </label>
                                <div class="col-sm-3" style="padding:0px">
                                    <input name="t7_nd_no" class="form-control" placeholder="Nomor Nota Dinas" id="t7_nd_no" value="" >
                                </div>
                                <label class="col-sm-2 rev text-right"  style="padding-right:0px">Tanggal ND : </label>
                                <div class="col-sm-3" style="padding-right:0px">
                                    <div class="input-group date datetimepicker1">
                                        <input name="t7_nd_tgl" id="t7_nd_tgl" type="text" class="form-control" placeholder="Tanggal Nota Dinas">
                                        <span class="input-group-addon">
                                            <i class="fa fa-calendar text-primary"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group jarak">
                                        <label class="col-sm-2 rev text-right">File ND :</label>
                                        <div class="col-sm-8" style="padding:0px">
                                            <input type="text" name="t7_nd_file" id="t7_nd_file" class="form-control" placeholder="File Nota Dinas" required>
                                            <input type="file" name="form7" id="form7" class="file" data-show-preview="false" >
                                        </div>
                                    </div>

                            <div class="form-group jarak">
                                <label class="col-sm-2 rev text-right">Catatan : </label>
                                <div class="col-sm-8" style="padding:0px">
                                    <textarea class="form-control" placeholder="Catatan Nota Dinas" name="t7_nd_catatan" id="t7_nd_catatan" rows="5"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>




                <div class="box-footer">
                    <div class="col-sm-12 pull-right">
                        <div class="pull-right">
                            <input type="hidden" name="rev_id"  id="rev_id" value="<?php echo $rev_id ?>" >
                            <button type="submit" id="proses" class="btn btn-info">Proses Penetapan Revisi</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- <script type="text/javascript">
    $("#form7").fileinput({
        uploadUrl: "<?php echo site_url("revisi_7/fileupload_form7/form7/$rev_id") ?>",
        allowedFileExtensions : ['17','jpg','xlsx','doc','docx','rtf','pdf'],
        overwriteInitial: true,
        maxFileSize: 1000,
        slugCallback: function(filename) {
            return filename.replace('(', '_').replace(']', '_');
        }
    });
    $("#form7").on('filebatchuploadcomplete', function(event, files, extra) {
        var id = event.target.id;
        var name = '';
        if (id=='form7') name = 't7_nd_file';
        $("#"+name).val( $("#"+id).val() );
        alert($("#"+id).val());
    });
</script> -->

<!-- New -->
<script type="text/javascript">
    $('#form7').fileinput({
        uploadExtraData: function(previewID,index){
            var data = {rev_id : $("#rev_id").val(), name: 'form7'};
            return data;
        },
        uploadUrl: "<?php echo site_url('revisi_7/fileupload_form7')  ?>",
        allowedFileExtensions : ['pdf'],
        msgInvalidFileExtension: "Invalid extension file !",
        overwriteInitial: true,
        maxFileCount: 1,
        //maxFileSize: 5000
    });
    $('#form7').on('filebatchuploadsuccess', function(event, data, extra) {
      var extra = data.extra, response = data.response;
      $('#t7_nd_file').val( response.uploaded );
      $('#t7_nd_file').attr('type','text'); $('#form7').css('display', 'none');
    });
    $('#form7').on('filebatchuploaderror', function(event, data) {
      var response = data.response;
      alert('Gagal upload !');
    });
</script>