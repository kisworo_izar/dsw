<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-type" content="text/html;charset=UTF-8">
<style type="text/css">
.vertical_line{margin-top:3px;height:3px; width:700px;background:#000;}
.vertical_line2{margin-top:2px;height:1px; width:700px;background:#000;}
table {
    border-collapse: collapse;
  }
body{
    margin:40px 20px 0px 0px;
}
.square {
    width: 5px;
    height: 5px;
    border: solid;
}
.inlineTable {
    display: inline-block;
}
.td {
    style: border 1px solid;
}
@page { margin: 0px 50px; }
</style>
</head>
<body>
<font face="arial,helvetica, sans-serif">

    <table style="margin-top:50px;font-size:10pt;">
        <tbody>
            <tr>
                <td>KEMENTERIAN KEUANGAN RI</td>
            </tr>
            <tr>
                <td>DIREKTORAT JENDERAL ANGGARAN</td>
            </tr>
        </tbody>
    </table>

<div class="inlineTable">
    <table style="font-size:11pt; margin-top:50px;margin-bottom:0px!important;margin-left:170px;">
        <tbody>
            <tr>
                <td>Nomor Tiket</td>
                <td>:</td>
                <td><strong><?php echo $routing['rev_id']; ?></strong></td>
            </tr>
        </tbody>
    </table>
</div>

<table style="font-size:12pt;margin-left:100px;margin-top:0px!important;">
    <tbody>
        <tr>
            <td><strong><center>ROUTING SLIP PENYELESAIAN REVISI ANGGARAN</center></strong></td>
        </tr>
    </tbody>
</table>

<table style="font-size:11pt;margin-top:30px;">
    <tbody>
        <tr style="vertical-align: top;">
            <td>UNIT PEMROSES</td>
            <td>:</td>
            <td><?php echo $routing['nmso']; ?></td>
        </tr>
    </tbody>
</table>

<table style="font-size:11pt;margin-top:10px;">
    <tbody>
        <tr>
            <td><strong>Proses Penyelesaian</strong></td>
        </tr>
    </tbody>
</table>

<table border="1px" style="font-size:11pt;width:710px;">
  <thead style="background-color: #ddd;">
    <tr>
      <th rowspan="2" style="width:25px">No</th>
      <th rowspan="2" style="width:170px">Pejabat</th>
      <th colspan="2">Paraf dan Tanggal</th>
      <th rowspan="2">Catatan Disposisi</th>
    </tr>
    <tr>
      <th style="width:100px">Diterima</th>
      <th style="width:100px">Selesai Diproses</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td style="height:65px;text-align:center">1</td>
      <td>Pengajuan</td>
      <td style="line-height:3pt;">
      <center><p style="margin-top:0px"><?php echo $this->fc->idtgl($routing['pus_tgl'],'tgl');  ?></p>
       
      <p style="margin-top:15px"><?php echo $this->fc->idtgl($routing['pus_tgl'],'jam') ?></p>
      </center>
      </td>
      <td style="text-align:right"></td>
      <td>Diterima Oleh Sistem</td>
    </tr>
    <tr>
      <td style="height:65px;text-align:center">2</td>
      <td>Kasubdit Anggaran</td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td style="height:65px;text-align:center">3</td>
      <td>Kepala Seksi Anggaran</td>
      <td></td>
      <td></td>
      <td style="line-height:7pt;border-bottom:solid 1px white;border-right:solid 1.5px black">
        <p style="margin-top:0px">Tgl Penelaahan : ......</p>
        <p style="margin-top:10px">Tgl Dokumen diterima lengkap : ......</p>
      </td>
    </tr>
    <tr>
      <td style="height:65px;text-align:center">4</td>
      <td>Pelaksana Anggaran</td>
      <td></td>
      <td></td>
      <td style="line-height:7pt;border-top:solid 1px white;border-right:solid 1.5px black">
        <p style="margin-top:10px">Tgl Pengiriman ke Pusat Layanan : ......</p>
        <p style="margin-top:10px">Tgl Upload data : ......</p>
      </td>
    </tr>
    <tr>
      <td style="height:65px;text-align:center">5</td>
      <td>Kasubdit Daduktek</td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
  </tbody>
</table>

<table style="font-size:10pt;margin-left:12px">
  <tr>
    <td>1) Tanggal Penyerahan/ Pengiriman dokumen revisi ke K/L</td>
  </tr>
</table>

<table style="margin-left:12px;margin-top:25px;">
  <tbody>
    <tr>
      <td><strong>Proses Penetapan Direktur Anggaran</strong></td>
    </tr>
  </tbody>
</table>

<table border="1px" style="font-size:11pt;width:710px;">
  <thead style="background-color: #ddd;">
    <tr>
      <th rowspan="2" style="width:170px">Pejabat</th>
      <th colspan="2">Paraf dan Tanggal</th>
      <th rowspan="2">Catatan/Disposisi</th>
    </tr>
    <tr>
      <th style="width:100px">Diterima</th>
      <th style="width:100px">Ditetapkan</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td style="height:50px;text-align:center">Direktur Anggaran</td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
  </tbody>
</table>

<table style="font-size:8pt;margin-top:70px;margin-bottom:10px;">
  <tbody>
    <tr>
      <td>Catatan:</td>
    </tr>
    <tr>
      <td>Routing Slip ini merupakan satu kesatuan</td>
    </tr>
    <tr>
      <td>dengan berkas usulan revisi dan Tanda Terima</td>
    </tr>
  </tbody>
</table>


<table style="margin-top:40px">
  <tbody>
    <tr>
      <td></td>
    </tr>
  </tbody>
</table>
<div class="vertical_line2"></div>
<!--    <div class="vertical_line2"></div> -->
    <table style="font-size:9px;margin-top:10px; margin-left:20px">
            <tr>
                <td><strong>Pusat Layanan DJA</strong></td>
            </tr>
            <tr>
                <td>Lobby Gedung Sutikno Slamet, Jl. Wahidin No. 1 Jakarta Pusat</td>
            </tr>
            <tr>
                <td>Fax : 021-34832515, Call Center : 021-34832511</td>
            </tr>
            <tr>
                <td>e-mail : pusatlayanan.dja@kemenkeu.go.id</td>
            </tr>
    </table>
</font>
</body>
</html>