<script src="<?= base_url('assets/plugins/jQuery/jquery.maskMoney.min.js') ?>" type="text/javascript"></script>
<link  href="<?= base_url('assets/plugins/select2/select2.css') ?>" rel="stylesheet" type="text/css" />
<script src="<?= base_url('assets/plugins/select2/select2.full.min.js') ?>" type="text/javascript"></script>
<link  href="<?= base_url('assets/bootstrap/css/bootstrap-datetimepicker.min.css') ?>" rel="stylesheet" type="text/css" />
<script src="<?= base_url('assets/plugins/datepicker/moment.min.js') ?>" type="text/javascript"></script>
<script src="<?= base_url('assets/plugins/datepicker/id.js') ?>" type="text/javascript"></script>
<script src="<?= base_url('assets/bootstrap/js/bootstrap-datetimepicker.min.js') ?>" type="text/javascript"></script>
<script src="<?= base_url('assets/AngularJS/angular.min.js') ?>"></script>
<link  href="<?= base_url('assets/plugins/uploadfile/fileinput.css') ?>" rel="stylesheet">
<script src="<?= base_url('assets/plugins/uploadfile/fileinput.min.js') ?>" type="text/javascript"></script>


<style media="screen">
.checkbox-custom-label{
    display: inline-block;vertical-align: middle;margin: 0px;}
.inforev{padding-top:0px;padding-left:0px;}
.infotext{font-weight: normal}
.infojarak{margin-bottom:0px;}
.jarak{margin-bottom:8px;}
</style>


<div class="row" ng-app="app" >
    <div class="col-md-12">
        <div class="box box-widget">
            <div class="box-header with-border">
              <h3 style="margin:0px">F2. Teliti Dokumen</h3>
            </div>
            <form name="myForm" class="form-horizontal" action="<?=site_url('revisi_dja/crud_form2')?>" onsubmit="" method="post"
            role="form">
                <div class="box-body">
                    <?php $this->load->view('revisi_dja/v_info'); ?>
                </div>


                <div class="box-footer">
                    <div class="col-sm-12 pull-right">
                        <div class="pull-right">
                            <input type="hidden" name="rev_id"  id="rev_id" value="<?php echo $rev_id ?>" >
                            <button type="button" id="cetak" class="btn btn-success" onclick="pdf_routing()">Cetak Routing Slip</button>

                            <button type="submit" id="proses" class="btn btn-info">Proses Teliti Dokumen</button>
                        </div>
                    </div>
                </div>
            </form>

        </div>
    </div>
</div>


<script type="text/javascript">

    function pdf_routing() {
    var rev_id = $('#rev_id').val();
    window.location.href = '<?= site_url('Revisi_dja/pdf_routing') ?>' +'/'+ rev_id;
  }
</script>
