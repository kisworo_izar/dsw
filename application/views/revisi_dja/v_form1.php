<script src="<?= base_url('assets/plugins/jQuery/jquery.maskMoney.min.js') ?>" type="text/javascript"></script>
<link  href="<?= base_url('assets/plugins/select2/select2.css') ?>" rel="stylesheet" type="text/css" />
<script src="<?= base_url('assets/plugins/select2/select2.full.min.js') ?>" type="text/javascript"></script>
<link  href="<?= base_url('assets/bootstrap/css/bootstrap-datetimepicker.min.css') ?>" rel="stylesheet" type="text/css" />
<script src="<?= base_url('assets/plugins/datepicker/moment.min.js') ?>" type="text/javascript"></script>
<script src="<?= base_url('assets/plugins/datepicker/id.js') ?>" type="text/javascript"></script>
<script src="<?= base_url('assets/bootstrap/js/bootstrap-datetimepicker.min.js') ?>" type="text/javascript"></script>
<script src="<?= base_url('assets/AngularJS/angular.min.js') ?>"></script>
<link  href="<?= base_url('assets/plugins/uploadfile/fileinput.css') ?>" rel="stylesheet">
<script src="<?= base_url('assets/plugins/uploadfile/fileinput.min.js') ?>" type="text/javascript"></script>

<style media="screen">
  .checkbox-custom, .radio-custom {opacity: 0;position: absolute;}
  .checkbox-custom, .checkbox-custom-label, .radio-custom, .radio-custom-label {
    display: inline-block;vertical-align: middle;margin: 0px;}
  .checkbox-custom-label, .radio-custom-label {position: relative;}
  .checkbox-custom + .checkbox-custom-label:before, .radio-custom + .radio-custom-label:before {
    content: '';background: #fff;border: 1px solid #DFDFDF;display: inline-block;vertical-align: middle;
    width: 20px;height: 20px;padding: 2px;margin-right: 5px;text-align: center;}
  .checkbox-custom:checked + .checkbox-custom-label:before {background: #3C8DBC;}
  .radio-custom + .radio-custom-label:before {border-radius: 50%;}
  .radio-custom:checked + .radio-custom-label:before {background: #3C8DBC;}
  .checkbox-custom:focus + .checkbox-custom-label, .radio-custom:focus + .radio-custom-label {outline: 0px solid #ddd;}
  .inforev {padding-top:0px;padding-left:0px;}
  .infotext {font-weight: normal}
  .infojarak {margin-bottom:0px;}
  .rev {padding-top:8px;padding-left:35px;text-indent:-20px}
  .jarak {margin-bottom:8px;}
  .fdok {padding-left:30px;text-indent:-30px}
</style>

<script>
  var app = angular.module('app', [])
  app.controller('ctrlrefr', function ($scope, $http) {
    $http.get('<?= site_url('files/json/Dept.json') ?>').success( function( data ) {
      $scope.refrdept = data;
    });
    $http.get('<?= site_url('files/json/Unit.json') ?>').success( function( data ) {
      $scope.refrunit = data;
    });
  });
</script>

<script type="text/javascript">
  $(function () {
  });
</script>

<script type="text/javascript">
  $(document).ready(function() {
    $('.datetimepicker1').datetimepicker({ format : 'dddd, DD MMMM YYYY', locale : 'id' });
    $(".h-tujuan,.h-dari,.h-penandatangan,.1-kl,.1-unit").select2();
    $(".jr-11,.jr-12,.jr-13").select2({ minimumResultsForSearch: 7 });
    $(".3-31,.3-32,.3-33,.3-34").select2({ minimumResultsForSearch:5 });

    var revisi = <?= count($revisi) ?>;
    if (revisi > 0) {
      $('#li_tamb, #li_awal, #btn-awal').hide(); $('#btn-cetak').show();
      $('input[type=text], input[type=checkbox]').prop('disabled', true);
      $('#file1').css('display','none');
    }
  });
</script>

<script>
  function toggle($dok) {
    if( document.getElementById($dok).style.display=='none' ) document.getElementById($dok).style.display = '';
    else document.getElementById($dok).style.display='none';
  }

  function toggletab( $tab ) {
    if ($tab=='tambahan') {
      $('#li_tamb').addClass("active"); $('#li_awal').removeClass("active");
      $('.tambahan').css("display","inline"); $(".awal").css("display", "none");  $('#info').css("display","none");
      $('#lblSurat').html('DOKUMEN TAMBAHAN DATA DUKUNG');
      $('#rev_jns_upload').val('2');
      $("#kembali").css("display", "none");
      $("#terima").html('Perbaikan Dokumen');
      $('#kl_surat_no, #kl_surat_tgl, #kl_surat_hal, #kl_pjb_jab, #kl_pjb_nama, #doc_usulan_revisi, #doc_adk').removeAttr('required');
    } else {
      $('#li_awal').addClass("active"); $('#li_tamb').removeClass("active");
      $('.tambahan').css("display", "none");  $(".awal").css("display", "inline"); $('#info').css("display","none");
      $('#lblSurat').html('PENGAJUAN AWAL REVISI ANGGARAN');
      $('#rev_jns_upload').val('1');
      $("#kembali").css("display", "inline");
      $("#terima").html('Terima Dokumen');
      $('#kl_surat_no, #kl_surat_tgl, #kl_surat_hal, #kl_pjb_jab, #kl_pjb_nama, #doc_usulan_revisi, #doc_adk').prop('required', true);
    }
  }

  function cari_revid() {
    $("#li_awal").css("display", "none");
    var rev_id = $('#rev_id_tamb').val();

    $.ajax({
      url : "<?php echo site_url('revisi/get_rev_id/tambah') ?>",
      type: "POST",
      data: { 'rev_id': rev_id },
    })
      .done( function (msg) {
        info = $.parseJSON(msg);
        $("#rev_id").val(rev_id);
        $('#tam_rev_id').html( rev_id +' / '+ info['rev_tahun'] );
        $('#tam_kl_email').html( info['kl_email'] );
        $('#tam_kl_dept').html( info['kl_dept'] );
        $('#tam_kl_unit').html( info['kl_unit'] );
        $('#tam_kl_pjb_jab').html( info['kl_pjb_jab'] );
        $('#tam_kl_surat_no').html( info['kl_surat_no'] );
        $('#tam_kl_surat_tgl').html( info['kl_surat_tgl'] );
        $('#tam_kl_surat_hal').html( info['kl_surat_hal'] );
        $('#tam_pus_catatan').html( info['pus_catatan'] );
        $('.tambahan').css("display","none"); $('#info').css("display","inline");
        info_mitra( info );
      })
  }
</script>


<div class="row" ng-app="app" >
  <div class="col-md-12">
    <div class="box box-widget">
      <div class="nav-tabs-custom" style="margin-bottom:1px;border:0px;border-radius:0px">
        <ul class="nav nav-tabs pull-right" style="border-bottom:0px;">
          <li class="pull-left header"><h3 style="margin-top:7px">F1. Pengajuan Revisi Anggaran</h3></li>
          <li id="li_tamb"><a href="" onclick="toggletab('tambahan')" >Tambahan Data Dukung</a></li>
          <li id="li_awal" class="active"><a href="" onclick="toggletab('awal')" >Pengajuan Awal</a></li>
        </ul>
      </div>

      <div class="box-body" ng-controller="ctrlrefr">

        <form class="form-horizontal" role="form">
          <div class="col-sm-12 text-center awal" style="padding:0px;background:#3C8DBC; margin:10px 0px">
            <label for="checkbox-5" class="checkbox-custom-label" style="color:#FFF;padding:5px 15px"> NO TIKET (ID REVISI) </label>
          </div>
          <div class="form-group jarak awal">
            <label class="col-sm-2 rev text-right">ID REVISI : </label>
            <div class="col-sm-2" style="padding:0px">
              <input type="text" id="rev_idx" class="form-control" readonly value="<?= $revisi['rev_id'] ?>">
            </div>
          </div>

          <div class="col-sm-12 text-center tambahan" style="padding:0px;background:#3C8DBC; margin:5px 0px 15px 0px; display:none">
            <label for="checkbox-5" class="checkbox-custom-label" style="color:#FFF;padding:5px 15px">TAMBAHAN DATA DUKUNG REVISI ANGGARAN</label>
          </div>
          <div class="form-group jarak tambahan" style="display:none">
            <label class="col-sm-2 rev text-right">ID REVISI : </label>
            <div class="col-sm-3" style="padding:0px">
              <input name="rev_id_tamb" class="form-control" placeholder="Nomor ID Revisi" id="rev_id_tamb" value="" >
            </div>
            <div class="col-sm-5">
              <button class="btn btn-primary" onclick="cari_revid()"> Cari</button>
            </div>
          </div>
        </form>

        <form id="form1" class="form-horizontal" role="form">
          <input type="hidden" name="rev_id" id="rev_id" value="<?= $revisi['rev_id'] ?>">
          <input type="hidden" name="rev_jns_upload" id="rev_jns_upload" value="1">
          <div id="info" style="display:none"><?php $this->load->view('revisi/v_info'); ?></div>

          <div class="col-sm-12 text-center" style="padding:0px;background:#3C8DBC;margin:5px 0px 15px 0px">
            <label for="checkbox-5" id="lblSurat" class="checkbox-custom-label" style="color:#FFF;padding:5px 15px">PENGAJUAN AWAL REVISI ANGGARAN</label>
          </div>

          <div class="form-group jarak awal" id="tab-dept">
            <label class="col-sm-2 rev text-right">K/L : </label>
            <div class="col-sm-8" style="padding:0px">
              <?php if ($revisi) { ?>
                <input type="text" class="form-control" name="kl_dept" readonly value="<?= $revisi['kl_dept'] .' '. $revisi['nmdept'] ?>">
              <?php } else { ?>
                <select class="1-kl form-control" name="kl_dept" id="kl_dept" required ng-model="kl_dept">
                  <option ng-repeat="dept in refrdept" value="{{ dept.kddept }}" >{{ dept.nmdept }}</option>
                </select>
              <?php } ?>
            </div>
          </div>

          <div class="form-group jarak awal" id="tab-unit">
            <label class="col-sm-2 rev text-right">Unit Eselon I : </label>
            <div class="col-sm-8" style="padding:0px">
              <?php if ($revisi) { ?>
                <input type="text" class="form-control" name="kl_unit" readonly value="<?= $revisi['kl_dept'] .'.'. $revisi['kl_unit'] .' '. $revisi['nmunit'] ?>">
              <?php } else { ?>
                <select class="1-unit form-control" name="kl_unit" required ng-model="kl_unit" onchange="get_rev_id(this.value)">
                  <option ng-repeat="unit in refrunit | filter: { kddept:kl_dept }" value="{{ unit.kdunit }}">{{ unit.nmunit }}</option>
                </select>
              <?php } ?>
            </div>
          </div>

          <div class="form-group jarak">
              <label class="col-sm-2 rev text-right">Alamat email : </label>
              <div class="col-sm-4" style="padding:0px">
                  <input type="text" id="kl_email" name="kl_email" class="form-control" readonly value="<?= $revisi['kl_email'] ?>" >
              </div>
              <label class="col-sm-2 rev text-right">Telepon : </label>
              <div class="col-sm-2" style="padding:0px">
                  <input type="text" id="kl_telp" name="kl_telp" class="form-control" readonly value="<?= $revisi['kl_telp'] ?>" >
              </div>
          </div>
          <div class="form-group jarak">
              <label class="col-sm-2 rev text-right">Sub Direktorat : </label>
              <div class="col-sm-8" style="padding:0px">
                  <input type="hidden" name="rev_kdso" id="rev_kdso" value="" >
                  <input id="rev_nmso" class="form-control" readonly value="" >
              </div>
          </div>
          <div class="form-group jarak">
              <label class="col-sm-2 rev text-right">No. Intern : </label>
              <div class="col-sm-8" style="padding:0px">
                  <input id="rev_notlp" class="form-control" readonly value="" >
              </div>
          </div>
          <div class="form-group jarak">
              <label class="col-sm-2 rev text-right">Contact Person : </label>
              <div class="col-sm-8" style="padding:0px">
                  <input id="rev_nmcp" class="form-control" readonly value="" >
              </div>
          </div>

          <div class="form-group jarak">
            <label class="col-sm-2 rev text-right">No. Surat : </label>
            <div class="col-sm-3" style="padding:0px">
              <input type="text" id="kl_surat_no" name="kl_surat_no" class="form-control" placeholder="Nomor Surat" required value="<?= $revisi['kl_surat_no'] ?>" >
            </div>
            <label class="col-sm-2 rev text-right" style="padding-right:0px">Tanggal Surat : </label>
            <div class="col-sm-3" style="padding-right:0px">
              <div class="input-group date datetimepicker1">
                <input type="text" id="kl_surat_tgl" name="kl_surat_tgl" class="form-control" placeholder="Tanggal Surat" required value="<?php echo $this->fc->idtgl( $revisi['kl_surat_tgl'], 'hari') ?>" style="height:37px">
                <span class="input-group-addon"><i class="fa fa-calendar text-primary"></i></span>
              </div>
            </div>
          </div>

          <div class="form-group jarak">
            <label class="col-sm-2 rev text-right">Hal : </label>
            <div class="col-sm-8" style="padding:0px">
              <input type="text" id="kl_surat_hal" name="kl_surat_hal" class="form-control" placeholder="Hal Surat" required value="<?= $revisi['kl_surat_hal'] ?>" >
            </div>
          </div>

          <div style="margin-top:0px"><?php $this->load->view('revisi/v_form1_infodok') ?></div>

          <div class="box-footer" id="btn-awal">
            <div class="col-sm-12 pull-right">
              <div class="pull-right">
              	<input name="simpan" type="hidden" value="terima">
                <button type="submit" name="simpan" value="kembali" id="kembali" class="btn btn-warning">Pengembalian</button>
                <button type="submit" name="simpan" value="terima"  id="terima"  class="btn btn-info" disabled="true" onclick="crud_form1()">Terima Dokumen</button>
              </div>
            </div>
          </div>

          <div class="box-footer" id="btn-cetak" style="display: none">
            <div class="col-sm-12 pull-right">
              <div class="pull-right">
                <button type="button" id="cetak" class="btn btn-success" onclick="pdf_routing()">Cetak Routing Slip</button>
                <button type="button" id="cetak" class="btn btn-success" onclick="cetak_tanda_terima()">Cetak Tanda Terima</button>
                <button onclick="window.history.back();" class="btn btn-warning">Kembali</button>
              </div>
            </div>
          </div>
        </form>

      </div>

    </div>
  </div>
</div>


<script type="text/javascript">
  function get_rev_id( kdunit ) {
    $.ajax({
  		url: '<?= site_url('revisi/get_rev_id/awal') ?>', type: 'POST', data: {'dept': $("#kl_dept").val(), 'unit': kdunit},
  		success: function( info ) { 
        var obj = jQuery.parseJSON( info );
        $("#rev_id").val( obj.rev_id );
        $("#rev_idx").val( obj.rev_id );
        info_mitra( obj );
      }
		});  
  }

  function info_mitra( arr ) {
    $("#kl_email").val( arr.kl_email );
    $("#kl_telp").val( arr.kl_telp );
    $("#rev_kdso").val( arr.rev_kdso );
    $("#rev_nmso").val( arr.rev_nmso );
    $("#rev_notlp").val( arr.rev_notlp );
    $("#rev_nmcp").val( arr.rev_nmcp );
    if (arr.kl_email !== '') $("#terima").attr('disabled', false);
    else $("#terima").attr('disabled', true);
  }

  function crud_form1() {
    var tamb = $('#rev_id_tamb').val();
    if (tamb == '' && $('#doc_usulan_revisi').val() == '') return alert('Dokumen Usulan Revisi \nbelum di-Upload');
    if (tamb == '' && $('#doc_usulan_revisi').val() == '') return alert('Dokumen Usulan Revisi \nbelum di-Upload');
    if (tamb == '' && $('#doc_adk').val() == '') return alert('File ADK Revisi \nbelum di-Upload');

    // setTimeout(function(){ alert("Sedang proses ... \n - Create PDF Tanda Terima \n - Kirim Email"); }, 500);
    $.ajax({  
      url: '<?= site_url('revisi/crud_form1') ?>', type: 'POST', data: $('#form1').serializeArray(),  
        success: function( msg ) {  
          alert('Data tersimpan ...');
          $('#btn-awal').hide(); $('#btn-cetak').show();
      }
    });  
	}

  function cetak_tanda_terima(rev_id) {
    window.location.href = '<?= site_url('revisi/pdf1') ?>' +'/'+ $('#rev_id').val();
  }

  function pdf_routing(rev_id) {
    window.location.href = '<?= site_url('revisi/pdf_routing') ?>' +'/'+ $('#rev_id').val();
  }
</script>


<script type="text/javascript">
  $(function() {
    $("#kembali").on("click", function() {
      $("#doc_usulan_revisi").attr('readonly', 'readonly');
      $("#doc_adk").attr('readonly', 'readonly');
    });
  });
</script> 