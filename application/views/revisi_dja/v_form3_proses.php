<link href="<?=base_url('assets/bootstrap/css/bootstrap-datetimepicker.min.css');?>" rel="stylesheet" type="text/css" />
<script src="<?=base_url('assets/plugins/datepicker/moment.min.js');?>" type="text/javascript"></script>
<script src="<?=base_url('assets/plugins/datepicker/id.js');?>" type="text/javascript"></script>
<script src="<?=base_url('assets/bootstrap/js/bootstrap-datetimepicker.min.js');?>" type="text/javascript"></script>
<link href="<?=base_url(); ?>assets/plugins/uploadfile/fileinput.css" rel="stylesheet">
<script src="<?=base_url();?>assets/plugins/uploadfile/fileinput.min.js" type="text/javascript"></script>



<style media="screen">
.checkbox-custom, .radio-custom{opacity: 0;position: absolute;}
.checkbox-custom, .checkbox-custom-label, .radio-custom, .radio-custom-label{
    display: inline-block;vertical-align: middle;margin: 0px;}
.checkbox-custom-label, .radio-custom-label {position: relative;}
.checkbox-custom + .checkbox-custom-label:before, .radio-custom + .radio-custom-label:before{
    content: '';background: #fff;border: 1px solid #DFDFDF;display: inline-block;vertical-align: middle;
    width: 20px;height: 20px;padding: 2px;margin-right: 5px;text-align: center;}
.checkbox-custom:checked + .checkbox-custom-label:before{background: #3C8DBC;}
.radio-custom + .radio-custom-label:before{border-radius: 50%;}
.radio-custom:checked + .radio-custom-label:before {background: #3C8DBC;}
.checkbox-custom:focus + .checkbox-custom-label, .radio-custom:focus + .radio-custom-label {outline: 0px solid #ddd;}
.inforev{padding-top:0px;padding-left:0px;}
.infotext{font-weight: normal}
.infojarak{margin-bottom:0px;}
.jarak{margin-bottom:8px;}
.fdok{padding-left:30px;text-indent:-30px}
</style>

<script>
function toggle_opt($opt) {
 if( $opt == 'yes' ){
     document.getElementById("undangan").style.display = '';
     document.getElementById("proses").innerHTML = 'Kirim Undangan';
     $('#proses').removeClass('btn-primary');
     $('#proses').addClass('btn-info');
 }else{
     document.getElementById("undangan").style.display = 'none';
     document.getElementById("proses").innerHTML = 'Proses';
     $('#proses').removeClass('btn-info');
     $('#proses').addClass('btn-primary');
    }
}
</script>
<script type="text/javascript">
    $(function () {
        $('.datetimepicker1').datetimepicker({
            format : 'YYYY-MM-DD',
            locale : 'id'
        });
    });
    $(function () {
        $('.datetimepicker2').datetimepicker({
            format : 'YYYY-MM-DD',
            locale : 'id'
        });
    });
</script>

<?php
    $disb = ''; $terima = ''; $tolak = ''; $sesi=$this->session->userdata('idusergroup');$tidak = ''; $ya = '';
    if ( ($proses['t3_status']=='2') And ($sesi <> '001') ) { $disb = 'disabled'; $no = $proses['t3_und_no']; $tgl= $proses['t3_und_tgl']; $file= $proses['t3_und_file']; $catatan = $proses['t3_catatan']; $telaah = $proses['t3_penelaahan_tgl']; $pukul = substr($proses['t3_penelaahan_tgl'], 11, 5);   }
    if ( ($proses['t3_status']=='1') And ($sesi <> '001') ) { $disb = '';  $no = ''; $tgl= ''; $file= ''; $catatan = ''; $telaah= ''; $pukul=''; }
    
?>


<div class="row" ng-app="app" >
    <div class="col-md-12">
        <div class="box box-widget">
            <div class="box-header with-border">
              <h3 style="margin:0px">F3. Undangan Penelaahan</h3>
            </div>
            <form name="myForm" class="form-horizontal" action="<?=site_url('revisi_dja/crud_form3_proses')?>" onsubmit="" method="post" 
                role="form">
                <div class="box-body">
                    <?php $this->load->view('revisi_dja/v_info'); ?>

                    <div class="col-sm-12 text-center" style="padding:0px;background:#3C8DBC;margin:5px 0px 15px 0px">
                        <label class="checkbox-custom-label" style="color:#FFF;padding:5px 15px">
                            UNDANGAN PENELAAHAN REVISI ANGGARAN
                        </label>
                    </div>

                    <div style="margin-top:20px">
                        <div style="padding-top:20px">
                            <div class="form-group jarak">
                                
                                <div class="form-group jarak">
                                    <label class="col-sm-2 rev text-right">No. Surat : </label>
                                    <div class="col-sm-3" style="padding:0px">
                                        <input name="t3_und_no" class="form-control" placeholder="Nomor Surat" id="" value="<?php echo $no ?>" <?php echo $disb ?>  >
                                    </div>
                                    <label class="col-sm-2 rev text-right"  style="padding-right:0px">Tanggal Surat : </label>
                                    <div class="col-sm-3" style="padding-right:0px">
                                        <div class="input-group date datetimepicker1">
                                            <input name="t3_und_tgl" id="t3_und_tgl" type="text" class="form-control" placeholder="Tanggal Surat" value="<?php echo $tgl ?>" <?php echo $disb ?>  >
                                            <span class="input-group-addon">
                                                <i class="fa fa-calendar text-primary"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group jarak">
                                    <label class="col-sm-2 rev text-right">Tanggal Penelaahan : </label>
                                    <div class="col-sm-3" style="padding-right:0px">
                                        <dsiv class="input-group date datetimepicker2">
                                            <input name="tglpenelaahan" id="tglpenelaahan" type="text" class="form-control" value="<?php echo $telaah ?>" placeholder="Tanggal Penelaahan" <?php echo $disb ?>>
                                            <span class="input-group-addon">
                                                <i class="fa fa-calendar text-primary"></i>
                                            </span>
                                        </div>
                                    </div>
                                    <label class="col-sm-2 rev text-right">Pukul : </label>
                                    <div class="col-sm-3" style="padding:0px">  
                                        <input name="pukul" class="form-control" placeholder="00:00" id="pukul" value="<?php echo $pukul ?>" <?php echo $disb ?> >
                                    </div>
                                </div>

                                    <div class="form-group jarak">
                                        <label class="col-sm-2 rev text-right">File Surat :</label>
                                        <div class="col-sm-8" style="padding:0px">
                                             <?php if ($proses['t3_status']=='1') { ?>
                                            <input type="text" name="t3_und_file" id="t3_und_file" style="width: 100%" value="" required>
                                            <input type="file" name="form3" id="form3" class="file" data-show-preview="false" <?php echo $disb ?> >
                                             <?php } else { ?>
                                            <input name="t3_file" value="<?php echo $file ?>" <?php echo "$disb" ?> class="form-control" >   
                                            <?php } ?>
                                        </div>
                                    </div> 
                            

                            <div class="form-group jarak">
                                <label class="col-sm-2 rev text-right">Catatan : </label>
                                <div class="col-sm-8" style="padding:0px">
                                    <textarea class="form-control"  placeholder="Catatan" <?php echo $disb ?>  name="t3_catatan" rows="5"><?php echo $catatan ?></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <div class="col-sm-12 pull-right">
                        <div class="pull-right">
                            <input type="hidden" name="rev_id"  id="rev_id" value="<?php echo $rev_id ?>" >

 
                            <button type="submit" id="proses" <?php echo $disb ?>  class="btn btn-primary">Proses</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>


<script type="text/javascript">
    $('#form3').fileinput({
        uploadExtraData: function(previewID,index){
            var data = {rev_id : $("#rev_id").val(), name: 'form3'};
            return data;
        },
        uploadUrl: "<?php echo site_url('Revisi_dja/fileupload_form3')  ?>",
        allowedFileExtensions : ['pdf'],
        msgInvalidFileExtension: "Invalid extension file !",
        overwriteInitial: true,
        maxFileCount: 1,
        maxFileSize: 5000
    });
    $('#form3').on('filebatchuploadsuccess', function(event, data, extra) {
      var extra = data.extra, response = data.response;
      $('#t3_und_file').val( response.uploaded );
      $('#t3_und_file').attr('type','text'); $('#form3').css('display', 'none');
    });
    $('#form3').on('filebatchuploaderror', function(event, data) {
      var response = data.response;
      alert('Gagal upload !');
    });

</script>