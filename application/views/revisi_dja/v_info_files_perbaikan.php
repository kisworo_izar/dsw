<?php
  $dbrevisi = $this->load->database('revisi'. $this->session->userdata('thang'), TRUE);
  //$dbrevisi = $this->load->database('dbsatu', TRUE);
  $query = $dbrevisi->query("Select * From revisi_perbaikan Where rev_id='$rev_id'");
  $perba = $query->result_array();

  $list = array();
  $list['doc_usulan_revisi'] = 'Surat usulan revisi yang ditandatangani pejabat eselon I';
  $list['doc_matrix']        = 'Matriks perubahan (semula menjadi)';
  $list['doc_rka']           = 'Rencana Kerja dan Anggaran (RKA) Satker';
  $list['doc_adk']           = 'Arsip Data Komputer (ADK) RKA-K/L DIPA revisi Satker';
  $list['doc_dukung_es1']= 'Dokumen pendukung terkait sesuai hasil kesepakatan antara K/L dengan DJA dalam pembahasan usulan revisi anggaran';
  $list['doc_dukung_hal4']   = 'Dokumen pendukung terkait perubahan/penghapusan catatan dalam halaman IV DIPA';
  $list['doc_dukung_lainnya']= 'Dokumen pendukung terkait lainnya';
?>


<?php $no=1; 
  foreach ($perba as $row) { ?>

    <form class="form-horizontal" role="form">
      <div class="col-sm-12 text-center" style="padding:0px;background:#3C8DBC;margin:5px 0px 15px 0px">
        <label class="checkbox-custom-label" style="color:#FFF;padding:5px 15px">PERBAIKAN DATA DUKUNG KE-<?= $no ?> </label>
      </div>

      <div class="form-group jarak">
        <label class="col-sm-2 rev text-right">No. Surat : </label>
        <div class="col-sm-3" style="padding:0px"><input class="form-control" disabled value="<?= $row['kl_surat_no'] ?>" /></div>
        <label class="col-sm-2 rev text-right" style="padding-right:0px">Tanggal Upload : </label>
        <div class="col-sm-3" style="padding-right:0px">
          <div class="input-group">
            <input class="form-control" disabled value="<?php echo $this->fc->idtgl( $row['pus_tgl'], 'hari') ?>" />
            <span class="input-group-addon"><i class="fa fa-calendar text-primary"></i></span>
          </div>
        </div>
      </div>

      <div class="form-group jarak">
        <label class="col-sm-2 rev text-right">Pejabat : </label>
        <div class="col-sm-8" style="padding:0px"><input class="form-control" disabled value="<?= $row['kl_pjb_jab'] ?>" /></div>
      </div>

      <div style="margin-top:5px">
        <div style="padding-top:5px">

          <?php foreach ($list as $key=>$value) { 
            if ( $row[ $key ]!='' And file_exists("files/revisi_SatuDJA/$rev_id/".$row[ $key ]) ) { ?>
              <div class="form-group jarak">
                <label class="col-sm-2 rev text-right"></label>
                <div class="col-sm-8" style="padding:0px">
                  <input class="checkbox-custom" type="checkbox" checked disabled />
                  <label class="checkbox-custom-label fdok infotext">
                    <?php echo $value .'<br><a href="'. site_url("files/revisi_SatuDJA/$rev_id/".$row[ $key ]) .'">'. $row[ $key ] .'</a>'; ?>
                  </label>
                </div>
              </div>
          <?php } } ?>

        </div>
      </div>
    </form>

<?php $no++; } ?>