<link href="<?=base_url(); ?>assets/plugins/uploadfile/fileinput.css" rel="stylesheet">
<script src="<?=base_url();?>assets/plugins/uploadfile/fileinput.min.js" type="text/javascript"></script>
<link href="<?=base_url('assets/bootstrap/css/bootstrap-datetimepicker.min.css');?>" rel="stylesheet" type="text/css" />
<script src="<?=base_url('assets/plugins/datepicker/moment.min.js');?>" type="text/javascript"></script>
<script src="<?=base_url('assets/plugins/datepicker/id.js');?>" type="text/javascript"></script>
<script src="<?=base_url('assets/bootstrap/js/bootstrap-datetimepicker.min.js');?>" type="text/javascript"></script>
<script src="<?=base_url();?>assets/plugins/jQuery/jquery.js"></script>
<script src="<?=base_url();?>assets/plugins/jQuery/jquery.validate.js"></script>
<script src="<?=base_url();?>assets/plugins/uploadfile/fileinput.min.js" type="text/javascript"></script>

<style media="screen">
.checkbox-custom, .radio-custom{opacity: 0;position: absolute;}
.checkbox-custom, .checkbox-custom-label, .radio-custom, .radio-custom-label{
    display: inline-block;vertical-align: middle;margin: 0px;}
.checkbox-custom-label, .radio-custom-label {position: relative;}
.checkbox-custom + .checkbox-custom-label:before, .radio-custom + .radio-custom-label:before{
    content: '';background: #fff;border: 1px solid #DFDFDF;display: inline-block;vertical-align: middle;
    width: 20px;height: 20px;padding: 2px;margin-right: 5px;text-align: center;}
.checkbox-custom:checked + .checkbox-custom-label:before{background: #3C8DBC;}
.radio-custom + .radio-custom-label:before{border-radius: 50%;}
.radio-custom:checked + .radio-custom-label:before {background: #3C8DBC;}
.checkbox-custom:focus + .checkbox-custom-label, .radio-custom:focus + .radio-custom-label {outline: 0px solid #ddd;}
.inforev{padding-top:0px;padding-left:0px;}
.infotext{font-weight: normal}
.infojarak{margin-bottom:0px;}
.jarak{margin-bottom:8px;}
.fdok{padding-left:30px;text-indent:-30px}
.hide {display: none;}
</style>

<script>
function toggle_opt($opt) {
 if( $opt == 'terima' ){
     document.getElementById("tolak_up").style.display = '';
     document.getElementById("proses").innerHTML = 'Proses';
     $('#proses').removeClass('btn-danger');
     $('#proses').addClass('btn-primary');
     $('#proses').attr('disabled',false);
 }else if( $opt == 'tolak' ) {
     document.getElementById("tolak_up").style.display = 'none';
     document.getElementById("proses").innerHTML = 'Penolakan Dokumen';
     $('#proses').removeClass('btn-primary');
     $('#proses').addClass('btn-danger');
     $('#proses').attr('disabled',false);
 
    }
}

function toggle_opt1($opt1) {
 if( $opt1 == 'no' ){
     document.getElementById("tolak_up").style.display = '';
     document.getElementById("proses").innerHTML = 'Proses';
    // $('#proses').removeClass('btn-danger');
     $('#proses').addClass('btn-primary');
      $('#proses').attr('disabled',false);

  }else if( $opt1 == 'yes' ){ 
     document.getElementById("tolak_up").style.display = '';
     document.getElementById("proses").innerHTML = 'Proses';
   //  $('#proses').removeClass('btn-primary');
     $('#proses').addClass('btn-primary');
      $('#proses').attr('disabled',false);
    }
}
</script>

<script type="text/javascript">
    $(function () {
        $('.datetimepicker1').datetimepicker({
            format : 'YYYY-MM-DD',
            locale : 'id'
        });
    });
</script>

<script>
       $(document).ready(function(){
           $("#pilih").validate();
        });
   </script>

<?php
    $disb = ''; $terima = ''; $tolak = ''; $sesi=$this->session->userdata('idusergroup');$tidak = ''; $ya = ''; $dis='disabled' ;
    if ( ($proses['t2_status']=='2') And ($sesi <> '001') ) { $disb = 'disabled'; $terima = 'checked'; $tidak = 'checked'; $isi = $proses['t2_catatan']; }
    if ( ($proses['t2_status']=='0') And ($sesi <> '001') ) { $disb = 'disabled'; $tolak = 'checked'; $ya = 'checked'; $isi = $proses['t2_catatan']; }
    if ( ($proses['t2_status']=='2') And ($sesi == '001') ) {  $terima = 'checked'; $isi = $proses['t2_catatan'];}
    if ( ($proses['t2_status']=='0') And ($sesi == '001') ) {  $tolak = 'checked'; $isi = $proses['t2_catatan'];}
    if ( ($proses['t2_status']=='1') And ($sesi == '001') ) {  $isi = '';}
    if ( ($proses['t2_status']=='1') And ($sesi <> '001') ) {   $isi = '';}

?>

<div class="row" ng-app="app" >
    <div class="col-md-12">
        <div class="box box-widget">
            <div class="box-header with-border">
              <h3 style="margin:0px">F2. Teliti Dokumen</h3>
            </div>
            <form name="myForm" class="form-horizontal" action="<?=site_url('Revisi_dja/crud_form2_proses')?>" onsubmit="" method="post" role="form">
                <div class="box-body">
                    <?php $this->load->view('revisi_dja/v_info'); ?>
                    <?php $this->load->view('revisi_dja/v_info_files'); ?>
                    

                            <div class="col-sm-12 text-center" style="padding:0px;background:#3C8DBC;margin:5px 0px 15px 0px">
                                <label class="checkbox-custom-label" style="color:#FFF;padding:5px 15px">
                                    HASIL PENELITIAN KELENGKAPAN DOKUMEN USULAN REVISI
                                </label>
                            </div>

                            <div class="form-group jarak">
                                <label class="col-sm-2 rev text-right">Hasil : </label>
                                <div id="pilih" class="col-sm-8" style="padding:0px">
                                    <label class="col-sm-12" style="padding-left:0px">
                                        <input id="radio-terima" class="radio-custom" name="radio-group"  value="1" type="radio" <?php echo "$terima $disb" ?> onclick="toggle_opt('terima')">
                                        <label for="radio-terima" class="radio-custom-label">Diterima</label>
                                    </label>
                                    <label class="col-sm-12" style="padding-left:0px">
                                        <input id="radio-tolak" class="radio-custom" name="radio-group" value="2" type="radio"  <?php echo "$tolak  $disb" ?> onclick="toggle_opt('tolak')">
                                        <label for="radio-tolak" class="radio-custom-label">Pengembalian</label>
                                    </label>
                                </div>
                                <?php echo $proses['t2_status']=='0'; ?>
                            </div>
                            <div id="tolak_up" style="display:none">
                              <div class="form-group jarak">
                                <label class="col-sm-2 rev text-right">Penelaahan : </label>
                                <div class="col-sm-8" style="padding:0px">
                                    <label class="col-sm-12" style="padding-left:0px">
                                        <input id="radio-1" class="radio-custom" name="radio-group2" value="1" type="radio" <?php echo "$tidak $disb" ?>  onclick="toggle_opt1('no')">
                                        <label for="radio-1" class="radio-custom-label">Tidak dilakukan penelaahan</label>
                                    </label>
                                    <label class="col-sm-12" style="padding-left:0px">
                                        <input id="radio-2" class="radio-custom" name="radio-group2" type="radio" value="2" <?php echo "$ya $disb" ?>  onclick="toggle_opt1('yes')">
                                        <label for="radio-2" class="radio-custom-label">Dilakukan penelaahan</label>
                                    </label>
                                </div>
                            </div>
                                
                            </div>

                            <div class="form-group jarak">
                                <label class="col-sm-2 rev text-right">Catatan : </label>
                                <div class="col-sm-8" style="padding:0px">
                                    <?php if  ($proses['t2_status']=='1') { ?>
                                    <textarea class="form-control" placeholder="Catatan hasil penelitian dokumen" name="t2_catatan" <?php echo $disb ?> rows="5" required  ></textarea>
                                    <?php } else { ?>
                                    <textarea class="form-control"><?php echo $isi; ?></textarea>
                                     <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <div class="col-sm-12 pull-right">
                        <div class="pull-right" >
                            <input type="hidden" name="rev_id"  id="" value="<?php echo $rev_id ?>" >

                             <!-- <button type="submit" id="proses" echo $disb  class="btn btn-primary" onclick="return validasi_input(this)">Proses</button> -->
                             <button  type="submit" id="proses" disabled="disabled"  > </button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript">
    $("#form2").fileinput({
        uploadUrl: "<?php echo site_url("revisi_SatuDJA/fileupload_form2/form2/$rev_id") ?>",
        allowedFileExtensions : ['17','jpg','xlsx','doc','docx','rtf','pdf'],
        overwriteInitial: true,
        maxFileSize: 1000,
        slugCallback: function(filename) {
            return filename.replace('(', '_').replace(']', '_');
        }
    });
    $("#form2").on('filebatchuploadcomplete', function(event, files, extra) {
        var id = event.target.id;
        var name = '';
        if (id=='form2') name = 't2_tolak_file';
        $("#"+name).val( $("#"+id).val() );
        alert($("#"+id).val());
    });

    function pdf_routing(rev_id) {
    window.location.href = '<?= site_url('Revisi_dja/pdf_routing') ?>' +'/'+ $('#rev_id').val();
  }
</script>