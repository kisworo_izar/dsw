<?php
    $dbrevisi = $this->load->database('revisi'. $this->session->userdata('thang'), TRUE);
    $query = $dbrevisi->query("Select doc_usulan_revisi, doc_matrix, doc_rka, doc_adk, doc_dukung_sepakat, doc_dukung_hal4, doc_dukung_lainnya, doc_dukung_es1 From revisi Where rev_id='$rev_id'");
    $docs  = $query->row_array();
    //print_r($docs); exit;

    $link_F01 = '';  $link_FE1 = ''; $doc_F01 = '';  
    if ( $docs['doc_usulan_revisi']!='' And $docs['doc_usulan_revisi']!='' And file_exists("files/revisi_SatuDJA/$rev_id/". $docs['doc_usulan_revisi']) ) {
        $link_F01 = '<a href="'. site_url("files/revisi_SatuDJA/$rev_id/". $docs['doc_usulan_revisi']) .'">';  
        $link_FE1 = '</a>'; $doc_F01 = 'checked'; 
    }
    $link_F02 = '';  $link_FE2 = ''; $doc_F02 = '';  
    if ( $docs['doc_matrix']!='' And file_exists("files/revisi_SatuDJA/$rev_id/". $docs['doc_matrix']) ) {
        $link_F02 = '<a href="'. site_url("files/revisi_SatuDJA/$rev_id/". $docs['doc_matrix']) .'">';  
        $link_FE2 = '</a>'; $doc_F02 = 'checked'; 
    }
    $link_F03 = '';  $link_FE3 = ''; $doc_F03 = '';  
    if ( $docs['doc_rka']!='' And file_exists("files/revisi_SatuDJA/$rev_id/". $docs['doc_rka']) ) {
        $link_F03 = '<a href="'. site_url("files/revisi_SatuDJA/$rev_id/". $docs['doc_rka']) .'">';  
        $link_FE3 = '</a>'; $doc_F03 = 'checked'; 
    }
    $link_F04 = '';  $link_FE4 = ''; $doc_F04 = '';  
    if ( $docs['doc_adk']!='' And file_exists("files/revisi_SatuDJA/$rev_id/". $docs['doc_adk']) ) {
        $link_F04 = '<a href="'. site_url("files/revisi_SatuDJA/$rev_id/". $docs['doc_adk']) .'">';  
        $link_FE4 = '</a>'; $doc_F04 = 'checked'; 
    }
    $link_F05 = '';  $link_FE5 = ''; $doc_F05 = '';  
    if ( $docs['doc_dukung_sepakat']!='' And file_exists("files/revisi_SatuDJA/$rev_id/". $docs['doc_dukung_sepakat']) ) {
        $link_F05 = '<a href="'. site_url("files/revisi_SatuDJA/$rev_id/". $docs['doc_dukung_sepakat']) .'">';  
        $link_FE5 = '</a>'; $doc_F05 = 'checked'; 
    }
    $link_F06 = '';  $link_FE6 = ''; $doc_F06 = '';  
    if ( $docs['doc_dukung_hal4']!='' And file_exists("files/revisi_SatuDJA/$rev_id/". $docs['doc_dukung_hal4']) ) {
        $link_F06 = '<a href="'. site_url("files/revisi_SatuDJA/$rev_id/". $docs['doc_dukung_hal4']) .'">';  
        $link_FE6 = '</a>'; $doc_F06 = 'checked'; 
    }
    $link_F07 = '';  $link_FE7 = ''; $doc_F07 = '';  
    if ( $docs['doc_dukung_lainnya']!='' And file_exists("files/revisi_SatuDJA/$rev_id/". $docs['doc_dukung_lainnya']) ) {
        $link_F07 = '<a href="'. site_url("files/revisi_SatuDJA/$rev_id/". $docs['doc_dukung_lainnya']) .'">';  
        $link_FE7 = '</a>'; $doc_F07 = 'checked'; 
    }
    $link_F08 = '';  $link_FE8 = ''; $doc_F08 = '';  
    if ( $docs['doc_dukung_es1']!='' And file_exists("files/revisi_SatuDJA/$rev_id/". $docs['doc_dukung_es1']) ) {
        $link_F08 = '<a href="'. site_url("files/revisi_SatuDJA/$rev_id/". $docs['doc_dukung_es1']) .'">';  
        $link_FE8 = '</a>'; $doc_F08 = 'checked'; 
    }
    $link_F09 = '';  $link_FE9 = ''; $doc_F09 = '';  
    if ( file_exists("files/revisi_SatuDJA/$vrev_id/adk_satker") ) {
        $link_F09 = '<a href="'. site_url("Revisi_dja/create_zip_adk/$vrev_id") .'">';  
        $link_FE9 = '</a>'; $doc_F09 = 'checked'; 
    }
?>


<div class="col-sm-12 text-center" style="padding:0px;background:#3C8DBC;margin:5px 0px 15px 0px">
  <label class="checkbox-custom-label" style="color:#FFF;padding:5px 15px">KELENGKAPAN DOKUMEN USULAN REVISI</label>
</div>

<div style="margin-top:20px">
  <div style="padding-top:20px">

    <div class="form-group jarak">
        <label class="col-sm-2 rev text-right"></label>
        <div class="col-sm-8" style="padding:0px">
            <input id="chk-F01" class="checkbox-custom" name="chk-F01" type="checkbox" <?= $doc_F01 ?> disabled>
            <label for="chk-F01" class="checkbox-custom-label fdok infotext">
                <?= $link_F01 ?> Surat usulan revisi yang ditandatangani pejabat eselon I <?= $link_FE1 ?>
            </label>
        </div>
    </div>
    <div class="form-group jarak">
        <label class="col-sm-2 rev text-right"></label>
        <div class="col-sm-8" style="padding:0px">
            <input id="chk-F04" class="checkbox-custom" name="chk-F04" type="checkbox" <?= $doc_F04 ?> disabled>
            <label for="chk-F04" class="checkbox-custom-label fdok infotext">
                <?= $link_F04 ?> Arsip Data Komputer (ADK) RKA-K/L DIPA revisi Satker <?= $link_FE4 ?>
           
                 <br /> 
                <?php
                if ( file_exists("files/revisi_SatuDJA/$rev_id/adk_satker") ) {
                  echo '<a href="'. site_url("Revisi_dja/create_zip_adk/$rev_id").'"'.' style="color:#3C8DBC"> ( ADK Persatker ) </a> </li>';}
                ?> 
            </label>
           
           <!--  <input id="chk-F09" class="checkbox-custom" name="chk-F09" type="checkbox" <?= $doc_F09 ?> disabled>
            <label for="chk-F04" class="checkbox-custom-label fdok infotext">
                <?= $link_F09 ?> Arsip Data Komputer (ADK) Per Satker <?= $link_FE9 ?>
            </label> -->
        </div>
    </div>
     <div class="form-group jarak">
        <label class="col-sm-2 rev text-right"></label>
        <div class="col-sm-8" style="padding:0px">
            <input id="chk-F04" class="checkbox-custom" name="chk-F04" type="checkbox" checked="">
            <label for="chk-F04" class="checkbox-custom-label fdok infotext">
                <a href="<?=site_url('Monit_satker/adk_per_tiket/') .'/'. $rev_id; ?>"> Informasi ADK </a>
            </label>
        </div>
    </div>

        <div class="form-group jarak">
        <label class="col-sm-2 rev text-right"></label>
        <div class="col-sm-8" style="padding:0px">
            <input id="chk-F08" class="checkbox-custom" name="chk-F08" type="checkbox" <?= $doc_F08 ?> disabled>
            <label for="chk-F08" class="checkbox-custom-label fdok infotext">
                <?= $link_F08 ?> Dokumen pendukung terkait persetujuan unit eselon I  <?= $link_FE8 ?>
            </label>
        </div>
        </div>

    <!-- <div class="form-group jarak">
        <label class="col-sm-2 rev text-right"></label>
        <div class="col-sm-8" style="padding:0px">
            <input id="chk-F05" class="checkbox-custom" name="chk-F05" type="checkbox" <?= $doc_F05 ?> disabled>
            <label for="chk-F05" class="checkbox-custom-label fdok infotext">
                <?= $link_F05 ?> Dokumen pendukung terkait sesuai hasil kesepakatan antara K/L dengan DJA dalam pembahasan usulan revisi anggaran <?= $link_FE5 ?>
            </label>
        </div>
    </div> -->
    <!-- <div class="form-group jarak">
        <label class="col-sm-2 rev text-right"></label>
        <div class="col-sm-8" style="padding:0px">
            <input id="chk-F06" class="checkbox-custom" name="chk-F06" type="checkbox" <?= $doc_F06 ?> disabled>
            <label for="chk-F06" class="checkbox-custom-label fdok infotext">
                <?= $link_F06 ?> Dokumen pendukung terkait perubahan/penghapusan catatan dalam halaman IV DIPA <?= $link_FE6 ?>
            </label>
        </div>
    </div> -->
    <div class="form-group jarak">
        <label class="col-sm-2 rev text-right"></label>
        <div class="col-sm-8" style="padding:0px">
            <input id="chk-F07" class="checkbox-custom" name="chk-F07" type="checkbox" <?= $doc_F07 ?> disabled>
            <label for="chk-F07" class="checkbox-custom-label fdok infotext">
                <?= $link_F07 ?> Dokumen pendukung terkait lainnya <?= $link_FE7 ?>
            </label>
        </div>
    </div>

  </div>
</div>

<?php $this->load->view('revisi_dja/v_info_files_perbaikan'); ?>