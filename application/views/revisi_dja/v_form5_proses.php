<link href="<?=base_url('assets/bootstrap/css/bootstrap-datetimepicker.min.css');?>" rel="stylesheet" type="text/css" />
<script src="<?=base_url('assets/plugins/datepicker/moment.min.js');?>" type="text/javascript"></script>
<script src="<?=base_url('assets/plugins/datepicker/id.js');?>" type="text/javascript"></script>
<script src="<?=base_url('assets/bootstrap/js/bootstrap-datetimepicker.min.js');?>" type="text/javascript"></script>
<link href="<?=base_url(); ?>assets/plugins/uploadfile/fileinput.css" rel="stylesheet">
<script src="<?=base_url();?>assets/plugins/uploadfile/fileinput.min.js" type="text/javascript"></script>

<style media="screen">
.checkbox-custom, .radio-custom{opacity: 0;position: absolute;}
.checkbox-custom, .checkbox-custom-label, .radio-custom, .radio-custom-label{
    display: inline-block;vertical-align: middle;margin: 0px;}
.checkbox-custom-label, .radio-custom-label {position: relative;}
.checkbox-custom + .checkbox-custom-label:before, .radio-custom + .radio-custom-label:before{
    content: '';background: #fff;border: 1px solid #DFDFDF;display: inline-block;vertical-align: middle;
    width: 20px;height: 20px;padding: 2px;margin-right: 5px;text-align: center;}
.checkbox-custom:checked + .checkbox-custom-label:before{background: #3C8DBC;}
.radio-custom + .radio-custom-label:before{border-radius: 50%;}
.radio-custom:checked + .radio-custom-label:before {background: #3C8DBC;}
.checkbox-custom:focus + .checkbox-custom-label, .radio-custom:focus + .radio-custom-label {outline: 0px solid #ddd;}
.inforev{padding-top:0px;padding-left:0px;}
.infotext{font-weight: normal}
.infojarak{margin-bottom:0px;}
.jarak{margin-bottom:8px;}
.fdok{padding-left:30px;text-indent:-30px}
</style>

<script>
function toggle_opt($opt) {
 if( $opt == 'tidak' ){
     document.getElementById("surat_perbaikan").style.display = 'none';
     document.getElementById("proses").innerHTML = 'Proses';
     $('#proses').removeClass('btn-info');
     $('#proses').addClass('btn-primary');
 }else{
     document.getElementById("surat_perbaikan").style.display = '';
     document.getElementById("proses").innerHTML = 'Proses Surat Permintaan';
     $('#proses').removeClass('btn-primary');
     $('#proses').addClass('btn-info');
    }
}
</script>
<script type="text/javascript">
    $(function () {
        $('.datetimepicker1').datetimepicker({
            format : 'YYYY-MM-DD',
        });
    });
</script>

<?php
    $disb = ''; $terima = ''; $tolak = ''; $sesi=$this->session->userdata('idusergroup');$tidak = ''; $ya = '';
    if ( ($proses['t5_status']=='2') And ($sesi <> '001') ) { $disb = 'disabled';  $terima = 'checked'; $tidak = 'checked'; }
    if ( ($proses['t5_status']=='0') And ($sesi <> '001') ) { $disb = 'disabled'; $tolak = 'checked'; $ya = 'checked'; }    
?> 

<div class="row" ng-app="app" >
    <div class="col-md-12">
        <div class="box box-widget">
            <div class="box-header with-border">
              <h3 style="margin:0px">F5. Perbaikan Dokumen</h3>
            </div>
            <form name="myForm" class="form-horizontal" action="<?=site_url('revisi_dja/crud_form5_proses') ?>" method="post" role="form">
                <div class="box-body">
                    <?php $this->load->view('revisi_dja/v_info'); ?>

                    <div class="col-sm-12 text-center" style="padding:0px;background:#3C8DBC;margin:5px 0px 15px 0px">
                        <label class="checkbox-custom-label" style="color:#FFF;padding:5px 15px">
                            PERMINTAAN PERBAIKAN DOKUMEN REVISI ANGGARAN
                        </label>
                    </div>

                    <div style="margin-top:20px">

                        <div class="form-group jarak">
                            <label class="col-sm-2 rev text-right"></label>
                            <div class="col-sm-8" style="padding:0px">
                                <label class="col-sm-12" style="padding-left:0px">
                                    <input id="radio-tidak" class="radio-custom" name="radio-group" value="1" type="radio" checked <?php echo "$tidak $disb" ?>   onclick="toggle_opt('tidak')" >
                                    <label for="radio-tidak" class="radio-custom-label">Telah disampaikan pada saat penelaahan</label>
                                </label>
                                <label class="col-sm-12" style="padding-left:0px">
                                    <input id="radio-surat" class="radio-custom" name="radio-group" value="2" type="radio" <?php echo "$ya $disb" ?>  onclick="toggle_opt('surat')">
                                    <label for="radio-surat" class="radio-custom-label">Surat permintaan perbaikan dokumen revisi anggaran</label>
                                </label>
                            </div>
                        </div>
                        <div id="surat_perbaikan" style="display:none">
                            <div class="form-group jarak">
                                <label class="col-sm-2 rev text-right">No. Surat : </label>
                                <div class="col-sm-3" style="padding:0px">
                                    <input name="t5_hasil_no" class="form-control" placeholder="Nomor Surat" value="" >
                                </div>
                                <label class="col-sm-2 rev text-right"  style="padding-right:0px">Tanggal Surat : </label>
                                <div class="col-sm-3" style="padding-right:0px">
                                    <div class="input-group date datetimepicker1">
                                        <input name="t5_hasil_tgl" id="t5_hasil_tgl" type="text" class="form-control" placeholder="Tanggal Surat">
                                        <span class="input-group-addon">
                                            <i class="fa fa-calendar text-primary"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group jarak" id="surat_perbaikan">
                                <label class="col-sm-2 rev text-right">File Surat :</label>
                                <div class="col-sm-8" style="padding:0px">
                                    <input type="hidden" name="t5_hasil_file" id="t5_hasil_file">
                                    <input id="form5" name="form5" type="file" class="file" data-show-preview="false">
                                </div>
                            </div>
                        </div>
                        <div class="form-group jarak">
                            <label class="col-sm-2 rev text-right">Catatan : </label>
                            <div class="col-sm-8" style="padding:0px">
                                <textarea class="form-control" placeholder="Catatan perbaikan" name="t5_hasil_catatan" rows="5"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <div class="col-sm-12 pull-right">
                        <div class="pull-right">
                            <input type="hidden" name="rev_id" id="rev_id" value="<?php echo $rev_id?>" >
                            <button type="submit" id="proses" class="btn btn-primary">Proses </button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript">
    $('#form5').fileinput({
        uploadExtraData: function(previewID,index){
            var data = {rev_id : $("#rev_id").val(), name: 'form5'};
            return data;
        },
        uploadUrl: "<?php echo site_url('revisi_45/fileupload_form4')  ?>",
        allowedFileExtensions : ['pdf'],
        msgInvalidFileExtension: "Invalid extension file !",
        overwriteInitial: true,
        maxFileCount: 1,
        maxFileSize: 5000
    });
    $('#form5').on('filebatchuploadsuccess', function(event, data, extra) {
      var extra = data.extra, response = data.response;
      $('#t5_hasil_file').val( response.uploaded );
      $('#t5_hasil_file').attr('type','text'); $('#form5').css('display', 'none');
    });
    $('#form5').on('filebatchuploaderror', function(event, data) {
      var response = data.response;
      alert('Gagal upload !');
    });
</script>