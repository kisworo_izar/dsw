<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-type" content="text/html;charset=UTF-8">
<style type="text/css">
.vertical_line{margin-top:3px;height:3px; width:700px;background:#000;}
.vertical_line2{margin-top:2px;height:1px; width:700px;background:#000;}
table {
    border-collapse: collapse;
    font-family: Arial,Helvetica,sans-serif;
    font-style: normal;font-variant:normal;
  }
   
body{
    margin:40px 20px 0px 0px;
}
.square {
    width: 10px;
    height: 10px;
    border: solid;
}
.squareinside{
    width: 9px;
    height: 12px;
}
.inlineTable {
    display: inline-block;
}
@page { margin: 0px 50px; }
</style>
</head>
<body>
  <table>
        <tbody>
        <tr>
            <td width="50"><img src="<?php echo site_url('files/cuti/header.jpg'); ?>" width="96" height="100%" /></td>
            <td width="527">
                <table style="margin-left:40px;" >
                    <tbody>
                        <tr>
                            <td style="font-size: 13pt;text-align:center"><strong>KEMENTERIAN KEUANGAN REPUBLIK INDONESIA</strong></td>
                        </tr>
                        <tr>
                            <td style="font-size: 12pt;text-align:center"><strong>DIREKTORAT JENDERAL ANGGARAN</strong></td>
                        </tr>
                        <tr>
                            <td style="font-size: 11pt;text-align:center"><strong>SEKRETARIAT DIREKTORAT JENDERAL</strong></td>
                        </tr>
                        <tr style="margin-top:100px;">
                          <td></td>
                        </tr>
                        <tr>
                            <td style="font-size: 7pt;text-align:center">GEDUNG SUTIKNO SLAMET LANTAI 11, JALAN DR.WAHIDIN NOMOR 1, JAKARTA 10710</td>
                        </tr>
                        <tr>
                            <td style="font-size: 7pt;text-align:center">TELEPON (021) 3866117: FAKSIMILE (021) 3866117;FAKSIMILE (021) 38505118; SITUS www.anggaran.kemenkeu.go.id</td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        </tbody>
    </table>

    <div class="vertical_line"></div>
    <div class="vertical_line2"></div>

<div style="text-align:center;margin-top:10px;font-size:11px;width:290px;height:80px;border:solid;">
    <table>
        <tbody>
            <tr>
                <td>Nomor Tiket</td>
                <td>:</td>
                <td><?php echo $revisi['rev_id']; ?>/<?php echo $revisi['rev_tahun']; ?></td>
            </tr>
            <tr>
                <td>Diterima</td>
                <td>:</td>
                <td><?php echo $revisi['pus_tgl']; ?></td>

            </tr>
            <tr>
                <td style="vertical-align: top;">Pemroses</td>
                <td style="vertical-align: top;">:</td>
                <td style="vertical-align: top;"><?php echo $revisi['nmso']; ?></td>
            </tr>
        </tbody>
    </table>
</div>

<div class="inlineTable" style="width: 90px; height: 90px; margin-left: 270px;margin-top: -50px!important;">
<table>
  <tbody>
    <tr>
      <td>
       <?php
        $foto_profile="files/profiles/_noprofile.png";
        if (file_exists("files/profiles/".$this->session->userdata('nip').".gif")) {$foto_profile =  "files/profiles/".$this->session->userdata('nip').".gif";}
      ?>
      <img src="<?php echo site_url('files/revisi/'.$revisi["rev_id"].'/qrcode_'.$revisi["rev_id"].'.png'); ?>" width="150px" height="150px"  /></td>
    </tr>
  </tbody>
</table>
</div>

  <table style="text-align:center;margin-left:150px;margin-bottom:5px;">
    <tbody>
      <tr>
        <td style="font-size:14pt;"><center><strong>TANDA TERIMA</strong></center></td>
      </tr>
      <tr>
        <td><center>PENGAJUAN REVISI ANGGARAN</center></td>
      </tr>
      <tr>
        <td><center>
           <?php if ($cari['revisi_ke'] !=null) echo "(Tambahan Data Dukung)"; else echo "(Pengajuan Awal)"; ?>
      </tr>
    </tbody>
  </table>


    <table style="font-size:11pt;margin-top:25px;margin-left:10px;">
      <tbody>
        <tr>
          <td>Nomor Surat Usulan</td>
          <td>:</td>
          <td><?php echo $revisi['kl_surat_no'] ?></td>
        </tr>
        <tr>
          <td>Tanggal Surat Usulan</td>
          <td>:</td>
          <td><?php echo $this->fc->idtgl( $revisi['kl_surat_tgl'], 'hari') ?></td>
        </tr>
      </tbody>
    </table>
    
    <table style="font-size:11pt;margin-left:10px;margin-top:17px">
        <tbody>
            <tr>
                <td><strong>Informasi unit Pengusul</strong></td>
            </tr>
        </tbody>
    </table>

    <table style="font-size:11pt;margin-left:10px;">
        <tbody>
            <tr>
              <td>Nama K/L</td>
              <td>:</td>
              <td><?php echo $revisi['nmdept']; ?></td>
            </tr>
            <tr>
              <td>Eselon</td>
              <td>:</td>
              <td><?php echo $revisi['nmunit']; ?></td>
            </tr>
            <tr>
                <td>Nama Petugas</td>
                <td>:</td>
                <td><?php echo $revisi['kl_pjb_nama'] ?></td>
            </tr>
            <tr>
                <td>NIP</td>
                <td>:</td>
                <td><?php echo $revisi['kl_pjb_nip'] ?></td>
            </tr>
            <tr>
                <td>Jabatan</td>
                <td>:</td>
                <td><?php echo $revisi['kl_pjb_jab'] ?></td>
            </tr>
            <tr>
                <td>No. HP</td>
                <td>:</td>
                <td><?php echo $revisi['kl_telp'] ?></td>
            </tr>
            <tr>
                <td>Email</td>
                <td>:</td>
                <td><?php echo $revisi['kl_email'] ?></td>
            </tr>
        </tbody>
    </table>

    <table style="font-size:11pt;margin-left:10px;margin-top:5px">
        <tbody>
            <tr>
                <td><strong>Kelengkapan Dokumen</strong></td>
            </tr>
        </tbody>
    </table>

<div style="font-family: DejaVu Sans, sans-serif;">
    <table style="margin-left:10px;">
        <tr>
            <td><?php if ($revisi['doc_usulan_revisi']!='') {
                echo '<div class="square" style="background-color:black;"></div>';
            } else {
                echo '<div class="square" style="color:solid"></div>';
            }
             ?></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td> Surat usulan revisi yang ditandatangani pejabat eselon I </td>
        </tr>
        
        <tr>
            <td><?php if ($revisi['doc_matrix']!='') {
                echo '<div class="square" style="background-color:black;"></div>';
            } else {
                echo '<div class="square" style="color:solid"></div>';
            }
             ?></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>Matriks perubahan (semula-menjadi)</td>
        </tr>

        <tr>
            <td><?php if ($revisi['doc_rka']!='') {
                echo '<div class="square" style="background-color:black;"></div>';
            } else {
                echo '<div class="square" style="color:solid"></div>';
            }
             ?></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>Rencana Kerja dan Anggaran (RKA) Satker</td>
        </tr>

        <tr>
            <td><?php if ($revisi['doc_adk']!='') {
                echo '<div class="square" style="background-color:black;"></div>';
            } else {
                echo '<div class="square" style="color:solid"></div>';
            }
             ?></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>Arsip Data Komputer (ADK) RKA-K/L DIPA revisi Satker</td>
        </tr>
         <tr>
            <td><?php if($revisi['doc_dukung_sepakat']!=''){
                echo '<div class="square" style="background-color:black;"></div>' ;
                } else {
                echo '<div class="square" style="color:solid;"></div>';
                    }?></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>Dokumen pendukung terkait sesuai hasil kesepakatan antara K/L dengan DJA dalam pembahasan usulan revisi anggaran</td>
        </tr>
        <tr>
            <td><?php if($revisi['doc_dukung_hal4']!=''){
                echo '<div class="square" style="background-color:black;"></div>' ;
                } else {
                echo '<div class="square" style="color:solid;"></div>';
                    }?></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>Dokumen pendukung terkait perubahan/penghapusan catatan dalam halaman IV DIPA</td>
        </tr>

        <tr>
            <td><?php if($revisi['doc_dukung_lainnya']!=''){
                echo '<div class="square" style="background-color:black;"></div>' ;
                } else {
                echo '<div class="square" style="color:solid;"></div>';
                    }?></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>Dokumen pendukung terkait lainnya</td>
        </tr>
    </table>
    </div>

    <table style="font-size:11pt;margin-left:10px;margin-top:17px">
        <tbody>
            <tr>
                <td><strong>Catatan Petugas Pusat Layanan DJA</strong></td>
            </tr>
        </tbody>
    </table>

    <table style="font-size:11pt;margin-left:10px;">
        <tbody>
            <tr>
                <td><?php echo $revisi['pus_catatan'] ?></td>
            </tr>
        </tbody>
    </table>

   <!--  <table style="margin-left:310px;font-size:11pt;margin-top:40px;">
        <tbody>
            <tr>
                <td>Jakarta, <?php echo $this->fc->idtgl( $revisi['kl_surat_tgl'], 'tglfull') ?></td>
            </tr>
            <tr>
                <td>Petugas Pusat Layanan DJA,</td>
            </tr>
        </tbody>
    </table>

    <table style="margin-left:310px;font-size:11pt;margin-top:60px;">
        <tbody>
            <tr>
                <td>Tufhatul Jamilah</td>
            </tr>
        </tbody>
    </table> -->

    <table style="margin-left:10px;font-size:8pt;margin-top:190px;margin-bottom:5px;">
        <tbody>
            <tr>
                <td><i></i></td>
            </tr>
        </tbody>
    </table>
    <div class="vertical_line2"></div>

    <table style="font-size:9px;margin-top:5px;">
            <tr>
                <td><strong>Pusat Layanan DJA</strong></td>
            </tr>
            <tr>
                <td>Lobby Gedung Sutikno Slamet, Jl. Wahidin No. 1 Jakarta Pusat</td>
            </tr>
            <tr>
                <td>Fax : 021-34832515, Call Center : 021-34832511</td>
            </tr>
            <tr>
                <td>e-mail : pusatlayanan.dja@kemenkeu.go.id</td>
            </tr>
    </table>
    <div class="inlineTable" style="font-size:9px;width:200px;margin-left:220px;border:solid;margin-top:8px;">
        <table>
                <tr>
                    <td><strong>Segala bentuk gratifikasi akan diproses sesuai dengan Peraturan Perundang-Undangan yang berlaku</strong></td>
                </tr>
        </table>
    </div>
</body>
</html>