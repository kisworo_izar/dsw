<style media="screen">
   p{color:#0E5FAE; margin-top: 10px; margin-bottom: 0px}
</style>
<div class="row">
    <div class="col-md-9">
        <div class="box box-widget">
	          <div id="box-body" class="box-body with border">
                <?php
                   require 'simple_html_dom.php';
                   $html = file_get_html( $link );
                   $e = $html->find('.xInstaller');
                   foreach ($e as $key => $value) {
                      echo $value->innertext;
                   }
                   $html->clear();
                   unset($html);
                 ?>

             </div>
        </div>
    </div>

    <div class="col-md-3">

      <div class="box box-widget" style="margin-bottom:0px">
       <div class="box-header with-border" style="border-radius:0px;border-bottom: 1px solid #FEB811; padding-top:3px; padding-bottom:5px; background-color:#1661AB">
           <h3 class="box-title" style="color:#FFFFFF">Sistem Aplikasi</h3>
       </div>
        <div class="box box-widget">
            <div class="box-body with-border" style="padding-bottom:20px">

                    <p class="text-muted">
                        <a href="<?php echo site_url('installer/index/1') ?>" class="pull-left">Aplikasi <b>RKA-K/L DIPA</b></a>
                    </p>
                    <br>
                    <p class="text-muted">
                        <a href="<?php echo site_url('installer/index/2') ?>" class="pull-left">Aplikasi <b>TPNBP</b></a>
                    </p>
                    <br>
                    <p class="text-muted">
                        <a href="<?php echo site_url('installer/index/3') ?>" class="pull-left">Aplikasi <b>SBK</b></a>
                    </p>
                    <br>
                    <p class="text-muted">
                        <a href="<?php echo site_url('installer/index/4') ?>" class="pull-left">Aplikasi <b>ADIK</b></a>
                    </p>
                    <br>
                    <p class="text-muted">
                        <a href="<?php echo site_url('installer/index/5') ?>" class="pull-left">Aplikasi <b>KPJM</b></a>
                    </p>
            </div>
        </div>
    </div>

     <div class="box box-widget hidden-xs" >
         <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
             <div class="carousel-inner">
                 <?php
                 $min = 1; $max = 30; $mid = 4;
                 $ran = range($min, $max);
                 shuffle($ran);

                 for ($i=$min; $i < $mid; $i++) {
                 echo '<div class="item';
                     if ($i==1) echo ' active'; echo '">';
                 ?>
                 <?php $poster="files/poster/poster".$ran[$i].".jpg"; ?>
                 <img src="<?php echo site_url($poster); ?>" >
                 </div>
                 <?php } ?>
             </div>
         </div>
</div>
