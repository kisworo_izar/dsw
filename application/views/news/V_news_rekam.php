<link href="<?php echo base_url(); ?>assets/plugins/uploadfile/fileinput.css" rel="stylesheet">
<script src="<?php echo base_url();?>assets/plugins/uploadfile/fileinput.min.js" type="text/javascript"></script>

<div class='row'>
    <div class='col-md-12'>
        <div class="box box-widget">
			<div class="box-header with border">
                <h3 class='box-title'><?php echo $ruh ?> News Ticker</h3>
            </div>

            <form action="<?php echo site_url('news/crud') ?>" method="post" role="form" enctype="multipart/form-data">
            <div class='box-body pad'>

                <input name="toplist" type="hidden" value="0" />
                <input name="ruh" type="hidden" value="<?php echo $ruh ?>" />
                <input name="idnews" type="hidden" value="<?php echo $table['idnews'] ?>" />

                <div class="form-group">
                    <label>News Ticker :</label>
                    <div class="input-group">
                        <div class="input-group-addon"><i class="fa fa-pencil"></i></div>
                        <input name="notif1" type="text" id="" class="form-control" value="<?php echo $table['notif1'] ?>" />
                    </div>
                    <span class="small">Maksimal 100 Karakter</span>
                </div>
                <div class="form-group">
                    <label>Link :</label>
                    <div class="input-group">
                        <div class="input-group-addon"><i class="fa fa-chain"></i></div>
                        <input name="link" type="text" id="" class="form-control" value="<?php echo $table['link'] ?>" />
                    </div>
                </div>
                <div class="form-group">
                    <label>Files :</label> &nbsp; <span class="text-muted"><?php echo $table['file'] ?></span>
                    <input name="file" type="file" class="file" />
                </div>
                <div class="form-group">
                    <label>Tampilkan : &nbsp;</label>
                    <input name="toplist" type="checkbox" <?php if ($table['toplist']=='1') echo 'checked' ?> value="1"> &nbsp;News Ticker <i class="fa fa-bullhorn text-danger"></i>
                </div>
            </div>

            <div class="box-footer">
                <div class=" pull-right">
                    <input name="simpan" type="submit" id="simpan" class="btn btn-primary" value="<?php echo $ruh ?>">
                </div>
            </div>
            </form>
        </div>
    </div>
</div>

<script src="<?php echo base_url();?>assets/plugins/ckeditor/ckeditor.js"></script>
<script type="text/javascript">
    $(function () {
        CKEDITOR.replace('editor1');
    });
</script>

<script type="text/javascript">
    $("#excel").fileinput({
        uploadUrl: "<?php echo site_url('upload/fileupload') ?>",
        allowedFileExtensions : ['xls','xlsx','doc','docx','rtf','pdf','mp4','mov'],
        overwriteInitial: true,
        maxFileSize: 1000,
        maxFilesNum: 10,
        minFileCount: 1,
        maxFileCount: 5,
        dropZoneTitle: 'Drag & Drop file News Ticker disini ...',
        msgInvalidFileExtension: "Invalid extension file {name}. Hanya {extensions} files yang bisa diproses ...",
        slugCallback: function(filename) {
            return filename.replace('(', '_').replace(']', '_');
        }
    });
</script>
