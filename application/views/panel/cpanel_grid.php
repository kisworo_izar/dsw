<script src="<?=base_url('assets/AngularJS/angular.min.js');?>"></script>
<script src="<?=base_url('assets/AngularJS/angular-locale_id-id.js');?>"></script>
<script src="<?=base_url('assets/AngularJS/angular-input-masks-dependencies.js');?>"></script>
<script src="<?=base_url('assets/AngularJS/angular-input-masks.js');?>"></script>
<script>
  var app = angular.module('app', ['ui.utils.masks'])
  app.controller('ctrlrefr', function ($scope, $http) {
    $scope.hdrColm = 'Menu';
    $scope.hdrAsc  = false;  
    $scope.hdrCari = '';
    $scope.table = <?= $table ?>;
  });
</script>


<div class="row" ng-app="app" ng-init="">
  <div class="col-md-12" ng-controller="ctrlrefr">
    <div class="box box-widget">
      <div class="box-body">

        <table id="iGrid" class="table table-hover table-bordered">
          <thead>
            <tr>
              <?php foreach ($kolom as $row) { ?>
              <th>
                <a href="#" ng-click="hdrColm = '<?= $row['judul'] ?>'; hdrAsc = !hdrAsc"> <?= $row['judul'] ?>
                  <span ng-show="hdrColm=='<?= $row['judul'] ?>' && !hdrAsc" class="fa fa-caret-down"></span>
                  <span ng-show="hdrColm=='<?= $row['judul'] ?>' && hdrAsc" class="fa fa-caret-up"></span>
                </a>
              </th>
              <?php } ?>
            </tr>
          </thead>
          <tbody>
            <tr style="margin:0px;">
              <td colspan="5">
                <form>
                  <div class="form-group">
                    <div class="input-group">
                      <span class="input-group-btn">
                        <button id="btn_data" onclick="newadd()" class="btn btn-primary"><i class="fa fa-plus"></i></button>
                      </span>
                      <div class="input-group-addon"><i class="fa fa-search"></i></div>
                      <input type="text" class="form-control" placeholder="ketik kata cari" ng-model="hdrCari">
                    </div>      
                  </div>
                </form>
                <div id="addform" style="display: none"><?php $this->load->view('panel/cpanel_crud'); ?></div>
              </td>
            </tr>

            <tr style="margin:0px;" class="{{ row.idmenu }}:{{ row.menu }}:{{ row.link }}:{{ row.icon }}:{{ row.urutan }}" id="{{ row.idmenu }}" ng-repeat="row in table | orderBy:hdrColm:hdrAsc | filter:hdrCari" onclick="ajax_layer( this.id )">
              <?php foreach ($kolom as $row) { ?>
                <td class="text-primary">{{ row.<?= $row['nama'] ?> }}</td>
              <?php } ?>
            </tr>

          </tbody>
        </table>

      </div>
    </div>
  </div>
</div>


<script type="text/javascript">
  function ajax_layer( idkey ) {
    var arr = $('#'+idkey).attr('class').split(":");
    var htm = $('#addform').html(); if (htm=='') htm = $('#crud').html();
    var msg = '<tr class="child"><td colspan="5"><div id="crud">'+ htm +'</div></td></tr>';

    $('#addform').html('');
    $('table tr.child').remove();
    $('#'+arr[0]).after(msg);

    $('input').each(function () {
      $(this).val( arr[$(this).attr('id')] );
    })
  }

  function newadd( idkey ) {
    var htm = $('#addform').html(); if (htm=='') htm = $('#crud').html();

    $('table tr.child').remove();
    $('#addform').html( htm );
  }
</script>



<style media="screen">
  table { border-spacing: 1; width: 100%; }
</style>


