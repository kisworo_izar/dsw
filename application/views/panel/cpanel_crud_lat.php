<script src="<?=base_url('assets/AngularJS/angular.min.js');?>"></script>
<script src="<?=base_url('assets/AngularJS/angular-locale_id-id.js');?>"></script>
<script src="<?=base_url('assets/AngularJS/angular-input-masks-dependencies.js');?>"></script>
<script src="<?=base_url('assets/AngularJS/angular-input-masks.js');?>"></script>
<script>
  var app = angular.module('app', ['ui.utils.masks'])
  app.controller('ctrlrefr', function ($scope, $http) {
    $scope.hdrColm = 'menu';
    $scope.hdrAsc  = false;  
    $scope.hdrCari = '';
    $scope.table = <?= $table ?>;
    $scope.field = <?= $field ?>;
  });
</script>


<div class="box box-widget" ng-app="app">
  <div class="box-body" ng-controller="ctrlrefr">

    <form role="form" name="myForm" class="form-horizontal">
      <?php foreach ($kolom as $row) { ?>
        <div class="form-group">
          <label class="col-md-2 text-right"><?=$row['judul']?></label>
          <div class="col-md-4">
            <input name="<?=$row['nama']?>" ng-model="<?=$row['nama']?>" type="text" class="form-control" placeholder="<?=$row['nama']?>">
          </div>
        </div>
      <?php } ?>

      <div class= "form-group">
        <label class= "col-md-2"></label>
        <div class="col-md-4">
          <button ng-click="saveCustomer(customer);" class="btn btn-primary">Simpan</button>
          <button ng-click="deleteCustomer(customer)" class="btn btn-warning">Delete</button>
        </div>
      </div>
    </form>



  </div>
</div>

