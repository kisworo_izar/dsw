<!DOCTYPE html>
<html>
<head>
  <?php $this->load->view('main/utama_link'); ?>
  <script src="<?=base_url('assets/AngularJS/angular.min.js');?>"></script>
  <script src="<?=base_url('assets/AngularJS/angular-locale_id-id.js');?>"></script>
  <script src="<?=base_url('assets/AngularJS/angular-input-masks-dependencies.js');?>"></script>
  <script src="<?=base_url('assets/AngularJS/angular-input-masks.js');?>"></script>
  <script>
    var app = angular.module('app', ['ui.utils.masks'])
    app.controller('ctrlrefr', function ($scope, $http) {
      $scope.sortType = 'menu';
      $scope.sortReverse  = false;  
      $scope.searchFish   = '';
      $scope.table = <?= $table ?>;
      $scope.field = <?= $field ?>;
    });
  </script>
</head>

<body ng-app="app">

<div class="row" ng-controller="ctrlrefr">
  <div class="col-md-12">
    <div class="box box-widget">
      <div class="box-body">

      <div class="alert alert-info">
        <p>Sort Type: {{ sortType }}</p>
        <p>Sort Reverse: {{ sortReverse }}</p>
        <p>Search Query: {{ searchFish }}</p>
      </div>

      <table class="table table-bordered">
        <thead>
          <tr>
          <?php 
            foreach ($kolom->t_menu as $row) {
              $nil = $row->header;
              echo "
              <th>
                <a href='#' ng-click='sortType = \"$nil\"; sortReverse = !sortReverse'> $nil
                  <span ng-show='sortType == \"$nil\" && !sortReverse' class='fa fa-caret-down'></span>
                  <span ng-show='sortType == \"$nil\" && sortReverse' class='fa fa-caret-up'></span>
                </a>
              </th>";
            }
          ?>
          </tr>
        </thead>
        <tbody>
          <?php 
            $str = '<tr ng-repeat="row in table | orderBy:sortType:sortReverse">';
            foreach ($kolom->t_menu as $row) $str .= '<td>{{ row.'. $row->menu .' }}</td>';
            echo "$str </tr>";
          ?>
        </tbody>
      </table>

      </div>
    </div>
  </div>
</div>



</body>
</html>
