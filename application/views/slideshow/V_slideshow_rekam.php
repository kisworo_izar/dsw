<link href="<?php echo base_url(); ?>assets/plugins/uploadfile/fileinput.css" rel="stylesheet">
<script src="<?php echo base_url();?>assets/plugins/uploadfile/fileinput.min.js" type="text/javascript"></script>

<section class="content">
    <div class='row'>
        <!-- <div class='col-md-12'> -->
            <div class='box box-widget'>
                <div class='box-header'>
                    <h3 class='box-title'><?php echo $ruh ?> Data Slideshow</h3>
                </div><!-- /.box-header -->

                <form action="<?php echo site_url('slideshow/crud') ?>" method="post" role="form" enctype="multipart/form-data">
                <div class='box-body pad'>
                    <div class="form-group">
                        <label>Keterangan :</label>
                        <div class="input-group">
                            <div class="input-group-addon"><i class="fa fa-newspaper-o"></i></div>
                            <input name="ruh" type="hidden" value="<?php echo $ruh ?>" />
                            <input name="idslideshow" type="hidden" value="<?php echo $table['idslideshow'] ?>" />
                            <input name="keterangan" type="text" id="" placeholder="Informasi yang akan ditampilkan pada slideshow" class="form-control" value="<?php echo $table['keterangan'] ?>" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Image : </label> &nbsp; <span class="text-muted"><?php echo $table['gambar'] ?></span>
                        <div class="input-group">
                            <div class="input-group-addon"><i class="fa fa-image"></i></div>
                            <input name="gambar" type="file" class="file" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Link :</label>
                        <div class="input-group">
                            <div class="input-group-addon"><i class="fa fa-link"></i></div>
                            <input name="link" type="text" id="" placeholder="Alamat yang akan ditautkan pada gambar" class="form-control" value="<?php echo $table['link'] ?>" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Tampilkan : &nbsp;</label>
                        <input name="aktif" type="checkbox" <?php if ($table['kdaktif']=='1') echo 'checked' ?> value="1"> &nbsp; Slideshow akan ditampilkan
                    </div>
                </div>

                <div class="box-footer">
                    <div class=" pull-right">
                        <input name="simpan" type="submit" id="simpan" class="btn btn-primary" value="<?php echo $ruh ?>">
                    </div>
                </div>

                </form>
            </div>

        <!-- </div> -->
    </div><!-- ./row -->
</section><!-- /.content -->
