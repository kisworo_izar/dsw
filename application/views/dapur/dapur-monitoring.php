<!-- Google Fonts -->
<link href="https://fonts.googleapis.com/css?family=Roboto:400,700&amp;subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
<link href="<?= base_url(); ?>assets/plugins/materialicons/materialicons.css" rel="stylesheet" type="text/css">
<!-- bootstrapValidator -->
<link rel="stylesheet" href="<?= base_url(); ?>assets/plugins/bootstrapvalidator/css/bootstrapValidator.min.css" />
<!-- SweetAlert2 -->
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
<!-- Waves Effect Css -->
<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/plugins/node-waves/waves.css">
<!-- Animation Css -->
<link href="<?= base_url(); ?>assets/plugins/animate-css/animate.css" rel="stylesheet" />
<!-- Bootstrap DatePicker Css -->
<link href="<?= base_url(); ?>assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.css" rel="stylesheet" />
<!-- Bootstrap Select Css -->
<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/plugins/bootstrap-select/css/bootstrap-select.css">
<!-- Dropify -->
<link href="<?= base_url(); ?>assets/plugins/dropify/css/dropify.min.css" rel="stylesheet">
<!--WaitMe Css-->
<link href="<?= base_url(); ?>assets/plugins/waitme/waitMe.css" rel="stylesheet" />
<!-- AdminBSB Custom Css -->
<link href="<?= base_url(); ?>assets/plugins/adminbsb/style.css" rel="stylesheet">

<style type="text/css">
  form .dropify-wrapper p {
    text-align: center;
  }

  .swal2-radio [type="radio"]:not(:checked),
  .swal2-radio [type="radio"]:checked {
    position: unset;
    opacity: 1;
  }

  .form-group .form-line.focused .form-label {
    top: -15px;
  }

  .bs-searchbox .form-control {
    width: 88%;
  }

  .capitalise {
    text-transform: capitalize;
  }

  .bootstrap-select .dropdown-menu {
    width: 100%;
  }

  .bootstrap-select .dropdown-menu li small {
    white-space: normal;
  }
</style>

<div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="card">
      <div class="header">
        <h2>
          MONITORING PERATURAN DAN KEBIJAKAN
        </h2>
        <button type="button" class="btn btn-primary waves-effect" data-toggle="modal" data-target="#monitoring-modal" data-action="add" style=" position: absolute; top: 10px; right: 20px; ">
          <i class="material-icons">add</i>
        </button>
      </div>
      <div class="body">
        <h2 class="card-inside-title">
          Filter
        </h2>
        <form id="monitoring-filter-form">
          <div class="row clearfix">
            <div class="col-sm-12 button-demo">
              <button type="button" class="btn btn-primary waves-effect filter-produk" data-unit-pic-id="all">Semua</button>
              <?php foreach ($unitPICList as $unitPIC) {  ?>
                <button type="button" class="btn btn-default waves-effect dropdown-toggle filter-produk" data-unit-pic-id="<?= $unitPIC->id ?>"><?= $unitPIC->name ?></button>
              <?php  } ?>
            </div>
          </div>
          <input name="unitPICId" type="text" value="all" hidden>
          <div class="row clearfix m-t-10">
            <div class="col-sm-4">
              <div class="form-group form-float">
                <div class="form-line">
                  <select name="kl_pemrakarsa_id" class="form-control show-tick capitalise" title="Pilih K/L Pemrakarsa">
                    <option value="all" selected>Semua</option>
                    <?php foreach ($kls as $kl) {  ?>
                      <option value="<?= $kl->id ?>"><?= $kl->nama ?></option>
                    <?php  } ?>
                  </select>
                  <label class="form-label">K/L Pemrakarsa/Pengusul</label>
                </div>
              </div>
            </div>
            <div class="col-sm-4">
              <div class="form-group form-float">
                <div class="form-line">
                  <select name="kl_bidang_id" class="form-control show-tick capitalise" title="Pilih K/L Bidang">
                    <option value="all" selected>Semua</option>
                    <?php foreach ($kls as $kl) {  ?>
                      <option value="<?= $kl->id ?>"><?= $kl->nama ?></option>
                    <?php  } ?>
                  </select>
                  <label class="form-label">K/L Bidang</label>
                </div>
              </div>
            </div>
            <div class="col-sm-4">
              <div class="form-group form-float">
                <div class="form-line">
                  <select name="jenis_rapat_id" class="form-control show-tick capitalise" title="Pilih Status">
                    <option value="all" selected>Semua</option>
                    <?php foreach ($jenis_rapats as $jenis_rapat) {  ?>
                      <option value="<?= $jenis_rapat->id ?>"><?= $jenis_rapat->nama ?></option>
                    <?php  } ?>
                  </select>
                  <label class="form-label">Status</label>
                </div>
              </div>
            </div>
          </div>
        </form>
        <div style=" border-bottom: 1px solid rgb(237 237 237); margin-bottom: 30px; margin-top: -30px; "></div>
        <div class="panel-group" id="monitoring-list" role="tablist" aria-multiselectable="true">
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Monitoring Modal -->
<div class="modal fade" id="monitoring-modal" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Tambah Monitoring</h4>
      </div>
      <form id="monitoring-form">
        <div class="modal-body">
          <br>
          <input name="id" type="text" hidden>
          <div class="row clearfix">
            <div class="col-sm-12">
              <div class="form-group form-float">
                <div class="form-line">
                  <select name="unitPICId" class="form-control show-tick capitalise" title="Pilih Unit PIC">
                    <?php foreach ($unitPICList as $unitPIC) {  ?>
                      <option value="<?= $unitPIC->id ?>"><?= $unitPIC->name ?></option>
                    <?php  } ?>
                  </select>
                  <label class="form-label">Unit PIC</label>
                </div>
              </div>
            </div>
          </div>
          <div class="row clearfix">
            <div class="col-sm-12">
              <div class="form-group form-float">
                <div class="form-line">
                  <select name="jenisProdukId" class="form-control show-tick capitalise" title="Pilih Jenis Produk">
                    <?php foreach ($jenisProdukList as $jenisProduk) {  ?>
                      <option value="<?= $jenisProduk->id ?>"><?= $jenisProduk->name ?></option>
                    <?php  } ?>
                  </select>
                  <label class="form-label">Jenis Produk</label>
                </div>
              </div>
            </div>
          </div>
          <div class="row clearfix">
            <div class="col-sm-12">
              <div class="form-group form-float">
                <div class="form-line">
                  <select name="kl_pemrakarsa_id" class="form-control show-tick capitalise" title="Pilih K/L Pemrakarsa">
                    <?php foreach ($kls as $kl) {  ?>
                      <option value="<?= $kl->id ?>"><?= $kl->nama ?></option>
                    <?php  } ?>
                  </select>
                  <label class="form-label">K/L Pemrakarsa/Pengusul</label>
                </div>
              </div>
            </div>
          </div>
          <div class="row clearfix">
            <div class="col-sm-12">
              <div class="form-group form-float">
                <div class="form-line">
                  <select name="kl_bidang_id" class="form-control show-tick capitalise" title="Pilih K/L Bidang">
                    <?php foreach ($kls as $kl) {  ?>
                      <option value="<?= $kl->id ?>"><?= $kl->nama ?></option>
                    <?php  } ?>
                  </select>
                  <label class="form-label">K/L Bidang</label>
                </div>
              </div>
            </div>
          </div>
          <div class="row clearfix">
            <div class="col-sm-12">
              <div class="form-group form-float">
                <div class="form-line">
                  <input name="judul" type="text" class="form-control">
                  <label class="form-label">Judul</label>
                </div>
              </div>
            </div>
          </div>
          <div class="row clearfix">
            <div class="col-sm-12">
              <div class="form-group form-float">
                <div class="form-line">
                  <textarea rows="1" class="form-control no-resize auto-growth" name="isu"></textarea>
                  <label class="form-label">Isu Krusial</label>
                </div>
              </div>
            </div>
          </div>
          <div class="row clearfix">
            <div class="col-sm-12">
              <div class="form-group form-float">
                <div class="form-line">
                  <textarea rows="1" class="form-control no-resize auto-growth" name="pending_issue"></textarea>
                  <label class="form-label">Pending Issue</label>
                </div>
              </div>
            </div>
          </div>
          <div class="row clearfix">
            <div class="col-sm-12">
              <div class="form-group form-float">
                <div class="form-line">
                  <textarea rows="1" class="form-control no-resize auto-growth" name="pendapat_rekomendasi"></textarea>
                  <label class="form-label">Pendapat/Rekomendasi</label>
                </div>
              </div>
            </div>
          </div>
          <div class="row clearfix">
            <div class="col-sm-12">
              <div class="form-group form-float">
                <div class="form-line">
                  <textarea rows="1" class="form-control no-resize auto-growth" name="catatan_direktur"></textarea>
                  <label class="form-label">Catatan Direktur</label>
                </div>
              </div>
            </div>
          </div>
          <div class="row clearfix">
            <div class="col-sm-12">
              <div class="form-group form-float">
                <div class="form-line">
                  <textarea rows="1" class="form-control no-resize auto-growth" name="catatan"></textarea>
                  <label class="form-label">Catatan Tambahan</label>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn bg-teal waves-effect" id="monitoring-form-save">SIMPAN</button>
        </div>
      </form>
    </div>
  </div>
</div>

<!-- imagesloaded -->
<script src="<?= base_url(); ?>assets/plugins/imagesloaded/imagesloaded.pkgd.min.js"></script>
<!-- Masonry -->
<script src="<?= base_url(); ?>assets/plugins/masonry/masonry.pkgd.min.js"></script>
<!-- bootstrapValidator -->
<script type="text/javascript" src="<?= base_url(); ?>assets/plugins/bootstrapvalidator/js/bootstrapValidator.js">
</script>
<script type="text/javascript" src="<?= base_url(); ?>assets/plugins/bootstrapvalidator/js/language/id_ID.js"></script>
<!-- jquery.serializeObject -->
<script type="text/javascript" src="<?= base_url(); ?>assets/plugins/jQuery.serializeObject/jquery.serializeObject.min.js"></script>
<!-- Bootstrap Select Css -->
<script type="text/javascript" src="<?= base_url(); ?>assets/plugins/bootstrap-select/js/bootstrap-select.min.js">
</script>
<!-- Slimscroll Plugin Js -->
<script src="<?= base_url(); ?>assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- JQuery Steps Plugin Js -->
<script src="<?= base_url(); ?>assets/plugins/jquery-steps/jquery.steps.js"></script>
<!-- Bootstrap Notify Plugin Js -->
<script src="<?= base_url(); ?>assets/plugins/bootstrap-notify/bootstrap-notify.js"></script>
<!-- Waves Effect Plugin Js -->
<script type="text/javascript" src="<?= base_url(); ?>assets/plugins/node-waves/waves.js"></script>
<!-- Bootstrap Datepicker Plugin Js -->
<script src="<?= base_url(); ?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<!-- Dropify -->
<script src="<?= base_url(); ?>assets/plugins/dropify/js/dropify.min.js"></script>
<!-- EasyTimer.js -->
<script src="<?= base_url(); ?>assets/plugins/easytimer/easytimer.min.js"></script>
<!-- jQuery LoadingOverlay -->
<script src="<?= base_url(); ?>assets/plugins/gasparesganga-jquery-loading-overlay/loadingoverlay.min.js"></script>
<!-- Wait Me Plugin Js -->
<script src="<?= base_url(); ?>assets/plugins/waitme/waitMe.js"></script>
<!-- Autosize Plugin Js -->
<script src="<?= base_url(); ?>assets/plugins/autosize/autosize.js"></script>
<!-- AdminBSB Custom Js -->
<script src="<?= base_url(); ?>assets/plugins/adminbsb/admin.js"></script>
<!-- App Js -->
<script src="<?= base_url(); ?>assets/plugins/milea/js/app.js"></script>

<script type="text/javascript">
  const
    NIP = "<?= $this->session->userdata('nip') ?>",
    API_SERVER = "<?= $this->config->item('api_server') ?>";

  let $monitoringForm = $('#monitoring-form')
  let $monitoringModal = $('#monitoring-modal')
  let $monitoringList = $('#monitoring-list')
  let $monitoringFilterForm = $('#monitoring-filter-form')
  let readMonitoringReq

  $(function() {

    /* MONITORING FORM */
    $monitoringForm
      /* VALIDATOR */
      .bootstrapValidator({
        excluded: [':disabled', ':hidden'],
        feedbackIcons: {
          valid: 'glyphicon glyphicon-ok',
          invalid: 'glyphicon glyphicon-remove',
          validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
          judul: {
            validators: {
              notEmpty: {}
            }
          },
          unitPICId: {
            validators: {
              notEmpty: {}
            }
          },
          jenisProdukId: {
            validators: {
              notEmpty: {}
            }
          },
          kl_pemrakarsa_id: {
            validators: {
              notEmpty: {}
            }
          },
          kl_bidang_id: {
            validators: {
              notEmpty: {}
            }
          },
        }
      })
      .on('success.form.bv', function(e) {
        e.preventDefault();
        $monitoringModal.modal('toggle');

        /*CREATE & UPDATE*/
        var data = $monitoringForm.serializeObject();
        let type
        if ($monitoringForm.find('[type="submit"]').attr('data-action') == 'edit') {
          data.updatedby = NIP
          type = 'PUT'
        } else {
          data.createdby = NIP
          type = 'POST'
        }

        $.ajax({
          url: API_SERVER + "dapur/monitoring",
          data: data,
          type: type,
          dataType: 'json',
          beforeSend: function() {
            showLoading($monitoringList)
          },
          complete: function() {
            hideLoading($monitoringList)
          },
          success: function(result) {
            readMonitoring();
            notification('bg-green', `BERHASIL - ${result}`)
          },
          error: function(result) {
            notification('bg-orange', `GAGAL - ${result.responseJSON || result.statusText}`)
          }
        });

      })

    /* MONITORING MODAL */
    $monitoringModal.on('shown.bs.modal', function(event) {
      var action = $(event.relatedTarget).data('action')
      $monitoringForm.find('[type="submit"]').attr('data-action', action)
      $monitoringForm.bootstrapValidator('resetForm', true)

      if (action == 'edit') {
        $monitoringForm.find('h4').text('Ubah Monitoring')

        let monitoring = JSON.parse(decodeURIComponent($(event.relatedTarget).data('monitoring')));

        $monitoringForm.find('[name="id"]').val(monitoring.id)
        $monitoringForm.find('[name="unitPICId"]').selectpicker('val', monitoring.dapur_unit_id)
        $monitoringForm.find('[name="jenisProdukId"]').selectpicker('val', monitoring.dapur_jenisproduk_id)
        $monitoringForm.find('[name="kl_pemrakarsa_id"]').selectpicker('val', monitoring.kl_pemrakarsa_id)
        $monitoringForm.find('[name="kl_bidang_id"]').selectpicker('val', monitoring.kl_bidang_id)
        $monitoringForm.find('[name="judul"]').val(monitoring.judul).closest('.form-line').addClass('focused')
        $monitoringForm.find('[name="isu"]').val(monitoring.isu).closest('.form-line').addClass('focused')
        $monitoringForm.find('[name="pending_issue"]').val(monitoring.pending_issue).closest('.form-line').addClass('focused')
        $monitoringForm.find('[name="pendapat_rekomendasi"]').val(monitoring.pendapat_rekomendasi).closest('.form-line').addClass('focused')
        $monitoringForm.find('[name="catatan_direktur"]').val(monitoring.catatan_direktur).closest('.form-line').addClass('focused')
        $monitoringForm.find('[name="catatan"]').val(monitoring.catatan).closest('.form-line').addClass('focused')
      } else {
        $monitoringForm.find('h4').text('Tambah Monitoring');

        $monitoringForm.find('[name="id"]').val('')
        $monitoringForm.find('[name="unitPICId"]').selectpicker('val', '')
        $monitoringForm.find('[name="jenisProdukId"]').selectpicker('val', '')
        $monitoringForm.find('[name="kl_pemrakarsa_id"]').selectpicker('val', '')
        $monitoringForm.find('[name="kl_bidang_id"]').selectpicker('val', '')
        $monitoringForm.find('[name="judul"]').val('').closest('.form-line').removeClass('focused')
        $monitoringForm.find('[name="isu"]').val('').closest('.form-line').removeClass('focused')
        $monitoringForm.find('[name="pending_issue"]').val('').closest('.form-line').removeClass('focused')
        $monitoringForm.find('[name="pendapat_rekomendasi"]').val('').closest('.form-line').removeClass('focused')
        $monitoringForm.find('[name="catatan_direktur"]').val('').closest('.form-line').removeClass('focused')
        $monitoringForm.find('[name="catatan"]').val('').closest('.form-line').removeClass('focused')
      }
    })

    /* DELETE MONITORING */
    $monitoringList.on('click', '.delete', function() {
      Swal.fire({
        title: 'Hapus Monitoring?',
        type: 'warning',
        confirmButtonText: 'Ya',
        showCancelButton: true,
        cancelButtonText: 'Tidak',
        focusCancel: true,
        reverseButtons: true,
        showLoaderOnConfirm: true,
        preConfirm: () => {}
      }).then((result) => {
        if (result.value) {
          $.ajax({
            url: API_SERVER + "dapur/monitoring",
            data: {
              id: $(this).attr('data-id')
            },
            type: 'DELETE',
            dataType: 'json',
            beforeSend: function() {
              showLoading($monitoringList)
            },
            complete: function() {
              hideLoading($monitoringList)
            },
            success: function(result) {
              readMonitoring();
              notification('bg-green', `BERHASIL - ${result}`)
            },
            error: function(result) {
              notification('bg-orange', `GAGAL - ${result.responseJSON || result.statusText}`)
            }
          });
        }
      })
    });

    /* MONITORING FILTER */
    $monitoringFilterForm.on('click', '.filter-produk', function(e) {
      e.preventDefault();
      $monitoringFilterForm.find('.filter-produk').removeClass('btn-primary')
      $monitoringFilterForm.find('.filter-produk').addClass('btn-default')
      $(this).removeClass('btn-default')
      $(this).addClass('btn-primary')
      $monitoringFilterForm.find('[name="unitPICId"]').val($(this).attr('data-unit-pic-id'))

      readMonitoring()
    })
    $monitoringFilterForm.on('change', 'select', function(e) {
      readMonitoring()
    })


    readMonitoring()

  })
</script>

<script type="text/javascript">
  function readMonitoring() {
    readMonitoringReq = $.ajax({
      url: `${API_SERVER}dapur/monitoring?userNip=<?= $this->session->userdata('nip') ?>&${$monitoringFilterForm.serialize()}`,
      type: 'GET',
      dataType: 'json',
      beforeSend: function() {
        showLoading($monitoringList)
        if (readMonitoringReq) {
          readMonitoringReq.abort();
        }
      },
      complete: function() {
        hideLoading($monitoringList)
      },
      success: function(result) {
        let html = ''
        if (result.length == 0) {
          html = '<p class="font-italic col-grey">Tidak ada data yang ditampilkan</p>';
        } else {
          html = result.map(r => {
              let creatorAbilityHtml = ''
              if (r.createdby == NIP) {
                creatorAbilityHtml = `
                  <button type="button" class="btn bg-orange btn-xs waves-effect" data-toggle="modal" data-target="#monitoring-modal" data-action="edit" data-monitoring="${encodeURIComponent(JSON.stringify(r))}">Ubah</button>
                  <button type="button" class="btn bg-red btn-xs waves-effect delete" data-id="${r.id}">Hapus</button>`
              }
              let out = `
                <div class="panel" style="border: 1px solid rgb(221, 221, 221);">
                  <div class="panel-heading" role="tab" id="monitoring-${r.id}">
                    <div class="panel-title">
                      <a role="button" data-toggle="collapse" data-parent="#monitoring-list" href="#monitoring-${r.id}-detail" aria-expanded="false" class="collapsed">
                        <div class="row clearfix">
                          <div class="col-sm-6">
                            <div class="font-12 col-grey">
                              Judul
                            </div>
                            <div class="font-14">
                              ${r.judul}
                            </div>
                          </div>
                          <div class="col-sm-2">
                            <div class="font-12 col-grey">
                              Produk
                            </div>
                            <div class="badge bg-${r.jenisProdukCol} font-12">${r.jenisProdukName}</div>
                          </div>
                          <div class="col-sm-2">
                            <div class="font-12 col-grey">
                              K/L Bidang
                            </div>
                            <div class="font-12">${r.klbidang}</div>
                          </div>
                          <div class="col-sm-2">
                            <div class="font-12 col-grey">
                              Status
                            </div>
                            <div class="badge bg-${r.status.col} font-12">${r.status.text}</div>
                          </div>
                        </div>
                      </a>
                    </div>
                  </div>
                  <div id="monitoring-${r.id}-detail" class="panel-collapse collapse" role="tabpanel" aria-expanded="false">
                    <div class="panel-body">
                      <b>Unit PIC</b>
                      <p>${r.esl3}</p>
                      <b>K/L Pemrakarsa/Pengusul</b>
                      <p>${r.klpemrakarsa}</p>
                      <b>Isu</b>
                      <p>${r.isu}</p>
                      <b>Pending Issue</b>
                      <p>${r.pending_issue}</p>
                      <b>Pendapat/Rekomendasi</b>
                      <p>${r.pendapat_rekomendasi}</p>
                      <b>Catatan Direktur</b>
                      <p>${r.catatan_direktur}</p>
                      <b>Catatan Tambahan</b>
                      <p>${r.catatan}</p>
                      <div class="align-right p-t-15" style="border-top: 1px solid rgb(221, 221, 221);">
                        <a href="/dapur/kronologis/${r.id}" type="button" class="btn bg-primary btn-xs waves-effect">Kronologis</a>
                        ${creatorAbilityHtml}
                      </div>
                    </div>
                  </div>
                </div>
                `
              return out
            })
            .join('\n')
        }

        $monitoringList.html(html)
      },
      error: function(result) {
        notification('bg-orange', `GAGAL - ${result.responseJSON || result.statusText}`)
      }
    });

  }
</script>