<!-- Google Fonts -->
<link href="https://fonts.googleapis.com/css?family=Roboto:400,700&amp;subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
<link href="<?= base_url(); ?>assets/plugins/materialicons/materialicons.css" rel="stylesheet" type="text/css">
<!-- bootstrapValidator -->
<link rel="stylesheet" href="<?= base_url(); ?>assets/plugins/bootstrapvalidator/css/bootstrapValidator.min.css" />
<!-- SweetAlert2 -->
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
<!-- Waves Effect Css -->
<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/plugins/node-waves/waves.css">
<!-- Animation Css -->
<link href="<?= base_url(); ?>assets/plugins/animate-css/animate.css" rel="stylesheet" />
<!-- Bootstrap DatePicker Css -->
<link href="<?= base_url(); ?>assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.css" rel="stylesheet" />
<!-- Bootstrap Select Css -->
<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/plugins/bootstrap-select/css/bootstrap-select.css">
<!-- Dropify -->
<link href="<?= base_url(); ?>assets/plugins/dropify/css/dropify.min.css" rel="stylesheet">
<!--WaitMe Css-->
<link href="<?= base_url(); ?>assets/plugins/waitme/waitMe.css" rel="stylesheet" />
<!-- AdminBSB Custom Css -->
<link href="<?= base_url(); ?>assets/plugins/adminbsb/style.css" rel="stylesheet">

<style type="text/css">
  form .dropify-wrapper p {
    text-align: center;
  }

  .swal2-radio [type="radio"]:not(:checked),
  .swal2-radio [type="radio"]:checked {
    position: unset;
    opacity: 1;
  }

  .form-group .form-line.focused .form-label {
    top: -15px;
  }

  .bs-searchbox .form-control {
    width: 88%;
  }

  .capitalise {
    text-transform: capitalize;
  }

  .bootstrap-select .dropdown-menu {
    width: 100%;
  }

  .bootstrap-select .dropdown-menu li small {
    white-space: normal;
  }
</style>

<div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="card">
      <div class="header">
        <h2 id="monitoring-judul">
          PRODUK HUKUM DAN KEBIJAKAN
        </h2>
        <button type="button" class="btn btn-primary waves-effect" data-toggle="modal" data-target="#produk-modal" data-action="add" style=" position: absolute; top: 10px; right: 20px; ">
          <i class="material-icons">add</i>
        </button>
      </div>
      <div class="body">
        <h2 class="card-inside-title">
          Filter
        </h2>
        <form id="produk-filter-form">
          <div class="row clearfix">
            <div class="col-sm-12 button-demo">
              <button type="button" class="btn btn-primary waves-effect filter-produk" data-unit-pic-id="all">Semua</button>
              <?php foreach ($unitPICList as $unitPIC) {  ?>
                <button type="button" class="btn btn-default waves-effect dropdown-toggle filter-produk" data-unit-pic-id="<?= $unitPIC->id ?>"><?= $unitPIC->name ?></button>
              <?php  } ?>
            </div>
          </div>
          <input name="unitPICId" type="text" value="all" hidden>
          <div class="row clearfix m-t-10">
            <div class="col-sm-6">
              <div class="form-group form-float">
                <div class="form-line">
                  <select name="kl_pemrakarsa_id" class="form-control show-tick capitalise" title="Pilih K/L Pemrakarsa">
                    <option value="all" selected>Semua</option>
                    <?php foreach ($kls as $kl) {  ?>
                      <option value="<?= $kl->id ?>"><?= $kl->nama ?></option>
                    <?php  } ?>
                  </select>
                  <label class="form-label">K/L Pemrakarsa</label>
                </div>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="form-group form-float">
                <div class="form-line">
                  <select name="kl_bidang_id" class="form-control show-tick capitalise" title="Pilih K/L Bidang">
                    <option value="all" selected>Semua</option>
                    <?php foreach ($kls as $kl) {  ?>
                      <option value="<?= $kl->id ?>"><?= $kl->nama ?></option>
                    <?php  } ?>
                  </select>
                  <label class="form-label">K/L Bidang</label>
                </div>
              </div>
            </div>
          </div>
        </form>
        <div style=" border-bottom: 1px solid rgb(237 237 237); margin-bottom: 30px; margin-top: -30px; "></div>
        <div class="panel-group m-t-10" id="produk-list" role="tablist" aria-multiselectable="true">
        </div>
      </div>
    </div>
  </div>
</div>

<!-- PRODUK MODAL -->
<div class="modal fade" id="produk-modal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Tambah Produk</h4>
      </div>
      <form id="produk-form">
        <div class="modal-body">
          <br>
          <input name="id" type="text" hidden>
          <div class="row clearfix">
            <div class="col-sm-12">
              <div class="form-group form-float">
                <div class="form-line">
                  <select name="unitPICId" class="form-control show-tick capitalise" title="Pilih Unit PIC">
                    <?php foreach ($unitPICList as $unitPIC) {  ?>
                      <option value="<?= $unitPIC->id ?>"><?= $unitPIC->name ?></option>
                    <?php  } ?>
                  </select>
                  <label class="form-label">Unit PIC</label>
                </div>
              </div>
            </div>
          </div>
          <div class="row clearfix">
            <div class="col-sm-12">
              <div class="form-group form-float">
                <div class="form-line">
                  <select name="jenisProdukId" class="form-control show-tick capitalise" title="Pilih Jenis Produk">
                    <?php foreach ($jenisProdukList as $jenisProduk) {  ?>
                      <option value="<?= $jenisProduk->id ?>"><?= $jenisProduk->name ?></option>
                    <?php  } ?>
                  </select>
                  <label class="form-label">Jenis Produk</label>
                </div>
              </div>
            </div>
          </div>
          <div class="row clearfix">
            <div class="col-sm-12">
              <div class="form-group form-float">
                <div class="form-line">
                  <select name="kl_pemrakarsa_id" class="form-control show-tick capitalise" title="Pilih K/L Pemrakarsa">
                    <?php foreach ($kls as $kl) {  ?>
                      <option value="<?= $kl->id ?>"><?= $kl->nama ?></option>
                    <?php  } ?>
                  </select>
                  <label class="form-label">K/L Pemrakarsa</label>
                </div>
              </div>
            </div>
          </div>
          <div class="row clearfix">
            <div class="col-sm-12">
              <div class="form-group form-float">
                <div class="form-line">
                  <select name="kl_bidang_id" class="form-control show-tick capitalise" title="Pilih K/L Bidang">
                    <?php foreach ($kls as $kl) {  ?>
                      <option value="<?= $kl->id ?>"><?= $kl->nama ?></option>
                    <?php  } ?>
                  </select>
                  <label class="form-label">K/L Bidang</label>
                </div>
              </div>
            </div>
          </div>
          <div class="row clearfix">
            <div class="col-sm-12">
              <div class="form-group form-float">
                <div class="form-line">
                  <input name="judul" type="text" class="form-control datepicker" autocomplete="off">
                  <label class="form-label">Judul</label>
                </div>
              </div>
            </div>
          </div>
          <div class="row clearfix">
            <div class="col-sm-12">
              <div class="form-group form-float">
                <div class="form-line">
                  <textarea rows="1" class="form-control no-resize auto-growth" name="deskripsi"></textarea>
                  <label class="form-label">Deskripsi</label>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn bg-teal waves-effect">SIMPAN</button>
        </div>
      </form>
    </div>
  </div>
</div>

<!-- LAMPIRAN MODAL -->
<div class="modal fade" id="lampiran-modal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Upload Lampiran</h4>
      </div>
      <form id="lampiran-form">
        <div class="modal-body">
          <br>
          <input name="id" type="text" hidden>
          <input name="produkId" type="text" hidden>
          <input name="path" type="text" hidden>
          <div class="row clearfix">
            <div class="col-sm-12">
              <div class="form-group form-float">
                <div class="form-line">
                  <input name="nama" type="text" class="form-control">
                  <label class="form-label">Nama Lampiran</label>
                </div>
              </div>
            </div>
          </div>
          <div class="row clearfix">
            <div class="col-sm-12">
              <div class="form-group form-float">
                <div class="form-line">
                  <input name="doc" type="file" class="dropify" />
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn bg-teal waves-effect">SIMPAN</button>
        </div>
      </form>
    </div>
  </div>
</div>

<!-- imagesloaded -->
<script src="<?= base_url(); ?>assets/plugins/imagesloaded/imagesloaded.pkgd.min.js"></script>
<!-- Masonry -->
<script src="<?= base_url(); ?>assets/plugins/masonry/masonry.pkgd.min.js"></script>
<!-- bootstrapValidator -->
<script type="text/javascript" src="<?= base_url(); ?>assets/plugins/bootstrapvalidator/js/bootstrapValidator.js">
</script>
<script type="text/javascript" src="<?= base_url(); ?>assets/plugins/bootstrapvalidator/js/language/id_ID.js"></script>
<!-- jquery.serializeObject -->
<script type="text/javascript" src="<?= base_url(); ?>assets/plugins/jQuery.serializeObject/jquery.serializeObject.min.js"></script>
<!-- Bootstrap Select Css -->
<script type="text/javascript" src="<?= base_url(); ?>assets/plugins/bootstrap-select/js/bootstrap-select.min.js">
</script>
<!-- Slimscroll Plugin Js -->
<script src="<?= base_url(); ?>assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- JQuery Steps Plugin Js -->
<script src="<?= base_url(); ?>assets/plugins/jquery-steps/jquery.steps.js"></script>
<!-- Bootstrap Notify Plugin Js -->
<script src="<?= base_url(); ?>assets/plugins/bootstrap-notify/bootstrap-notify.js"></script>
<!-- Waves Effect Plugin Js -->
<script type="text/javascript" src="<?= base_url(); ?>assets/plugins/node-waves/waves.js"></script>
<!-- Bootstrap Datepicker Plugin Js -->
<script src="<?= base_url(); ?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<!-- Dropify -->
<script src="<?= base_url(); ?>assets/plugins/dropify/js/dropify.min.js"></script>
<!-- EasyTimer.js -->
<script src="<?= base_url(); ?>assets/plugins/easytimer/easytimer.min.js"></script>
<!-- jQuery LoadingOverlay -->
<script src="<?= base_url(); ?>assets/plugins/gasparesganga-jquery-loading-overlay/loadingoverlay.min.js"></script>
<!-- Wait Me Plugin Js -->
<script src="<?= base_url(); ?>assets/plugins/waitme/waitMe.js"></script>
<!-- Autosize Plugin Js -->
<script src="<?= base_url(); ?>assets/plugins/autosize/autosize.js"></script>
<!-- AdminBSB Custom Js -->
<script src="<?= base_url(); ?>assets/plugins/adminbsb/admin.js"></script>
<!-- App Js -->
<script src="<?= base_url(); ?>assets/plugins/milea/js/app.js"></script>

<script type="text/javascript">
  const
    NIP = "<?= $this->session->userdata('nip') ?>",
    API_SERVER = "<?= $this->config->item('api_server') ?>";

  let $produkForm = $('#produk-form')
  let $produkModal = $('#produk-modal')
  let $produkList = $('#produk-list')
  let $lampiranForm = $('#lampiran-form')
  let $lampiranModal = $('#lampiran-modal')
  let $produkFilterForm = $('#produk-filter-form')
  let readProdukReq

  $(function() {


    /* DROPIFY */
    $('.dropify').dropify({
      messages: {
        'default': 'Drag dan drop file/dokumen di sini atau klik',
        'replace': 'Drag dan drop atau klik untuk mengganti',
        'remove': 'Hapus',
        'error': 'Ooops, terjadi kesalahan.'
      }
    })

    /* PRODUK FORM */
    $produkForm
      /* VALIDATOR */
      .bootstrapValidator({
        excluded: [':disabled', ':hidden'],
        feedbackIcons: {
          valid: 'glyphicon glyphicon-ok',
          invalid: 'glyphicon glyphicon-remove',
          validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
          judul: {
            validators: {
              notEmpty: {}
            }
          },
          deskripsi: {
            validators: {
              notEmpty: {}
            }
          },
          unitPICId: {
            validators: {
              notEmpty: {}
            }
          },
          jenisProdukId: {
            validators: {
              notEmpty: {}
            }
          },
          kl_pemrakarsa_id: {
            validators: {
              notEmpty: {}
            }
          },
          kl_bidang_id: {
            validators: {
              notEmpty: {}
            }
          },
        }
      })
      .on('success.form.bv', function(e) {
        e.preventDefault();
        $produkModal.modal('toggle');

        /*CREATE & UPDATE*/
        var data = $produkForm.serializeObject();
        let type
        if ($produkForm.find('[type="submit"]').attr('data-action') == 'edit') {
          data.updatedby = NIP
          type = 'PUT'
        } else {
          data.createdby = NIP
          type = 'POST'
        }

        $.ajax({
          url: `${API_SERVER}dapur/produk`,
          data: data,
          type: type,
          dataType: 'json',
          beforeSend: function() {
            showLoading($produkList)
          },
          complete: function() {
            hideLoading($produkList)
          },
          success: function(result) {
            readProduk();
            notification('bg-green', `BERHASIL - ${result}`)
          },
          error: function(result) {
            notification('bg-orange', `GAGAL - ${result.responseJSON || result.statusText}`)
          }
        });

      })

    /* PRODUK MODAL */
    $produkModal.on('shown.bs.modal', function(event) {
      var action = $(event.relatedTarget).data('action')
      $produkForm.find('[type="submit"]').attr('data-action', action)
      $produkForm.bootstrapValidator('resetForm', true)

      if (action == 'edit') {
        $produkForm.find('h4').text('Ubah Produk')

        let produk = JSON.parse(decodeURIComponent($(event.relatedTarget).data('produk')));

        $produkForm.find('[name="id"]').val(produk.id)
        $produkForm.find('[name="judul"]').val(produk.judul).closest('.form-line').addClass('focused')
        $produkForm.find('[name="deskripsi"]').val(produk.deskripsi).closest('.form-line').addClass('focused')
        $produkForm.find('[name="unitPICId"]').selectpicker('val', produk.dapur_unitpic_id)
        $produkForm.find('[name="jenisProdukId"]').selectpicker('val', produk.dapur_jenisproduk_id)
        $produkForm.find('[name="kl_pemrakarsa_id"]').selectpicker('val', produk.kl_pemrakarsa_id)
        $produkForm.find('[name="kl_bidang_id"]').selectpicker('val', produk.kl_bidang_id)
      } else {
        $produkForm.find('h4').text('Tambah Produk');

        $produkForm.find('[name="id"]').val('')
        $produkForm.find('[name="judul"]').val('').closest('.form-line').removeClass('focused')
        $produkForm.find('[name="deskripsi"]').val('').closest('.form-line').removeClass('focused')
        $produkForm.find('[name="unitPICId"]').selectpicker('val', '')
        $produkForm.find('[name="jenisProdukId"]').selectpicker('val', '')
        $produkForm.find('[name="kl_pemrakarsa_id"]').selectpicker('val', '')
        $produkForm.find('[name="kl_bidang_id"]').selectpicker('val', '')
      }
    })

    /* DELETE PRODUK */
    $produkList.on('click', '.delete', function() {
      Swal.fire({
        title: 'Hapus Produk?',
        type: 'warning',
        confirmButtonText: 'Ya',
        showCancelButton: true,
        cancelButtonText: 'Tidak',
        focusCancel: true,
        reverseButtons: true,
        showLoaderOnConfirm: true,
        preConfirm: () => {}
      }).then((result) => {
        if (result.value) {
          $.ajax({
            url: API_SERVER + "dapur/produk",
            data: {
              id: $(this).attr('data-id')
            },
            type: 'DELETE',
            dataType: 'json',
            beforeSend: function() {
              showLoading($produkList)
            },
            complete: function() {
              hideLoading($produkList)
            },
            success: function(result) {
              readProduk();
              notification('bg-green', `BERHASIL - ${result}`)
            },
            error: function(result) {
              notification('bg-orange', `GAGAL - ${result.responseJSON || result.statusText}`)
            }
          });
        }
      })
    });


    /* LAMPIRAN FORM */
    $lampiranForm
      /* VALIDATOR */
      .bootstrapValidator({
        excluded: [':disabled', ':hidden'],
        feedbackIcons: {
          valid: 'glyphicon glyphicon-ok',
          invalid: 'glyphicon glyphicon-remove',
          validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
          nama: {
            validators: {
              notEmpty: {}
            }
          },
          doc: {
            validators: {
              notEmpty: {},
              file: {
                maxSize: 5 * 1024 * 1024,
                /* 5 MB */
              }
            }
          },
        }
      })
      /* SUBMIT */
      .on('success.form.bv', function(e) {
        e.preventDefault();
        $lampiranModal.modal('toggle');

        /* UPLOAD DOCUMENTS */
        let data = $lampiranForm.serializeObject()
        let produkId = data.produkId
        var formData = new FormData();
        formData.append('file', $lampiranForm.find('[name="doc"]')[0].files[0]);
        formData.append('produkId', produkId);

        $.ajax({
          url: API_SERVER + "dapur/produk/upload",
          data: formData,
          type: 'POST',
          processData: false,
          contentType: false,
          beforeSend: function() {
            showLoading($produkList)
          },
          complete: function() {
            hideLoading($produkList)
          },
          success: function(result) {
            let type
            if ($lampiranForm.find('[type="submit"]').attr('data-action') == 'edit') {
              data.updatedby = NIP
              type = 'PUT'
            } else {
              data.createdby = NIP
              type = 'POST'
            }

            if (result) data.path = result;

            /*CREATE / UPDATE LAMPIRAN */
            $.ajax({
              url: API_SERVER + "dapur/produk/lampiran",
              data: data,
              type: type,
              dataType: 'json',
              beforeSend: function() {
                showLoading($produkList)
              },
              complete: function() {
                hideLoading($produkList)
              },
              success: function(result) {
                readProduk()
              },
              error: function(result) {
                notification('bg-orange', `GAGAL - ${result.responseJSON || result.statusText}`)
              }
            });

          },
          error: function(result) {
            notification('bg-orange', `GAGAL - ${result.responseJSON || result.statusText}`)
          }
        });

      })

    /* LAMPIRAN MODAL */
    $lampiranModal.on('shown.bs.modal', function(event) {
      var action = $(event.relatedTarget).data('action')
      $lampiranForm.find('[type="submit"]').attr('data-action', action)
      $lampiranForm.bootstrapValidator('resetForm', true)

      if (action == 'edit') {
        $lampiranForm.find('h4').text(`Ubah Bukti Dukung`)

        var lampiran = JSON.parse(decodeURIComponent($(event.relatedTarget).data('lampiran')));

        $lampiranForm.find('[name="id"]').val(lampiran.id)
        $lampiranForm.find('[name="produkId"]').val('')
        $lampiranForm.find('[name="nama"]').val(lampiran.nama).closest('.form-line').addClass('focused')
        $lampiranForm.find('[name="path"]').val(lampiran.path)

        var defaultFile = (lampiran.path) ? `${API_SERVER}dapur/monitoring/produk/lampiran/view?id=${lampiran.id}` : ``;
        var drEvent = $lampiranForm.find('[name="doc"]').dropify({
          defaultFile: defaultFile
        });

        drEvent.on('dropify.beforeClear', function(event, element) {
          $lampiranForm.bootstrapValidator('revalidateField', 'doc');
        });

        drEvent = drEvent.data('dropify');
        drEvent.resetPreview();
        drEvent.clearElement();
        drEvent.settings.defaultFile = defaultFile;
        drEvent.destroy();
        drEvent.init();

        if (lampiran.path) $lampiranForm.bootstrapValidator('updateStatus', 'doc', 'VALID');

      } else {
        $lampiranForm.find('h4').text(`Tambah Lampiran`);

        var produkId = $(event.relatedTarget).data('produk-id')

        $lampiranForm.find('[name="id"]').val('')
        $lampiranForm.find('[name="produkId"]').val(produkId)
        $lampiranForm.find('[name="nama"]').val('').closest('.form-line').removeClass('focused')
        $lampiranForm.find('[name="path"]').val('')


        var drEvent = $lampiranForm.find('[name="doc"]').dropify();
        drEvent = drEvent.data('dropify');
        drEvent.resetPreview();
        drEvent.clearElement();

        $lampiranForm.bootstrapValidator('updateStatus', 'doc', 'NOT_VALIDATED');
      }
    })

    /* DELETE LAMPIRAN */
    $produkList.on('click', '.delete-lampiran', function() {
      Swal.fire({
        title: 'Hapus Lampiran?',
        type: 'warning',
        confirmButtonText: 'Ya',
        showCancelButton: true,
        cancelButtonText: 'Tidak',
        focusCancel: true,
        reverseButtons: true,
        showLoaderOnConfirm: true,
        preConfirm: () => {}
      }).then((result) => {
        if (result.value) {
          $.ajax({
            url: API_SERVER + "dapur/produk/lampiran",
            data: {
              id: $(this).attr('data-id')
            },
            type: 'DELETE',
            dataType: 'json',
            beforeSend: function() {
              showLoading($produkList)
            },
            complete: function() {
              hideLoading($produkList)
            },
            success: function(result) {
              readProduk();
              notification('bg-green', `BERHASIL - ${result}`)
            },
            error: function(result) {
              notification('bg-orange', `GAGAL - ${result.responseJSON || result.statusText}`)
            }
          });
        }
      })
    });


    /* PRODUK FILTER */
    $produkFilterForm.on('click', '.filter-produk', function(e) {
      e.preventDefault();
      $produkFilterForm.find('.filter-produk').removeClass('btn-primary')
      $produkFilterForm.find('.filter-produk').addClass('btn-default')
      $(this).removeClass('btn-default')
      $(this).addClass('btn-primary')
      $produkFilterForm.find('[name="unitPICId"]').val($(this).attr('data-unit-pic-id'))

      readProduk()
    })
    $produkFilterForm.on('change', 'select', function(e) {
      readProduk()
    })

    readProduk()

  })
</script>

<script type="text/javascript">
  function readProduk() {
    readProdukReq = $.ajax({
      url: `${API_SERVER}dapur/produk?userNip=<?= $this->session->userdata('nip') ?>&${$produkFilterForm.serialize()}`,
      type: 'GET',
      dataType: 'json',
      beforeSend: function() {
        if (readProdukReq) {
          readProdukReq.abort();
        }
      },
      complete: function() {
        hideLoading($produkList)
      },
      success: function(result) {
        let html = ''
        if (result.length == 0) {
          html = '<p class="font-italic col-grey">Tidak ada data yang ditampilkan</p>';
        } else {
          html = result.map((r, index) => {
              let lampiranHtml
              if (r.lampiran.length == 0) {
                lampiranHtml = '<p class="font-italic col-grey m-t-10 font-12">Tidak ada data yang ditampilkan</p>';
              } else {
                lampiranHtml = r.lampiran.map((bd, bdIdx) => {
                    let lampiranCreatorAbilityHtml = ''
                    if (r.createdby == NIP) {
                      lampiranCreatorAbilityHtml = `
                        <button type="button" class="btn bg-orange btn-xs waves-effect" data-toggle="modal" data-target="#lampiran-modal" data-action="edit" data-lampiran="${encodeURIComponent(JSON.stringify(bd))}">Ubah</button>
                        <button type="button" class="btn bg-red btn-xs waves-effect delete-lampiran" data-id="${bd.id}">Hapus</button>
                      `
                    }
                    let out = `
                    <tr>
                      <td><a href="${API_SERVER}dapur/produk/lampiran/view?id=${bd.id}" target="_blank">${bd.nama}</a></td>
                      <td>
                        <div class="pull-right">
                          ${lampiranCreatorAbilityHtml}
                        </div>
                      </td>
                    </tr>`
                    return out
                  })
                  .join('\n')

                lampiranHtml = `
                <table class="table table-hover m-t-10 font-12">
                  <tbody>
                    ${lampiranHtml}
                  </tbody>
                </table>`
              }

              let creatorAbilityHtml = ''
              if (r.createdby == NIP) {
                creatorAbilityHtml = `
                  <button type="button" class="btn bg-orange btn-xs waves-effect" data-toggle="modal" data-target="#produk-modal" data-action="edit" data-produk="${encodeURIComponent(JSON.stringify(r))}">Ubah</button>
                  <button type="button" class="btn bg-red btn-xs waves-effect delete" data-id="${r.id}">Hapus</button>
                  `
              }
              let out = `
                <div class="panel" style="border: 1px solid rgb(221, 221, 221);">
                  <div class="panel-heading" role="tab" id="produk-${r.id}">
                    <div class="panel-title">
                      <div class="row clearfix">
                        <a role="button" data-toggle="collapse" data-parent="#produk-list" href="#produk-${r.id}-detail" aria-expanded="false" class="collapsed">
                          <div class="col-sm-6">
                            <div class="font-12 col-grey">
                              Judul
                            </div>
                            <div class="font-14">
                              ${r.judul}
                            </div>
                          </div>
                          <div class="col-sm-2">
                            <div class="font-12 col-grey">
                              Produk
                            </div>
                            <div class="badge bg-${r.jenisProdukCol} font-12">${r.jenisProdukName}</div>
                          </div>
                          <div class="col-sm-2">
                            <div class="font-12 col-grey">
                              K/L Bidang
                            </div>
                            <div class="font-12">${r.klbidang}</div>
                          </div>
                        </a>
                        <div class="col-sm-2">
                          <div class="font-12 pull-right">
                            ${creatorAbilityHtml}
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div id="produk-${r.id}-detail" class="panel-collapse collapse" role="tabpanel" aria-expanded="false">
                    <div class="panel-body">
                      <b>Unit PIC</b>
                      <p>${r.esl3}</p>
                      <b>Deskripsi</b>
                      <p>${r.deskripsi}</p>
                      <b>K/L Pemrakarsa</b>
                      <p>${r.klpemrakarsa}</p>
                      <p class="font-bold">Lampiran</p>
                      <button type="button" class="btn bg-primary btn-xs waves-effect" data-toggle="modal" data-target="#lampiran-modal" data-action="add" data-produk-id="${r.id}">Upload</button>
                      ${lampiranHtml}
                    </div>
                  </div>
                </div>
                `
              return out
            })
            .join('\n')
        }

        $produkList.html(html)
      },
      error: function(result) {
        notification('bg-orange', `GAGAL - ${result.responseJSON || result.statusText}`)
      }
    });

  }
</script>