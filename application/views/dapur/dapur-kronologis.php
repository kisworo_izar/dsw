<!-- Google Fonts -->
<link href="https://fonts.googleapis.com/css?family=Roboto:400,700&amp;subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
<link href="<?= base_url(); ?>assets/plugins/materialicons/materialicons.css" rel="stylesheet" type="text/css">
<!-- bootstrapValidator -->
<link rel="stylesheet" href="<?= base_url(); ?>assets/plugins/bootstrapvalidator/css/bootstrapValidator.min.css" />
<!-- SweetAlert2 -->
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
<!-- Waves Effect Css -->
<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/plugins/node-waves/waves.css">
<!-- Animation Css -->
<link href="<?= base_url(); ?>assets/plugins/animate-css/animate.css" rel="stylesheet" />
<!-- Bootstrap DatePicker Css -->
<link href="<?= base_url(); ?>assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.css" rel="stylesheet" />
<!-- Bootstrap Select Css -->
<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/plugins/bootstrap-select/css/bootstrap-select.css">
<!-- Dropify -->
<link href="<?= base_url(); ?>assets/plugins/dropify/css/dropify.min.css" rel="stylesheet">
<!--WaitMe Css-->
<link href="<?= base_url(); ?>assets/plugins/waitme/waitMe.css" rel="stylesheet" />
<!-- AdminBSB Custom Css -->
<link href="<?= base_url(); ?>assets/plugins/adminbsb/style.css" rel="stylesheet">

<style type="text/css">
  form .dropify-wrapper p {
    text-align: center;
  }

  .swal2-radio [type="radio"]:not(:checked),
  .swal2-radio [type="radio"]:checked {
    position: unset;
    opacity: 1;
  }

  .form-group .form-line.focused .form-label {
    top: -15px;
  }

  .bs-searchbox .form-control {
    width: 88%;
  }

  .capitalise {
    text-transform: capitalize;
  }

  .bootstrap-select .dropdown-menu {
    width: 100%;
  }

  .bootstrap-select .dropdown-menu li small {
    white-space: normal;
  }
</style>

<div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="card">
      <div class="header">
        <h2 id="monitoring-judul">
          Judul
        </h2>
      </div>
      <div class="body">
        <p class="font-bold">KRONOLOGIS</p>
        <button type="button" class="btn bg-primary btn-xs waves-effect" data-toggle="modal" data-target="#kronologis-modal" data-action="add">Tambah</button>
        <div class="panel-group m-t-10" id="kronologis-list" role="tablist" aria-multiselectable="true">
        </div>
      </div>
    </div>
  </div>
</div>

<!-- KRONOLOGIS MODAL -->
<div class="modal fade" id="kronologis-modal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Tambah Kronologis</h4>
      </div>
      <form id="kronologis-form">
        <div class="modal-body">
          <br>
          <input name="id" type="text" hidden>
          <div class="row clearfix">
            <div class="col-sm-12">
              <div class="form-group form-float">
                <div class="form-line">
                  <select name="jenis_rapat_id" class="form-control show-tick capitalise" title="Pilih Tahapan">
                    <?php foreach ($jenisRapatList as $jenisRapat) {  ?>
                      <option value="<?= $jenisRapat->id ?>"><?= $jenisRapat->nama ?></option>
                    <?php  } ?>
                  </select>
                  <label class="form-label">Tahapan</label>
                </div>
              </div>
            </div>
          </div>
          <div class="row clearfix">
            <div class="col-sm-12">
              <div class="form-group form-float">
                <div class="form-line">
                  <input name="tanggal" type="text" class="form-control datepicker" autocomplete="off">
                  <label class="form-label">Tanggal</label>
                </div>
              </div>
            </div>
          </div>
          <div class="row clearfix">
            <div class="col-sm-12">
              <div class="form-group form-float">
                <div class="form-line">
                  <textarea rows="1" class="form-control no-resize auto-growth" name="pembahasan"></textarea>
                  <label class="form-label">Pembahasan</label>
                </div>
              </div>
            </div>
          </div>
          <div class="row clearfix">
            <div class="col-sm-12">
              <div class="form-group form-float">
                <div class="form-line">
                  <textarea rows="1" class="form-control no-resize auto-growth" name="rekomendasi_pendapat"></textarea>
                  <label class="form-label">Rekomendasi/Pendapat</label>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn bg-teal waves-effect">SIMPAN</button>
        </div>
      </form>
    </div>
  </div>
</div>

<!-- KRONOLOGIS UPLOAD MODAL -->
<div class="modal fade" id="buktidukung-modal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Upload Bukti Dukung</h4>
      </div>
      <form id="buktidukung-form">
        <div class="modal-body">
          <br>
          <input name="id" type="text" hidden>
          <input name="kronologisId" type="text" hidden>
          <input name="path" type="text" hidden>
          <div class="row clearfix">
            <div class="col-sm-12">
              <div class="form-group form-float">
                <div class="form-line">
                  <input name="nama" type="text" class="form-control">
                  <label class="form-label">Nama Bukti Dukung</label>
                </div>
              </div>
            </div>
          </div>
          <div class="row clearfix">
            <div class="col-sm-12">
              <div class="form-group form-float">
                <div class="form-line">
                  <input name="doc" type="file" class="dropify" />
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn bg-teal waves-effect">SIMPAN</button>
        </div>
      </form>
    </div>
  </div>
</div>

<!-- imagesloaded -->
<script src="<?= base_url(); ?>assets/plugins/imagesloaded/imagesloaded.pkgd.min.js"></script>
<!-- Masonry -->
<script src="<?= base_url(); ?>assets/plugins/masonry/masonry.pkgd.min.js"></script>
<!-- bootstrapValidator -->
<script type="text/javascript" src="<?= base_url(); ?>assets/plugins/bootstrapvalidator/js/bootstrapValidator.js">
</script>
<script type="text/javascript" src="<?= base_url(); ?>assets/plugins/bootstrapvalidator/js/language/id_ID.js"></script>
<!-- jquery.serializeObject -->
<script type="text/javascript" src="<?= base_url(); ?>assets/plugins/jQuery.serializeObject/jquery.serializeObject.min.js"></script>
<!-- Bootstrap Select Css -->
<script type="text/javascript" src="<?= base_url(); ?>assets/plugins/bootstrap-select/js/bootstrap-select.min.js">
</script>
<!-- Slimscroll Plugin Js -->
<script src="<?= base_url(); ?>assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- JQuery Steps Plugin Js -->
<script src="<?= base_url(); ?>assets/plugins/jquery-steps/jquery.steps.js"></script>
<!-- Bootstrap Notify Plugin Js -->
<script src="<?= base_url(); ?>assets/plugins/bootstrap-notify/bootstrap-notify.js"></script>
<!-- Waves Effect Plugin Js -->
<script type="text/javascript" src="<?= base_url(); ?>assets/plugins/node-waves/waves.js"></script>
<!-- Bootstrap Datepicker Plugin Js -->
<script src="<?= base_url(); ?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<!-- Dropify -->
<script src="<?= base_url(); ?>assets/plugins/dropify/js/dropify.min.js"></script>
<!-- EasyTimer.js -->
<script src="<?= base_url(); ?>assets/plugins/easytimer/easytimer.min.js"></script>
<!-- jQuery LoadingOverlay -->
<script src="<?= base_url(); ?>assets/plugins/gasparesganga-jquery-loading-overlay/loadingoverlay.min.js"></script>
<!-- Wait Me Plugin Js -->
<script src="<?= base_url(); ?>assets/plugins/waitme/waitMe.js"></script>
<!-- Autosize Plugin Js -->
<script src="<?= base_url(); ?>assets/plugins/autosize/autosize.js"></script>
<!-- AdminBSB Custom Js -->
<script src="<?= base_url(); ?>assets/plugins/adminbsb/admin.js"></script>
<!-- App Js -->
<script src="<?= base_url(); ?>assets/plugins/milea/js/app.js"></script>

<script type="text/javascript">
  const
    NIP = "<?= $this->session->userdata('nip') ?>",
    API_SERVER = "<?= $this->config->item('api_server') ?>";
  MONITORING_ID = "<?= $monitoringId ?>";

  let $kronologisForm = $('#kronologis-form')
  let $kronologisModal = $('#kronologis-modal')
  let $kronologisList = $('#kronologis-list')
  let $buktidukungForm = $('#buktidukung-form')
  let $buktidukungModal = $('#buktidukung-modal')

  $(function() {
    /* DROPIFY */
    $('.dropify').dropify({
      messages: {
        'default': 'Drag dan drop file/dokumen di sini atau klik',
        'replace': 'Drag dan drop atau klik untuk mengganti',
        'remove': 'Hapus',
        'error': 'Ooops, terjadi kesalahan.'
      }
    })

    /* KRONOLOGIS FORM */
    $kronologisForm
      .find('.datepicker')
      .datepicker({
        autoclose: true,
        format: "yyyy-mm-dd",
        language: "id",
        clearBtn: true,
        daysOfWeekDisabled: [0, 6],
        todayHighlight: true,
        container: $kronologisModal
      }).on('changeDate', function(ev) {
        $(this).closest('form').bootstrapValidator('revalidateField', $(this).attr('name'));
        $(this).closest('.form-line').addClass('focused')
      })
      .end()
      /* VALIDATOR */
      .bootstrapValidator({
        excluded: [':disabled', ':hidden'],
        feedbackIcons: {
          valid: 'glyphicon glyphicon-ok',
          invalid: 'glyphicon glyphicon-remove',
          validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
          jenis_rapat_id: {
            validators: {
              notEmpty: {}
            }
          },
          tanggal: {
            validators: {
              notEmpty: {}
            }
          },
          pembahasan: {
            validators: {
              notEmpty: {}
            }
          },
          rekomendasi_pendapat: {
            validators: {
              notEmpty: {}
            }
          },
        }
      })
      .on('success.form.bv', function(e) {
        e.preventDefault();
        $kronologisModal.modal('toggle');

        /*CREATE & UPDATE*/
        var data = $kronologisForm.serializeObject();
        data.dapur_monitoring_id = MONITORING_ID
        let type
        if ($kronologisForm.find('[type="submit"]').attr('data-action') == 'edit') {
          data.updatedby = NIP
          type = 'PUT'
        } else {
          data.createdby = NIP
          type = 'POST'
        }

        $.ajax({
          url: API_SERVER + "dapur/monitoring/kronologis",
          data: data,
          type: type,
          dataType: 'json',
          beforeSend: function() {
            showLoading($kronologisList)
          },
          complete: function() {
            hideLoading($kronologisList)
          },
          success: function(result) {
            readKronologis();
            notification('bg-green', `BERHASIL - ${result}`)
          },
          error: function(result) {
            notification('bg-orange', `GAGAL - ${result.responseJSON || result.statusText}`)
          }
        });

      })

    /* KRONOLOGIS MODAL */
    $kronologisModal.on('shown.bs.modal', function(event) {
      var action = $(event.relatedTarget).data('action')
      $kronologisForm.find('[type="submit"]').attr('data-action', action)
      $kronologisForm.bootstrapValidator('resetForm', true)

      if (action == 'edit') {
        $kronologisForm.find('h4').text('Ubah Kronologis')

        let kronologis = JSON.parse(decodeURIComponent($(event.relatedTarget).data('kronologis')));

        $kronologisForm.find('[name="id"]').val(kronologis.id)
        $kronologisForm.find('[name="jenis_rapat_id"]').selectpicker('val', kronologis.dapur_monitoring_jenis_rapat_id)
        $kronologisForm.find('[name="tanggal"]').datepicker('setDate', new Date(kronologis.tanggal))
        $kronologisForm.find('[name="pembahasan"]').val(kronologis.pembahasan).closest('.form-line').addClass('focused')
        $kronologisForm.find('[name="rekomendasi_pendapat"]').val(kronologis.rekomendasi_pendapat).closest('.form-line').addClass('focused')
      } else {
        $kronologisForm.find('h4').text('Tambah Kronologis');
      }
    })
    /* DELETE KRONOLOGIS */
    $kronologisList.on('click', '.delete', function() {
      Swal.fire({
        title: 'Hapus Kronologis?',
        type: 'warning',
        confirmButtonText: 'Ya',
        showCancelButton: true,
        cancelButtonText: 'Tidak',
        focusCancel: true,
        reverseButtons: true,
        showLoaderOnConfirm: true,
        preConfirm: () => {}
      }).then((result) => {
        if (result.value) {
          $.ajax({
            url: API_SERVER + "dapur/monitoring/kronologis",
            data: {
              id: $(this).attr('data-id')
            },
            type: 'DELETE',
            dataType: 'json',
            beforeSend: function() {
              showLoading($kronologisList)
            },
            complete: function() {
              hideLoading($kronologisList)
            },
            success: function(result) {
              readKronologis();
              notification('bg-green', `BERHASIL - ${result}`)
            },
            error: function(result) {
              notification('bg-orange', `GAGAL - ${result.responseJSON || result.statusText}`)
            }
          });
        }
      })
    });

    /* BUKTIDUKUNG FORM */
    $buktidukungForm
      /* VALIDATOR */
      .bootstrapValidator({
        excluded: [':disabled', ':hidden'],
        feedbackIcons: {
          valid: 'glyphicon glyphicon-ok',
          invalid: 'glyphicon glyphicon-remove',
          validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
          nama: {
            validators: {
              notEmpty: {}
            }
          },
          doc: {
            validators: {
              notEmpty: {},
              file: {
                maxSize: 5 * 1024 * 1024,
                /* 5 MB */
              }
            }
          },
        }
      })
      /* SUBMIT */
      .on('success.form.bv', function(e) {
        e.preventDefault();
        $buktidukungModal.modal('toggle');

        /* UPLOAD DOCUMENTS */
        let data = $buktidukungForm.serializeObject()
        let kronologisId = data.kronologisId
        var formData = new FormData();
        formData.append('file', $buktidukungForm.find('[name="doc"]')[0].files[0]);
        formData.append('kronologisId', kronologisId);

        $.ajax({
          url: API_SERVER + "dapur/monitoring/kronologis/upload",
          data: formData,
          type: 'POST',
          processData: false,
          contentType: false,
          beforeSend: function() {
            showLoading($kronologisList)
          },
          complete: function() {
            hideLoading($kronologisList)
          },
          success: function(result) {
            let type
            if ($buktidukungForm.find('[type="submit"]').attr('data-action') == 'edit') {
              data.updatedby = NIP
              type = 'PUT'
            } else {
              data.createdby = NIP
              type = 'POST'
            }

            if (result) data.path = result;

            /*CREATE / UPDATE BUKTIDUKUNG */
            $.ajax({
              url: API_SERVER + "dapur/monitoring/kronologis/buktidukung",
              data: data,
              type: type,
              dataType: 'json',
              beforeSend: function() {
                showLoading($kronologisList)
              },
              complete: function() {
                hideLoading($kronologisList)
              },
              success: function(result) {
                readKronologis()
              },
              error: function(result) {
                notification('bg-orange', `GAGAL - ${result.responseJSON || result.statusText}`)
              }
            });

          },
          error: function(result) {
            notification('bg-orange', `GAGAL - ${result.responseJSON || result.statusText}`)
          }
        });

      })

    /* BUKTIDUKUNG MODAL */
    $buktidukungModal.on('shown.bs.modal', function(event) {
      var action = $(event.relatedTarget).data('action')
      $buktidukungForm.find('[type="submit"]').attr('data-action', action)
      $buktidukungForm.bootstrapValidator('resetForm', true)

      if (action == 'edit') {
        $buktidukungForm.find('h4').text(`Ubah Bukti Dukung`)

        var buktidukung = JSON.parse(decodeURIComponent($(event.relatedTarget).data('buktidukung')));

        $buktidukungForm.find('[name="id"]').val(buktidukung.id)
        $buktidukungForm.find('[name="kronologisId"]').val('')
        $buktidukungForm.find('[name="nama"]').val(buktidukung.nama).closest('.form-line').addClass('focused')
        $buktidukungForm.find('[name="path"]').val(buktidukung.path)

        var defaultFile = (buktidukung.path) ? `${API_SERVER}dapur/monitoring/kronologis/buktidukung/view?id=${buktidukung.id}` : ``;
        var drEvent = $buktidukungForm.find('[name="doc"]').dropify({
          defaultFile: defaultFile
        });

        drEvent.on('dropify.beforeClear', function(event, element) {
          $buktidukungForm.bootstrapValidator('revalidateField', 'doc');
        });

        drEvent = drEvent.data('dropify');
        drEvent.resetPreview();
        drEvent.clearElement();
        drEvent.settings.defaultFile = defaultFile;
        drEvent.destroy();
        drEvent.init();

        if (buktidukung.path) $buktidukungForm.bootstrapValidator('updateStatus', 'doc', 'VALID');

      } else {
        $buktidukungForm.find('h4').text(`Tambah Bukti Dukung`);

        var kronologisId = $(event.relatedTarget).data('kronologis-id')

        $buktidukungForm.find('[name="id"]').val('')
        $buktidukungForm.find('[name="kronologisId"]').val(kronologisId)
        $buktidukungForm.find('[name="nama"]').val('').closest('.form-line').removeClass('focused')
        $buktidukungForm.find('[name="path"]').val('')


        var drEvent = $buktidukungForm.find('[name="doc"]').dropify();
        drEvent = drEvent.data('dropify');
        drEvent.resetPreview();
        drEvent.clearElement();

        $buktidukungForm.bootstrapValidator('updateStatus', 'doc', 'NOT_VALIDATED');
      }
    })
    /* DELETE BUKTIDUKUNG */
    $kronologisList.on('click', '.delete-buktidukung', function() {
      Swal.fire({
        title: 'Hapus Bukti dukung?',
        type: 'warning',
        confirmButtonText: 'Ya',
        showCancelButton: true,
        cancelButtonText: 'Tidak',
        focusCancel: true,
        reverseButtons: true,
        showLoaderOnConfirm: true,
        preConfirm: () => {}
      }).then((result) => {
        if (result.value) {
          $.ajax({
            url: API_SERVER + "dapur/monitoring/kronologis/buktidukung",
            data: {
              id: $(this).attr('data-id')
            },
            type: 'DELETE',
            dataType: 'json',
            beforeSend: function() {
              showLoading($kronologisList)
            },
            complete: function() {
              hideLoading($kronologisList)
            },
            success: function(result) {
              readKronologis();
              notification('bg-green', `BERHASIL - ${result}`)
            },
            error: function(result) {
              notification('bg-orange', `GAGAL - ${result.responseJSON || result.statusText}`)
            }
          });
        }
      })
    });

    readKronologis()
  })
</script>

<script type="text/javascript">
  function readKronologis() {
    $.ajax({
      url: `${API_SERVER}dapur/monitoring/kronologis?dapur_monitoring_id=${MONITORING_ID}`,
      type: 'GET',
      dataType: 'json',
      beforeSend: function() {
        showLoading($kronologisList)
      },
      complete: function() {
        hideLoading($kronologisList)
      },
      success: function(result) {
        let html = ''
        if (result.kronologis.length == 0) {
          html = '<p class="font-italic col-grey">Tidak ada data yang ditampilkan</p>';
        } else {
          html = result.kronologis.map((r, index) => {
              let buktidukungListHtml
              if (r.buktidukungs.length == 0) {
                buktidukungListHtml = '<p class="font-italic col-grey m-t-10 font-12">Tidak ada data yang ditampilkan</p>';
              } else {
                buktidukungListHtml = r.buktidukungs.map((bd, bdIdx) => {
                    let buktidukungCreatorAbilityHtml = ''
                    if (r.createdby == NIP) {
                      buktidukungCreatorAbilityHtml = `
                      <button type="button" class="btn bg-orange btn-xs waves-effect" data-toggle="modal" data-target="#buktidukung-modal" data-action="edit" data-buktidukung="${encodeURIComponent(JSON.stringify(bd))}">Ubah</button>
                      <button type="button" class="btn bg-red btn-xs waves-effect delete-buktidukung" data-id="${bd.id}">Hapus</button>
                    `
                    }
                    let out = `
                    <tr>
                      <td><a href="${API_SERVER}dapur/monitoring/kronologis/buktidukung/view?id=${bd.id}" target="_blank">${bd.nama}</a></td>
                      <td>
                        <div class="pull-right">
                          ${buktidukungCreatorAbilityHtml}
                        </div>
                      </td>
                    </tr>`
                    return out
                  })
                  .join('\n')

                buktidukungListHtml = `
                <table class="table table-hover m-t-10 font-12">
                  <tbody>
                    ${buktidukungListHtml}
                  </tbody>
                </table>`
              }

              let creatorAbilityHtml = ''
              if (r.createdby == NIP) {
                creatorAbilityHtml = `
                  <button type="button" class="btn bg-orange btn-xs waves-effect" data-toggle="modal" data-target="#kronologis-modal" data-action="edit" data-kronologis="${encodeURIComponent(JSON.stringify(r))}">Ubah</button>
                  <button type="button" class="btn bg-red btn-xs waves-effect delete" data-id="${r.id}">Hapus</button>
                `
              }

              let out = `
                <div class="panel" style="border: 1px solid rgb(221, 221, 221);">
                  <div class="panel-heading" role="tab" id="kronologis-${r.id}">
                    <div class="panel-title">
                      <div class="row clearfix">
                        <a role="button" data-toggle="collapse" data-parent="#kronologis-list" href="#kronologis-${r.id}-detail" aria-expanded="false" class="collapsed">
                          <div class="col-sm-1">
                            <div class="font-12 col-grey">
                              No
                            </div>
                            <div class="font-14">
                              ${index+1}
                            </div>
                          </div>
                          <div class="col-sm-2">
                            <div class="font-12 col-grey">
                              Tahapan
                            </div>
                            <div class="badge bg-${r.jenis_rapat_col} font-12">${r.jenis_rapat}</div>
                          </div>
                          <div class="col-sm-2">
                            <div class="font-12 col-grey">
                              Tanggal
                            </div>
                            <div class="font-12">${formatDate(r.tanggal)}</div>
                          </div>
                          <div class="col-sm-3">
                            <div class="font-12 col-grey">
                              Pembahasan
                            </div>
                            <div class="font-12">${r.pembahasan}</div>
                          </div>
                          <div class="col-sm-2">
                            <div class="font-12 col-grey">
                              Rekomendasi/Pendapat
                            </div>
                            <div class="font-12">${r.rekomendasi_pendapat}</div>
                          </div>
                        </a>
                        <div class="col-sm-2">
                          <div class="font-12">
                            ${creatorAbilityHtml}
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div id="kronologis-${r.id}-detail" class="panel-collapse collapse" role="tabpanel" aria-expanded="false">
                    <div class="panel-body">
                      <p class="font-bold">Bukti Dukung</p>
                      <button type="button" class="btn bg-primary btn-xs waves-effect" data-toggle="modal" data-target="#buktidukung-modal" data-action="add" data-kronologis-id="${r.id}">Upload</button>
                      ${buktidukungListHtml}
                    </div>
                  </div>
                </div>
                `
              return out
            })
            .join('\n')
        }

        $('#monitoring-judul').text(result.monitoring_judul)
        $kronologisList.html(html)
      },
      error: function(result) {
        notification('bg-orange', `GAGAL - ${result.responseJSON || result.statusText}`)
      }
    });

  }
</script>