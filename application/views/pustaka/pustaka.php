<script src="<?= base_url('assets/plugins/slick/slick.min.js'); ?>" type="text/javascript"></script>
<link href="<?= base_url('assets/plugins/slick/slick.css'); ?>" rel="stylesheet" type="text/css" />
<link href="<?= base_url('assets/plugins/slick/slick-theme.css'); ?>" rel="stylesheet" type="text/css" />


<style media="screen">
   #search input[type="text"] {
      background: url(search-white.png) no-repeat 10px 6px #FFF;
      border: 1px solid #d1d1d1;
      font: 12px Arial, Helvetica, Sans-serif;
      color: #bebebe;
      width: 150px;
      padding: 6px 15px 6px 10px;
      transition: all 0.7s ease 0s;
   }

   #search input[type="text"]:focus {
      width: 200px;
   }

   .bayangan {
      box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
      border-radius: 5px;
   }

   .baru {
      margin-bottom: 70px;
      width: 450px;
      float: left;
      border-right: solid 1px #ECF0F5;
      margin-right: 10px;
      border-radius: 10px;
   }

   .teks {
      color: #222D32
   }
</style>
<div class="row" style="padding:10px 0px;background:#fff;">
   <div class="col-md-4 hidden-sm hidden-xs" style="padding-top:3px">
      <!-- <span class="pull-left" style="padding:7px 0px 0px 0px">
         <a href="http://intranet.anggaran.depkeu.go.id/perpustakaan" data-toggle="tooltip" title="Pencarian menuju laman Perpustakaan DJA">
            <i class="fa fa-search" style="color:#7E8892"></i></a>
      </span> -->
   </div>
   <div class="col-md-4 text-center">
      <h4 style="font-family:Arial;color:#2E5984">
         <a href="<?= base_url("/perpustakaan") ?>" data-toggle="tooltip" title="Perpustakaan DJA">Perpustakaan <b>DJA</b></a>
      </h4>
   </div>
   <!-- <div class="col-md-4 text-right hidden-sm hidden-xs">
      <span class="pull-right" style="padding:9px 0px 0px 0px">
         <a href="http://intranet.anggaran.depkeu.go.id/perpustakaan" data-toggle="tooltip" title="Statistik">
            <i class="fa fa-bar-chart" style="color:#7E8892"></i> </a> &nbsp;&nbsp;
         <a href="http://intranet.anggaran.depkeu.go.id/perpustakaan" data-toggle="tooltip" title="Perpustakaan">
            <i class="fa fa-safari" style="color:#7E8892"></i> </a>
      </span>
   </div> -->
</div>

<div class="row">
   <div class="col-md-12" style="padding:10px 0px">
      <div class="bukubaru">
         <?php foreach ($buku_baru as $baru) {
            $warna = "fff";
         ?>
            <div class="box box-widget baru" style="background: <?php echo '#' . $warna; ?>;">
               <div class="box-body with-border" style="margin-left: 10px;">
                  <div class="user-block-dsw">
                     <?php
                     $cover = base_url("/perpustakaan/images/docs/no_cover.png");
                     if ($baru['image']) $cover =  base_url("/perpustakaan/images/docs/" . $baru['image']); ?>

                     <img class="bayangan" style="margin:20px 0px -45px 20px; width: 170px; height: 250px;" src="<?php echo $cover ?>">
                     <div style="padding-left:150px;padding-top:20px">
                        <span class="username" style="color:#2E5984; padding-bottom: 7px"><?php echo $baru['title'] ?></span>
                        <span class="description text-bold" style="color:#528AAE; padding-bottom:7px"><?php echo $baru['author_name'] ?><br></span>
                        <span class="description" style="color:#528AAE"><?php echo $baru['publisher_name'] . ' ' . $baru['edition'] . ' ' . $baru['place_name'] . ' ' . $baru['publish_year'] ?> <br></span>
                        <span class="description" style="color:#528AAE"><?php echo $baru['collation'] ?> <br></span>
                        <span class="description" style="color:#528AAE"><?php if ($baru['isbn_issn']) echo "ISBN " . $baru['isbn_issn'] ?> <br></span>
                     </div>
                  </div>

                  <a href="#">
                     <div class="text-right" style="margin:-10px 20px -10px 300px;">
                        <h6 style="padding:0px;">
                           <a href="<?= base_url("/perpustakaan") ?>" data-toggle="tooltip" title="Perpustakaan DJA"><i class="text-orange fa fa-certificate"></i> Buku <b>Baru</b></a>
                        </h6>
                     </div>
                  </a>

               </div>
            </div>
            <div></div>

         <?php } ?>
      </div>
   </div>
</div>

<div class="row">
   <div class="col-md-12">
      <div class="col-md-10">
         <div style="background-color: #fff; border-bottom: solid 1px #96D3DD">
            <span style="vertical-align:middle;">
               <h4 style="color:#2E5984;margin:0px;padding:10px 15px;">Buku <b>Terpopuler</b>
                  <small> (Paling Sering Dipinjam)</small>
               </h4>
            </span>
         </div>

         <div class="col-md-12" style="background-color: #fff;; padding:0px 15px;">&nbsp;</div>
         <div class="col-md-12" style="padding:0px; background-color: #fff;">
            <?php
            foreach ($popular as $pop) { ?>

               <div class="col-md-6" style="padding-bottom:0px;padding-left:0px">
                  <div class="box box-widget">
                     <div class="box-body" style="padding-left:15px">
                        <div class="user-block-dsw">
                           <?php
                           $cover = base_url("/perpustakaan/images/docs/no_cover.png");
                           if ($pop['image']) $cover =  base_url("/perpustakaan/images/docs/" . $pop['image']); ?>
                           <img class="bayangan" style="margin-top:0px; width: 100px; height: 135px" src="<?php echo $cover; ?>">

                           <div class="" style="padding:0px 10px 0px 115px; line-height: 130%">
                              <span class="text-bold" style="text-overflow: ellipsis;"><?php echo $pop['title'] ?><br></span>
                              <span class="small text-primary"><?php echo $pop['author_name'] ?><br></span>
                              <span class="small "><?php echo $pop['publisher_name'] . ' ' . $pop['edition'] . ' ' . $pop['place_name'] . ' ' . $pop['publish_year'] ?> <br></span>
                              <span class="small "><?php echo $pop['collation'] ?><br></span>
                              <span class="small "><?php if ($baru['isbn_issn']) echo "ISBN " . $baru['isbn_issn'] ?></span>
                           </div>

                        </div>
                     </div>
                     <div class="small text-center" style="padding:7px 15px;">
                        <?php
                        if ($pop['is_return'] == '0') {
                           echo "Sedang dipinjam, jatuh tempo : <b class='text-danger'>" . $this->fc->idtgl($pop['due_date'], 'hari') . "</b>";
                        } else {
                           echo "Status: <b class='text-success'>Tersedia</b>";
                        }
                        ?>
                     </div>

                  </div>
               </div>
            <?php } ?>
         </div>
      </div>

      <div class="card">
         <div class="col-md-2" style="padding: 0px;">
            <div class="box box-widget" style="padding: 10px;">
               <div style="border-bottom:solid 1px #96D3DD;padding-bottom:5px">
                  <h5 style="font-family:Arial;color:#2E5984;margin:0px;padding:8px 0px 0px 0px">Anggota Teraktif <b><?php echo $this->session->userdata('thang'); ?> </b></h5>

               </div>
               <div class="small" style="padding-top:5px;color:#408EBA">
                  <ul style="padding-left:15px">
                     <?php
                     foreach ($teraktif as $aktif) { ?>
                        <li><?php echo ucwords($aktif['member_name']); ?></li>
                     <?php } ?>
                  </ul>
               </div>
               <br>
               <div style="border-bottom:solid 1px #96D3DD;padding-bottom:5px">
                  <h5 style="font-family:Arial;color:#2E5984;margin:0px;padding:0px">Anggota <b>Teraktif </b></h5>
               </div>
               <div class="small" style="padding-top:5px;color:#408EBA">
                  <ul style="padding-left:15px">
                     <?php
                     foreach ($teraktif_all as $aktif) { ?>
                        <li><?php echo ucwords($aktif['member_name']); ?></li>
                     <?php } ?>
                  </ul>
               </div>
            </div>
         </div>
      </div>
   </div>

   <div class="row">
      <div class="col-md-12 text-center" style="margin-top:10px">
         <h5 style="margin-bottom:3px">Perputakaan <b>Direktorat Jenderal Anggaran</b></h5>
         <h6 style="font-family:Arial;color:#2E5984; margin:0px;padding:0px">Gedung Sutikno Slamet Lt. 15, Jln. Wahidin No. 15 Jakarta Pusat. Intern 8054 - 9715 </h6>
      </div>
   </div>


   <script type="text/javascript">
      $(".bukubaru").slick({
         infinite: true,
         centerMode: true,
         slidesToShow: 3,
         slidesToScroll: 1,
         autoplay: true,
         autoplaySpeed: 2000,
         variableWidth: true,
      });
   </script>