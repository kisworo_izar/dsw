<link href="<?=base_url('assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css');?>" rel="stylesheet" type="text/css" />
<link href="<?=base_url('assets/dist/css/revisi.css');?>" rel="stylesheet" type="text/css" />
<script src="<?=base_url('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js');?>" type="text/javascript"></script>

<div class="row" ng-app="app" >
  <div class="col-md-12">
    <div class="box box-widget">

      <div class="box-header">
         <div class="row">

            <div class="col-md-5">
              <form role="form" action="<?php echo site_url("Revisi_Kanwil?=rk4w1l") ?>" method="post" style="margin-bottom:0px">
                  <div class="input-group">
                    <input type="text" name="cari" class="form-control" placeholder="Pencarian..." value="<?php echo $this->session->userdata('cari') ?>" autocomplete="off">
                    <span class="input-group-btn">
                        <button type="submit" name="search" value="search" class="btn btn-primary btn-flat"><i class="fa fa-search" style="height:16px;margin-top:4px"></i></button>
                    </span>
                    <span class="input-group-btn">
                        <button type="submit" name="search" value="clear" class="btn btn-primary btn-flat"><i class="fa fa-times-circle" style="height:16px;margin-top:4px"></i></button>
                    </span>
                  </div>
              </form>

            </div>

            <div class="col-md-1">
                <span class="input-group-btn">
                  <button type="button" class="btn btn-primary btn-flat" data-toggle="tooltip" title="Laporan Revisi - Excel" ><i class="fa fa-file-excel-o" style="height:16px;margin-top:4px" onclick="excel()"></i></button>
                </span>
            </div>

            <div class="col-md-4 pull-right" id="sandbox-container" style="height:20px">
              <div class="input-daterange input-group" id="datepicker">
                <input type="text" class="input-sm form-control" value="<?php echo $start; ?>" name="start" id="start" style="padding-top:0px; padding-bottom:0px;  height:30px">
                <span class="input-group-addon" style="border:0px;"> s.d. </span>
                <input type="text" class="input-sm form-control" value="<?php echo $end; ?>" name="end" id="end" style="padding-top:0px; padding-bottom:0px; height:30px">
              </div>
            </div>

         </div>
        </div>

      <div class="box-body" style="padding-top:0px">
         <section class="">
         <div class="container" style="height:650px">

        <table id="iGrid" class="table table-hover table-bordered">
            <thead class="hidden-xs">
               <tr class="header">
                  <th style="padding:0px" colspan="2"><div class="calculated-width" style="padding-left:0px">ID Revisi<br><span class="small">Satuan Kerja </span></div></th>
                  <th style="width:73px;padding:0px"><div style="padding-left:10px">Tahap 1<br><span class="small">Upload</span></div></th>
                  <th style="width:73px;padding:0px"><div style="padding-left:10px">Tahap 2<br><span class="small">Diteliti</span></div></th>
                  <th style="width:73px;padding:0px"><div style="padding-left:10px">Tahap 3<br><span class="small">Lengkap</span></div></th>
                  <th style="width:73px;padding:0px"><div style="padding-left:10px">Tahap 4<br><span class="small">Penetapan</span></div></th>
                  <th style="width:73px;padding:0px"><div style="width:73px">Tahapan<br><span class="small">Status</span></div></th>
               </tr>
            </thead>
          <tbody>


            <?php
            if ($revisi) {
              foreach ($revisi as $key=>$row) { ?>
              <tr style="margin:0px">

                <!-- <td class="text-bold text-primary" style="text-align:center;padding-right:7px" data-toggle="collapse" data-target=".<?= str_replace('.', '', $row['rev_id']) ?>"><?= substr($row['rev_id'],-3) ?></td> -->
                <td class="text-bold" style="width:7px;border:solid 0px;border-bottom:solid 1px #F4F4F4;" data-toggle="collapse" data-target=".<?= str_replace('.', '', $row['rev_id']) ?>"><?= $row['kl_dept'] ?></td>
                <td style="padding-left:0px;border:solid 0px;border-bottom:solid 1px #F4F4F4; padding-left:8px" data-toggle="collapse" data-target=".<?= str_replace('.', '', $row['rev_id']) ?>">
                  <div class="text-bold"><?= ' ID Revisi '. $row['rev_id']  ; ?></div>
                  <div><?= $row['kl_satker'] .' '. $row['nmsatker'] ?></div>
                </td>

                <!-- Tahap 1: PUSAT LAYANAN  -->
                <?php $w=$row['pus_status']; $w1='';
                  if ($w=='0') $w1='merah'; if ($w=='1') $w1='biru'; if ($w=='2') $w1='hijau';
                  $site1 = "";
                ?>
                <td class="small text-center <?= $w1 ?>">
                  <a href="<?= $site1 ?>" data-toggle="tooltip" title="Nomor Tiket <?= $row['rev_id'] ?> - Proses Tahap 1">
                  <span><?= $this->fc->idtgl($row['pus_tgl']) ?></span><br>
                  <span><i class="fa fa-clock-o"></i> <?= $this->fc->idtgl($row['pus_tgl'], 'jam') ?></span></a>
                </td>


                <!-- Tahap 2: TELITI DOKUMEN  -->
                <?php $w=$row['t2_status']; $w2=''; $j2=''; $u=$this->session->userdata('idusergroup');
                  if ($w=='0') $w2='merah'; if ($w=='1') $w2='biru'; if ($w=='2') $w2='hijau'; if ($w=='9') $w2='abu';
                  if ($row['rev_tahap']>='2' and $w2!=''  ) {
                    if ($row['t2_status']=='9') {
                      if ((strpos("x$u", '100') or strpos("x$u", '611'))) {
                      $site2 = site_url('Revisi_Kanwil?q=rk4w1l2a&rev_id='. $row['rev_id']);
                      } else { $site2='';  }
                    } else {
                      $j2 = '<i class="fa fa-clock-o"></i>';
                      if ((strpos("x$u", '100') or strpos("x$u", '611'))) {
                      $site2 = site_url('Revisi_Kanwil?q=rk4w1l2b&rev_id='. $row['rev_id']);
                       } else { $site2='';  }
                    }
                ?>
                <td class="small text-center <?= $w2 ?>">
                  <a href="<?= $site2 ?>" data-toggle="tooltip" title="Nomor Tiket <?= $row['rev_id'] ?> - Proses Tahap 2">
                  <span><?= $this->fc->idtgl($row['t2_tgl']) ?></span><br>
                  <span><?= $j2 .' '. $this->fc->idtgl($row['t2_tgl'], 'jam') ?></span></a>
                </td>
                <?php } else { echo '<td> &nbsp; </td>'; } ?>



                <!-- Tahap 6: KELENGKAPAN DOKUMEN -->
                <?php $w=$row['t6_status']; $w6=''; $j6='';
                  if ($w=='0') $w6='merah'; if ($w=='1') $w6='biru'; if ($w=='2') $w6='hijau'; if ($w=='9') $w6='abu'; if ($w=='3') $w6='kuning';
                  if ($row['rev_tahap']>='6' and $w6!='') {
                    if ($row['t6_status']=='9') {
                      if ((strpos("x$u", '100') or strpos("x$u", '612'))) {
                         $site6 = site_url('Revisi_Kanwil?q=rk4w1l6a&rev_id='. $row['rev_id']);
                      } else { $site6=''; }
                    } else {
                      $j6 = '<i class="fa fa-clock-o"></i>';
                      if ((strpos("x$u", '100') or strpos("x$u", '612'))) {
                        $site6 = site_url('Revisi_Kanwil?q=rk4w1l6b&rev_id='. $row['rev_id']);
                      } else { $site6=''; }
                    }
                ?>
                <td class="small text-center <?= $w6 ?>">
                  <a href="<?= $site6 ?>" data-toggle="tooltip" title="Nomor Tiket <?= $row['rev_id'] ?> - Proses Tahap 3">
                  <span><?= $this->fc->idtgl($row['t6_tgl']) ?></span><br>
                  <span><?= $j6 .' '. $this->fc->idtgl($row['t6_tgl'], 'jam') ?></span></a>
                </td>
                <?php } else { echo '<td> &nbsp; </td>'; } ?>

                <!-- Tahap 7: NOTA DINAS REVISI -->
                <?php $w=$row['t7_status']; $w7=''; $j7='';
                  if ($w=='0') $w7='merah'; if ($w=='1') $w7='biru'; if ($w=='2') $w7='hijau'; if ($w=='9') $w7='abu';
                  if ($row['rev_tahap']>='7') {
                    if ($row['t7_status']=='9') {
                      if ((strpos("x$u", '100') or strpos("x$u", '612'))) {
                      $site7 = site_url('Revisi_Kanwil?q=rk4w1l7a&rev_id='. $row['rev_id']);
                       } else { $site7=''; }
                    } else {
                      $j7 = '<i class="fa fa-clock-o"></i>';
                      if ((strpos("x$u", '100') or strpos("x$u", '612'))) {
                      $site7 = site_url('Revisi_Kanwil?q=rk4w1l7b&rev_id='. $row['rev_id']);
                       } else { $site7=''; }
                    }
                ?>
                <td class="small text-center <?= $w7 ?>">
                  <a href="<?= $site7 ?>" data-toggle="tooltip" title="Nomor Tiket <?= $row['rev_id'] ?> - Proses Tahap 4">
                  <span><?= $this->fc->idtgl($row['t7_tgl']) ?></span><br>
                  <span><?= $j7 . $this->fc->idtgl($row['t7_tgl'], 'jam') ?></span></a>
                </td>
                <?php } else { echo '<td> &nbsp; </td>'; } ?>

                <?php $w8='';
                  if ($row['rev_tahap']==2) { $w=$row['t2_status']; $tampil_tahap='2'; }
                  if ($row['rev_tahap']==6) { $w=$row['t6_status']; $tampil_tahap='3'; }
                  if ($row['rev_tahap']==7) { $w=$row['t7_status']; $tampil_tahap='4'; }
                  if ($w=='0') $w8='merah'; if ($w=='1') $w8='biru'; if ($w=='2') $w8='hijau'; if ($w=='9') $w8='abu';
                 ?>
                <td class="text-center text-bold text-muted <?= $w8 ?>">
                  <span style="font-size:25px"><?=$tampil_tahap ?></span><br>

                </td>
              </tr>


              <tr style="background:#FFF;color:#34495E">
                  <td class="hiddenRow" colspan="10" style="border-bottom: solid 1px #E7E9EE">
                    <div class="small collapse info <?= str_replace('.', '', $row['rev_id']) ?>">
                      <div style="margin-left:35px; padding-top:10px">

                          <table>
                            <tr>
                              <td><span class="x">ID Revisi</span>: <b><?= $row['rev_id']  ?></b></td>
                              <td></td>
                            </tr>
                            <tr>
                              <td width="50%"><span class="x">Nomor Surat</span>: <?= $row['kl_surat_no'] ?></td>
                              <td><span class="x">Tanggal Surat</span>: <?= $this->fc->idtgl($row['kl_surat_tgl']) ?></td>
                            </tr>
                            <tr>
                              <td width="50%"><span class="x">Pejabat</span>: <?= $row['kl_pjb_jab'] ?></td>
                              <td><span class="x">Nama Pejabat</span>: <?= $row['kl_pjb_nama'] ?></td>

                            </tr>
                            <tr>
                              <td width="50%"><span class="x">Perihal</span>: <?= $row['kl_surat_hal'] ?></td>
                              <td><span class="x">No Hp</span>: <?= $row['kl_telp'] ?></td>

                          </tr>
                          </table>
                          <hr style="margin:5px 0px">

                        <div class="">

                          <div style="float:left; width:50%">
                            <b>Dokumen Usulan Revisi Anggaran</b><br>
                            <ul style="padding-left:17px">
                              <?php
                              $vrev_id = $row['rev_id'];
                              $tahap   = $row['rev_tahap'];

                               //$dbrevisi = $this->load->database('revisi', TRUE);

                              $query = $this->load->dbrevisi->query("Select doc_usulan_revisi, doc_matrix, doc_rka, doc_adk, doc_dukung_sepakat,doc_dukung_es1, doc_dukung_hal4, t3_und_file, t4_menteri_file, t5_hasil_file, t6_surat_file, doc_dukung_lainnya, t7_nd_file, t7_sp_file, appl From revisi Where rev_id='$vrev_id'");
                              $docs  = $query->row_array();

                              if ( $docs['doc_usulan_revisi']!='' And file_exists("files/revisi_SatuDJA/$vrev_id/". $docs['doc_usulan_revisi']) ) {
                                echo '<li><a href="'. site_url("files/revisi_SatuDJA/$vrev_id/". $docs['doc_usulan_revisi']) .'"'.' style="color:#3C8DBC">Surat usulan Revisi Anggaran</a></li>';
                              }
                              if ( $docs['doc_adk']!='' And file_exists("files/revisi_SatuDJA/$vrev_id/". $docs['doc_adk']) ) {
                                echo '<li><a href="'. site_url("files/revisi_SatuDJA/$vrev_id/". $docs['doc_adk']) .'"'.' style="color:#3C8DBC">Arsip Data Komputer (ADK) RKA-K/L revisi Satker</a></li>';
                                if ( file_exists("files/revisi_SatuDJA/$vrev_id/adk_satker") ) {
                                echo '<a href="'. site_url("Revisi_dja/create_zip_adk/$vrev_id").'"'.' style="color:#3C8DBC"> Download ADK Persatker </a> </li>';}
                              }
                              if ( $docs['doc_dukung_es1']!='' And file_exists("files/revisi_SatuDJA/$vrev_id/". $docs['doc_dukung_es1']) ) {
                                echo '<li><a href="'. site_url("files/revisi_SatuDJA/$vrev_id/". $docs['doc_dukung_es1']) .'"'.' style="color:#3C8DBC">Dokumen pendukung terkait persetujuan unit eselon I</a></li>';
                              }
                              if ( $docs['doc_dukung_lainnya']!='' And file_exists("files/revisi_SatuDJA/$vrev_id/". $docs['doc_dukung_lainnya']) ) {
                                  echo '<li><a href="'. site_url("files/revisi_SatuDJA/$vrev_id/". $docs['doc_dukung_lainnya']) .'"'.' style="color:#3C8DBC">Dok. Pendukung terkait lainnya</a></li>';
                              }
                              ?>
                            </ul>
                          </div>

                          <?php 
                            if ($docs['appl']=='sakti') { ?>
                            <span class="pull-right" style="margin-right: 10px"><img src="/files/images/inter.png" width="100px" alt=""></span>
                          <?php  } ?>

                          <div style="float:right; width:50%">
                             <?php if ($docs['t3_und_file'] != '' or $docs['t4_menteri_file'] != '' or $docs['t5_hasil_file'] != ''  or $docs['t6_surat_file'] != '') {?>

                               <b>Dokumen Proses Revisi Anggaran </b><br>
                               <ul style="padding-left:17px">
                                  <?php
                                  if ( $docs['t3_und_file']!='' And file_exists("files/revisi_SatuDJA/$vrev_id/". $docs['t3_und_file']) ) {
                                    echo '<li><a href="'. site_url("files/revisi_SatuDJA/$vrev_id/". $docs['t3_und_file']) .'"'.' style="color:#3C8DBC">Undangan Penelaahan Revisi Anggaran</a></li>';
                                  }
                                  if ( $docs['t4_menteri_file']!='' And file_exists("files/revisi_SatuDJA/$vrev_id/". $docs['t4_menteri_file']) ) {
                                    echo '<li><a href="'. site_url("files/revisi_SatuDJA/$vrev_id/". $docs['t4_menteri_file']) .'"'.' style="color:#3C8DBC">Surat Persetujuan Menteri Keuangan</a></li>';
                                  }
                                  if ( $docs['t5_hasil_file']!='' And file_exists("files/revisi_SatuDJA/$vrev_id/". $docs['t5_hasil_file']) ) {
                                    echo '<li><a href="'. site_url("files/revisi_SatuDJA/$vrev_id/". $docs['t5_hasil_file']) .'"'.' style="color:#3C8DBC">Surat Permintaan Perbaikan Dokumen Revisi Anggaran</a></li>';
                                  }
                                  if ( $docs['t6_surat_file']!='' And file_exists("files/revisi_SatuDJA/$vrev_id/". $docs['t6_surat_file']) ) {
                                    echo '<li><a href="'. site_url("files/revisi_SatuDJA/$vrev_id/". $docs['t6_surat_file']) .'"'.' style="color:#3C8DBC">Surat Kelengkapan Dokumen Revisi Anggaran</a></li>';
                                  }
                                  ?>
                               </ul>
                            <?php } ?>


                            <?php if ($docs['t7_nd_file'] != '' or $docs['t7_sp_file'] != '') {?>

                               <b>Penetapan Revisi Anggaran</b><br>
                                 <ul style="padding-left:17px">
                                 <?php
                                   if ( $docs['t7_nd_file']!='' And file_exists("files/revisi_SatuDJA/$vrev_id/". $docs['t7_nd_file']) ) {
                                     echo '<li><a href="'. site_url("files/revisi_SatuDJA/$vrev_id/". $docs['t7_nd_file']) .'"'.' style="color:#3C8DBC">Kertas Kerja Nota Dinas Revisi Anggaran</a></li>';
                                   }
                                   if ( $docs['t7_sp_file']!='' And file_exists("files/revisi_SatuDJA/$vrev_id/". $docs['t7_sp_file']) ) {
                                     echo '<li><a href="'. site_url("files/revisi_SatuDJA/$vrev_id/". $docs['t7_sp_file']) .'"'.' style="color:#3C8DBC">Surat Penetapan Revisi Angggaran</a></li>';
                                   }
                                 ?>
                               </ul>
                            <?php } ?>
                          </div>
                       </div>
    
                        <?php
                        if ($row['bypass_ctt'] != '') { ?>
                            <div style="float:right; width:100%; margin-bottom:5px">
                              <b>Catatan Pengajuan Revisi: </b><br>
                              <span class="text-warning" style=""><em> <?php echo $row['bypass_ctt'] ?> </em>
                              <?php
                              if ($row['bypass_file'] != '') { ?>
                              <br>
                              <ul style="padding-left:17px">
                                <?php
                                  if ( $row['bypass_file']!='' And file_exists("files/revisi_SatuDJA/$vrev_id/". $row['bypass_file']) ) {
                                     echo '<li><a href="'. site_url("files/revisi_SatuDJA/$vrev_id/". $row['bypass_file']) .'"'.' style="color:#3C8DBC">Dokumen Pendukung Catatan Pengajuan Revisi</a></li>';
                                }?>
                              <?php } ?>   
                              </ul>
                            </span>
                            </div>

                        <?php } ?>   


                       <?php
                         if ($row['t2_catatan'] != '' and ($row['rev_tahap'] == '2' or ($row['rev_tahap'] == '6' and $row['t6_status'] == '9'))) {
                          $teks = ($row['rev_status'] =='0' ? 'text-danger' : 'text-success');
                          echo '
                           <div style="float:right; width:100%; margin-bottom:10px">
                             <b>Catatan Hasil Penelitian: </b><br>
                             <span class="'.$teks.' small" style="white-space:pre"><em>'. $row['t2_catatan'] .'</em><span>
                           </div>';}

                       if ($row['rev_tahap'] == '7' and $row['t7_catatan'] != '') {
                        $teks = ($row['rev_status'] =='0' ? 'text-danger' : 'text-success');
                        echo '
                        <div style="float:right; width:100%; margin-bottom:10px">
                          <b>Catatan Penetapan : </b><br>
                          <span class="'.$teks.' small" style="white-space:pre"><em>'. $row['t7_catatan'] .'</em><span>
                        </div>';}
                       ?>


                           <div style="float:right; width:100%">
                              <?php
                                $grp = explode(";", $this->session->userdata('idusergroup'));
                                if( in_array('001', $grp) or in_array('002', $grp) or in_array('021', $grp) or in_array('600', $grp) or in_array('601', $grp) or in_array('611', $grp) or in_array('612', $grp) ) {
                                  $t2  ="";$t3=""; $dis = 'display:none;';

                                  if( $tahap <= '2' and $row['t2_status'] >'1' ) {$t2=$dis;$t3 = $dis;}
                                  elseif($tahap <='2' and $row['t2_status'] !='9') {$t3 = $dis;}
                                  else{$t2; $t3;}
                              ?>
                                   <!-- Khusus Puslay dan Admin dan Kanwil  -->
                                   <div class="pull-right" style="cursor: pointer; margin-bottom: 10px; margin-right: 5px">
                                      <span style="<?= $t2 ?>"><b>Turunkan Status Revisi &nbsp; &nbsp;</b></span>
                                      <span style="background: red; color: white; padding: 5px 10px;<?= $t2 ?>" data-toggle="tooltip" title="Tahap 2: Teliti Dokumen" onclick="mund('2','<?= $vrev_id ?>')"> Tahap <b>2</b>&nbsp;</span>&nbsp;
                                      <span style="background: red; color: white; padding: 5px 10px;<?= $t3 ?>" data-toggle="tooltip" title="Tahap 3: Penetapan" onclick="mund('3','<?= $vrev_id ?>')"> Tahap <b>3</b>&nbsp;</span>
                                   </div>
                                   <!-- End -->
                              <?php } ?>
                           </div>
                        </div>
                      </div>
                    </div>
                  </td>
              </tr>
<!-- hiddenRow -->

              <?php } } else { echo '<tr><td colspan="10" class="text-center"><br>Tidak ada data ...</td></tr>'; } ?>

          </tbody>
        </table>
         </div>
         Keterangan : Laporan Revisi Anggaran berdasarkan pilihan range tanggal untuk Revisi yang telah ditetapkan.
         </section>
      </div> <!-- box body -->
    </div>
  </div>
</div>

<script type="text/javascript">
  $('.collapse').on('show.bs.collapse', function () {
  $('.collapse.in').collapse('hide');
  });
</script>

<script type="text/javascript">
  $('#sandbox-container .input-daterange').datepicker({
    format: "dd-mm-yyyy",
    language: "id",
    orientation: "bottom auto",
    autoclose: true,
    todayHighlight: true
  }).on('changeDate', function() {
    var sta = $('#start').val();
    var end = $('#end').val();
    window.location.href = "<?= site_url('Revisi_Kanwil?q=rk4w1l') ?>" +'&start='+ sta +"&end="+ end });
</script>


</script>
<script type="text/javascript">
   var $rows = $('#iGrid tbody tr');
   $('#cari').keyup(function() {
   var val = $.trim($(this).val()).replace(/ +/g, ' ').toLowerCase();

   $rows.show().filter(function() {
        var text = $(this).text().replace(/\s+/g, ' ').toLowerCase();
        return !~text.indexOf(val);
   }).hide();
   });
</script>

<?php
  $grp = explode(";", $this->session->userdata('idusergroup'));
  if( in_array('001', $grp) or in_array('002', $grp) or in_array('021', $grp) or in_array('600', $grp) or in_array('601', $grp) or in_array('611', $grp) or in_array('612', $grp)  ) { ?>
<script type="text/javascript">

  function mund(lvl,rev_id) {
    var result = confirm(" **PERHATIAN** \n Apakah Anda Yakin Menurunkan Status Revisi ke Tahap " + lvl + "???");
    if(result){
     window.location.href = '<?= site_url('Revisi_Kanwil?q=m4nd4r') ?>' +'&lvl='+ lvl + '&rev_id='+ rev_id;

    }
  }

  function excel(){
    var sta = document.getElementById("start").value;
    var end = document.getElementById("end").value;

    // alert(end);

    window.location.href = "<?php echo site_url('Revisi_Kanwil?q=r3p0rtkw1') ?>" +'&sta='+ sta +"&end="+ end;
  }
</script>
<?php } ?>
