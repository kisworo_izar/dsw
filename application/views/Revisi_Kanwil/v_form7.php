
<div class="row" ng-app="app" >
    <div class="col-md-12">
        <div class="box box-widget">
            <div class="box-header with-border">
              <h3 style="margin:0px">F7. Penetapan Revisi</h3>
            </div>
            <form name="myForm" class="form-horizontal" action="<?=site_url('Revisi_Kanwil?q=rk4w1l7c&rev_id='. $rev_id)?>" onsubmit="" method="post" role="form">
                <div class="box-body">
                    <?php $this->load->view("Revisi_Kanwil/v_info"); ?>
                </div>
                <div class="box-footer">
                    <div class="col-sm-12 pull-right">
                        <div class="pull-right">
                        <input type="hidden" name="rev_id"  id="" value="<?php echo $rev_id ?>" >
                            <button type="submit" id="proses" class="btn btn-info">Proses Penetapan Revisi</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
