<!-- Google Fonts -->
<link href="https://fonts.googleapis.com/css?family=Roboto:400,700&amp;subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
<link href="<?=base_url();?>assets/plugins/materialicons/materialicons.css" rel="stylesheet" type="text/css">
<!-- bootstrapValidator -->
<link rel="stylesheet" href="<?=base_url();?>assets/plugins/bootstrapvalidator/css/bootstrapValidator.min.css"/>
<!-- SweetAlert2 -->
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
<!-- Waves Effect Css -->
<link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/plugins/node-waves/waves.css">
<!-- Bootstrap DatePicker Css -->
<link href="<?=base_url();?>assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.css" rel="stylesheet" />
<!-- Bootstrap Select Css -->
<link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/plugins/bootstrap-select/css/bootstrap-select.css">
<!-- Dropify -->
<link href="<?=base_url();?>assets/plugins/dropify/css/dropify.min.css" rel="stylesheet">
<!-- AdminBSB Custom Css -->
<link href="<?=base_url();?>assets/plugins/adminbsb/style.css" rel="stylesheet">

<style type="text/css">
  form .dropify-wrapper p {
    text-align: center;
  }

  .swal2-radio [type="radio"]:not(:checked),
  .swal2-radio [type="radio"]:checked {
    position: unset;
    opacity: 1;
  }

  .form-group .form-line.focused .form-label {
    top: -15px;
  }

  .bs-searchbox .form-control {
    width: 88%;
  }

  .capitalise {
    text-transform:capitalize;
  }

  .demo-choose-skin li {
    font-size: 12px;
    padding: 15px;
  }
</style>

<div class="row">
  <div class="col-md-12">
    <div class="card">
      <div class="header">
        <br>
        <form id="cuti-filter-form" style="margin-bottom: 0px;">
          <div class="row clearfix">
            <div class="col-sm-6">
              <div class="form-group form-float">
                <div class="form-line">
                  <select name="org_id" class="form-control show-tick capitalise selectpicker org_id" data-live-search="true" data-size="5" title="Pilih Unit" >

                  </select>
                  <label class="form-label">Unit</label>
                </div>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="form-group form-float">
                <div class="form-line">
                  <input name="date" type="text" class="form-control datepicker date">
                  <label class="form-label">Tanggal</label>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    <div class="alert alert-info">
      <strong>KETENTUAN!</strong> Maksimal persentase pegawai yang cuti di suatu unit dan/atau level tertentu pada tanggal tertentu sebesar 30%.
    </div>
  </div>
</div>
<div class="row">
  <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
    <div class="row">
      <div class="col-lg-12">
        <div class="info-box-3 hover-expand-effect LoadingOverlay">
          <div class="icon" style="bottom: 10px;">
            <i class="material-icons">brightness_low</i>
          </div>
          <div class="content" style="margin-left: 0px;">
            <div class="text">TOTAL PEGAWAI CUTI</div>
            <div class="number" id="cuti-total"> </div>
          </div>
        </div>
      </div>
      <div class="col-lg-12">
        <div class="card LoadingOverlay">
          <div class="body bg-teal">
            <div class="font-bold m-b--35">PEGAWAI CUTI PER LEVEL</div>
            <ul class="dashboard-stat-list" id="cuti-level">

            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
    <div class="card LoadingOverlay">
      <div class="header">
        <h2>
          PEGAWAI CUTI PER UNIT
        </h2>
      </div>
      <div class="body" style="padding: 0px">
        <div class="table-responsive">
          <table class="table table-hover dashboard-task-infos" style="font-size: 14px; margin-bottom: 0px">
            <tbody id="cuti-unit">

            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- imagesloaded -->
<script src="<?=base_url();?>assets/plugins/imagesloaded/imagesloaded.pkgd.min.js"></script>
<!-- Masonry -->
<script src="<?=base_url();?>assets/plugins/masonry/masonry.pkgd.min.js"></script>
<!-- bootstrapValidator -->
<script type="text/javascript" src="<?=base_url();?>assets/plugins/bootstrapvalidator/js/bootstrapValidator.js"></script>
<script type="text/javascript" src="<?=base_url();?>assets/plugins/bootstrapvalidator/js/language/id_ID.js"></script>
<!-- jquery.serializeObject -->
<script type="text/javascript" src="<?=base_url();?>assets/plugins/jQuery.serializeObject/jquery.serializeObject.min.js"></script>
<!-- Bootstrap Select Css -->
<script type="text/javascript" src="<?=base_url();?>assets/plugins/bootstrap-select/js/bootstrap-select.min.js"></script>
<!-- Slimscroll Plugin Js -->
<script src="<?=base_url();?>assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- JQuery Steps Plugin Js -->
<script src="<?=base_url();?>assets/plugins/jquery-steps/jquery.steps.js"></script>
<!-- Waves Effect Plugin Js -->
<script type="text/javascript" src="<?=base_url();?>assets/plugins/node-waves/waves.js"></script>
<!-- Bootstrap Datepicker Plugin Js -->
<script src="<?=base_url();?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<!-- Dropify -->
<script src="<?=base_url();?>assets/plugins/dropify/js/dropify.min.js"></script>
<!-- jQuery LoadingOverlay -->
<script src="<?=base_url();?>assets/plugins/gasparesganga-jquery-loading-overlay/loadingoverlay.min.js"></script>
<!-- AdminBSB Custom Js -->
<script src="<?=base_url();?>assets/plugins/adminbsb/admin.js"></script>
<!-- <script src="<?=base_url();?>assets/plugins/adminbsb/demo.js"></script> -->

<script type="text/javascript">
  $(function(){

    /* FILTER CUTI FORM */
    $('#cuti-filter-form')
    .find('select')
    .selectpicker()
    .change(function(e) {
      $(this).closest('.form-line').addClass('focused');
      cutidata();
    })
    .end()
    .find('.datepicker')
    .datepicker({
      autoclose: true,
      format: "yyyy-mm-dd",
      language: "id",
    })
    .on('changeDate', function(ev) {
      $(this).closest('.form-line').addClass('focused');
      cutidata();
    })
    .datepicker('setDate', new Date())
    .end()

    /* UNIT REFERENCE */
    $.ajax({
      url: '<?= $this->config->item('api_server') ?>organization?nip=<?= $this->session->userdata('nip') ?>',
      type: 'GET',
      dataType: 'json',
      success: function(result) {
        if (result.status == 200) {
          $('select[name="org_id"]').html(buildGroup([result.values])).selectpicker('refresh');
          $('#cuti-filter-form .org_id').selectpicker('val', result.values.id).trigger('change');
        } else {
          console.log('Gagal load data pegawai');
        }
      }
    })

    /* LOADING */

    $('body').ajaxSend(function(event, jqxhr, settings){
      $('.LoadingOverlay').LoadingOverlay("show");
    });
    $('body').ajaxComplete(function(event, jqxhr, settings){
      $('.LoadingOverlay').LoadingOverlay("hide");
    });

  })

  function cutidata() {
    $.ajax({
      url: "<?= $this->config->item('api_server') ?>cuti/dashboard?" + $('#cuti-filter-form').serialize(),
      type: 'GET',
      dataType: 'json',
      success: function(result) {
        if (result.status == 200) {
          let cutidata = result.values;
          
          /* TOTAL */
          let cutitotal = (cutidata.total.pegawai_total) ? Math.round((cutidata.total.pegawai_cuti / cutidata.total.pegawai_total) * 100) : 0;
          let cutitotalcolor;

          if (cutitotal < 10) cutitotalcolor = 'bg-green';
          else if (cutitotal < 20) cutitotalcolor = 'bg-orange';
          else cutitotalcolor = 'bg-red';
          
          $('#cuti-total').text( cutitotal + '%' );
          $('#cuti-total').parent().parent().addClass(cutitotalcolor);

          /* LEVEL */
          let cutilevelhtml = '';
          for ( let cutilevel of cutidata.level ) {
            let cutileveltotal = (cutilevel.pegawai_cuti / cutilevel.pegawai_total) * 100;

            cutilevelhtml += '<li>\
            ' + cutilevel.name + '\
            <span class="pull-right"><b>' + Math.round(cutileveltotal) + '</b> <small>%</small></span>\
            </li>';
          }
          $('#cuti-level').html(cutilevelhtml);

          /* UNIT */
          let cutiunithtml = '';

          if (cutidata.units.length == 0) {
            cutiunithtml = '<div class="alert alert-info font-italic" style="margin-bottom: 0px;">Tidak ada unit di bawah eselon unit ini</div>';
          }

          for ( let cutiunit of cutidata.units ) {
            let cutiunittotal = (cutiunit.pegawai_total) ? Math.round((cutiunit.pegawai_cuti / cutiunit.pegawai_total) * 100) : 0;
            cutiunittotaltext = cutiunittotal + '% (' + cutiunit.pegawai_cuti + ' dari ' + cutiunit.pegawai_total + ' pegawai)';
            let cutiunitcolor;

            if (cutiunittotal < 10) cutiunitcolor = 'bg-green';
            else if (cutiunittotal < 20) cutiunitcolor = 'bg-orange';
            else cutiunitcolor = 'bg-red';

            cutiunithtml += '<tr>\
            <td style=" padding-left: 20px; ">' + cutiunit.name + '</td>\
            <td>\
            <span class="label ' + cutiunitcolor + ' pull-right">' + cutiunittotaltext + '</span>\
            </td>\
            </tr>';

          }
          $('#cuti-unit').html(cutiunithtml);

        } else {
          console.log('Data Unit Gagal')
        }
      },
      error: function(result) { console.log('Data Unit Error') }
    });
  }

  function buildGroup(items) {
    if (items.length == 0) { return ''; }

    var html = "";

    $.each(items, function(index, item){
      html += '<option value='+item.id+'>'+item.name+'</option>';
      if (item.children) {
        html += buildGroup(item.children);
      } 
    })

    return html;
  }
</script>