<font face="arial, helvetica, sans-serif">

<style>
.vertical_line{margin-top:3px;height:3px; width:700px;background:#000;}
.vertical_line2{margin-top:2px;height:1px; width:700px;background:#000;}
.wrapperss{
    margin-left: 18pt;
    background: white; margin: 0 auto;
    margin-right: 15pt;
}
.table{
  float: left;
}
</style>

<table border="0" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td width="111"><img src="<?=base_url('files/cuti/header.jpg'); ?>" width="96" height="100%" /></td>
<td width="527">
<table>
<tbody>
    <tr>
        <td style="font-size: 13pt;text-align:center"><strong>KEMENTERIAN KEUANGAN REPUBLIK INDONESIA</strong></td>
    </tr>
    <tr>
        <td style="font-size: 11pt;text-align:center"><strong>DIREKTORAT JENDERAL ANGGARAN</strong></td>
    </tr>
    <tr>
        <td style="font-size: 11pt;text-align:center"><strong>SEKRETARIAT DIREKTORAT JENDERAL</strong></td>
    </tr>
    <tr style="margin-top:100px;">
      <td></td>
    </tr>
    <tr>
        <td style="font-size: 7pt;text-align:center">GEDUNG SUTIKNO SLAMET  LANTAI 11, JALAN DR. WAHIDIN NO. 1, JAKARTA PUSAT 10710, KOTAK POS 2435</td>
    </tr>
    <tr>
        <td style="font-size: 7pt;text-align:center">TELEPON (021) 3866117: FAKSIMILE (021) 3505118; SITUS www.anggaran.depkeu.go.id</td>
    </tr>
</tbody>
</table>

</td>
</tr>
</tbody>
</table>
</font>

<div class="vertical_line">
</div>
<div class="vertical_line2">
</div>
&nbsp;
<font face="arial, helvetica, sans-serif" style="font-size: 12pt;">
<table style="text-align:center;margin-left:180px;">
    <tr>
        <td><strong>SURAT IZIN CUTI TAHUNAN</strong></td>
    </tr>
    <tr>
        <td>Nomor SI- <?php echo $cuti['nosurat']?>&nbsp;&nbsp;&nbsp;/AG/UP.5/2016</td>
    </tr>
</table>
</font>


<div class="wrapperss">
<p style="text-align: justify;font-size: 12pt;"><font face="arial,helvetica, sans-serif">1.   Diberikan cuti tahunan untuk tahun 2016  kepada Pegawai Negeri Sipil :</font></p>

<font face="arial,helvetica, sans-serif">
<table border="0" cellpadding="1" cellspacing="1" style="width:600px; margin-left:12px;font-size: 12pt;">
  <tbody>
    <tr>
      <td width="200px" style="text-align: justify;">Nama</td>
      <td width=" 10px" style="text-align: justify;">:</td>
      <td style="text-align: justify;"><?php echo strtoupper($cuti['nmuser']);?></td>
    </tr>
    <tr>
      <td width="50px" style="text-align: justify;">NIP</td>
      <td width=" 2px" style="text-align: justify;">:</td>
      <td style="text-align: justify;"><?php echo $cuti['nip'] ?></td>
    </tr>
    <tr>
      <td width="200px" style="text-align: justify;">Pangkat/ Gol.ruang</td>
      <td width=" 2px" style="text-align: justify;">:</td>
      <td style="text-align: justify;"><?php echo ucwords(strtolower($cuti['pangkat']));?>&nbsp;/ &nbsp;(<?php echo $cuti['golongan'] ?>)</td>
    </tr>
    <tr>
      <td width="50px" style="text-align: justify;">Jabatan</td>
      <td width=" 2px" style="text-align: justify;">:</td>
      <td style="text-align: justify;"><?php echo ucwords(strtolower($cuti['jabatan']));?></td>
    </tr>
    <tr>
      <td width="50px" style="text-align: justify;">Unit Organisasi</td>
      <td width=" 2px" style="text-align: justify;">:</td>
      <td style="text-align: justify;"><?php echo ucwords(strtolower($cuti['nmso'])); ?></td>
    </tr>
  </tbody>
</table>
</font>

<div style="margin-top:10px;">
  <p style="margin-left:12px;text-align: justify; font-size:12pt;"><font face="arial,helvetica, sans-serif">Selama <?php echo ABS($cuti['jumlah'])?> hari kerja, terhitung mulai tanggal <?php echo $this->fc->idtgl($cuti['tglawal'],'tglfull')?> sampai dengan tanggal <?php echo $this->fc->idtgl($cuti['tglakhir'],'tglfull')?> dengan ketentuan sebagai berikut:
  </font>
  </p>
</div>

<font face="arial,helvetica, sans-serif">
<table style="text-align:justify;font-size:12pt;margin-left:10px;">
<tbody>
    <tr>
      <td width="10px" style="text-align: justify; vertical-align:top">a.</td>
      <td width="2px" style="text-align: justify;">&nbsp;</td>
      <td>Sebelum menjalankan cuti tahunan wajib menyerahkan pekerjaannya kepada atasan langsungnya atau pejabat yang ditunjuk;</td>
    </tr>
    <tr class="rata">
        <td width="10px" style="text-align: justify;vertical-align:top">b.</td>
        <td width="2px" style="text-align: justify;"nbsp;</td>
        <td>Setelah selesai menjalankan cuti tahunan wajib melaporkan diri kepada atasan langsungnya dan bekerja kembali sebagaimana biasa.</td>
    </tr>
</tbody>
</table>
</font>
&nbsp;

<font face="arial,helvetica, sans-serif">
<table style="text-align: justify; font-size:12pt;">
  <tr>
    <td width="2px" style="text-align: justify;">2.</td>
    <td width="400px" style="text-align: justify;">Demikian surat izin cuti tahunan ini dibuat untuk dapat dipergunakan sebagaimana mestinya.</td>
  </tr>
</table>
</font>
&nbsp;

<font face="arial,helvetica, sans-serif">
  <table style="margin-left:280px;font-size: 12pt; height: 63px;" border="0">
    <tbody>
    <tr>
      <td>&nbsp;</td>
      <td >Jakarta,&nbsp;&nbsp;<?php echo $this->fc->idtgl($cuti['tglsurat'],'tglfull') ?></td>
    </tr>
    <tr>
      <td>a.n.</td>
      <td>Direktur Jenderal Anggaran</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>Kepala Bagian SDM</td>
    </tr>
    </tbody>
  </table>
</font>
<p>&nbsp;</p>
<p>&nbsp;</p>


<font face="arial,helvetica, sans-serif">
<table style="margin-left:300px;text-align:justify;font-size:12pt;">
    <tbody>
        <tr>
            <td style="text-align: justify;">Triana Ambarsari</td>
        </tr>
        <tr>
            <td style="text-align: justify;">NIP 19710216 199603 2 001</td>
        </tr>
    </tbody>
</table>
</font>
<p>&nbsp;</p>
<p>&nbsp;</p>
</div>

<font face="arial, helvetica, sans-serif">
<table style="text-align:justify;font-size:12pt;">
    <tr>
        <td>Tembusan&nbsp;:</td>
    </tr>
    <tr>
        <td>1. <?php echo ucwords(strtolower($cuti['nmso'])); ?></td>
    </tr>
    <tr>
        <td>2. Sekretaris Direktorat Jenderal Anggaran</td>
    </tr>
    <tr>
        <td>&nbsp; &nbsp;Up. Petugas Pengelola Administrasi Belanja Pegawai&nbsp;</td>
    </tr>
</table>
</font>