<link href="<?=base_url('assets/plugins/select2/select2.min.css');?>" rel="stylesheet" type="text/css" />
<script src="<?=base_url('assets/plugins/select2/select2.full.min.js'); ?>" type="text/javascript"></script>

<link href="<?=base_url('assets/bootstrap/css/bootstrap-datetimepicker.min.css');?>" rel="stylesheet" type="text/css" />
<script src="<?=base_url('assets/plugins/datepicker/moment.min.js');?>" type="text/javascript"></script>
<script src="<?=base_url('assets/plugins/datepicker/id.js');?>" type="text/javascript"></script>
<script src="<?=base_url('assets/bootstrap/js/bootstrap-datetimepicker.min.js');?>" type="text/javascript"></script>
<script src="<?=base_url('assets/bootstrap/js/jQuery.js');?>" type="text/javascript"></script>
<script src="<?=base_url('assets/bootstrap/js/bootstrap.min.js');?>" type="text/javascript"></script>
<script src="<?=base_url('assets/bootstrap/js/moment.js');?>" type="text/javascript"></script>
<script src="<?=base_url('assets/bootstrap/js/bootstrap-datetimepicker.min.js');?>" type="text/javascript"></script>
<script src="<?=base_url('assets/plugins/datepicker/id.js');?>" type="text/javascript"></script>

<script type="text/javascript">
function handleClick( nil ) {
        document.getElementById("jnscuti").value = nil;
        if (nil==1) {
             document.getElementById('lbl_cutiambil').innerText = 'Lama Cuti Ambil';
             document.getElementById('tgl_cuti').innerText = 'Tanggal Cuti Ambil';
             
        }

        if (nil==2) {
            document.getElementById('lbl_cutiambil').innerText = 'Lama Cuti Panggil';
            document.getElementById('tgl_cuti').innerText = 'Tanggal Cuti Panggil';

        }
    }
</script>

<?php 
$udisabled= '';
if ($ruh=='Hapus') {
    $udisabled = 'disabled=""';
}
?>

<div class="row">
    <div class="col-md-7">
        <div class="box box-widget">
            <div class="box-header with-border">
                <h3 class="box-title"><?php echo $ruh ?> Detail Cuti</h3>
            </div>
            <form name="myForm" class="form-horizontal" action="<?php echo site_url('cuti/crud') ?>" method="post" role="form">
                <div class="box-body">

                    <div class="form-group"> 
                        <label class="col-sm-3 control-label">Jenis Cuti</label>
                        <div class="col-sm-3">
                            <label style="margin: 7px 0px 0px 0px">
                                <input type="hidden" id="jnscuti" value="1" />
                                <input type="radio" name="jnscuti" id="optRadio1" value="1" checked onchange="handleClick(this.value);" > &nbsp;&nbsp;Cuti Ambil
                            </label>
                        </div>

                        <div class="col-sm-3">
                            <label style="margin: 7px 0px 0px 0px">
                                <input  type="radio" name="jnscuti" id="optRadio2" value="2" onchange="handleClick(this.value);" > &nbsp;&nbsp;Cuti Panggil
                            </label>
                        </div>
                    </div>


                    <div class="form-group" style="display:block;">
                        <label class="col-sm-3 control-label">Nama Pegawai</label>
                            <div class="col-sm-7">
                                <input name="ruh" type="hidden" value="<?php echo $ruh ?>" />
                                <input name="idcuti" type="hidden" value="<?php echo $table['idcuti'] ?>" /> 
                                <input style="display:block;" name="iduser" type="hidden" value="<?php echo $table['iduser'] ?>" />
                                <input  type="text" name="nmuser" id="nmuser" class="form-control"  value="<?php echo $table['nmuser'] ?>" placeholder="Nama Pegawai" >
                            </div>
                    </div>  

                    <div class="form-group">
                        <label class="col-sm-3 control-label">Nomor Surat Cuti</label>
                            <div class="col-sm-7">
                                <input type="text" name="nosurat" id="nosurat" class="form-control" value="<?php echo $table['nosurat'] ?>" placeholder="Nomor Surat" >
                            </div>
                    </div>

                    <div class="form-group">

                          <label id="tgl_cuti" class="col-sm-3 control-label">Tanggal Cuti Ambil</label>

                            <div class="col-sm-4">
                                <div class="input-group date tglawal" id="tglawal">
                                    <input  type="text" name="tglawal" id="tglawal" class="form-control"  value="<?php echo $table['tglawal']; ?>" placeholder="Tanggal Awal Cuti..."required >
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar text-primary"></i>
                                    </span>
                                </div>
                            </div>


                        <div class="col-sm-3">
                            <div class="input-group date tglakhir" id="tglakhir">
                                <input  name="tglakhir" id="tglakhir" type="text" class="form-control" value="<?php echo $table['tglakhir']?>" placeholder="Tanggal Akhir Cuti..." >
                                <span class="input-group-addon">
                                    <i class="fa fa-calendar text-primary"></i>
                                </span>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label id="lbl_cutiambil" class="col-sm-3 control-label">Lama Cuti Ambil</label>
                        <div class="col-sm-7">
                            <input  name="jumlah" id="jumlah" class="form-control" type="text"  value="<?php echo $table['jumlah']?>" placeholder="Jumlah Hari Cuti Ambil. . .">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">Tanggal Surat Cuti</label>
                        <div class="col-sm-7">
                            <div class="input-group date tglsurat" id="tglsurat">
                                <input  type="text" name="tglsurat" class="form-control" id="tglsurat" value="<?php echo $table['tglsurat']?>" placeholder="Tanggal Surat Cuti">
                                <span class="input-group-addon">
                                    <i class="fa fa-calendar text-primary"></i>
                                </span>
                            </div>
                        </div>
                    </div>     

                    <div class="form-group">
                        <label class="col-sm-3 control-label">Keterangan</label>
                        <div class="col-sm-7">
                         <textarea  name="keterangan" id="keterangan" type="text" class="form-control" value="<?php echo $table['keterangan']?>" rows="5" placeholder="Keterangan. . ."></textarea>                    
                         </div>
                    </div>

                    <div class="box-footer">
                        <button type="submit" id="simpan" class="btn btn-info pull-right"><?php echo $ruh ?></button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div> <!-- /col-sm-6 -->

    <script type="text/javascript">
            $(function () {
               $('#tglawal').datetimepicker({
                format : 'YYYY-MM-DD',
               });
                
               $('#tglakhir').datetimepicker({
               format : 'YYYY-MM-DD',
               });
                
               $('#tglsurat').datetimepicker({
               format : 'YYYY-MM-DD',
               });
            });
    </script>