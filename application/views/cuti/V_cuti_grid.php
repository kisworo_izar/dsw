<!--datetimepicker-->
<script src="<?=base_url('assets/plugins/jQuery/jquery.validate.min.js');?>" type="text/javascript"></script>
<script src="<?=base_url('assets/plugins/datepicker/bootstrap-datepicker.js');?>" type="text/javascript"></script>
<!--datetimepicker-->
<script src="<?=base_url('assets/plugins/jQueryUI/jquery-ui.1.11.4.min.js'); ?>" type="text/javascript"></script>
<script src="<?=base_url('assets/plugins/jQueryUI/bootbox.min.js'); ?>"></script>
<style media="screen">
th { text-align: center; }
  thead tr { color: #fff; background: #00acd6; }

  #iGrid .ui-selecting { background: #FECA40; }
  #iGrid .ui-selected { background: #F39814; color: white; }

  .child    { background: #ECF0F5;}

   }
  .datetimepicker { z-index:1151 !important; }

}
</style>

<div class="row">
	<div class="col-md-10">
		<div class="box box-widget">
			<div class="box-header with border">
				<div class="wel wel-sm">
					<form id="iForm" class="form-inline" role="form" action="<?php echo site_url("cuti/caricuti") ?>" method="post">
						  <div class="form-group pull-right">
        					<div class="input-group">
                      <input class="form-control" name="cari" type="text" placeholder="Pencarian..." value="<?php echo $this->session->userdata('cari') ?>">
                    			<span class="input-group-btn">
                    					<button type="submit" name="search" value="search" id="search-btn" class="btn btn-info btn-flat"><i class="fa fa-search"></i></button>
                    				</span>
                    					<span class="input-group-btn">
                    						<button type="submit" name="search" value="clear" class="btn btn-info btn-flat"><i class="fa fa-times-circle"></i></button>
                        			</span>
                                  <span class="input-group-btn">
                                      <button title="Cetak Surat Cuti" type="button" id="cetak" class="btn btn-info btn-flat"><i class="fa fa-file-pdf-o"></i></button>
                                  </span>
                        			</div>
                        		</div>

                        		<div class="form-group">
                        			<div class="form-group" style="margin-bottom:0px">
                                        <span class="input-group-btn">
                                            <input  type="hidden" name="iduser" id="iduser" value="">
                                            <input  type="hidden" name="idcuti" id="idcuti" value="">
                                            <button type="button" id="addrow"   class="btn btn-warning" onclick="window.location.href='<?php echo site_url('cuti/rekam/r') ?>'">Rekam</button>
                                            <button type="button" id="editrow" class="btn btn-warning">Ubah</button>
                                            <button type="button" id="delrow"  class="btn btn-warning">Hapus</button>
                                        </span>
                                    </div>
                           		</div>
             	</form>

                                <div class="box-body">
                                    <div class="col-md-12">
                                        <table id="iGrid" class="table table-hover table-responsive table-bordered">
                                            <thead>
                                                <tr>
                                                    <td>Nama Pegawai</td>
                                                    <td>NIP</td>  
                                                    <td>Unit</td>
                                                    <td>Sisa Cuti 2015</td>
                                                    <td>Cuti 2016</td>
                                                    <!-- <td>Cuti Bersama 2016</td>  -->
                                                    <td>Cuti Dapat Diambil 2016</td>
                                                </tr>
                                            </thead>

                                            <tbody>
                                                <?php foreach ($table as $row) { ?>
                                                    <tr id="<?php echo $row['tahun'].'_'.$row['iduser'] ?>" onclick="ajax_layer(this.id)">
                                                        <td id="<?php echo $row['iduser'] ?>"><?php echo $row['nmuser'] ?></td>
                                                        <td><?php echo $row['nip'] ?></td>
                                                        <td><?php echo $row['nmso1'] ?></td>
                                                        <td><?php echo $row['cuti_lalu']; ?></td>
                                                        <td><?php echo $row['cuti_ini']; ?></td> 
                                                        <!-- <td><?php echo $row['cuti_bersama'] ?></td>  -->
                                                        <td><?php echo $row['cuti_lalu']+$row['cuti_ini']-$row['cuti_bersama']-$row['cuti_ambil'] ?></td>
                                                    </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>

                                <div class="box-footer clearfix">
                                    <?php echo $this->pagination->create_links(); ?>
                                </div>

                                </div>
                </div>
            </div>
        </div>
    </div>


      <div class="col-sm-2">
        <div id="carousel" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
                <div class="item active">
                    <img src="<?=base_url('files/cuti/kalenderLibur2016.jpg'); ?>" type="image">
                </div>
                <div class="item">
                    <img src="<?=base_url('files/cuti/libur2017.jpg'); ?> " type="image">
                </div>
            </div>
        </div> 
    </div> <!-- /col-sm-6 -->

</div>                                


<script type="text/javascript">
	$('.pagination').addClass('pagination-sm no-margin pull-right');
</script>

<script type="text/javascript">
    $(function() {

        // Tandai ROW yang dipilih dalam iGRID
        $( "#iGrid tbody" ).selectable({
            filter: ":not(td)",
                create: function( e, ui ) {
                iGridLink( $() );
            },
            selected: function( e, ui ) {
                var widget = $(this).find('.ui-selected');
                $(ui.unselected).addClass("info");
                iGridLink( widget );
            },
            unselected: function( e, ui ) {
                $(ui.unselected).removeClass("info");
                var widget = $(this).find('.ui-selected');
                iGridLink( widget );
            }
        });

        // ONCLICK untuk Rekam / Ubah / Hapus
        $("#addrow").on("click", function() {
            window.location.href = "<?php echo site_url('cuti/rekam/r') ?>"  +"/"+ document.getElementById("iduser").value;
        });
        $("#editrow").on("click", function() {
            if (document.getElementById("idcuti").value != 'undefined') {
           window.location.href = "<?php echo site_url('cuti/rekam/u') ?>" +"/"+ document.getElementById("idcuti").value;
           }
        });
        $("#delrow").on("click", function() {
            if (document.getElementById("idcuti").value != 'undefined') {
            window.location.href = "<?php echo site_url('cuti/rekam/h') ?>" +"/"+ document.getElementById("idcuti").value;
            }
        });
        $("#cetak").on("click", function() {
            if (document.getElementById("idcuti").value != 'undefined') {
            window.location.href = "<?php echo site_url('cuti/pdf') ?>" +"/"+ document.getElementById("idcuti").value;
            }
        });

        // Ambil Nilai ROW pada iGRID yang dipilih per-ID sesuai kolom CHILD
        function iGridLink( $selectees ) {
            selected = $.makeArray( $selectees.filter( ".ui-selected" ) );
                selected.reduce( function( a, b ) {
                    document.getElementById("iduser").value = $(b).children( "td:nth-child(1)" ).attr('id');
                    document.getElementById("idcuti").value = $(b).children( "td:nth-child(2)" ).attr('id');
                }, 0 
            );
         }

    }); 
</script>

<script type="text/javascript">
  function ajax_layer( idkey ) {
    $.ajax({
      url : "<?php echo site_url('cuti/ajax_layer') ?>",
      type: "POST",
      data: { 'idkey': idkey },
    })
      .done( function (msg) {
        $('table tr.child').remove();
        $('#'+idkey).after(msg);
      })
  }
</script>