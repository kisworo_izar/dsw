<div class="row">
    <div class="col-md-12">
        <div class="box box-widget">
            <div class="box-body with-border">
                <form role="form" action="<?php echo site_url("paket/cari") ?>" method="post">
                  <div class="input-group" style="margin-bottom:10px">
                    <input type="hidden" name="nmfunction" value="dash">
                    <input type="text" name="cari" class="form-control" placeholder="Pencarian..." value="<?php echo $this->session->userdata('cari') ?>">
                        <span class="input-group-btn">
                          <button type="submit" name="search" value="search" id="search-btn" class="btn btn-default btn-flat"><i class="fa fa-search"></i>
                          </button>
                        </span>
                        <span class="input-group-btn">
                            <button type="submit" name="search" value="clear" class="btn btn-default btn-flat"><i class="fa fa-times-circle"></i></button>
                        </span>
                  </div>
                </form>

              <div class="box-body table-responsive no-padding">
                <table class="table table-hover table-bordered">
                    <tr>
                        <th>No</th>
                        <th>Nama</th>
                        <th>Nip</th>
                        <th>Sisa Cuti Tahun Lalu</th>
                        <th>Hak Cuti Tahun Ini</th>
                        <th class="hidden-xs">Keterangan</th>
                    </tr>

                <?php
                if ($nilai) {
                    foreach ($nilai as $row) { ?>
                        <tr>
                            <!-- <td class="text-bold"># <?php echo $row['idpaket'] ?></td>
                            <td><?php echo $this->fc->idtgl( $row['tglrekam'], 'tgljam' ) ?></td>
                            <td><?php echo $row['nmterima'] ?></td>
                            <td class="hidden-xs"><?php echo $row['keterangan'] ?></td> -->
                        </tr>
                    <?php
                    }
                } else { ?>
                        <tr><td colspan="4" class="text-danger text-center"> Data tersebut tidak ditemukan ...</td></tr>
                <?php } ?>

                </table>
              </div>
            </div>

            <div class="box-footer clearfix">
                <?php echo $this->pagination->create_links(); ?>
            </div>
        </div>
    </div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="<?=base_url('assets/plugins/morris/morris.min.js');?>" type="text/javascript"></script>

