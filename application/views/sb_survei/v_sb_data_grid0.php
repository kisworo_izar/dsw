<link href="<?=base_url(); ?>assets/plugins/uploadfile/fileinput.css" rel="stylesheet">
<script src="<?=base_url();?>assets/plugins/uploadfile/fileinput.min.js" type="text/javascript"></script>
<script src="<?=base_url();?>assets/plugins/jQuery-Mask/dist/jquery.mask.min.js" type="text/javascript"></script>
<script src="<?=base_url();?>assets/plugins/jQuery/jquery.maskMoney.min.js" type="text/javascript"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<style media="screen">
  th{text-align: center;background: #E7E9EE;color:#34495e} td a {display:block;width:100%;}
  .calculated-width {width: -moz-calc(100% - 50px);width: -webkit-calc(100% - 50px);width: calc(100% - 50px);}​
  A:link,A:visited,A:active,A:hover {text-decoration: none; color: white;}
  .table-bordered > thead > tr > th,
  .table-bordered > thead > tr > td {border-bottom: 0px solid #E7E9EE;}
  .table-bordered > tbody > tr > td {padding:3px}
  .tbl_h {text-align: center; font-weight: bold; border: solid 1px #9ECEC5}
  .dt {padding:0px 5px;} .hiddenRow {padding: 0 !important;}
  .info{border-top:solid 0px #E7E9EE;padding:0px;margin:0px}
  .x { display: inline-block; width: 650px; }
  .p3 {padding:3px} .p1 {padding:3px; border-right:solid 0px #9ECEC5}
  .w1 {width:25%} .w2 {width:12%} .w3 {width:15px}
  .hijau{background: #27ae60; color:white} .biru{background: #3498db; color:white}
  .abu{background: white; color:#bdc3c7} .merah{background: #e74c3c; color:white}
  .green{background: white; color: green}
  section {position: relative;} section.positioned {position: absolute;top:10px;left:10px;}
  .container {overflow-y: auto;padding: 0px;padding-top: 14px;width: 100%}
  table {border-spacing: 1;width:100%;}
  th {height: 0;line-height: 0;padding-top: 0;padding-bottom: 0;color: #000;border: none;white-space: nowrap;}
  th div{position: absolute;background:#E7E9EE;color:black;border-bottom: solid #E7E9EE 0px;border-top: solid 0px #E7E9EE; padding: 6px 0px;top: 0;line-height: normal;} input {width: 100%; border: 0px!important}
  input:read-only {
    background-color: #bdc3c7;
  }
</style>

<div class="row" ng-app="app" >
  <div class="col-md-12">
    <div class="box box-widget">
      <div class="box-header with-border">
         <div class="span5 col-md-8 pull-left" style="border:0px; padding:0px;">
           <h4 style="margin:6px 0px; color:#337AB7 !important ">Data Hasil Survei </h4>
         </div>
         <div class="span5 col-md-4 pull-right" id="sandbox-container" style="border:0px; padding:0px;">
           <button type="button" class="btn btn-sm btn-primary pull-right" data-toggle="modal" data-target="#myModal2">Kirim Data Survei</button>
         </div>
      </div>
      <div class="box-body">
        <section class="">
          <div class="container">
    				<table id="iGrid" class="table table-hover table-bordered">
            <input type="hidden" id="kdlokasi" value="<?php echo $this->session->userdata('kdso') ?>">
    					<thead>
                <tr>
                  <th style="width:50px;padding-left:0px">
                    <div style="width:50px;text-align:center;border-left:solid 1px #E7E9EE">ID</div></th>
                  <th colspan="3"style="padding:0px;padding-left:0px">
                    <div class="calculated-width" style="text-align:left;padding-left:2px">Satuan Biaya</div></th>
                </tr>
    					</thead>
    			    <tbody style="margin:10px">

                <?php foreach ($sb_survei_data as $key=>$row) {
                    $icon       = ''; if ($row['kdisi'] !='0') { $icon='hijau'; }
                    $readonly   = ''; if ($row['kdisi'] !='0') $readonly = 'readonly' ;
                    $iditem     = substr($row['kditem'],0,3);
                    $nmitem     = $row['uraian'];
                    $stsdata    = ''; if ($row['kdcheck']=='1') $stsdata = 'green';
                ?>

                <?php if (substr($row['kditem'],3,12) == '000000000000' && $row['kdinput'] == '0') { ?>
                  <tr style="margin:0px" id="<?php $iditem ?>">
                    <td class="text-bold text-primary text-center" data-toggle="collapse" data-target=".<?php echo $iditem ?>">
                      <?= $iditem ?></td>
                    <td style="border:solid 0px;border-bottom:solid 0px #F4F4F4" data-toggle="collapse" data-target=".<?php echo $iditem ?>">
                      <div><?= $nmitem ?></div>
                    </td>
                    
                    <td style="padding:0px; width:30px" class="text-center">
                    <?php if ($row['file'] !='') { ?>
                      <a title="File Upload <?=$nmitem?>" href="<?php site_url() ?>/files/sb_survei/2021/<?=$row['kdlokasi'] ?>/<?=$row['kditem'] ?>_<?=$row['file']?>"><i class="fa fa-file-pdf-o text-info"></i></a>
                      <?php } ?>
                    </td>
                    
                    <td style="padding:0px; width:30px" class="text-center">
                      <i class="fa fa-lg fa-upload Keterangan" type="button" title="Keterangan dan Upload File" id="<?= 'ket_'.$row['kditem'] ?>" style="width: 100%" data-list="<?= $row['kditem'] .'#'. $row['uraian'].'#'. $row['r1_keterangan']?> " data-toggle="modal" data-target="#myModal"></i>
                    </td>
                    <td style="padding:0px; width:30px" class="text-center">
                      <i class="fa fa-lg fa-check <?php echo  $stsdata; ?>" data-toggle="tooltip" title="Status Data"></i>
                    </td>
                  </tr>
                <?php } else { ?>

                  <tr>
                    <td class="hiddenRow" colspan="8" style="border:solid 0px">
                      <div class="small collapse info <?php echo $iditem ?>" style="padding:0px 7px;background:#FDFDFD">
                        <table border="1" style="padding:3px; margin:1px 0px">
                          <?php if (substr($row['kditem'],6,9) == '000000000' && $row['kdinput'] == '0') { ?>
                          <tr class="dt" style="border: solid 1px white; padding-bottom:0px">
                            <td colspan="10" class="p1 text-bold" style="padding:0px"><?php echo $nmitem ?></td>
                          </tr>

                          <?php } elseif(substr($row['kditem'],9,6) == '000000' && $row['kdinput'] == '0') { ?>
                          <tr class="dt">
                            <td colspan="10" class="p1" style="border: solid 1px white; padding:0px">
                              <?php echo $nmitem ?></td>
                          </tr>
                          <?php } elseif($row['kdinput'] == '1') { ?>
                            <tr class="dt" style="border: solid 1px #9ECEC5">
                              <td class="p1 w1"><?php echo $nmitem ?></td>
                              <td class="p1 w2"><input id="<?= 'inp1_'.$row['kditem'] ?>" type="text" placeholder="Responden 1 ..." value="<?php echo $row['r1_nama'] ?>" <?php echo $readonly ?> />
                              </td>
                              <td class="p1 w2"><input id="<?= 'inp2_'.$row['kditem'] ?>"  style="text-align:right" type="text" placeholder="... Nilai 1" value="<?php echo $row['r1_harga']?>" <?php echo $readonly ?> />

                              </td>
                              <td class="p1 w2"><input id="<?= 'inp3_'.$row['kditem']?>" type="text" placeholder="Responden 2 ..." value="<?php echo $row['r2_nama'] ?>" <?php echo $readonly ?> /></td>
                              <td class="p1 w2"><input id="<?= 'inp4_'.$row['kditem']?>" style="text-align:right" type="text" placeholder="... Nilai 2" value="<?php echo $row['r2_harga']  ?>" <?php echo $readonly ?> /></td>
                              <td class="p1 w2"><input id="<?= 'inp5_'.$row['kditem']?>" type="text" placeholder="Responden 3 ..." value="<?php echo $row['r3_nama'] ?>" <?php echo $readonly ?> /></td>
                              <td class="p1 w2"><input id="<?= 'inp6_'.$row['kditem']?>" style="text-align:right" type="text" placeholder="... Nilai 3" value="<?php echo $row['r3_harga'] ?>" <?php echo $readonly ?> /></td>
                              <td class="text-center w3"><i style="padding: 5px" type="submit" class="fa fa-floppy-o text-blue" onclick="aksi('<?= $row['kditem'] ?>')"></i></td>
                              <td id="<?= 'inp7_'.$row['kditem']?>" class="text-center w3">
                                <i style="padding:5px" type="submit" name="jnsverif" onclick="verify('<?= $row['kditem'] ?>','<?= $row['kdisi'] ?>')" class="fa fa-pencil text-blue"></i>
                              </td>
                              <td id="<?= 'inp8_'.$row['kditem']?>" class="text-center w3">
                                <i style="padding:5px" type="submit" onclick="flag('<?= $row['kditem'] ?>','<?= $row['kdisi'] ?>')" class="fa fa-remove text-red"></i>
                              </td>
                              <td class="text-center w3 keterangansubitem">
                                <i style="padding: 5px" type="button" data-toggle="modal" data-target="#Modalkomen" data-list="<?= $row['kditem'] .'#'. $row['r2_keterangan']?>" class="fa fa-comments-o text-blue" onclick="parsenil('<?= $row['kditem'] ?>','<?= $row['r2_keterangan']?>')"></i>
                              </td>
                              <td id="<?= 'inp9_'.$row['kditem']?>" class="small text-center <?php echo $icon; ?> " >&nbsp;&nbsp;</td>
                             </tr>
                          <?php } ?>
                        </table>
                      </div>
                    </td>
                  </tr>

                  <?php }  ?>
              <?php }  ?>

    					</tbody>
    				</table>
          </div>
        </section>
      </div>
    </div>
  </div>
</div>


<!-- Modal bagian Upload file di dalam Grid -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Isikan Keterangan</h4>
      </div>
      <div class="modal-body form-group">
        <label id="ModalLabel"></label>
        <input id="ModalItem" type="hidden">
        <textarea id="ModalInput" type="text" class="form-control" style="border: solid 0.5px!important;"></textarea>
        <input id="nmfile" name="nmfile" type="hidden">
      </div>
      <div class="modal-body form-group">
        <label>Upload File</label>
          <div style="padding:0px">
               <input id="UploadFile" name="UploadFile" type="file" class="file">
          </div>
      </div>
      <div class="modal-footer">
        <button id="Save" class="btn btn-info" data-dismiss="modal" onclick="simpan()">Simpan</button>
        <button id="Cancel" class="btn btn-danger" data-dismiss="modal">Batal</button>
      </div>
    </div>
  </div>
</div>
<!--End of Modal Bagian Upload File dalam Grid-->

<!--Modal Bagian Kirim data Survei-->

<div id="myModal2" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body form-group">
      <input id="nmfile2" name="nmfile2" type="hidden">
        <label>Upload Surat Pengantar dan Kirim Data Survei</label>
          <div style="padding:0px">
               <input id="UploadFile2" name="UploadFile2" type="file" class="file">
          </div>
      </div>
      <div class="modal-footer">
        <button class="btn btn-info" data-dismiss="modal" onclick="kirim()">Kirim Data Survei</button>
        <button class="btn btn-danger" data-dismiss="modal">Batal</button>
      </div>
    </div>
  </div>
</div>
<!--End of Modal Bagian Kirim data Survei-->

<!--Modal Bagian Keterangan Per Subitem-->
<div id="Modalkomen" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body form-group">
        <input id="ModalSubItem" type="hidden">
          <div class="box-body">
            <div class="form-group">
              <label class="col-sm-2 control-label">Keterangan</label>
              <div class="col-sm-10">
                <textarea class="form-control" type="text" id="ModalSubInput" name="ketsubitem"></textarea>
              </div>
            </div>
          </div>
      </div>
      <div class="modal-footer">
        <button class="btn btn-sm btn-info pull-right" data-dismiss="modal" onclick="saveketerangansubitem()">Simpan</button>
        <button class="btn btn-sm btn-danger pull-left" data-dismiss="modal">Batal</button>
      </div>
    </div>
  </div>
</div>
<!--End of Modal Bagian Keterangan Per Subitem-->



<script type="text/javascript">
$(function() {
    $('.money').maskMoney();
  })

  $('.collapse').on('show.bs.collapse', function () {
    $('.collapse.in').collapse('hide');
  });

  $('.Keterangan').on('click', function() {
    var arr = $(this).data('list').split("#");
    $('#ModalItem').val(arr[0]);
    $('#ModalLabel').html('Keterangan Item : '+arr[1]);
    $('#ModalInput').val(arr[2]);
    $('#nmfile').val(arr[3]);
  });

  function simpan() {
    var kditem    = $('#ModalItem').val();
    var kdlokasi  = $('#kdlokasi').val();
    var arr       = $('#ket_'+kditem).data('list').split("#");
    var isian     = 'kditem='+ $('#ModalItem').val() +'&r1_keterangan='+ $('#ModalInput').val() + '&file='+ $('#nmfile').val()+ '&kdlokasi='+ kdlokasi;
    $.ajax({
      url  : "<?= site_url('sb_survei/crud_survei_data_keterangan') ?>",
      type : 'POST',
      data : isian
    }).done(function(){
      arr[2] = $('#ModalInput').val();
      $('#ket_'+kditem).data('list', arr.join('#'));
      $('#ModalInput').val('');
      swal({
      title:"Upload File dan Keterangan",
      text: "File dan Keterangan anda Tersimpan!",
      timer: 2000,
      showConfirmButton: false
    });
  });
  }

  function aksi( id ) {
    var r1_nama   = $('#inp1_'+id).val(),
        r1_harga  = $('#inp2_'+id).val(),
        r2_nama   = $('#inp3_'+id).val(),
        r2_harga  = $('#inp4_'+id).val(),
        r3_nama   = $('#inp5_'+id).val(),
        r3_harga  = $('#inp6_'+id).val();
        check     = $('#inp9_'+id).val();
        kdlokasi  = $('#kdlokasi').val();

    var isian = 'kditem='+id +'&r1_nama='+r1_nama +'&r1_harga='+r1_harga+'&r2_nama='+r2_nama+'&r2_harga='+r2_harga+'&r3_nama='+r3_nama+'&r3_harga='+r3_harga+'&check='+check+'&kdlokasi='+kdlokasi;
    $.ajax({
      url : "<?= site_url('sb_survei/crud_survei_data') ?>",
      type: "POST",
      data: isian
    }).done(function(){
      $('#inp9_'+id).addClass("hijau");
      $('#inp1_'+id).attr("readonly", true);
      $('#inp2_'+id).attr("readonly", true);
      $('#inp3_'+id).attr("readonly", true);
      $('#inp4_'+id).attr("readonly", true);
      $('#inp5_'+id).attr("readonly", true);
      $('#inp6_'+id).attr("readonly", true);
      swal({
      title: "Data Tersimpan!",
      text: "Data Survei anda Telah Tersimpan!",
      timer: 2000,
      showConfirmButton: false
  }).then(function () {
        window.location.reload();
      });
    });
  }

  function verify(kditem, kdisi){
    isian = 'kditem=' + kditem +'&kdisi='+kdisi;
     $.ajax({
      url : "<?= site_url('sb_survei/crud_survei_data_edit') ?>",
      type: "POST",
      data: isian
    }).done(function(){
      $('#inp1_'+kditem).prop('readonly', false);
      $('#inp2_'+kditem).prop('readonly', false);
      $('#inp3_'+kditem).prop('readonly', false);
      $('#inp4_'+kditem).prop('readonly', false);
      $('#inp5_'+kditem).prop('readonly', false);
      $('#inp6_'+kditem).prop('readonly', false);
      $('#inp9_'+kditem).find("i").css("display", "none");
    });
    e.preventDefault();
  }

  function flag(kditem,kdisi){
    kdlokasi = $('#kdlokasi').val();
    isian = 'kdlokasi='+kdlokasi+'&kditem=' + kditem + '&kdisi='+kdisi;
    $.ajax({
      url  : "<?= site_url('sb_survei/crud_survei_data_flag') ?>",
      type : "POST",
      data : isian
    }).done(function(){
      $('#inp1_'+kditem).prop('readonly', true);
      $('#inp2_'+kditem).prop('readonly', true);
      $('#inp3_'+kditem).prop('readonly', true);
      $('#inp4_'+kditem).prop('readonly', true);
      $('#inp5_'+kditem).prop('readonly', true);
      $('#inp6_'+kditem).prop('readonly', true);
      swal({
      title: "Data Tersimpan!",
      text: "Data Survei anda Telah Tersimpan!",
      timer: 2000,
      showConfirmButton: false
  }).then(function () {
        window.location.reload();
      });
    });
  }

  function kirim() {
    var isian  = 'kdlokasi='+ $('#kdlokasi').val() +'&file='+$('#nmfile2').val();
    
    $.ajax({
      url : "<?=site_url('sb_survei/kirim_perbenpusat')?>",
      type: "POST",
      data: isian
    }).done(function(){
        swal({
        title: "Kirim Data Survei Ke DJPB Pusat",
        text: "Data Survei anda Telah Terkirim ke DJPB Pusat!",
        timer: 2000,
        showConfirmButton: false
        });
    });
  }

  function parsenil(kditem,ket){
    $('#ModalSubItem').val(kditem);
    $('#ModalSubInput').val(ket);
  }

  function saveketerangansubitem(){
    var kditem   = $('#ModalSubItem').val();
    var ket      = $('#ModalSubInput').val();
    var kdlokasi = $('#kdlokasi').val();
    var isian    = 'ket='+ket+'&kdlokasi='+kdlokasi+'&kditem='+kditem;
     $.ajax({
      url : "<?=site_url('sb_survei/kirim_keterangansubitem')?>",
      type: "POST",
      data: isian
    }).done(function(){
        swal({
        title: "Keterangan anda telah Tersimpan !",
        text: " ",
        timer: 2000,
        showConfirmButton: false
        }).then(function () {
        window.location.reload();
      });
    });
  }


</script>

<script type="text/javascript">
    $('#UploadFile').fileinput({
        uploadExtraData:  function(previewId, index) {
            var data = { tahun : '2021', lokasi: $('#kdlokasi').val() , kditem : $('#ModalItem').val(), name : 'UploadFile' };
            return data;
        },
        uploadUrl: "<?php echo site_url('sb_survei/fileupload') ?>",
        showPreview: true,
        uploadAsync: false,
        allowedFileExtensions : ['doc','docx','rtf','pdf','rar','zip'],
        msgInvalidFileExtension: "Invalid extension file !",
        overwriteInitial: true,
        maxFileCount: 1
    });

    $('#UploadFile').on('filebatchuploadsuccess', function(event, data, extra) {
        var extra = data.extra, response = data.response;
        $('#nmfile').val(response.file);
    });

    $('#UploadFile').on('filebatchuploaderror', function(event, data) {
        var response = data.response;
        alert('Gagal upload !');
    });
</script>

<script type="text/javascript">
    $('#UploadFile2').fileinput({
        uploadExtraData:  function(previewId, index) {
            var data = { tahun : '2021', lokasi:$('#kdlokasi').val() , name : 'UploadFile2' };
            return data;
        },
        uploadUrl: "<?php echo site_url('sb_survei/fileupload2') ?>",
        showPreview: true,
        uploadAsync: false,
        allowedFileExtensions : ['doc','docx','rtf','pdf','rar','zip'],
        msgInvalidFileExtension: "Invalid extension file !",
        overwriteInitial: true,
        maxFileCount: 1
    });

    $('#UploadFile2').on('filebatchuploadsuccess', function(event, data, extra) {
        var extra = data.extra, response = data.response;
        $('#nmfile2').val(response.file);
    });

    $('#UploadFile2').on('filebatchuploaderror', function(event, data) {
        var response = data.response;
        alert('Gagal upload !');
    });
</script>
