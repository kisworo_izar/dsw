<script src="<?=base_url('assets/plugins/jQueryUI/jquery-ui.1.11.4.min.js'); ?>" type="text/javascript"></script>

<style media="screen">
	th {
    	text-align: center;
  	}
  	thead tr {
    	color: #fff;
    	background: #00acd6;
  	}

  	#iGrid .ui-selecting { background: #FECA40; }
  	#iGrid .ui-selected { background: #F39814; color: white; }
</style>

<div class="row">
	<div class="col-md-12">
		<div class="box box-widget">
			<div class="box-header with border">
				<div class="wel wel-sm">
					<form id="iForm" class="form-inline" role="form" action="<?php echo site_url("sb_survei/cari") ?>" method="post">
						<div class="form-group pull-right">
							<div class="input-group">
                    			<input type="hidden" name="nmfunction" value="grid">
                				<input class="form-control" name="cari" type="text" value="<?php echo $this->session->userdata('cari') ?>">
            					<span class="input-group-btn">
            						<button type="submit" name="search" value="search" class="btn btn-default btn-flat"><i class="fa fa-search"></i></button>
            					</span>
            					<span class="input-group-btn">
            						<button type="submit" name="search" value="clear" class="btn btn-default btn-flat"><i class="fa fa-times-circle"></i></button>
                				</span>
                			</div>
                		</div>
                		<div class="form-group">
                			<div class="input-group">
	                			<span class="input-group-btn">
                    				<input type="hidden" id="idnotifikasi" value="">
									<button type="button" id="addrow"  class="btn btn-warning">Rekam</button>
					  				<button type="button" id="editrow" class="btn btn-warning">Ubah</button>
					  				<button type="button" id="delrow"  class="btn btn-warning">Hapus</button>
	                			</span>
                			</div>
                		</div>
                	</form>
                </div>
            </div>

			<div class="box-body table-responsive" style="padding-top:0px">
				<table id="iGrid" class="table table-hover table-bordered">
					<thead>
					<tr>
						<th>ID</th>
						<th>Tanggal</th>
						<th>Notif Atas</th>
						<th>Notif Bawah</th>
						<th>Link</th>
						<th>File </th>
					</tr>
					</thead>
				<tbody>
					<?php
					if ($table) {
						foreach ($table as $row) { ?>
							<tr>
								<td id="<?php echo $row['idnotifikasi'] ?>">
									<?php echo '# '.$row['idnotifikasi'] ?></td>
								<td><?php echo $this->fc->idtgl($row['tglrekam'],'hari') ?></td>
								<td><?php echo $row['notif1']; if ($row['toplist']=='1') echo ' <i class="fa fa-bullhorn text-danger"></i>'; ?>
								<td><?php echo $row['notif2'] ?>
								</td>
								<td><?php echo $row['link'] ?></td>
								<td><?php echo $row['file'] ?></td>
							</tr>
						<?php
						}
					} else { ?>
							<tr><td colspan="4" class="text-danger"><i class="fa fa-exclamation-circle"></i> Data tidak ditemukan ...</td></tr>
					<?php } ?>
				</tbody>
				</table>
			</div>

			<div class="box-footer clearfix">
				<?php echo $this->pagination->create_links(); ?>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$('.pagination').addClass('pagination-sm no-margin pull-right');
</script>

<script type="text/javascript">
	$(function() {

		// Tandai ROW yang dipilih dalam iGRID
		$( "#iGrid tbody" ).selectable({
			filter: ":not(td)",
				create: function( e, ui ) {
				iGridLink( $() );
			},
			selected: function( e, ui ) {
				var widget = $(this).find('.ui-selected');
				$(ui.unselected).addClass("info");
				iGridLink( widget );
			},
			unselected: function( e, ui ) {
				$(ui.unselected).removeClass("info");
				var widget = $(this).find('.ui-selected');
				iGridLink( widget );
			}
		});

		// ONCLICK untuk Rekam / Ubah / Hapus
		$("#addrow").on("click", function() {
			window.location.href = "<?php echo site_url('sb_survei/notif_rekam/r') ?>" ;
		});
		$("#editrow").on("click", function() {
			window.location.href = "<?php echo site_url('sb_survei/notif_rekam/u') ?>" +"/"+ document.getElementById("idnotifikasi").value;
		});
		$("#delrow").on("click", function() {
			window.location.href = "<?php echo site_url('sb_survei/notif_rekam/h') ?>" +"/"+ document.getElementById("idnotifikasi").value;
		});

		// Ambil Nilai ROW pada iGRID yang dipilih per-ID sesuai kolom CHILD
		function iGridLink( $selectees ) {
			selected = $.makeArray( $selectees.filter( ".ui-selected" ) );
				selected.reduce( function( a, b ) {
					document.getElementById("idnotifikasi").value= $(b).children( "td:nth-child(1)" ).attr('id');
				}, 0
			);
		 }

	});
</script>
