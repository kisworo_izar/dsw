<style media="screen">
  .form-group { margin: 0px;}
</style>


<div class="box box-widget" style="margin: 0px 0px 0px 0px;">
  <div class="box-body">
      <table class=" table table-hover table-bordered" style="border-spacing: 1; width: 100%;">
        <tbody>
          <tr>
            <td width="5%" class="text-left pull-left">001001</td>
            <td width="35%" class="text-left pull-left">Paket Makan Pertama</td>
            <td width="10%" class="text-left pull-left">
              <input type="text" name="" id="" value="" placeholder="nama 1" class="form-control">
            </td>
            <td width="10%" class="text-left pull-left">
              <input type="text" name="" id="" value="" placeholder="nilai 1" class="form-control">
            </td>
            <td width="10%" class="text-left pull-left">
              <input type="text" name="" id="" value="" placeholder="nama 2" class="form-control">
            </td>
            <td width="10%" class="text-left pull-left">
              <input type="text" name="" id="" value="" placeholder="nilai 2" class="form-control">
            </td>
            <td width="10%" class="text-left pull-left">
              <input type="text" name="" id="" value="" placeholder="nama 2" class="form-control">
            </td>
            <td width="10%" class="text-left pull-left">
              <input type="text" name="" id="" value="" placeholder="nilai 2" class="form-control">
            </td>
            <td width="5%"></td>
            <td width="5%"></td>
          </tr>
        </tbody>
      </table>
  </div>
</div>


<script type="text/javascript">
  function crud( aksi ) {
    var isian = new Array(),
        kdkey  = $('#aktif').val();
    $("input[ name='isian[]' ]").each(function() { isian.push( $(this).val() ); });

    $.ajax({
      url : "<?php echo site_url('admin_panel/crud_so') ?>",
      type: "POST",
      data: { 'aksi': aksi, 'isian': isian, 'kdkey': kdkey },
      success: function(pesan) {
        if (aksi=='Rekam') { window.location.href = "<?php echo site_url('admin_panel/admin_so') ?>" }
        if (aksi=='Ubah') {
          var arr = pesan.split("#");
          var cls = $('#'+kdkey).attr('class');
          $('#'+kdkey).attr('class', pesan);
          $('#'+kdkey).children( 'td:nth-child(1)' ).text( arr[0] );
          $('#'+kdkey).children( 'td:nth-child(2)' ).text( arr[1] );
          $('#'+kdkey).children( 'td:nth-child(3)' ).text( arr[2] );
          $('#'+kdkey).children( 'td:nth-child(4)' ).text( arr[3] );
          $('#'+kdkey).children( 'td:nth-child(5)' ).text( arr[4] );
          $('#'+kdkey).children( 'td:nth-child(6)' ).text( arr[5] );
          move_row( kdkey );
        }
        if (aksi=='Hapus') { 
          move_row( kdkey );
          $('table#iGrid tr#'+kdkey).remove(); 
        }
      }  
    })
  }
</script>
