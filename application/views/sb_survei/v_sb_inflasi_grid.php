<div class="box box-widget">
  <input type="hidden" id="aktif">
  <input type="hidden" id="strlen" value="0">

  <div class="box-body">
    <table id="iGrid" class="table table-hover table-bordered" style="border-spacing: 1; width: 100%;">
      <thead>
        <tr style="color: #fff; background-color: #3c8dbc;">
          <th class="text-center" width="25px">Kode</th>
          <th class="text-center">Uraian</th>
        </tr>
        <tr id="tr_cari" style="margin: 0px" bgcolor="#fff">
          <td colspan="2">
              <input id="cari" class="form-control" placeholder="ketik kata cari ... " value="">
          </td>
        </tr>
      </thead>

      <tbody>
          <tr id="tr_crud" style="display: none" bgcolor="#eee">
            <td colspan="5"><div id="crud_form"><?php $this->load->view('sb_survei/v_sb_inflasi_crud'); ?></div></td>
          </tr>

          <?php foreach($item as $row) { ?>
          <tr id="id__<?= $row['kditem'] ?>" data-list="<?= implode('#', $row) ?>" style="margin: 0px" onclick="move_row( this.id )" class="text-bold">
            <td class="text-center" style="padding:5px 10px"><?= substr($row['kditem'],0,3) ?></td>
            <td class="text-left" style="padding:5px 10px"><?= $row['uraian'] ?></td>
          </tr>
          <?php } ?>
      </tbody>
    </table>

  </div>
</div>

<script type="text/javascript">
  // FIRST START
  $( document ).ready(function() {
    $('#crud_form').slideUp().addClass('CloseForm');
  });

  // PENCARIAN
  $("#cari").keyup( function(event) {
    var kata = $('#cari').val();
    $('#tr_crud').css('display', 'none').insertAfter( $('#tr_cari') );

    if (kata.length > $('#strlen').val()) {
      $('#iGrid > tbody  > tr:visible').each(function() {
        var id  = $(this).attr('id');
        var str = $('#'+id).text().toLowerCase();
        if (str.indexOf( kata.toLowerCase() ) < 0) $('#'+id).hide();
      });
    } else {
      $('#iGrid > tbody  > tr:hidden').each(function() {
        var id  = $(this).attr('id');
        var str = $('#'+id).text().toLowerCase();
        if (str.indexOf( kata.toLowerCase() ) >= 0) $('#'+id).show();
      });
    }
    $('#strlen').val( kata.length );
  });

  // Click ROW untuk Buka Form CRUD
  function move_row( idkey ) {
    if ( $('#aktif').val()==idkey ) {
      animeCrud('close');
    } else {
      animeCrud( idkey );
      var arr = $('#'+idkey).data('list').split("#");
      $("#1").prop('readonly', true);
      $("form input").each(function(){ $(this).val(arr[ $(this).attr('id') ]); })
      $('#Rekam').hide(); $('#Ubah, #Hapus').show();
    }
  }

  // Click Tombol PLUS untuk Buka Form CRUD
  function newadd() {
    if ( $('#aktif').val()=='tr_cari' ) {
      animeCrud('close');
    } else {
      animeCrud('tr_cari');
      $("#1").prop('readonly', false);
      $("form input").each(function(){ $(this).val(''); })
      $("#0").val('2018');
      $('#Rekam').show(); $('#Ubah, #Hapus').hide();
    }
  }

  // Animasi Slide Up Down Form Crud biar smooth
  function animeCrud( aksi ) {
    if (aksi == 'close') {
      $('#aktif').val('');
      $('#crud_form').slideUp('slow').addClass('CloseForm').removeClass('OpenForm');
      setTimeout(function(){ $('#tr_crud').css('display', 'none'); }, 500);
    } else {
      $('#aktif').val(aksi);
      $('#tr_crud').css('display', 'none');
      if ($('#crud_form').attr('class') == 'OpenForm') $('#crud_form').slideUp('fast');
      $('#tr_crud').css('display', '').insertAfter( $('#'+aksi) );
      $('#crud_form').slideDown().addClass('OpenForm').removeClass('CloseForm');
    }
  }
</script>
