

<style media="screen">
  .form-group { margin: 0px;}
  .btn-crud { margin-top: 10px;}
</style>


<div class="box box-widget" style="margin: 0px 0px 0px 0px;">
  <div class="box-body">

    <form action="" enctype="multipart/form-data">
        <div class="row">
          <div class="col-md-12">
            <div class="box box-widget">
                <table class="table table-hover table-bordered">
                  <thead>
                    <tr style="background: rgb(60,141,188);color: #fff">
                      <th width="" class="text-center" style="padding:5px 3px">Provinsi</th>
                      <th width="" class="text-center" style="padding:5px 3px">Inflasi N</th>
                      <th width="" class="text-center" style="padding:5px 3px">Inflasi N + 1</th>
                    </tr>
                  </thead>
                  <tbody>
                  <?php foreach ($table as $row) { ?>
                    <tr>
                      <td style="padding:3px 5px 0px 8px"><?= $row['kdlokasi'].'. '.$row['nmlokasi']  ?></td>
                      <td style="padding:0px">
                        <input id="01" type="text" data-mask="#.##0,00" data-mask-reverse="true" data-mask-maxlength="false" name="inflasi_n" class="form-control input-sm" style="text-align:right">
                      </td>
                      <td style="padding:0px">
                        <input id="02" type="text" data-mask="#.##0,00" data-mask-reverse="true" data-mask-maxlength="false" name="inflasi_n" name="inflasi_n1" class="form-control input-sm" style="text-align:right">
                      </td>
                    </tr>
                    <?php } ?>
                  </tbody>
                </table>
            </div>
          </div>
        </div>
    </form>

  </div>
</div>

<script src="<?= base_url('assets/plugins/jQuery-Mask/dist/jquery.mask.min.js') ?>" type="text/javascript"></script>

<script type="text/javascript">
  function crud( aksi ) {
    $('form').submit(function(e){
      e.preventDefault();
      $.ajax({
        url : "<?php echo site_url('sb_survei/crud_survei_inflasi') ?>",
        type: "POST",
        data: $('form').serialize() +'&aksi='+ aksi,
        success: function(pesan) {
          var arr = pesan.split("#"), key = 'id_'+arr[1];
          if (aksi=='Rekam') { window.location.href = "<?php echo site_url('sb_survei/inflasi') ?>" }
          if (aksi=='Ubah') {
            $('#'+key).data('list', pesan);
            $('#'+key).children( 'td:nth-child(1)' ).text( arr[1] );
            $('#'+key).children( 'td:nth-child(2)' ).text( arr[2] );
            move_row( key );
          }
          if (aksi=='Hapus') {
            move_row( key );
            $('table#iGrid tr#'+key).remove();
          }
        }
      });
    });
  }
</script>
