<div class="row">
    <div class="col-md-12">
            <div class="box-body with-border">
                <div class="box-body table-responsive no-padding">
                  <table class="table table-striped">
                    <thead>
                      <tr style="background: #00acd6; color: #fff;">
                        <th style="width: 40px" class="text-left">KdLokasi</th>
                        <th hidden-xs hidden-sm>Nama Lokasi</th>
                        <th class="text-right">R1 Harga</th>
                        <th class="text-right">R2 Harga</th>
                        <th class="text-right">R3 Harga</th>
                        <th class="text-right">Min</th>
                        <th class="text-right">Max</th>
                        <th class="text-right">Average</th>
                      </tr>
                    </thead>
                    <tbody style="margin:10px">
                      <?php foreach($table as $key=> $row) {?>
                      <tr>
                        <td><?= $row['kdlokasi'] ?></td>
                        <td><?= $row['nmlokasi'] ?></td>
                        <td class="text-right"><?php echo number_format($row['r1_harga'], 0, ',', '.') ?></td>
                        <td class="text-right"><?php echo number_format($row['r2_harga'], 0, ',', '.') ?></td>
                        <td class="text-right"><?php echo number_format($row['r3_harga'], 0, ',', '.') ?></td>
                        <td class="text-right"><?php echo number_format(min($row['r1_harga'],$row['r2_harga'],$row['r3_harga']),0,',','.')  ?></td>
                        <td class="text-right"><?php echo number_format(max($row['r1_harga'],$row['r2_harga'],$row['r3_harga']),0,',','.')  ?></td>
                        <td class="text-right"><?php if($row['r1_harga']>0 && $row['r2_harga']>0 && $row['r3_harga']>0) echo number_format(array_sum(array($row['r1_harga'],$row['r2_harga'],$row['r3_harga']))/ count(array($row['r1_harga'],$row['r2_harga'],$row['r3_harga'])),0,',','.') ;
                        ?>
                        </td>
                      </tr>
                      <?php } ?>
                  </tbody>
                </table>
              </div>
            </div>
            <div class="box-footer with-border">
                <p class="small text-muted pull-left" style="margin-bottom:0px">
                   <button type="button" class="btn-sm btn-primary" onclick="goBack()">Back</button>
                </p>
                <p class="small text-muted pull-right" style="margin-bottom:0px">
                   <button type="button" class="btn-sm btn-primary" onclick="excel('<?=$row['kditem']?>')">Export Data ke Excel</button>
                </p>
            </div>
        </div>
    </div>


<script>
  function goBack() {
    window.history.back();
  }

  function excel(kditem){
    var kditem = kditem;

    window.location.href = "<?php echo site_url('sb_survei/print_excel') ?>"+'/'+kditem;
  }
</script>