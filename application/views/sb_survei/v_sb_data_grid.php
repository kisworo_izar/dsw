<link href="<?=base_url(); ?>assets/plugins/uploadfile/fileinput.css" rel="stylesheet">
<script src="<?=base_url();?>assets/plugins/uploadfile/fileinput.min.js" type="text/javascript"></script>

<div class="box box-widget">
  <div>
    <input type="hidden" id="aktif">
    <input type="hidden" id="kdso" value="00">
  </div>

  <div class="box-body">
    <table id="iGrid" class="table table-hover table-bordered" style="border-spacing: 1; width: 100%;">
      <thead>
        <tr style="color: #fff; background-color: #3c8dbc;">
          <th class="text-center" width=" 3%" rowspan="2">No.</th>
          <th class="text-center" width="17%"    rowspan="2">Uraian Satuan Biaya</th>
          <th class="text-center" width="20%" colspan="2">Responden #1</th>
          <th class="text-center" width="20%" colspan="2">Responden #2</th>
          <th class="text-center" width="20%" colspan="2">Responden #3</th>
          <th class="text-center" width="20%" rowspan="2">Keterangan & Upload</th>
        </tr>
        <tr style="color: #fff; background-color: #3c8dbc;">
          <th class="text-center" width="10%">Nama</th>
          <th class="text-center" width="10%">Nilai</th>
          <th class="text-center" width="10%">Nama</th>
          <th class="text-center" width="10%">Nilai</th>
          <th class="text-center" width="10%">Nama</th>
          <th class="text-center" width="10%">Nilai</th>
        </tr>
        <!-- <tr id="tr_cari" style="margin: 0px" bgcolor="#fff">
          <td colspan="9">
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-btn">
                  <button id="btn_data" onclick="newadd()" class="btn btn-default"><i class="fa fa-plus"></i></button>
                </span>
                <input id="cari" class="form-control" placeholder="ketik kata cari" value="">
              </div>
            </div>
          </td>
        </tr> -->
      </thead>

      <tbody>
        <?php foreach ($sb_survei_data as $key=>$row) {
          $icon = ''; if ($row['kdisi'] !='0') { $icon='<i id="first" class="fa fa-check text-success" ></i>'; }
          $readonly = ''; if ($row['kdisi'] !='0') $readonly = 'readonly';
          $class1 = implode('#', $row) .'# 00 '. substr($row['kditem'],0,2);
          $bold = ''; if (substr($row['kditem'],6) == '000000' && $row['kdinput'] !='1') $bold = 'text-bold';
          $padding = '30px'; if (substr($row['kditem'],6,3) == '000') $padding = '15px'; if (substr($row['kditem'],3,6) == '000000') $padding = '0px'; ?>

          <?php if (substr($row['kditem'],3,6) == '000000') { ?>
            <tr id="<?= substr($row['kditem'],0,3) ?>" class="<?= $class1 .' '. $bold ?>" style="margin:0px;" onclick="buka( this.id )" >
              <td class="text-right"><?php if($padding == '0px') echo substr($row['kditem'],0,3); ?></td>
              <td colspan="7" class="text-left" style="padding-left: <?= $padding ?>">  <?= $row['uraian'] ?></td>
              <td class="text-left">
                <span type="button" id="<?= 'ket_'.$row['kditem'] ?>" class="btn btn-primary btn-sm Keterangan" style="width: 100%" data-list="<?= $row['kditem'] .'#'. $row['uraian'].'#'. $row['r1_keterangan'] ?>" data-toggle="modal" data-target="#myModal" >Isikan Keterangan & Upload</span>
             </td>
              <td class="text-left">
            </tr>

          <?php } else if ($row['kdinput'] == '0') { ?>
            <tr id="id_<?= $row['kditem'] ?>" class="<?= substr($row['kditem'],0,3) .' Sub '. $class1 .' '. $bold ?>" style="margin:0px; display: none">
              <td class="text-center"><?php if($padding == '0px') echo substr($row['kditem'],0,3); ?></td>
              <td colspan="8" class="text-left" style="padding-left: <?= $padding ?>">  <?= $row['uraian'] ?></td>
            </tr>

          <?php } else  { ?>
            <tr id="sub_<?= $row['kditem'] ?>" class="<?= substr($row['kditem'],0,3) .' Sub '. $class1 .' '. $bold ?>" style="margin:0px; display: none" >
              <td class="text-center"><?php if($padding == '0px') echo substr($row['kditem'],0,3); ?></td>
              <td class="text-left" style="padding-left: <?= $padding ?>">  <?= $row['uraian'] ?></td>
              <td class="text-center">
                <input style="text-align:left" id="<?= 'inp1_'.$row['kditem'] ?>" type="text" placeholder="Nama ..." class="small form-control" <?php echo $readonly ?> value="<?php echo $row['r1_nama'] ?>" />
              </td>
              <td class="text-left">
                <input data-mask="#.###.###.###.###" data-mask-reverse="true" data-mask-maxlength="false"  style="text-align:left" id="<?= 'inp2_'.$row['kditem'] ?>" type="text" placeholder="Harga ..." class="small form-control" <?php echo $readonly ?> value="<?php echo $row['r1_harga'] ?>" />
              </td>
              <td class="text-center">
                <input style="text-align:left" id="<?= 'inp3_'.$row['kditem']?>" type="text" placeholder="Nama ..." class="small form-control" <?php echo $readonly ?> value="<?php echo $row['r2_nama'] ?>" />
              </td>
              <td class="text-left">
                 <input data-mask="#.###.###.###.###" data-mask-reverse="true" data-mask-maxlength="false" style="text-align:left" id="<?= 'inp4_'.$row['kditem']?>" type="text" placeholder="Harga ..." class="small form-control" <?php echo $readonly ?> value="<?php echo $row['r2_harga'] ?>" />
              </td>
              <td class="text-center">
                <input style="text-align:left" id="<?= 'inp5_'.$row['kditem']?>" type="text" placeholder="Nama ..." class="small form-control" <?php echo $readonly ?> value="<?php echo $row['r3_nama'] ?>" />
              </td>
              <td class="text-left">
                <input data-mask="#.###.###.###.###" data-mask-reverse="true" data-mask-maxlength="false" style="text-align:left" id="<?= 'inp6_'.$row['kditem']?>" type="text" placeholder="Harga ..." class="small form-control" name="<?= $row['kditem'] ?>" data-list="" <?php echo $readonly ?> value="<?php echo $row['r3_harga'] ?>"  onblur="aksi('<?= $row['kditem'] ?>')"/>
              </td>
              <td class="text-left" id="<?= 'inp7_'.$row['kditem'] ?>">
                <?php echo $icon ?>
                <input type="submit" name="jnsverif" onclick="verify('<?= $row['kditem'] ?>','<?= $row['kdisi'] ?>')" class="btn btn-danger btn-sm" value="Edit">
                <button type="submit" class="btn btn-primary" onclick="flag('<?= $row['kditem'] ?>','<?= $row['kdisi'] ?>')">
                  <span class="fa fa-times-circle-o"></span> 
                </button>
              </td>
              <td class="text-left"> &nbsp</td>
            </tr>
        <?php } } ?>
      </tbody>
    </table>

  </div>
</div>

<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Isikan Keterangan</h4>
      </div>
      <div class="modal-body form-group">
        <label id="ModalLabel"></label>
        <input id="ModalItem" type="hidden">
        <input id="ModalInput" type="text" class="form-control">
      </div>
      <div class="modal-body form-group">
        <label>Upload File</label>
          <div style="padding:0px">
              <input id="ModalItem2" type="hidden">
              <input id="UploadFile" name="files[]" type="file" multiple class="file" data-overwrite-initial="true" data-min-file-count="1">
          </div>
      </div>
      <div class="modal-footer">
        <button id="Save" class="btn btn-info" data-dismiss="modal" onclick="simpan()">Save</button>
        <button id="Cancel" class="btn btn-danger" data-dismiss="modal">Cancel</button>
      </div>
    </div>

  </div>
</div>

<script src="<?= base_url('assets/plugins/jQuery-Mask/dist/jquery.mask.min.js') ?>" type="text/javascript"></script>

<script type="text/javascript">
  function buka( id ) {
    $('.Sub').hide(); $('.'+id).show();
  }

  $('.Keterangan').on('click', function() {
    var arr = $(this).data('list').split("#");
    $('#ModalItem').val(arr[0]);
    $('#ModalLabel').html('Keterangan Item : '+arr[1]);
    $('#ModalInput').val(arr[2]);
  });

  function simpan() {
    var kditem = $('#ModalItem').val();
    var arr    = $('#ket_'+kditem).data('list').split("#"); 
    var isian  = 'kditem='+ $('#ModalItem').val() +'&r1_keterangan='+ $('#ModalInput').val();
    alert (kditem);
    $.ajax({
      url  : "<?= site_url('sb_survei/crud_survei_data_keterangan') ?>",
      type : 'POST',
      data : isian
    }).done(function(){
      arr[2] = $('#ModalInput').val(); 
      $('#ket_'+kditem).data('list', arr.join('#'));
      $('#ModalInput').val('');
    })
  }

  function aksi( id ) {
    var r1_nama   = $('#inp1_'+id).val(),
        r1_harga  = $('#inp2_'+id).val(),
        r2_nama   = $('#inp3_'+id).val(),
        r2_harga  = $('#inp4_'+id).val(),
        r3_nama   = $('#inp5_'+id).val(),
        r3_harga  = $('#inp6_'+id).val();
        check     = $('#inp7_'+id).val();

    var isian = 'kditem='+id +'&r1_nama='+r1_nama +'&r1_harga='+r1_harga+'&r2_nama='+r2_nama+'&r2_harga='+r2_harga+'&r3_nama='+r3_nama+'&r3_harga='+r3_harga+'&check='+check;
    $.ajax({
      url : "<?= site_url('sb_survei/crud_survei_data') ?>",
      type: "POST",
      data: isian
    }).done(function(){
      $('#inp7_'+id).prepend("<i class=' text-left fa fa-check text-success'></i>");
      $('#inp1_'+id).attr("disabled", true);
      $('#inp2_'+id).attr("disabled", true);
      $('#inp3_'+id).attr("disabled", true);
      $('#inp4_'+id).attr("disabled", true);
      $('#inp5_'+id).attr("disabled", true);
      $('#inp6_'+id).attr("disabled", true);
    });
  }

  $('input[type=text]').on('keyup', function(e) {
    if (e.which == 13) {
    }
  });

  function verify(kditem, kdisi){
    isian = 'kditem=' + kditem +'&kdisi='+kdisi;
    
     $.ajax({
      url : "<?= site_url('sb_survei/crud_survei_data_edit') ?>",
      type: "POST",
      data: isian
    }).done(function(){
      $('#inp1_'+kditem).prop('readonly', false);
      $('#inp2_'+kditem).prop('readonly', false);
      $('#inp3_'+kditem).prop('readonly', false);
      $('#inp4_'+kditem).prop('readonly', false);
      $('#inp5_'+kditem).prop('readonly', false);
      $('#inp6_'+kditem).prop('readonly', false);
      $('#inp7_'+kditem).find("i").css("display", "none");
    }); 
  }

  function flag(kditem,kdisi){
    kdlokasi = '01';
    isian = 'kdlokasi='+kdlokasi+'&kditem=' + kditem + '&kdisi='+kdisi;
    $.ajax({
      url  : "<?= site_url('sb_survei/crud_survei_data_flag') ?>",
      type : "POST",
      data : isian
    }).done(function(){
      $('#inp1_'+kditem).prop('readonly', true);
      $('#inp2_'+kditem).prop('readonly', true);
      $('#inp3_'+kditem).prop('readonly', true);
      $('#inp4_'+kditem).prop('readonly', true);
      $('#inp5_'+kditem).prop('readonly', true);
      $('#inp6_'+kditem).prop('readonly', true);
    })
  }

</script>

<script type="text/javascript">
  var tahun  = '2020',
      lokasi ='11',
      kditem = '001';

    $("#UploadFile").fileinput({
        uploadUrl: "<?php echo site_url('sb_survei/fileupload') ?>" +'/'+tahun+'/'+lokasi+'/'+ kditem,
        allowedFileExtensions : ['xls','xlsx','doc','docx','ppt','pptx','pdf','rar','zip'],
        overwriteInitial: true,
        maxFileSize: 10000,
        maxFilesNum: 10,
        minFileCount: 1,
        maxFileCount: 5,
        dropZoneTitle: 'Drag & Drop file Anda di Sini',
        msgInvalidFileExtension: "Invalid extension file {name}. Hanya {extensions} files yang bisa diproses ...",
          slugCallback: function(filename) {
            return filename.replace('(', '_').replace(']', '_');
          }
     });
</script>