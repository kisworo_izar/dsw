<link href="<?=base_url(); ?>assets/plugins/uploadfile/fileinput.css" rel="stylesheet">
<script src="<?=base_url();?>assets/plugins/uploadfile/fileinput.min.js" type="text/javascript"></script>
<script src="<?= base_url('assets/plugins/jQuery-Mask/dist/jquery.mask.min.js') ?>" type="text/javascript"></script>

<style media="screen">
th{text-align: center;background: #E7E9EE;color:#34495e} td a {display:block;width:100%;}
  .calculated-width {width: -moz-calc(100% - 50px);width: -webkit-calc(100% - 50px);width: calc(100% - 80.9px);}​
  .table-bordered > thead > tr > th,
  .table-bordered > thead > tr > td {border-bottom: 0px solid #E7E9EE;}
  .table-bordered > tbody > tr > td {padding:3px}
  .tbl_h {text-align: center; font-weight: bold; border: solid 1px #9ECEC5}
  .dt {padding:0px 5px;} .hiddenRow {padding: 0 !important;}
  .info{border-top:solid 0px #E7E9EE;padding:0px;margin:0px}
  .x { display: inline-block; width: 650px; }
  .p3 {padding:3px} .p1 {padding:3px; border-right:solid 0px #9ECEC5}
  .w1 {width:25%} .w2 {width:12%} .w3 {width:15px}
  .hijau{background: #27ae60; color:white} .biru{background: #3498db; color:white}
  .abu{background: white; color:#bdc3c7} .merah{background: #e74c3c; color:white}
  .green{background: white; color: green}
  section {position: relative;} section.positioned {position: absolute;top:10px;left:10px;}
  .container {overflow-y: auto;padding: 0px;padding-top: 14px;width: 100%}
  table {border-spacing: 1;width:100%;}
  th {height: 0;line-height: 0;padding-top: 0;padding-bottom: 0;color: #000;border: none;white-space: nowrap;}
  th div{position: absolute;background:#E7E9EE;color:black;border-bottom: solid #E7E9EE 0px;border-top: solid 0px #E7E9EE; padding: 0px 0px;top: 0;line-height: normal;} input {width: 100%; border: 0px!important}
  th{text-align: center;background: #E7E9EE;color:#34495e} td a {display:block;width:100%;}
  /*.table-bordered > thead > tr > th, .table-bordered > thead > tr > td {border-bottom: 2px solid #E7E9EE;}
  .table-bordered > tbody > tr > td {padding:3px}*/
  .td_sts_hijau {width: 75px; background:#27AE60; color:white; text-align:center; padding-top: 5px !important}
  .td_sts_abu {width: 75px; background:#DEDEDE; color:white; text-align:center; padding-top: 5px !important}
  .tbl_h {text-align: center; font-weight: bold; border: solid 1px #9ECEC5}
  .dt {padding:0px 5px;} .hiddenRow {padding: 0 !important;}
  .info{border-top:solid 1px #E7E9EE;padding:0px;margin:0px}
  .x { display: inline-block; width: 650px; } .p3 {padding:3px} .p1 {padding:3px; border-right:solid 1px #9ECEC5}
  .abu{background: white; color:#bdc3c7} .merah{background: #e74c3c; color:white}
  section {position: relative;} section.positioned {position: absolute;top:10px;left:10px;}
  .container {overflow-y: auto;padding: 0px;width: 100%} table {border-spacing: 1;width:100%;}
  th {height: 0;line-height: 0;padding-top: 0;padding-bottom: 0;color: #000;border: none;white-space: nowrap;}
  th div{position: absolute;background:#E7E9EE;color:black;border-bottom: solid #E7E9EE 1px;border-top: solid 1px #E7E9EE; padding: 6px 0px;top: 0;line-height: normal;} input {width: 100%; border: 0px!important}
</style>

<div class="row" ng-app="app" >
  <div class="col-md-12">
    <div class="box box-widget">
      <div class="box-header with-border">
         <div class="span5 col-md-8 pull-left" style="border:0px; padding:0px;">
           <h4 style="margin:6px 0px; color:#337AB7 !important ">Rincian Detail Survei</h4>
         </div>
      </div>
      <div class="box-body">
        <section class="">
          <div class="container" style="height:650px">
            <table id="iGrid" class="table table-hover table-bordered">
              <thead>
                <tr style="padding-left:10px">
                  <th colspan="2" style="padding: 5px" class="text-left">Kode dan Uraian Item Survei</th>
                </tr>
              </thead>
              <tbody>
              <?php foreach ($table as $key=>$row) {
                $iditem     = substr($row['kditem'],0,3);
                $nmitem     = $row['uraian'];
              ?>

              <?php if (substr($row['kditem'],3,12) == '000000000000' && $row['kdinput'] == '0') { ?>
                  <tr style="margin:0px" id="<?php $iditem ?>">
                    <td class="text-bold text-primary text-center" data-toggle="collapse" data-target=".<?php echo $iditem ?>">
                      <?= $iditem ?></td>
                    <td style="border:solid 0px;border-bottom:solid 0px #F4F4F4" data-toggle="collapse" data-target=".<?php echo $iditem ?>">
                      <div><?= $nmitem ?></div>
                    </td>
                  </tr>
                <?php } else { ?>
                  <tr>
                    <td class="hiddenRow" colspan="8" style="border:solid 0px">
                      <div class="small collapse info <?php echo $iditem ?>" style="padding:0px 7px;background:#FDFDFD">
                        <table border="1" style="padding:3px; margin:1px 0px">
                          <?php if (substr($row['kditem'],6,9) == '000000000' && $row['kdinput'] == '0') { ?>
                          <tr class="dt" style="border: solid 1px white; padding-bottom:0px">
                            <td colspan="10" class="p1 text-bold" style="padding:0px"><?php echo $nmitem ?></td>
                          </tr>

                          <?php } elseif(substr($row['kditem'],9,6) == '000000' && $row['kdinput'] == '0') { ?>
                          <tr class="dt">
                            <td colspan="10" class="p1" style="border: solid 1px white; padding:0px">
                              <?php echo $nmitem ?></td>
                          </tr>
                          <?php } elseif($row['kdinput'] == '1') { ?>
                            <tr class="row" style="border: solid 1px white">
                            <div class="col-md-6">
                               <td class="p1 w1"><?php echo $nmitem ?></td>
                              <td><button type="button" class="btn btn-sm btn-primary pull-right" onclick="parsing('<?=$row['kditem']?>')" >Lihat Rincian Data</button></td>
                            </div>
                            </tr>
                          <?php } ?>
                        </table>
                      </div>
                    </td>
                  </tr>
                <?php }  ?>
              <?php }  ?>
              </tbody>
            </table>
          </div>
        </section>
      </div>
    </div>
  </div>
</div>

<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Rincian Per Item Survei</h4>
      </div>
      <div class="modal-body form-group">
        <input type="text" id="kditem" name="kditem">
          <div class="container">
            <table id="iGrid" class="table table-hover table-bordered">
              <thead>
                <tr>
                  <th style="width: 40px" class="text-left">Kode Lokasi</th>
                  <th style="width: 30%" class="text-left">Nama Lokasi</th>
                  <th class="text-left">R1 Harga</th>
                  <th class="text-left">R2 Harga</th>
                  <th class="text-left">R3 Harga</th>
                  <th class="text-left">Min</th>
                  <th class="text-left">Max</th>
                  <th class="text-left">Average</th>
                </tr>
              </thead>
              <tbody style="margin:10px">
              <?php foreach($table2 as $key=> $row) {?>
                <tr>
                  <td><?= $row['kdlokasi'] ?></td>
                  <td><?= $row['nmlokasi'] ?></td>
                  <td ><?php echo number_format($row['r1_harga'], 0, ',', '.') ?></td>
                  <td><?php echo number_format($row['r2_harga'], 0, ',', '.') ?></td>
                  <td><?php echo number_format($row['r3_harga'], 0, ',', '.') ?></td>
                  <td><?php echo number_format(min($row['r1_harga'],$row['r2_harga'],$row['r3_harga']),0,',','.')  ?></td>
                  <td><?php echo number_format(max($row['r1_harga'],$row['r2_harga'],$row['r3_harga']),0,',','.')  ?></td>
                  <td><?php if($row['r1_harga']>0 && $row['r2_harga']>0 && $row['r3_harga']>0) echo number_format(array_sum(array($row['r1_harga'],$row['r2_harga'],$row['r3_harga']))/ count(array($row['r1_harga'],$row['r2_harga'],$row['r3_harga'])),0,',','.') ;
                  ?>
                  </td>
                </tr>
              <?php } ?>
              </tbody>
            </table>
          </div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  $('.collapse').on('show.bs.collapse', function () {
    $('.collapse.in').collapse('hide');
  });
  function parsing(kditem){
    var kditem = kditem;
    window.location.href = "<?php echo site_url('sb_survei/sb_rincian_dja') ?>" +'/' +kditem;
  }
</script>