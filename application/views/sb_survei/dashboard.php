<link rel="stylesheet" href="<?= base_url('assets/plugins/Leaflet/leaflet.css'); ?>" />
<link rel="stylesheet" href="<?= base_url('assets/plugins/Leaflet-MiniMap-master/src/Control.MiniMap.css'); ?>" />

<script src="<?= base_url('assets/plugins/Leaflet/leaflet.js'); ?>"></script>
<script src="<?= base_url('assets/plugins/Leaflet-MiniMap-master/src/Control.MiniMap.js'); ?>"></script>

<style>
   #mapindonesia {
      height: 460px;
   }

   .info {
      padding: 6px 8px;
      font: 12px/14px Arial, Helvetica, sans-serif;
      background: white;
      background: rgba(255, 255, 255, 0.8);
      box-shadow: 0 0 15px rgba(0, 0, 0, 0.2);
      border-radius: 0px;
   }

   .info h4 {
      margin: 0 0 5px;
      color: #777;
   }

   .legend {
      text-align: left;
      line-height: 15px;
      color: #555;
   }

   .legend i {
      width: 15px;
      height: 15px;
      display: inline-block;
      margin-right: 3px;
      opacity: 0.7;
   }
</style>

<div class="row">

   <div class="col-md-6">
      <div class="dsw-info-box" style="padding-right:45px">
         <span class="dsw-info-box-icon bg-white"><i class="fa fa-street-view text-orange"></i></span>
         <div id="text-carousel" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner" style="position:absolute; left:45px">
               <?php
               $i = 1;
               foreach ($d_rss as $row) {
               ?>
                  <div class="item <?php if ($i == 1) echo 'active' ?>">
                     <div class="carousel-content dsw-info-box-content" style="margin-left:-10px">
                        <span class="dsw-info-box-text">
                           <b><?php echo $row['site'] ?></b> <?php echo $this->fc->idtgl($row['pubdate'], 'full') ?>
                        </span>
                        <span class="dsw-info-box-text">
                           <a href="<?php echo $row['link'] ?>" target="_blank"><?php echo $row['berita'] ?></a>
                        </span>
                     </div>
                  </div>
               <?php
                  $i++;
               }
               ?>
            </div>
         </div>
      </div>
   </div>

   <div class="col-md-6">
      <div class="dsw-info-box">
         <span class="dsw-info-box-icon bg-white"><i class="fa fa-heartbeat text-red"></i></span>
         <div class="dsw-info-box-content" style="padding-left:0px">
            <span class="dsw-info-box-text">
               <?php
               if ($notif) {
                  echo $notif[0]['notif1'];
               ?>
            </span>
            <span class="dsw-info-box-text text-danger">
            <?php
                  echo $notif[0]['notif2'];
               }
            ?>
            </span>
         </div>
      </div>
   </div>
</div>

<div class="row">
   <div class="col-md-12">
      <div id="mapindonesia">
         <span class="dsw-info-box-content pull-right text-bold text-blue" style="font-size:40px;margin-top:-4px">
            <?php
            // print_r($jml);exit();
            if ($jml) {
               if (max(array_keys($jml)) == 3) {
                  echo round($jml[2]['persen'], 2) . '%';
               } else {
                  echo '0%';
               }
            }
            ?>
         </span>
      </div>
   </div>
</div>

<div class="box box-widget">
   <div class="box-body">
      <span class="text-blue"><b>Standar Biaya Masukan 2022</b></span>
      <br>
      Standar Biaya Masukan Tahun Anggaran 2022 adalah satuan biaya berupa harga satuan, tarif, dan indeks yang ditetapkan untuk menghasilkan biaya komponen keluaran dalam penyusunan rencana kerja dan anggaran kementerian negara/lembaga Tahun Anggaran 2022.
   </div>
</div>

<script>
   var style = {
      "fillColor": "#ff7800",
      "fillOpacity": 0.8,
      "stroke": true,
      "color": "#03f",
      "weight": 1,
   };

   var geojson;
   var map = L.map('mapindonesia').setView([-2.721933, 116.593895], 5);
   // var osm2 = new L.TileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {minZoom: 0, maxZoom: 13, attribution: 'osm'});
   // var miniMap = new L.Control.MiniMap(osm2, { toggleDisplay: true, position: 'bottomleft'}).addTo(map);

   $.ajax({
      dataType: "json",
      // call data
      url: "<?php echo site_url('sb_survei/dash_map'); ?>",
      success: function(datasql) {
         indonesialayer(datasql);
      }
   });
   legenda();

   function indonesialayer(datasql) {
      $.ajax({
         dataType: "json",
         url: "<?= base_url('assets/plugins/Leaflet/provinsi.json'); ?>",
         success: function(data) {
            geojson = L.geoJSON(data, {
               onEachFeature: onEachFeature,
               style: style
            }).addTo(map);

            function onEachFeature(feature, layer) {
               var provinsi, persentase, sts, ketstatus;

               layer.on({
                  mouseover: highlightFeature,
                  mouseout: resetHighlight,
               });
               for (var i = 0; i < datasql.provinsi.length; i++) {
                  if (datasql.provinsi[i].ID == feature.properties.ID2013) {
                     sts = parseInt(datasql.provinsi[i].KDSTATUS);
                     if (sts <= 0) {
                        layer.setStyle({
                           fillColor: "#fff"
                        });
                        ketstatus = "Belum rekam data"
                     } else if (sts > 0 && sts <= 1) {
                        layer.setStyle({
                           fillColor: "#F4C400"
                        });
                        ketstatus = "Selesai rekam data hasil survei"
                     } else if (sts > 1 && sts <= 2) {
                        layer.setStyle({
                           fillColor: "#0083E8"
                        });
                        ketstatus = "Kirim data hasil survei ke DJA"
                     }
                     provinsi = datasql.provinsi[i].PROVINSI;
                  }
               }

               var html = "<b>" + provinsi + "</b><br>";
               html += "Status : " + ketstatus;
               layer.bindPopup(html);
            }
         }
      });
   }
   //fungsi menampilkan legenda
   function legenda() {
      var legend = L.control({
         position: 'bottomleft'
      });
      legend.onAdd = function(map) {
         var div = L.DomUtil.create('div', 'info legend'),
            grades = [0, 1, 2],
            labels = ['Belum Rekam', 'Selesai Rekam', 'Kirim data ke DJA']; // kebutuhan data
         jlbl = [<?php foreach ($jml as $key) {
                     echo $key['jml'] . ' ,';
                  } ?>];
         for (var i = 0; i < grades.length; i++) {
            div.innerHTML +=
               '<i style="background:' + getColor(grades[i] + 1) + '">&nbsp</i> ' +
               labels[i] + '<b>&nbsp;' + jlbl[i] + '</b>' + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
         }
         return div;
      };

      legend.addTo(map);
   }


   //fungsi menampilkan warna sesuai nilai
   function getColor(d) {
      return d > 3 ? '#77DD00' :
         d > 2 ? '#0083E8' :
         d > 1 ? '#F4C400' :
         '#ECF0F5';
   }

   function highlightFeature(e) {
      var layer = e.target;

      layer.setStyle({
         weight: 2,
         color: '#ff0000',
         dashArray: '',
         fillOpacity: 0.7
      });

      if (!L.Browser.ie && !L.Browser.opera && !L.Browser.edge) {
         layer.bringToFront();
      }
   }

   function resetHighlight(e) {
      var layer = e.target;

      layer.setStyle({
         stroke: true,
         color: "#03f",
         weight: 1,
      })
   }
</script>