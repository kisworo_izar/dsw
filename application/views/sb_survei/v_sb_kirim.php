<link href="<?=base_url(); ?>assets/plugins/uploadfile/fileinput.css" rel="stylesheet">
<script src="<?=base_url();?>assets/plugins/uploadfile/fileinput.min.js" type="text/javascript"></script>
<script src="<?= base_url('assets/plugins/jQuery-Mask/dist/jquery.mask.min.js') ?>" type="text/javascript"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>


<style media="screen">
th{text-align: center;background: #E7E9EE;color:#34495e} td a {display:block;width:100%;}
  .calculated-width {width: -moz-calc(100% - 50px);width: -webkit-calc(100% - 50px);width: calc(100% - 80.9px);}​
  .table-bordered > thead > tr > th,
  .table-bordered > thead > tr > td {border-bottom: 0px solid #E7E9EE;}
  .table-bordered > tbody > tr > td {padding:3px}
  .tbl_h {text-align: center; font-weight: bold; border: solid 1px #9ECEC5}
  .dt {padding:0px 5px;} .hiddenRow {padding: 0 !important;}
  .info{border-top:solid 0px #E7E9EE;padding:0px;margin:0px}
  .x { display: inline-block; width: 650px; }
  .p3 {padding:3px} .p1 {padding:3px; border-right:solid 0px #9ECEC5}
  .w1 {width:25%} .w2 {width:12%} .w3 {width:15px}
  .hijau{background: #27ae60; color:white} .biru{background: #3498db; color:white}
  .abu{background: white; color:#bdc3c7} .merah{background: #e74c3c; color:white}
  .green{background: white; color: green}
  section {position: relative;} section.positioned {position: absolute;top:10px;left:10px;}
  .container {overflow-y: auto;padding: 0px;padding-top: 14px;width: 100%}
  table {border-spacing: 1;width:100%;}
  th {height: 0;line-height: 0;padding-top: 0;padding-bottom: 0;color: #000;border: none;white-space: nowrap;}
  th div{position: absolute;background:#E7E9EE;color:black;border-bottom: solid #E7E9EE 0px;border-top: solid 0px #E7E9EE; padding: 0px 0px;top: 0;line-height: normal;} input {width: 100%; border: 0px!important}
  th{text-align: center;background: #E7E9EE;color:#34495e} td a {display:block;width:100%;}
  /*.table-bordered > thead > tr > th, .table-bordered > thead > tr > td {border-bottom: 2px solid #E7E9EE;}
  .table-bordered > tbody > tr > td {padding:3px}*/
  .td_sts_hijau {width: 75px; background:#27AE60; color:white; text-align:center; padding-top: 5px !important}
  .td_sts_abu {width: 75px; background:#DEDEDE; color:white; text-align:center; padding-top: 5px !important}
  .tbl_h {text-align: center; font-weight: bold; border: solid 1px #9ECEC5}
  .dt {padding:0px 5px;} .hiddenRow {padding: 0 !important;}
  .info{border-top:solid 1px #E7E9EE;padding:0px;margin:0px}
  .x { display: inline-block; width: 650px; } .p3 {padding:3px} .p1 {padding:3px; border-right:solid 1px #9ECEC5}
  .abu{background: white; color:#bdc3c7} .merah{background: #e74c3c; color:white}
  section {position: relative;} section.positioned {position: absolute;top:10px;left:10px;}
  .container {overflow-y: auto;padding: 0px;width: 100%} table {border-spacing: 1;width:100%;}
  th {height: 0;line-height: 0;padding-top: 0;padding-bottom: 0;color: #000;border: none;white-space: nowrap;}
  th div{position: absolute;background:#E7E9EE;color:black;border-bottom: solid #E7E9EE 1px;border-top: solid 1px #E7E9EE; padding: 6px 0px;top: 0;line-height: normal;} input {width: 100%; border: 0px!important}
</style>

<div class="row">
  <div class="col-md-12">
    <div class="box box-widget">
      <div class="box-header with-border">
         <div class="span5 col-md-8 pull-left" style="border:0px; padding:0px;">
           <h4 style="margin:6px 0px; color:#337AB7 !important ">Monitoring Proses</h4>
         </div>
      </div>
      <div class="box-body">
        <section class="">
          <div class="container" style="height:650px">
    				<table id="iGrid" class="table table-hover table-bordered">
    					<thead>
                <tr style="padding-left:10px">
                  <th colspan="2"style="padding: 5px" class="text-left">Kode dan Uraian Kantor Wilayah</th>
                  <th style="padding: 5px" class="text-center small">Data Selesai Direkam</th>
                  <th style="padding: 5px" class="text-center small">Data Dikirim ke DJA</th>
                </tr>
    					</thead>

    			    <tbody>
              <?php 
              foreach ($table as $key=>$row)  { ?>

                <tr style="margin:0px">
                  <td style="width:40px" data-toggle="collapse" data-target=".<?= $row['kdlokasi'] ?>" class="text-center text-bold text-primary"><?= $row['kdlokasi'] ?></td>
                  <td style="border:solid 0px;border-bottom:solid 1px #F4F4F4" data-toggle="collapse" data-target=".<?= $row['kdlokasi'] ?>"><?= $row['nmlokasi'] ?></td>
                  <td class="<?php if($row['kdstatus']>'0') echo "td_sts_hijau"; elseif($row['kdstatus']<'1') echo "td_sts_abu";  ?> small"><?= $this->fc->idtgl($row['tglrekam'],'hr') ?></td>
                  <td class="<?php if($row['kdstatus']=='2') echo "td_sts_hijau"; elseif($row['kdstatus']<'2') echo "td_sts_abu";  ?> small"><?= $this->fc->idtgl($row['tglkirim'],'hr') ?></td>
                </tr>

                  <tr style="background:#FFF;color:#34495E">
                    <td class="hiddenRow" colspan="8">
                      <div class="small collapse info <?= $row['kdlokasi'] ?>" style="padding:7px;background:#FDFDFD; border: solid 1px #9ECEC5;">
                        <h5 style="margin:3px 0px 7px 0px; color:#2D608C"><b>Kanwil <?= $row['kdlokasi'] ?> <?= $row['nmlokasi'] ?></b></h5>
                        <div>
                          <table style="margin-bottom:5px">
                            <tbody>
                              <tr>
                                <td style="width:110px">Data Selesai Direkam</td>
                                <td>: <?= $row['tglrekam'] ?></td>
                                <td style="width:110px">Status Proses</td>
                                <td class="text-primary">: <b><?php if($row['kdstatus']=='1') echo "Data Selesai Direkam"; elseif($row['kdstatus']>'1') echo "Data Dikirim ke DJA"; else echo "Data Belum Direkam";  ?></b></td>
                              </tr>
                              <tr>
                                <td style="width:110px">Data Dikirim Ke DJA</td>
                                <td>: <?= $row['tglkirim'] ?></td>
                                <td style="width:110px">Surat Pengantar</td>
                                <td class="text-primary"><a href="<?php site_url() ?>/files/sb_survei/2019/<?=$row['kdlokasi'] ?>/<?=$row['file'] ?>" ><b>: <?= $row['file']?></b></a></td>
                              </tr>
                              <tr>
                                <td colspan="3" type="button" style="width:110px" onclick="rincian('<?= $row['kdlokasi'] ?>')"><button type="button" class="btn btn-xs btn-info">Lihat Rincian Data</button></td>
                                <td class="pull-right">
                                  <button style="margin-top:3px" type="button" class="btn btn-xs btn-warning" id="batal"  value="batal"  onclick="batal('<?= $row['kdlokasi'] ?>')">Batal Terima Data</button>
                                  <button style="margin-top:3px" type="button" class="btn btn-xs btn-primary" id="simpan" value="simpan" onclick="simpan('<?= $row['kdlokasi'] ?>')">Kirim Data ke DJA</button>
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </td>
                  </tr>
                <?php } ?>
    					</tbody>
    				</table>
          </div>
        </section>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  $('.collapse').on('show.bs.collapse', function () {
    $('.collapse.in').collapse('hide');
  });

  function simpan(lok) {
    var lokasi = lok;
    var isian  = 'kdlokasi='+lok+'&value='+$('#simpan').val();
    $.ajax({
      url  : "<?= site_url('sb_survei/kirim_dja') ?>",
      type : 'POST',
      data : isian,
    }).done(function(){
      swal({
        title:"Sukses",
        text: "Data Survei Berhasil dikirm ke DJA !",
        timer: 4000,
        showConfirmButton: false
      }).then(function () {
        window.location.reload();
      })
  });
  }

  function rincian(lok){
    var lokasi = lok;
    window.location.href = "<?php echo site_url('sb_survei/sb_survei_kirim_rincian') ?>" +'/' +lokasi;
  }

  function batal(lok) {
    var lokasi = lok;
    var isian  = 'kdlokasi='+lok+'&value='+$('#batal').val();
    $.ajax({
      url  : "<?= site_url('sb_survei/kirim_dja') ?>",
      type : 'POST',
      data : isian
    }).done(function(){
      window.location.reload();
    })
  }
</script>
