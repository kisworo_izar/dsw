<style media="screen">
  .form-group { margin: 0px;}
  .btn-crud { margin-top: 10px;}
</style>


<div class="box box-widget" style="margin: 0px 0px 0px 0px;">
  <div class="box-body">

    <form action="" enctype="multipart/form-data">
      <div class="col-sm-2 pull-right"><div>&nbsp;</div></div>
      <div class="col-sm-1 pull-left"><div>&nbsp;</div></div>

      <div class="col-sm-9 pull-left">
        <div class="form-group">
          <label class="col-sm-2 text-right">Thang</label>
          <div class="col-sm-10">
            <input id="0" name="thang" type="text" class="form-control" readonly value="">
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-2 text-right">Kode Lokasi</label>
          <div class="col-sm-10">
            <input id="1" name="kdlokasi" type="text" class="form-control" readonly value="">
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-2 text-right">Kode Item</label>
          <div class="col-sm-10">
            <input id="2" name="kditem" type="text" class="form-control" placeholder="" value="">
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-2 text-right">Uraian</label>
          <div class="col-sm-10">
            <input id="3" name="uraian" type="text" class="form-control" placeholder="" value="">
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-2 text-right">Aktif</label>
          <div class="col-sm-10">
            <input id="4" name="kdaktif" type="text" class="form-control" placeholder="" value="">
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-2 text-right">Input</label>
          <div class="col-sm-10">
            <input id="5" name="kdinput" type="text" class="form-control" placeholder="" value="">
          </div>
        </div>

        <div class= "form-group text-center" style="padding-top: 10px">
          <button id="Rekam" class="btn btn-warning" onclick="crud( this.id )">Rekam</button>
          <button id="Ubah"  class="btn btn-warning" onclick="crud( this.id )">Ubah</button>
          <button id="Hapus" class="btn btn-warning" onclick="crud( this.id )">Hapus</button>
        </div>
      </div>
    </form>

  </div>
</div>


<script type="text/javascript">
  function crud( aksi ) {
    $('form').submit(function(e){
      e.preventDefault();
      //alert($('form').serialize());
      $.ajax({
        url : "<?php echo site_url('sb_survei/crud_survei_item') ?>",
        type: "POST",
        data: $('form').serialize() +'&aksi='+ aksi,
        success: function(pesan) {
          var arr = pesan.split("#"), key = 'id_'+arr[1];
          if (aksi=='Rekam') { window.location.href = "<?php echo site_url('sb_survei/sb_survei_item') ?>" }
          if (aksi=='Ubah') {
            $('#'+key).data('list', pesan);
            $('#'+key).children( 'td:nth-child(1)' ).text( arr[1] );
            $('#'+key).children( 'td:nth-child(2)' ).text( arr[2] );
            $('#'+key).children( 'td:nth-child(3)' ).text( arr[3] );
            $('#'+key).children( 'td:nth-child(4)' ).text( arr[4] );
            $('#'+key).children( 'td:nth-child(5)' ).text( arr[5] );
            move_row( key );
          }
          if (aksi=='Hapus') {
            move_row( key );
            $('table#iGrid tr#'+key).remove();
          }
        }
      });
    });
  }
</script>
