<link href="<?=base_url(); ?>assets/plugins/uploadfile/fileinput.css" rel="stylesheet">
<script src="<?=base_url();?>assets/plugins/uploadfile/fileinput.min.js" type="text/javascript"></script>
<script src="<?=base_url();?>assets/plugins/jQuery-Mask/dist/jquery.mask.min.js" type="text/javascript"></script>
<script src="<?=base_url();?>assets/plugins/jQuery/jquery.maskMoney.min.js" type="text/javascript"></script>
 <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<style media="screen">
  th{text-align: center;background: #E7E9EE;color:#34495e} td a {display:block;width:100%;}
  .calculated-width {width: -moz-calc(100% - 50px);width: -webkit-calc(100% - 50px);width: calc(100% - 50px);}​
  A:link,A:visited,A:active,A:hover {text-decoration: none; color: white;}
  .table-bordered > thead > tr > th,
  .table-bordered > thead > tr > td {border-bottom: 0px solid #E7E9EE;}
  .table-bordered > tbody > tr > td {padding:3px}
  .tbl_h {text-align: center; font-weight: bold; border: solid 1px #9ECEC5}
  .dt {padding:0px 5px;} .hiddenRow {padding: 0 !important;}
  .info{border-top:solid 0px #E7E9EE;padding:0px;margin:0px}
  .x { display: inline-block; width: 650px; }
  .p3 {padding:3px} .p1 {padding:3px; border-right:solid 0px #9ECEC5}
  .w1 {width:25%} .w2 {width:12%} .w3 {width:15px}
  .hijau{background: #27ae60; color:white} .biru{background: #3498db; color:white}
  .abu{background: white; color:#bdc3c7} .merah{background: #e74c3c; color:white}
  .green{background: white; color: green}
  section {position: relative;} section.positioned {position: absolute;top:10px;left:10px;}
  .container {overflow-y: auto;padding: 0px;padding-top: 14px;width: 100%}
  table {border-spacing: 1;width:100%;}
  th {height: 0;line-height: 0;padding-top: 0;padding-bottom: 0;color: #000;border: none;white-space: nowrap;}
  th div{position: absolute;background:#E7E9EE;color:black;border-bottom: solid #E7E9EE 0px;border-top: solid 0px #E7E9EE; padding: 6px 0px;top: 0;line-height: normal;} input {width: 100%; border: 0px!important}
  input:read-only {
    background-color: #bdc3c7;
  }
</style>

<div class="row" ng-app="app" >
  <div class="col-md-12">
    <div class="box box-widget">
      <div class="box-header with-border">
         <div class="span5 col-md-8 pull-left" style="border:0px; padding:0px;">
           <h4 style="margin:6px 0px; color:#337AB7 !important ">Data Hasil Survei </h4>
         </div>
         <div class="span5 col-md-4 pull-right" id="sandbox-container" style="border:0px; padding:0px;">
           <button type="button" class="btn btn-sm btn-primary pull-right" onclick="go_back()">Kembali</button>
         </div>
      </div>
      <div class="box-body">
        <section class="">
          <div class="container">
            <table id="iGrid" class="table table-hover table-bordered">
              <thead>
                <tr>
                  <th style="width:50px;padding-left:0px">
                    <div style="width:50px;text-align:center;border-left:solid 1px #E7E9EE">ID</div></th>
                  <th colspan="3" style="padding:0px;padding-left:0px">
                    <div class="calculated-width" style="text-align:left;padding-left:2px">Satuan Biaya</div></th>
                </tr>
              </thead>
              <tbody style="margin:10px">

                <?php foreach ($table2 as $key=>$row) {
                    $icon       = ''; if ($row['kdisi'] !='0') { $icon='hijau'; }
                    $readonly   = ''; if ($row['kdisi'] !='0') $readonly = 'readonly' ;
                    $iditem     = substr($row['kditem'],0,3);
                    $nmitem     = $row['uraian'];
                    $stsdata    = ''; if ($row['kdcheck']=='1') $stsdata = 'green';
                ?>

                <?php if (substr($row['kditem'],3,12) == '000000000000' && $row['kdinput'] == '0') { ?>
                  <tr style="margin:0px" id="<?php $iditem ?>">
                    <td class="text-bold text-primary text-center" data-toggle="collapse" data-target=".<?php echo $iditem ?>">
                      <?= $iditem ?></td>
                    <td style="border:solid 0px;border-bottom:solid 0px #F4F4F4" data-toggle="collapse" data-target=".<?php echo $iditem ?>">
                      <div><?= $nmitem ?></div>
                    </td>
                    <td style="padding:0px; width:30px" class="text-center">
                    <?php if($row['file']!=null) { ?>
                    <a href="<?php site_url() ?>/files/sb_survei/2021/<?=$row['kdlokasi'] ?>/<?=$row['kditem'] ?>_<?= $row['file']  ?>" ><?php } ?><i class="fa fa-download Keterangan text-info" title="Download File Per Item"></i></a>
                    
                    </td>
                    <td style="padding:0px; width:30px" class="text-center">
                      <i class="fa fa-lg fa-check <?php echo  $stsdata; ?>" data-toggle="tooltip" title="Status Data"></i>
                    </td>
                  </tr>
                <?php } else { ?>

                  <tr>
                    <td class="hiddenRow" colspan="8" style="border:solid 0px">
                      <div class="small collapse info <?php echo $iditem ?>" style="padding:0px 7px;background:#FDFDFD">
                        <table border="1" style="padding:3px; margin:1px 0px">
                          <?php if (substr($row['kditem'],6,9) == '000000000' && $row['kdinput'] == '0') { ?>
                          <tr class="dt" style="border: solid 1px white; padding-bottom:0px">
                            <td colspan="10" class="p1 text-bold" style="padding:0px"><?php echo $nmitem ?></td>
                          </tr>

                          <?php } elseif(substr($row['kditem'],9,6) == '000000' && $row['kdinput'] == '0') { ?>
                          <tr class="dt">
                            <td colspan="10" class="p1" style="border: solid 1px white; padding:0px">
                              <?php echo $nmitem ?></td>
                          </tr>
                          <?php } elseif($row['kdinput'] == '1') { ?>
                            <tr class="dt" style="border: solid 1px #9ECEC5">
                              <td class="p1 w1"><?php echo $nmitem ?></td>
                              <td class="p1 w2"><input id="<?= 'inp1_'.$row['kditem'] ?>" type="text" placeholder="Responden 1 ..." value="<?php echo $row['r1_nama'] ?>" readonly />
                              </td>
                              <td class="p1 w2"><input id="<?= 'inp2_'.$row['kditem'] ?>" style="text-align:right" type="text" placeholder="... Nilai 1" value="<?php echo number_format($row['r1_harga'], 0, ',', '.') ?>" readonly />

                              </td>
                              <td class="p1 w2"><input id="<?= 'inp3_'.$row['kditem']?>" type="text" placeholder="Responden 2 ..." value="<?php echo $row['r2_nama'] ?>" readonly /></td>
                              <td class="p1 w2"><input id="<?= 'inp4_'.$row['kditem']?>" style="text-align:right" type="text" placeholder="... Nilai 2" value="<?php echo number_format($row['r2_harga'], 0, ',', '.') ?>" readonly /></td>
                              <td class="p1 w2"><input id="<?= 'inp5_'.$row['kditem']?>" type="text" placeholder="Responden 3 ..." value="<?php echo $row['r3_nama'] ?>" readonly /></td>
                              <td class="p1 w2"><input id="<?= 'inp6_'.$row['kditem']?>" style="text-align:right" type="text" placeholder="... Nilai 3" value="<?php echo number_format($row['r3_harga'], 0, ',', '.') ?>" readonly/></td>  
                              <td class="<?php echo $icon; ?>"></td>
                             </tr>
                          <?php } ?>
                        </table>
                      </div>
                    </td>
                  </tr>
                <?php }  ?>
                <?php }  ?>
              </tbody>
            </table>
          </div>
        </section>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  function go_back(){
    window.history.back();
  }
</script>