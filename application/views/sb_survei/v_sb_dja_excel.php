<table border="1">
  <thead>
    <tr style="background: #00acd6; color: #fff;">
      <th style="width: 40px" class="text-left">KdLokasi</th>
      <th hidden-xs hidden-sm>Nama Lokasi</th>
      <th class="text-left">R1 Harga</th>
      <th class="text-left">R2 Harga</th>
      <th class="text-left">R3 Harga</th>
      <th class="text-left">Min</th>
      <th class="text-left">Max</th>
      <th class="text-left">Average</th>
    </tr>
  </thead>
  <tbody style="margin:10px">
    <?php foreach($table as $key=> $row) {?>
    <tr>
      <td><?= $row['kdlokasi'] ?></td>
      <td><?= $row['nmlokasi'] ?></td>
      <td><?php echo number_format($row['r1_harga'], 0, ',', '.') ?></td>
      <td><?php echo number_format($row['r2_harga'], 0, ',', '.') ?></td>
      <td><?php echo number_format($row['r3_harga'], 0, ',', '.') ?></td>
      <td><?php echo number_format(min($row['r1_harga'],$row['r2_harga'],$row['r3_harga']),0,',','.')  ?></td>
      <td><?php echo number_format(max($row['r1_harga'],$row['r2_harga'],$row['r3_harga']),0,',','.')  ?></td>
      <td><?php if($row['r1_harga']>0 && $row['r2_harga']>0 && $row['r3_harga']>0) echo number_format(array_sum(array($row['r1_harga'],$row['r2_harga'],$row['r3_harga']))/ count(array($row['r1_harga'],$row['r2_harga'],$row['r3_harga'])),0,',','.') ;
      ?>
      </td>
    </tr>
    <?php } ?>
  </tbody>
</table>