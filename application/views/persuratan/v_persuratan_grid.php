<!DOCTYPE html>
<html lang="en">
<head>
  <?php $this->load->view('main/utama_link'); ?>
  <style media="screen">
    table {
      font-family: 'Arial';
      margin: 0 auto;
      border-collapse: collapse;
      border: 1px solid #eee;
      border-bottom: 2px solid #00cccc;
      box-shadow: 0px 0px 10px rgba(0,0,0,0.10),
       0px 10px 20px rgba(0,0,0,0.05),
       0px 20px 20px rgba(0,0,0,0.05),
       0px 20px 20px rgba(0,0,0,0.05);}

      th {
      background: #00cccc;
      color: #fff;
      text-transform: uppercase;
      font-size: 12px;
      &.last {border-right: none;}
    }

    input.transparent-input{
      background-color:transparent !important;
      border:none !important;
    }
  </style>
<script type="text/javascript">
  var myHilitor2;

  document.addEventListener("DOMContentLoaded", function() {
    myHilitor2 = new Hilitor("playground");
    myHilitor2.setMatchType("open");
    myHilitor2.apply("<?php echo $param->cari ?>");
  }, false);
</script>

</head>
<body>

  <nav class="navbar navbar-default navbar-fixed-top" style="margin-bottom: 10px;">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#" style="width: 50px; padding: 8px 10px;">
            <img alt="DJA" src="<?php echo base_url(); ?>assets/images/depkeu.jpg" width="35px;"></a>
        </div>

        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" style="padding-left: 5px; padding-right: 5px;"><?php echo $param->tahun ?><span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                      <?php 
                        foreach ($tahun as $row) {
                          $url = site_url("monitor/goto_grid/$row->tahun/$param->bulan/$param->bagian/$param->status");
                          echo "<li><a href='$url'> $row->tahun </a></li>";
                        }
                      ?>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" style="padding-left: 5px; padding-right: 5px;"><?php echo $bulan[(int)$param->bulan] ?><span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                      <?php 
                        for ($i=0; $i<count($bulan); $i++) {
                          $url = site_url("monitor/goto_grid/$param->tahun/". sprintf('%02d', $i) ."/$param->bagian/$param->status");
                          echo "<li><a href='$url'>". $bulan[$i] ."</a></li>";
                        }
                      ?>
                    </ul>
                </li>
            </ul>

            <ul class="nav navbar-nav" style="width: 205px; margin-top: 16px;">
                <li class="dropdown">
                     <input class="form-control easyui-combotree transparent-input" style="width: 205px; border-radius:0px" value="<?php echo $param->bagian ?>" data-options="url:'<?php echo site_url("monitor/json_tree_esl2"); ?>',method:'get',required:true">
                </li>
            </ul>

            <form class="navbar-form navbar-left" role="search" action="<?php echo site_url("monitor/cari/$param->link") ?>" method="post" style="padding-left: 3px; padding-right: 3px;">
                <input name="nilai" type="hidden" value="<-?php echo $param->nilai; ?>" />
                <div class="form-group">
                    <input type="text" name="cari" class="form-control" placeholder="Isikan hal yang dicari" value="<?php echo $param->cari; ?>">
                </div>
                <button type="submit" name="tombol" value="Cari" data-toggle="tooltip" title="Cari" class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button>
                <button type="submit" name="tombol" value="Clear" data-toggle="tooltip" title="Clear" class="btn btn-default"><span class="glyphicon glyphicon-remove-circle"></span></button>
            </form>
            <ul class="nav navbar-nav navbar-right">
                <div class style="margin-right: 15px;">
                    <a class="btn btn-sm btn-info navbar-btn" role="button" href="<?php echo site_url("/monitor/goto_grid/$param->tahun/$param->bulan/$param->bagian/1/$param->rowstart") ?>">Selesai <span class="badge"><?php echo $badge[1] ?></span></a>
                    <a class="btn btn-sm btn-success navbar-btn" role="button" href="<?php echo site_url("/monitor/goto_grid/$param->tahun/$param->bulan/$param->bagian/2/$param->rowstart") ?>">Proses <span class="badge"><?php echo $badge[2] ?></span></a>
                    <a class="btn btn-sm btn-warning navbar-btn" role="button" href="<?php echo site_url("/monitor/goto_grid/$param->tahun/$param->bulan/$param->bagian/3/$param->rowstart") ?>">Segera <span class="badge"><?php echo $badge[3] ?></span></a>
                    <a class="btn btn-sm btn-danger navbar-btn" role="button" href="<?php echo site_url("/monitor/goto_grid/$param->tahun/$param->bulan/$param->bagian/4/$param->rowstart") ?>">Terlambat <span class="badge"><?php echo $badge[4] ?></span></a>
                	<a href="<?php echo site_url("login/logout_user") ?>" value="Logout" class="btn btn-default" role="button" data-toggle="tooltip" title="User Logout"><span class="glyphicon glyphicon-log-out"></span></a>
                </div>
            </ul>
        </div>
    </div>
  </nav>


  <div class="iGrid">
      <table id="GridRekap" class="table table-bordered table-condensed table-striped">

        <thead>
          <tr style="background-color: #d8ebf1;">
            <th style="width: 8%" class="text-center"> TANGGAL </th>
            <th style="width:15%" class="text-center hidden-phone hidden-tablet"> NO. AGENDA <br> <i>DISPOSISI</i></th>
            <th style="width:10%" class="text-center"> SURAT PENGIRIM <br> SURAT KELUAR</th>
            <th style="width:35%" class="text-center"> HAL <br> <i>URAIAN DISPOSISI</i></th>
            <th style="width:32%" class="text-center hidden-phone hidden-tablet"> PENGIRIM <br> <i>CATATAN DISPOSISI </th>
          </tr>
        </thead>
        <tbody id="playground">

          <?php
          if ($surat) {
            $no= $param->rowstart + 1;

            foreach ($surat as $row) {
              $url = site_url("/monitor/goto_grid/1/$param->tahun/$param->bulan");
              $golsurat = 'ND-'; 
              	if ($row['golsurat']=='1' and $row['jenis']=='10') $golsurat = 'RSM-'; 
              	if ($row['golsurat']=='2') $golsurat = 'UND-';
              $tgagenda = date('d-m-Y',strtotime($row['tgagenda'])); if($row['tgagenda']=="01-01-1970") $tgagenda="&nbsp;";
              $selesai  = date('d-m-Y',strtotime($row['selesai']));   if($row['selesai']=="0000-00-00")  $selesai="&nbsp;";
              $tgsurat  = date('d-m-Y',strtotime($row['tgsurat']));  if($row['tgsurat']=="01-01-1970")  $tgsurat="&nbsp;";
              $perihal  = $row['perihal'] . $row['perihal2'];
              $warna    = array('#F9F9F9','#5BC0DE','#49A049','#EB9316','#CE4844');
              if ($row['nmfile'] && $this->session->userdata('kodelink')=='1') {
                  //$perihal = "<a href='". file_put_contents($row['nmfile'], fopen("\\\\10.242.42.62\\suratdja_repo\\2015\\". $row['nmfile'], 'r')) ."' target='_blank'>$perihal</a>";
                  $perihal = "<a href='". site_url("/monitor/open_pdf/". $row['nmfile']) ."' target='_blank'>". $row['perihal'] ."</a>". $row['perihal2'];
              }

              $catatan = '';
              if ($row['catatantup']) { $catatan .= '<br><label style="color:#EC9A25">'. $row['catatantup']; }
              if ($row['catatan']) { $catatan .= '<br><label style="color:#EC9A25">'. $row['catatan']; }
              if ($row['catatan1']) { $catatan .= '<br><label style="color:#EC9A25">'. $row['catatan1']; }
             
              $tlnosurat = '';
              if ($row['tlnosurat']) { $tlnosurat .= '<br /><br /><label style="color:#419641">'. $row['tlnosurat']; }

          ?>
              <tr>
                  <td class="text-center" > <?php echo "<b>$tgagenda</b><br><h4 style='color:". $warna[$row['nilai']] ."'>". $row['status'] ."</h4>"?></td>
                  <td class="text-left hidden-phone hidden-tablet" style="border-left-color: <?php echo $warna[$row['nilai']] ?>; border-left-style: solid; border-left-width: 4px;">
                    <?php echo "<b>$golsurat". $row['noagenda'].$row['noagenda1'] ."</b>". $row['selesai'] . $row['dispos'] ?>
                  </td>
                  <td class="text-left">
                    <?php echo "<b>$tgsurat<br>". $row['nosurat'] ."</b>" . $tlnosurat ?>
                  </td>
                  <td class="text-left content"> <?php echo $perihal ?></td>
                  <td class="text-left hidden-phone hidden-tablet"> <?php echo $row['pengirim'] . $catatan .'</label>' ?></td>
              </tr>
          <?php
              $no++;

            }
          } else {
              echo '<tr><td colspan="5" class="text-center"><span class="danger"><i>Tidak ada data ...</i></span></td></tr>';
          }
          ?>

        </tbody>
      </table>
      <div class="pagination-area">
          <?php echo $this->pagination->create_links(); ?>
          <?php echo "<span class='pull-right'>[ ". $bagian[ $param->bagian ]['uraian2'] .": $record records ]</span>"; ?> 
      </div>

  </div>


</body>
<script>
(function($){
  var combotree = $.fn.combotree.defaults.onChange;
  $.fn.combotree.defaults.onChange = function(newValue, oldValue){
    // alert(newValue);
    var url = '<?php echo site_url("monitor/goto_grid/$param->tahun/$param->bulan/") ?>';
    window.open( url +'/'+ newValue, '_parent' );
  };
})
(jQuery);
</script>
</html>
