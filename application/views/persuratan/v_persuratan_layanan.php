<script src="<?php echo base_url();?>assets/plugins/hilitor/hilitor.js" type="text/javascript"></script>

<link href="<?php echo base_url();?>assets/plugins/easyui/easyui.css" type="text/css" rel="stylesheet"></script>
<script src="<?php echo base_url();?>assets/plugins/easyui/jquery.easyui.min.js" type="text/javascript"></script>

<style media="screen">
    input.transparent-input{
      background-color:transparent !important;
      border:none !important;
    }
</style>

<script type="text/javascript">
    var myHilitor2;

    document.addEventListener("DOMContentLoaded", function() {
      myHilitor2 = new Hilitor("playground");
      myHilitor2.setMatchType("open");
      myHilitor2.apply("<?php echo $param->cari ?>");
    }, false);
</script>

<div class="row">
    <div class="col-md-12">
        <div class="box box-widget">
            <div class="box-body with-border">
                <div class="col-md-5">
                    <form role="form" action="<?php echo site_url("persuratan/cari/$param->link") ?>" method="post">
                        <div class="input-group">
                          <input type="hidden" name="nilai" value="<-?php echo $param->nilai; ?>">
                          <input type="text" name="cari" class="form-control input-sm" placeholder="Pencarian..." value="<?php echo $param->cari; ?>">
                              <span class="input-group-btn">
                                <button type="submit" name="tombol" value="Cari" data-toggle="tooltip" title="Cari" class="btn btn-sm btn-default"><i class="fa fa-search"></i>
                                </button>
                              </span>
                              <span class="input-group-btn">
                                  <button type="submit" name="tombol" value="Clear" data-toggle="tooltip" title="Clear" class="btn btn-sm btn-default"><i class="fa fa-times-circle"></i></button>
                              </span>
                        </div>
                    </form>
                </div>

                <ul class="nav navbar-nav hidden-xs">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" style="padding:5px   5px;"><?php echo $param->tahun ?><span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                          <?php
                            foreach ($tahun as $row) {
                              $url = site_url("persuratan/goto_layanan/$param->model/$row->tahun/$param->bulan/$param->bagian/$param->status");
                              echo "<li><a href='$url'> $row->tahun </a></li>";
                            }
                          ?>
                        </ul>
                    </li>
                    <li class="dropdown" style="padding:0px">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" style="padding:5px 5px;"><?php echo $bulan[(int)$param->bulan] ?><span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                          <?php
                            for ($i=0; $i<count($bulan); $i++) {
                              $url = site_url("persuratan/goto_layanan/$param->model/$param->tahun/". sprintf('%02d', $i) ."/$param->bagian/$param->status");
                              echo "<li><a href='$url'>". $bulan[$i] ."</a></li>";
                            }
                          ?>
                        </ul>
                    </li>
                </ul>

                <ul class="nav navbar-nav  hidden-xs" style="width: 100px;">
                    <li class="dropdown" style="padding:5px">
                         <input class="input-sm form-control easyui-combotree transparent-input" style="width: 200px; border-radius:0px; padding-top:26px" value="<?php echo $param->bagian ?>" data-options="url:'<?php echo site_url("persuratan/json_tree_esl2"); ?>',method:'get',required:true">
                    </li>
                </ul>

                <div class="pull-right hidden-xs" style="margin:5px 0px">
                    <a href="<?php echo site_url("/persuratan/goto_layanan/$param->model/$param->tahun/$param->bulan/$param->bagian/1/$param->rowstart") ?>">
                        <!-- <span class="small text-info hidden-xs">Selesai </span> -->
                        <span class="badge bg-aqua"><?php echo $badge[1] ?></span></a>
                    &nbsp;&nbsp;
                    <a href="<?php echo site_url("/persuratan/goto_layanan/$param->model/$param->tahun/$param->bulan/$param->bagian/2/$param->rowstart") ?>">
                        <!-- <span class="small text-success hidden-xs">Proses </span> -->
                        <span class="badge bg-green"><?php echo $badge[2] ?></span></a> &nbsp;&nbsp;
                    <a href="<?php echo site_url("/persuratan/goto_layanan/$param->model/$param->tahun/$param->bulan/$param->bagian/3/$param->rowstart") ?>">
                        <!-- <span class="small text-warning hidden-xs">Segera </span> -->
                        <span class="badge bg-yellow"><?php echo $badge[3] ?></span></a> &nbsp;&nbsp;
                    <a href="<?php echo site_url("/persuratan/goto_layanan/$param->model/$param->tahun/$param->bulan/$param->bagian/4/$param->rowstart") ?>">
                        <!-- <span class="small text-danger hidden-xs">Terlambat </span> -->
                        <span class="badge bg-red"><?php echo $badge[4] ?></span></a>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="box box-widget">
            <div class="box-body with-border">
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover table-bordered">
                        <thead>
                        <tr style="background: #00acd6; color: #fff;">
                          <th style="width: 8%">Tanggal <br> Surat</th>
                          <th>No Agenda <br> Disposisi</th>
                          <th>Surat Pengirim <br> Surat Keluar</th>
                          <th>Hal <br> Uraian Disposisi</th>
                          <th class="hidden-md hidden-xs">Pengirim <br>Catatan Disposisi</th>
                        </tr>
                        </thead>
                        <tbody id="playground">
                        <?php
                        if ($surat) {
                          $no= $param->rowstart + 1;

                          foreach ($surat as $row) {
                            $url = site_url("/persuratan/goto_layanan/$param->model/1/$param->tahun/$param->bulan");
                            $golsurat = 'ND-';
                              if ($row['golsurat']=='1' and $row['jenis']=='10') $golsurat = 'RSM-';
                              if ($row['golsurat']=='2') $golsurat = 'UND-';
                            $tgagenda = date('d-m-Y',strtotime($row['tgagenda'])); if($row['tgagenda']=="01-01-1970") $tgagenda="&nbsp;";
                            $selesai  = date('d-m-Y',strtotime($row['selesai']));   if($row['selesai']=="0000-00-00")  $selesai="&nbsp;";
                            $tgsurat  = date('d-m-Y',strtotime($row['tgsurat']));  if($row['tgsurat']=="01-01-1970")  $tgsurat="&nbsp;";
                            $perihal  = $row['perihal'] . $row['perihal2'];
                            $warna    = array('#F9F9F9','#5BC0DE','#49A049','#EB9316','#CE4844');
                            if ($row['nmfile'] && $this->session->userdata('kodelink')=='1') {
                                //$perihal = "<a href='". file_put_contents($row['nmfile'], fopen("\\\\10.242.42.62\\suratdja_repo\\2015\\". $row['nmfile'], 'r')) ."' target='_blank'>$perihal</a>";
                                $perihal = "<a href='". site_url("/persuratan/open_pdf/". $row['nmfile']) ."' target='_blank'>". $row['perihal'] ."</a>". $row['perihal2'];
                            }

                            $catatan = '';
                            if ($row['catatantup']) { $catatan .= '<br><label style="color:#EC9A25">'. $row['catatantup']; }
                            if ($row['catatan']) { $catatan .= '<br><label style="color:#EC9A25">'. $row['catatan']; }
                            if ($row['catatan1']) { $catatan .= '<br><label style="color:#EC9A25">'. $row['catatan1']; }

                            $tlnosurat = '';
                            if ($row['tlnosurat']) { $tlnosurat .= '<br /><br /><label style="color:#419641">'. $row['tlnosurat']; }

                        ?>
                            <tr>
                                <td> <?php echo "$tgagenda<br><h4 style='color:". $warna[$row['nilai']] ."'>". $row['status'] ."</h4>"?></td>
                                <td class="text-left" style="border-left-color: <?php echo $warna[$row['nilai']] ?>; border-left-style: solid; border-left-width: 2px;">
                                  <?php echo "<b>$golsurat". $row['noagenda'].$row['noagenda1'] ."</b>". $row['selesai'] . $row['dispos'] ?>
                                </td>
                                <td class="text-left">
                                  <?php echo "<b>$tgsurat<br>". $row['nosurat'] ."</b>" . $tlnosurat ?>
                                </td>
                                <td class="text-left content"> <?php echo $perihal ?></td>
                                <td class="text-left hidden-md hidden-xs"> <?php echo $row['pengirim'] . $catatan .'</label>' ?></td>
                            </tr>
                        <?php
                            $no++;

                          }
                        } else {
                            echo '<tr><td colspan="5" class="text-center"><span class="danger"><i>Tidak ada data ...</i></span></td></tr>';
                        }
                        ?>
                    </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12" style="margin-top:-20px">
        <?php echo "<span class='small hidden-xs' style='padding-top:40px'>[ ". $bagian[ $param->bagian ]['uraian2'] .": $record records ]</span>"; ?>
        <span class="small"> &nbsp;&nbsp;&nbsp;Ket :
            <i class="fa fa-circle text-aqua"> <span class="small"> Selesai</span></i>&nbsp;
            <i class="fa fa-circle text-green"> <span class="small"> Proses</span></i>&nbsp;
            <i class="fa fa-circle text-yellow"> <span class="small"> Segera</span></i>&nbsp;
            <i class="fa fa-circle text-red"> <span class="small"> Terlambat</span></i>
        </span>
        <?php echo $this->pagination->create_links(); ?>
    </div>

</div>

<script>
(function($){
  var combotree = $.fn.combotree.defaults.onChange;
  $.fn.combotree.defaults.onChange = function(newValue, oldValue){
    // alert(newValue);
    var url = '<?php echo site_url("persuratan/goto_layanan/$param->model/$param->tahun/$param->bulan/") ?>';
    window.open( url +'/'+ newValue, '_parent' );
  };
})
(jQuery);
</script>
<script type="text/javascript">
  $('.pagination').addClass('pagination-sm pull-right');
</script>
